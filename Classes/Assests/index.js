var ajaxUrl = [];

// 懒加载图片偏移量设置
var offsetYWebViewLoadImageFloat = 280;

function scrollMethod(offesty, cache) {

  $("img").each(function(index, img) {

    var datasrc = $(this).attr("data-src")

    if ($(this).offset().top < offesty + offsetYWebViewLoadImageFloat && ajaxUrl.indexOf(datasrc) == -1) {

      ajaxUrl.push(datasrc);

      if (cache) {

        sendScrollToLoadImageMethod(index,datasrc);

      } else {

        $(this).attr("src", datasrc);
      }
    }
  })
};

/**
 * 刷新当前页面的标题以及内容的自提大小
 *
 * @param titleSize {Float} 页面内标题的像素大小
 * @param bodySize {Float} 页面内内容的像素大小
 */
function refreshFontModeMethod(titleSize,bodySize) {

    $("#title").css("font-size", titleSize+'px');
    $("#body_section").css("font-size", bodySize+'px');
}

/**
 * 在用户完成了 评论之后需要刷新页面的内容的时候，完善新闻的评论数目字符串，刷新页面
 *
 * @param desc {String} 新闻其他信息的描述字符串
 */
function refreshInfoDescString(desc) {
  $("#subtitle").html(desc);
};

/**
 * 在用户更高了 当前的模式的时候 - 包含 夜间模式和日渐模式 修改页面的一些参数
 * 包含 页面背景颜色 标题颜色 自标题颜色 超链接颜色 以及 Img 标签的蒙层
 *
 * @param backColor {String} 页面的背景颜色
 * @param titleColor {String} 页面标题的颜色
 * @param subtitleColor {String} 页面子标题的颜色
 * @return lineColor {String} 页面超链接的颜色
 * @return alpha {Float} 页面图片div的alpha程度
 */
function refreshThemeMethod(backColor, titleColor, subtitleColor, lineColor, alpha) {
  $(document.body).css("background-color", backColor);


  $(".line-class").css("background-color", lineColor);

  $(".imgDiv").css("opacity", alpha);

  $("#title").css("color", titleColor);
  $("#subtitle").css("color", subtitleColor);
  $("#body_section").css("color", titleColor);

  $("a").css("color", titleColor);
  $("a:link").css("color", titleColor);
  $("a:visited").css("color", titleColor);
  $("a:hover").css("color", titleColor);
  $("a:active").css("color", titleColor);
    
  $("img").css("background-color", lineColor);
};

$(function() {

  /// 监听 所有的 Img 的点击事件儿
  $("#body_section img").click(function() {

      sendClickImageMethod($("img").index(this),getAbsoluteRectByElement(this));
  });

  if (window.webkit) {

    $('img').load(function() {
      window.webkit.messageHandlers.JSBridge.postMessage({
        "type": 0
      });
    });
  }

  var winWidth = $("#widthSc").text()

  $("#video iframe").width(winWidth - 36);
  $("#video iframe").height(winWidth * 3 / 5);
  $("#video iframe").addClass("center-block");
  $("#video iframe").attr("frameborder", "no");

  $("a").each(function(index, img) {

    $(this).attr("href", "javascript:volid(0);");
  });
});


function getAbsoluteRectByIndex(i){

	var o = $("img")[i];

	return getAbsoluteRectByElement(o);
}

function getAbsoluteRectByElement(o){

	var rectJsonObj = "{{"+o.offsetLeft+","+o.offsetTop+"},{"+o.offsetWidth+","+o.offsetHeight+"}}";

	return rectJsonObj;
}

function makeHidden(ishidden,index){
    
    var o = $("img")[index];
    
    if(ishidden) {
        
        $(o).attr("style","visibility:hidden;");//隐藏div
    }else{
        $(o).attr("style","visibility:visible;");//显示div
    }
}

/**
 *
 发送点击图片的方法
 *
 @method 方法名
 *
 @param {int} index 点击的img index
 @param {string} imgframe index img 的Frame string
 */
function sendClickImageMethod(index,imgframe){

  // 配置 WKWebview js 交互的方法
  if ( window.webkit ){
    window.webkit.messageHandlers.JSBridge.postMessage({
      "type": 1,
      "index": index,
      "imgframe": imgframe
    });
  }

  // 配置 UIWebView 的js 交互方式
  clickImage(index,imgframe);
}

/**
 *
 发送 需要加载这个图片的消息
 *
 @method 方法名
 *
 @param {int} index 点击的img index
 @param {string} url 图片的地址
 */
function sendScrollToLoadImageMethod(index,url) {

  if ( window.webkit ) {
    window.webkit.messageHandlers.JSBridge.postMessage({
      "type": 3,
      "index": index,
      "url": url
    });
  }

  scrollToLoadImage(index,url);
}