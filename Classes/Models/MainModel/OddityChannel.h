//
//  OddityChannel.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityNew.h"
#import <Realm/Realm.h>
#import "OddityChannelInterface.h"


@interface OddityChannel : RLMObject

/**
 *  频道服务器请求 默认处理的数据
 *  服务器的数据返回 并不需要进行处理
 */

/// 频道ID 唯一表示
@property int id;
/// 频道名称 名称
@property NSString* cname;



/**
 *  排序的参数 version ~> 0.2 数据库版本 2
 *  版本迁移所需参数
 */
@property int isdelete;
@property int orderindex;

/**
 *  是否向用户隐藏的参数 version ~> 0.3 数据库版本 3
 *  版本迁移所需参数
 */
@property BOOL isHidden;



/**
 *  version ~> 0.4 数据库版本 4
 *  新版本添加字符串
 */
/// 排序的index
@property int order_num;
/// 频道是否上线  每次请求成功默认处理为
@property int state;
/// 是否选中 1.已选 0.未选 和 isHidden 一样
@property int selected;
/// 是否含有子频道 1. 含有 0 不含邮
@property int has_children;
/// 父级 频道 -> 用于子频道的搜索
@property OddityChannel *personChannel;


/**
 是否是子频道
 */
@property BOOL is_children;
/**
 *  频道服务器请求 默认处理的数据
 *  版本迁移所需参数
 */

/// 是否为视频频道 只读属性
@property (readonly) BOOL isVideoChannel;

/**
 频道从服务中 请求数据 并且完成数据的更新替换

 @param complete 请求失败
 */
+(void)reloadDataSourceSort:(void (^)(BOOL success))complete;


/**
 请求子频道
 */
-(void)requestSubChannel;


-(RLMResults<OddityChannel *> *)subChannelResults;

@end
RLM_ARRAY_TYPE(OddityChannel) // define RLMArray<Dog>
