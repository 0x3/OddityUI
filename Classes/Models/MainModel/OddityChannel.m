//
//  OddityChannel.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityUtils.h"

#import "OddityChannel.h"
#import "OddityRequest.h"
#import "OddityCategorys.h"

#import "OddityShareUser.h"

#import "OddityNewsArrayObject.h"

#import "OddityLogObject.h"


#define ODDITY_CHANNEL_VERSION_ADJ @"odditychannelversionadj"


@implementation OddityChannel

/**
 *  设置频道对象的 主键
 *
 *  @return 主键字符串
 */
+ (NSString *)primaryKey {
    return @"id";
}


+(NSDictionary *)defaultPropertyValues{

    return @{
             @"is_children":@(false),
             @"has_children":@(0),
             @"order_num":@(0),
             @"state":@(0),
             @"selected":@(0),
             @"isdelete":@(0),
             @"isHidden":@(false),
             @"orderindex":@(999)
             };
}

-(BOOL)isVideoChannel{
    
    return self.id == 44 || [self.cname isEqualToString:@"视频"] || self.personChannel.id == 44 || [self.personChannel.cname isEqualToString:@"视频"];
}



/**
 *  请求网络数据 刷新本地 频道库
 */
+(void)reloadDataSourceSort:(void (^)(BOOL success))complete{
    
    BOOL firstRequest = [OddityChannel allObjectsInRealm:[RLMRealm OddityRealm]].count <= 0; //  是否第一次进入该程序
    BOOL firstJsonThreeVersion = [OddityNewsArrayObject channelManager].normalChannelArray.count <= 0; //  是否第一次进入该程序de 0.3版本
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[[OddityAFAppDotNetAPIClient sharedClient] GET:@"/v2/ns/ch/cho" parameters: @{@"channel":@(OddityShareUser.defaultUser.channelChannelId)} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] != 2000) {
            
            if (complete) {
                
                complete(false);
            }
            
            return;
        }
        
        RLMRealm *realm = [RLMRealm OddityRealm];
        
        [realm beginWriteTransaction];
        
        [self VERSION_MAKE_UPGRADE_HANDLE_METHOD:firstRequest :firstJsonThreeVersion :realm];
        
        if (!OddityShareUser.defaultUser.isAdjustmentChannel) {
            
            [OddityNewsArrayObject.channelManager.normalChannelArray removeAllObjects];
            [OddityNewsArrayObject.channelManager.deleteChannelArray removeAllObjects];
        }
        
        NSArray *arrayDatas = [([responseObject objectForKey:@"data"] ?: @[]) sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            
            return [obj1[@"order_num"] integerValue] > [obj2[@"order_num"] integerValue] ? NSOrderedDescending : NSOrderedAscending;
        }];
        
        for (id data in arrayDatas) {

            OddityChannel *channel = [OddityChannel createOrUpdateInRealm:realm withValue:data];
            
            if (firstRequest || !OddityShareUser.defaultUser.isAdjustmentChannel) { // 如果是第一次请求
                
                [[OddityNewsArrayObject channelManager] appendChannel:channel isNormal: channel.selected];
            }
        }
        
        [self deleteAllStateZeroChannel:realm];
        
        [realm commitWriteTransaction];
        
        if (complete) {
            
            complete(true);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (complete) {
            
            complete(false);
        }
    }] resume];
}



-(void)requestSubChannel{

    NSDictionary *param = @{
                            @"channel":@(OddityShareUser.defaultUser.channelChannelId),
                            @"chid":@(self.id)
                            };
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[OddityAFAppDotNetAPIClient sharedClient] GET:@"/v2/ns/ch/scho" parameters: param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] != 2000) {
            return;
        }
        
        RLMRealm *realm = [RLMRealm OddityRealm];
        
        [realm beginWriteTransaction];
        
        for (id data in [responseObject objectForKey:@"data"] ?: @[]) {
            
            OddityChannel *channel = [OddityChannel createOrUpdateInRealm:realm withValue:data];
            
            channel.is_children = true;
            
            channel.personChannel = self;
        }
        
        [realm commitWriteTransaction];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
    }];
}








/**
 数据库版本 迁移的方法
 
 @param firstRequest 第一次进入该应用 也就是 直接使用 最新版本
 @param firstJsonThreeVersion 是否第一次进入第三版本的
 @param realm 数据库
 */
+(void)VERSION_MAKE_UPGRADE_HANDLE_METHOD:(BOOL)firstRequest :(BOOL)firstJsonThreeVersion :(RLMRealm *)realm{
    
    RLMResults<OddityChannel *> *results =  [[[OddityChannel allObjectsInRealm:realm] objectsWhere:@"is_children = false"] sortedResultsUsingKeyPath:@"orderindex" ascending:true];
    
    if (results <= 0) {
        
        return;
    }
    
    if (!firstRequest && firstJsonThreeVersion) { // OdditySDK Version ~> 0.2 到 ~> 0.3 版本 数据版本 Version 2 -> 3
        
        for (OddityChannel *channel in [results objectsWhere:@"isdelete == 0"] ) {
            
            [OddityNewsArrayObject.channelManager appendChannel:channel isNormal:true];
        }
        
        for (OddityChannel *channel in [results objectsWhere:@"isdelete == 1"] ) {
            
            [[OddityNewsArrayObject channelManager] appendChannel:channel isNormal:false];
        }
    
        /// 如果既不是第一次请求数据 或者 不是 第三次 数据
        
    }
    
    for (OddityChannel *channel in [results objectsWhere:@"is_children = false"]) {
        
        channel.state = 0;
    }
}


/**
 判断版本 是否 小于 某个版本

 @param version 版本
 @return 是否小雨
 */
+(BOOL)JudgementVersion:(NSInteger)version{
    
    if ([NSUserDefaults.standardUserDefaults integerForKey:ODDITY_CHANNEL_VERSION_ADJ] < version) {
        
        [NSUserDefaults.standardUserDefaults setInteger:version forKey:ODDITY_CHANNEL_VERSION_ADJ];
        [NSUserDefaults.standardUserDefaults synchronize];
        
        return true;
    }
    
    return false;
}


/**
 删除所有下线频道 对象

 @param realm 数据库 对象
 */
+(void)deleteAllStateZeroChannel:(RLMRealm *)realm{

    RLMResults<OddityChannel *> *results =  [[OddityChannel allObjectsInRealm:realm] objectsWhere:@"state == 0"];
    
    for (OddityChannel *channel in results) {
        
        [realm deleteObject:channel];
    }
}

-(RLMResults<OddityChannel *> *)subChannelResults{

    return [[[OddityChannel allObjectsInRealm:RLMRealm.OddityRealm] objectsWhere:@"state == 1 AND personChannel == %@",self] sortedResultsUsingKeyPath:@"order_num" ascending:true];
}

@end
