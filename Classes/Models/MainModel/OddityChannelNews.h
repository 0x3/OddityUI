//
//  OddityChannelNews.h
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "OddityNew.h"
#import "OddityChannel.h"

#import <Realm/Realm.h>

typedef void(^OddityRequestNewDataBlock) (BOOL success,BOOL refresh,NSString *message,UIColor *backColor);

@interface OddityChannelNews : RLMObject


/**
 *  新闻广告对象
 */
@property OddityChannel *channel;
/**
 *  该频道所存储的新闻集合
 */
@property RLMArray<OddityNew *><OddityNew> *pressListArray;


+(instancetype)getObjectBy:(OddityChannel *)channel;




/**
 *  获取所有的新闻集合 isdelete 为 0 并且按照时间 进行倒叙 排序
 *
 *  @return 新闻集合
 */
-(RLMResults *)allNewArrlyResults;

/**
 *  在获取用户之前 如果当前频道的数据大雨 30个，那么就进行 数据的删除
 *
 *  @return 新闻集合
 */
-(RLMResults <OddityNew *>*)carvNewArrlyResults;


/**
 请求新闻对象
 
 @param refresh 刷新还是 加载 更多新闻
 @param date 请求依据事件
 @param cdate 创建时间
 @param block 完成回调
 */
-(void)requestNewDataSource:(BOOL)refresh reqDate:(NSDate *)date creDate:(NSDate *)cdate complete:(OddityRequestNewDataBlock)block;


/**
 删除新闻

 @param new 新闻
 */
-(void)toDeletNew:(OddityNew *)new;

@end
