//
//  OddityChannelNews.m
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "OddityLogObject.h"

#import "OddityShareUser.h"
#import "OddityAFAppDotNetAPIClient.h"

#import "OddityCategorys.h"
#import "OddityChannelNews.h"

#import "OddityManagerInfoMationUtil.h"

/// 是否允许有重复的额新闻
#define OddityIsAllowRepeat true

#define OddityMessageViewAlpha 0.6f

@implementation OddityChannelNews


+(instancetype)getObjectBy:(OddityChannel *)channel{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    OddityChannelNews *object =  [self objectsInRealm:realm where:@"channel = %@",channel].firstObject;
    
    if (!object) {
        
        if (realm.inWriteTransaction) {
            
            object = [OddityChannelNews createInRealm:realm withValue:@{ @"channel" : channel }];
            
        }else{
        
            [realm beginWriteTransaction];
            
            object = [OddityChannelNews createInRealm:realm withValue:@{ @"channel" : channel }];
            
            [realm commitWriteTransaction];
        }
    }
    
    return object;
}




-(RLMResults *)allNewArrlyResults{
    
    return [[self.pressListArray objectsWhere:@"isdelete == 0"] sortedResultsUsingKeyPath:@"ptimes" ascending:false];
}

-(RLMResults <OddityNew *>*)carvNewArrlyResults{
    
    RLMResults <OddityNew *> *results = [[self.pressListArray objectsWhere:@"isdelete = 0"] sortedResultsUsingKeyPath:@"ptimes" ascending:false];
    
    if (results.count > 10 && ![[RLMRealm OddityRealm] inWriteTransaction]) {
        
        [[RLMRealm OddityRealm]beginWriteTransaction];
        
        /**
         *  获取所有的 时间校于 第二十个 的新闻，并且移除掉
         */
        for (OddityNew *new in [results objectsWhere:@"ptimes < %@",[results objectAtIndex:9].ptimes] ?: @[]) {
            
            /**
             *  如果新闻的 已经删除 那么为了防止 用户察觉，删除的新闻还会存在，那么就不进行移除了
             */
            if(new.isdelete == 1 ) { continue; }
            
            [self.pressListArray removeObjectAtIndex:[self.pressListArray indexOfObject:new]];
        }
        
        [[RLMRealm OddityRealm]commitWriteTransaction];
    }
    
    return results;
}


-(void)requestNewDataSource:(BOOL)refresh reqDate:(NSDate *)date creDate:(NSDate *)cdate complete:(OddityRequestNewDataBlock)block{
    
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *hostUrl = refresh ? @"/v2/ns/fed/ra" : @"/v2/ns/fed/la";
    
    /**
     *  生成广告新闻的请求字段
     */
    
    NSString *placementID = [OddityShareUser userPlacementBy:(OddityAdvertisementPlacementModeFeedBig) RequestAdsMode:(OddityUserRequestAdsModeGDTApi)];
    
    NSString *base64 = [[OddityManagerInfoMationUtil managerInfoMation] getAdsInfo:placementID];
    
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setValuesForKeysWithDictionary:@{
                                           @"cid":@(self.channel.id),
                                           @"tcr":@([[date oddity_unixString] longLongValue]),
                                           @"tmk":@(0),
                                           @"uid":@(uid),
#if oddityIsHttps
                                           @"s":@(1),
#else
                                           @"s":@(0),
#endif
                                           @"t":@(1),
                                           @"v":@(1),
                                           
                                           @"b":base64,
                                           @"ads": @(OddityShareUser.defaultUser.getAdsMode)
                                           }];
    
    if (self.channel.personChannel) {
        
        [dict setValue:@(self.channel.personChannel.id) forKey:@"cid"];
        [dict setValue:@(self.channel.id) forKey:@"scid"];
    }
    
    /// 开始加载数据了
    [self beginDelegateMethod];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:hostUrl parameters: dict success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        RLMRealm *realm = [RLMRealm OddityRealm];
        
        NSInteger code =  [responseObject[@"code"] integerValue];
        NSArray *data = responseObject[@"data"];
        
        if ( code == 2000 && data.count > 0) {
            
            [realm beginWriteTransaction];
            
            [self refreshIsidentification:refresh count:data.count date:cdate];
            
            __block OddityNew *adsNew;
            
            NSInteger adIndex =  self.channel.isVideoChannel ? OddityShareUser.defaultUser.feedVideoAdPos : OddityShareUser.defaultUser.feedAdPos;
            
            NSArray *getArray = [OddityNew dealWithDifferentAds:responseObject[@"data"] isrefresh:refresh insertIndex:adIndex];
            
            for (id data in getArray) {
                
                @autoreleasepool {
                
                    OddityNew * oddityObj = [[OddityNew createOrUpdateInRealm:[RLMRealm OddityRealm] withValue: data] analysis:data];
                    
                    
#if OddityIsAllowRepeat // 允许重复 不做判断
                    
                    BOOL isCanInsertToDB = true;
#else
                    
                    BOOL isCanInsertToDB = [self.pressListArray indexOfObject:oddityObj] == NSNotFound;
#endif
                    
                    if( isCanInsertToDB ) {
                        
                        if (oddityObj.rtype == 3) {
                            
                            adsNew = oddityObj;
                        }
                        
                        if (refresh) {
                            
                            [self.pressListArray insertObject:oddityObj atIndex:0];
                        }else{
                            
                            [self.pressListArray addObject:oddityObj];
                        }
                    }
                }
            }
            
            [realm commitWriteTransaction];
            
            /// 打点 -> 获取广告打点
            [OddityLogObject createAdGet:adsNew snum:adsNew ? 1 : 0 rnum:1];
            
            NSString *message = [NSString stringWithFormat:NSString.oddity_request_success_count_string,(unsigned long)getArray.count];
            
            block(true,refresh,message,[[OddityUISetting shareOddityUISetting].mainTone colorWithAlphaComponent:OddityMessageViewAlpha]);
            
            [self finishDelegateMethod];
            
        }else if(code == 2002){
            
            [self failDelegateMethod];
            
            NSString *message = NSString.oddity_request_success_nomore_string;
            block(false,refresh,message,[[OddityUISetting shareOddityUISetting].mainTone colorWithAlphaComponent:OddityMessageViewAlpha]);
            
        }else{
            
            [self failDelegateMethod];
            
            NSString *message = NSString.oddity_request_fail_data_error_string;
            block(false,refresh,message,[[UIColor color_t_7] colorWithAlphaComponent:OddityMessageViewAlpha]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        [self failDelegateMethod];
        
        NSString *message = NSString.oddity_request_fail_network_error_string;
        block(false,refresh,message,[[UIColor color_t_7] colorWithAlphaComponent:OddityMessageViewAlpha]);
        
    }] resume];

}

/**
 *  开始加载数据
 */
-(void)beginDelegateMethod{
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didBeginLoadingNewArrayAction:)] ){
        
        [[OdditySetting shareOdditySetting].delegate didBeginLoadingNewArrayAction:self.channel.cname];
    }
}

/**
 *   加载数据成功
 */
-(void)finishDelegateMethod{
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didFinishRequestNewListAction:)] ){
        
        [[OdditySetting shareOdditySetting].delegate didFinishRequestNewListAction:self.channel.cname];
    }
}

/**
 *   加载数据失败
 */
-(void)failDelegateMethod{
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didFailRequestNewListAction:)] ){
        
        [[OdditySetting shareOdditySetting].delegate didFailRequestNewListAction:self.channel.cname];
    }
}

/**
 根据所提供的时间 进行生成 刷新 cell的方法

 @param refresh 是否是刷新
 @param c 数据的个数
 @param d 时间
 @return 新闻对象
 */
-(OddityNew *)refreshIsidentification:(BOOL)refresh count:(NSInteger)c date:(NSDate *)d {
    
    if (!refresh || c <= 0 || self.pressListArray.count <= 0) {
        return nil;
    }
    
    OddityNew *new = [self.pressListArray objectsWhere:@"isidentification == 1"].firstObject;
    
    if (new ) {
        
        [[RLMRealm OddityRealm] deleteObject:new];
    }
    
    
    OddityNew *cnew = [OddityNew createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:@{
                                                                              @"isidentification":@1,
                                                                              @"nid":@(self.channel.id-199)
                                                                              }];
    
    
    [self.pressListArray insertObject:cnew atIndex:0];
    
//    if (!new) {
//    
//        NSDictionary *param = @{@"isidentification":@1,@"nid":@(self.channel.id-199)};
//        new = [OddityNew createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:param];
//        [self.pressListArray addObject:new];
//    }
//    
//    new.ptimes = [NSDate dateWithTimeIntervalSince1970:([d oddity_unixInteger]+1)/1000];
    
    return cnew;
}


-(void)toDeletNew:(OddityNew *)new{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
        
        NSInteger index = [self.pressListArray indexOfObject:new];
        
        if (index != NSNotFound) {
        
            [self.pressListArray removeObjectAtIndex:index];
        }
    }
    
    [realm transactionWithBlock:^{
       
        NSInteger index = [self.pressListArray indexOfObject:new];
        
        if (index != NSNotFound) {
            
            [self.pressListArray removeObjectAtIndex:index];
        }
    }];
}


@end
