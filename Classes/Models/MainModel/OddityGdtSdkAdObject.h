//
//  OddityGdtSdkAdObject.h
//  Pods
//
//  Created by 荆文征 on 2017/5/9.
//
//

#import <Realm/Realm.h>




/**
 广告从拉取到曝光超过 45 分钟，将作为无效曝光。
 4. 广告从拉取到点击超过 90 分钟，将作为无效点击，不会进行计费。
 */
@interface OddityGdtSdkAdObject : RLMObject


/**
 广点通 sdk 广告对象的 nsdata的对象
 */
@property NSData *gdtSdkAdObjectData;


/**
 入库时间
 */
@property NSDate *storageTime;

/**
 过期时间
 */
@property NSDate *overdueTime;

@end
RLM_ARRAY_TYPE(OddityGdtSdkAdObject)
