//
//  OddityLogObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/18.
//
//

#import "OddityNew.h"
#import "OddityNewContent.h"

#import <Realm/Realm.h>


//ptype对应表
//参数值	对应平台
//IOS	IOS平台
//Android	安卓平台

//ctype对应表
//参数值	对应渠道
//qidian	奇点资讯
//huangli	黄历天气
//wenzi	纹字锁屏
//lybrowser	猎鹰浏览器
//baipai	白牌


/// 新闻来源地址
#define OdditySourceFeedName @"feed"
#define OdditySourcerelateName @"relate"


/// 新闻广告来源字符串
#define OddityAdvertisementNoneSourceStr @"none"
#define OddityAdvertisementGDTAPISourceStr @"gdtAPI"
#define OddityAdvertisementGDTSDKSourceStr @"gdtSDK"
#define OddityAdvertisementYifuSDKSourceStr @"yifuSDK"

/// 用户的动作
#define OddityLogAtypeLogin @"login" /// 登陆
#define OddityLogAtypeRefreshFeed @"refreshFeed" /// 上拉刷新feed流
#define OddityLogAtypeLoadFeed @"loadFeed" /// 下拉加载feed流
#define OddityLogAtypeDetailClick @"detailClick" /// 点击详情页
#define OddityLogAtypeCommentClick @"commentClick" /// 点击获取评论
#define OddityLogAtypeChangeChannel @"changeChannel" /// 切换频道
#define OddityLogAtypeSubChannel @"subChannel" /// 订阅频道
#define OddityLogAtypeSubPublisher @"subPublisher" /// 订阅来源
#define OddityLogAtypeSearch @"search" /// 搜索
#define OddityLogAtypeRelateClick @"relateClick" /// 点击相关新闻
#define OddityLogAtypeMyComments @"myComments" /// 我的评论
#define OddityLogAtypeMyCollections @"myCollections" /// 我的收藏
#define OddityLogAtypeMyMessages @"myMessages" /// 我的消息
#define OddityLogAtypeLike @"like" /// 点赞
#define OddityLogAtypeComment @"comment" /// 评论
#define OddityLogAtypeShare @"share" /// 分享
#define OddityLogAtypeCollect @"collect" /// 分享
#define OddityLogAtypeChangeMode @"changeMode" /// 日夜间模式切换
#define OddityLogAtypeFeedVideoPlay @"feedVideoPlay" /// feed流播放视频
#define OddityLogAtypeDislike @"dislike" /// 不喜欢新闻
#define OddityLogAtypePublisherClick @"publisherClick" /// 点击来源 pname（来源名称）,source(search（从搜索页面点击）、detail（从个详情页面点击）)
#define OddityLogAtypeProfileClick @"profileClick" /// 点击个人中心
#define OddityLogAtypeFontSetting @"fontSetting" /// 字体调整
#define OddityLogAtypeMoveChannel @"moveChannel" /// 移动频道
#define OddityLogAtypeAddChannel @"addChannel" /// 添加频道
#define OddityLogAtypeChannelPageClick @"channelPageClick" /// 点击频道管理
#define OddityLogAtypeProfileSettingClick @"profileSettingClick" /// 点击个中心 设置页面
#define OddityLogAtypeBlockNews @"blockNews" /// 不喜欢新闻上报
#define OddityLogAtypeJumpDetail @"jumpDetail" /// 黄历列表页跳转详情页
#define OddityLogAtypeBackFeed @"backFeed" /// 返回列表页
#define OddityLogAtypeJumpBack @"jumpBack" /// 跳转回黄历


/// 用户的页面
#define OddityLogPageNullPage @"nullPage" ///空白页面
#define OddityLogPageFeedPage @"feedPage" ///feed流页面
#define OddityLogPageDetailPage @"detailPage" ///新闻详情页
#define OddityLogPageChannelPage @"channelPage" ///频道设置页面
#define OddityLogPageTopicPage @"topicPage" ///feed流页面
#define OddityLogPageLoginPage @"loginPage" /// 登陆页面
#define OddityLogPageSearchPage @"searchPage" ///搜索页面
#define OddityLogPagePfeedPage @"pfeedPage" ///来源feed流页面
#define OddityLogPageMyCommentPage @"myCommentPage" ///我的评论页面
#define OddityLogPageMyCollectionPage @"myCollectionPage" ///我的收藏页面
#define OddityLogPageMyMessagePage @"myMessagePage" ///我的消息页面
#define OddityLogPageSettingPage @"settingPage" ///设置页面
#define OddityLogPageUserCenterPage @"userCenterPage" /// 个人中心



/**
 日志类型

 - OddityLogTypeShow: 展示日志每4条未展示过的新闻漏出
 - OddityLogTypeNewClick: 新闻点击日志 详情页成功加载
 - OddityLogTypeNewRead: 新闻阅读时长日志 从详情页跳转出
 - OddityLogTypeAdClick: 广告点击日志 请求时机：点击时同步上传
 - OddityLogTypeAppUse: APP使用时长日志 统计后异步上传
 - OddityLogTypeHuangliSlide: 黄历天气滑动结算
 - OddityLogTypeAppAction: 用户操作行为 事件触发后实时上传
 - OddityLogTypeAdGet: 广告请求日志 广告请求后实时上传
 - OddityLogTypeSignup: 用户注册完成后日志上传
 */
typedef NS_ENUM(NSUInteger, OddityLogType) {
    OddityLogTypeShow = 1,
    OddityLogTypeNewClick = 2,
    OddityLogTypeNewRead = 3,
    OddityLogTypeAdClick = 4,
    OddityLogTypeAppUse = 5,
    OddityLogTypeHuangliSlide = 6,
    OddityLogTypeAppAction = 7,
    OddityLogTypeAdGet = 8,
    OddityLogTypeSignup = 9
};



//新闻上报source: feed
//广告上报source: gdtAPI   gdtSDK


/// 专门用来上传日志的一个对象 用于保证 日志的绝对上传
@interface OddityLogObject : RLMObject

/**
 *  请求的链接
 */
@property NSString *param;


/**
 当前需要上传的日志的 类型
 */
@property int logType;


/**
 该日志是否已经上传到服务器了
 */
@property BOOL isUploaded;


/**
 上传日志错误次数
 */
@property int uploadErrCount;


/**
 日志创建时间
 */
@property NSDate *createDate;


/**
 检查数据库中的 日志对象 并且上传
 
 该方法在用户重新打开应用时将会打开时 判断是否还有没有没有上传的日志 进行上传
 */
+(void)checkoutAndUploadMethod;


/**
 创建一个 展示 新闻的 日志提交

 @param array 查看的默认的日记
 */
+(void)createNewShow:(NSArray *)array;


/**
 创建 展示 相关新闻 日志

 @param about 相关新闻对象
 @param chid 频道 id
 */
+(void)createRelateNewShow:(OddityNewContentAbout *)about channel:(NSInteger)chid;

/**
 创建一个 新闻 点击 日志对象

 @param nid 新闻id
 @param cid 频道id
 @param lt 服务器传回的logtype
 @param lg 服务器传回的logchid
 @param sou 点击新闻查看的地方
 */
+(void)createNewsClick:(int)nid chid:(int)cid logtype:(int)lt logchid:(int)lg source:(NSString *)sou;


/**
 创建 新闻阅读市场的 日志 对象

 @param nid 新闻 id
 @param bt 新闻开始阅读时间
 @param et 新闻结束阅读时间
 @param rt 新闻阅读时长
 @param per 新闻华东进度
 */
+(void)createNewsRead:(NSInteger)nid beginTime:(long long)bt endTime:(long long)et readTime:(long long)rt percent:(float)per;



/**
 创建 应用使用时长

 @param bt 开始使用时间
 @param et 结束使用时间
 @param ut 使用时长秒数
 */
+(void)createAppUse:(long long)bt endTime:(long long)et useTime:(long long)ut;

/**
 黄历天气结算打点

 @param params 打点类型
 */
+(void)createHuangLiSlide:(NSDictionary *)params;


/**
 创建应用动作

 @param atype 类型
 @param from 来源的页面
 @param to 去往的页面
 @param effe 是否加载了新数据
 */
+(void)createAppAction:(NSString *)atype fromPageName:(NSString *)from toPageName:(NSString *)to effective:(BOOL)effe params:(NSDictionary *)pa;


/**
 用户注册成功日志上传接口
 */
+(void)createUserSignup;





/**
 创建广告展示
 
 @param object 对象
 */
+(void)createAdShow:(id)object;


/**
 创建 广告点击打点
 
 @param object 广告对象
 */
+(void)createAdsClick:(id)object;



/**
 广告请求日志
 
 @param object 对象
 @param s 想要请求的数目
 @param r 请求成功的数目
 */
+(void)createAdGet:(id)object snum:(int)s rnum:(int)r;

@end
