//
//  OddityLogObject.m
//  Pods
//
//  Created by 荆文征 on 2017/4/18.
//
//

#import "OddityModels.h"

#import "OddityCategorys.h"

#import "OddityShareUser.h"

#import "OddityLogObject.h"
#import "OddityManagerInfoMationUtil.h"

#import <AFNetworking/AFNetworking.h>


/// 是否 https
#define oddityLogIsHttps false

#if oddityLogIsHttps
static NSString * const LogBaseUrlString = @"https://log.deeporiginalx.com";
#else
static NSString * const LogBaseUrlString = @"http://log.deeporiginalx.com";
#endif




@implementation OddityLogObject

+(NSDictionary *)defaultPropertyValues{

    return @{
             @"isUploaded":@false,
             @"uploadErrCount" : @0,
             @"createDate" : [NSDate date]
             };
}


+(void)createLog:(OddityLogType)lt withParam:(NSDictionary *)param{

        NSDictionary *dict = @{
                               @"logType":@((int)lt),
                               @"param":[param oddity_dataToJsonString]
                               };
    
    NSString *queueName = [NSString stringWithFormat:@"com.deeporiginalx.gcd.adsupload-%@", [[NSUUID UUID] UUIDString]];
    
    dispatch_queue_t queue = dispatch_queue_create([queueName cStringUsingEncoding:NSASCIIStringEncoding], DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(queue, ^{
        
        @autoreleasepool {
            
            RLMRealm *realm = [RLMRealm OddityRealm];
            
            [realm beginWriteTransaction];
            
            OddityLogObject *logObj = [OddityLogObject createInRealm:realm withValue:dict];
            
            [realm commitWriteTransaction];
            
            [logObj uploadToService:[logObj getRequestObject]];
        }
    });
    
}

+(void)checkoutAndUploadMethod{

    for (OddityLogObject *logObject in [self objectsInRealm:[RLMRealm OddityRealm] where:@"isUploaded = false"]) {
        
        if (logObject) {
            
            [logObject uploadToService:[logObject getRequestObject]];
        }
    }
}


-(void)uploadToService:(NSURLRequest *)request{
    
    RLMThreadSafeReference *logRef = [RLMThreadSafeReference referenceWithThreadConfined:self];
    
    NSURLSessionDataTask *task =  [[NSURLSession sharedSession] dataTaskWithRequest: request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (response && [response isKindOfClass:[NSHTTPURLResponse class]] && ((NSHTTPURLResponse *)response).statusCode == 200) {
            
            RLMRealm *realm = [RLMRealm OddityRealm];
            
            OddityLogObject *logObj = [realm resolveThreadSafeReference:logRef];
            
            if(logObj.isUploaded) { return; }
            
            if (realm.inWriteTransaction) {
                
                logObj.isUploaded = true;
                return;
            }
            
            [realm beginWriteTransaction];
            
            logObj.isUploaded = true;
            
            [realm commitWriteTransaction];
        }
    }];
    
    [task resume];
}

/**
 根据类型创建一个 URLRequest

 @return 创建一个请求对象
 */
-(NSURLRequest *)getRequestObject{

    NSString *method = @"GET";
    NSString *URLString = @"";
    
    switch ((OddityLogType)self.logType) {
        case OddityLogTypeShow:{
            URLString = @"/rep/v2/log/news/show";
        }
        break;
        case OddityLogTypeNewClick:{
            URLString = @"/rep/v2/log/news/click";
        }
            break;
        case OddityLogTypeNewRead:{
            URLString = @"/rep/v2/log/news/read";
        }
            break;
        case OddityLogTypeAdClick:{
            URLString = @"/rep/v2/log/ad/click";
        }
            break;
        case OddityLogTypeAppUse:{
            URLString = @"/rep/v2/log/app/use";
        }
            break;
        case OddityLogTypeHuangliSlide:{
            URLString = @"/rep/v2/log/huangli/slide";
        }
            break;
        case OddityLogTypeAppAction:{
            URLString = @"/rep/v2/log/app/action";
        }
            break;
        case OddityLogTypeAdGet:{
            URLString = @"/rep/v2/log/ad/get";
        }
            break;
        case OddityLogTypeSignup:{
            URLString = @"/rep/v2/log/users/signup";
        }
            break;
    }
    
//    NSLog(@"请求 %@ ->",URLString);
    
    return [[AFHTTPRequestSerializer serializer] requestWithMethod:method URLString:[NSString stringWithFormat:@"%@%@",LogBaseUrlString,URLString] parameters:@{@"log_data":self.param} error:nil];
}


+(void)createNewShow:(NSArray *)array{
    
    NSMutableArray *params = [NSMutableArray array];
    
    for (id object in array) {
        
        if ([object isKindOfClass:[OddityNew class]]) {
            
            OddityNew *new = (OddityNew *)object;
            
            if (new.cellStyle == PrerryCellStyleOneAds || new.cellStyle == PrerryCellStyleThree || new.cellStyle == PrerryCellStyleBigAds) {
                
                continue;
            }
            
            [params addObject:@{
                                @"nid": @(new.nid),
                                @"source" : @"feed",
                                @"chid" : @(new.channel),
                                @"logtype" : @(new.logtype),
                                @"logchid" : @(new.logchid),
                                @"ctime" : @([[NSDate date] oddity_unixInteger])
                                }];
        }
        
        if ([object isKindOfClass:[OddityNewContentAbout class]]) {
            
            OddityNewContentAbout *about = (OddityNewContentAbout *)object;
            
            if (about.isAdsAbout) {
                
                continue;
            }
            
            [params addObject:@{
                                @"nid": @(about.nid),
                                @"source" : @"relate",
                                @"chid" : @(-1),
                                @"logtype" : @(about.logtype),
                                @"logchid" : @(about.logchid),
                                @"ctime" : @([[NSDate date] oddity_unixInteger])
                                }];
        }
    }
    
    if (!params || params.count <= 0) { // 如果没有数据 就不要上传
        return;
    }
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : @{
                                        @"news_list":params
                                        }
                                };
    
    
    [self createLog:(OddityLogTypeShow) withParam:finalDict];
}
    
+(void)createRelateNewShow:(OddityNewContentAbout *)about channel:(NSInteger)chid{
    
    NSMutableArray *params = [NSMutableArray array];
    
    [params addObject:@{
                        @"nid": @(about.nid),
                        @"source" : @"relate",
                        @"chid" : @(chid),
                        @"logtype" : @(about.logtype),
                        @"logchid" : @(about.logchid),
                        @"ctime" : @([[NSDate date] oddity_unixInteger])
                        }];
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : @{
                                        @"news_list":params
                                        }
                                };
    
    
    [self createLog:(OddityLogTypeShow) withParam:finalDict];
}


+(void)createNewsClick:(int)nid chid:(int)cid logtype:(int)lt logchid:(int)lg source:(NSString *)sou{

    NSDictionary *params = @{
                             @"nid" : @(nid),
                             @"chid" : @(cid),
                             @"source" : sou,
                             @"logtype" : @(lt),
                             @"logchid" : @(lg)
                             };
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : params
                                };
    
    [self createLog:(OddityLogTypeNewClick) withParam:finalDict];
}

+(void)createNewsRead:(NSInteger)nid beginTime:(long long)bt endTime:(long long)et readTime:(long long)rt percent:(float)per{

    NSDictionary *params = @{
                             @"nid" : @(nid),
                             @"begintime" : @(bt),
                             @"endtime" : @(et),
                             @"readtime" : @(rt/1000),
                             @"percent":[NSString stringWithFormat:@"%0.2f%%",per*100]
                             };
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : params
                                };
    
    [self createLog:(OddityLogTypeNewRead) withParam:finalDict];
}



+(void)createHuangLiSlide:(NSDictionary *)params{

    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : params
                                };
    
    
    [self createLog:(OddityLogTypeHuangliSlide) withParam:finalDict];
}

+(void)createAppUse:(long long)bt endTime:(long long)et useTime:(long long)ut{

    if (ut <= 0) {
        return;
    }
    
    NSDictionary *params = @{
                             @"begintime": @(bt),
                             @"endtime" : @(et),
                             @"utime" : @(ut/1000)
                             };
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : params
                                };
    
    [self createLog:(OddityLogTypeAppUse) withParam:finalDict];
}


+(void)createAppAction:(NSString *)atype fromPageName:(NSString *)from toPageName:(NSString *)to effective:(BOOL)effe params:(NSDictionary *)pa{

    NSDictionary *params = @{
                             @"atype": atype,
                             @"from" : from,
                             @"to" : to,
                             @"effective" : @(effe),
                             @"params": pa ?: @{}
                             };
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : params
                                };
    
    [self createLog:(OddityLogTypeAppAction) withParam:finalDict];
}

+(void)createUserSignup{
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString]
                                };
    
    [self createLog:(OddityLogTypeSignup) withParam:finalDict];
}



// ------ 广告相关


+(void)createAdsClick:(id)object{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if ([object isKindOfClass:[OddityNew class]]) {
        
        OddityNew *new = (OddityNew *)object;
        
        [params addEntriesFromDictionary:@{
                                           @"aid": new.advertisementId ?: [OddityNew advertisementId],
                                           @"source" : new.advertisementSourceStr ?: OddityAdvertisementNoneSourceStr,
                                           @"title" : new.pname
                                           }];
    }
    
    if ([object isKindOfClass:[OddityNewContentAbout class]]) {
        
        OddityNewContentAbout *about = (OddityNewContentAbout *)object;
        
        [params addEntriesFromDictionary:@{
                                           @"aid":  about.advertisementId ?:[OddityNewContentAbout advertisementId],
                                           @"source" : about.advertisementSourceStr ?: OddityAdvertisementNoneSourceStr,
                                           @"title" : about.pname
                                           }];
    }
    
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : params
                                };
    
    
    [self createLog:(OddityLogTypeAdClick) withParam:finalDict];
}


+(void)createAdShow:(id)object{
    
    NSMutableArray *params = [NSMutableArray array];
    
    if ([object isKindOfClass:[OddityNew class]]) {
        
        OddityNew *new = (OddityNew *)object;
        
        [params addObject:@{
                            @"aid": new.advertisementId ?:  [OddityNew advertisementId],
                            @"source" : new.advertisementSourceStr ?: OddityAdvertisementNoneSourceStr,
                            @"title" : new.pname,
                            @"ctime" : @([[NSDate date] oddity_unixInteger])
                            }];
    }
    
    if ([object isKindOfClass:[OddityNewContentAbout class]]) {
        
        OddityNewContentAbout *about = (OddityNewContentAbout *)object;
        
        [params addObject:@{
                            @"aid": about.advertisementId ?:  [OddityNewContentAbout advertisementId],
                            @"source" : about.advertisementSourceStr ?: OddityAdvertisementNoneSourceStr,
                            @"title" : about.pname,
                            @"ctime" : @([[NSDate date] oddity_unixInteger])
                            }];
    }

    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : @{
                                        @"news_list": params
                                    }
                                };
    
    [self createLog:(OddityLogTypeShow) withParam:finalDict];
}

+(void)createAdGet:(id)object snum:(int)s rnum:(int)r{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    if ([object isKindOfClass:[OddityNew class]]) {
        
        OddityNew *new = (OddityNew *)object;
        
        [params addEntriesFromDictionary:@{
                                           @"aid": new.advertisementId ?: [OddityNewContentAbout advertisementId],
                                           @"source" : new.advertisementSourceStr ?: OddityAdvertisementNoneSourceStr,
                                           @"rnum" : @(r),
                                           @"snum" : @(s)
                                           }];
    }
    
    if ([object isKindOfClass:[OddityNewContentAbout class]]) {
        
        OddityNewContentAbout *about = (OddityNewContentAbout *)object;
        
        [params addEntriesFromDictionary:@{
                                           @"aid": about.advertisementId ?:[OddityNewContentAbout advertisementId],
                                           @"source" : about.advertisementSourceStr ?: OddityAdvertisementNoneSourceStr,
                                           @"rnum" : @(r),
                                           @"snum" : @(s)
                                           }];
    }
    
    NSDictionary *finalDict = @{
                                @"basicinfo" : [[OddityManagerInfoMationUtil managerInfoMation] getBasicInfoString],
                                @"data" : params
                                };
    
    [self createLog:(OddityLogTypeAdGet) withParam:finalDict];
}

@end
