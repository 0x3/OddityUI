//
//  OddityMainModel.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#ifndef OddityMainModel_h
#define OddityMainModel_h

#import "OdditySearch.h"

#import "OddityLogObject.h"

#import "OddityChannel.h"
#import "OdditySpecial.h"
#import "OddityNewContent.h"

#import "OddityChannelNews.h"
#import "OddityNewsArrayObject.h"

#import "OddityShareUser.h"

#endif /* OddityMainModel_h */

