//
//  OddityNew.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//


#import <Realm/Realm.h>
#import "OddityNewAdsObject.h"

#import "OddityNewContent.h"

@class OddityNewContent;

/**
 *  新闻对象 该App的主要model类
 *  所有的操作都是为了获得这个对象活着操作这个对象而存在。
 *  新闻，也叫消息，是指通过报纸、电台、广播、电视台等媒体途径所传播信息的一种称谓。
 *
 *  我们通过网络请求方式 获取到新闻的 标题 评论 来源 等信息，进行展示给用户的
 */
@interface OddityNew : RLMObject


/**
 *  新闻广告对象
 */
@property OddityNewAdsObject *adsResponse;


/**
 *  新闻的nid 用来唯一标识的新闻。
 */
@property int nid;

/**
 *  新闻的来源地址，一般方面该地址没有什么作用 但是广告的类型 就需要了
 */
//@property NSString* purl;

/**
 *  新闻的来源的头像地址，这个地址可以请求到用户的来源的邮箱
 */
@property NSString* icon;

/**
 *  需要通过这个参数来获取到新闻的评论数据
 */
@property NSString* docid;

/**
 *   新闻的标题，可以让用户 浏览的时候 第一注意的参数
 */
@property NSString* title;

/**
 搜索结果 带有 html 格式的 
 */
@property NSString* searchTitle;
/**
 *  新闻的爬取时间 这个时间其实并不是很准确 但是需要根据这个时间进行排序
 */
@property NSString* ptime;

/**
 *   新闻的来源数据 这个数据有什么用吗？  没有的～  呜哈哈哈 真的就是给别人玩的
 */
@property NSString* pname;

/**
 *  新闻的图片格式 风格
 *  列表图格式，0、1、2、3 视频格式，6、7、8 11:第一张为大图、12:第二张为大图、13:第三张为大图
 */
@property int style;

/**
 *  评论的个数
 */
@property int comment; ///  评论数字

/**
 *  通过评论个数的 生成的 格式化字符串
 */
@property NSString *commentString;

//@property NSString* imgs;
//@property NSString* tags;

/**
 *  新闻的创建时间 NSDate 对象
 */
@property NSDate *ptimes; /// 时间 NSDate 对象

/**
 *  图片类型的 多个图片类型集合
 */
@property RLMArray<OddityStringObject *><OddityStringObject> *imgslist;

/**
 *  新闻的标签对象集合
 */
@property RLMArray<OddityStringObject *><OddityStringObject> *tagslist;
/**
 *  新闻的标签对象集合
 */
@property RLMArray<OddityStringObject *><OddityStringObject> *adsList;

/**
 *  视频 URL
 */
@property NSString* videourl;

/**
 *  视频的缩略图 URL 地址
 */
@property NSString* thumbnail;

/**
 *  新闻的 频道 ID
 */
@property int channel;

/**
 *  该新闻的 关心数
 */
@property int concern;

/**
 *   新闻的收藏数
 */
@property int collect;

/**
 *  视频地址
 */
@property int duration;

/**
 *  新闻  的省
 */
@property NSString* province;

/**
 *   新闻的市
 */
@property NSString* city;

/**
 *   新闻的 区
 */
@property NSString* district;

/**
 *  是否删除 表示新闻是否展示
 */
@property int isdelete;

/**
 *  用户是否阅读完成
 */
@property int isread;


/**
 *  表示该新闻是否为本地创建新闻 用于用户 点击进行数据的刷新
 */
@property int isidentification;

/**
 * 新闻的主要类型
 *  0普通、1热点、2推送、3广告、4专题、5图片新闻、6视频
 */
@property int rtype;


/**
 *  推荐日志类型:比rtype区分更细
 */
@property int logtype;

/**
 *  点击新闻所在频道:区分奇点和其他频道
 */
@property int logchid;




////////////// version 3 新增字段
/**
 *  获取广告的方式
 */
@property int adRequestMode;
/**
 *  使用广点通SDK获取的广告对象
 */
@property NSData *gdtSdkAdObjectData;

/**
 根据主题色 自动 适配的颜色
 */
@property (readonly) NSString *searchFormatTitle;



/**
 处理对象 根据广告来处理该对象

 @param data 数据
 @param refresh 是否刷新数据
 @param index 广告插入的位置
 @return 处理后的数据
 */
+(NSArray *)dealWithDifferentAds:(NSArray *)data isrefresh:(BOOL)refresh insertIndex:(NSInteger)index;

/**
 *  New 的对象方法，进行 新闻 的删除
 */
-(void)toDeleteMethod;

/**
 *  获取广告新闻
 *
 *  @param completed 完成 block
 */
+(void)requestAdsMethod:(void(^)(OddityNew *))completed;

/**
 *  上传用户日志
 *
 *  @param cid      频道ID
 *  @param nContent 新闻详情
 *  @param times    停留时间
 *  @param lc    新闻从那个频道点击进入
 *  @param lt    新闻推荐类型
 */
//+(void)uploadClickLogRequest:(int)cid logtype:(int)lt logchid:(int)lc newContent:(OddityNewContent *)nContent times:(double)times;

/**
 *  每一次都进行一次提交
 */
+(void)firstuploadClickLogRequest;


/**
 *  根据传入的数据完善新闻对象
 *
 *  @param data 需要获取所有属性的对象 数据源
 *
 *  @return 返回处理完成后的新闻对象
 */
-(OddityNew *)analysis:(NSMutableDictionary *)data;


@end
RLM_ARRAY_TYPE(OddityNew) // define RLMArray<Dog>

