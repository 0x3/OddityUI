//
//  OddityNew.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityNew.h"
#import "OddityUtils.h"
#import "OddityRequest.h"
#import "OddityCategorys.h"

#import "OddityShareUser.h"

#import "GDTNativeAd+OddityBlock.h"
@implementation OddityNew


+ (NSString *)primaryKey {
    return @"nid";
}

+ (NSDictionary *)defaultPropertyValues {
    
    return @{
             
             @"isidentification": @0,
             @"isdelete" : @0,
             @"isread" : @0,
             
             @"duration":@0,
             
             @"rtype":@0,
             @"style":@0,
             
             @"comment":@0,
             @"channel":@0,
             
             @"logtype":@0,
             @"logchid":@0,
             
             @"concern":@0,
             @"collect":@0,
             
             @"adRequestMode" : @0
             };
}


-(NSString *)searchFormatTitle{

    NSString *rColorStr = [[OddityUISetting shareOddityUISetting].mainTone oddity_color2String];
    
    NSString *rString = [self.searchTitle oddity_replaceStringRegex:@"'#(\\w+)'" replace:rColorStr];
    
    NSString *fString = [NSString stringWithFormat:@"<span style=\"color:%@\">%@</span>",[[UIColor color_t_2] oddity_color2String],rString];
    
    return fString;
}

/**
 *  New 新闻的 对象方法
 *  设置传递的 新闻对象设置为 已经删除
 */
-(void)toDeleteMethod{
    
    if (self.isdelete == 1) {
        return;
    }
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    [realm beginWriteTransaction];
    
    self.isdelete = 1;
    
    [realm commitWriteTransaction];
}

-(void)toReadedMethod:(RLMNotificationToken *) token{
    
    if (self.isread == 1) {
        return;
    }
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    [realm beginWriteTransaction];
    
    self.isread = 1;
    
    [realm commitWriteTransactionWithoutNotifying:@[token] error:nil];
}




/**
 *  根据传入的数据完善新闻对象
 *
 *  @param data 需要获取所有属性的对象 数据源
 *
 *  @return 返回处理完成后的新闻对象
 */
-(OddityNew *)analysis:(NSMutableDictionary *)data{
    
    self.ptimes = [NSDate oddity_stringToDate:self.ptime ?: @"2017-02-10 10:10:24"];
    
    /**
     *  提取其中的新闻 图片集合 设置进入 新闻的 imgsList中
     */
    [self.imgslist removeAllObjects];
    for (NSString *imgStr in data[@"imgs"] ?: @[]) {
        
        [self.imgslist addObject:[OddityStringObject createOrUpdateInRealm: [RLMRealm OddityRealm] withValue:@{@"value":imgStr}]];
    }
    
    /**
     *  提取数据源中的 tah 数据，设置进入新闻的 tagList
     */
    [self.tagslist removeAllObjects];
    for (NSString *tagStr in data[@"tags"] ?: @[]) {
        
        [self.tagslist addObject:[OddityStringObject createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:@{@"value":tagStr}]];
    }
    
    /**
     *   广告对象
     */
    id respones = data[@"adresponse"];
    if (respones) {
        
        self.adsResponse = [[OddityNewAdsObject createInRealm:[RLMRealm OddityRealm] withValue:respones] analysis:respones];
    }
    
    self.commentString = [NSString stringWithFormat:@"%@%@",[NSString commentFormatString:self.comment],@"评"];
    
    return self;
}




+(void)requestAdsMethod:(void(^)(OddityNew *))completed{
    
    switch (OddityShareUser.defaultUser.getAdsMode) {
        case OddityUserRequestAdsModeGDTApi:{
        
            [self requestAdsMethodGDTApiMode:completed];
        }
            break;
        case OddityUserRequestAdsModeGDTSDK:{
            
            [[GDTNativeAd initGDTNativeAdWithAdMode:(OddityAdvertisementPlacementModeDetailBig)] getAdvertisementWithComplete:^(NSArray<GDTNativeAdData *> *nativeAdDataArray, NSError *error) {
               
                if (nativeAdDataArray) {
                    
                    RLMRealm *realm = [RLMRealm OddityRealm];
                    
                    [realm beginWriteTransaction];
                    
                    id value = [self AdDataToNewDictionary:nativeAdDataArray.firstObject reData:nil];
                    
                    OddityNew * new = [[OddityNew createOrUpdateInRealm:realm withValue:value] analysis:value];
                    
                    [realm commitWriteTransaction];
                    
                    RLMThreadSafeReference *logRef = [RLMThreadSafeReference referenceWithThreadConfined:new];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        RLMRealm *realm = [RLMRealm OddityRealm];
                        
                        OddityNew *logObj = [realm resolveThreadSafeReference:logRef];
                        
                        completed(logObj);
                    });
                }else{
                
                    completed(nil);
                }
            }];
        }
            break;
        default:
            break;
    }
    
}


/**
 获取广告接口 
 
 详情页内的广告获取

 @param completed 结束
 */
+(void)requestAdsMethodGDTApiMode:(void(^)(OddityNew *))completed{

    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSString *placementId = [OddityShareUser userPlacementBy:OddityAdvertisementPlacementModeDetailBig RequestAdsMode:(OddityUserRequestAdsModeGDTApi)];
    /**
     *  生成广告新闻的请求字段
     */
    NSString *base64 = [[OddityManagerInfoMationUtil managerInfoMation] getAdsInfo:placementId];
    
    NSDictionary *dict = @{
                           @"uid":@(uid),
#if oddityIsHttps
                           @"s":@(1),
#else
                           @"s":@(0),
#endif
                           @"b":base64
                           };
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/ns/ad" parameters: dict success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            NSMutableDictionary *data = [(NSDictionary *)[(NSArray *)responseObject[@"data"] firstObject] mutableCopy];
            [data setValue:@(-[[NSDate date] oddity_unixInteger]) forKeyPath:@"nid"];
            
            __block OddityNew * new;
            
            [[RLMRealm OddityRealm] transactionWithBlock:^{
                
                new = [[OddityNew createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:data] analysis:data];
                
                new.adRequestMode = 1;
            }];
            
            if (new && completed) {
                
                completed(new);
            }
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (completed) {
            
            completed(nil);
        }
        
    }] resume];

}



+(void)firstuploadClickLogRequest{
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    NSInteger ctype = OddityShareUser.defaultUser.appCtype;
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    NSString *placementId = [OddityShareUser userPlacementBy:OddityAdvertisementPlacementModeDetailBig RequestAdsMode:(OddityUserRequestAdsModeGDTApi)];
    NSString *base64 = [[OddityManagerInfoMationUtil managerInfoMation] getAdsInfo:placementId];
    
    [dict setObject:@(uid) forKey:@"uid"];
    [dict setObject:@(ctype) forKey:@"ctype"];
    [dict setObject:@(1) forKey:@"ptype"];
    [dict setObject:base64 forKey:@"b"];
    
    if ( [[OddityManagerInfoMationUtil managerInfoMation] city]) {
        
        [dict setObject:[[OddityManagerInfoMationUtil managerInfoMation] city] forKey:@"city"];
    }
    if ( [[OddityManagerInfoMationUtil managerInfoMation] province]) {
        
        [dict setObject:[[OddityManagerInfoMationUtil managerInfoMation] province] forKey:@"province"];
    }
    if ( [[OddityManagerInfoMationUtil managerInfoMation] thoroughfare]) {
        
        [dict setObject:[[OddityManagerInfoMationUtil managerInfoMation] thoroughfare] forKey:@"area"];
    }
    
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[[OddityAFAppDotNetAPIClient sharedClient] POST:@"/v2/au/phone" parameters:dict success:nil failure:nil] resume];
#pragma clang diagnostic pop
}






+(NSArray *)dealWithDifferentAds:(NSArray *)data isrefresh:(BOOL)refresh insertIndex:(NSInteger)index{

    NSMutableArray *justToLookGood = [NSMutableArray array];
    
    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        id nobj = [obj mutableCopy];
        
        switch (OddityShareUser.defaultUser.getAdsMode) {
                
            case OddityUserRequestAdsModeGDTApi:{
            
                if (nobj[@"adresponse"]) {
                    
                    nobj[@"nid"] = @((long)[[NSDate date] timeIntervalSince1970]);
                }
                
                [nobj setObject:@1 forKey:@"adRequestMode"];
            }
                break;
            case OddityUserRequestAdsModeGDTSDK:{
                
                if (idx == index) {
                    
                    dispatch_semaphore_t sema = dispatch_semaphore_create(0);  //创建信号量
                    
                    [[GDTNativeAd initGDTNativeAdWithAdMode:(OddityAdvertisementPlacementModeFeedBig)] getAdvertisementWithComplete:^(NSArray<GDTNativeAdData *> *nativeAdDataArray, NSError *error) {
                       
                        if (nativeAdDataArray) {
                            
                            id ibjt = [self AdDataToNewDictionary:nativeAdDataArray.firstObject reData:nobj];
                            
                            if (refresh) {
                                
                                [justToLookGood insertObject:ibjt atIndex:0];
                            }else{
                                
                                [justToLookGood addObject:ibjt];
                            }
                        }
                        
                        dispatch_semaphore_signal(sema);  //关键点，在此发送信号量
                    }];
                    
                    dispatch_semaphore_wait(sema, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)));  //关键点，在此等待信号量
                }
            }
                break;
            default:
                break;
        }
        
        if (refresh) {
            
            [justToLookGood insertObject:nobj atIndex:0];
        }else{
        
            [justToLookGood addObject:nobj];
        }
    }];
    
    return justToLookGood;
}


+(NSDictionary *)AdDataToNewDictionary:(GDTNativeAdData *)data reData:(id)rdata{
    
    //    *          1. GDTNativeAdDataKeyTitle      标题
    //    *          2. GDTNativeAdDataKeyDesc       描述
    //    *          3. GDTNativeAdDataKeyIconUrl    图标Url
    //    *          4. GDTNativeAdDataKeyImgUrl     大图Url
    //    *          5. GDTNativeAdDataKeyAppRating  应用类广告的星级
    //    *          6. GDTNativeAdDataKeyAppPrice   应用类广告的价格
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    
    
    NSDictionary *dict = @{
      @"nid": @((long)[[NSDate date] timeIntervalSince1970]),
      @"icon":[data.properties objectForKey:GDTNativeAdDataKeyIconUrl],
      @"title" : [data.properties objectForKey:GDTNativeAdDataKeyDesc],
      @"pname" : [data.properties objectForKey:GDTNativeAdDataKeyTitle],
      @"style" : @(11),
      @"rtype" : @(3),
      @"adRequestMode" : @(2),
      @"gdtSdkAdObjectData" : [NSKeyedArchiver archivedDataWithRootObject:data],
      @"imgs" : @[[data.properties objectForKey:GDTNativeAdDataKeyImgUrl]]
      };
    
    [dictionary addEntriesFromDictionary:dict];
    
//    [dictionary setValuesForKeysWithDictionary:dict];
    
    if (rdata) {
        
        [dictionary addEntriesFromDictionary:@{
                                                     @"ptime" : [rdata objectForKey:@"ptime"],
                                                     @"ptimes" : [NSDate oddity_stringToDate:[rdata objectForKey:@"ptime"]],
                                                     }];
    }
    
    return dictionary;
}


@end
