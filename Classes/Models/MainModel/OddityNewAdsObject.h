//
//  OddityNewAdsObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>
#import "OddityNewAdspaceObject.h"

/// 广告数据对象
@interface OddityNewAdsObject : RLMObject

/// 状态
@property int status;
/// 当前版本号
@property double version;

/// 自定义数据
@property NSString *message;

/// 广告位信息集合
@property RLMArray<OddityNewAdspaceObject *><OddityNewAdspaceObject> *adspaceList;

-(OddityNewAdsObject *)analysis:(NSMutableDictionary *)data;

@end
