//
//  OddityNewAdsObject.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCategorys.h"
#import "OddityNewAdsObject.h"

@implementation OddityNewAdsObject

+(NSDictionary *)defaultPropertyValues{
    
    return @{
             @"status":@(0),
             @"version":@(0)
             };
}

-(OddityNewAdsObject *)analysis:(NSMutableDictionary *)data{
    
    [self.adspaceList removeAllObjects];
    for (id obj in [data objectForKey:@"data"][@"adspace"] ?: @[]) {
        
        [self.adspaceList addObject:[[OddityNewAdspaceObject createInRealm:[RLMRealm OddityRealm] withValue:obj] analysis:obj]];
    }
    
    return self;
}

@end
