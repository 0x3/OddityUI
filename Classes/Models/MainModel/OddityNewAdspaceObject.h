//
//  OddityNewAdspaceObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>
#import "OddityNewCreativeObject.h"
#import "OddityNewTrackingObject.h"

@interface OddityNewAdspaceObject : RLMObject

/// 广告位 id。
@property int aid;
// 广告展现形式 1:横幅 2:插屏 3:开屏 4:视频 5:原生
@property int adformat;

/// 创意对象，一次 response 可以包含多个 creative
@property RLMArray<OddityNewCreativeObject *><OddityNewCreativeObject> *creativeList;


-(OddityNewAdspaceObject *)analysis:(NSMutableDictionary *)data;
@end
RLM_ARRAY_TYPE(OddityNewAdspaceObject) // define RLMArray<Dog>
