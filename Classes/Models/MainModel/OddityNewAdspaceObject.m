//
//  OddityNewAdspaceObject.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCategorys.h"
#import "OddityNewAdspaceObject.h"

@implementation OddityNewAdspaceObject

+(NSDictionary *)defaultPropertyValues{
    
    return @{
             @"aid":@(0),
             @"adformat":@(0)
             };
}

-(OddityNewAdspaceObject *)analysis:(NSMutableDictionary *)data{
    
    [self.creativeList removeAllObjects];
    
    for (id obj in [data objectForKey:@"creative"] ?: @[]) {
        
        
        [self.creativeList addObject:[[OddityNewCreativeObject createInRealm:[RLMRealm OddityRealm] withValue:obj] analysis:obj]];
    }
    return self;
}

@end

