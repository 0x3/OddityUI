//
//  OddityNewContent.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OdditySetting.h"


#import "OddityNew.h"
#import "OddityNewContentBody.h"
#import "OddityNewContentAbout.h"
#import "OddityNewContentComment.h"

/**
 *  新闻详情对象
 */
@interface OddityNewContent : RLMObject

/**
 *   新闻的id
 */
@property int nid;

/**
 *  新闻的评论对象
 */
@property NSString * _Nonnull docid;

/**
 *  新闻url
 */
//@property NSString* url;

/**
 *  新闻的标题
 */
@property NSString * _Nonnull title;

/**
 *  新闻创建时间
 */
@property NSString * _Nonnull ptime;

/**
 *  新闻来源地址
 */
@property NSString * _Nonnull pname;

/**
 *  新闻来源地址
 */
//@property NSString* purl;

/**
 *   新闻简介
 */
@property NSString* _Nonnull descr;

/**
 *  评论数目
 */
@property int comment;

@property NSDate* _Nonnull ptimes;

/**
 *  视频新闻 的url
 */
@property NSString* _Nonnull videourl;

/**
 *  视频新闻详情的 缩略图参数
 */
@property NSString* _Nonnull thumbnail;

/**
 *  新闻详情 保存的 评论文稿
 */
@property NSString* _Nonnull commentPresentation;
/**
 * 新闻内容对象集合 详情参照 `NewContentBody`
 */
@property RLMArray<OddityNewContentBody *><OddityNewContentBody> *_Nonnull content;

/**
 *  该新闻详情的全部评论
 */
@property RLMArray<OddityNewContentComment *><OddityNewContentComment> *_Nonnull commits;

/**
 *  该新闻的 热门评论
 */
@property RLMArray<OddityNewContentComment *><OddityNewContentComment> * _Nonnull hotCommits;

/**
 *  新闻详情的相关新闻列表
 */
@property RLMArray<OddityNewContentAbout *><OddityNewContentAbout> *_Nonnull aboutNewList;



//// version to 4.0 新增字段

/// 阅读进度 最小为0 最大为 1
@property int channel;
/// 收藏数目
@property int collect;

/// 关心数目
@property int concern;

/// 是否 已经收藏 1 -> 是 0 -> 否
@property int colFlag;

/// 是否 已经关心 1 -> 是 0 -> 否
@property int conFlag;

/// 是否 已经关心新闻发布源 1 -> 是 0 -> 否
@property int conPubFlag;

/// 阅读进度 最小为0 最大为 1
@property float percent;



/**
 *  根据新闻id 获取新闻的详情对象
 *
 *  @param nid    新闻id
 *  @param iv     是否为视频新闻
 *  @param finish 完成的 block对象
 */
+(void)requestByNid:(int)nid isVideo:(BOOL)iv finishBlock:(nullable void (^)(OddityNewContent * _Nullable task, BOOL isCache))finish failBlock:(void (^_Nullable)())fail;

/**
 *  请求评论数据
 *
 *  @param ishot 是否热门评论
 */
-(void)requestComments:(BOOL) ishot;

/**
 *  请求相关新闻
 */
-(void)requestAbout;



/**
 将新闻 设置为 已经阅读
 */
-(void)toReadedMethod;


/**
 增加用户评论 选择保存 文稿推出 时，保存文稿

 @param pre 文稿字符串对象
 */
-(void)savaPresentation:(NSString *_Nullable)pre;


/**
 用户 滑动时 不断更新 滑动进度
 
 @param per 华东进度
 */
-(void)savaPercentValue:(float)per;

/**
 创建评论对象

 @param content 评论内容
 @param finish 完成评论回调
 */
-(void)createComment:(NSString *_Nullable)content complete:(nullable void (^)(BOOL success,NSString * _Nullable message))finish;


/**
 收藏方法 如果已经收藏 则取消收藏 否则就正常收藏

 @param completed 是否完成
 */
-(void)collect:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;


/**
 取消收藏新闻

 @param contents 新闻id集合
 @param completed 完成回调
 */
+(void)down:(NSArray *_Nullable)contents complete:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;
@end
RLM_ARRAY_TYPE(OddityNewContent)

