//
//  OddityNewContent.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityShareUser.h"

#import "OddityLogObject.h"

#import "OddityRequest.h"
#import "OddityCategorys.h"
#import "OddityNewContent.h"


#import "OddityManagerInfoMationUtil.h"
#import "OddityShareUser.h"
/**
 *  请求 评论的 数据的 时候 返回的评论数据
 *
 *  @return 数据个数
 */
#define commentPageLimit 20


/**
 *  请求相关新闻的 的个数
 *
 *  @return 数据个数
 */
#define aboutsPageLimit 5

#define NewUrl @"http://deeporiginalx.com/news.html?type=0&nid=%d"

@implementation OddityNewContent

/**
 *  主键 设置
 *
 *  @return 主键Syring
 */
+ (NSString *)primaryKey {
    return @"nid";
}

/**
 *  设置默认值防止数据库错误
 *
 *  @return 默认值 字典
 */
+ (NSDictionary *)defaultPropertyValues {
    
    return @{
             @"channel":@(-1),
             @"percent":@0.0,
             @"comment": @0,
             @"collect": @0,
             @"concern": @0,
             @"colflag": @0,
             @"conflag": @0,
             @"conpubflag": @0
             };
}






/**
 *  评论String 的 Base64
 *
 *  @return 返回评论数据的base64
 */
-(NSString *)docidBase64{
    
    NSData *plainData = [self.docid dataUsingEncoding:(NSUTF8StringEncoding)];
    
    if ( plainData ) {
        
        return [plainData base64EncodedStringWithOptions:0];
    }
    return nil;
}



-(OddityNewContent *)analysis:(id) datas{
    
    self.ptimes = [NSDate oddity_stringToDate:self.ptime ?: @"2017-02-10 10:10:24"];
    return self;
}




+(void)requestByNid:(int)nid isVideo:(BOOL)iv finishBlock:(void (^)(OddityNewContent * _Nullable, BOOL))finish failBlock:(void (^)())fail{

    OddityNewContent *newContent = [OddityNewContent objectInRealm:[RLMRealm OddityRealm] forPrimaryKey:@(nid)];
    
    BOOL again = true;
    
    if(newContent){
        again = false;
        finish(newContent,true);
//        return;
    }
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    NSString *hostUrl = iv ? @"/v2/vi/con" : @"/v2/ns/con";
    NSDictionary *parama = @{
                             @"nid":@(nid),
                             @"uid":@(uid),
#if oddityIsHttps
                             @"s":@(1)
#else
                             @"s":@(0)
#endif
                             };
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[[OddityAFAppDotNetAPIClient sharedClient] GET:hostUrl parameters: parama success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            id data = responseObject[@"data"];
            
            __block OddityNewContent *newContent;
            
            RLMRealm *realm = [RLMRealm OddityRealm];
            
            [realm transactionWithBlock:^{
                
                newContent = [[OddityNewContent createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:data] analysis:data];
            }];
            
            if (again) {
                
                finish(newContent,false);
            }
            
        }else{
            
            if (again) {
                
                fail();
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (again) {
            
            fail();
        }
        
    }] resume];
}

/**
 *  根据数据集合 以及 数据 limit 进行页码的获取 。
 *
 *  @param results 数据集合
 *  @param limit   数据limit
 *
 *  @return 页码
 */
-(int)pageFor:(RLMArray *)results pageLimit:(int)limit{
    
    if ( !results ) { return 1; }
    
    int page = (int)((CGFloat)results.count/(CGFloat)limit);
    
    if (page == 0 && results.count > 0) {
        page++;
    }
    
    return page+1;
}

/**
 *  请求新闻的评论对象
 *
 *  @param ishot 是否请求热门评论
 */
-(void)requestComments:(BOOL) ishot{
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    NSString *hostUrl = ishot ? @"/v2/ns/coms/h" : @"/v2/ns/coms/c";
    
    NSDictionary *parama = ishot ? @{
                                     @"did":[self docidBase64],
                                     @"uid":@(uid)
                                     }
    : @{
        @"did":[self docidBase64],
        @"uid":@(uid),
        @"p":@([self pageFor:self.commits pageLimit:commentPageLimit]),
        @"c":@(commentPageLimit)
        };
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[[OddityAFAppDotNetAPIClient sharedClient] GET:hostUrl parameters: parama success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            for (id data in responseObject[@"data"]) {
                
                OddityNewContentComment *comment = [[OddityNewContentComment createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:data] analysis:data];
                
                if( ishot ) {
                    
                    if( [self.hotCommits indexOfObject:comment] == NSNotFound ) {
                        
                        [self.hotCommits addObject:comment];
                    }
                }else{
                    
                    if( [self.commits indexOfObject:comment] == NSNotFound ) {
                        
                        [self.commits addObject:comment];
                    }
                }
            }
            
            [[RLMRealm OddityRealm] commitWriteTransaction];
            
            BOOL nomore = ((NSArray *)responseObject[@"data"]).count < commentPageLimit;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"requestCommentscomplete" object:self userInfo:@{@"nomore": @(nomore) }];
            
        }else{
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"requestCommentscomplete" object:self userInfo:@{@"nomore": @true }];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"requestCommentscomplete" object:self userInfo:@{@"nomore": @true }];
        
    }] resume];
}


/**
 *  请求相关新闻数据
 */
-(void)requestAbout{
    
    NSString *placementId = [OddityShareUser userPlacementBy:(OddityAdvertisementPlacementModeRelevantSmall) RequestAdsMode:(OddityUserRequestAdsModeGDTApi)];
    
    NSString *base64 = [[OddityManagerInfoMationUtil managerInfoMation] getAdsInfo:placementId];
    
    NSDictionary *parama = @{
                             @"nid":@(self.nid),
                             @"b":base64,
                             @"p":@([self pageFor:self.aboutNewList pageLimit:aboutsPageLimit]),
                             @"c":@(aboutsPageLimit),
                             @"ads": @(OddityShareUser.defaultUser.getAdsMode)
                             };
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[[OddityAFAppDotNetAPIClient sharedClient] POST:@"/v2/ns/ascad" parameters: parama success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            NSInteger oldCount = self.aboutNewList.count;
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            OddityNewContentAbout *adAbout;
            
            for (id data in [OddityNewContentAbout dealWithDifferentAds:responseObject[@"data"] old:(int)oldCount]) {
                
                OddityNewContentAbout *about =  [[OddityNewContentAbout createInRealm:[RLMRealm OddityRealm] withValue:data] analysis:data];
                
                if (about.isAdsAbout) {
                    adAbout = about;
                }
                
                [self.aboutNewList addObject:about];
            }
            
            [[RLMRealm OddityRealm] commitWriteTransaction];
            
            /// 打点 -> 获取广告打点
            [OddityLogObject createAdGet:adAbout snum:adAbout ? 1 : 0 rnum:1];
            
            NSInteger newCount = self.aboutNewList.count;
            
            BOOL nomore = (newCount - oldCount) < aboutsPageLimit;
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"requestAboutscomplete" object:self userInfo:@{@"nomore": @(nomore) }];
        }else{
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"requestAboutscomplete" object:self userInfo:@{@"nomore": @true }];
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"requestAboutscomplete" object:self userInfo:@{@"nomore": @true }];
        
    }] resume];
}




-(void)toReadedMethod{
    
    OddityNew *jou = [OddityNew objectInRealm:[RLMRealm OddityRealm] forPrimaryKey:@(self.nid)];
    
    if (jou && jou.isread == 0 && ![[RLMRealm OddityRealm] inWriteTransaction]) {
        
        [[RLMRealm OddityRealm] beginWriteTransaction];
        
        jou.isread = 1;
        
        [[RLMRealm OddityRealm]commitWriteTransaction];
    }
}

-(void)savaPresentation:(NSString *)pre{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
        
        self.commentPresentation = pre;
        
        return;
    }
    
    [[RLMRealm OddityRealm] beginWriteTransaction];
    
    self.commentPresentation = pre;
    
    [[RLMRealm OddityRealm] commitWriteTransaction];
}

-(void)savaPercentValue:(float)per{

    float trper = MIN(1, per);
    trper = MAX(0, trper);
    
    if (trper <= self.percent) {
        
        return;
    }
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
        
        self.percent = trper;
        
        return;
    }
    
    [[RLMRealm OddityRealm] beginWriteTransaction];
    
    self.percent = trper;
    
    [[RLMRealm OddityRealm] commitWriteTransaction];
}

-(void)createComment:(NSString *)content complete:(void (^)(BOOL, NSString * _Nullable))finish{

    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    manager.requestSerializer     = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *parama = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                    @"content":content,
                                                                                    @"commend":@(0),
                                                                                    @"ctime": [NSDate.date oddity_dateToString:@"yyyy-MM-dd HH:mm:ss"],
                                                                                    @"uid" : @(OddityShareUser.defaultUser.uid),
                                                                                    @"uname": OddityShareUser.defaultUser.uname,
                                                                                    @"docid": self.docid
                                                                                    }];
    
    if (OddityShareUser.defaultUser.avator.length > 0 ) {
        
        [parama setValue:OddityShareUser.defaultUser.avator forKey:@"avatar"];
    }
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/ns/coms" parameters: parama success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2000 && responseObject[@"data"]){
            
            [parama setValue:responseObject[@"data"] forKey:@"id"];
            [parama setValue:self.title forKey:@"ntitle"];
            [parama setValue:@(self.videourl ? 6 : 0) forKey:@"rtype"];
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            OddityNewContentComment *comment = [[OddityNewContentComment createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:parama] analysis:parama];
            
            if ([self.commits indexOfObject:comment] == NSNotFound) {
                
                OddityNew *obj = [OddityNew objectInRealm:[RLMRealm OddityRealm] forPrimaryKey:@(self.nid)];
                
                if (obj) { obj.comment = obj.comment + 1; }
                
                self.comment = self.comment + 1;
                
                [self.commits addObject:comment];
            }
            
            [OddityShareUser.defaultUser.comments addObject:comment];
            
            [[RLMRealm OddityRealm] commitWriteTransaction];
            
            finish(true,@"评论成功");
            
            return;
        }
        
        finish(false,@"数据错误");
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        finish(false,@"网络错误");
        
    }] resume];
}





-(void)collect:(void(^)(BOOL success,NSString *mess))completed{
    
    if (self.colFlag == 0) {
        
        [OddityLogObject createAppAction:OddityLogAtypeCollect fromPageName:OddityLogPageDetailPage toPageName:OddityLogPageNullPage effective:true params:@{@"nid":@(self.nid)}];
        
        [self up:completed];
    }else{
        
        [OddityNewContent down:@[@(self.nid)] complete:completed];
    }
}

+(void)down:(NSArray *)contents complete:(void(^)(BOOL success,NSString *mess))completed{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];
    
    NSMutableString *nids = [NSMutableString string];
    
    [contents enumerateObjectsUsingBlock:^(id _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        [nids appendString: [NSString stringWithFormat:idx == contents.count-1 ? @"%@" : @"%@,",obj]];
    }];
    
    NSString *urlParam = [NSString stringWithFormat:@"/v2/ns/cols?uid=%ld&nid=%@",(long)uid,nids];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager DELETE:urlParam parameters: @{} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2003 ){
            
            completed(false,@"取消收藏失败");
            return;
        }
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            for (id nid in contents) {
                
                OddityNewContent *content = [OddityNewContent objectInRealm:RLMRealm.OddityRealm forPrimaryKey:nid];
                
                if (content) {
                    content.colFlag = 0;
                    content.collect = content.collect-1;
                }
                
                OddityNew *obj = [OddityNew objectInRealm:RLMRealm.OddityRealm forPrimaryKey:nid];
                
                if (obj) {
                    
                    NSInteger index = [OddityShareUser.defaultUser.collects indexOfObject:obj];
                    
                    if (index != NSNotFound) {
                        
                        [OddityShareUser.defaultUser.collects removeObjectAtIndex:index];
                    }
                }
            }
            
            
            [[RLMRealm OddityRealm] commitWriteTransaction];
            
            completed(true,@"取消收藏成功");
        }else{
            
            completed(false,@"取消收藏失败");
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completed(false,@"取消收藏失败");
        
    }] resume];
}


-(void)up:(void(^)(BOOL success,NSString *mess))completed{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];
    
    
    NSString *urlParam = [NSString stringWithFormat:@"/v2/ns/cols?uid=%ld&nid=%d",(long)uid,self.nid];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:urlParam parameters: @{} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2003 ){
            
            completed(false,@"收藏失败");
            return;
        }
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            self.colFlag = 1;
            self.collect = [responseObject[@"data"] intValue];
            
            OddityNew *obj = [OddityNew objectInRealm:RLMRealm.OddityRealm forPrimaryKey:@(self.nid)];
            
            if (obj) {
                
                [OddityShareUser.defaultUser.collects addObject:obj];
            }
            
            [[RLMRealm OddityRealm] commitWriteTransaction];
            
            completed(true,@"收藏成功");
        }else{
            
            completed(false,@"收藏失败");
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completed(false,@"收藏失败");
        
    }] resume];
}

@end
