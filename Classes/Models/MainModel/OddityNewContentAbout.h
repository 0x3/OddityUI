//
//  OddityNewContentAbout.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityNewAdsObject.h"
#import <Realm/Realm.h>

/**
 *  新闻的相关新闻对象
 */
@interface OddityNewContentAbout : RLMObject

/**
 *  相关新闻id
 */
@property int nid;

/**
 *  新闻点赞数
 */
@property int rank;

/**
 *  相关新闻的是否阅读
 */
@property int isread;

/**
 *  新闻地址
 */
@property NSString* url;

/**
 *  相关新闻图片地址
 */
@property NSString* img;
/**
 *  新闻 title
 */
@property NSString* title;

/**
 *  新闻来源
 */
@property NSString* from;

/**
 *  来源 名称
 */
@property NSString* pname;

/**
 *  创建时间
 */
@property NSString* ptime;

/**
 *  这个是什么东西呢
 */
@property NSString* abs;

/**
 *  html 格式的 自负转对象
 */
@property NSString* htmlTitle;

/**
 *  创建时间
 */
@property NSDate* ptimes;

/**
 *  推荐日志类型:比rtype区分更细
 */
@property int logtype;

/**
 *  点击新闻所在频道:区分奇点和其他频道
 */
@property int logchid;



////////////// version 3 新增字段



/**
 是不是广告相关新闻
 */
@property BOOL isAdsAbout;

/**
 *  获取广告的方式
 */
@property int adRequestMode;

/**
 *  使用广点通SDK获取的广告对象
 */
@property NSData *gdtSdkAdObjectData;
/**
 *  新闻广告对象
 */
@property OddityNewAdsObject *adsResponse;


/**
 排序 Index
 */
@property int sortIndex;

/**
 视频时间
 */
@property int duration;

/**
 *  About 的对象方法，进行 相关新闻 的已读
 */
-(void)toReadedMethod;


-(OddityNewContentAbout *)analysis:(id) datas;



/**
 处理不同位置的广告放置于 相关新闻

 @param data 数据
 @return 数据源
 */
+(NSArray *)dealWithDifferentAds:(NSArray *)data old:(int)count;

@end
RLM_ARRAY_TYPE(OddityNewContentAbout)

