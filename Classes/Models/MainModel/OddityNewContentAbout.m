//
//  OddityNewContentAbout.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCategorys.h"
#import "OddityNewContentAbout.h"

#import "OddityShareUser.h"
#import "GDTNativeAd+OddityBlock.h"

/**
 * 判断字符串
 */
#define checkUrl @"deeporiginalx.com"

@implementation OddityNewContentAbout

//+ (NSString *)primaryKey {
//    return @"url";
//}


+ (NSDictionary *)defaultPropertyValues {
    
    return @{
             @"isAdsAbout" : @(false),
             
             @"duration":@(0), 
             
             @"sortIndex" : @(0),
             
             @"nid": @0,
             @"rank": @0,
             @"isread": @0,
             @"logtype":@0,
             @"adRequestMode":@0,
             @"logchid":@0
             };
}



-(OddityNewContentAbout *)analysis:(id) datas{
    
    //    NSString *title = self.title;
    
    self.ptimes = [NSDate oddity_stringToDate:self.ptime ?: @"2017-02-10 10:10:24"];
    self.htmlTitle = datas[@"title"];
    self.title = [[self.title stringByReplacingOccurrencesOfString:@"<font color='#0091fa' >" withString:@""] stringByReplacingOccurrencesOfString:@"</font>" withString:@""];
    //    NSError *error;
    //    NSAttributedString *attrStr = [[NSAttributedString alloc]initWithData:[datas[@"title"] dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:&error];
    //    if (!error) {
    //        self.title = attrStr.string;
    //    }
    
    /**
     *   广告对象
     */
    id respones = datas[@"adresponse"];
    if (respones) {
        
        self.adsResponse = [[OddityNewAdsObject createInRealm:[RLMRealm OddityRealm] withValue:respones] analysis:respones];
    }
    
    return self;
}




-(void)toReadedMethod{
    
    if (self.isread == 1) {
        return;
    }
    
    
    RLMThreadSafeReference *logRef = [RLMThreadSafeReference referenceWithThreadConfined:self];
    
    NSString *queueName = [NSString stringWithFormat:@"com.deeporiginalx.gcd.adsupload-%@", [[NSUUID UUID] UUIDString]];
    
    dispatch_queue_t queue = dispatch_queue_create([queueName cStringUsingEncoding:NSASCIIStringEncoding], DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(queue, ^{
        
        @autoreleasepool {
            
            RLMRealm *realm = [RLMRealm OddityRealm];
            
            OddityNewContentAbout *logObj = [realm resolveThreadSafeReference:logRef];
            
            [realm transactionWithBlock:^{
               
                logObj.isread = 1;
            }];
        }
    });
    
    
    [[RLMRealm OddityRealm] beginWriteTransaction];
    
    self.isread = 1;
    
    [[RLMRealm OddityRealm] commitWriteTransaction];
    
}

+(NSArray *)dealWithDifferentAds:(NSArray *)data old:(int)count{
    
    NSMutableArray *justToLookGood = [NSMutableArray array];
    
    [data enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        id nobj = [obj mutableCopy];
        
        switch (OddityShareUser.defaultUser.getAdsMode) {
                
            case OddityUserRequestAdsModeGDTApi:{
                
                if (nobj[@"adresponse"]) {
                    [nobj setObject:@((long)[[NSDate date] timeIntervalSince1970]) forKey:@"nid"];
                    [nobj setObject:@(true) forKey:@"isAdsAbout"];
                    [nobj setObject:@1 forKey:@"adRequestMode"];
                }
            }
                break;
            case OddityUserRequestAdsModeGDTSDK:{
                
                if (idx == OddityShareUser.defaultUser.relatedAdPos) {
                    
                    dispatch_semaphore_t sema = dispatch_semaphore_create(0);  //创建信号量
                    
                    [[GDTNativeAd initGDTNativeAdWithAdMode:(OddityAdvertisementPlacementModeRelevantSmall)] getAdvertisementWithComplete:^(NSArray<GDTNativeAdData *> *nativeAdDataArray, NSError *error) {
                        
                        if (nativeAdDataArray) {
                            
                            id ibjt = [self AdDataToNewDictionary:nativeAdDataArray.firstObject reData:nobj];
                            
                            [justToLookGood addObject:ibjt];
                        }
                        
                        dispatch_semaphore_signal(sema);  //关键点，在此发送信号量
                    }];
                    
//                    DISPATCH_TIME_FOREVER
                    dispatch_semaphore_wait(sema, dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)));  //关键点，在此等待信号量
                }
            }
                break;
            default:
                break;
        }
        
        [nobj setObject:@(count+idx) forKey:@"sortIndex"];
        
        [justToLookGood addObject:nobj];
    }];
    
    return justToLookGood;
}


+(NSDictionary *)AdDataToNewDictionary:(GDTNativeAdData *)data reData:(id)rdata{
    
    //    *          1. GDTNativeAdDataKeyTitle      标题
    //    *          2. GDTNativeAdDataKeyDesc       描述
    //    *          3. GDTNativeAdDataKeyIconUrl    图标Url
    //    *          4. GDTNativeAdDataKeyImgUrl     大图Url
    //    *          5. GDTNativeAdDataKeyAppRating  应用类广告的星级
    //    *          6. GDTNativeAdDataKeyAppPrice   应用类广告的价格
    
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    
    
    NSDictionary *dict = @{
                           @"isAdsAbout": @(true),
                           @"nid": @((long)[[NSDate date] timeIntervalSince1970]),
                           @"url": [[NSUUID UUID] UUIDString],
                           @"img" : [data.properties objectForKey:GDTNativeAdDataKeyImgUrl],
                           @"title" : [data.properties objectForKey:GDTNativeAdDataKeyDesc],
                           @"pname" : [data.properties objectForKey:GDTNativeAdDataKeyTitle],
                           @"adRequestMode" : @(2),
                           @"gdtSdkAdObjectData" : [NSKeyedArchiver archivedDataWithRootObject:data],
                           };
    
    [dictionary addEntriesFromDictionary:dict];
    
    //    [dictionary setValuesForKeysWithDictionary:dict];
    
    if (rdata) {
        
        [dictionary addEntriesFromDictionary:@{
                                               @"ptime" : [rdata objectForKey:@"ptime"],
                                               @"ptimes" : [NSDate oddity_stringToDate:[rdata objectForKey:@"ptime"]],
                                               }];
    }
    
    return dictionary;
}






@end
