//
//  OddityNewContentBody.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>

/**
 *  新闻内容对象
 包含了 新闻的 文字内容 图片内容 以及是剖呢
 */
@interface OddityNewContentBody : RLMObject

/**
 *  新闻内容 文字 内容
 */
@property NSString* txt;

/**
 *  新闻内容 图片内容
 */
@property NSString* img;

/**
 *  新闻内容 视频内容
 */
@property NSString* vid;

@end
RLM_ARRAY_TYPE(OddityNewContentBody)
