//
//  OddityNewContentComment.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>

/**
 *  新闻的评论对象 包含 热门对象以及 普通对象
 */
@interface OddityNewContentComment : RLMObject

/**
 *  新闻评论 id
 */
@property int id;

/**
 *  点赞数
 */
@property int commend;

/**
 *  评论内容
 */
@property NSString* _Nullable content;

/**
 *  创建时间字符串对象
 */
@property NSString* _Nullable ctime;

/**
 *  用户 名称
 */
@property NSString* _Nullable uname;

/**
 *  用户头像
 */
@property NSString* _Nullable avatar;

/**
 *  评论创建时间 NSDate 格式
 */
@property NSDate* _Nullable ctimes;


/// ---- Version 4: 升级评论


/**
 *  推荐日志类型:比rtype区分更细
 */
@property int logtype;

/**
 *  点击新闻所在频道:区分奇点和其他频道
 */
@property int logchid;


/// 创建评论的用户ID
@property int uid;

/// 评论对应新闻的 docid
@property NSString* _Nullable docid;

/// 用户是否能对该条评论点赞，0、1 对应 可点、不可点
@property int upflag;

/// 新闻 id
@property int nid;

/// 原文标题
@property NSString* _Nullable ntitle;

/// 新闻类型
@property int rtype;

-(OddityNewContentComment * _Nullable)analysis:(id _Nullable) datas;



/**
 给某个评论 点赞

 @param tokens 数据库 监听 对象
 @param comp 完成 数据评论的 回调
 */
-(void)vote:(NSArray<RLMNotificationToken *> * _Nullable)tokens complete:(void(^_Nullable)(OddityNewContentComment * _Nullable))comp;

-(void)del:(NSArray<RLMNotificationToken *> *_Nullable)tokens complete:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;
@end
RLM_ARRAY_TYPE(OddityNewContentComment)
