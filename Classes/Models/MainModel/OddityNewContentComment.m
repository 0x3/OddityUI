//
//  OddityNewContentComment.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityShareUser.h"
#import "OddityRequest.h"
#import "OddityCategorys.h"
#import "OddityNewContentComment.h"
#import "OddityLogObject.h"

@implementation OddityNewContentComment

+ (NSString *)primaryKey {
    return @"id";
}


+ (NSDictionary *)defaultPropertyValues {
    
    return @{
             @"logtype":@0,
             @"logchid":@0,
             @"nid":@0,
             @"rtype":@0,
             @"uid" : @0,
             @"commend": @0,
             @"upflag" : @0
             };
}


-(OddityNewContentComment *)analysis:(id) datas{
    
    self.ctimes = [NSDate oddity_stringToDate:self.ctime ?: @"2017-02-10 10:10:24"];
    
    return self;
}


-(void)vote:(NSArray<RLMNotificationToken *> *)tokens complete:(void (^)(OddityNewContentComment * _Nullable))comp{

    if (self.upflag == 0) {
        
        [OddityLogObject createAppAction:OddityLogAtypeLike fromPageName:OddityLogPageMyCommentPage toPageName:OddityLogPageNullPage effective:true params:@{@"commentid":@(self.id)}];
        
        [self up:tokens complete:comp];
    }else{
        
        [OddityLogObject createAppAction:OddityLogAtypeDislike fromPageName:OddityLogPageMyCommentPage toPageName:OddityLogPageNullPage effective:true params:@{@"commentid":@(self.id)}];
        
        [self down:tokens complete:comp];
    }
}

-(void)down:(NSArray<RLMNotificationToken *> *)tokens complete:(void(^)(OddityNewContentComment * _Nullable))comp{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];
    
    
    NSString *urlParam = [NSString stringWithFormat:@"/v2/ns/coms/up?uid=%ld&cid=%d",(long)uid,self.id];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager DELETE:urlParam parameters: @{} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2003 ){
            
            comp(self);
            return;
        }
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            self.upflag = 0;
            self.commend = [responseObject[@"data"] intValue];
            
            [[RLMRealm OddityRealm] commitWriteTransactionWithoutNotifying:tokens error:nil];
            
            comp(self);
        }else{
            
            comp(self);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        comp(self);
        
    }] resume];
}


-(void)up:(NSArray<RLMNotificationToken *> *)tokens complete:(void(^)(OddityNewContentComment * _Nullable))comp{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];

    
    NSString *urlParam = [NSString stringWithFormat:@"/v2/ns/coms/up?uid=%ld&cid=%d",(long)uid,self.id];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:urlParam parameters: @{} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2003 ){
            
            comp(self);
            return;
        }
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            self.upflag = 1;
            self.commend = [responseObject[@"data"] intValue];
            
            [[RLMRealm OddityRealm] commitWriteTransactionWithoutNotifying:tokens error:nil];
            
            comp(self);
        }else{
            
            comp(self);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        comp(self);
        
    }] resume];
}

-(void)del:(NSArray<RLMNotificationToken *> *)tokens complete:(void(^)(BOOL success,NSString *mess))completed{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];
    
    NSString *base64Docid = [[self.docid dataUsingEncoding:(NSUTF8StringEncoding)] base64EncodedStringWithOptions:0];
    
    NSString *urlParam = [NSString stringWithFormat:@"/v2/ns/au/coms?did=%@&cid=%d",base64Docid,self.id];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager DELETE:urlParam parameters: @{} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if( [responseObject[@"code"] integerValue] == 2004 ){
        
            completed(false,@"服务器删除失败");
            
            return;
        }
        
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            [RLMRealm.OddityRealm deleteObject:self];
            
            [[RLMRealm OddityRealm] commitWriteTransactionWithoutNotifying:tokens error:nil];
            
            completed(true,@"删除成功");
        }else{
            
            completed(false,@"删除失败");
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completed(false,@"删除失败");
        
    }] resume];
}


@end
