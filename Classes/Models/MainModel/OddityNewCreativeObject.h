//
//  OddityNewCreativeObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>
#import "OddityStringObject.h"
#import "OddityNewEventObject.h"
#import "OddityNewTrackingObject.h"

@interface OddityNewCreativeObject : RLMObject

/// 印象数 检测代码
@property RLMArray<OddityStringObject *><OddityStringObject> *impressionList;
/// 点击数检测 检测代码
@property RLMArray<OddityStringObject *><OddityStringObject> *clickList;
/// 其他时间检测 检测代码
@property RLMArray<OddityNewEventObject *><OddityNewEventObject> *eventsList;
/// 广告互动 检测代码
@property RLMArray<OddityNewTrackingObject *><OddityNewTrackingObject> *trackingsList;

-(OddityNewCreativeObject *)analysis:(NSMutableDictionary *)data;
@end
RLM_ARRAY_TYPE(OddityNewCreativeObject) // define RLMArray<Dog>
