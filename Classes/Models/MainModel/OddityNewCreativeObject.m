//
//  OddityNewCreativeObject.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCategorys.h"
#import "OddityNewCreativeObject.h"
#import "OddityNewTrackingObject.h"


@implementation OddityNewCreativeObject

-(OddityNewCreativeObject *)analysis:(NSMutableDictionary *)data{
    
    [self.impressionList removeAllObjects];
    for (NSString *imp in data[@"impression"] ?: @[]) {
        
        [self.impressionList addObject:[OddityStringObject createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:@{@"value":imp}]];
    }
    
    [self.clickList removeAllObjects];
    for (NSString *cli in data[@"click"] ?: @[]) {
        
        [self.clickList addObject:[OddityStringObject createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:@{@"value":cli}]];
    }
    
    [self.trackingsList removeAllObjects];
    for (id track in data[@"tracking"] ?: @[]) {
        
        [self.trackingsList addObject:[[OddityNewTrackingObject createInRealm:[RLMRealm OddityRealm] withValue:track] analysis:track]];
    }
    
    [self.eventsList removeAllObjects];
    for (id eve in data[@"event"] ?: @[]) {
        
        [self.eventsList addObject:[OddityNewEventObject createInRealm:[RLMRealm OddityRealm] withValue:eve]];
    }
    
    return self;
}

@end


