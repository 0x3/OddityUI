//
//  OddityNewEventObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>

// 印象 数据 对象
@interface OddityNewEventObject : RLMObject

/// 交互事件类型 1:下载开始 2:下载完成 3:点击安装
@property int event_key;

/// 广告点击互动内容，如跳转连接或下载地址。
@property NSString* event_value;

@end
RLM_ARRAY_TYPE(OddityNewEventObject) // define RLMArray<Dog>
