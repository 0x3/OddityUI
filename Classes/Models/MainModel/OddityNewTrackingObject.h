//
//  OddityNewTrackingObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>
#import "OddityStringObject.h"

// 印象 数据 对象
@interface OddityNewTrackingObject : RLMObject

/// 交互事件类型 1:下载开始 2:下载完成 3:点击安装
@property int tracking_key;

/// 事件监测地址
@property RLMArray<OddityStringObject *><OddityStringObject> *trackingsList;

-(OddityNewTrackingObject *)analysis:(NSMutableDictionary *)data;

@end
RLM_ARRAY_TYPE(OddityNewTrackingObject) // define RLMArray<Dog>
