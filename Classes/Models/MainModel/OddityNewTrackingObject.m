//
//  OddityNewTrackingObject.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCategorys.h"
#import "OddityNewTrackingObject.h"

/// 其他事件检测
@implementation OddityNewTrackingObject

+(NSDictionary *)defaultPropertyValues{
    
    return @{
             @"tracking_key":@(0)
             };
}


-(OddityNewTrackingObject *)analysis:(NSMutableDictionary *)data{
    
    [self.trackingsList removeAllObjects];
    for (NSString *imp in data[@"tracking_value"] ?: @[]) {
        
        [self.trackingsList addObject:[OddityStringObject createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:@{@"value":imp}]];
    }
    
    return self;
}

@end


