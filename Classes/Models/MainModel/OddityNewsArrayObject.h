//
//  OddityBrowseNew.h
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "OddityNew.h"
#import "GDTNativeAd.h"
#import <Realm/Realm.h>
#import "OddityChannel.h"
#import "OddityNewContentAbout.h"
#import "OddityGdtSdkAdObject.h"


/**
 新闻集合对象 比如 新闻的浏览 集合 新闻的 上传集合 新闻的 
 */
@interface OddityNewsArrayObject : RLMObject

@property NSInteger modeInt;

/**
 *  该频道所存储的新闻集合
 */
@property RLMArray<OddityNew *><OddityNew> *tidings;

@property RLMArray<OddityNewContentAbout *><OddityNewContentAbout> *abouts;

@property RLMArray<OddityGdtSdkAdObject *><OddityGdtSdkAdObject> *adsDataArray;

@property RLMArray<OddityChannel *><OddityChannel> *normalChannelArray;
@property RLMArray<OddityChannel *><OddityChannel> *deleteChannelArray;

/**
 检验新闻是否已经上传过了

 @param tiding 新闻对象
 @return 是否上传过 true 已经上传过 false 没有上传过
 */
+(BOOL)checkWhetherToUpload:(OddityNew *)tiding;


/**
 检查相关新闻的广告是否已经上传过

 @param about 相关新闻
 @return 是否上传过
 */
+(BOOL)checkWhetherAboutToUpload:(OddityNewContentAbout *)about;

/**
 判断集合 是不是 没有展示过

 @param array 当前展示的表格视图
 */
+(void)checkWhetherToLogArray:(NSArray<OddityNew *> *)array;


/**
 广点通 广告 入库

 @param array 广点通 广告集合
 */
+(void)StorageGDTSDKAdvObjects:(NSArray *)array;


/**
 广点通 广告 出库

 @return 广点通 原生广告
 */
+(GDTNativeAd *)OutboundGDTSDKAdvObjects;


/**
 频道管理对象

 @return 集合对象
 */
+(OddityNewsArrayObject *)channelManager;

/**
 追加 频道 对象到 数组内

 @param channel 频道对象
 @param normal 是否是普通的数组
 */
-(void)appendChannel:(OddityChannel *)channel isNormal:(BOOL)normal;

/**
 追加 频道 对象到 数组内
 
 @param from 来源
 @param to 前往
 */
-(void)moveFromIndex:(NSInteger)from toIndex:(NSInteger)to;
@end
