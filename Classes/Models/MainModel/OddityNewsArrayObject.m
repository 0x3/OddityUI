//
//  OddityBrowseNew.m
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "OddityCategorys.h"
#import "OddityLogObject.h"
#import "OddityNewBaseTableViewCell.h"
#import "OddityNewsArrayObject.h"

#import "OddityShareUser.h"

/**
 新闻集合类型

 - OddityNewsArrayModeLogArray: 日志上传需要记录 集合
 - OddityNewsArrayModeUploaded: 正常浏览新闻的上传集合对象
 - OddityNewsArrayModeBrowered: 新闻浏览集合
 - OddityNewsArrayModeAboutUpload: 相关新闻光改上传集合
 - OddityNewsArrayModeGdtSdkAds: 广点通sdk 缓存池
 */
typedef NS_OPTIONS(NSUInteger, OddityNewsArrayMode) {
    OddityNewsArrayModeLogArray = 0,
    OddityNewsArrayModeUploaded = 1,
    OddityNewsArrayModeBrowered = 2,
    OddityNewsArrayModeAboutUpload = 3,
    OddityNewsArrayModeGdtSdkAds = 4,
    OddityNewsArrayModeChannelManager = 5,
};


@implementation OddityNewsArrayObject


+(NSString *)primaryKey{

    return @"modeInt";
}


+(OddityNewsArrayObject *)OddityNewArrayMode:(OddityNewsArrayMode)mode{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    OddityNewsArrayObject *object = [OddityNewsArrayObject objectInRealm:realm forPrimaryKey:@(mode)];
    
    if (!object) {
        
        if (realm.inWriteTransaction) {
            
            
            object = [OddityNewsArrayObject createOrUpdateInRealm:realm withValue:@{
                                                                                    @"modeInt":@(mode)
                                                                                    }];
            
            return object;
        }
        
        [realm beginWriteTransaction];
        
        object = [OddityNewsArrayObject createOrUpdateInRealm:realm withValue:@{
                                                                       @"modeInt":@(mode)
                                                                       }];
        
        [realm commitWriteTransaction];
    }
    
    return object;
}


+(BOOL)checkWhetherAboutToUpload:(OddityNewContentAbout *)about{
    
    OddityNewsArrayObject *naObject = [OddityNewsArrayObject OddityNewArrayMode:(OddityNewsArrayModeAboutUpload)];
    
    if ([naObject.abouts indexOfObject:about] == NSNotFound) {
        
        if ([RLMRealm OddityRealm].inWriteTransaction) {
            
            [naObject.abouts addObject:about];
            
            return false;
        }
        
        RLMThreadSafeReference *newRef = [RLMThreadSafeReference referenceWithThreadConfined:about];
        RLMThreadSafeReference *newsRef = [RLMThreadSafeReference referenceWithThreadConfined:naObject];
        
        NSString *queueName = [NSString stringWithFormat:@"com.deeporiginalx.gcd.adsupload-%@", [[NSUUID UUID] UUIDString]];
        
        dispatch_queue_t queue = dispatch_queue_create([queueName cStringUsingEncoding:NSASCIIStringEncoding], DISPATCH_QUEUE_CONCURRENT);
        
        dispatch_async(queue, ^{
            
            @autoreleasepool {
                
                RLMRealm *realm = [RLMRealm OddityRealm];
                
                OddityNewContentAbout *newObi = [realm resolveThreadSafeReference:newRef];
                OddityNewsArrayObject *newsObi = [realm resolveThreadSafeReference:newsRef];
                
                [realm beginWriteTransaction];
                
                [newsObi.abouts addObject:newObi];
                
                [realm commitWriteTransaction];
            }
        });
        
        return false;
    }
    
    return true;
}

+(BOOL)checkWhetherToUpload:(OddityNew *)tiding{

    OddityNewsArrayObject *naObject = [OddityNewsArrayObject OddityNewArrayMode:(OddityNewsArrayModeUploaded)];
    
    if ([naObject.tidings indexOfObject:tiding] == NSNotFound) {
        
        if([RLMRealm OddityRealm].inWriteTransaction){
        
            [naObject.tidings addObject:tiding];
            
            return false;
        }
        
        RLMRealm *realm = [RLMRealm OddityRealm];
        
        [realm beginWriteTransaction];
        
        [naObject.tidings addObject:tiding];
        
        [realm commitWriteTransaction];
        
        return false;
    }
    
    return true;
}

+(void)checkWhetherToLogArray:(NSArray<OddityNew *> *)array{
    
    OddityNewsArrayObject *naObject = [OddityNewsArrayObject OddityNewArrayMode:(OddityNewsArrayModeLogArray)];
    
    for (OddityNew *new in array) {
        
        NSInteger index = [naObject.tidings indexOfObject: new];
        
        if (index != NSNotFound) {
            
            return;
        }
    }
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
        
        [naObject.tidings addObjects:array];
    }else{
        
        [realm beginWriteTransaction];
        
        [naObject.tidings addObjects:array];
        
        [realm commitWriteTransaction];
    }
    
    /// 打点 -> 新闻展示打点
    [OddityLogObject createNewShow:array];
    
    if (OddityShareUser.defaultUser.appCtype == OddityApplicationModeHuangLi) {
        
        // 打点 -> 黄历结算打点 - 滑动操作
        [OddityLogObject createHuangLiSlide:@{@"operate_type": @1}];
    }
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didLookUpAllNewScreen)]) {
        
        [[OdditySetting shareOdditySetting].delegate didLookUpAllNewScreen];
    }
}

+(void)StorageGDTSDKAdvObjects:(NSArray *)array{

    if ([RLMRealm OddityRealm].inWriteTransaction) {
        
        [self Beater_StorageGDTSDKAdvObjects:array];
    }else{
    
        [[RLMRealm OddityRealm] transactionWithBlock:^{
            
            [self Beater_StorageGDTSDKAdvObjects:array];
        }];
    }
}


/**
 StorageGDTSDKAdvObjects 的 打手
 
 用户解耦 正在 写入线程和不在写入线程的两种选择

 @param array 广告对象
 */
+(void)Beater_StorageGDTSDKAdvObjects:(NSArray *)array{

    for (id data in array) {
        
        OddityGdtSdkAdObject *sdkObj = [OddityGdtSdkAdObject createInRealm:([RLMRealm OddityRealm]) withValue:@{
                                                                                                                @"gdtSdkAdObjectData":[NSKeyedArchiver archivedDataWithRootObject:data],
                                                                                                                @"storageTime":[NSDate date],
                                                                                                                @"overdueTime":[[NSDate date] dateByAddingTimeInterval:(60*40)]
                                                                                                                }];
        
        [[self OddityNewArrayMode:(OddityNewsArrayModeGdtSdkAds)].adsDataArray addObject:sdkObj];
    }
}

+(GDTNativeAd *)OutboundGDTSDKAdvObjects{

    OddityNewsArrayObject *object = [self OddityNewArrayMode:(OddityNewsArrayModeGdtSdkAds)];
    
    RLMResults *availableResults = [object.adsDataArray objectsWhere:@"overdueTime > %@",[NSDate date]];
    
    OddityGdtSdkAdObject *results = [availableResults sortedResultsUsingKeyPath:@"overdueTime" ascending:true].firstObject; /// 获取最容易过期的那一个
    
    if (results) {
        
        if ([RLMRealm OddityRealm].inWriteTransaction) {
            
            [object Beater_OutboundGDTSDKAdvObjects:results];
        }else{
            
            [[RLMRealm OddityRealm] transactionWithBlock:^{
                
                [object Beater_OutboundGDTSDKAdvObjects:results];
            }];
        }
        
        return [NSKeyedUnarchiver unarchiveObjectWithData: results.gdtSdkAdObjectData];
    }
    
    return nil;
}

-(void)Beater_OutboundGDTSDKAdvObjects:(OddityGdtSdkAdObject*)results{

    [self.adsDataArray removeObjectAtIndex:[self.adsDataArray indexOfObject:results]];
}


+(OddityNewsArrayObject *)channelManager{

    return [self OddityNewArrayMode:(OddityNewsArrayModeChannelManager)];
}

-(void)appendChannel:(OddityChannel *)channel isNormal:(BOOL)normal{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
        
        channel.isdelete = normal ? 0 : 1;
        [self appendChannelH:channel isNormal:normal];
        
    }else{
    
        [realm transactionWithBlock:^{
            
            channel.isdelete = normal ? 0 : 1;
            [self appendChannelH:channel isNormal:normal];
        }];
    }
}


-(void)appendChannelH:(OddityChannel *)channel isNormal:(BOOL)normal{

    if (normal) {
        
        if ([self.normalChannelArray indexOfObject:channel] == NSNotFound) {
            [self.normalChannelArray addObject:channel];
        }
        if ([self.deleteChannelArray  indexOfObject:channel] != NSNotFound) {
            [self.deleteChannelArray  removeObjectAtIndex:[self.deleteChannelArray  indexOfObject:channel]];
        }
    }else{
        
        if ([self.deleteChannelArray indexOfObject:channel] == NSNotFound) {
            [self.deleteChannelArray addObject:channel];
        }
        if ([self.normalChannelArray  indexOfObject:channel] != NSNotFound) {
            [self.normalChannelArray  removeObjectAtIndex:[self.normalChannelArray  indexOfObject:channel]];
        }
    }
}

-(void)moveFromIndex:(NSInteger)from toIndex:(NSInteger)to{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
        
        [self.normalChannelArray moveObjectAtIndex:from toIndex:to];
        
    }else{
        
        [realm transactionWithBlock:^{
            
            [self.normalChannelArray moveObjectAtIndex:from toIndex:to];
        }];
    }
}

-(void)moveFromIndexH:(NSInteger)from toIndex:(NSInteger)to{

    [self.normalChannelArray moveObjectAtIndex:from toIndex:to];
}

@end
