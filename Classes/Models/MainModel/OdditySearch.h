//
//  OdditySearch.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityNew.h"
#import <Realm/Realm.h>

#define OddityRequestSearchLimit 20



/**
 搜索网络请求接口

 - OddityResponesStateSuccess: 请求成功
 - OddityResponesStateWifiFail: 网络请求失败
 - OddityResponesStateDataFail: 数据请求失败
 - OddityResponesStateNoMore: 无更多数据
 */
typedef NS_ENUM(NSUInteger, OddityResponesState) {
    OddityResponesStateSuccess = 0,
    OddityResponesStateWifiFail = 1,
    OddityResponesStateDataFail = 2,
    OddityResponesStateNoMore = 3
};


@interface OdditySearch : RLMObject

/**
 *  搜索对象的关键字
 */
@property NSString* title;


/**
 state = 0 为热门
 state = 1 为历史记录
 */
@property int state;


/**
 *  搜索对象的关键字
 */
@property NSDate* createDate;

/**
 *  该频道所存储的新闻集合
 */
@property RLMArray<OddityNew *><OddityNew> *pressListArray;





/**
 获取网络上的 热门 搜索日志

 @return 热门搜索日志
 */
+(RLMResults <OdditySearch *>*)hotSearchResults;



/**
 获取本地搜索日志

 @return 本地搜索日志
 */
+(RLMResults <OdditySearch *>*)logSearchResults;


/**
 删除所有的 历史搜索 数据
 */
+(void)removeAllLogSearchResults;

/**
 创建本地日志

 @param title 关键字
 */
+(OdditySearch *)createLogSearch:(NSString *)title;


/**
 搜索关键字的 属性 字符串

 @return 属性字符串
 */
-(NSAttributedString *)searchKeyAttributedString;


/**
 移除所有的新闻集合
 */
-(void)removeAllNewsArray;

/**
 请求热门 热词
 */
+(void)RequestHotWord;

-(void)RequestPage:(NSInteger)page finishBlock:(void (^)(OddityResponesState state))finish;
@end
