//
//  OdditySearch.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityShareUser.h"

#import "OddityLogObject.h"

#import "OdditySearch.h"
#import "OddityRequest.h"
#import "OddityCategorys.h"

#define OddityFontFooter @"</font>"
#define OddityFontHeader @"<font color='#0091fa' >"

@implementation OdditySearch

//+(NSString *)primaryKey{
//
//    return @"title";
//}

+(NSDictionary *)defaultPropertyValues{

    return @{
             @"state" : @0
             };
}


+(void)RequestHotWord{

    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager GET:@"/v2/hot/words" parameters: nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        RLMRealm *realm = [RLMRealm OddityRealm];
        
        [realm beginWriteTransaction];
        
        [realm deleteObjects:[OdditySearch hotSearchResults]];
        
        for (id data in responseObject ?: @[]) {
            
            OdditySearch *search = [OdditySearch createOrUpdateSearchInRealm:realm withValue:data];
            search.createDate = [NSDate date];
        }
        
        [realm commitWriteTransaction];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }] resume];
}

-(void)RequestPage:(NSInteger)page finishBlock:(void (^)(OddityResponesState state))finish{
    
    NSDictionary *params =  @{
                              @"keywords":self.title,
                              @"p":@(page),
                              @"c":@(OddityRequestSearchLimit) ,
                              @"uid": @(OddityShareUser.defaultUser.uid)
                              };
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager GET:@"/v2/ns/es/s" parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            RLMRealm *realm = [RLMRealm OddityRealm];
            
            [realm beginWriteTransaction];
            
            for (id ddata in responseObject[@"data"] ?: @[]) {
                
                OddityNew * oddityObj = [[OddityNew createOrUpdateInRealm:[RLMRealm OddityRealm] withValue: ddata] analysis:ddata];
                
                oddityObj.searchTitle = oddityObj.title;
                
                NSString *title = [oddityObj.title stringByReplacingOccurrencesOfString:OddityFontHeader withString:@""];
                title = [title stringByReplacingOccurrencesOfString:OddityFontFooter withString:@""];
                
                oddityObj.title = title;
                
                if( [self.pressListArray indexOfObject:oddityObj] == NSNotFound ) {
                    
                    [self.pressListArray addObject:oddityObj];
                }
            }
            
            [realm commitWriteTransaction];
            
            finish(OddityResponesStateSuccess);
            
            // 打点 -> 用户动作 点击获取评论
            [OddityLogObject createAppAction: OddityLogAtypeSearch fromPageName:OddityLogPageNullPage toPageName:OddityLogPageSearchPage effective: true params:params];
            
        }else if( [responseObject[@"code"] integerValue] == 2002 ){
            
            finish(OddityResponesStateNoMore);
            // 打点 -> 用户动作 点击获取评论
            [OddityLogObject createAppAction: OddityLogAtypeSearch fromPageName:OddityLogPageNullPage toPageName:OddityLogPageSearchPage effective: false params:params];
        }else{
            
            finish(OddityResponesStateDataFail);
            // 打点 -> 用户动作 点击获取评论
            [OddityLogObject createAppAction: OddityLogAtypeSearch fromPageName:OddityLogPageNullPage toPageName:OddityLogPageSearchPage effective: false params:params];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        finish(OddityResponesStateWifiFail);
        // 打点 -> 用户动作 点击获取评论
        [OddityLogObject createAppAction: OddityLogAtypeSearch fromPageName:OddityLogPageNullPage toPageName:OddityLogPageSearchPage effective: false params:params];
        
    }] resume];
   
}


-(void)removeAllNewsArray{
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    [realm beginWriteTransaction];
    
    [self.pressListArray removeAllObjects];
    
    [realm commitWriteTransaction];
}


/**
 c查询所有在网络上 获取到的 搜索热值

 @return 结果
 */
+(RLMResults <OdditySearch *>*)hotSearchResults{

    return [[OdditySearch objectsInRealm:[RLMRealm OddityRealm] where:@"state == 0"] sortedResultsUsingKeyPath:@"createDate" ascending:false];
}


/**
 查询所有的本地搜索日志

 @return 结果
 */
+(RLMResults<OdditySearch *> *)logSearchResults{
    
    return [[OdditySearch objectsInRealm:[RLMRealm OddityRealm] where:@"state == 1"] sortedResultsUsingKeyPath:@"createDate" ascending:false];
}


/**
 删除所有的网络请求 历史 数据
 */
+(void)removeAllHotSearchResults{
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    [realm beginWriteTransaction];
    
    [realm deleteObjects:[OdditySearch hotSearchResults]];
    
    [realm commitWriteTransaction];
}


/**
 删除所有的 历史搜索 数据
 */
+(void)removeAllLogSearchResults{
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    [realm beginWriteTransaction];
    
    [realm deleteObjects:[OdditySearch logSearchResults]];
    
    [realm commitWriteTransaction];
}


/**
 创建本地请求历史

 @param title 搜索的内容
 */
+(OdditySearch *)createLogSearch:(NSString *)title{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    
    [realm beginWriteTransaction];
    
    OdditySearch *search = [OdditySearch createOrUpdateSearchInRealm:realm withValue:@{
                                                                            @"title": title,
                                                                            @"state" : @(1),
                                                                            @"createDate" : [NSDate date]
                                                                            }];
    
    [realm commitWriteTransaction];
    
    return search;
}


-(NSAttributedString *)searchKeyAttributedString{
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_4],
                             NSForegroundColorAttributeName: [UIColor color_t_2]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.title attributes:attris];
    
    return attriString;
}


+(OdditySearch *)createOrUpdateSearchInRealm:(RLMRealm *)realm withValue:(NSDictionary *)data{

    [NSString stringWithFormat:@"title == %@ AND state == %d",[data objectForKey:@"title"],[[data objectForKey:@"state"] intValue]];
    
    OdditySearch *search = [self objectsInRealm:realm where:@"title == %@ AND state == %d",[data objectForKey:@"title"],[[data objectForKey:@"state"] intValue]].firstObject;
    
    if (search) {
        
        search.createDate = [data objectForKey:@"createDate"];
    }else{
    
        search = [OdditySearch createInRealm:realm withValue:data];
    }
    
    return search;
}

@end
