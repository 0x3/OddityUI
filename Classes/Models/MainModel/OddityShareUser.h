//
//  OddityShareUser.h
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//
#import "OddityNew.h"
#import "OddityNewContentComment.h"
#import <Realm/Realm.h>

#define Oddity_Register_User_start_Notifition_Name_String @"startRegisterUserNotifitionNameString"
#define Oddity_Register_User_fail_Notifition_Name_String @"startRegisterUserFailNotifitionNameString"
#define Oddity_Register_User_success_Notifition_Name_String @"startRegisterUserSuccessNotifitionNameString"

/**
 用户获得广告的方式
 
 - OddityUserRequestAdsModeGDTApi: 广点通API
 - OddityUserRequestAdsModeGDTSDK: 广点通SDK
 - OddityUserRequestAdsModeYiFuSdk: yifu sdk
 */
typedef NS_OPTIONS(NSInteger, OddityUserRequestAdsMode) {
    OddityUserRequestAdsModeGDTApi = 1,
    OddityUserRequestAdsModeGDTSDK = 2,
    OddityUserRequestAdsModeYiFuSdk = 3,
};



/**
 应用的模式
 
 - OddityApplicationModeDefault: 默认的应用 也就是奇点资讯
 - OddityApplicationModeHuangLi: 黄历天气
 */
typedef NS_OPTIONS(NSInteger, OddityApplicationMode) {
    OddityApplicationModeDefault = 1,
    OddityApplicationModeHuangLi = 2
};

/**
 广告位类型
 
 - OddityAdvertisementPlacementModeFeedBig: feed 流 大图
 - OddityAdvertisementPlacementModeFeedSmall: feed流 小图
 - OddityAdvertisementPlacementModeDetailBig: 详情页 大图
 - OddityAdvertisementPlacementModeRelevantSmall: 相关新闻 小图
 */
typedef NS_OPTIONS(NSInteger, OddityAdvertisementPlacementMode) {
    OddityAdvertisementPlacementModeFeedBig = 1,
    OddityAdvertisementPlacementModeFeedSmall = 2,
    OddityAdvertisementPlacementModeDetailBig = 3,
    OddityAdvertisementPlacementModeRelevantSmall = 4
};






@class OddityDateObject;

@interface OddityDateObject : RLMObject

@property NSDate * _Nullable date;

@end
RLM_ARRAY_TYPE(OddityDateObject) // define RLMArray<Dog>




/**
 获取的广告的方式为：
 首先确认获取广告的方式 ： getAdsMode 分为 广点
 
 */
@interface OddityShareUser : RLMObject

// 用户ID
@property  NSInteger primaryKey;

// 用户ID
@property NSInteger uid;

// 用户头像
@property NSString * _Nullable avator;

// 用户名称
@property NSString * _Nullable uname;

// 用户Token
@property NSString * _Nullable token;

// 用户密码
@property NSString * _Nullable password;

// 用户类型
@property NSInteger utype;

////  广告相关 ->>>>>>

@property NSInteger feedAdPos; // 新闻列表的 普通广告 位置
@property NSInteger relatedAdPos; // 相关新闻的 普通广告 位置
@property NSInteger feedVideoAdPos; // 新闻列表 视频广告的 位置
@property NSInteger relatedVideoAdPos; // 相关新闻 视频广告的 位置
@property NSInteger getAdsMode; // 获取广告的模式

// 应用的模式
@property (readonly) OddityApplicationMode appCtype;

@property (readonly) NSURL * _Nullable avatorUrl;

/// 是否调整过 频道顺序
@property BOOL isAdjustmentChannel;

/// 是否显示 用户个人中心
@property (readonly) BOOL isShowUserCenter;
/// 是否显示新闻详情的收藏
@property (readonly) BOOL isShowDetailCollec;

////   日志祥光

// 打点所需字段 来源信息
@property (readonly) NSString * _Nullable appSourceString;
/// --渠道号，1：奇点资讯， 2：黄历天气，3：纹字锁屏，4：猎鹰浏览器，5：白牌 6.纹字主题 7.应用汇
@property (readonly) NSInteger channelChannelId;
// 日活跃上传时间 集合
@property RLMArray<OddityDateObject *><OddityDateObject> * _Nullable updateDates;


/// 个人中心 -> 我的收藏新闻 数据集合
@property RLMArray<OddityNew *><OddityNew> * _Nonnull collects;
/// 个人中心 -> 我的评论 数据集合
@property RLMArray<OddityNewContentComment *><OddityNewContentComment> * _Nonnull comments;


/**
 更新 已经调整过频道
 */
+(void)updateAdjustmentChannel;

/**
 默认的用户

 @return 用户
 */
+(OddityShareUser *_Nullable)defaultUser;


/**
 根据不同类型不同位置获取广告位
 
 @param pMode 不同位置
 @param rmode 不同类型
 @return 广告位
 */
+(NSString *_Nullable)userPlacementBy:(OddityAdvertisementPlacementMode)pMode RequestAdsMode:(OddityUserRequestAdsMode)rmode;


/**
 上传订阅号
 
 @param subs 订阅集合
 */
+(void)subscribe:(NSArray *_Nullable)subs;

/**
 请求 Token
 
 @param finish 完成
 */
+(void)requestToken:(void(^_Nullable)(NSString *_Nullable))finish;


/**
 用户配置 信息
 */
+(void)userConfigurationInformation;

/**
 *  用户日志上船
 *  渠道类型, 1：奇点资讯， 2：黄历天气，3：纹字锁频，4：猎鹰浏览器，5：白牌 6:纹字主题
 *  平台类型，1：IOS，2：安卓，3：网页，4：无法识别
 *  操作类型，1：滑动操作 ，2：广告上船
 */
+(void)adsLocalServerUpload:(int)operate_type;


/**
 本地注销方法

 @param completed 注销完成
 */
-(void)localLogOutMethod:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;

/**
 本地注册方法

 @param uname 用户名称
 @param pass 密码
 @param email 邮件
 @param completed 完成
 */
-(void)localRegisterMethod:(NSString *_Nullable)uname password:(NSString *_Nullable)pass email:(NSString *_Nullable)email complete:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;


/**
 本地登陆方法

 @param email 邮箱字符串
 @param pass 密码
 @param completed 完成回调
 */
-(void)localLogin:(NSString *_Nullable)email password:(NSString *_Nullable)pass complete:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;
/**
 重置密码

 @param email 邮件地址
 @param completed 回调方法
 */
-(void)resetPasswordMethod:(NSString *_Nullable)email complete:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;


/**
 get Comment 刷新评论对象
 */
-(void)gm;


/**
 get collecti 获取收藏新闻
 */
-(void)gc;


/**
 第三方 用户 请求注册

 @param mode 注册的请求类型
 @param suid 第三方的用户的id
 @param stoken 用户的token
 @param sexp 用户的认证的过期的时间
 @param uname 用户的额名称
 @param sex 用户的额性别
 @param ava 用户的头像地址
 @param completed 完成的回调
 */
-(void)thirdPartySign:(OddityShareMode)mode suid:(NSString * _Nonnull)suid stoken:(NSString *_Nonnull)stoken sexpires:(NSString *_Nonnull)sexp username:(NSString *_Nonnull)uname gender:(NSString *_Nonnull)sex avatar:(NSString *_Nonnull)ava complete:(void(^_Nullable)(BOOL success,NSString * _Nullable mess))completed;
@end
