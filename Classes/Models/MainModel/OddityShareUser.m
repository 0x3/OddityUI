//
//  OddityShareUser.m
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import "OddityLogObject.h"
#import "OddityRequest.h"
#import "OddityCategorys.h"
#import "OddityShareUser.h"
#import "OddityManagerInfoMationUtil.h"


#define OddityApplicationOddityAooBundleId @"YU.EveryoneNews"
#define OddityAppliactionHuangLiBundleId @"weather.nd.com.iPhone"

NSString *const SHARE_USER_TOKEN = @"oddityusertoken";
NSString *const SHARE_USER_ID = @"oddityuserid";
NSString *const SHARE_USER_LASTUPLOADDATE = @"oddityuserlastuploaddate";
NSString *const SHARE_USER_PASSWORD = @"oddityuserpassword";


NSString *const SHARE_ADS_GETMODE = @"SHARE_ADS_GETMODE";

NSString *const SHARE_ADS_FEED_AD_POS = @"SHARE_ADS_FEEDADPOS";
NSString *const SHARE_ADS_RELATE_AD_POS = @"SHARE_ADS_RELATEADPOS";

NSString *const SHARE_ADS_FEED_VIDEO_AD_POS = @"SHARE_ADS_FEED_VIDEO_AD_POS";
NSString *const SHARE_ADS_RELATE_VIDEO_AD_POS = @"SHARE_ADS_RELATE_VIDEO_AD_POS";

@implementation OddityDateObject



@end


#define OddityUserDefaultPrimaryKey 19941220

@implementation OddityShareUser

+(NSString *)primaryKey{ return @"primaryKey"; }

+(NSDictionary *)defaultPropertyValues{

    return @{
             
             @"isAdjustmentChannel":@(false),
             @"uname":@"游客",
             
             @"avator":@"",
             @"uid":@(-1),
             @"utype":@(-1),
             @"getAdsMode":@(-1),
             @"feedAdPos": @(-1),
             @"relatedAdPos": @(-1),
             @"feedVideoAdPos": @(-1),
             @"relatedVideoAdPos": @(-1),
             };
}

+(void)updateAdjustmentChannel{
    
    if (OddityShareUser.defaultUser.isAdjustmentChannel) {
        return;
    }
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
    
        OddityShareUser.defaultUser.isAdjustmentChannel = true;
        
        return;
    }
    
    [realm beginWriteTransaction];
    OddityShareUser.defaultUser.isAdjustmentChannel = true;
    [realm commitWriteTransaction];
}


+(OddityShareUser *)defaultUser{

    RLMRealm *realm = [RLMRealm OddityRealm];
    
    OddityShareUser *object = [OddityShareUser objectInRealm:realm forPrimaryKey:@(OddityUserDefaultPrimaryKey)];
    
    if (!object) {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        [dict setValue:@(OddityUserDefaultPrimaryKey) forKey:@"primaryKey"];
        
        [self fill:dict];
        
        if (realm.inWriteTransaction) { return [OddityShareUser createOrUpdateInRealm:realm withValue:dict]; }
        
        [realm beginWriteTransaction];
        object = [OddityShareUser createOrUpdateInRealm:realm withValue:dict];
        [realm commitWriteTransaction];
    }
    
    return object;
}


+(void)fill:(NSMutableDictionary *)dict{
    
    id uid = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_USER_ID];
    
    id token = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_USER_TOKEN];
    id password = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_USER_PASSWORD];
    
    id getAdsMode = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_ADS_GETMODE];
    
    id feedAdPos = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_ADS_FEED_AD_POS];
    id relatedAdPos = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_ADS_RELATE_AD_POS];
    
    id feedVideoAdPos = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_ADS_FEED_VIDEO_AD_POS];
    id relatedVideoAdPos = [[NSUserDefaults standardUserDefaults] objectForKey:SHARE_ADS_RELATE_VIDEO_AD_POS];
    
    if (uid) {  [dict setValue:uid forKey:@"uid"]; }
    
    if (token) { [dict setValue:token forKey:@"token"]; }
    if (password) { [dict setValue:password forKey:@"password"]; }
    
    if (getAdsMode) {  [dict setValue:uid forKey:@"getAdsMode"]; }
    
    if (feedAdPos) {  [dict setValue:uid forKey:@"feedAdPos"]; }
    if (relatedAdPos) {  [dict setValue:uid forKey:@"relatedAdPos"]; }
    
    if (feedVideoAdPos) {  [dict setValue:uid forKey:@"feedVideoAdPos"]; }
    if (relatedVideoAdPos) {  [dict setValue:uid forKey:@"relatedVideoAdPos"]; }
}



+(void)requestToken:(void (^)(NSString *))finish{
    
    NSString *token = OddityShareUser.defaultUser.token;
    
    if (token) {
        finish(token);
        return;
    }
    
    if (!OddityShareUser.defaultUser.uid || !OddityShareUser.defaultUser.password) {
        
        [self sign:finish];
    }else{
        
        [self relogin:finish];
    }
}

-(NSURL *)avatorUrl{

    NSURL *url = [[NSURL alloc] initWithString:self.avator];
    
    return url;
}

-(BOOL)isShowUserCenter{

    switch (self.appCtype) {
        case OddityApplicationModeHuangLi:
            return false;
        default:
            return true;
    }
}

-(BOOL)isShowDetailCollec{

    switch (self.appCtype) {
        case OddityApplicationModeHuangLi:
            return false;
        default:
            return true;
    }
}

-(OddityApplicationMode)appCtype{

    NSString *appNameStr = [[NSBundle mainBundle]bundleIdentifier];

    if ([appNameStr isEqualToString:OddityAppliactionHuangLiBundleId]) {

        return OddityApplicationModeHuangLi;
    }

    return OddityApplicationModeDefault;
}

-(NSInteger)channelChannelId{
    switch (self.appCtype) {
        case OddityApplicationModeDefault:
            return 1;
        case OddityApplicationModeHuangLi:
            return 2;
    }
}

-(NSString *)appSourceString{
    switch (self.appCtype) {
        case OddityApplicationModeDefault:
            return @"qidian";
        case OddityApplicationModeHuangLi:
            return @"huangli";
    }
}

/**
 *  注册用户
 *
 *  @param finish 完成后的 返回的token block
 */
+(void)sign:(void (^)(NSString *token))finish{
    
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    manager.requestSerializer     = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/au/sin/g" parameters:@{@"utype":@2,@"platform":@1} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if (responseObject && [responseObject[@"code"] integerValue] != 2000) {
            
            finish(nil);
            
            return;
        }
        
        NSString *token            = [(NSHTTPURLResponse *)task.response allHeaderFields][@"Authorization"];
        NSString *uid              = [responseObject objectForKey:@"data"][@"uid"];
        NSString *password         = [responseObject objectForKey:@"data"][@"password"];
        NSString *utype            = [responseObject objectForKey:@"data"][@"utype"];
        
        if (!token || !uid.integerValue || !password){
            
            finish(nil);
            
            return;
        }
        
        [RLMRealm.OddityRealm transactionWithBlock:^{
            
            OddityShareUser.defaultUser.token = token;
            OddityShareUser.defaultUser.uid = uid.integerValue;
            OddityShareUser.defaultUser.utype = utype.integerValue;
            OddityShareUser.defaultUser.password = password;
        }];
        
        finish(token);
        
        /// 打点 -> 用户注册
        [OddityLogObject createUserSignup];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        finish(nil);
        
    }] resume];
}

/**
 *   重新登录的方法
 *
 *  @param finish 重新登录完成后的 返回的 token
 */
+(void)relogin:(void (^)(NSString *token))finish{
    
    if(!OddityShareUser.defaultUser.uid || !OddityShareUser.defaultUser.password) {
        
        return;
    }
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    NSString *password = OddityShareUser.defaultUser.password;
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/au/lin/g" parameters:@{@"uid":@(uid),@"password":password} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        NSString *token = [(NSHTTPURLResponse *)task.response allHeaderFields][@"Authorization"];
        
        if (!token ){
            
            finish(nil);
            
            return;
        }
        
        if (RLMRealm.OddityRealm.inWriteTransaction) {
            
            [OddityShareUser defaultUser].token = token;
        }else{
        
            [RLMRealm.OddityRealm transactionWithBlock:^{
                
                [OddityShareUser defaultUser].token = token;
            }];
        }
        
        finish(token);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        finish(nil);
        
    }] resume];
}

+(void)subscribe:(NSArray *)subs{
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    for (NSString *subname in subs) {
        
        NSString *urlStr = [[NSString stringWithFormat:@"%@/v2/ns/pbs/cocs?uid=%ld&pname=%@",AFAppDotNetAPIBaseURLString,(long)uid,subname] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        [[AFHTTPSessionManager manager] POST:urlStr parameters:nil success:nil failure:nil];
#pragma clang diagnostic pop
    }
}

-(void)localRegisterMethod:(NSString *)uname password:(NSString *)pass email:(NSString *)email complete:(void(^)(BOOL success,NSString *mess))completed{

    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    manager.requestSerializer     = [AFJSONRequestSerializer new];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    id params = @{
                  @"utype" : @(1),
                  @"platform" : @(1),
                  @"uid":@(self.uid),
                  @"uname": uname,
                  @"email" : email,
                  @"password": pass
                  };
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/au/sin/l" parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 2003) {
            
            completed(false,@"邮件已存在\n\n忘记密码 可找回密码");
            return;
        }
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {
            
            if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {
                
                NSString *token            = [(NSHTTPURLResponse *)task.response allHeaderFields][@"Authorization"];
                NSString *uid              = [responseObject objectForKey:@"data"][@"uid"];
                NSString *uname         = [responseObject objectForKey:@"data"][@"uname"];
                NSString *utype            = [responseObject objectForKey:@"data"][@"utype"];
                
                [RLMRealm.OddityRealm transactionWithBlock:^{
                    
                    OddityShareUser.defaultUser.token = token;
                    OddityShareUser.defaultUser.uid = uid.integerValue;
                    OddityShareUser.defaultUser.utype = utype.integerValue;
                    OddityShareUser.defaultUser.uname = uname;
                }];
                
                completed(true,@"登陆成功");
                
                return;
            }
            
            completed(true,@"请求成功");
            
            return;
        }
        
        completed(false,@"数据错误");
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completed(false,@"请求失败");
        
    }] resume];
}

-(void)resetPasswordMethod:(NSString *)email complete:(void(^)(BOOL success,NSString *mess))completed{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    manager.requestSerializer     = [AFJSONRequestSerializer new];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    id params = @{
                  @"email" : email
                  };
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/au/lin/r" parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop

        if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {

            completed(true,@"修改密码成功\n登陆邮箱可查看密码");
            
            return;
        }
        
        completed(false,@"数据错误");
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completed(false,@"请求失败");
        
    }] resume];
}


-(void)localLogin:(NSString *)email password:(NSString *)pass complete:(void(^)(BOOL success,NSString *mess))completed{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    manager.requestSerializer     = [AFJSONRequestSerializer new];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    id params = @{
                  @"email" : email,
                  @"password": pass
                  };
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/au/lin/g" parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 4003) {

            completed(false,@"密码错误\n重新输入或者选择重置密码");
            
            return;
        }

        if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {
            
            NSString *token            = [(NSHTTPURLResponse *)task.response allHeaderFields][@"Authorization"];
            NSString *uid              = [responseObject objectForKey:@"data"][@"uid"];
            NSString *uname         = [responseObject objectForKey:@"data"][@"uname"];
            NSString *utype            = [responseObject objectForKey:@"data"][@"utype"];
            
            [RLMRealm.OddityRealm transactionWithBlock:^{
                
                OddityShareUser.defaultUser.token = token;
                OddityShareUser.defaultUser.uid = uid.integerValue;
                OddityShareUser.defaultUser.utype = utype.integerValue;
                OddityShareUser.defaultUser.uname = uname;
            }];
            
            completed(true,@"登陆成功");
            
            return;
        }
        
        completed(false,@"数据错误");
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completed(false,@"请求失败");
        
    }] resume];
}


-(void)localLogOutMethod:(void(^)(BOOL success,NSString *mess))completed{
    
    [RLMRealm.OddityRealm transactionWithBlock:^{
       
        [RLMRealm.OddityRealm deleteObject:self];
    }];
    
    [OddityShareUser sign:^(NSString *token) {
        
        completed(true,@"注销完成");
    }];
}

/**
 进行每日打卡 ->  获取用户的
 */
+(void)userConfigurationInformation{

    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([OddityShareUser.defaultUser needUploadHuangLiDayUserMethod]) {
            
            if (OddityShareUser.defaultUser.appCtype == OddityApplicationModeHuangLi) {
                
                // 打点 -> 黄历结算打点 - 滑动操作
                [OddityLogObject createHuangLiSlide:@{@"operate_type": @3}];
            }
            
            /// 如果当天上传过了 那么就不再上传了
            if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didEnterInSDKAction)] ){
                
                [[OdditySetting shareOdditySetting].delegate didEnterInSDKAction];
            }
        }
    });
    
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    manager.requestSerializer     = [AFJSONRequestSerializer new];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    id params = [[OddityManagerInfoMationUtil managerInfoMation] getAdSourceInfo];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/ad/source" parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {
            
            [RLMRealm.OddityRealm transactionWithBlock:^{
                OddityShareUser.defaultUser.getAdsMode = (OddityUserRequestAdsMode)[[responseObject objectForKey:@"data"] integerValue];
                
                OddityShareUser.defaultUser.feedAdPos = [[responseObject objectForKey:@"feedAdPos"] integerValue];
                OddityShareUser.defaultUser.relatedAdPos = [[responseObject objectForKey:@"relatedAdPos"] integerValue];
                OddityShareUser.defaultUser.feedVideoAdPos = [[responseObject objectForKey:@"feedVideoAdPos"] integerValue];
                OddityShareUser.defaultUser.relatedVideoAdPos = [[responseObject objectForKey:@"relatedVideoAdPos"] integerValue];
            }];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"请求失败");
        
    }] resume];
}

-(BOOL)needUploadHuangLiDayUserMethod{

    NSDate *date = [[NSDate alloc] init];
    NSDateFormatter *todayformatter = [[NSDateFormatter alloc] init];
    todayformatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *today = [todayformatter dateFromString:[NSString stringWithFormat:@"%@-%@-%@ 00:00:00",date.oddityYear,date.oddityMonth,date.oddityDay]];
    NSDate *todayend = [todayformatter dateFromString:[NSString stringWithFormat:@"%@-%@-%@ 23:59:59",date.oddityYear,date.oddityMonth,date.oddityDay]];
    
    NSInteger count = [self.updateDates objectsWhere:@"date BETWEEN %@",@[today,todayend]].count;
    
    [self addTime];
    
    if (count <= 0) {
        
        return true;
    }
    
    return false;
}

-(void)addTime{

    RLMRealm *realm = RLMRealm.OddityRealm;
    
    [RLMRealm.OddityRealm beginWriteTransaction];
    
    OddityDateObject *dateObj = [OddityDateObject createInRealm:realm withValue:@{@"date":[[NSDate alloc] init]}];
    
    [self.updateDates addObject:dateObj];
    
    [RLMRealm.OddityRealm commitWriteTransaction];
}

/**
 *  用户日志上船 - > 老老老老老老上传日志
 *  渠道类型, 1：奇点资讯， 2：黄历天气，3：纹字锁频，4：猎鹰浏览器，5：白牌 6:纹字主题
 *  平台类型，1：IOS，2：安卓，3：网页，4：无法识别
 *  操作类型，1：滑动操作 ，2：广告上传，3：用户dau上传
 */
+(void)adsLocalServerUpload:(int)operate_type{
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    NSInteger ctype = OddityShareUser.defaultUser.appCtype;
    
    NSString *idfa = [[OddityManagerInfoMationUtil managerInfoMation]idfa];
    
    NSString *versionStr = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    
    NSDictionary *dict = @{
                           @"ctype":@(ctype),
                           @"ptype":@"1",
                           @"uid":@(uid),
                           @"mid":idfa,
                           @"version_text":versionStr,
                           @"operate_type":@(operate_type)
                           };
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[OddityAFAppDotNetAPIClient sharedClient] GET:@"/v2/sl/ins" parameters:dict success:nil failure:nil];
#pragma clang diagnostic pop
    
}




-(void)gm{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    [manager.requestSerializer setValue:self.token forHTTPHeaderField:@"Authorization"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager GET:@"/v2/ns/au/coms" parameters:@{@"uid":@(self.uid)} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {
            
            [RLMRealm.OddityRealm transactionWithBlock:^{
               
                [self.comments removeAllObjects];
                
                for (id data in [responseObject objectForKey:@"data"] ?: @[]) {
                    
                    OddityNewContentComment *comment = [[OddityNewContentComment createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:data] analysis:data];
                    
                    [self.comments addObject:comment];
                }
            }];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"请求失败");
        
    }] resume];
}

-(void)gc{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    [manager.requestSerializer setValue:self.token forHTTPHeaderField:@"Authorization"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager GET:@"/v2/ns/au/cols" parameters:@{@"uid":@(self.uid)} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {
            
            [RLMRealm.OddityRealm transactionWithBlock:^{
                
                [self.collects removeAllObjects];
                
                for (id data in [responseObject objectForKey:@"data"] ?: @[]) {
                    
                    OddityNew *new = [[OddityNew createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:data] analysis:data];
                    
                    [self.collects addObject:new];
                }
            }];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"请求失败");
        
    }] resume];
}

-(void)thirdPartySign:(OddityShareMode)mode suid:(NSString *)suid stoken:(NSString *)stoken sexpires:(NSString *)sexp username:(NSString *)uname gender:(NSString *)sex avatar:(NSString *)ava complete:(void(^)(BOOL success,NSString *mess))completed{

    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    manager.requestSerializer     = [AFJSONRequestSerializer new];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    id params = @{
                  @"muid" : @(self.uid),
                  @"utype":@(mode == OddityShareModeSina ? 3 : 4), // 用户类型 本地注册用户:1, 游客用户:2 ,微博三方用户:3 ,微信三方用户:4, 黄历天气:12, 纹字锁频:13, 猎鹰浏览器:14, 白牌:15
                  @"platform" : @(1),
                  @"suid": suid,
                  @"stoken": stoken,
                  @"sexpires" : sexp,
                  @"uname": uname,
                  @"gender":@([sex isEqualToString:@"男"] ? 1 : 0), // 性别 1为男生 0为女生
                  @"avatar":ava
                  };
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/au/sin/s" parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        if ([[responseObject objectForKey:@"code"] integerValue] == 2000) {
            
            NSString *token            = [(NSHTTPURLResponse *)task.response allHeaderFields][@"Authorization"];
            NSString *uid              = [responseObject objectForKey:@"data"][@"uid"];
            NSString *uname         = [responseObject objectForKey:@"data"][@"uname"];
            NSString *utype            = [responseObject objectForKey:@"data"][@"utype"];
            NSString *avatar            = [responseObject objectForKey:@"data"][@"avatar"];
            
            [RLMRealm.OddityRealm transactionWithBlock:^{
                
                OddityShareUser.defaultUser.token = token;
                OddityShareUser.defaultUser.uid = uid.integerValue;
                OddityShareUser.defaultUser.utype = utype.integerValue;
                OddityShareUser.defaultUser.uname = uname;
                OddityShareUser.defaultUser.avator = avatar;
            }];
            
            [NSNotificationCenter.defaultCenter postNotificationName:Oddity_Register_User_success_Notifition_Name_String object:nil];
            
            return;
        }
        
        [NSNotificationCenter.defaultCenter postNotificationName:Oddity_Register_User_fail_Notifition_Name_String object:nil];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [NSNotificationCenter.defaultCenter postNotificationName:Oddity_Register_User_fail_Notifition_Name_String object:nil];
        
    }] resume];
}



+(NSString *)userPlacementBy:(OddityAdvertisementPlacementMode)pMode RequestAdsMode:(OddityUserRequestAdsMode)rmode{
    
    switch (OddityShareUser.defaultUser.appCtype) {
        case OddityApplicationModeDefault:
            return [self OddityAppPlacementBy:pMode RequestAdsMode:rmode];
        case OddityApplicationModeHuangLi:
            return [self HuangLiPlacementBy:pMode RequestAdsMode:rmode];
    }
}



/**
 黄历天气的 广告位 计算方法
 
 @return 广告位
 */
+(NSString *)OddityAppPlacementBy:(OddityAdvertisementPlacementMode)pMode RequestAdsMode:(OddityUserRequestAdsMode)rmode{
    
    switch (rmode) {
        case OddityUserRequestAdsModeGDTApi:{
            
            switch (pMode) {
                case OddityAdvertisementPlacementModeFeedBig:{ return @"463"; } break;
                case OddityAdvertisementPlacementModeFeedSmall:{ return @"424"; }  break;
                case OddityAdvertisementPlacementModeDetailBig:{ return @"472"; } break;
                case OddityAdvertisementPlacementModeRelevantSmall:{ return @"427"; } break;
            }
        }
            break;
        case OddityUserRequestAdsModeGDTSDK:{
            
            switch (pMode) {
                case OddityAdvertisementPlacementModeFeedBig:{ return @"4090922222202235"; } break;
                case OddityAdvertisementPlacementModeFeedSmall:{ return @"8050623262808277"; }  break;
                case OddityAdvertisementPlacementModeDetailBig:{ return @"8030120202507304"; } break;
                case OddityAdvertisementPlacementModeRelevantSmall:{ return @"6000925272404397"; } break;
            }
        }
            break;
        case OddityUserRequestAdsModeYiFuSdk:{
            
            return nil;
        }
            break;
    }
}


/**
 黄历天气的 广告位 计算方法
 
 @return 广告位
 */
+(NSString *)HuangLiPlacementBy:(OddityAdvertisementPlacementMode)pMode RequestAdsMode:(OddityUserRequestAdsMode)rmode{
    
    switch (rmode) {
        case OddityUserRequestAdsModeGDTApi:{
            
            switch (pMode) {
                case OddityAdvertisementPlacementModeFeedBig:{ return @"407"; } break;
                case OddityAdvertisementPlacementModeFeedSmall:{ return @"368"; }  break;
                case OddityAdvertisementPlacementModeDetailBig:{ return @"416"; } break;
                case OddityAdvertisementPlacementModeRelevantSmall:{ return @"371"; } break;
            }
        }
            break;
        case OddityUserRequestAdsModeGDTSDK:{
            
            switch (pMode) {
                case OddityAdvertisementPlacementModeFeedBig:{ return @"1040026202419463"; } break;
                case OddityAdvertisementPlacementModeFeedSmall:{ return @"9060123292919426"; }  break;
                case OddityAdvertisementPlacementModeDetailBig:{ return @"1080325262814449"; } break;
                case OddityAdvertisementPlacementModeRelevantSmall:{ return @"3020328262017593"; } break;
            }
        }
            break;
        case OddityUserRequestAdsModeYiFuSdk:{
            
            return nil;
        }
            break;
    }
}

@end
