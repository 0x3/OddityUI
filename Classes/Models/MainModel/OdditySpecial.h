//
//  OdditySpecial.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>
#import "OdditySpecialClass.h"
#import "OdditySpecialInterface.h"

/**
 *  专题对象 用于展示运营生成的砖头对象
 */
@interface OdditySpecial : RLMObject<OdditySpecialInterface>

/**
 *  专题ID
 */
@property int id;

/**
 *  专题名称
 */
@property NSString* name;

/**
 *  专题封面地址
 */
@property NSString* cover;

/**
 *  专题的简介 字符串
 */
@property NSString* descriptions; /// 专题简介

/**
 *   专题内的 专题 分类的 个数
 */
@property int class_count;

/**
 *  专题内的 新闻 个数
 */
@property int news_count; // 专题包含新闻数

/**
 *  标识专题是否上线的参数
 */
@property int online; // 专题是否上线

/**
 *  专题是否标记为指定
 */
@property int top; // 专题是否置顶

/**
 *  专题的创建时间
 */
@property NSString* create_time; /// 封面ID

/**
 *  专题的创建时间
 */
@property NSDate* create_times; /// 时间 NSDate 对象

/**
 *  专题中 专题分类的 集合
 */
@property RLMArray<OdditySpecialClass *><OdditySpecialClass> *specialClasses; /// tag 列表

/**
 *  请求专题对象通过 专题的ID
 *  如果本地存在数据那么直接返回 如果不存在那么就从网络服务器中请求
 *  @param tid    专题的id
 *  @param finish 完成的block 包含存储到数据中的 专题对象
 */
+(void)RequestSpecialByTid:(NSInteger) tid finishBlock:(void (^)(OdditySpecial *))finish failBlock:(void(^)())fail;


/// 获得分享对象
-(OddityShareObject *)getShareObject;

@end


