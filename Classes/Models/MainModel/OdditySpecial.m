//
//  OdditySpecial.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityShareUser.h"

#import "OdditySetting.h"

#import "OddityRequest.h"
#import "OddityCategorys.h"

#import "OdditySpecial.h"


#define SpecialUrl @"http://deeporiginalx.com/zhuanti-share/index.html?tid=%d"

@implementation OdditySpecial

/**
 *  设置主键
 *
 *  @return 主键String
 */
+ (NSString *)primaryKey {
    return @"id";
}

/**
 *  默认的值 防止数据库报错
 *
 *  @return 默认值字典对象
 */
+ (NSDictionary *)defaultPropertyValues {
    
    return @{
             @"class_count": @0,
             @"news_count" : @0,
             
             @"online":@0,
             @"top":@0
             };
}

/**
 *  请求专题 tid 如果本地游的话 直接返回 否则请求网络结果
 *
 *  @param tid    专题 id
 *  @param finish 完成返回的 block
 */
+(void)RequestSpecialByTid:(NSInteger) tid finishBlock:(void (^)(OdditySpecial *))finish failBlock:(void(^)())fail{
    
    OdditySpecial *special = [OdditySpecial objectInRealm:[RLMRealm OddityRealm] forPrimaryKey:@(tid)];
    
    if ( special ){
        
        finish(special);
        
        [self Request:tid token:OddityShareUser.defaultUser.token finishBlock:nil failBlock: nil];
        return;
    }
    
    [self Request:tid token:OddityShareUser.defaultUser.token finishBlock:finish failBlock: fail];
}


/**
 *  根据用户 提供的 tid 进行服务器请求 专题的 方法
 *
 *  @param tid         专题 id
 *  @param tokenString 用户的 认证 token
 *  @param finish      完成 block
 */
+(void)Request:(NSInteger) tid token:(NSString *)tokenString finishBlock:(void (^)(OdditySpecial *))finish failBlock:(void(^)())fail{
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    [manager.requestSerializer setValue:tokenString forHTTPHeaderField:@"Authorization"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager GET:@"/v2/ns/tdq" parameters: @{ @"tid":@(tid) } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        if( [responseObject[@"code"] integerValue] == 2000 ){
            
            [[RLMRealm OddityRealm] beginWriteTransaction];
            
            id datas = responseObject[@"data"];
            OdditySpecial *special = [[OdditySpecial createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:datas[@"topicBaseInfo"]] analysis:datas];
            
            [[RLMRealm OddityRealm] commitWriteTransaction];
            
            if (finish) {
                
                finish(special);
            }
            
        }else{
            
            if (fail) {
                
                fail();
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (fail) {
            
            fail();
        }
        
    }] resume];
}

/**
 *  完善专题对象
 *
 *  @param datas 数据源
 *
 *  @return 专题对象
 */
-(OdditySpecial *)analysis:(id) datas{
    
    self.create_times = [NSDate oddity_stringToDate:self.create_time ?: @"2017-02-10 10:10:24"];
    
    self.descriptions = datas[@"topicBaseInfo"][@"description"];
    
    for (id data in datas[@"topicClass"] ?: @[]) {
        
        OdditySpecialClass *specialClass = [[OdditySpecialClass createOrUpdateInRealm:[RLMRealm OddityRealm] withValue:data[@"topicClassBaseInfo"]] analysis:data[@"newsFeed"]];
        
        if( [self.specialClasses indexOfObject:specialClass] == NSNotFound ) {
            
            [self.specialClasses addObject:specialClass];
        }
    }
    
    return self;
}


/// 封面IRL
-(NSURL *)coverImageUrl{
    
    if ( self.cover ) {
        
        return [[NSURL alloc]initWithString: [NSString stringWithFormat:@"%@?x-oss-process=image/resize,m_fixed,h_400",self.cover]];
    }
    return nil;
}

/**
 *  简介字符串
 *  根据判断字符串的多少，来判断是否需要现实简介 label 如果需要展示的话  进行空格预留
 *
 *  @return 字符串
 */
-(NSString *)descString {
    
    if (self.descriptions && self.descriptions.length > 0) {
        
        return [NSString stringWithFormat:@"       %@", self.descriptions];
    }
    
    return nil;
}


-(OddityShareObject *)getShareObject{
    
    NSString* shareUrl = [NSString stringWithFormat:SpecialUrl,self.id];
    
    return [[OddityShareObject alloc]initWithTitle:self.name coverImageUrl:self.cover urlString:shareUrl];
}

@end
