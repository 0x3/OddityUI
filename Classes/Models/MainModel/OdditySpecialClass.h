//
//  OdditySpecialClass.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityNew.h"
#import <Realm/Realm.h>

/**
 *  专题分类
 主要是为 主题的 每一个分类
 */
@interface OdditySpecialClass : RLMObject

/**
 *  专题分类的ID
 */
@property int id;

/**
 *  专题分类的标题
 */
@property NSString* name;

/**
 *  所属的专题 ID
 */
@property int topic;

/**
 *  专题分类的 Order 排序
 */
@property int order;

/**
 *  该 专题分类的 下的所有新闻集合
 */
@property RLMArray<OddityNew *><OddityNew> *pressListArray;


/**
 *  完善 专题分类对象
 *
 *  @param datas 数据源
 *
 *  @return 专题分类对象
 */
-(OdditySpecialClass *)analysis:(id) datas;

@end
RLM_ARRAY_TYPE(OdditySpecialClass)
