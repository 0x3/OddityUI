//
//  OdditySpecialClass.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "RLMRealm+Oddity.h"
#import "OddityNew.h"
#import "OdditySpecialClass.h"

@implementation OdditySpecialClass

/**
 *  设置主键
 *
 *  @return 主键String
 */
+ (NSString *)primaryKey {
    return @"id";
}

/**
 *  设置默认值 防止数据库报错
 *
 *  @return 默认字典对象
 */
+ (NSDictionary *)defaultPropertyValues {
    
    return @{
             @"topic": @0,
             @"order" : @0
             };
}

/**
 *  完善 专题分类对象
 *
 *  @param datas 数据源
 *
 *  @return 专题分类对象
 */
-(OdditySpecialClass *)analysis:(id) datas{
    
    for (id data in datas ?: @[]) {
        
        OddityNew * new = [[OddityNew createOrUpdateInRealm:[RLMRealm OddityRealm] withValue: data] analysis:data];
        
        if( [self.pressListArray indexOfObject:new] == NSNotFound ) {
            
            [self.pressListArray addObject:new];
        }
    }
    
    return self;
}

@end
