//
//  OddityChannel+Interface.m
//  Pods
//
//  Created by 荆文征 on 2017/6/16.
//
//
#import "OddityNewsArrayObject.h"
#import "OddityCategorys.h"
#import "OddityChannel+Interface.h"

@implementation OddityChannel (Interface)

-(NSAttributedString *)cNameAttributedString{
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_6],
                             //                             NSForegroundColorAttributeName: [UIColor oddity_color3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.cname attributes:attris];
    
    return attriString;
}

-(CGSize)cNameAttributedStringNeedSize{
    
    NSStringDrawingOptions opts = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    CGSize size = [self.cNameAttributedString boundingRectWithSize:(CGSizeMake(CGFLOAT_MAX, 22)) options:opts context:nil].size;
    
    return CGSizeMake(size.width+20, 22);
}

/**
 *  获取 频道的 颜色 如果为 奇点 频道 颜色为不用颜色
 *
 *  @return 颜色
 */
-(UIColor *)getTitleColor{
    if ( self.id == 1 ) { return [UIColor color_t_3]; }
    return [UIColor color_t_2];
}

/// 获得管理图片
-(UIImage *)getManagerImage{
    
    if ( [[OddityNewsArrayObject channelManager].normalChannelArray indexOfObject:self] == NSNotFound ) {
        
        return [UIImage oddity_channel_insert_image];
    }
    return [UIImage oddity_channel_delete_image];
}

/// 获得管理图片是否显示
-(BOOL)getManagerImageHidden{
    return self.id == 1;
}

/// 获得背景颜色
-(UIColor *)getBackColor{
    if ( self.id == 1 ) { return [UIColor color_t_6]; }
    return [UIColor color_t_9];
}

@end
