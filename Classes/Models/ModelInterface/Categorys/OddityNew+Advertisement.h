//
//  OddityNew+Advertisement.h
//  Pods
//
//  Created by 荆文征 on 2017/5/3.
//
//

#import "OddityNew.h"
#import "GDTNativeAd+OddityBlock.h"

@interface OddityNew (Advertisement)



-(void)ExposureAdvertisement:(UITableViewCell *)view nativeAD:(GDTNativeAd *)ad;
-(void)ClickAdvertising:(UITableView *)tableView indexPath:(NSIndexPath *)index nativeAD:(GDTNativeAd *)ad viewController:(UIViewController *)viewController;

/**
 *  上传 互动
 *
 *  @param clickId 点击时间
 */
-(void)uploadTrackArrayList:(NSString *)clickId;


/**
 *  获取点击 广告 id 和 下载id
 *
 *  @param urlStr 新闻url
 *  @param finish 成功
 *  @param fail   失败
 */
+(void)getClickIdAndDstLink:(NSString *)urlStr finishBlock:(void (^)(NSString *clickid,NSString *dstlink))finish failBlock:(void(^)())fail;

@end
