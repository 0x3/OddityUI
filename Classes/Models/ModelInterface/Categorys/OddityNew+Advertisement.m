//
//  OddityNew+Advertisement.m
//  Pods
//
//  Created by 荆文征 on 2017/5/3.
//
//

#import "OddityLogObject.h"

#import "OddityRequest.h"
#import "OddityCategorys.h"
#import "OddityUtilModels.h"
#import "OddityNew+Advertisement.h"
#import "OddityManagerInfoMationUtil.h"

#import "OddityNewsArrayObject.h"
#import "OddityNewAdsDetailViewController.h"

@implementation OddityNew (Advertisement)

-(void)ExposureAdvertisement:(UITableViewCell *)view nativeAD:(GDTNativeAd *)ad{
    
    /// 打点 -> 展示广告打点
    [OddityLogObject createAdShow:self];
    
    RLMRealm *realm = [RLMRealm OddityRealm];
    
    if (realm.inWriteTransaction) {
        
        OddityNew *newObi = self;
        
        switch (newObi.adRequestMode) {
            case OddityUserRequestAdsModeGDTApi:{
                
                [newObi uploadTrueAdsUrl];
            }
                break;
            case OddityUserRequestAdsModeGDTSDK:{
                
                GDTNativeAdData *unarchivePerson = [NSKeyedUnarchiver unarchiveObjectWithData: newObi.gdtSdkAdObjectData];
                
                [ad attachAd:unarchivePerson toView:view];
            }
                break;
            default:
                break;
        }
        
        return;
    }
    
    RLMThreadSafeReference *newRef = [RLMThreadSafeReference referenceWithThreadConfined:self];
    
    NSString *queueName = [NSString stringWithFormat:@"com.deeporiginalx.gcd.adsupload-%@", [[NSUUID UUID] UUIDString]];
    
    dispatch_queue_t queue = dispatch_queue_create([queueName cStringUsingEncoding:NSASCIIStringEncoding], DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(queue, ^{
        
        @autoreleasepool {
            
            OddityNew *newObi = [[RLMRealm OddityRealm] resolveThreadSafeReference:newRef];
            
            switch (newObi.adRequestMode) {
                case OddityUserRequestAdsModeGDTApi:{
                    
                    [newObi uploadTrueAdsUrl];
                }
                    break;
                case OddityUserRequestAdsModeGDTSDK:{
                    
                    GDTNativeAdData *unarchivePerson = [NSKeyedUnarchiver unarchiveObjectWithData: newObi.gdtSdkAdObjectData];
                    
                    [ad attachAd:unarchivePerson toView:view];
                }
                    break;
                default:
                    break;
            }
        }
    });
}

-(void)ClickAdvertising:(UITableView *)tableView indexPath:(NSIndexPath *)index nativeAD:(GDTNativeAd *)ad viewController:(UIViewController *)viewController{

    OddityNewBaseTableViewCell *cell = ((OddityNewBaseTableViewCell *) [tableView cellForRowAtIndexPath:index]);
    
    
    switch (self.adRequestMode) {
        case OddityUserRequestAdsModeGDTApi:{
            
            UIWindow *currentWindow = [UIApplication sharedApplication].keyWindow;
            
            /// 1. 获取广告点击开始和结束的位置
            CGPoint beginPoint = CGPointZero;
            CGPoint endPoint = CGPointZero;
            
            
            if (cell) {
                beginPoint = [cell convertPoint:cell.beginPoint toView: currentWindow];
                endPoint = [cell convertPoint:cell.endPoint toView: currentWindow];
            }
            
            /// 上传点击数组的数据
            [self uploadClickArrayList];
            
            /// 根据点击的 位置 进行 实际广告的地址的请求 和确认广告需要跳转还是进行网页的展示
            [self getEventUrlByClickPoint:beginPoint endPoint:endPoint finishBlock:^(NSString *url, BOOL toDo) {
                
                OddityAdsNewDetailsViewController *adsVC;
                
                if (toDo) {
                    
                    adsVC = [[OddityAdsNewDetailsViewController alloc] initWithAdsUrlString:url];
                } else {
                    
                    adsVC = [[OddityAdsNewDetailsViewController alloc] initWithReqAdsUrlString:url aboutNew:self];
                }
                
                [viewController presentViewController:adsVC animated:true completion:^{
                    
                    [tableView deselectRowAtIndexPath:index animated:YES];// 取消选中
                }];
            }];
        }
            break;
        case OddityUserRequestAdsModeGDTSDK:{
            
            GDTNativeAdData *unarchivePerson = [NSKeyedUnarchiver unarchiveObjectWithData: self.gdtSdkAdObjectData];
            
            ad.controller = viewController;
            
            [ad attachAd:unarchivePerson toView:cell];
            
            [ad clickAd:unarchivePerson];
            
            [tableView deselectRowAtIndexPath:index animated:YES];// 取消选中
        }
            break;
        default:
            break;
    }
}


-(void)uploadTrueAdsUrl{

    OddityNewCreativeObject *create = [self getTrueAdsObjects];
    
    for (OddityStringObject *stringObj in create.impressionList ?: @[]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        
        [[OddityAFAppDotNetAPIClient sharedClient] GET:stringObj.value parameters:nil success:nil failure:nil];
#pragma clang diagnostic pop
    }
}



/**
 *  根据电击位置 获取 需要进行操作的惦记链接
 *
 *  @param pointa 按下 位置
 *  @param pointb 弹起 位置
 *
 */
-(void)getEventUrlByClickPoint:(CGPoint)pointa endPoint:(CGPoint)pointb finishBlock:(void (^)(NSString *url,BOOL toDo))finish{
    
    OddityNewEventObject *event = [self getTrueAdsObjects].eventsList.firstObject;
    
    if (!event) {
        
        finish(@"",true);
        
        return;
    }
    
    NSDictionary *dict = @{
                           @"down_x":@((int)pointa.x),
                           @"down_y":@((int)pointa.y),
                           @"up_x":@((int)pointb.x),
                           @"up_y":@((int)pointb.y)
                           };
    
    
    NSMutableString *handleStrUrlString = [NSMutableString string];
    
    [handleStrUrlString appendString:[event.event_value oddity_replaceStringRegex:@"&(s=.+)" replace:[NSString stringWithFormat:@"&s=%@",[dict oddity_dataToJsonString]]]];
    
    NSString *lat = [[OddityManagerInfoMationUtil managerInfoMation] latitude];
    NSString *lon = [[OddityManagerInfoMationUtil managerInfoMation] longitude];
    
    if (lat && lon) {
        
        [handleStrUrlString appendString:[NSString stringWithFormat:@"&lat=%@&lon=%@",lat,lon]];
    }
    
    if (event.event_key == 1) {
        
        finish(handleStrUrlString,true);
        
        return;
    }
    
    if (event.event_key == 2) {
        
        handleStrUrlString = [[handleStrUrlString oddity_replaceStringRegex:@"&acttype=(\\d?)&" replace:@"&acttype=1&"] mutableCopy];
        
        finish(handleStrUrlString,false);
        
        return;
    }
    
    finish(@"",true);
}

/**
 *   上船 click list 数据
 */
-(void)uploadTrackArrayList:(NSString *)clickId{
    
    OddityNewCreativeObject *create = [self getTrueAdsObjects];
    
    OddityStringObject *trackingObject =  [create.trackingsList objectsWhere:@"tracking_key == 1"].firstObject.trackingsList.firstObject;
    
    if (trackingObject) {
        
        NSString *trackingString = [trackingObject.value oddity_replaceStringRegex:@"%%CLICKID%%" replace:clickId];
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        [[OddityAFAppDotNetAPIClient sharedClient] GET:trackingString parameters:nil success:nil failure:nil];
#pragma clang diagnostic pop
    }
}



/**
 *   上船 click list 数据
 */
-(void)uploadClickArrayList{
    
    OddityNewCreativeObject *create = [self getTrueAdsObjects];
    
    for (OddityStringObject *stringObj in create.clickList ?: @[]) {
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        
        [[OddityAFAppDotNetAPIClient sharedClient] GET:stringObj.value parameters:nil success:nil failure:nil];
#pragma clang diagnostic pop
    }
}



/**
 *  获取点击 广告 id 和 下载id
 *
 *  @param urlStr 新闻url
 *  @param finish 成功
 *  @param fail   失败
 */
+(void)getClickIdAndDstLink:(NSString *)urlStr finishBlock:(void (^)(NSString *clickid,NSString *dstlink))finish failBlock:(void(^)())fail{
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[OddityAFAppDotNetAPIClient sharedDefaultClient] GET:urlStr parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        NSString *clickId = responseObject[@"data"][@"clickid"];
        NSString *dstlink = responseObject[@"data"][@"dstlink"];
        
        if (clickId && dstlink) {
            
            finish(clickId,dstlink);
        }else{
            
            fail();
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        fail();
    }];
}

/**
 *  获得真正的广告对象
 *
 *  @return 广告对象
 */
-(OddityNewCreativeObject *)getTrueAdsObjects{
    
    return self.adsResponse.adspaceList.firstObject.creativeList.firstObject;
}

@end
