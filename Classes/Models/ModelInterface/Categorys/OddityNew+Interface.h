//
//  OddityNew+Interface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "OddityNew.h"
#import "OddityNewInterface.h"
#import "OddityShareInterface.h"

@interface OddityNew (Interface) <OddityNewInterface,OddityShareInterface>

@end
