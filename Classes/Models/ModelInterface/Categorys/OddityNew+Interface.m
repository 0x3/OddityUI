//
//  OddityNew+Interface.m
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "NSString+Oddity.h"

#import "OddityLogObject.h"

#import "OddityShareUser.h"
#import "UIFont+Theme.h"
#import "UIColor+Theme.h"
#import <UIKit/UIKit.h>

#import "OddityNew+Interface.h"

@implementation OddityNew (Interface)

-(NSString *)advertisementId{
    
    if (self.rtype != 3) {
        
        return @"-1";
    }
    
    OddityAdvertisementPlacementMode PlacementMode = self.cellStyle == PrerryCellStyleOneAds ? OddityAdvertisementPlacementModeFeedSmall : OddityAdvertisementPlacementModeFeedBig;
    
    return [OddityShareUser userPlacementBy:(PlacementMode) RequestAdsMode:self.adRequestMode];
}

-(NSString *)advertisementSourceStr{

    switch ((OddityUserRequestAdsMode)self.adRequestMode) {
        case OddityUserRequestAdsModeGDTApi:
            return OddityAdvertisementGDTAPISourceStr;
        case OddityUserRequestAdsModeGDTSDK:
            return OddityAdvertisementGDTSDKSourceStr;
        case OddityUserRequestAdsModeYiFuSdk:
            return OddityAdvertisementYifuSDKSourceStr;
    }
}

+(NSString *)advertisementId{
    
    return [OddityShareUser userPlacementBy:(OddityAdvertisementPlacementModeFeedSmall) RequestAdsMode: OddityShareUser.defaultUser.getAdsMode];
}

/**
 *  是否隐藏标签视图 当标签部位0的时候都是需要现实标签的
 *
 *  @return 返回是否需要现实标签视图
 */
-(BOOL)isHiddenTagView { return (self.rtype) == 0; }

/**
 *  获取标签对象 包括标签对象的文字内容 文字颜色以及边框颜色
 *
 *  @return 标签对象
 */
-(TagObject *)gTagObject{
    TagObject * tObject= [[TagObject alloc] init];
    switch (self.rtype) {
        case 1:
            tObject.textString = @"热点";
            tObject.borderAndTextColor = [UIColor color_t_7];
            break;
        case 2:
            tObject.textString = @"推送";
            tObject.borderAndTextColor = [UIColor color_t_15];
            break;
        case 3:
            tObject.textString = @"广告";
            tObject.borderAndTextColor = [UIColor color_t_3];
            break;
        case 4:
            tObject.textString = @"专题";
            tObject.borderAndTextColor = [UIColor color_t_8];
            break;
        case 6:
            tObject.textString = @"视频";
            tObject.borderAndTextColor = [UIColor color_t_8];
            break;
        default:
            tObject.textString = @"神奇";
            tObject.borderAndTextColor = [UIColor redColor];
            break;
    }
    
    return tObject;
}

/**
 *  是否隐藏评论 lable 党评论树木大于0 时 就需要现实评论 label
 *
 *  @return 是否现实 评论 label
 */
-(BOOL)isHiddenComment{
    
    return self.comment <= 0;
}

//let strs = self.comment/10000 > 0 ? self.comment%10000 < 1000 ? "\(Int(self.comment/10000))万" : "\(String(format: "%.1f", CGFloat(Int(self.comment/10000))+CGFloat(CGFloat(self.comment%10000)/10000)))万" : "\(self.comment)"

/**
 *  格式化评论的数据 如果超过1W的话 那么就进行数据的格式化
 *
 *  @return 返回格式化户的评论label 现实的字符串
 */
-(NSString *)commentFormatString{
    
    NSString *result = nil;
    
    if (self.comment < 10000) {
        result = [NSString stringWithFormat:@"%d", self.comment];
        
    }else if(self.comment > 10000){
        
        double doubleNum = self.comment /10000.0;
        result = [NSString stringWithFormat:@"%.1f万", doubleNum];
    }
    
    return [NSString stringWithFormat:@"%@评",result];
}

/**
 *  根据新闻对象各种属性完成 cellSyule的 计算 以及判断。
 *
 *  @return 返回后的cellsyule 用来 展示不用样式的 cell
 */
-(OddityPrerryCellStyle)cellStyle{
    
    if(self.isidentification == 1) {
        
        return PrerryCellStyleRefresh;
    }
    
    if (self.rtype == 3){
        if (self.style == 1 || self.style == 2) {
            return PrerryCellStyleOneAds;
        }
        
        if (self.style == 3) {
            return PrerryCellStyleThreeAds;
        }
        
        return PrerryCellStyleBigAds;
    }
    
    if (self.rtype == 4 ) {
        
        return PrerryCellStyleSpecial;
    }
    
    switch (self.style) {
        case 1:
            return PrerryCellStyleOne;
        case 2:
            return PrerryCellStyleOne;
        case 3:
            return PrerryCellStyleThree;
        case 6: case 7: case 8:
            return PrerryCellStyleVideo;
        case 11: case 12:case 13:
            return PrerryCellStyleBig;
        default:
            return PrerryCellStyleNone;
    }
}

-(UIColor *)getTitleColor{
    
    if (self.isread == 0) {
        
        return [UIColor color_t_2];
    }
    
    return [UIColor color_t_3];
}



/**
 *  第一张图
 *
 *  @return 第一张图的额URl
 */
-(NSURL *)firstImageUrl{
    if (self.imgslist.count >= 1) {
        OddityStringObject *image = [self.imgslist objectAtIndex:0];
        if (image) {
            
            return [[NSURL alloc]initWithString: [NSString stringWithFormat:@"%@?x-oss-process=image/resize,m_fixed,h_400",image.value]];
        }
    }
    return nil;
}

/**
 *  第二张图
 *
 *  @return 第二张图的URL
 */
-(NSURL *)secondImageUrl{
    
    if (self.adRequestMode == 2) {
        
        return [self firstImageUrl];
    }
    
    if (self.imgslist.count >= 2) {
        OddityStringObject *image = [self.imgslist objectAtIndex:1];
        if (image) {
            return [[NSURL alloc]initWithString: [NSString stringWithFormat:@"%@?x-oss-process=image/resize,m_fixed,h_400",image.value]];
        }
    }
    return nil;
}

/**
 *  第三张图
 *
 *  @return 第三张图的URL
 */
-(NSURL *)thirdImageUrl{
    
    if (self.imgslist.count >= 2) {
        OddityStringObject *image = [self.imgslist objectAtIndex:2];
        if (image) {
            return [[NSURL alloc]initWithString: [NSString stringWithFormat:@"%@?x-oss-process=image/resize,m_fixed,h_400",image.value]];
        }
    }
    return nil;
}

/**
 *  大图
 *
 *  @return 大图的URL
 */
-(NSURL *)bigImageUrl{
    
    NSInteger index = self.style-11;
    
    if (self.imgslist.count >= index) {
        OddityStringObject *image = [self.imgslist objectAtIndex:index];
        if (image) {
            return [[NSURL alloc]initWithString:image.value];
        }
    }
    return nil;
}

/**
 *  视频地址
 *
 *  @return <#return value description#>
 */
-(NSURL *)videoImageUrl{
    if (self.thumbnail) {
        return [[NSURL alloc]initWithString:self.thumbnail];
    }
    return nil;
}

-(NSURL *)pIconImageUrl{
    
    if (self.icon) {
        return [[NSURL alloc]initWithString:self.icon];
    }
    return nil;
}


-(OddityShareObject *)getShareObject{
    
    NSString* shareUrl = [NSString stringWithFormat:ODDITY_SHARE_URL,self.nid];
    
    return [[OddityShareObject alloc]initWithTitle:self.title coverImageUrl:self.thumbnail urlString:shareUrl];
}


-(NSAttributedString *)titleAttributedString{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 3;
//    paragraphStyle.alignment = NSTextAlignmentJustified;//（两端对齐的）文本对齐方式：（左，中，右，两端对齐，自然）
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_new_title],
                             NSParagraphStyleAttributeName: paragraphStyle,
                             NSKernAttributeName:@1,
                             NSForegroundColorAttributeName: [self getTitleColor]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.title attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)pNameAttributedString{
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_7],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: self.pname attributes:attris];
    
    return attriString;
}


-(NSAttributedString *)tagAttributedString{
    
    TagObject *to = [self gTagObject];
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_8],
                             NSForegroundColorAttributeName: [to borderAndTextColor]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:to.textString attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)commentAttributedString{
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_7],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@",[NSString commentFormatString:self.comment],@"评"] attributes:attris];
    
    return attriString;
}


-(NSAttributedString *)adsTitleAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_5],
                             NSForegroundColorAttributeName: [UIColor color_t_2]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.title attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)playListTitleAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_6],
                             NSForegroundColorAttributeName: [UIColor color_t_11]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.title attributes:attris];
    
    return attriString;
}

-(BOOL)isHiddenTimeLabel{

    return self.duration <= 0;
}

-(NSString *)timeString{
    
    NSInteger proMin = self.duration / 60;//当前秒
    NSInteger proSec = self.duration % 60;//当前分钟
    
    NSString *currentTimeStr = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];
    
    return currentTimeStr;
}

-(NSAttributedString *)timeAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_9],
                             NSForegroundColorAttributeName: UIColor.color_t_10
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.timeString attributes:attris];
    
    return attriString;
}

#define Oddity_delete_button_width 18.f
#define Oddity_tag_label_width 28.f
#define Oddity_right_image_width UIScreen.mainScreen.bounds.size.width/3

-(CGFloat)pnameRightSpaceMinValue{

    CGFloat rightSpace = Oddity_delete_button_width;
    
    if (!self.isHiddenTagView) {
        
        rightSpace += 5;
        rightSpace += 28;
    }
    
    if (!self.isHiddenComment) {
        
        rightSpace += 6;
        rightSpace += [self.commentAttributedString boundingRectWithSize:(CGSizeMake(1000, 100)) options:0 context:nil].size.width;
    }
    
    rightSpace += 16;
    
    switch (self.cellStyle) {
        case PrerryCellStyleNone:case PrerryCellStyleTwo:case PrerryCellStyleThree:case PrerryCellStyleBig:case PrerryCellStyleVideo:case PrerryCellStyleThreeAds:case PrerryCellStyleBigAds: break;
        case PrerryCellStyleOne:case PrerryCellStyleOneAds:{
            
            rightSpace += 12;
            rightSpace += Oddity_right_image_width;
        }
            break;
        case PrerryCellStyleSpecial:case PrerryCellStyleRefresh:break;
    }
    
    return -rightSpace;
}

@end

