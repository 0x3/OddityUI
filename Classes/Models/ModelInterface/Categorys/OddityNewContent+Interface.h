//
//  OddityNewContent+Interface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "OddityNewContent.h"
#import "OddityShareInterface.h"
#import "OddityNewContentInterface.h"

@interface OddityNewContent (Interface)<OddityNewContentInterface,OddityShareInterface>

@end
