//
//  OddityNewContent+Interface.m
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//

#import "OddityCategorys.h"

#import "OddityNewContent+Interface.h"

@implementation OddityNewContent (Interface)


-(BOOL)isShowHotHeader{
    
    return self.hotCommits.count > 0;
}

-(BOOL)isShowNormalHeader{
    
    return self.commits.count > 0;
}

-(NSString *)HotHeaderString{
    
    return [NSString stringWithFormat:@"热门评论(%lu)",(unsigned long)self.hotCommits.count];
}

-(NSString *)NormalHeaderString{
    
    return [NSString stringWithFormat:@"最新评论(%@)",[NSString commentFormatString:self.comment]];
}


- (OddityShareObject *)getShareObject{
    
    NSString* shareUrl = [NSString stringWithFormat: ODDITY_SHARE_URL,self.nid];
    
    return [[OddityShareObject alloc]initWithTitle:self.title coverImageUrl:[self getImageUrl] urlString:shareUrl];
}

-(NSString *)getImageUrl{
    
    OddityNew *new = [OddityNew objectInRealm:[RLMRealm OddityRealm] forPrimaryKey:@(self.nid)];
    
    if(new){
        
        if (new.cellStyle == PrerryCellStyleVideo) {
            
            return new.thumbnail;
        }
        
        if(new.imgslist.count > 0 && new.imgslist.firstObject.value.length > 0){
            
            return new.imgslist.firstObject.value;
        }
    }
    
    if (self.thumbnail) {
        
        return self.thumbnail;
    }
    
    RLMResults<OddityNewContentBody *> *results = [self.content objectsWhere:@"img != nil"];
    
    if (results.count > 0){
        
        return results.firstObject.img;
    }
    
    return nil;
}


- (NSString *)jouDescString:(BOOL)html{
    
    NSMutableString *string = [[NSMutableString alloc]init];
    
    NSString *htmlSpace = html ? @"&nbsp;&nbsp;&nbsp;" : @"   ";
    
    [string appendString:self.pname];
    
    if ( [self.ptimes oddity_isThisYear] ){
        
        [string appendString:[NSString stringWithFormat:@"%@%@",htmlSpace,[self.ptimes oddity_dateToString:@"MM-dd HH:mm"]]];
    }else{
        
        [string appendString:[NSString stringWithFormat:@"%@%@",htmlSpace,[self.ptimes oddity_dateToString:@"yyyy-MM-dd HH:mm"]]];
    }
    
    if ( self.comment > 0 ){
        
        [string appendString:[NSString stringWithFormat:@"%@%@评",htmlSpace,[NSString commentFormatString:self.comment]]];
    }
    
    return string;
}

-(NSAttributedString *)titleAttributedString{

    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 10;// 字体的行间距
    paragraphStyle.minimumLineHeight = 13;//最低行高
    
    NSDictionary *attris = @{
                             NSFontAttributeName: [UIFont boldSystemFontOfSize:[UIFont font_t_text_detail_title].pointSize],
                             NSParagraphStyleAttributeName: paragraphStyle,
                             NSForegroundColorAttributeName: [UIColor color_t_2]
                             };
    
    
    
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.title attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)infoAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_6],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[self jouDescString:false] attributes:attris];
    
    return attriString;
}


-(OddityNewContent *)clearSelf{
    
    [[RLMRealm OddityRealm] beginWriteTransaction];
    
    [self.commits removeAllObjects];
    [self.hotCommits removeAllObjects];
    [self.aboutNewList removeAllObjects];
    
    [[RLMRealm OddityRealm] commitWriteTransaction];
    
    return self;
}


-(int)newStyle{
    
    OddityNew *new = [OddityNew objectInRealm:[RLMRealm OddityRealm] forPrimaryKey:[NSNumber numberWithInt:self.nid]];
    
    if (new && new.rtype != 0) {
        
        if( new.rtype == 1 ){
            
            return 2;
        }
        
        if( new.rtype == 2 ){
            
            return 3;
        }
        if( new.rtype == 3 ){
            
            return 4;
        }
    }
    
    return 0;
}


-(NSString *)WK_WebViewLocalHtmlString:(BOOL)isWebview{
    
    NSString *color2Str = [[UIColor color_t_2] oddity_color2String];
    NSString *color3Str = [[UIColor color_t_3] oddity_color2String];
    NSString *color5Str = [[UIColor color_t_5] oddity_color2String];
    NSString *color6Str = [[UIColor color_t_6] oddity_color2String];
    
    CGFloat alphtFloat = [OddityUISetting shareOddityUISetting].oddityThemeMode == OddityThemeModeNormal ? 1 : 0.45;;
    
    NSString *bodyStr = @"";
    
    NSString *title = [NSString stringWithFormat:@"#title{ font-size:%fpx; font-weight:500; color: %@;}",[UIFont font_t_text_detail_title].pointSize,color2Str];
    NSString *subtitle = [NSString stringWithFormat:@"#subtitle{ font-size:%fpx; color: %@; }",[UIFont font_t_6].pointSize,color3Str];
    NSString *bodysection = [NSString stringWithFormat:@"#body_section{ font-size:%fpx; color: %@; }",[UIFont font_t_detail_context].pointSize,color2Str];
    
    NSString *themeStr = [NSString stringWithFormat:@"body{ background-color : %@ } a { color: %@; } a:link { color: %@; }a:visited { color: %@; }a:hover { color: %@; }a:active { color: %@; } .line-class {background-color: %@;height:1px;border:none; } .imgDiv{ opacity: %f; } ",color6Str,color2Str,color2Str,color2Str,color2Str,color2Str,color5Str,alphtFloat];
    

    for (OddityNewContentBody *body in self.content) {
        
        if( body.txt ) {
            
            bodyStr = [bodyStr stringByAppendingString:[NSString stringWithFormat:@"<p>%@</p>",body.txt]];
        }
        
        if ( body.vid ) {
            
            NSString *videoStr =  [body.vid oddity_replaceStringRegex:@"(preview.html)" replace:@"player.html"];
            videoStr = [videoStr oddity_replaceStringRegex:@"(allowfullscreen=\"\")" replace:@""];
            videoStr = [videoStr oddity_replaceStringRegex:@"(class=\"video_iframe\")" replace:@""];
            videoStr = [videoStr oddity_replaceStringRegex:@"(width=[0-9]+&)" replace:@""];
            videoStr = [videoStr oddity_replaceStringRegex:@"(height=[0-9]+&)" replace:@""];
            videoStr = [videoStr oddity_replaceStringRegex:@"(height=\"[0-9]+\")" replace:@""];
            videoStr = [videoStr oddity_replaceStringRegex:@"(width=\"[0-9]+\")" replace:@""];
            videoStr = [videoStr oddity_replaceStringRegex:@"(&?amp;)" replace:@""];
            videoStr = [videoStr oddity_replaceStringRegex:@"(auto=0)" replace:@""];
            
            bodyStr = [bodyStr stringByAppendingString:[NSString stringWithFormat:@"<div id= \"video\">%@</div>",videoStr]];
//            bodyStr = [bodyStr stringByAppendingString:videoStr];
        }

        if( body.img ) {
            
            NSString *baseStr = @"<div class=\"imgDiv\">&img&</div>";
            
            NSArray *strArray = [body.img oddity_findStringsRegex:@"_(\\d+)X(\\d+)."];

            if (strArray.count == 3) {
 
                float width = [strArray[1] floatValue];
                float height = [strArray[2] floatValue];
                
                float cwidth = oddityUIScreenWidth-36;
                
                int nwidth = (int)MIN(width, cwidth);
                int nheight = (int)height/(width/nwidth);
                
                baseStr = [NSString stringWithFormat:@"<div style=\"width:%dpx;height:%dpx; background-color: %@;\" class=\"imgDiv\">&img&</div>",nwidth,nheight,UIColor.color_t_5.oddity_color2String];
                baseStr = [baseStr oddity_replaceStringRegex:@"&img&" replace: [NSString stringWithFormat:@"<img data-src=\"%@\" width=100%% height=100%% >",body.img]];
            }else{
            
                baseStr = [baseStr oddity_replaceStringRegex:@"&img&" replace: [NSString stringWithFormat:@"<img data-src=\"%@\" width=100%% >",body.img]];
            }
            
            bodyStr = [bodyStr stringByAppendingString:baseStr];
        }
    }
    
    
    bodyStr = [bodyStr stringByAppendingString:[NSString stringWithFormat:@"<p id=\"widthSc\" hidden>%f</p>",oddityUIScreenWidth]];
    
    NSMutableString *results = [[NSMutableString alloc]init];
    
    [results appendString:@"<html manifest=\"appcache.manifest\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>XHTML Tag Reference</title><meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0\" />"];
    
    
    NSString *topHeader = @"<script src=\"jquery.js\"></script><link href=\"content.css\" rel=\"stylesheet\" />";
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 9.0) {
        
        NSString *file1 =  [[NSBundle oddity_shareBundle] pathForResource:@"jquery" ofType:@"js"];
        NSString *file3 =  [[NSBundle oddity_shareBundle] pathForResource:@"content" ofType:@"css"];
        if (file1 && file3) {
            
            NSString *fileStr1 = [NSString stringWithContentsOfFile:file1 encoding:(NSUTF8StringEncoding) error:nil];
            NSString *fileStr3 = [NSString stringWithContentsOfFile:file3 encoding:(NSUTF8StringEncoding) error:nil];
            
            topHeader = [NSString stringWithFormat:@"<script type=\"text/javascript\">%@</script><style type=\"text/css\">%@</style>",fileStr1,fileStr3];
        }
    }
    
    [results appendString:topHeader];
    
    
    [results appendString:[NSString stringWithFormat:@"<style type=\"text/css\">%@%@%@%@</style>",title,subtitle,bodysection,themeStr]];
    
    [results appendString:@"</head><body id=\"container\"><div id=\"section\"><div id = \"imhg\"></div><div id=\"title_section\">"];
    
    [results appendString:[NSString stringWithFormat:@"<div id=\"title\">%@</div><div id=\"subtitle\">%@</div></div><HR class = \"line-class\" /><div id=\"body_section\">%@</div>",self.title,[self jouDescString:true],bodyStr]];
    
    NSString *bottomHeader = @"</div></body><script src=\"index.js\"></script></html>";
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [[[UIDevice currentDevice] systemVersion] floatValue] < 9.0) {
        
        NSString *inStr =  [NSString stringWithContentsOfFile:[[NSBundle oddity_shareBundle] pathForResource:@"index" ofType:@"js"] encoding:(NSUTF8StringEncoding) error:nil];
        if (inStr) {
            
            bottomHeader = [NSString stringWithFormat:@"</div></body><script type=\"text/javascript\">%@</script></html>",inStr];
        }
    }
    
    
    [results appendString:bottomHeader];
    
    return results;
}

-(UIColor *)collectButtonTintColor{

    if (self.colFlag == 0) {
        
        return UIColor.color_t_2;
    }
    
    return OddityUISetting.shareOddityUISetting.mainTone;
}

-(UIImage *)collectButtonImage{
    
    if (self.colFlag == 0) {
        
        return UIImage.oddity_detail_collect_image;
    }
    
    return UIImage.oddity_detail_collected_image;
}

-(NSArray<NSString *> *)contentBodyImageArray{

    NSMutableArray *array = [NSMutableArray array];
    
    for (OddityNewContentBody *body in self.content) {
        
        if (body.img) {
            
            [array addObject:body.img];
        }
    }
    
    return array;
}

-(BOOL)isVideoModel{

    return self.videourl;
}

@end
