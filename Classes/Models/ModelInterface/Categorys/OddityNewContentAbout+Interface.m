//
//  OddityNewContentAbout+Interface.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityShareUser.h"
#import "OddityLogObject.h"
#import "OddityCategorys.h"
#import "OddityNewContentAbout+Interface.h"

@implementation OddityNewContentAbout (Interface)

-(NSString *)advertisementId{
    
    if (!self.isAdsAbout) {
        
        return @"-1";
    }
    
    return [OddityShareUser userPlacementBy:(OddityAdvertisementPlacementModeRelevantSmall) RequestAdsMode:self.adRequestMode];
}

-(NSString *)advertisementSourceStr{

    switch ((OddityUserRequestAdsMode)self.adRequestMode) {
        case OddityUserRequestAdsModeGDTApi:
            return OddityAdvertisementGDTAPISourceStr;
        case OddityUserRequestAdsModeGDTSDK:
            return OddityAdvertisementGDTSDKSourceStr;
        case OddityUserRequestAdsModeYiFuSdk:
            return OddityAdvertisementYifuSDKSourceStr;
    }
}

+(NSString *)advertisementId{
    
    return [OddityShareUser userPlacementBy:(OddityAdvertisementPlacementModeRelevantSmall) RequestAdsMode: OddityShareUser.defaultUser.getAdsMode];
}

-(UIColor *)getTitleColor{
    
    if (self.isread == 0) {
        
        return [UIColor color_t_2];
    }
    
    return [UIColor color_t_3];
}

-(OddityAboutNewStyle)cellStyle{
    
    if ( ![self.img oddity_isStringEmpty] ) {
        
        return AboutNewStyleImage;
    }
    
    return AboutNewStyleText;
}

-(NSURL *)coverImageUrl{
    
    if ( self.img ) {
        
        return [[NSURL alloc]initWithString: [NSString stringWithFormat:@"%@",self.img]];
    }
    return nil;
}

-(int)getTrueNewId{
    
    if( self.nid  ) { return self.nid; }
    
    if (self.url) {
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            
            NSURLComponents *urlComponents = [NSURLComponents componentsWithURL: [[NSURL alloc] initWithString:self.url] resolvingAgainstBaseURL:NO];
            
            NSArray *queryItems = urlComponents.queryItems;
            
            return [[self valueForKey:@"nid" fromQueryItems:queryItems] intValue];
        }else{
            
            NSArray *res = [self.url oddity_findStringsRegex:@"nid=(\\d+)"];
            
            if( res.count == 2 ){ return [res[1] intValue];  }
            
            return 10981896;
        }
    }
    return 0;
}

- (NSString *)valueForKey:(NSString *)key fromQueryItems:(NSArray *)queryItems {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems
                                  filteredArrayUsingPredicate:predicate]
                                 firstObject];
    return queryItem.value;
}

-(NSAttributedString *)titleAttributedString{
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentJustified;//（两端对齐的）文本对齐方式：（左，中，右，两端对齐，自然）
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_new_title],
                             NSParagraphStyleAttributeName: paragraphStyle,
                             NSForegroundColorAttributeName: [self getTitleColor]
                             };
    
    
    
//    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"[%d]-%@",self.sortIndex,self.title] attributes:attris];
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.title attributes:attris];
    return attriString;
}

-(NSAttributedString *)pNameAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_7],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.pname attributes:attris];
    
    return attriString;
}

-(BOOL)isHiddenTimeLabel{
    
    return self.duration <= 0;
}

-(NSString *)timeString{
    
    NSInteger proMin = self.duration / 60;//当前秒
    NSInteger proSec = self.duration % 60;//当前分钟
    
    NSString *currentTimeStr = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];
    
    return currentTimeStr;
}

-(NSAttributedString *)timeAttributedString{
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_9],
                             NSForegroundColorAttributeName: UIColor.color_t_10
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.timeString attributes:attris];
    
    return attriString;
}

@end
