//
//  OddityNewContentComment+Interface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityNewContentComment.h"
#import "OddityNewCommentInterface.h"

@interface OddityNewContentComment (Interface)<OddityNewCommentInterface>

@end
