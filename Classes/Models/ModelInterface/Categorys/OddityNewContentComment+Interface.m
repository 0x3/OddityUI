//
//  OddityNewContentComment+Interface.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OdditySetting.h"
#import "OddityCategorys.h"
#import "OddityNewContentComment+Interface.h"

@implementation OddityNewContentComment (Interface)

-(NSURL *)avatorImageUrl{
    
    if ( self.avatar ) {
        
        return [[NSURL alloc]initWithString: [NSString stringWithFormat:@"%@",self.avatar]];
    }
    return nil;
}

-(BOOL)isHiddenCommentLabel {
    
    return self.commend <= 0;
}

-(NSString *)commentStr{
    
    return [NSString stringWithFormat:@"%d",self.commend];
}


-(NSAttributedString *)uNameAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_6],
                             NSForegroundColorAttributeName: [OddityUISetting shareOddityUISetting].mainTone
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.uname attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)commentAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_7],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.commentStr attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)contentAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_comment_context],
                             NSForegroundColorAttributeName: [UIColor color_t_2]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self.content attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)timeAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_9],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: [self.ctimes oddity_distanceTimeWithBeforeTime] attributes:attris];
    
    return attriString;
}

-(UIImage *)commentButtonImage{

    if (self.upflag == 0) {
        
        return UIImage.oddity_detail_up_normal_image;
    }
    
    return UIImage.oddity_detail_up_select_image;
}

-(UIColor *)commentButtonTintColor{

    if (self.upflag == 0) {
        
        return UIColor.color_t_3;
    }
    
    return UIColor.color_t_7;
//    return OddityUISetting.shareOddityUISetting.mainTone;
}

-(NSString *)willShowNumberString{

    
    if (self.upflag == 0) {
        
        return [NSString stringWithFormat:@"%d",self.commend+1];
    }
    
    return [NSString stringWithFormat:@"%d",self.commend-1];
}

-(NSAttributedString *)userCenter_contentAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_4],
                             NSForegroundColorAttributeName: [UIColor color_t_2]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: self.content attributes:attris];
    
    return attriString;
}

-(NSAttributedString *)userCenter_ntitleAttributedString{
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_5],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: [NSString stringWithFormat:@"%@%@",@"[原文] ",self.ntitle] attributes:attris];
    
    return attriString;
    
}

-(NSAttributedString *)userCenter_timeAttributedString{

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_6],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: [self.ctimes oddity_distanceTimeWithBeforeTime] attributes:attris];
    
    return attriString;
}
@end
