//
//  OddityChannelInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>


/**
 *  频道管理对象的 ViewModel 协议
 */
@protocol OddityChannelInterface <NSObject>
@required

/**
 *   获得管理图片，也就是右上角的删除还是新增
 *
 *  @return 一个图片对象
 */
-(UIImage *) getManagerImage;

/**
 *  获取到 背景颜色 背景颜色只针对于 是否为 ·奇点· 频道。
 *
 *  @return 返回背景颜色
 */
-(UIColor *) getBackColor;

/**
 *   返回标题的 TextColor 。
 *
 *  @return 返回TextColor 颜色
 */
-(UIColor *) getTitleColor;

/**
 *  获取到 当前频道的 图片是否删除还是现实。 主要是也是 针对于 奇点 频道。
 *
 *  @return Bool
 */
-(BOOL)getManagerImageHidden;



/**
 子频道名称的 富文本 属性对象
 
 @return 富文本
 */
-(NSAttributedString *)cNameAttributedString;


/**
 展示 子频道 富文本 属性的所需要的大小
 
 @return 大小
 */
-(CGSize)cNameAttributedStringNeedSize;
@end

