//
//  OddityNewCommentInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>


/**
 *  新闻评论 viewmodel 数据
 */
@protocol OddityNewCommentInterface <NSObject>

/**
 *  是否隐藏新闻评论Label
 *
 *  @return 是否隐藏
 */
-(BOOL)isHiddenCommentLabel;

/**
 *  新闻的评论对象的 用户 头像地址
 *
 *  @return url 地址
 */
-(NSURL *)avatorImageUrl;




/**
 获取用户名称的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)uNameAttributedString;

/**
 获取点赞的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)commentAttributedString;

/**
 获取内容的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)contentAttributedString;

/**
 获取事件的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)timeAttributedString;


/**
 点赞按钮的 图片

 @return 图片
 */
-(UIImage *)commentButtonImage;

/**
 点赞按钮的 图片
 
 @return 图片
 */
-(UIColor *)commentButtonTintColor;

/**
 即将显示的数字
 
 @return 字符串
 */
-(NSString *)willShowNumberString;


/**
 个人中心的评论详情的 属性 字符串

 @return 富文本
 */
-(NSAttributedString *)userCenter_contentAttributedString;


/**
 个人中心的评论详情的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)userCenter_ntitleAttributedString;

/**
 个人中心的评论详情的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)userCenter_timeAttributedString;
@end
