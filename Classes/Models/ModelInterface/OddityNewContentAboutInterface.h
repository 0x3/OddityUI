//
//  OddityNewContentAboutInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

/**
 *  新闻列表的新闻对象的类型 用来控制Cell 显示成所需要的样式
 */
typedef NS_ENUM(NSInteger, OddityAboutNewStyle) {
    /**
     *  文字样式
     */
    AboutNewStyleText = 0,
    /**
     *  单张图的样式
     */
    AboutNewStyleImage = 1
};




/**
 *  相关新闻的 协议
 */
@protocol OddityNewContentAboutInterface <NSObject>

/**
 *  相关新闻烈性
 *
 *  @return 新闻类型
 */
-(OddityAboutNewStyle)cellStyle;

/**
 *  封面图片 URL
 *
 *  @return URL
 */
-(NSURL *)coverImageUrl;


/**
 广告id
 
 @return 广告ID
 */
-(NSString *)advertisementId;

/**
 广告来源地址
 
 @return 广告ID
 */
-(NSString *)advertisementSourceStr;

/**
 广告id
 
 @return 广告ID
 */
+(NSString *)advertisementId;

/**
 *  得到真实的新闻ID
 *
 *  @return 新闻id
 */
-(int)getTrueNewId;

/**
 隐藏 时间 Label
 
 @return 是否
 */
-(BOOL)isHiddenTimeLabel;

/**
 获取时间的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)timeAttributedString;

/**
 获取标题的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)titleAttributedString;

/**
 获取来源的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)pNameAttributedString;

@end
