//
//  OddityNewContentInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityNewContent.h"
#import <UIKit/UIKit.h>

/**
 *  关于新闻详情的 协议
 */
@protocol OddityNewContentInterface <NSObject>

/**
 *  是否显示热门头部视图
 *
 *  @return 是否显示
 */
-(BOOL)isShowHotHeader;

/**
 *  是否显示普通头部视图
 *
 *  @return 是否显示
 */
-(BOOL)isShowNormalHeader;

/**
 *  热门头部视图 String
 *
 *  @return String
 */
-(NSString *)HotHeaderString;

/**
 *  普通头部视图 String
 *
 *  @return String
 */
-(NSString *)NormalHeaderString;



/**
 获取标题的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)titleAttributedString;

/**
 获取信息的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)infoAttributedString;


/**
 获取 webview 页面展示需要的 html String 对象

 @param isWebview 是不是 WebView
 @return 字符串
 */
-(NSString *)WK_WebViewLocalHtmlString:(BOOL)isWebview;



/**
 清空 新闻详情里的 各种数据

 @return 返回新闻详情
 */
-(OddityNewContent *)clearSelf;

/**
 *  获取新闻的类型ID
 *
 0	普通新闻
 1	个性推荐
 2	热点推荐
 3	编辑推荐
 4	广告推广
 -	后续新增
 
 0普通、1热点、2推送、3广告、4专题
 
 *  @return 类型int值
 */
-(int)newStyle;


/**
 声称新闻的描述

 @param html 是否是要显示在 html 页面上的
 @return 字符串
 */
- (NSString *)jouDescString:(BOOL)html;


/**
 收藏按钮的 图片
 
 @return 图片
 */
-(UIImage *)collectButtonImage;

/**
 收藏按钮的 颜色
 
 @return 颜色
 */
-(UIColor *)collectButtonTintColor;


/**
 新闻详情的图片集合

 @return 图片URL集合
 */
-(NSArray<NSString *> *)contentBodyImageArray;


/**
 是否是 视频新闻

 @return 新闻视频
 */
-(BOOL)isVideoModel;
@end

