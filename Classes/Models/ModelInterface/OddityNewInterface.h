//
//  OddityNewInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//


#import <UIKit/UIKit.h>
#import "OddityTagObject.h"

/**
 *  新闻列表的新闻对象的类型 用来控制Cell 显示成所需要的样式
 */
typedef NS_ENUM(NSInteger, OddityPrerryCellStyle) {
    /**
     *  最最普通的样式
     */
    PrerryCellStyleNone = 0,
    /**
     *  单张图的样式
     */
    PrerryCellStyleOne = 1,
    /**
     *  两张图的样式
     */
    PrerryCellStyleTwo = 2,
    /**
     *  三张图的样式
     */
    PrerryCellStyleThree = 3,
    /**
     *  大图显示样式
     */
    PrerryCellStyleBig = 4,
    /**
     *  视频显示样式
     */
    PrerryCellStyleVideo = 5,
    /**
     *  主题样式
     */
    PrerryCellStyleSpecial = 6,
    /**
     *  新闻刷新视图
     */
    PrerryCellStyleRefresh = 7,
    /**
     *  单图广告样式
     */
    PrerryCellStyleOneAds = 8,
    /**
     *  三图广告样式
     */
    PrerryCellStyleThreeAds = 9,
    /**
     *  大图广告样式
     */
    PrerryCellStyleBigAds = 10
};


/**
 *   新闻展示到 Cell 上的时候 针对于Cell 所需要实现的 ViewModel 协议
 */
@protocol OddityNewInterface <NSObject>

@required
/**
 *  `UITableViewCell` 的风格。
 *
 * 根据新闻的某些属性的判断，获取到cell展示的风格
 *
 *  @return PrerryCellStyle
 */
-(OddityPrerryCellStyle)cellStyle;


/**
 广告id

 @return 广告ID
 */
-(NSString *)advertisementId;

/**
 广告来源地址
 
 @return 广告ID
 */
-(NSString *)advertisementSourceStr;


/**
 广告id
 
 @return 广告ID
 */
+(NSString *)advertisementId;

/**
 *   是否显示 Tag view，根据新闻是否为 热点 推送 广告 等新闻类型 进行判断是否显示 标签View
 *
 *  @return 是否显示标签
 */
-(BOOL)isHiddenTagView;

/**
 *   获取标签视图 包括 内容 边框颜色
 *
 *  @return 标签对象
 */
-(TagObject *) gTagObject;


/**
 *   第一张图的 URL 的对象
 *   没有图片的话 默认空字符串的一个URL
 *  @return URL
 */
-(NSURL *)firstImageUrl;

/**
 *   第二张图的 URL 的对象
 *   没有图片的话 默认空字符串的一个URL
 *  @return URL
 */
-(NSURL *)secondImageUrl;

/**
 *   第三张图的 URL 的对象
 *   没有图片的话 默认空字符串的一个URL
 *  @return URL
 */
-(NSURL *)thirdImageUrl;

/**
 *   大的 URL 的对象
 *   没有图片的话 默认空字符串的一个URL
 *  @return URL
 */
-(NSURL *)bigImageUrl;

/**
 *  视频封面地址
 *
 *  @return 视频URL
 */
-(NSURL *)videoImageUrl;

/**
 *  视频来源头像地址
 *
 *  @return 头像URL
 */
-(NSURL *)pIconImageUrl;
/**
 *  根据判断新闻的的评论个数 获取到是否展示评论 Label
 *
 *  @return 是否展示
 */
-(BOOL)isHiddenComment;



/**
 隐藏 时间 Label

 @return 是否
 */
-(BOOL)isHiddenTimeLabel;

/**
 获取时间的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)timeAttributedString;


/**
 获取标题的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)titleAttributedString;


/**
 获取来源的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)pNameAttributedString;

/**
 获取标签的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)tagAttributedString;

/**
 获取评论的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)commentAttributedString;




/**
 获取广告的 属性 字符串
 
 @return 富文本
 */
-(NSAttributedString *)adsTitleAttributedString;


/**
 播放列表的 标题属性

 @return 富文本
 */
-(NSAttributedString *)playListTitleAttributedString;


/**
 新闻 右侧距离最右侧的位置 应该是多少

 @return 右侧位置
 */
-(CGFloat)pnameRightSpaceMinValue;

@end
