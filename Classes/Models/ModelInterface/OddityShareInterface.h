//
//  OddityShareInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/19.
//
//


#import <UIKit/UIKit.h>
#import "OdditySetting.h"


#define ODDITY_SHARE_URL @"http://deeporiginalx.com/news.html?type=0&nid=%d"

/**
 *  相关新闻的 协议
 */
@protocol OddityShareInterface <NSObject>


/**
 获得分享对象

 @return 分享对象
 */
-(OddityShareObject *)getShareObject;
@end


