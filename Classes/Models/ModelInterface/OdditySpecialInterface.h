//
//  OdditySpecialInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//


#import <UIKit/UIKit.h>

/**
 *  主题 所需要的数据的 ViewModel 协议
 */
@protocol OdditySpecialInterface <NSObject>

@required

/**
 *  封面视图的 URL。 如果没有那么就返回一个 空的 字符串的 URL
 *
 *  @return URL
 */
-(NSURL *)coverImageUrl;

/**
 *  简介的。
 *  为了适配的前面的小标签 我们需要针对于 字符串 进行预留空格 操作
 *
 *  @return 字符串
 */
-(NSString *)descString;
@end
