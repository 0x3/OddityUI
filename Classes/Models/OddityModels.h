//
//  OddityModels.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

/// 是否使用 OddityChannleNews 对象 当作过度对象 作用
#ifndef OddityModels_h
#define OddityModels_h

#import "OddityMainModel.h" // 主要的model 对象
#import "OddityRequest.h" // 请求对象
#import "OddityUtilModels.h" // 扶助对象

#import "OddityNew+Interface.h"
#import "OddityChannel+Interface.h"
#import "OddityNewContent+Interface.h"
#import "OddityNewContentAbout+Interface.h"
#import "OddityNewContentComment+Interface.h"

#import "OddityNew+Advertisement.h"
#import "OddityNewContentAbout+Advertisement.h"

#endif /* OddityModels_h */
