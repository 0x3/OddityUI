//
//  OddityAFAppDotNetAPIClient.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <AFNetworking/AFNetworking.h>

/// 是否 https
#define oddityIsHttps false



#if oddityIsHttps
static NSString * const AFAppDotNetAPIBaseURLString = @"https://bdp.deeporiginalx.com";
#else
static NSString * const AFAppDotNetAPIBaseURLString = @"http://bdp.deeporiginalx.com";
#endif

@interface OddityAFAppDotNetAPIClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

+ (instancetype)sharedDefaultClient;

@end
