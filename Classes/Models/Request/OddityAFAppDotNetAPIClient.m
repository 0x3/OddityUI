//
//  OddityAFAppDotNetAPIClient.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityAFAppDotNetAPIClient.h"




@implementation OddityAFAppDotNetAPIClient

+ (instancetype)sharedClient {
    static OddityAFAppDotNetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[OddityAFAppDotNetAPIClient alloc] initWithBaseURL:[NSURL URLWithString:AFAppDotNetAPIBaseURLString]];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    });
    
    return _sharedClient;
}

+ (instancetype)sharedDefaultClient {
    static OddityAFAppDotNetAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _sharedClient = [[OddityAFAppDotNetAPIClient alloc] init];
        NSMutableSet *newSet = [NSMutableSet set];
        newSet.set = _sharedClient.responseSerializer.acceptableContentTypes;
        [newSet addObject:@"application/javascript"];
        _sharedClient.responseSerializer.acceptableContentTypes = newSet;
    });
    
    return _sharedClient;
}

@end
