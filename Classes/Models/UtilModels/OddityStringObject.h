//
//  OddityStringObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>


/**
 *  字符串对象 用来保存 [""，“”] 这种不可以直接保存的数据
 */
@interface OddityStringObject : RLMObject

/**
 *  需要存在的属性
 */
@property NSString* value;

@end
RLM_ARRAY_TYPE(OddityStringObject) // define RLMArray<Dog>

