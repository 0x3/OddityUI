//
//  OddityStringObject.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityStringObject.h"

@implementation OddityStringObject

/**
 *  String 对象的 主键
 *
 *  @return 主键名称
 */
+ (NSString *)primaryKey { return @"value"; }

@end
