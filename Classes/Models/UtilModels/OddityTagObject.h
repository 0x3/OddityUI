//
//  OddityTagObject.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

/**
 *   标签对象 主要针对于新闻的标签
 */
@interface TagObject : NSObject

/**
 *  标签的内容
 */
@property(nonatomic,strong)NSString *textString;

/**
 *  标签的边框颜色 以及 文字的内容的颜色
 */
@property(nonatomic,strong)UIColor *borderAndTextColor;

@end
