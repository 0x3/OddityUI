//
//  GDTNativeAd+OddityBlock.h
//  Pods
//
//  Created by 荆文征 on 2017/5/3.
//
//

#import "GDTNativeAd.h"
#import "OddityShareUser.h"
#import <OddityOcUI/OddityOcUI.h>



typedef void(^GDTNativeAdCompleteBlock) (NSArray<GDTNativeAdData *> *nativeAdDataArray,NSError *error);


@interface GDTNativeAd (OddityBlock)<GDTNativeAdDelegate>

+(instancetype)initGDTNativeAdWithAdId:(NSString *)advertisementId;

+(instancetype)initGDTNativeAdWithAdMode:(OddityAdvertisementPlacementMode)plac;

/**
 获取一个广告

 @param block 广告获取结果反馈回调
 */
-(void)getAdvertisementWithComplete:(GDTNativeAdCompleteBlock)block;

@end


@interface GDTNativeAdData (OddityCoder)

@end
