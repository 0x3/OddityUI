//
//  GDTNativeAd+OddityBlock.m
//  Pods
//
//  Created by 荆文征 on 2017/5/3.
//
//

#import <objc/runtime.h>
#import "NSDate+Oddity.h"
#import "OddityNewsArrayObject.h"
#import "UIViewController+Oddity.h"
#import "GDTNativeAd+OddityBlock.h"


/**
 默认的 测试 sdk app key
 */
#define OddityGDTSDKAppkeyString @"1105877617"

static void *OddityNativeadKey = "OddityNativeadKey";


static GDTNativeAd *feedBigAd = nil;
static GDTNativeAd *feedSmallAd = nil;
static GDTNativeAd *detailBigAd = nil;
static GDTNativeAd *relatedSmallAd = nil;

@implementation GDTNativeAd (OddityBlock)

+(instancetype)initGDTNativeAdWithAdId:(NSString *)advertisementId{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        feedBigAd = [[GDTNativeAd alloc] initWithAppkey:OddityGDTSDKAppkeyString placementId:advertisementId];
    });
    
    feedBigAd.controller = [UIViewController oddityCurrentViewController];
    
    return feedBigAd;
}

+(instancetype)initGDTNativeAdWithAdMode:(OddityAdvertisementPlacementMode)plac{

    
    NSString *placementId = [OddityShareUser userPlacementBy:plac RequestAdsMode:(OddityUserRequestAdsModeGDTSDK)];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        feedSmallAd = [[GDTNativeAd alloc] initWithAppkey:OddityGDTSDKAppkeyString placementId:placementId];
    });
    
    feedSmallAd.controller = [UIViewController oddityCurrentViewController];
    
    return feedSmallAd;
}

-(void)getAdvertisementWithComplete:(GDTNativeAdCompleteBlock)block{
    
    if (block) {
        
        GDTNativeAd *bestChoice = [OddityNewsArrayObject OutboundGDTSDKAdvObjects];
        if (bestChoice) {
            
            block(@[bestChoice],nil);
            return;
        }
        
        objc_removeAssociatedObjects(self);
        objc_setAssociatedObject(self, OddityNativeadKey, block, OBJC_ASSOCIATION_COPY);
        self.delegate = self;
        [self loadAd:10];
    }
}


-(void)nativeAdSuccessToLoad:(NSArray *)nativeAdDataArray{
    
    GDTNativeAdCompleteBlock block = objc_getAssociatedObject(self, OddityNativeadKey);
    if (block) {
        NSMutableArray *array = [nativeAdDataArray mutableCopy]; // -> 拷贝一份广告集合数据
        block(@[array.firstObject],nil); // -> 先将这一份传递回去
        [array removeObject:array.firstObject]; // -> 将传递回去的广告在集合中删除
        [OddityNewsArrayObject StorageGDTSDKAdvObjects:array]; // -> 将获取的广告集合 入库
    }
}

-(void)nativeAdFailToLoad:(NSError *)error{
    
    GDTNativeAdCompleteBlock block = objc_getAssociatedObject(self, OddityNativeadKey);
    if (block) {
        block(nil,error);
    }
}

@end




@implementation GDTNativeAdData (OddityCoder)

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    encodePropertiesOfObjectToCoder(self, aCoder);
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        
        decodePropertiesOfObjectFromCoder(self, aDecoder);
    }
    return self;
}


/**
 解码

 @param obj 对象
 @param coder 编码器
 */
void decodePropertiesOfObjectFromCoder(id obj, NSCoder *coder)
{
    // copy the property list
    unsigned propertyCount;
    objc_property_t *properties = class_copyPropertyList([obj class], &propertyCount);
    
    for (int i = 0; i < propertyCount; i++) {
        objc_property_t property = properties[i];
        
        char *readonly = property_copyAttributeValue(property, "R");
        if (readonly)
        {
            free(readonly);
//            continue;
        }
        
        NSString *propName = [NSString stringWithUTF8String:property_getName(property)];
        
        @try
        {
            [obj setValue:[coder decodeObjectForKey:propName] forKey:propName];
        }
        @catch (NSException *exception) {
            if (![exception.name isEqualToString:@"NSUnknownKeyException"])
            {
                @throw exception;
            }
            
            NSLog(@"Couldn't decode value for key %@.", propName);
        }
    }
    
    free(properties);
}


/**
 加码

 @param obj 对象
 @param coder 编码器
 */
void encodePropertiesOfObjectToCoder(id obj, NSCoder *coder)
{
    // copy the property list
    unsigned propertyCount;
    objc_property_t *properties = class_copyPropertyList([obj class], &propertyCount);
    
    for (int i = 0; i < propertyCount; i++) {
        objc_property_t property = properties[i];
        
        char *readonly = property_copyAttributeValue(property, "R");
        if (readonly)
        {
            free(readonly);
//            continue;
        }
        
        NSString *propName = [NSString stringWithUTF8String:property_getName(property)];
        
        @try {
            [coder encodeObject:[obj valueForKey:propName] forKey:propName];
        }
        @catch (NSException *exception) {
            if (![exception.name isEqualToString:@"NSUnknownKeyException"])
            {
                @throw exception;
            }
            
            NSLog(@"Couldn't encode value for key %@.", propName);
        }
    }
    
    free(properties);
}



@end
