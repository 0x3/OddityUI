//
//  NSArray+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Foundation/Foundation.h>


@interface NSArray(Oddity)

/**
 *  数据
 *
 *  @return string
 */
-(NSString *)oddity_dataToJsonString;

@end
