//
//  NSBundle+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Foundation/Foundation.h>

@interface NSBundle (Oddity)
/// 根据名称在Bundle中获取图片
+ (NSBundle *)oddity_shareBundle;

@end
