//
//  NSBundle+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "NSBundle+Oddity.h"
#import "OddityMainManagerViewController.h"

@implementation NSBundle (Oddity)

+(NSBundle *)oddity_shareBundle{
    
    static NSBundle *shareBundle;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        NSBundle *oddityBundle =  [NSBundle bundleForClass:[OddityMainManagerViewController class]];
        
        NSURL *url = [oddityBundle URLForResource:@"OdditBundle" withExtension:@"bundle"];
        
        if(url){
            
            shareBundle = [NSBundle bundleWithURL:url];
        }
        
    });
    return shareBundle;
}

@end
