//
//  NSDate+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (Oddity)


/**
 根据 NSDate对象 和 时间格式化 字符串 产生一个 字符串

 @param dateString 时间格式化时间
 @return 时间格式化后的字符串
 */
-(NSString *)oddity_dateToString:(NSString *)dateString;


/**
 类对象根据时间字符串和时间格式化字符串 生成一个NSDate对象
 因为项目所以项目设计了默认的格式化字符串为 yyyy-MM-dd hh:mm:ss
 
 @param dateString 时间字符串
 @return 时间对象
 */
+(NSDate *)oddity_stringToDate:(NSString *)dateString;


/**
 获取时间对象 unix intger 值

 @return unix 时间戳的值
 */
-(long long)oddity_unixInteger;


/**
 根据时间对象 获取时间的unix 时间戳字符串对象

 @return 时间字符串
 */
-(NSString *)oddity_unixString;


/**
 判断是不是今年的时间

 @return 是不是今年
 */
- (BOOL)oddity_isThisYear;


/**
 当前时间和现在的时间相隔时间的一个描述字符串

 @return 描述字符串
 */
- (NSString *)oddity_distanceTimeWithBeforeTime;

-(NSString *)oddityYear;

-(NSString *)oddityMonth;

-(NSString *)oddityDay;

@end
