//
//  NSDate+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "NSDate+Oddity.h"

@implementation NSDate (Oddity)

-(NSString *)oddityYear{

    return [self oddity_dateToString:@"yyyy"];
}

-(NSString *)oddityMonth{
    
    return [self oddity_dateToString:@"MM"];
}

-(NSString *)oddityDay{
    
    return [self oddity_dateToString:@"dd"];
}

+(NSDate *)oddity_stringToDate:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter dateFromString:dateString];
}

-(NSString *)oddity_dateToString:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateString];
    return [dateFormatter stringFromDate:self];
}

-(long long)oddity_unixInteger{
    NSTimeInterval interval = [self timeIntervalSince1970];
    long long totalMilliseconds = interval*1000 ;
    return totalMilliseconds;
}

-(NSString *)oddity_unixString{
    
    return [NSString stringWithFormat:@"%lld",[self oddity_unixInteger]];
}

- (BOOL)oddity_isThisYear{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        NSCalendar *calendar = [NSCalendar currentCalendar];
        // 获得年
        NSInteger nowYear = [calendar component:NSCalendarUnitYear fromDate:[NSDate date]];
        NSInteger selfYear = [calendar component:NSCalendarUnitYear fromDate:self];
        
        return nowYear == selfYear;
    }else{
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy"];
        
        NSInteger nowYear = [[dateFormatter stringFromDate:[NSDate date]] integerValue];
        NSInteger selfYear = [[dateFormatter stringFromDate:self] integerValue];
        
        return nowYear == selfYear;
    }
}

- (NSString *)oddity_distanceTimeWithBeforeTime{
    
    double beTime = [self timeIntervalSince1970];
    
    NSTimeInterval now = [[NSDate date]timeIntervalSince1970];
    double distanceTime = now - beTime;
    NSString * distanceStr;
    
    NSDate * beDate = [NSDate dateWithTimeIntervalSince1970:beTime];
    NSDateFormatter * df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm"];
    NSString * timeStr = [df stringFromDate:beDate];
    
    [df setDateFormat:@"dd"];
    NSString * nowDay = [df stringFromDate:[NSDate date]];
    NSString * lastDay = [df stringFromDate:beDate];
    
    if (distanceTime < 60) {//小于一分钟
        distanceStr = @"刚刚";
    }
    else if (distanceTime <60*60) {//时间小于一个小时
        distanceStr = [NSString stringWithFormat:@"%ld分钟前",(long)distanceTime/60];
    }
    else if(distanceTime <24*60*60 && [nowDay integerValue] == [lastDay integerValue]){//时间小于一天
        distanceStr = [NSString stringWithFormat:@"今天 %@",timeStr];
    }
    else if(distanceTime<24*60*60*2 && [nowDay integerValue] != [lastDay integerValue]){
        
        if ([nowDay integerValue] - [lastDay integerValue] ==1 || ([lastDay integerValue] - [nowDay integerValue] > 10 && [nowDay integerValue] == 1)) {
            distanceStr = [NSString stringWithFormat:@"昨天 %@",timeStr];
        }
        else{
            [df setDateFormat:@"MM-dd HH:mm"];
            distanceStr = [df stringFromDate:beDate];
        }
        
    }
    else if(distanceTime <24*60*60*365){
        [df setDateFormat:@"MM-dd HH:mm"];
        distanceStr = [df stringFromDate:beDate];
    }
    else{
        [df setDateFormat:@"yyyy-MM-dd HH:mm"];
        distanceStr = [df stringFromDate:beDate];
    }
    return distanceStr;
}

@end
