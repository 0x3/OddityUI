//
//  NSDictionary+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary(Oddity)

/**
 *  根据NSDict对象生成对应的String对象
 *
 *  @return string
 */
-(NSString *)oddity_dataToJsonString;


/**
 根据传入的dict 格式字符串 转化成为 dict对象

 @param dictString 字符串
 @return 对象
 */
+(instancetype)oddity_byString:(NSString *)dictString;

@end
