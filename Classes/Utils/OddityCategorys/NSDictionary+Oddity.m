//
//  NSDictionary+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "NSDictionary+Oddity.h"



@implementation NSDictionary(Oddity)

-(NSString *)oddity_dataToJsonString{
    
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

+(instancetype)oddity_byString:(NSString *)dictString{

    NSError * err;
    NSData *data =[dictString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary * response;
    if(data!=nil){
        response = (NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
    }
    
    if (err) {
        NSLog(@"Got an error: %@", err);
    }
    
    return response;
}

@end
