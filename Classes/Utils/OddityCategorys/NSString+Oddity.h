//
//  NSString+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityChannel.h"
#import <Foundation/Foundation.h>

/**
 *   NSString MDS 加密辅助类
 *   Message Digest Algorithm MD5（中文名为消息摘要算法第五版）为计算机安全领域广泛使用的一种散列函数，用以提供消息的完整性保护。
 *   该算法的文件号为RFC 1321（R.Rivest,MIT Laboratory for Computer Science and RSA Data Security Inc. April 1992）。
 */
@interface NSString (Oddity)

/**
 *   根据提供的NSString对象，完成MD5加密，并且返回一个加密完成的MD5字符串
 *
 *  @return MD5加密完成的字符串
 */
- (NSString *)oddity_MD5;


/**
 根据正则表达式 获取 字符串中的 特定字符

 @param regexStr 正则表达式的字符串
 @return 得到的字符串
 */
-(NSArray *)oddity_findStringsRegex:(NSString *)regexStr;


/**
 根据正则表达式 替换相对应的字符串

 @param content 需要替换的字符串
 @param strReplace 正则表达式字符串
 @return 替换完成的字符串
 */
- (NSString *)oddity_replaceStringRegex:(NSString *)content replace:(NSString *)strReplace;


/**
 根据图片的url地址字符串 进行图片等下载

 @param progress 进度的回调方法
 @param finish 图片下载完成的回调方法
 */
-(void)oddity_downloadImageByUrl:(void (^)(int))progress completed:(void (^)(NSString *))finish;


/**
 判断当前字符串是不是一个空的字符串

 @return 是不是空的
 */
-(BOOL)oddity_isStringEmpty;



/**
 获取字符串所有的 NSRange

 @return NSRange
 */
- (NSRange)fullRange;

/**
 格式化数据为 字符串

 @param comment 评论书
 @return 格式化后的字符串
 */
+(NSString *)commentFormatString:(int)comment;


/**
 根据频道名称获取频道

 @return 根据频道名称获取频道
 */
-(OddityChannel *)channelObj;


/**
 是否 Email 格式的 字符串

 @return 是否为 Email
 */
- (BOOL)isValidEmail;


/**
 缓存文件 Path file

 @return 自负
 */
+(NSString *)oddity_cachePathString;
@end
