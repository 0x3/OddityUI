//
//  NSString+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "NSString+Oddity.h"
#import <CommonCrypto/CommonCrypto.h>

#import "RLMRealm+Oddity.h"

#import <PINCache/PINCache.h>
#import <PINRemoteImage/PINRemoteImage.h>

@implementation NSString (Oddity)

-(BOOL)oddity_isStringEmpty {
    if([self length] == 0) { //string is empty or nil
        return YES;
    }
    
    if(![[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        //string is all whitespace
        return YES;
    }
    
    return NO;
}


- (NSString *)oddity_MD5
{
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    unsigned int x=(int)strlen(cStr) ;
    CC_MD5( cStr, x, digest );
    // This is the md5 call
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  [output lowercaseString];
}

-(NSString *)oddity_replaceStringRegex:(NSString *)content replace:(NSString *)strReplace{
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:content options:NSRegularExpressionDotMatchesLineSeparators error:&error];
    NSString *strResult = [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:strReplace];
    return strResult;
}

-(NSArray *)oddity_findStringsRegex:(NSString *)regexStr {
    
    NSMutableArray *mString = [[NSMutableArray alloc] init];
    NSError *error;
    NSRegularExpression *regex = [[NSRegularExpression alloc]initWithPattern:regexStr options:0 error:&error];
    
    NSTextCheckingResult *result = [regex firstMatchInString:self options:0 range:NSMakeRange(0, self.length)];
    
    for (int i = 0 ; i < result.numberOfRanges ; i++) {
        
        [mString addObject:[self substringWithRange:[result rangeAtIndex:i]]];
    }
    
    return mString;
}

-(void)oddity_downloadImageByUrl:(void (^)(int))progress completed:(void (^)(NSString *))finish{
    
    NSString *keyPath = [NSString stringWithFormat:@"html-base64%@",self];
    
    NSString *string = [[PINCache sharedCache] objectForKey: keyPath];
    
    if ( string ) {
        
        finish(string);
        return;
    }
    
    NSURL *downloadUrl = [[NSURL alloc]initWithString:self];
    
    [[PINRemoteImageManager sharedImageManager] downloadImageWithURL:downloadUrl options:(PINRemoteImageManagerDownloadOptionsNone) completion:^(PINRemoteImageManagerResult * _Nonnull result) {
        
        if( result.image ) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSString *base64 = [UIImageJPEGRepresentation(result.image, 0.9) base64EncodedStringWithOptions:0];
                base64 = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",base64];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    finish(base64);
                    
                    [[PINCache sharedCache] setObject:base64 forKey:keyPath];
                });
            });
        }
        
        if ( result.animatedImage ) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSString *base64 = [result.animatedImage.data base64EncodedStringWithOptions:0];
                base64 = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",base64];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    
                    finish(base64);
                    
                    [[PINCache sharedCache] setObject:base64 forKey:keyPath];
                });
            });
        }
        [[[PINRemoteImageManager sharedImageManager]defaultImageCache]removeObjectForKey:[[PINRemoteImageManager sharedImageManager] cacheKeyForURL:downloadUrl processorKey:nil]];
    }];
    
    //    [[PINRemoteImageManager sharedImageManager] downloadImageWithURL:[[NSURL alloc]initWithString:self] options:(PINRemoteImageManagerDownloadOptionsNone) progressDownload:^(int64_t completedBytes, int64_t totalBytes) {
    //
    //        if( [self hasSuffix:@"gif"] ){
    //
    //            int progressInt = (int)((CGFloat)completedBytes/(CGFloat)totalBytes*100);
    //
    //            progress(progressInt-5 < 0 ? 0 : progressInt-5);
    //        }
    //
    //    } completion:^(PINRemoteImageManagerResult * _Nonnull result) {
    //
    //        if( result.image ) {
    //
    //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //                NSString *base64 = [UIImageJPEGRepresentation(result.image, 0.9) base64EncodedStringWithOptions:0];
    //                base64 = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",base64];
    //                dispatch_sync(dispatch_get_main_queue(), ^{
    //
    //                    finish(base64);
    //
    //                    [[PINCache sharedCache] setObject:base64 forKey:keyPath];
    //                });
    //            });
    //        }
    //
    //        if ( result.animatedImage ) {
    //
    //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //
    //                NSString *base64 = [result.animatedImage.data base64EncodedStringWithOptions:0];
    //                base64 = [NSString stringWithFormat:@"data:image/jpeg;base64,%@",base64];
    //                dispatch_sync(dispatch_get_main_queue(), ^{
    //
    //                    finish(base64);
    //
    //                    [[PINCache sharedCache] setObject:base64 forKey:keyPath];
    //                });
    //            });
    //        }
    //    }];
}



+(NSString *)commentFormatString:(int)comment{
    
    NSString *result = nil;
    
    if (comment < 10000) {
        
        result = [NSString stringWithFormat:@"%d", comment];
        
    }else if(comment > 10000){
        
        double doubleNum = comment /10000.0;
        
        result = [NSString stringWithFormat:@"%.1f万", doubleNum];
    }
    
    return result;
}

- (NSRange)fullRange {
    
    return (NSRange){0, self.length};
}

-(OddityChannel *)channelObj{

    return  [OddityChannel objectsInRealm:[RLMRealm OddityRealm] where:@"cname == %@",self].firstObject;
}

-(BOOL)isValidEmail {
    
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

+(NSString *)oddity_cachePathString{

    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    
    return [paths objectAtIndex:0];
}

@end
