//
//  NSString+lproj.h
//  Pods
//
//  Created by 荆文征 on 2017/6/10.
//
//

#import <Foundation/Foundation.h>

@interface NSString (lproj)

+(NSString *)oddity_usercenter_me_string;
+(NSString *)oddity_usercenter_close_string;
+(NSString *)oddity_usercenter_setting_string;
+(NSString *)oddity_usercenter_comment_string;
+(NSString *)oddity_usercenter_favorite_string;
+(NSString *)oddity_usercenter_news_string;


+(NSString *)oddity_setting_about_string;
+(NSString *)oddity_setting_delete_string;
+(NSString *)oddity_setting_font_string;
+(NSString *)oddity_setting_grade_string;
+(NSString *)oddity_setting_moon_string;
+(NSString *)oddity_setting_privacy_string;
+(NSString *)oddity_setting_push_string;

+(NSString *)oddity_setting_font_normal_string;
+(NSString *)oddity_setting_font_big_string;
+(NSString *)oddity_setting_font_bigplus_string;

+(NSString *)oddity_manage_channel_title_string;
+(NSString *)oddity_manage_channel_my_title_string;
+(NSString *)oddity_manage_channel_hot_title_string;

+(NSString *)oddity_reason_submit_button_title_string;
+(NSString *)oddity_reason_reason_desc_string;
+(NSString *)oddity_reason_reason_dislike_string;
+(NSString *)oddity_reason_reason_old_string;
+(NSString *)oddity_reason_reason_low_string;


+(NSString *)oddity_request_success_count_string;
+(NSString *)oddity_request_success_nomore_string;
+(NSString *)oddity_request_fail_data_error_string;
+(NSString *)oddity_request_fail_network_error_string;

+(NSString *)oddity_mj_refresh_header_idle_string;
+(NSString *)oddity_mj_refresh_header_pulling_string;
+(NSString *)oddity_mj_refresh_header_refreshing_string;
+(NSString *)oddity_mj_refresh_footer_refreshing_string;
+(NSString *)oddity_mj_refresh_footer_nomore_data_string;
@end
