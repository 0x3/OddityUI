//
//  NSString+lproj.m
//  Pods
//
//  Created by 荆文征 on 2017/6/10.
//
//

#import "NSBundle+Oddity.h"
#import "NSString+lproj.h"


#define LocalizedString @"OddityLocalizedString"

@implementation NSString (lproj)


+(NSString *)oddity_usercenter_me_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_usercenter_close_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_usercenter_setting_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_usercenter_comment_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_usercenter_favorite_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_usercenter_news_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}



+(NSString *)oddity_setting_about_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_delete_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_font_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_grade_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_moon_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_privacy_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_push_string{

    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}


+(NSString *)oddity_setting_font_normal_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_font_big_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_setting_font_bigplus_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}


+(NSString *)oddity_manage_channel_title_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_manage_channel_my_title_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_manage_channel_hot_title_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}


+(NSString *)oddity_reason_submit_button_title_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_reason_reason_desc_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_reason_reason_dislike_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_reason_reason_old_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_reason_reason_low_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}



+(NSString *)oddity_request_success_count_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_request_success_nomore_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_request_fail_data_error_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_request_fail_network_error_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}



+(NSString *)oddity_mj_refresh_header_idle_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_mj_refresh_header_pulling_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_mj_refresh_header_refreshing_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_mj_refresh_footer_refreshing_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}

+(NSString *)oddity_mj_refresh_footer_nomore_data_string{
    
    return NSLocalizedStringFromTableInBundle(NSStringFromSelector(_cmd), LocalizedString, NSBundle.oddity_shareBundle, nil);
}


@end
