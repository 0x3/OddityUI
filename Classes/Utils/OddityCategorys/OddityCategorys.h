//
//  OddityCategorys.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#ifndef OddityCategorys_h
#define OddityCategorys_h

#import "OddityCustomView.h"

#import <Realm/Realm.h>
#import <Masonry/Masonry.h>


#import "NSString+lproj.h"
#import "UIImage+Theme.h"
#import "UIFont+Theme.h"

#import "NSArray+Oddity.h"
#import "NSBundle+Oddity.h"
#import "NSDate+Oddity.h"
#import "NSDictionary+Oddity.h"
#import "OdditySetting+Oddity.h"
#import "RLMRealm+Oddity.h"
#import "UIAlertView+Oddity.h"
#import "UIButton+Oddity.h"
#import "UICollectionView+Oddity.h"
#import "UIGestureRecognizer+Oddity.h"
#import "UIImage+Oddity.h"
#import "UITableView+Oddity.h"
#import "UIViewController+Oddity.h"
#import "UIWindow+Oddity.h"
#import "NSString+Oddity.h"

#endif /* OddityCategorys_h */

/// 系统版本小于等于
#define ODDITY_SYSTEM_LESS_THAN(t) [[[UIDevice currentDevice] systemVersion] floatValue] < t
/// 系统版本大于或者等于
#define ODDITY_SYSTEM_GREAT_OR_EQUAL(t) [[[UIDevice currentDevice] systemVersion] floatValue] >= t
