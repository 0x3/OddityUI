//
//  OdditySetting+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <OddityOcUI/OddityOcUI.h>

@interface OdditySetting(Oddity)

@property(nonatomic,assign,readonly)BOOL isShareArray;

/**
 获取 OdditySetting 设置的 集合对象的个数 用于展示 tableview的 datasource 测试

 @return 返回个数
 */
-(NSInteger)numberOfSectionsInCollectionView;


/**
 根据 indexPath section 的index值 获取需要展示 的 个数

 @param section section
 @return 个数
 */
-(NSInteger)numberOfItemsInSection:(NSInteger)section;


/**
 根据 indexPtah 获取的OdditySeeting 对象

 @param indexPath indexPath
 @return 设置对象
 */
-(OddityShareModeObject *)objForItemAtIndexPath:(NSIndexPath *)indexPath;
@end
