//
//  OdditySetting+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//
#import "OddityShareUser.h"
#import "OdditySetting+Oddity.h"

@implementation OdditySetting(Oddity)

-(BOOL)isShareArray{
    
    return [self.shareObjectArray.firstObject isKindOfClass:[OddityShareModeObject class]];
}

-(NSInteger)numberOfSectionsInCollectionView{
    
    if (self.isShareArray) {
        
        return self.shareObjectArray.count > 0 ? 1 : 0;
    }
    
    return self.shareObjectArray.count;
}

-(NSInteger)numberOfItemsInSection:(NSInteger)section{
    
    if (self.isShareArray) {
        
        return self.shareObjectArray.count;
    }
    
    id object = [self.shareObjectArray objectAtIndex:section];
    
    if ([object isKindOfClass:[NSArray class]]) {
        
        return ((NSArray *)object).count;
    }
    
    return 0;
}

-(OddityShareModeObject *)objForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isShareArray) {
        
        return (OddityShareModeObject *)[self.shareObjectArray objectAtIndex:indexPath.row];
    }
    
    id object = [self.shareObjectArray objectAtIndex:indexPath.section];
    
    if ([object isKindOfClass:[NSArray class]]) {
        
        return (OddityShareModeObject *)[((NSArray *)object) objectAtIndex:indexPath.row];
    }
    
    return nil;
}


@end
