//
//  OddityUISetting+Theme.h
//  Pods
//
//  Created by 荆文征 on 2017/5/12.
//
//

#import <OddityOcUI/OddityOcUI.h>

@interface OddityUISetting (Theme)


/**
 是否是普通模式

 @return <#return value description#>
 */
+(BOOL)isThemeModeNormal;


/**
 搜索 StatusBar style

 @return Statusbar style
 */
+(UIStatusBarStyle)searchStatusBarStyle;
@end
