//
//  OddityUISetting+Theme.m
//  Pods
//
//  Created by 荆文征 on 2017/5/12.
//
//

#import "OddityUISetting+Theme.h"

@implementation OddityUISetting (Theme)

+(BOOL)isThemeModeNormal{

    return [OddityUISetting shareOddityUISetting].oddityThemeMode == OddityThemeModeNormal;
}

+(UIStatusBarStyle)searchStatusBarStyle{

   return [OddityUISetting isThemeModeNormal] ? UIStatusBarStyleDefault : UIStatusBarStyleLightContent;
}



@end
