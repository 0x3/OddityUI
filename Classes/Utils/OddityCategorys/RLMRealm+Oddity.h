//
//  RLMRealm+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <Realm/Realm.h>


@interface RLMRealm (Oddity)

/**
 *  Oddity 数据库
 *
 *  @return 数据库对象
 */
+(instancetype)OddityRealm;

@end
