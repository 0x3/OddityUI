//
//  RLMRealm+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "NSBundle+Oddity.h"
#import "RLMRealm+Oddity.h"

@implementation RLMRealm(Oddity)

static RLMRealmConfiguration *_oddity_static_realmconfig;

+(RLMRealmConfiguration *)OddityRealmConfiguration{

    RLMRealmConfiguration *config = [[RLMRealmConfiguration alloc] init];
    
    // Use the default directory, but replace the filename with the username
    config.fileURL = [[[config.fileURL URLByDeletingLastPathComponent] URLByAppendingPathComponent:@"OddityOcUI"] URLByAppendingPathExtension:@"realm"];
    
    config.schemaVersion = 4;
    
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
    
        
    };
    
    return config;
    
}

+(instancetype)OddityRealm{

    NSError *error;
    
    RLMRealm *realm = [RLMRealm realmWithConfiguration:[self OddityRealmConfiguration] error: &error];
    
    if (error) {
    
        NSLog(@"开启式白白白白白白白 %@",error);
    }
    
    return realm;
}

@end
