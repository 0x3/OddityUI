//
//  UIAlertView+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

typedef void(^CompleteBlock) (NSInteger buttonIndex);

@interface UIAlertView (Oddity)

// 用Block的方式回调，这时候会默认用self作为Delegate
- (void)oddity_showAlertViewWithCompleteBlock:(CompleteBlock) block;

@end
