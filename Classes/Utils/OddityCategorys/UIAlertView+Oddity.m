//
//  UIAlertView+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <objc/runtime.h>

#import "UIAlertView+Oddity.h"

@implementation UIAlertView (Oddity)

static void *key = "OddityAlertView";

- (void)oddity_showAlertViewWithCompleteBlock:(CompleteBlock)block
{
    if (block) {
        ////移除所有关联
        objc_removeAssociatedObjects(self);
        objc_setAssociatedObject(self, key, block, OBJC_ASSOCIATION_COPY);
        ////设置delegate
        self.delegate = self;
    }
    ////show
    [self show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    ///获取关联的对象，通过关键字。
    CompleteBlock block = objc_getAssociatedObject(self, key);
    if (block) {
        ///block传值
        block(buttonIndex);
    }
}

@end
