//
//  UIButton+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

typedef void (^ActionBlock)();
@interface UIButton (Block)

/**
 *  UIButton添加UIControlEvents事件的block
 *
 *  @param event 事件
 *  @param action block代码
 */
- (void) oddity_handleControlEvent:(UIControlEvents)event withBlock:(ActionBlock)action;

@end
