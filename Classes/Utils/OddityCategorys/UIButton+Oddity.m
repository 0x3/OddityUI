//
//  UIButton+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <objc/runtime.h>
#import "UIButton+Oddity.h"

@implementation UIButton (Oddity)

static char eventKey;

/**
 *  UIButton添加UIControlEvents事件的block
 *
 *  @param event 事件
 *  @param action block代码
 */
- (void) oddity_handleControlEvent:(UIControlEvents)event withBlock:(void (^)())action {
    objc_setAssociatedObject(self, &eventKey, action, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:self action:@selector(callActionBlock:) forControlEvents:event];
}

- (void)callActionBlock:(id)sender {
    ActionBlock block              = (ActionBlock)objc_getAssociatedObject(self, &eventKey);
    if (block) {
        block();
    }
}

@end
