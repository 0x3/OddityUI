//
//  UICollectionView+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

/// 存储from IndexPath
@interface UICollectionView (Oddity)


/**
 获取 UICollectionView 的 NSindexPath 的 对象

 @return Indexpath
 */
- (NSIndexPath *)oddity_fromIndexPath;


/**
 设置 UICollectionView的 FromIndex

 @param fromIndexPath NSindexPath
 */
- (void)oddity_setFromIndexPath:(NSIndexPath *)fromIndexPath;
@end
