//
//  UICollectionView+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <objc/runtime.h>
#import "UICollectionView+Oddity.h"

@implementation UICollectionView (Oddity)

static char fromIndexPathKey;

-(NSIndexPath *)oddity_fromIndexPath{
    
    return (NSIndexPath *)objc_getAssociatedObject(self, &fromIndexPathKey);
}

-(void)oddity_setFromIndexPath:(NSIndexPath *)fromIndexPath{
    
    objc_setAssociatedObject(self, &fromIndexPathKey, fromIndexPath, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
