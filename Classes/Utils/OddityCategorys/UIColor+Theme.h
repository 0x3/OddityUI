//
//  UIColor+Theme.h
//  Pods
//
//  Created by 荆文征 on 2017/5/11.
//
//

#import <UIKit/UIKit.h>



@interface UIColor (Theme)

+(UIColor *)color_t_1;
+(UIColor *)color_t_2;
+(UIColor *)color_t_3;
+(UIColor *)color_t_4;
+(UIColor *)color_t_5;
+(UIColor *)color_t_6;
+(UIColor *)color_t_7;
+(UIColor *)color_t_8;
+(UIColor *)color_t_9;
+(UIColor *)color_t_10;
+(UIColor *)color_t_11;
+(UIColor *)color_t_12;
+(UIColor *)color_t_13;
+(UIColor *)color_t_14;
+(UIColor *)color_t_15;


/**
 把颜色 对象转换为 字符串
 
 @return 转出来的字符串
 */
-(NSString *)oddity_color2String;

/**
 根据两个颜色和其中的进度获取 相对应的 过渡色
 
 @param start 开始的颜色
 @param end 结束的颜色
 @param f 进度
 @return 颜色对象
 */
+ (UIColor *)oddity_interpolateHSVColorFrom:(UIColor *)start to:(UIColor *)end withFraction:(float)f;

@end
