//
//  UIColor+Theme.m
//  Pods
//
//  Created by 荆文征 on 2017/5/11.
//
//

#import "OdditySetting.h"
#import "OddityCategorys.h"

#import "UIColor+Theme.h"

@implementation UIColor (Theme)

+(UIColor *)color_t_1{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"ffac36"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"a07436"];
    }
}


+(UIColor *)color_t_2{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"333333"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"909090"];
    }
}


+(UIColor *)color_t_3{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"999999"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"545454"];
    }
}


+(UIColor *)color_t_4{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"d1d1d1"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"444444"];
    }
}


+(UIColor *)color_t_5{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"e8e8e8"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"2a2a2a"];
    }
}


+(UIColor *)color_t_6{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"f8f8f8"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"202020"];
    }
}


+(UIColor *)color_t_7{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"ff4c4c"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"d13b3b"];
    }
}


+(UIColor *)color_t_8{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"fda062"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"985f3a"];
    }
}

+(UIColor *)color_t_9{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"ffffff"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"242424"];
    }
}


+(UIColor *)color_t_10{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"ffffff"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"969696"];
    }
}


+(UIColor *)color_t_11{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"ffffff"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"c8c8c8"];
    }
}

+(UIColor *)color_t_12{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"ffffff"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"181818"];
    }
}

+(UIColor *)color_t_13{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"101010"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"101010"];
    }
}

+(UIColor *)color_t_14{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"262626"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"262626"];
    }
}

+(UIColor *)color_t_15{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIColor oddity_colorFromHexString:@"0091fa"];
        case OddityThemeModeNight:
            return [UIColor oddity_colorFromHexString:@"1168a7"];
    }
}



-(NSString *)oddity_color2String{
    
    CGFloat r, g, b, a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    int rgb = (int) (r * 255.0f)<<16 | (int) (g * 255.0f)<<8 | (int) (b * 255.0f)<<0;
    return [NSString stringWithFormat:@"#%06x", rgb];
}

+ (instancetype)oddity_colorFromHexString:(NSString *)hexString{
    return [UIColor oddity_colorFromHexString:hexString alpha:1.0];
}

+ (instancetype)oddity_colorFromHexString:(NSString *)hexString alpha:(CGFloat)alpha{
    if (!hexString) {
        return [UIColor blackColor];
    }
    unsigned rgbValue              = 0;
    hexString                      = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    NSScanner *scanner             = [NSScanner scannerWithString:hexString];
    
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:alpha];
}

+ (UIColor *)oddity_interpolateHSVColorFrom:(UIColor *)start to:(UIColor *)end withFraction:(float)f {
    
    f                              = MAX(0, f);
    f                              = MIN(1, f);
    
    CGFloat h1,s1,v1,a1;
    [start getHue:&h1 saturation:&s1 brightness:&v1 alpha:&a1];
    
    CGFloat h2,s2,v2,a2;
    [end getHue:&h2 saturation:&s2 brightness:&v2 alpha:&a2];
    
    CGFloat h                      = h1 + (h2 - h1) * f;
    CGFloat s                      = s1 + (s2 - s1) * f;
    CGFloat v                      = v1 + (v2 - v1) * f;
    CGFloat a                      = a1 + (a2 - a1) * f;
    
    return [UIColor colorWithHue:h saturation:s brightness:v alpha:a];
}

@end
