//
//  UIFont+Theme.h
//  Pods
//
//  Created by 荆文征 on 2017/5/11.
//
//

#import <UIKit/UIKit.h>

@interface UIFont (Theme)

+(UIFont *)font_t_1;
+(UIFont *)font_t_2;
+(UIFont *)font_t_3;
+(UIFont *)font_t_4;
+(UIFont *)font_t_5;
+(UIFont *)font_t_6;
+(UIFont *)font_t_7;
+(UIFont *)font_t_8;
+(UIFont *)font_t_9;
+(UIFont *)font_t_10;
+(UIFont *)font_t_11;
+(UIFont *)font_t_12;
+(UIFont *)font_t_13;


/**
 feed流 相关推荐 相关视频 标题

 @return 字体
 */
+(UIFont *)font_t_new_title;


/**
 图文详情页标题 以及 评论页面 标题大小

 @return 字体
 */
+(UIFont *)font_t_text_detail_title;

/**
 视频详情页标题
 
 @return 字体
 */
+(UIFont *)font_t_video_detail_title;

/**
 详情页正文
 
 @return 字体
 */
+(UIFont *)font_t_detail_context;

/**
 评论内容字体
 
 @return 字体
 */
+(UIFont *)font_t_comment_context;

@end
