//
//  UIFont+Theme.m
//  Pods
//
//  Created by 荆文征 on 2017/5/11.
//
//

#import "OdditySetting.h"
#import "UIFont+Theme.h"

@implementation UIFont (Theme)

+(UIFont *)font_t_1{

    return [UIFont systemFontOfSize:23];
}


+(UIFont *)font_t_2{

    return [UIFont systemFontOfSize:19];
}


+(UIFont *)font_t_3{

    return [UIFont systemFontOfSize:18];
}


+(UIFont *)font_t_4{

    return [UIFont systemFontOfSize:16];
}


+(UIFont *)font_t_5{

    return [UIFont systemFontOfSize:15];
}


+(UIFont *)font_t_6{

    return [UIFont systemFontOfSize:14];
}


+(UIFont *)font_t_7{

    return [UIFont systemFontOfSize:12];
}


+(UIFont *)font_t_8{

    return [UIFont systemFontOfSize:11];
}


+(UIFont *)font_t_9{

    return [UIFont systemFontOfSize:10];
}


+(UIFont *)font_t_10{

    return [UIFont systemFontOfSize:20];
}


+(UIFont *)font_t_11{

    return [UIFont systemFontOfSize:21];
}


+(UIFont *)font_t_12{

    return [UIFont systemFontOfSize:25];
}


+(UIFont *)font_t_13{

    return [UIFont systemFontOfSize:27];
}


+(UIFont *)font_t_new_title{

    switch ([[OddityUISetting shareOddityUISetting] oddityFontMode]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
        case -1:
            return [self font_t_4];
#pragma clang diagnostic pop
            
        case OddityFontModeNormal:
            return [self font_t_3];
            
        case OddityFontModeBig:
            return [self font_t_2];
            
        case OddityFontModeBigPlus:
            return [self font_t_10];
    }
}

+(UIFont *)font_t_text_detail_title{

    switch ([[OddityUISetting shareOddityUISetting] oddityFontMode]) {
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
        case -1:
            return [self font_t_2];
#pragma clang diagnostic pop
        case OddityFontModeNormal:
            return [self font_t_1];
            
        case OddityFontModeBig:
            return [self font_t_12];
            
        case OddityFontModeBigPlus:
            return [self font_t_13];
    }
}

+(UIFont *)font_t_video_detail_title{

    switch ([[OddityUISetting shareOddityUISetting] oddityFontMode]) {
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
        case -1:
            return [self font_t_4];
#pragma clang diagnostic pop
            
        case OddityFontModeNormal:
            return [self font_t_3];
            
        case OddityFontModeBig:
            return [self font_t_2];
            
        case OddityFontModeBigPlus:
            return [self font_t_11];
    }
}

+(UIFont *)font_t_detail_context{

    switch ([[OddityUISetting shareOddityUISetting] oddityFontMode]) {
            
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
        case -1:
            return [self font_t_4];
#pragma clang diagnostic pop
            
        case OddityFontModeNormal:
            return [self font_t_3];
            
        case OddityFontModeBig:
            return [self font_t_10];
            
        case OddityFontModeBigPlus:
            return [self font_t_11];
    }
}


+(UIFont *)font_t_comment_context{

    switch ([[OddityUISetting shareOddityUISetting] oddityFontMode]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wswitch"
        case -1:
            return [self font_t_4];
#pragma clang diagnostic pop
            
        case OddityFontModeNormal:
            return [self font_t_3];
            
        case OddityFontModeBig:
            return [self font_t_2];
            
        case OddityFontModeBigPlus:
            return [self font_t_10];
    }
}

@end
