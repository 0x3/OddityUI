//
//  UIGestureRecognizer+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

typedef void (^NVMGestureBlock)(UIGestureRecognizer * gesture);

@interface UIGestureRecognizer (block)


/**
 UIGestureRecognizer 设置 回调 方法

 @param block 回调方法
 @return 返回 UIGestureRecognizer 对象
 */
+ (instancetype)oddity_recognizerWithHandler:(NVMGestureBlock)block;

@end
