//
//  UIGestureRecognizer+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <objc/runtime.h>
#import "UIGestureRecognizer+Oddity.h"

static const int target_key;
@implementation  UIGestureRecognizer (Oddity)

+(instancetype)oddity_recognizerWithHandler:(NVMGestureBlock)block{
    
    return [[self alloc]initWithActionBlock:block];
}

- (instancetype)initWithActionBlock:(NVMGestureBlock)block {
    self = [self init];
    if (block) {
        objc_setAssociatedObject(self, &target_key, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    [self addTarget:self action:@selector(invoke:)];
    return self;
}

- (void)invoke:(id)sender {
    NVMGestureBlock block = objc_getAssociatedObject(self, &target_key);
    if (block) {
        block(sender);
    }
}

@end

