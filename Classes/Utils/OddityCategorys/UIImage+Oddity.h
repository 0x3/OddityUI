//
//  UIImage+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Oddity)


/**
 根据名称获取 bundle内的片

 @param named 图片名称
 @return 图片对象
 */
+ (UIImage *_Nullable) oddityImage:(NSString *_Nullable)named;


/**
 根据名称获取 bundle内的图片 并按照颜色绘制

 @param named 名称
 @param color 颜色
 @return 图片
 */
+ (UIImage*_Nullable)oddityImage:(NSString*_Nullable)named byColor:(UIColor *_Nullable)color;

/**
 获取圆形头像的占位图片

 @return 图片
 */
+ (UIImage *_Nullable)oddity_shareAvatorPlaceholderImage;

/**
 主题的默认的占位图

 @return 展位图片
 */
+ (UIImage *_Nullable) oddity_theme_placeholder_image;

/**
 根据一个图片获取一个圆形的切割图片

 @param size 圆形图片的大小
 @param width 边框的宽度
 @return 图片
 */
-(UIImage *_Nullable)oddity_makeCircularImageWithSize:(CGSize)size withBorderWidth:(CGFloat)width;


/**
 创建一个图片 根据颜色 和 大小

 @param color 颜色对象
 @param size 视图大小
 @return 创建的图片
 */
+(UIImage *_Nullable)oddity_createImage:(UIColor *_Nullable)color sizes:(CGSize)size;


/**
 保存图片

 @param data 图片的 NSData
 @param completionHandler 保存是否完成
 */
+(void)oddity_savaImage:(NSData *_Nullable)data completionHandler:(void (^_Nullable)(BOOL, NSError * _Nullable,NSString * _Nullable message))completionHandler;
@end
