//
//  UIImage+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//
#import "OddityUISetting.h"
#import "UIColor+Theme.h"
#import "UIImage+Oddity.h"
#import "NSBundle+Oddity.h"

#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>

@implementation UIImage (Oddity)


+ (UIImage*)oddityImage:(NSString*)named{
    
    NSBundle *bundle               = [NSBundle oddity_shareBundle];
    NSString *path                 = [bundle pathForResource:named ofType:@"png"];
    return [UIImage imageWithContentsOfFile:path];
}

+ (UIImage*)oddityImage:(NSString*)named byColor:(UIColor *)color{
    
    NSBundle *bundle               = [NSBundle oddity_shareBundle];
    NSString *path                 = [bundle pathForResource:named ofType:@"png"];
    return [[UIImage imageWithContentsOfFile:path] oddity_imageWithColor:color];
}

+(UIImage *)oddity_shareAvatorPlaceholderImage{
    
    static UIImage *shareImage = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        shareImage = [self oddity_sharePlaceholderImage:[UIColor color_t_4] sizes:CGSizeMake(100, 100)];
        shareImage = [shareImage oddity_makeCircularImageWithSize:(CGSizeMake(38, 38)) withBorderWidth:1];
    });
    
    return shareImage;
}


+(UIImage *)oddity_theme_placeholder_image{

    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return self.oddity_theme_white_placeholder_image;
        case OddityThemeModeNight:
            return self.oddity_theme_night_placeholder_image;
    }
}



+(UIImage *)oddity_theme_white_placeholder_image{
    
    static UIImage *shareImage = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        shareImage = [self oddity_sharePlaceholderImage:[UIColor color_t_4] sizes:CGSizeMake(100, 100)];
    });
    
    return shareImage;
}

+(UIImage *)oddity_theme_night_placeholder_image{
    
    static UIImage *shareImage = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        shareImage = [self oddity_sharePlaceholderImage:[UIColor color_t_4] sizes:CGSizeMake(100, 100)];
    });
    
    return shareImage;
}

+(UIImage *)oddity_createImage:(UIColor *)color sizes:(CGSize)size{

    return [self oddity_sharePlaceholderImage:color sizes:size];
}

+(UIImage *)oddity_sharePlaceholderImage:(UIColor *)color sizes:(CGSize)size{
    
    CGRect rect                    = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);//宽高 1.0只要有值就够了
    rect.size                      = size;
    UIGraphicsBeginImageContext(rect.size); //在这个范围内开启一段上下文
    CGContextRef context           = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);//在这段上下文中获取到颜色UIColor
    CGContextFillRect(context, rect);//用这个颜色填充这个上下文
    
    UIImage *image                 = UIGraphicsGetImageFromCurrentImageContext();//从这段上下文中获取Image属性,,,结束
    UIGraphicsEndImageContext();
    
    return image;
}

-(UIImage *)oddity_makeCircularImageWithSize:(CGSize)size withBorderWidth:(CGFloat)width{
    
    // make a CGRect with the image's size
    CGRect circleRect = (CGRect) {CGPointZero, size};
    
    // begin the image context since we're not in a drawRect:
    UIGraphicsBeginImageContextWithOptions(circleRect.size, NO, 0);
    
    // create a UIBezierPath circle
    UIBezierPath *circle = [UIBezierPath bezierPathWithRoundedRect:circleRect cornerRadius:circleRect.size.width/2];
    
    // clip to the circle
    [circle addClip];
    
    [[UIColor whiteColor] set];
    [circle fill];
    
    // draw the image in the circleRect *AFTER* the context is clipped
    [self drawInRect:circleRect];
    
    // create a border (for white background pictures)
    if (width > 0) {
        circle.lineWidth = width;
        [[UIColor whiteColor] set];
        [circle stroke];
    }
    
    // get an image from the image context
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // end the image context since we're not in a drawRect:
    UIGraphicsEndImageContext();
    
    return roundedImage;
}

-(UIImage *)oddity_imageWithColor:(UIColor *)color{
    
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, self.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextClipToMask(context, rect, self.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage*newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(void)oddity_savaImage:(NSData *)data completionHandler:(void (^)(BOOL, NSError * _Nullable,NSString * _Nullable message))completionHandler{

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            
            [PHAssetCreationRequest.creationRequestForAsset addResourceWithType:(PHAssetResourceTypePhoto) data:data options:nil];
            
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            
            if (completionHandler) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSString *message = success ? @"保存成功" : @"保存失败";
                    
                    completionHandler(success,error,message);
                });
            }
        }];
        
    }else{
        
        ALAssetsLibrary *assetLibrary=[[ALAssetsLibrary alloc] init];
        
        [assetLibrary writeImageDataToSavedPhotosAlbum:data metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
            
            if (completionHandler) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSString *message = assetURL ? @"保存成功" : @"保存失败";
                    
                    completionHandler(assetURL,error,message);
                });
            }
        }];
    }
}

@end

