//
//  UIImage+Theme.h
//  Pods
//
//  Created by 荆文征 on 2017/5/11.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Theme)

// 视频播放

+(UIImage *)oddity_video_play_image;
+(UIImage *)oddity_video_pause_image;

/// 频道管理

+(UIImage *)oddity_channel_insert_image;
+(UIImage *)oddity_channel_delete_image;

/// 分享相关

+(UIImage *)oddity_share_circle_image;
+(UIImage *)oddity_share_email_image;
+(UIImage *)oddity_share_font_image;
+(UIImage *)oddity_share_link_image;
+(UIImage *)oddity_share_message_image;
+(UIImage *)oddity_share_qq_image;
+(UIImage *)oddity_share_sina_image;
+(UIImage *)oddity_share_wechat_image;
+(UIImage *)oddity_share_fontsize_image;
+(UIImage *)oddity_share_theme_image;

+(UIImage *)oddity_waitview_error_image;

+(UIImage *)oddity_mainvc_userCenter_image;
+(UIImage *)oddity_mainvc_addchannel_image;


+(UIImage *)oddity_usercenter_comment_image;
+(UIImage *)oddity_usercenter_comment_delete_image;
+(UIImage *)oddity_usercenter_favorite_image;
+(UIImage *)oddity_usercenter_news_image;
+(UIImage *)oddity_usercenter_comment_bubble_image;
+(UIImage *)oddity_usercenter_avator_placeholder_image;


+(UIImage *)oddity_setting_arrow_image;
+(UIImage *)oddity_setting_about_image;
+(UIImage *)oddity_setting_delete_image;
+(UIImage *)oddity_setting_font_image;
+(UIImage *)oddity_setting_grade_image;
+(UIImage *)oddity_setting_moon_image;
+(UIImage *)oddity_setting_privacy_image;
+(UIImage *)oddity_setting_push_image;

+(UIImage *)oddity_auth_sina_image;
+(UIImage *)oddity_auth_wechat_image;
+(UIImage *)oddity_auth_username_image;
+(UIImage *)oddity_auth_password_image;

+(UIImage *)oddity_search_search_image;

+(UIImage *)oddity_about_play_button_image;
+(UIImage *)oddity_about_icon_image;
+(UIImage *)oddity_about_lieying_image;

+(UIImage *)oddity_detail_comment_default_avator_image;

+(UIImage *)oddity_detail_comment_image;
+(UIImage *)oddity_detail_no_comment_image;

+(UIImage *)oddity_detail_collect_image;
+(UIImage *)oddity_detail_collected_image;

+(UIImage *)oddity_detail_share_image;
+(UIImage *)oddity_detail_to_post_image;

+(UIImage *)oddity_detail_up_normal_image;
+(UIImage *)oddity_detail_up_select_image;

+(UIImage *)oddity_detail_download_image;

+(UIImage *)oddity_emtpy_sofa_image;
+(UIImage *)oddity_emtpy_mycomment_image;
+(UIImage *)oddity_emtpy_collect_image;
+(UIImage *)oddity_emtpy_search_image;
+(UIImage *)oddity_emtpy_network_image;
+(UIImage *)oddity_emtpy_message_image;
@end
