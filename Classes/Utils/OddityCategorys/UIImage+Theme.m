//
//  UIImage+Theme.m
//  Pods
//
//  Created by 荆文征 on 2017/5/11.
//
//

#import "OdditySetting.h"
#import "UIImage+Theme.h"
#import "UIImage+Oddity.h"

#import "UIColor+Theme.h"

#import "OddityManagerInfoMationUtil.h"

@implementation UIImage (Theme)

//// ->>> 视频

+(UIImage *)oddity_video_play_image {

    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"video_play"];
        case OddityThemeModeNight:
            return [self oddityImage:@"paly_centre_night"];
    }
}

+(UIImage *)oddity_video_pause_image {
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"video_pause"];
        case OddityThemeModeNight:
            return [self oddityImage:@"pause_centre_night"];
    }
}

//// ->>> 频道管理

+(UIImage *)oddity_channel_delete_image{

    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"频道减少"];
        case OddityThemeModeNight:
            return [self oddityImage:@"delete_channel_night"];
    }
}

+(UIImage *)oddity_channel_insert_image{

    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"频道增加"];
        case OddityThemeModeNight:
            return [self oddityImage:@"add_channel_night"];
    }
}



//// ->>> 分享

+(UIImage *)oddity_share_circle_image{

    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"circle_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"circle_share_night"];
    }
}


+(UIImage *)oddity_share_email_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"email_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"email_share_night"];
    }
}


+(UIImage *)oddity_share_font_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"font_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"font_share_night"];
    }
}


+(UIImage *)oddity_share_link_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"link_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"link_share_night"];
    }
}

+(UIImage *)oddity_share_message_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"message_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"message_share_night"];
    }
}


+(UIImage *)oddity_share_qq_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"qq_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"qq_share_night"];
    }
}


+(UIImage *)oddity_share_sina_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"sina_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"sina_share_night"];
    }
}



+(UIImage *)oddity_share_wechat_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"wechat_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"wechat_share_night"];
    }
}

+(UIImage *)oddity_share_fontsize_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"font_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"font_share_night"];
    }
}

+(UIImage *)oddity_share_theme_image{
    
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"moon_share_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"sun_share_night"];
    }
}


+(UIImage *)oddity_waitview_error_image{

    if (OddityManagerInfoMationUtil.managerInfoMation.networkCode.integerValue == 0 ) {
        
        return [UIImage oddityImage:@"wuwangluo_day"];
    }
    
    return [UIImage oddityImage:@"wupinglun_day"];
}


+(UIImage *)oddity_usercenter_avator_placeholder_image{

    return [UIImage oddityImage:@"touxiang_day"];
}

+(UIImage *)oddity_usercenter_comment_image{

    return [UIImage oddityImage:@"usercenter_comment_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_usercenter_comment_delete_image{

    return [UIImage oddityImage:@"mycomment_delete" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_usercenter_favorite_image{

    return [UIImage oddityImage:@"favorite_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_usercenter_news_image{

    return [UIImage oddityImage:@"news_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_usercenter_comment_bubble_image{
    
    return [UIImage oddityImage:@"usercenter_comment" byColor:[UIColor color_t_2]];
}


+(UIImage *)oddity_mainvc_userCenter_image{

    return [UIImage oddityImage:@"user_day" byColor:OddityUISetting.shareOddityUISetting.mainTone];
}

+(UIImage *)oddity_mainvc_addchannel_image{
    
    return [UIImage oddityImage:@"home_add"];
}


+(UIImage *)oddity_setting_arrow_image{

    return [UIImage oddityImage:@"arrow_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_setting_about_image{

    return [UIImage oddityImage:@"about_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_setting_delete_image{

    return [UIImage oddityImage:@"delete_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_setting_font_image{

    return [UIImage oddityImage:@"font_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_setting_grade_image{

    return [UIImage oddityImage:@"grade_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_setting_moon_image{

    return [UIImage oddityImage:@"moon_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_setting_privacy_image{

    return [UIImage oddityImage:@"privacy_day" byColor:[UIColor color_t_2]];
}

+(UIImage *)oddity_setting_push_image{

    return [UIImage oddityImage:@"push_day" byColor:[UIColor color_t_2]];
}



+(UIImage *)oddity_auth_sina_image{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"sina_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"sina_night"];
    }
}

+(UIImage *)oddity_auth_wechat_image{
    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [self oddityImage:@"wechat_day"];
        case OddityThemeModeNight:
            return [self oddityImage:@"wechat_night"];
    }
}

+(UIImage *)oddity_auth_username_image{
    
    return [UIImage oddityImage:@"user_input_day" byColor:[UIColor color_t_3]];
}

+(UIImage *)oddity_auth_password_image{
    
    return [UIImage oddityImage:@"password_day" byColor:[UIColor color_t_3]];
}

+(UIImage *)oddity_search_search_image{

    switch ([OddityUISetting shareOddityUISetting].oddityThemeMode) {
        case OddityThemeModeNormal:
            return [UIImage oddityImage:@"home_search" byColor:UIColor.color_t_4];
        case OddityThemeModeNight:
            return [UIImage oddityImage:@"home_search" byColor:UIColor.color_t_2];
    }
}

+(UIImage *)oddity_about_play_button_image{

    return [UIImage oddityImage:@"paly_bottom_day" byColor:UIColor.color_t_10];
}

+(UIImage *)oddity_about_icon_image{
    
    return [self oddityImage:@"icon_day"];
}

+(UIImage *)oddity_about_lieying_image{

    return [self oddityImage:@"lieying_day"];
}

+(UIImage *)oddity_detail_comment_default_avator_image{

    return [self oddityImage:@"ic_user_comment_default"];
}

+(UIImage *)oddity_detail_comment_image{

    return [self oddityImage:@"detail_comment_day"];
}
+(UIImage *)oddity_detail_no_comment_image{

    return [self oddityImage:@"detail_sofa_day"];
}

+(UIImage *)oddity_detail_collect_image{

    return [self oddityImage:@"xiangqing_normal_shoucang"];
}
+(UIImage *)oddity_detail_collected_image{

    return [self oddityImage:@"xiangqing_selected_shoucang"];
}

+(UIImage *)oddity_detail_share_image{

    return [self oddityImage:@"share_day"];
}

+(UIImage *)oddity_detail_to_post_image{

    return [self oddityImage:@"详情页原文"];
}


+(UIImage *)oddity_detail_up_normal_image{

    return [self oddityImage:@"up_normal_comment_day"];
}

+(UIImage *)oddity_detail_up_select_image{

    return [UIImage oddityImage:@"up_select_comment_day"];
}

+(UIImage *)oddity_detail_download_image{

    return [UIImage oddityImage:@"pic_download_day"];
}


+(UIImage *)oddity_emtpy_sofa_image{

    return [UIImage oddityImage:@"qiangshafa_day" byColor:[UIColor color_t_3]];
}

+(UIImage *)oddity_emtpy_mycomment_image{
    
    return [UIImage oddityImage:@"wupinglun_day" byColor:[UIColor color_t_3]];
}

+(UIImage *)oddity_emtpy_collect_image{
    
    return [UIImage oddityImage:@"wushoucang_day" byColor:[UIColor color_t_3]];
}

+(UIImage *)oddity_emtpy_search_image{
    
    return [UIImage oddityImage:@"wusousuo_day" byColor:[UIColor color_t_3]];
}

+(UIImage *)oddity_emtpy_network_image{
    
    return [UIImage oddityImage:@"wuwangluo_day" byColor:[UIColor color_t_3]];
}

+(UIImage *)oddity_emtpy_message_image{
    
    return [UIImage oddityImage:@"wuxiaoxi_day" byColor:[UIColor color_t_3]];
}

@end
