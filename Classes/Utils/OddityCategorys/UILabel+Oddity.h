//
//  UILabel+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/7/17.
//
//

#import <UIKit/UIKit.h>

@interface UILabel (Oddity)


/**
 获取 UIlabel的行

 @return 行数
 */
-(NSInteger)getLineNumbers;

@end
