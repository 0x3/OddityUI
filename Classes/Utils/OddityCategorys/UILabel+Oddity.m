//
//  UILabel+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/7/17.
//
//

#import "UILabel+Oddity.h"
#import <CoreText/CoreText.h>

@implementation UILabel (Oddity)

- (NSArray *)getLinesArrayInLabel {
    
    CGRect rect = self.frame;
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,rect.size.width,100000));
    
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    
    NSArray *lines = (__bridge NSArray *)CTFrameGetLines(frame);
    
    return lines;
}

-(NSInteger)getLineNumbers{
    
    return self.getLinesArrayInLabel.count;
}


- (NSArray *)getLinesArrayOfStringInLabel {
    
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    
    for (id line in self.getLinesArrayInLabel) {
        
        CTLineRef lineRef = (__bridge CTLineRef )line;
        CFRange lineRange = CTLineGetStringRange(lineRef);
        NSRange range = NSMakeRange(lineRange.location, lineRange.length);
        
        NSString *lineString = [self.attributedText.string substringWithRange:range];
        [linesArray addObject:lineString];
    }
    
    return (NSArray *)linesArray;
}

@end
