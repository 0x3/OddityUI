//
//  UITableView+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>
#import "OddityTableViewEmtpyBackView.h"

@interface UITableView (Oddity)

/**
 刷新 indexPath  cell 其实最后做成了

 @param indexPaths IndexPaths 集合
 */
-(void)oddity_reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths;


/**
 在表格中剥夺出新闻数组

 @return 返回新闻数组
 */
-(NSArray *)oddity_depriveArray;


/**
 制作 占位视图
 
 @param count 数据的数量
 @param mode 占位类型
 @return 个数
 */
-(NSInteger)makeEmtpyView:(NSInteger)count viewMode:(OddityTableViewEmtpyBackViewMode)mode;


/**
 制作 占位视图

 @param count 数据的数量
 @param mode 占位类型
 @param offset 偏移量
 @return 个数
 */
-(NSInteger)makeEmtpyView:(NSInteger)count viewMode:(OddityTableViewEmtpyBackViewMode)mode offset:(CGFloat)offset;
@end
