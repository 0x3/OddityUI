//
//  UITableView+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "UITableView+Oddity.h"
#import "OddityCustomTableViewCell.h"

@implementation UITableView(Oddity)

-(void)oddity_reloadRowsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths{
    
    for (NSIndexPath * indexPath in indexPaths) {
        
        
        UITableViewCell *currentCell = [self cellForRowAtIndexPath:indexPath];
        
        if (!currentCell || ![currentCell isKindOfClass:[OddityNewBaseTableViewCell class]]) {continue; }
        
//        [((OddityNewBaseTableViewCell *)currentCell) refreshTitleColor];
        
        //        UITableViewCell *newCell = [self.dataSource tableView:self cellForRowAtIndexPath:indexPath];
        //
        //        newCell.frame = currentCell.frame;
        //
        //        [self oddity_replaceCell:currentCell withNewCell:newCell];
    }
}

-(void)oddity_replaceCell:(UITableViewCell *)old withNewCell:(UITableViewCell *)new{
    
    UIView *superViewForOld = old.superview;
    if (!superViewForOld) { return; }
    [old removeFromSuperview];
    [superViewForOld addSubview:new];
}


-(NSArray *)oddity_depriveArray{

    NSMutableArray *array = [NSMutableArray array];
    
    for (id obj in self.visibleCells) {
        
        if (![obj isKindOfClass:[OddityNewBaseTableViewCell class]]) {
            continue;
        }
        
        [array addObject:((OddityNewBaseTableViewCell *)obj).object];
    }
    
    return array;
}

-(NSInteger)makeEmtpyView:(NSInteger)count viewMode:(OddityTableViewEmtpyBackViewMode)mode{

    return [self makeEmtpyView:count viewMode:mode offset:-32];
}

-(NSInteger)makeEmtpyView:(NSInteger)count viewMode:(OddityTableViewEmtpyBackViewMode)mode offset:(CGFloat)offset{
    
    if (count <= 0) {
        
        self.backgroundView = [[[OddityTableViewEmtpyBackView alloc] initWithMode:(mode) offset:offset] refresh];
    }else{
        
        self.backgroundView = nil;
    }
    
    return count;
}

@end
