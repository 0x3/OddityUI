//
//  UIViewController+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@class OddityWaitView;

@interface UIViewController (oddityCurrent)


/**
 获取当前的 UIViewController

 @return UIViewController
 */
+(UIViewController *)oddityCurrentViewController;


/**
 显示 UIViewController 根据 current View Controller
 */
-(void)presentViewControllerWithCurrent;

@property (nonatomic, strong) OddityWaitView *waitView;


/**
 隐藏等待视图
 */
-(void)oddity_hiddenWaitView;


/**
 显示等待视图

 @param top 上方的 top 距离
 */
-(void)oddity_showWaitView:(CGFloat)top;
@end
