//
//  UIViewController+Oddity.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCustomView.h"
#import "UIWindow+Oddity.h"
#import <Masonry/Masonry.h>
#import "UIViewController+Oddity.h"



static const void *waitViewKey = &waitViewKey;

@implementation UIViewController(oddityCurrent)

@dynamic waitView;

+(UIViewController *)oddityCurrentViewController{
    
    return [[[UIApplication sharedApplication]keyWindow]oddity_VisibleViewController];
}

-(void)presentViewControllerWithCurrent{
    
    [[UIViewController oddityCurrentViewController] presentViewController:self animated:true completion:nil];
}


-(OddityWaitView *)waitView{
    
    //    _waitView = [[WaitView alloc]init];
    //
    OddityWaitView *waitView = (OddityWaitView *)objc_getAssociatedObject(self, waitViewKey);
    
    if(waitView) { return waitView; }
    
    OddityWaitView *wwaitView = [[OddityWaitView alloc]init];
    
    
    objc_setAssociatedObject(self, waitViewKey, wwaitView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    return wwaitView;
}



-(void)oddity_showWaitView:(CGFloat)top{
    
    [self.waitView setHidden:false];
    
    [self.view addSubview:self.waitView];
    
    [self.waitView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        
        make.top.equalTo(self.view).offset(top);
    }];
    
    [self.view layoutIfNeeded];
    
    [self.waitView goNormaleStyle];
}

-(void)oddity_hiddenWaitView{
    
    [self.waitView setHidden:true];
    [self.waitView removeFromSuperview];
}


@end
