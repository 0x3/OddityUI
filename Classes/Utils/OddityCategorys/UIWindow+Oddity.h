//
//  UIWindow+Oddity.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@interface UIWindow (Oddity)


/**
 获取当前的 UIViewController

 @return UIViewController
 */
- (UIViewController *)oddity_VisibleViewController;

@end
