//
//  OddityBigButton.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityBigButton.h"

@implementation OddityBigButton

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    
    CGRect btnBounds = self.bounds;
    
    btnBounds = CGRectInset(btnBounds, -10, -10);
    
    return CGRectContainsPoint(btnBounds, point);
}

@end
