//
//  OddityChooseFontView.m
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//

#import "OddityCategorys.h"

#import "OddityChooseFontView.h"

@interface OddityChooseFontSubView : UIView

@property(nonatomic,assign) OddityFontMode mode;

@property(nonatomic,strong) UILabel *label;
@property(nonatomic,assign) BOOL selected;

-(instancetype)initWith:(OddityFontMode)mode;

@end




@implementation OddityChooseFontSubView

-(void)dealloc{

    [NSNotificationCenter.defaultCenter removeObserver:self];
}

-(instancetype)initWith:(OddityFontMode)mode{

    self = [super init];
    
    if (self) {
        
        self.backgroundColor = [UIColor color_t_9];
        
        _label = [[UILabel alloc] init];
        _label.textColor = [UIColor color_t_2];
        [self addSubview:_label];
        _label.font = [UIFont font_t_4];
        [_label mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.center.mas_equalTo(self);
        }];
        
        self.clipsToBounds = true;
        
        switch (mode) {
            case OddityFontModeBig:
                _label.text = NSString.oddity_setting_font_big_string;
                break;
            case OddityFontModeBigPlus:
                _label.text = NSString.oddity_setting_font_bigplus_string;
                break;
            case OddityFontModeNormal:
                _label.text = NSString.oddity_setting_font_normal_string;
                break;
        }
        
        self.mode = mode;
        self.selected = OddityUISetting.shareOddityUISetting.oddityFontMode == mode;
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchActionMethod)]];
        
        __weak __typeof__(self) weakSelf = self;
        [NSNotificationCenter.defaultCenter addObserverForName:@"oddityFontModeChange" object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification * _Nonnull note) {
            
            weakSelf.selected = OddityUISetting.shareOddityUISetting.oddityFontMode == self.mode;
        }];
    }
    
    return self;
}

-(void)touchActionMethod{

    OddityUISetting.shareOddityUISetting.oddityFontMode = self.mode;
}

-(void)setSelected:(BOOL)selected{
    
    [self layoutIfNeeded];
    
    _label.textColor = OddityUISetting.shareOddityUISetting.oddityFontMode == self.mode ? [UIColor color_t_7] : [UIColor color_t_2];
    self.backgroundColor = OddityUISetting.shareOddityUISetting.oddityFontMode == self.mode ? [UIColor color_t_5] : [[UIColor color_t_9] colorWithAlphaComponent:0.7];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    self.backgroundColor = [[UIColor color_t_5] colorWithAlphaComponent:0.7];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

//    [self setSelected:false];
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    self.backgroundColor = [[UIColor color_t_9] colorWithAlphaComponent:0.7];
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    switch (self.mode) {
        case OddityFontModeBigPlus:{
        
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(4.0, 4.0)];
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bounds;
            maskLayer.path  = maskPath.CGPath;
            self.layer.mask = maskLayer;
        }
            break;
        case OddityFontModeNormal:{
            
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(4.0, 4.0)];
            
            CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
            maskLayer.frame = self.bounds;
            maskLayer.path  = maskPath.CGPath;
            self.layer.mask = maskLayer;
        }
            break;
        default:
            break;
    }
}

@end



@interface OddityChooseFontView()

// 普通
@property(nonatomic,strong)OddityChooseFontSubView *normalButton;

// 大
@property(nonatomic,strong)OddityChooseFontSubView *bigerButton;

// 超大
@property(nonatomic,strong)OddityChooseFontSubView *bigestButton;

@end

@implementation OddityChooseFontView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor color_t_6];
        
        _normalButton = [[OddityChooseFontSubView alloc] initWith: OddityFontModeNormal];
        [self addSubview:_normalButton];
        [_normalButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.with.left.mas_equalTo(1);
            make.bottom.mas_equalTo(-1);
            make.width.mas_equalTo((154-4)/3);
        }];
        
        _bigerButton = [[OddityChooseFontSubView alloc] initWith: OddityFontModeBig];
        [self addSubview:_bigerButton];
        [_bigerButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(1);
            make.left.mas_equalTo(_normalButton.mas_right).offset(1);
            make.bottom.mas_equalTo(-1);
            make.width.mas_equalTo((154-4)/3);
        }];
        
        _bigestButton = [[OddityChooseFontSubView alloc] initWith: OddityFontModeBigPlus];
        [self addSubview:_bigestButton];
        [_bigestButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(1);
            make.bottom.mas_equalTo(-1);
            make.left.mas_equalTo(_bigerButton.mas_right).offset(1);
            make.width.mas_equalTo((154-4)/3);
        }];
    }
    return self;
}

@end
