//
//  OddityCustomRefreshFooterView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityCustomRefreshFooterView.h"

static NSString * const loadMoreAnimationKey = @"loadMoreRotation";

static const CGFloat fontSize = 16;
static const CGFloat loadMoreHeight = 40;

@interface OddityCustomRefreshFooterView()
//@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) CABasicAnimation *animation;

@end

@implementation OddityCustomRefreshFooterView

-(UILabel *)label{
    
    if (!_label) {
        
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:fontSize];
        label.textColor = [UIColor color_t_3];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        _label = label;
    }
    
    _label.textColor =  [UIColor color_t_3];
    
    return _label;
}

-(UIImageView *)imageView{
    
    if (!_imageView) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage oddityImage:@"加载更多"]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:imageView];
        _imageView = imageView;
    }
    
    _imageView.image = [UIImage oddityImage:@"加载更多" byColor: [UIColor color_t_3]];
    
    return _imageView;
}

-(CABasicAnimation *)animation{
    
    if (!_animation) {
        
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
        animation.repeatCount = FLT_MAX;
        animation.duration = 0.5;
        animation.cumulative = YES;
        animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(3.14 , 0, 0, 1)];
        _animation = animation;
    }
    
    return _animation;
}

#pragma mark - 重写方法
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare
{
    [super prepare];
    
    self.mj_h = loadMoreHeight;
}

#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews {
    
    [_label mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self);
        make.centerX.equalTo(self).offset(16);
    }];
    
    [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(_label);
        make.right.equalTo(_label.mas_left).offset(-16);
    }];
    
    [super placeSubviews];
}

#pragma mark 监听控件的刷新状态
- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    switch (state) {
        case MJRefreshStateIdle:
            self.label.text = @"";
            self.imageView.hidden = YES;
            break;
        case MJRefreshStateRefreshing:
            self.label.text = NSString.oddity_mj_refresh_footer_refreshing_string;
            self.imageView.hidden = NO;
            [self.imageView.layer addAnimation:self.animation forKey:loadMoreAnimationKey];
            break;
        case MJRefreshStateNoMoreData:
            self.label.text = NSString.oddity_mj_refresh_footer_nomore_data_string;
            self.imageView.hidden = YES;
            break;
        default:
            break;
    }
}

@end

