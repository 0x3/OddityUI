//
//  OddityCustomRefreshHeaderView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityCustomRefreshHeaderView.h"

@interface OddityCustomRefreshHeaderView ()

@property (nonatomic, weak) UILabel *label;
@property (nonatomic, weak) UIActivityIndicatorView *loading;

@end

@implementation OddityCustomRefreshHeaderView

-(UIActivityIndicatorView *)loading{
    
    if (!_loading) {
        
        // loading
        UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self addSubview:loading];
        
        _loading = loading;
    }
    
    _loading.color =  [UIColor color_t_3];
    
    return _loading;
}

-(UILabel *)label{
    
    if (!_label) {
        
        // 添加loading
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:12];
        label.textColor = [UIColor color_t_3];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        _label = label;
    }
    
    _label.textColor =  [UIColor color_t_3];
    
    return _label;
}

#pragma mark - 重写方法
#pragma mark 在这里做一些初始化配置（比如添加子控件）
- (void)prepare {
    
    [super prepare];
    
    // 设置控件高度
    self.mj_h = 54.0f;
}


#pragma mark 在这里设置子控件的位置和尺寸
- (void)placeSubviews {
    
    [_loading mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@2);
        make.centerX.equalTo(self);
    }];
    
    [_label mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.equalTo(self);
        make.top.equalTo(_loading.mas_bottom).offset(10);
    }];
    
    [super placeSubviews];
}

#pragma mark 监听控件的刷新状态
- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState;
    
    switch (state) {
        case MJRefreshStateIdle:
            [self.loading stopAnimating];
            
            self.label.text = NSString.oddity_mj_refresh_header_idle_string;
            break;
        case MJRefreshStatePulling:
            [self.loading startAnimating];
            self.label.text = NSString.oddity_mj_refresh_header_pulling_string;
            break;
        case MJRefreshStateRefreshing:
            self.label.text = NSString.oddity_mj_refresh_header_refreshing_string;
            [self.loading startAnimating];
            break;
        default:
            break;
    }
}


@end
