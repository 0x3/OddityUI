//
//  OddityChannelManagerCollectionReusableView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <UIKit/UIKit.h>

/**
 频道 管理 UICollectionView 头部视图
 */
@interface OddityChannelManagerCollectionReusableView : UICollectionReusableView

@property(nonatomic,strong) UILabel *descLabel;

-(void)setDescStr:(NSString *)desc;
@end


/**
 频道 管理 UICollectionView 尾部视图
 */
@interface OddityChannelManagerCollectionFReusableView : UICollectionReusableView

@end
