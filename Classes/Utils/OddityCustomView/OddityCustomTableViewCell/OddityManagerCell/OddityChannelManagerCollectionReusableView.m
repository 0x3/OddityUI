//
//  OddityChannelManagerCollectionReusableView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OdditySetting.h"

#import "UIColor+Theme.h"
#import "UIFont+Theme.h"
#import <Masonry/Masonry.h>

#import "OddityChannelManagerCollectionReusableView.h"

@interface OddityChannelManagerCollectionReusableView()

@property (nonatomic,strong) UIView *backView;
@property (nonatomic,strong) UIView *blockView;


@property (nonatomic,strong) UIView *toptBorder;

@property (nonatomic,strong) UIView *topBorder;
@property (nonatomic,strong) UIView *bottomBorder;


@end

@implementation OddityChannelManagerCollectionReusableView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        _backView                     = [[UIView alloc]init];
        [self addSubview:_backView];
        _backView.backgroundColor     = [UIColor color_t_6];
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.bottom.equalTo(self.mas_bottom);
            make.height.equalTo(@38);
        }];

        _toptBorder                   = [[UIView alloc]init];
        _toptBorder.backgroundColor   = [UIColor color_t_5];
        [self addSubview:_toptBorder];
        [_toptBorder mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.top.equalTo(self.mas_top);
            make.height.equalTo(@0.5);
        }];
        
        _bottomBorder                 = [[UIView alloc]init];
        _bottomBorder.backgroundColor = [UIColor color_t_5];
        [_backView addSubview:_bottomBorder];
        [_bottomBorder mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_backView.mas_left);
            make.right.equalTo(_backView.mas_right);
            make.bottom.equalTo(_backView.mas_bottom);
            make.height.equalTo(@0.5);
        }];
        
        
        _blockView                    = [[UIView alloc]init];
        _blockView.backgroundColor    = [OddityUISetting shareOddityUISetting].mainTone;
        [self addSubview:_blockView];
        [_blockView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_backView);
            make.centerY.equalTo(_backView);
            make.width.equalTo(@4);
            make.height.equalTo(@15);
        }];
        
        _descLabel                    = [[UILabel alloc]init];
        _descLabel.font               = [UIFont font_t_6];
        _descLabel.textColor          = [UIColor color_t_3];
        [self addSubview:_descLabel];
        [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_blockView.mas_right).offset(8);
            make.centerY.equalTo(_backView);
        }];
    }
    return self;
}

-(void)setDescStr:(NSString *)desc{
    
    _descLabel.text = desc;
    
}

@end

@interface OddityChannelManagerCollectionFReusableView ()

@property (nonatomic,strong) UIView *topBorder;

@end

@implementation OddityChannelManagerCollectionFReusableView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor          = [UIColor color_t_5];
        
        _topBorder                    = [[UIView alloc]init];
        _topBorder.backgroundColor    = [UIColor color_t_5];
        [self addSubview:_topBorder];
        [_topBorder mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.right.with.top.equalTo(self);
            make.height.equalTo(@0.5);
        }];
    }
    
    return self;
}

@end
