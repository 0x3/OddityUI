//
//  OddityChannelManagerCollectionViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <UIKit/UIKit.h>


@interface OddityChannelManagerCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *managerImageView;

-(void)setChannel:(OddityChannel *)channel;

@end
