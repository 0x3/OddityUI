//
//  OddityChannelManagerCollectionViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityChannelManagerCollectionViewCell.h"

@interface OddityChannelManagerCollectionViewCell()

@property (nonatomic,strong) UIView *backView;
@property(nonatomic,strong) UILabel *tagNameLabel;

@end

@implementation OddityChannelManagerCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if ( self ) {
        
        self.backgroundColor         = [UIColor clearColor];
        
        _backView                    = [[UIView alloc]init];
        [self.contentView addSubview:_backView];
        _backView.layer.cornerRadius = 7;
        _backView.backgroundColor    = [UIColor whiteColor];
        _backView.layer.borderWidth  = 1;
        _backView.clipsToBounds      = true;
        _backView.layer.borderColor  = [[UIColor color_t_5] CGColor];
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.mas_left);
            make.right.equalTo(self.mas_right);
            make.bottom.equalTo(self.mas_bottom);
            make.height.equalTo(@35);
        }];
        
        _tagNameLabel                = [[UILabel alloc]init];
        _tagNameLabel.font           = [UIFont font_t_5];
        [_backView addSubview:_tagNameLabel];
        [_tagNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(_backView);
        }];
        
        _managerImageView            = [[UIImageView alloc]init];
        [self.contentView addSubview:_managerImageView];
        [_managerImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(_backView.mas_top);
            make.right.equalTo(self.mas_right);
            make.width.equalTo(@18);
            make.height.equalTo(@18);
        }];
    }
    
    return self;
}


-(void)setChannel:(OddityChannel *)channel{
    
    _tagNameLabel.text        = channel.cname;
    
//        _tagNameLabel.text        = [NSString stringWithFormat:@"%@-%d",channel.cname,channel.orderindex];
    _tagNameLabel.textColor   = [channel getTitleColor];
    _backView.backgroundColor = [channel getBackColor];
    _managerImageView.image   = [channel getManagerImage];
    [_managerImageView setHidden:[channel getManagerImageHidden]];
}

@end


