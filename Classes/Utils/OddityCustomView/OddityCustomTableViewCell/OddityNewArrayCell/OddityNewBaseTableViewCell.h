//
//  OddityNewBaseTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//


#import "OddityNew.h"
#import "OddityNew+Interface.h"

#import "UIImage+Oddity.h"
#import "UIColor+Theme.h"

#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

#import <UIKit/UIKit.h>

@class OddityNew,OddityBigButton,OddityUILabelPadding,OddityThemeCImageView;

@interface OddityNewBaseTableViewCell : UITableViewCell

/**
 标题Label 用于展示 新闻标题
 */
@property (nonatomic,strong)UILabel *titleLabel;

/// 新闻类型标签
@property (nonatomic,strong)UILabel *tagLabel;


/**
 来源名称 记录新闻来源的名称
 */
@property (nonatomic,strong)UILabel *pubNameLabel;


/**
 来源icon标签 记录新闻来源的头像
 */
@property (nonatomic,strong) OddityThemeCImageView *pubIconImageView;

/**
 不喜欢按钮
 */
@property (nonatomic,strong) OddityBigButton *dislikeButton;

/**
 新闻对象
 */
@property (nonatomic,strong) OddityNew *object;

/**
 评论个数 如果 评论数 <= 0 隐藏该空间 否则展示格式化后的评论数字符串
 */
@property (nonatomic,strong) UILabel *commentLabel;


/**
 边框视图
 */
@property (nonatomic,strong)UIView *borderView;




/**
 记录用户的点击开始位置
 */
@property (nonatomic,assign)CGPoint beginPoint;

/**
 记录用户的点击结束位置
 */
@property (nonatomic,assign)CGPoint endPoint;


-(void)setConfig:(OddityNew *)n;

-(instancetype)hiddenCloseButton;


/**
 隐藏icon
 */
-(void)hiddenIconView;
/**
 制作底部的高度
 */
-(void)bottomMakeHeight;

-(void)toDelete;
-(void)toReset;

@end
