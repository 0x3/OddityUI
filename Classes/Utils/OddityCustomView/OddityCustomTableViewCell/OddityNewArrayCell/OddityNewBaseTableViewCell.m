//
//  OddityNewBaseTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCategorys.h"
#import "OddityNewBaseTableViewCell.h"

@interface OddityNewBaseTableViewCell ()

/// 新闻评论数目Label
@property (nonatomic,strong)UILabel *jcommentLabel;

@end

@implementation OddityNewBaseTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    
    if (selected) {
        
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_9]]; // set color here
    }else{
    
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_5]]; // set color here
    }
    
    [self setSelectedBackgroundView:selectedBackgroundView];
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        _borderView = [[UIView alloc] init];
        [self.contentView addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.mas_equalTo(2);
            make.left.with.right.with.bottom.mas_equalTo(self.contentView);
        }];
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.numberOfLines = 2;
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(15);
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
        }];
        
        _pubIconImageView = [[OddityThemeCImageView alloc]init];
        _pubIconImageView.clipsToBounds = true;
        _pubIconImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_pubIconImageView];
        [_pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(_titleLabel);
            make.top.equalTo(_titleLabel.mas_bottom).offset(8);
            make.bottom.equalTo(_borderView.mas_top).offset(-8);
            make.size.mas_equalTo(CGSizeMake(18, 18));
        }];
        
        _pubNameLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_pubNameLabel];
        [_pubNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(_pubIconImageView);
            make.left.equalTo(_pubIconImageView.mas_right).offset(5);
        }];
        
        _dislikeButton = [[OddityBigButton alloc] init];
        [self.contentView addSubview:_dislikeButton];
        [_dislikeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_titleLabel);
            make.centerY.equalTo(_pubNameLabel);
        }];
        
        _commentLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_commentLabel];
        [_commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(_dislikeButton);
            make.right.equalTo(_dislikeButton.mas_left).offset(-6);
        }];
        
        _jcommentLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_jcommentLabel];
        [_jcommentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.right.equalTo(_titleLabel);
            make.centerY.equalTo(_pubNameLabel);
        }];
        
        _tagLabel = [[UILabel alloc]init];
        _tagLabel.layer.cornerRadius = 2;
        _tagLabel.clipsToBounds = true;
        _tagLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_tagLabel];
        [_tagLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(_pubNameLabel);
            make.left.equalTo(self.pubNameLabel.mas_right).offset(5);
            make.width.mas_equalTo(28);
            make.height.mas_equalTo(14);
        }];
    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    
    self.backgroundColor = [UIColor color_t_9];
    _borderView.backgroundColor = [UIColor color_t_6];
    
    _object = n;
    
    _titleLabel.attributedText = n.titleAttributedString;
    
    _pubNameLabel.attributedText = n.pNameAttributedString;
    
    
    [_commentLabel setHidden:false];
    [_dislikeButton setHidden:false];
    [_jcommentLabel setHidden:true];
    
    [_tagLabel setHidden:[n isHiddenTagView]];
    [_commentLabel setHidden:[n isHiddenComment]];
    
    TagObject *to = [n gTagObject];
    _tagLabel.attributedText = n.tagAttributedString;
    _tagLabel.backgroundColor = [[to borderAndTextColor] colorWithAlphaComponent:0.12];
    
    _commentLabel.attributedText = n.commentAttributedString;
    _jcommentLabel.attributedText = n.commentAttributedString;
    
    [_dislikeButton setImage:[UIImage oddityImage:@"不感兴趣叉号" byColor:[UIColor color_t_3]] forState:UIControlStateNormal];
    
    UIImage *placeholderImage = [UIImage.oddity_theme_placeholder_image oddity_makeCircularImageWithSize: CGSizeMake(18, 18) withBorderWidth:0];
    
    __weak __typeof__(self) weakSelf = self;
    [self.pubIconImageView pin_setImageFromURL:[n pIconImageUrl] placeholderImage: placeholderImage completion:^(PINRemoteImageManagerResult * _Nonnull result) {
        
        weakSelf.pubIconImageView.image = [result.image oddity_makeCircularImageWithSize:(CGSizeMake(18, 18)) withBorderWidth:0];
    }];
    
    [_pubNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_lessThanOrEqualTo(n.pnameRightSpaceMinValue);
    }];
}

-(void)bottomMakeHeight{

    [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(0.5);
        make.left.with.right.with.bottom.mas_equalTo(self.contentView);
    }];
    
    self.backgroundColor = [UIColor color_t_6];
    self.borderView.backgroundColor = [UIColor color_t_5];
    
    [self.contentView layoutIfNeeded];
}

-(void)hiddenIconView{

    self.pubIconImageView.hidden = true;
    
    [_pubNameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.titleLabel);
    }];
}

-(instancetype)hiddenCloseButton{
    
    [_jcommentLabel setHidden:_commentLabel.hidden];
    
    [_commentLabel setHidden:true];
    [_dislikeButton setHidden:true];
    
    
    return self;
}

-(void)toDelete{
    
    NSRange rang = [self.titleLabel.attributedText.string fullRange];
    
    NSMutableAttributedString* strikeThroughText = [self.titleLabel.attributedText mutableCopy];
    
    [strikeThroughText addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlineStyleSingle) range:rang];
    [strikeThroughText addAttribute:NSStrikethroughColorAttributeName value:[UIColor redColor] range:rang];
    
    self.titleLabel.attributedText = nil;
    CATransition *transition = [CATransition new];
    transition.type = kCATransitionFromLeft;
    transition.duration = 0.5f;
    self.titleLabel.attributedText = strikeThroughText;
    [self.titleLabel.layer addAnimation:transition forKey:@"transition"];
}

-(void)toReset{
    
    NSRange rang = [self.titleLabel.attributedText.string fullRange];
    
    NSMutableAttributedString* strikeThroughText = [self.titleLabel.attributedText mutableCopy];
    
    [strikeThroughText removeAttribute:NSStrikethroughStyleAttributeName  range:rang];
    [strikeThroughText removeAttribute:NSStrikethroughColorAttributeName  range:rang];
    
    self.titleLabel.attributedText = nil;
    CATransition *transition = [CATransition new];
    transition.type = kCATransitionFromLeft;
    transition.duration = 0.5f;
    self.titleLabel.attributedText = strikeThroughText;
    [self.titleLabel.layer addAnimation:transition forKey:@"transition"];
}

@end



@interface OddityNewBaseTableViewCell(privateCat)

@end

@implementation OddityNewBaseTableViewCell (privateCat)

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    
    _beginPoint = [[[touches allObjects] firstObject] locationInView:nil];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesEnded:touches withEvent:event];
    
    _endPoint = [[[touches allObjects] firstObject] locationInView:nil];
}




@end
