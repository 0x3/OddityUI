//
//  OddityCoverImageTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityNewCoverTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityCoverImageTableViewCell ()

/// 边框视图
@property (nonatomic,strong)OddityThemeImageView *firstImageView;

@end

@implementation OddityCoverImageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.backgroundColor = [UIColor redColor];
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_firstImageView];
        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(12);
            make.right.equalTo(self.titleLabel);
            make.left.equalTo(self.titleLabel);
            
            make.height.equalTo(self.firstImageView.mas_width).multipliedBy((CGFloat)197/351);
        }];
        
        [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(self.titleLabel);
            make.top.equalTo(self.firstImageView.mas_bottom).offset(8);
            make.bottom.equalTo(self.borderView.mas_top).offset(-8);
            make.size.mas_equalTo(CGSizeMake(18, 18));
        }];
        
    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    [super setConfig:n];
    [_firstImageView pin_setImageFromURL:[n bigImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}


@end



@interface OddityCoverAdsImageTableViewCell ()

/// 边框视图
@property (nonatomic,strong)OddityThemeImageView *firstImageView;

@end

@implementation OddityCoverAdsImageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.backgroundColor = [UIColor redColor];
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_firstImageView];
        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(12);
            make.right.equalTo(self.titleLabel);
            make.left.equalTo(self.titleLabel);
            
            make.height.equalTo(self.firstImageView.mas_width).multipliedBy((CGFloat)10/19);
        }];
        
        [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(self.titleLabel);
            make.top.equalTo(self.firstImageView.mas_bottom).offset(8);
            make.bottom.equalTo(self.borderView.mas_top).offset(-8);
            make.size.mas_equalTo(CGSizeMake(18, 18));
        }];
        
    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    [super setConfig:n];
    [_firstImageView pin_setImageFromURL:[n bigImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}

-(void)makeFullMode{
    
    [_firstImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
    }];
}

@end
