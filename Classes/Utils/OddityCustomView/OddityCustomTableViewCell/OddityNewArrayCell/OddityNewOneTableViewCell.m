//
//  OddityNewOneTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//
#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityNewOneTableViewCell.h"
#import "UILabel+Oddity.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityNewOneTableViewCell ()

/// 边框视图
@property (nonatomic,strong)OddityThemeImageView *firstImageView;

@end

@implementation OddityNewOneTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.backgroundColor = [UIColor redColor];
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_firstImageView];

        if (ODDITY_SYSTEM_LESS_THAN(9)) {
            
            self.titleLabel.numberOfLines = 2;
        }else{
        
            self.titleLabel.numberOfLines = 3;
        }
        

        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.bottom.equalTo(self.contentView).offset(-12);
            
            make.width.equalTo(self.contentView.mas_width).multipliedBy((CGFloat)1/3);
            make.height.equalTo(_firstImageView.mas_width).multipliedBy((CGFloat)78/118);
        }];
        
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_firstImageView.mas_top);
            make.left.equalTo(self.contentView).offset(12);
            make.right.equalTo(_firstImageView.mas_left).offset(-12);
        }];

    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    
    [super setConfig:n];
    
     [_firstImageView pin_setImageFromURL:[n firstImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
    
    
    if (ODDITY_SYSTEM_GREAT_OR_EQUAL(9)) {
    
        if (self.titleLabel.getLineNumbers > 2) {
            
            [self MakeLineNumberGreatThanTwo];
            
        }else{
            
            [self MakeLineNumberLessThanThree];
        }
        
//        [self.contentView layoutIfNeeded];
    }
}

/**
 配置当前视图的 模式为 行数 大雨晾行的
 */
-(void)MakeLineNumberGreatThanTwo{

    [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        //            make.bottom.equalTo(self.contentView).offset(-12);
        
        make.width.equalTo(self.contentView.mas_width).multipliedBy((CGFloat)1/3);
        make.height.equalTo(_firstImageView.mas_width).multipliedBy((CGFloat)78/118);
    }];
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(12);
        make.left.equalTo(self.contentView).offset(12);
        make.right.equalTo(_firstImageView.mas_left).offset(-12);
    }];
    
    [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.titleLabel);
        make.bottom.equalTo(self.contentView).offset(-8);
        make.top.equalTo(self.firstImageView.mas_bottom).offset(8);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    
    [self.dislikeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-12);
        make.centerY.equalTo(self.pubNameLabel);
    }];
}


/**
 配置当前视图的 模式为 行数 小雨三行的
 */
-(void)MakeLineNumberLessThanThree{
    
    [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.bottom.equalTo(self.contentView).offset(-12);
        
        make.width.equalTo(self.contentView.mas_width).multipliedBy((CGFloat)1/3);
        make.height.equalTo(_firstImageView.mas_width).multipliedBy((CGFloat)78/118);
    }];
    
    [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_firstImageView.mas_top);
        make.left.equalTo(self.contentView).offset(12);
        make.right.equalTo(_firstImageView.mas_left).offset(-12);
    }];
    
    [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.titleLabel);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(8);
        make.size.mas_equalTo(CGSizeMake(18, 18));
    }];
    
    [self.dislikeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_firstImageView.mas_left).offset(-12);
        make.centerY.equalTo(self.pubNameLabel);
    }];
}

@end



@interface OddityNewOneAdsTableViewCell ()

/// 边框视图
@property (nonatomic,strong)OddityThemeImageView *firstImageView;

@end

@implementation OddityNewOneAdsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_firstImageView];
        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.bottom.equalTo(self.borderView.mas_top).offset(-12);
            
            make.width.equalTo(self.contentView.mas_width).multipliedBy((CGFloat)1/3);
            make.height.equalTo(_firstImageView.mas_width).multipliedBy((CGFloat)9/16);
        }];
        
        self.titleLabel.numberOfLines = 2;
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_firstImageView.mas_top);
            make.left.equalTo(self.contentView).offset(12);
            make.right.equalTo(_firstImageView.mas_left).offset(-12);
        }];
        
        [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(self.titleLabel);
            make.bottom.equalTo(_firstImageView);
            make.size.mas_equalTo(CGSizeMake(18, 18));
        }];
        
        [self.dislikeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_firstImageView.mas_left).offset(-12);
            make.centerY.equalTo(self.pubNameLabel);
        }];
    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    [super setConfig:n];
    [_firstImageView pin_setImageFromURL:[n firstImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}

@end
