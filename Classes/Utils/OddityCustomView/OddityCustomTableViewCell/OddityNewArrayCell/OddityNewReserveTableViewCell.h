//
//  OddityNewReserveTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>


/**
 在内容为空的时候 展示出来 好看的 UIView
 */
@interface OddityNewReserveTableViewCell : UITableViewCell

@end
