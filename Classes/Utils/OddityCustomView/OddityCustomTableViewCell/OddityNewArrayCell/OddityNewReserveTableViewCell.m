//
//  OddityNewReserveTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityNewReserveTableViewCell.h"

@interface OddityNewReserveTableViewCell()<CAAnimationDelegate>

@property (nonatomic,strong)UIView *topTextView;
@property (nonatomic,strong)UIView *bottomTextView;

@property (nonatomic,strong)UIView *coverImageView;

@property (nonatomic,strong)UIView *borderView;

@property (nonatomic, strong) CAGradientLayer *gradientLayer;
@property (nonatomic, assign) CGFloat flickeringGap; // default is 0.05. Max is 1.0. Min is 0.0
@property (nonatomic, assign) CGFloat flickeringAnimationDuration; // default is 2.0

@end

@implementation OddityNewReserveTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor color_t_9];
        
        _borderView = [[UIView alloc] init];
        _borderView.backgroundColor = [UIColor color_t_6];
        [self.contentView addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@0.5);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];
        
        _coverImageView = [[UIView alloc] init];
        _topTextView = [[UIView alloc] init];
        _bottomTextView = [[UIView alloc] init];
        
        self.topTextView.backgroundColor = [UIColor color_t_5];
        self.bottomTextView.backgroundColor = [UIColor color_t_5];
        self.coverImageView.backgroundColor = [UIColor color_t_5];
        
        [self.contentView addSubview: _coverImageView];
        
        [_coverImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self.contentView).offset(-15);
            make.right.equalTo(self.contentView).offset(-12);
            make.top.equalTo(self.contentView).offset(15);
            make.width.equalTo(@115);
            make.height.equalTo(@77);
        }];
        
        [self.contentView addSubview: _topTextView];
        [_topTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.coverImageView).offset(8);
            make.left.equalTo(self.contentView).offset(12);
            make.right.equalTo(self.coverImageView.mas_left).offset(-24);
            make.height.equalTo(@10);
        }];
        
        [self.contentView addSubview: _bottomTextView];
        [_bottomTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.topTextView.mas_bottom).offset(8);
            make.left.equalTo(self.topTextView);
            
            make.height.equalTo(@10);
            make.width.equalTo(self.topTextView).multipliedBy(0.5);
        }];
        
        [self setUp];
    }
    return self;
}


- (void)setUp {
    _flickeringGap               = 0.15f;
    _flickeringAnimationDuration = 1.0f;
    
    [self reloadData];
}

-(void)layoutSubviews{

    [super layoutSubviews];
    
    self.gradientLayer.frame = self.bounds;
}

- (void)reloadData{
    
    // 创建CAGradientLayer
    self.gradientLayer = [CAGradientLayer layer];
    
    // 设置梯度颜色
    self.gradientLayer.colors = @[(__bridge id)[[UIColor color_t_5] colorWithAlphaComponent:0.5].CGColor,
                                  (__bridge id)[UIColor color_t_5].CGColor,
                                  (__bridge id)[[UIColor clearColor] colorWithAlphaComponent:0.5].CGColor];
    // 设置梯度颜色的位置
    self.gradientLayer.locations = @[@(0), @(_flickeringGap), @(_flickeringGap*2)];
    
    // 这是颜色渐变的方向
    self.gradientLayer.startPoint = CGPointMake(0, 0);
    self.gradientLayer.endPoint = CGPointMake(0.7, 0.2);
    
    // 设置为mask，iOS8之后，也可以设置self.maskView
    self.layer.mask = self.gradientLayer;
    
    // 做动画
    [self doAnimation];
}

- (void)doAnimation {
    [self.gradientLayer removeAnimationForKey:@"slide"];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"locations"];
    animation.fromValue = @[@(0), @(_flickeringGap), @(_flickeringGap*2)];
    animation.toValue   = @[@(1-_flickeringGap*2), @(1-_flickeringGap), @(1)];
    animation.duration  = _flickeringAnimationDuration;
    animation.removedOnCompletion = YES;
    animation.delegate = self;
    [self.gradientLayer addAnimation:animation forKey:@"slide"];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    if (flag) {
        [self doAnimation];
    }
}

@end
