//
//  OddityNewSpecialTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityNewSpecialTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityNewSpecialTableViewCell ()

@property (nonatomic,strong)UIView *backView;
/// 边框视图
@property (nonatomic,strong)OddityThemeImageView *firstImageView;

@end

@implementation OddityNewSpecialTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _backView = [[UIView alloc]init];
        [self.contentView addSubview:_backView];
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(7);
            make.right.equalTo(self.contentView).offset(-12);
            make.left.equalTo(self.contentView).offset(12);
            
            make.height.equalTo(_backView.mas_width).multipliedBy((CGFloat)85/351);
        }];
        
        self.pubIconImageView.hidden = true;
        
        [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
        }];
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_backView addSubview:_firstImageView];
        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_backView);
            make.left.equalTo(_backView);
            make.bottom.equalTo(_backView);
            make.right.equalTo(_backView);
            
        }];
        
        
        [self.tagLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(self.titleLabel);
            make.top.equalTo(_backView.mas_bottom).offset(8);
            make.bottom.equalTo(self.borderView.mas_top).offset(-8);
            
            make.width.mas_equalTo(28);
            make.height.mas_equalTo(14);
        }];
        
    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    [super setConfig:n];
    self.pubNameLabel.hidden = true;
    [_firstImageView pin_setImageFromURL:[n firstImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}

@end
