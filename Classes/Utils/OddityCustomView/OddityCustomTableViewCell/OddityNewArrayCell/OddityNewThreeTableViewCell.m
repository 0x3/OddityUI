//
//  OddityNewThreeTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityNewThreeTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

#define OddityThreeImageWidth (CGRectGetWidth(UIScreen.mainScreen.bounds)-24-2*3)/3;

@interface OddityNewThreeTableViewCell ()

@property (nonatomic,strong)UIView *backView;
/// 边框视图
@property (nonatomic,strong)OddityThemeImageView *firstImageView;
@property (nonatomic,strong)OddityThemeImageView *secondImageView;
@property (nonatomic,strong)OddityThemeImageView *thirdImageView;

@end

@implementation OddityNewThreeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        CGFloat width = (CGRectGetWidth(UIScreen.mainScreen.bounds)-24-2*3)/3;
        
        _backView = [[UIView alloc]init];
        [self.contentView addSubview:_backView];
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(7);
            make.right.equalTo(self.contentView).offset(-12);
            make.left.equalTo(self.contentView).offset(12);
            
        }];
        
        
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_backView addSubview:_firstImageView];
        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_backView);
            make.left.equalTo(_backView);
            make.bottom.equalTo(_backView);
            
            make.width.mas_equalTo(width);
            
            make.width.equalTo(_firstImageView.mas_height).multipliedBy((CGFloat)3/2);
            
        }];
        
        
        _secondImageView = [[OddityThemeImageView alloc]init];
        [_secondImageView setPin_updateWithProgress:YES];
        _secondImageView.clipsToBounds = true;
        _secondImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_backView addSubview:_secondImageView];
        [_secondImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_backView);
            make.left.equalTo(_firstImageView.mas_right).offset(3);
            
            make.width.mas_equalTo(width);
            make.height.equalTo(_firstImageView.mas_height);
        }];
        
        
        _thirdImageView = [[OddityThemeImageView alloc]init];
        [_thirdImageView setPin_updateWithProgress:YES];
        _thirdImageView.clipsToBounds = true;
        _thirdImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_backView addSubview:_thirdImageView];
        [_thirdImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_backView);
            make.left.equalTo(_secondImageView.mas_right).offset(3);
            
            make.width.mas_equalTo(width);
            make.height.equalTo(_firstImageView.mas_height);
        }];
        
        
        [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(self.titleLabel);
            make.top.equalTo(self.backView.mas_bottom).offset(8);
            make.bottom.equalTo(self.borderView.mas_top).offset(-8);
            make.size.mas_equalTo(CGSizeMake(18, 18));
        }];
        
    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    [super setConfig:n];
    [_firstImageView pin_setImageFromURL:[n firstImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
    [_secondImageView pin_setImageFromURL:[n secondImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
    [_thirdImageView pin_setImageFromURL:[n thirdImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}

@end
