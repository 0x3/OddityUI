//
//  OddityNewTwoTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityNewTwoTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityNewTwoTableViewCell ()

@property (nonatomic,strong)UIView *backView;
/// 边框视图
@property (nonatomic,strong)OddityThemeImageView *firstImageView;
@property (nonatomic,strong)OddityThemeImageView *secondImageView;

@end

@implementation OddityNewTwoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _backView = [[UIView alloc]init];
        [self.contentView addSubview:_backView];
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(7);
            make.right.equalTo(self.contentView).offset(-12);
            make.left.equalTo(self.contentView).offset(12);
            
            make.height.equalTo(@183);
        }];
        
        
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_backView addSubview:_firstImageView];
        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_backView);
            make.left.equalTo(_backView);
            make.bottom.equalTo(_backView);
            
            make.width.equalTo(_backView).multipliedBy((CGFloat)1/2).offset(-1);
        }];
        
        
        _secondImageView = [[OddityThemeImageView alloc]init];
        [_secondImageView setPin_updateWithProgress:YES];
        _secondImageView.clipsToBounds = true;
        _secondImageView.contentMode = UIViewContentModeScaleAspectFill;
        [_backView addSubview:_secondImageView];
        [_secondImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_backView);
            make.left.equalTo(_firstImageView.mas_right).offset(1);
            make.bottom.equalTo(_backView);
            
            make.width.equalTo(_backView).multipliedBy((CGFloat)1/2);
        }];
        
        
        [self.pubNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_backView.mas_bottom).offset(8);
            make.left.equalTo(self.contentView).offset(12);
            make.bottom.equalTo(self.borderView.mas_top).offset(-17);
        }];
        
    }
    return self;
}

-(void)setConfig:(OddityNew *)n{
    [super setConfig:n];
    [_firstImageView pin_setImageFromURL:[n firstImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
    [_secondImageView pin_setImageFromURL:[n secondImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}

@end
