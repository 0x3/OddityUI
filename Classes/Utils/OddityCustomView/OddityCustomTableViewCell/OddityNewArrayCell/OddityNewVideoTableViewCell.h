//
//  OddityNewVideoTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>
#import "OddityNewBaseTableViewCell.h"

@class OddityThemeImageView;

@interface OddityNewVideoTableViewCell : OddityNewBaseTableViewCell

@property (nonatomic,strong)OddityBigButton *moreButton;

@property (nonatomic,strong)UIButton *playButton;

@property (nonatomic,strong)OddityThemeImageView *firstImageView;


-(void)playVideoGogo:(NSString *)cname;

-(void)setConfig:(OddityNew *)n;

-(void)makeFullMode;
@end
