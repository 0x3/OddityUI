//
//  OddityNewVideoTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityPlayerView.h"
#import "OddityCategorys.h"
#import "OddityNewVideoTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityNewVideoTableViewCell()

@property (nonatomic,strong)OddityUILabelPadding *timeLabel;
@property (nonatomic,strong)UIView * bottomView;

@end

@implementation OddityNewVideoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        
        _firstImageView = [[OddityThemeImageView alloc]init];
        [_firstImageView setPin_updateWithProgress:YES];
        _firstImageView.clipsToBounds = true;
        _firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_firstImageView];
        [_firstImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.titleLabel.mas_bottom).offset(14);
            make.left.equalTo(self.contentView).offset(12);
            make.right.equalTo(self.contentView).offset(-12);
            make.height.equalTo(_firstImageView.mas_width).multipliedBy((CGFloat)211/375);
        }];
        
        _playButton = [[UIButton alloc]init];
        [self.contentView addSubview:_playButton];
        [_playButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.equalTo(self.firstImageView);
        }];
        
        _timeLabel = [[OddityUILabelPadding alloc]init];
        _timeLabel.edgeInsets = UIEdgeInsetsMake(4, 8, 4, 8);
        _timeLabel.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
        [self.contentView addSubview:_timeLabel];
        [_timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self.firstImageView);
            make.right.equalTo(self.firstImageView);
        }];
        
        _bottomView = [[UIView alloc]init];
        [self.contentView addSubview:_bottomView];
        [_bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.bottom.equalTo(self.contentView);
            make.height.equalTo(@(36));
            make.top.equalTo(self.firstImageView.mas_bottom);
        }];
        
        if ( [OdditySetting shareOdditySetting].isDontWantToShare) { /// 不愿意分享
            
            [self.bottomView addSubview:self.commentLabel];
            [self.commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.right.equalTo(self.bottomView).offset(-12);
                make.centerY.equalTo(self.bottomView);
            }];
            
        }else{ /// 愿意分享
            
            _moreButton = [[OddityBigButton alloc]init];
            _moreButton.tintColor = [UIColor color_t_3];
            [self.bottomView addSubview:_moreButton];
            [_moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.right.equalTo(@(-12));
                make.centerY.equalTo(self.bottomView);
            }];
            
            [self.bottomView addSubview:self.commentLabel];
            [self.commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.right.equalTo(self.moreButton.mas_left).offset(-12);
                make.centerY.equalTo(self.bottomView);
            }];
        }
        
        

        [self.pubIconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.size.mas_equalTo(CGSizeMake(18, 18));
            make.left.equalTo(@(12));
            make.centerY.equalTo(self.bottomView);
        }];
        
        [self.bottomView addSubview:self.pubNameLabel];
        [self.pubNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.pubIconImageView.mas_right).offset(5);
            make.centerY.equalTo(self.bottomView);
        }];
    }
    return self;
}

-(void)playVideoGogo:(NSString *)cname{
    
    OddityPlayerView *playView = [OddityPlayerView sharePlayerView];
    
    OddityPlayerObject *playObject = [[OddityPlayerObject alloc] initWith:self.object.nid VideoUrl:self.object.videourl Title:self.object.title Objtect:self.object];
    [[OddityPlayerView sharePlayerView] playWithPlayerObject:playObject];
    
    [playView toState:(OddityPlayViewStateNormal) inView:self.firstImageView];
}

-(void)setConfig:(OddityNew *)n{
    [super setConfig:n];
    
    self.dislikeButton.hidden = true;
    self.pubNameLabel.hidden = false;
    
    [_playButton setImage:[UIImage oddity_video_play_image] forState:(UIControlStateNormal)];
    [_moreButton setImage:[UIImage oddityImage:@"video_share_black" byColor:[UIColor color_t_3]] forState:(UIControlStateNormal)];
    
    [_firstImageView pin_setImageFromURL:[n videoImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
    
    _timeLabel.hidden = n.isHiddenTimeLabel;
    _timeLabel.attributedText = n.timeAttributedString;
}

-(void)makeFullMode{
    
    [_firstImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
    }];
}

@end
