//
//  OddityRefreshTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "UIColor+Theme.h"

#import "OddityModels.h"

#import "OddityCategorys.h"

#import "OddityRefreshTableViewCell.h"

@interface OddityRefreshTableViewCell()

@property(nonatomic,strong) UILabel *label;

@end

@implementation OddityRefreshTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _label = [[UILabel alloc]init];
        [self.contentView addSubview:_label];
        
        _label.text = @"上次看到这里，点击刷新";
        _label.font = [UIFont systemFontOfSize:14];
        
        _label.textColor = [OddityUISetting shareOddityUISetting].mainTone;
        
        [_label mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.equalTo(self.contentView);
            make.top.equalTo(@8);
            make.top.equalTo(@8);
        }];
        
    }
    return self;
}


@end
