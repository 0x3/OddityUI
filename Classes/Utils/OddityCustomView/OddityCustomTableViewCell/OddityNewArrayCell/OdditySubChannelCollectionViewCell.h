//
//  OdditySubChannelCollectionViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import <UIKit/UIKit.h>

@interface OdditySubChannelCollectionCell : UICollectionViewCell

@property UILabel *label;

@end
