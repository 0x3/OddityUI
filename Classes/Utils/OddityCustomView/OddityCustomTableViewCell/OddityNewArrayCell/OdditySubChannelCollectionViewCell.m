//
//  OdditySubChannelCollectionViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OdditySubChannelCollectionViewCell.h"

@implementation OdditySubChannelCollectionCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if ( self ) {
        
        _label = [[UILabel alloc] init];
        [self.contentView addSubview:_label];
        [_label mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.equalTo(self.contentView);
        }];
        
        self.label.textColor = [UIColor color_t_3];
        self.contentView.backgroundColor = [UIColor color_t_9];
        
        self.contentView.layer.borderColor = [UIColor color_t_5].CGColor;
        
        self.contentView.layer.borderWidth = 0.5;
        self.contentView.layer.cornerRadius = 2;
    }
    
    return self;
}

-(void)setSelected:(BOOL)selected{
    
    [super setSelected:selected];
    
    if (selected) {
        
        self.label.textColor = [UIColor color_t_10];
        self.contentView.backgroundColor = [UIColor color_t_4];
        self.contentView.layer.borderColor = [UIColor color_t_5].CGColor;
    }else{
        
        self.label.textColor = [UIColor color_t_3];
        self.contentView.backgroundColor = [UIColor color_t_9];
        self.contentView.layer.borderColor = [UIColor color_t_5].CGColor;
    }
    
}

@end
