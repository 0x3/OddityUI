//
//  OddityAboutNewImageTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@class OddityNewContentAbout;


#import "OddityAboutNewTextTableViewCell.h"

@interface OddityAboutNewImageTableViewCell : OddityAboutNewBaseViewCell

-(void)setAboutNew:(OddityNewContentAbout *)about isVideo:(BOOL)isv;

@end
