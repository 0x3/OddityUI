//
//  OddityAboutNewImageTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "UIFont+Theme.h"
#import "OddityCategorys.h"
#import "OddityAboutNewImageTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityAboutNewImageTableViewCell ()

@property(nonatomic,strong) OddityThemeImageView *coverImage;

/// 新闻类型标签
@property (nonatomic,strong)UILabel *tagLabel;

@property(nonatomic,strong) UILabel *timeLabel;
@property(nonatomic,strong) UIImageView *playButton;
@end

@implementation OddityAboutNewImageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _coverImage = [[OddityThemeImageView alloc] init];
        [_coverImage setPin_updateWithProgress:YES];
        _coverImage.clipsToBounds = true;
        _coverImage.backgroundColor = [UIColor redColor];
        _coverImage.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_coverImage];
        [_coverImage mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(@16);
            make.right.equalTo(@-18);
            
            make.width.equalTo(self.contentView.mas_width).multipliedBy((CGFloat)1/3);
            
            make.bottom.equalTo(@-16);
            
        }];
        
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_coverImage);
            make.right.equalTo(self.coverImage.mas_left).offset(-15);
            make.left.equalTo(@18);
        }];
        
        [self.pnameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.bottom.equalTo(_coverImage);
            make.left.equalTo(@18);
            make.right.mas_lessThanOrEqualTo(self.coverImage.mas_left).offset(-26);
        }];
        
        _tagLabel = [[UILabel alloc]init];
        _tagLabel.layer.cornerRadius = 2;
        [self.contentView addSubview:_tagLabel];
        
        _tagLabel.font = [UIFont systemFontOfSize:10];
        
        _tagLabel.text = @"广告";
        _tagLabel.textAlignment = NSTextAlignmentCenter;
        _tagLabel.font = [UIFont font_t_8];
        _tagLabel.textColor = [UIColor color_t_3];
        _tagLabel.backgroundColor = [[UIColor color_t_3] colorWithAlphaComponent:0.12];
        
        [_tagLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self.pnameLabel);
            make.left.equalTo(self.pnameLabel.mas_right).offset(8);
            make.size.mas_equalTo(CGSizeMake(28, 14));
        }];
        
        
        _timeLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_timeLabel];
        [_timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.mas_equalTo(self.coverImage).offset(-3);
            make.right.mas_equalTo(self.coverImage).offset(-3);
        }];
        
        _playButton = [[UIImageView alloc] init];
        [self.contentView addSubview:_playButton];
        
    }
    return self;
}

-(void)setAboutNew:(OddityNewContentAbout *)about isVideo:(BOOL)isv{
    
    [super setAboutNew:about];
    
    _tagLabel.hidden = !about.isAdsAbout;
    
    [_coverImage pin_setImageFromURL:[about coverImageUrl] placeholderImage:UIImage.oddity_theme_placeholder_image];
    
    if (about.isAdsAbout) {
        
        [_coverImage mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(_coverImage.mas_width).multipliedBy((CGFloat)9/16);
        }];
        
        [self.pnameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.right.mas_lessThanOrEqualTo(self.coverImage.mas_left).offset(-46);
        }];
    }else{
        
        [_coverImage mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(_coverImage.mas_width).multipliedBy((CGFloat)78/118);
        }];
        
    
        [self.pnameLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.right.mas_lessThanOrEqualTo(self.coverImage.mas_left).offset(-16);
        }];
        
        [_coverImage mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@77);
        }];
    }
    
    self.timeLabel.hidden = !isv || about.isAdsAbout;
    self.playButton.hidden = !isv || about.isAdsAbout;
    
    if (isv && !about.isAdsAbout) {
        
        self.timeLabel.hidden = about.isHiddenTimeLabel;
        self.timeLabel.attributedText = about.timeAttributedString;
        self.playButton.image = UIImage.oddity_about_play_button_image;
        
        if (about.isHiddenTimeLabel) {
            
            [_playButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(self.timeLabel);
                make.right.mas_equalTo(self.coverImage).offset(-3);
            }];
        }else{
            
            [_playButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(self.timeLabel);
                make.right.mas_equalTo(self.timeLabel.mas_left).offset(-2);
            }];
        }
    }
    
    [self.contentView layoutIfNeeded];
}

@end
