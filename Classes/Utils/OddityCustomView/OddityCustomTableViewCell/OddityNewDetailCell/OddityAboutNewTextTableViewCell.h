//
//  OddityAboutNewTextTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@interface OddityAboutNewBaseViewCell : UITableViewCell


@property (nonatomic,assign)CGPoint beginPoint;
@property (nonatomic,assign)CGPoint endPoint;


@property(nonatomic,strong) UIView *borderView;

@property(nonatomic,strong) UILabel *titleLabel;

@property(nonatomic,strong) UILabel *pnameLabel;

-(void)setAboutNew:(OddityNewContentAbout *)about;

@end


@interface OddityAboutNewTextTableViewCell : OddityAboutNewBaseViewCell

-(void)setAboutNew:(OddityNewContentAbout *)about;

@end
