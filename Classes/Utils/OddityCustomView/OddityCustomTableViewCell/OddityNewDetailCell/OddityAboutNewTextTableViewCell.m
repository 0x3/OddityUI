//
//  OddityAboutNewTextTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityAboutNewTextTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityAboutNewBaseViewCell ()


@end

@implementation OddityAboutNewBaseViewCell


-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    
    _beginPoint = [[[touches allObjects] firstObject] locationInView:nil];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesEnded:touches withEvent:event];
    
    _endPoint = [[[touches allObjects] firstObject] locationInView:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    
    if (selected) {
        
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_9]]; // set color here
    }else{
        
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_5]]; // set color here
    }
    
    [self setSelectedBackgroundView:selectedBackgroundView];
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        

        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 2;
        [self.contentView addSubview:_titleLabel];
        
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(@16);
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
        }];
        
        _pnameLabel = [[UILabel alloc] init];
        _pnameLabel.numberOfLines = 1;
        [self.contentView addSubview:_pnameLabel];
        
        [_pnameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_titleLabel.mas_bottom).offset(14);
            make.left.equalTo(@18);
            make.bottom.equalTo(@-16);
        }];
        
        _borderView = [[UIView alloc]init];
        [self.contentView addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(@0);
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
            make.height.equalTo(@0.5);
        }];
    }
    return self;
}

-(void)setAboutNew:(OddityNewContentAbout *)about{
    
    self.contentView.backgroundColor = [UIColor color_t_6];
    
    _borderView.backgroundColor = [UIColor color_t_5];
    
    self.titleLabel.attributedText = about.titleAttributedString;
    self.pnameLabel.attributedText = about.pNameAttributedString;
}

@end


@implementation OddityAboutNewTextTableViewCell

-(void)setAboutNew:(OddityNewContentAbout *)about{
    
    [super setAboutNew:about];
}

@end
