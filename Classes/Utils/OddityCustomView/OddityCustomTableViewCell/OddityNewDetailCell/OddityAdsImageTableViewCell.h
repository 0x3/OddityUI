//
//  OddityAdsImageTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>


@class OddityNew;

@interface OddityAdsImageTableViewCell : UITableViewCell

@property (nonatomic,assign)CGPoint beginPoint;
@property (nonatomic,assign)CGPoint endPoint;

-(void)setNewObject:(OddityNew *)new;
@end
