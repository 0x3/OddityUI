//
//  OddityAdsImageTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityAdsImageTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityAdsImageTableViewCell()

@property(nonatomic,strong) UILabel *tagLabel;
@property(nonatomic,strong) UILabel *titleLabel;

@property(nonatomic,strong) OddityThemeImageView *adsImageView;
@property(nonatomic,strong) UIView *adsBackView;


@end

@implementation OddityAdsImageTableViewCell

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesBegan:touches withEvent:event];
    
    _beginPoint = [[[touches allObjects] firstObject] locationInView:nil];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesEnded:touches withEvent:event];
    
    _endPoint = [[[touches allObjects] firstObject] locationInView:nil];
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _adsBackView = [[UIView alloc]init];
        _adsBackView.backgroundColor = [UIColor color_t_12];
        [self.contentView addSubview:_adsBackView];
        [_adsBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(@20);
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
            make.bottom.equalTo(@-34);
        }];
        
        _adsImageView = [[OddityThemeImageView alloc] init];
        _adsImageView.clipsToBounds = true;
        _adsImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.adsBackView addSubview:_adsImageView];
        [_adsImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.with.left.with.right.mas_equalTo(0);
            make.width.equalTo(_adsImageView.mas_height).multipliedBy((CGFloat)19/(CGFloat)10);
        }];
        
        _tagLabel = [[UILabel alloc] init];
        [self.adsBackView addSubview:_tagLabel];
        _tagLabel.font = [UIFont systemFontOfSize:10];
        _tagLabel.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.4];
        _tagLabel.layer.cornerRadius = 7;
        _tagLabel.clipsToBounds = true;
        _tagLabel.text = @"广告";
        _tagLabel.textAlignment = NSTextAlignmentCenter;
        
        [_tagLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.width.equalTo(@29);
            make.height.equalTo(@14);
            
            make.top.equalTo(@15);
            make.right.equalTo(@-15);
        }];
        
        _titleLabel = [[UILabel alloc] init];
        [self.adsBackView addSubview:_titleLabel];
        _titleLabel.numberOfLines = 2;
        
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(@10);
            make.right.equalTo(@-10);
            make.bottom.equalTo(@-8);
            
            make.top.equalTo(_adsImageView.mas_bottom).offset(8);
        }];
        
    }
    return self;
}

-(void)setNewObject:(OddityNew *)new{
    
    _tagLabel.textColor = [UIColor color_t_10];
    self.contentView.backgroundColor = [UIColor color_t_6];
    
    _adsBackView.backgroundColor = [UIColor color_t_12];
    
    _titleLabel.attributedText = new.adsTitleAttributedString;
    [_adsImageView pin_setImageFromURL:[new secondImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}

@end

