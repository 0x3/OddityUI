//
//  OddityCommentTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@class OddityNewContentComment,OddityAnimationNumberView,OddityBigButton;

@protocol OddityCommentTableViewCellDelegate <NSObject>

- (void)clickCommentButton:(OddityNewContentComment *)comment animate:(OddityAnimationNumberView *)view button:(OddityBigButton *)button;

@end

@interface OddityCommentTableViewCell : UITableViewCell

@property (weak, nonatomic) id<OddityCommentTableViewCellDelegate> delegate;

@property(nonatomic,strong) OddityNewContentComment *comm;

-(void)setNewContentComment:(OddityNewContentComment *)ncc;
@end
