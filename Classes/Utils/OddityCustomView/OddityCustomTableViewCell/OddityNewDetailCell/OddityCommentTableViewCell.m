//
//  OddityCommentTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityCommentTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityCommentTableViewCell ()

@property (nonatomic,strong)OddityThemeCImageView *avatorImageView;
@property (nonatomic,strong)OddityBigButton *commentButton;

@property (nonatomic,strong)UILabel *unameLabel;
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *commentLabel;

@property (nonatomic,strong)UIView *borderView;

@property (nonatomic,strong) OddityAnimationNumberView *commentLabelView;

@end

@implementation OddityCommentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _borderView = [[UIView alloc]init];
        [self.contentView addSubview:_borderView];
        
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@0.5);
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
            make.bottom.equalTo(@0);
        }];
        
        _avatorImageView = [[OddityThemeCImageView alloc] init];
        [_avatorImageView setPin_updateWithProgress:YES];
        _avatorImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_avatorImageView];
        [_avatorImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(@21);
            make.left.equalTo(@18);
            
            make.width.equalTo(@38);
            make.height.equalTo(@38);
        }];
        
        _unameLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_unameLabel];
        [_unameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.width.lessThanOrEqualTo(@150);
            make.top.equalTo(_avatorImageView);
            make.left.equalTo(_avatorImageView.mas_right).offset(12);
        }];
        
        
        
        _commentButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
        [self.contentView addSubview:_commentButton];
        [_commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.right.equalTo(@-12);
            make.centerY.equalTo(_unameLabel);
        }];
        
        _commentLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_commentLabel];
        [_commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
                        make.right.equalTo(_commentButton.mas_left).offset(-4);
            
//            make.right.equalTo(_commentButton.mas_right);
            make.bottom.equalTo(_commentButton).offset(1);
        }];
        
        _commentLabelView = [[OddityAnimationNumberView alloc] initWithFrame:(CGRectMake(0, 0, 300, 20))];
        _commentLabelView.numberFont = UIFont.font_t_7;
        _commentLabelView.tag = 10085;
        [self.contentView addSubview:_commentLabelView];
        [_commentLabelView layoutIfNeeded];
        [_commentLabelView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.right.bottom.left.mas_equalTo(_commentLabel);
        }];
        
        _timeLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_timeLabel];
        [_timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_unameLabel.mas_bottom).offset(8);
            make.left.equalTo(_avatorImageView.mas_right).offset(12);
        }];
        
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentLabel];
        [_contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_timeLabel.mas_bottom).offset(12);
            make.left.equalTo(_timeLabel);
            make.right.equalTo(@-18);
            make.bottom.equalTo(_borderView.mas_top).offset(-21);
        }];
        
    }
    return self;
}

-(void)setNewContentComment:(OddityNewContentComment *)ncc{
    
    _comm = ncc;
    
    self.borderView.backgroundColor = [UIColor color_t_5];
    self.contentView.backgroundColor = [UIColor color_t_6];
    
    self.timeLabel.attributedText = ncc.timeAttributedString;
    self.unameLabel.attributedText = ncc.uNameAttributedString;
    self.contentLabel.attributedText = ncc.contentAttributedString;
    self.commentLabel.attributedText = ncc.commentAttributedString;
    
    self.commentLabel.hidden = true;
    self.commentLabelView.hidden = ncc.isHiddenCommentLabel;
    
    self.commentLabelView.numberColor = UIColor.color_t_3;
    self.commentLabelView.numberFont = UIFont.font_t_7;
    
    [self.commentLabelView setCurrentNumber:ncc.commentAttributedString.string];
    
    _commentButton.tintColor = ncc.commentButtonTintColor;
    [_commentButton setImage:ncc.commentButtonImage forState:(UIControlStateNormal)];
    
    __weak typeof(self) weakSelf = self;
    [self.avatorImageView pin_setImageFromURL:[ncc avatorImageUrl] placeholderImage:UIImage.oddity_detail_comment_default_avator_image completion:^(PINRemoteImageManagerResult * _Nonnull result) {
        
        weakSelf.avatorImageView.image = [(result.image ?: UIImage.oddity_detail_comment_default_avator_image) oddity_makeCircularImageWithSize:(CGSizeMake(38, 38)) withBorderWidth:0];
    }];
    
    for (id tag in self.commentButton.allTargets) {
        [self.commentButton removeTarget:tag action:nil forControlEvents:(UIControlEventAllEvents)];
    }
    
    [self.commentButton addTarget:self action:@selector(clickCommentButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
}

-(void)clickCommentButtonAction{

    if (self.delegate && [self.delegate respondsToSelector:@selector(clickCommentButton:animate:button:)]) {
        
        [self.delegate clickCommentButton:_comm animate:_commentLabelView button:_commentButton];
    }
}

@end
