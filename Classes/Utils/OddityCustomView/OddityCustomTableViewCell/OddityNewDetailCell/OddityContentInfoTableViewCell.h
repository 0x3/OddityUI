//
//  OddityContentInfoTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@class OddityNewContent;

@interface OddityContentInfoTableViewCell : UITableViewCell

-(void)setContent:(OddityNewContent *)nContent;

@end
