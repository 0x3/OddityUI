//
//  OddityContentInfoTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityContentInfoTableViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityContentInfoTableViewCell()

@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *infoDescLabel;

@end

@implementation OddityContentInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 0;
        [self.contentView addSubview:_titleLabel];
        
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(@17);
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
        }];
        
        _infoDescLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_infoDescLabel];
        
        [_infoDescLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(@18);
            make.top.equalTo(_titleLabel.mas_bottom).offset(8);
            make.bottom.equalTo(@-17);
        }];
    }
    return self;
}

-(void)setContent:(OddityNewContent *)nContent{
    
    self.contentView.backgroundColor = [UIColor color_t_6];
    
    self.titleLabel.attributedText = nContent.titleAttributedString;
    self.infoDescLabel.attributedText  = nContent.infoAttributedString;
}

@end

