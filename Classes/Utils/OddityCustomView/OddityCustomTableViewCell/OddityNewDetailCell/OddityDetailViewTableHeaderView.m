//
//  OddityDetailViewTableHeaderView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityDetailViewTableHeaderView.h"

@interface OddityDetailViewTableHeaderView ()

@property (nonatomic,strong)UILabel *desclabel;
/// 边框视图
@property (nonatomic,strong)UIView *borderView;

/**
 *  简短的边框
 */
@property (nonatomic,strong)UIView *shortBorderView;
@end

@implementation OddityDetailViewTableHeaderView


- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        
        _borderView = [[UIView alloc]init];
        [self.contentView addSubview:_borderView];
        
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@1);
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
            make.bottom.equalTo(@0);
        }];
        
        _desclabel = [[UILabel alloc]init];
        _desclabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:_desclabel];
        [_desclabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_borderView);
            make.bottom.equalTo(_borderView.mas_top).offset(-6);
        }];
        
        
        _shortBorderView = [[UIView alloc]init];
        _shortBorderView.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
        [self.contentView addSubview:_shortBorderView];
        [_shortBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@1);
            make.left.equalTo(_borderView);
            make.centerY.equalTo(_borderView);
            make.width.equalTo(_desclabel);
        }];
    }
    return self;
}

-(void)setDescInfoString:(NSString *)desc{
    
    self.desclabel.text = desc;
    
    _desclabel.textColor = [UIColor color_t_3];
    _borderView.backgroundColor = [UIColor color_t_5];
    self.contentView.backgroundColor = [UIColor color_t_6];
    _shortBorderView.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
}

@end

