//
//  OddityToMoreCommentTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"

#import "OddityCategorys.h"

#import "OddityToMoreCommentTableViewCell.h"

@interface OddityToMoreCommentTableViewCell()

@property(nonatomic,strong) UIButton *toNoMoreButton;

@end

@implementation OddityToMoreCommentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _toNoMoreButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
        _toNoMoreButton.layer.cornerRadius = 4;
        _toNoMoreButton.tag = 123;
        [_toNoMoreButton setTitleColor:[OddityUISetting shareOddityUISetting].mainTone forState:(UIControlStateNormal)];
        [_toNoMoreButton setTitle:@"查看全部评论 >" forState:(UIControlStateNormal)];
        [self.contentView addSubview:_toNoMoreButton];
        
        [_toNoMoreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(@28);
            make.bottom.equalTo(@-31);
            make.height.equalTo(@39);
            
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
        }];
    }
    return self;
}

-(void)refresh{
    
    self.contentView.backgroundColor = [UIColor color_t_6];
    _toNoMoreButton.backgroundColor = [UIColor color_t_5];
    [_toNoMoreButton setTitleColor:[OddityUISetting shareOddityUISetting].mainTone forState:(UIControlStateNormal)];
}

@end

