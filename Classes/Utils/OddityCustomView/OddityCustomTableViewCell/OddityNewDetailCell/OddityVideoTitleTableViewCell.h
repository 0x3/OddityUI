//
//  OddityVideoTitleTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@interface OddityVideoTitleTableViewCell : UITableViewCell

-(void)configCell:(NSString *)title;
@end

