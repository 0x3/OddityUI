//
//  OddityVideoTitleTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityVideoTitleTableViewCell.h"

@interface OddityVideoTitleTableViewCell ()

@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation OddityVideoTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.numberOfLines = 0;
        _titleLabel.font = [UIFont systemFontOfSize:19];
        [self.contentView addSubview:_titleLabel];
        
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.edges.equalTo(self.contentView).insets(UIEdgeInsetsMake(14, 15, 20, 15));
        }];
    }
    
    return self;
}

-(void)configCell:(NSString *)title{
    
    self.contentView.backgroundColor = [UIColor color_t_6];
    
    self.titleLabel.text = title;
    _titleLabel.textColor = [UIColor color_t_2];
}

@end
