//
//  OddityPlaylistCollectionViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/5/9.
//
//

#import "OddityNew.h"
#import <UIKit/UIKit.h>

@interface OddityPlaylistCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) UILabel *willPlayLabel;

-(void)setModel:(OddityNew*)nObject;

@end
