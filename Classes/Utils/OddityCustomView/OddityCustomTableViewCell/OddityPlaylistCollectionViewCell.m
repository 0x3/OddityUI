//
//  OddityPlaylistCollectionViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/5/9.
//
//

#import "OddityCategorys.h"
#import "OddityPlaylistCollectionViewCell.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OddityPlaylistCollectionViewCell ()

@property(nonatomic,strong) UILabel *titleLabel;
@property(nonatomic,strong) UIImageView *coverImageView;

@end

@implementation OddityPlaylistCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if ( self ) {
        
        _coverImageView = [[UIImageView alloc] init];
        _coverImageView.clipsToBounds = true;
        _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_coverImageView];
        [_coverImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.with.left.mas_equalTo(8);
            make.bottom.mas_equalTo(-8);
            make.size.mas_equalTo(CGSizeMake(97, 65));
        }];
        
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
        _titleLabel.numberOfLines = 2;
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.mas_equalTo(8);
            make.left.mas_equalTo(_coverImageView.mas_right).offset(6);
            make.right.mas_equalTo(-8);
        }];
        
        _willPlayLabel = [[UILabel alloc] init];
        _willPlayLabel.text = @"即将播放";
        _willPlayLabel.textColor = [UIColor whiteColor];
        _willPlayLabel.textAlignment = NSTextAlignmentCenter;
        _willPlayLabel.font = [UIFont systemFontOfSize:12];
        _willPlayLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
        [_coverImageView addSubview:_willPlayLabel];
        [_willPlayLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.bottom.with.right.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(57, 19));
        }];
        
        _titleLabel.text = @"与美元终结日线6连阳 人民币汇率多头将重回市场？";
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _coverImageView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = [UIColor color_t_1];
    }
    
    return self;
}

-(void)setModel:(OddityNew *)nObject{

    self.titleLabel.attributedText = nObject.playListTitleAttributedString;
    
    [self.coverImageView pin_setImageFromURL:[nObject videoImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
}

@end
