//
//  OddityPromptInfoTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/27.
//
//

#import <UIKit/UIKit.h>

@interface OddityPromptInfoTableViewCell : UITableViewCell

+(NSAttributedString *)createAttributed:(NSString *)str;

-(void)configAttriString:(NSAttributedString *)string image:(UIImage *)img;

@end
