//
//  OddityPromptInfoTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/27.
//
//

#import "OddityCategorys.h"
#import "OddityPromptInfoTableViewCell.h"

@interface OddityPromptInfoTableViewCell ()

@property(nonatomic,strong) UILabel *infoLabel;
@property(nonatomic,strong) UIImageView *infoImageView;

@end

@implementation OddityPromptInfoTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _infoImageView = [[UIImageView alloc] init];
        _infoImageView.contentMode = UIViewContentModeScaleToFill;
        [self.contentView addSubview:_infoImageView];
        [_infoImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.centerX.mas_equalTo(self.contentView);
            make.centerY.mas_equalTo(self.contentView).offset(-50);
        }];
        
        _infoLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_infoLabel];
        [_infoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.centerX.mas_equalTo(self.contentView);
            make.top.mas_equalTo(self.infoImageView.mas_bottom);
        }];
    }
    
    return self;
}

-(void)configAttriString:(NSAttributedString *)string image:(UIImage *)img{

    self.infoImageView.image = img;
    self.infoLabel.attributedText = string;
    
    self.backgroundColor = UIColor.color_t_6;
}

+(NSAttributedString *)createAttributed:(NSString *)str{

    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_6],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:str attributes:attris];
    
    return attriString;
}

@end
