//
//  OddityHotSearchContainerTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityHotSearchContainerTableViewCell.h"

@implementation OddityHotSearchContainerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
