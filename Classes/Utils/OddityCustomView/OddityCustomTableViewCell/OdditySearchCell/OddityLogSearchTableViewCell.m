//
//  OddityLogSearchTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityCategorys.h"
#import "OddityLogSearchTableViewCell.h"

@interface OddityLogSearchTableViewCell()

@property (nonatomic,strong)UILabel *descLabel;

@property (nonatomic,strong)UIView *borderView;

@end

@implementation OddityLogSearchTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    
    if (selected) {
        
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_9]]; // set color here
    }else{
        
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_5]]; // set color here
    }
    
    [self setSelectedBackgroundView:selectedBackgroundView];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        
        _descLabel = [[UILabel alloc ] init];
        [self.contentView addSubview:_descLabel];
        [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self.contentView);
            make.left.mas_equalTo(15);
        }];
        
        _borderView = [[UIView alloc] init];
        [self.contentView addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.right.with.bottom.mas_equalTo(0);
            make.height.mas_equalTo(0.5);
        }];
    }
    
    return self;
}

-(void)setdescribe:(NSAttributedString *)desc{
    
    _descLabel.attributedText = desc;
    self.contentView.backgroundColor = [UIColor color_t_6];
    _borderView.backgroundColor = [UIColor color_t_5];
}

@end
