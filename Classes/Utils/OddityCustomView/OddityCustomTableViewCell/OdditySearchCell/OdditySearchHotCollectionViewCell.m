//
//  OdditySearchHotCollectionViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityCategorys.h"
#import "OdditySearchHotCollectionViewCell.h"

@interface OdditySearchHotCollectionViewCell()

@property (nonatomic,strong)UILabel *descLabel;

@property (nonatomic,strong)UIView *borderView;

@end

@implementation OdditySearchHotCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        _borderView = [[UIView alloc] init];
        _borderView.layer.borderWidth = 0.5;
        _borderView.layer.borderColor = [UIColor color_t_5].CGColor;
        [self.contentView addSubview:_borderView];
        _borderView.layer.cornerRadius = 15;
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.right.with.bottom.with.top.mas_equalTo(0);
        }];
        
        
        _descLabel = [[UILabel alloc ] init];
        [self.contentView addSubview:_descLabel];
        [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.center.mas_equalTo(self);
        }];
    }
    return self;
}

-(void)setdescribe:(NSAttributedString *)desc{
    
    _descLabel.attributedText = desc;
    _borderView.layer.borderColor = [UIColor color_t_5].CGColor;
}

@end

