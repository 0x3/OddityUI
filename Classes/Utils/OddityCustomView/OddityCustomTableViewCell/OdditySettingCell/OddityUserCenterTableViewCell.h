//
//  UserCenterTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//

#import <UIKit/UIKit.h>
#import "OddityUserCenterObject.h"

typedef NS_ENUM(NSUInteger, UserCenterTableViewCellBorderPostion) {
    UserCenterTableViewCellBorderPostionTop,
    UserCenterTableViewCellBorderPostionBottom
};

@interface OddityUserCenterTableViewCell : UITableViewCell

/**
 右侧视图
 */
@property(nonatomic,strong,readonly)UIView *rightView;


-(instancetype)fill:(OddityUserCenterObject *)object;

/**
 制作默认的右侧测试图
 */
-(void)makeDefultRightView;
/**
 隐藏边框方法

 @param potion 哪一个边框
 */
-(void)hidden:(UserCenterTableViewCellBorderPostion)potion;

/**
 修改距离左边的 位置

 @param potion 哪一个边框
 @param left 左边的距离
 */
-(void)update:(UserCenterTableViewCellBorderPostion)potion leftMargin:(CGFloat)left;

-(void)makeSubTitleString:(NSString *)string;
@end
