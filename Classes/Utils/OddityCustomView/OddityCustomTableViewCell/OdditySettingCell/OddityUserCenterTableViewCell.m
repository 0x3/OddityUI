//
//  UserCenterTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//

#import "OddityUserCenterObject.h"

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityUserCenterTableViewCell.h"

#import "OddityChooseFontView.h"

#import "OddityClearCacheTool.h"
@interface OddityUserCenterTableViewCell()

@property(nonatomic,strong)UIView *topBorderView;
@property(nonatomic,strong)UIView *bottomBorderView;

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *subTitleLabel;
@property(nonatomic,strong)UIImageView *iconImageView;

@end

@implementation OddityUserCenterTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.contentView.backgroundColor = [UIColor color_t_9];
        
        _iconImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconImageView];
        [_iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(12);
            make.centerY.mas_equalTo(self.contentView);
        }];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont font_t_2];
        _titleLabel.textColor = [UIColor color_t_2];
        [self.contentView addSubview:_titleLabel];
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self.contentView);
            make.left.mas_equalTo(_iconImageView.mas_right).offset(12);
        }];
        
        _topBorderView = [[UIView alloc] init];
        _topBorderView.backgroundColor = [UIColor color_t_5];
        [self.contentView addSubview:_topBorderView];
        [_topBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.right.with.left.with.top.mas_equalTo(0);
            make.height.mas_equalTo(0.5);
        }];
        
        _bottomBorderView = [[UIView alloc] init];
        _bottomBorderView.backgroundColor = [UIColor color_t_5];
        [self.contentView addSubview:_bottomBorderView];
        [_bottomBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.right.with.left.with.bottom.mas_equalTo(0);
            make.height.mas_equalTo(0.5);
        }];
        
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.textColor = UIColor.color_t_3;
        _subTitleLabel.font = UIFont.font_t_7;
        [self.contentView addSubview:_subTitleLabel];
        [_subTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(_titleLabel);
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(6);
        }];
        _subTitleLabel.hidden = true;
    }
    return self;
}


-(instancetype)fill:(OddityUserCenterObject *)object{
    
    _titleLabel.textColor = [UIColor color_t_2];
    self.contentView.backgroundColor = [UIColor color_t_9];
    _topBorderView.backgroundColor = [UIColor color_t_5];
    _bottomBorderView.backgroundColor = [UIColor color_t_5];
    
    _titleLabel.text = object.settingName;
    _iconImageView.image = object.iconImage;
    
    _topBorderView.backgroundColor = [UIColor color_t_5];
    _bottomBorderView.backgroundColor = [UIColor color_t_5];
    
    self.contentView.backgroundColor = [UIColor color_t_9];
    
    [self.contentView layoutIfNeeded];
    
    if (_rightView.superview) {
        [_rightView removeFromSuperview];
        _rightView = nil;
    }
    
    switch (object.type) {
            
        case UserCenterSettingTypeDel:{
            UILabel *label = [[UILabel alloc] init];
            label.font = UIFont.font_t_7;
            label.textColor = UIColor.color_t_3;
            dispatch_async(dispatch_get_main_queue(), ^{
               
                label.text = [OddityClearCacheTool getCacheSizeWithFilePath:NSString.oddity_cachePathString];
            });
            
            _rightView = label;
            [self.contentView addSubview:_rightView];
            [_rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(self.contentView);
                make.right.mas_equalTo(-12);
            }];
        }
            break;
            
        case UserCenterSettingTypeNone:{
        
            _rightView.hidden = true;
        }
            break;
        case UserCenterSettingTypeGo:{
            
            self.selectionStyle = UITableViewCellSelectionStyleDefault;
            
            _rightView = [[UIImageView alloc] initWithImage:[UIImage oddityImage:@"arrow_day"]];
            [self.contentView addSubview:_rightView];
            [_rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(self.contentView);
                make.right.mas_equalTo(-12);
            }];
        }
            break;
        case UserCenterSettingTypeChangeFont:{
            
            self.selectionStyle = UITableViewCellSelectionStyleNone;
            
            _rightView = [[OddityChooseFontView alloc] init];
            _rightView.layer.cornerRadius = 4;
            _rightView.clipsToBounds = true;
            [self.contentView addSubview:_rightView];
            [_rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(self.contentView);
                make.right.mas_equalTo(-12);
                make.width.mas_equalTo(154);
                make.height.mas_equalTo(32);
            }];
        }
            break;
        case UserCenterSettingTypeSwitch:{
            
            self.selectionStyle = UITableViewCellSelectionStyleNone;
            
            _rightView = [[UISwitch alloc] init];
            [self.contentView addSubview:_rightView];
            [_rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.mas_equalTo(self.contentView);
                make.right.mas_equalTo(-12);
            }];
        }
            break;
    }
    return self;
}

-(void)makeDefultRightView{

    _rightView = [[UIImageView alloc] initWithImage:[UIImage oddityImage:@"arrow_day"]];
    [self.contentView addSubview:_rightView];
    [_rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(self.contentView);
        make.right.mas_equalTo(-12);
    }];
}

-(void)hidden:(UserCenterTableViewCellBorderPostion)potion{

    switch (potion) {
        case UserCenterTableViewCellBorderPostionTop:{
        
            self.topBorderView.hidden = true;
        }
            break;
        case UserCenterTableViewCellBorderPostionBottom:{
            
            self.bottomBorderView.hidden = true;
        }
            break;
    }
}

-(void)update:(UserCenterTableViewCellBorderPostion)potion leftMargin:(CGFloat)left{
    
    switch (potion) {
        case UserCenterTableViewCellBorderPostionTop:{
            
            [self.topBorderView mas_updateConstraints:^(MASConstraintMaker *make) {
               
                make.left.mas_equalTo(left);
            }];
        }
            break;
        case UserCenterTableViewCellBorderPostionBottom:{
            
            [self.bottomBorderView mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.left.mas_equalTo(left);
            }];
        }
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
    
    UIView * selectedBackgroundView = [[UIView alloc] init];
    
    if (selected) {
        
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_9]]; // set color here
    }else{
        
        [selectedBackgroundView setBackgroundColor:[UIColor color_t_5]]; // set color here
    }
    
    [self setSelectedBackgroundView:selectedBackgroundView];
}

-(void)makeSubTitleString:(NSString *)string{
    
    _subTitleLabel.hidden = false;
    _subTitleLabel.text = string;
    
    [_titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(self.contentView).offset(-12);
    }];
}

@end
