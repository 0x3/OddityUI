//
//  OdditySpecialInfoTableViewHeaderView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@class OdditySpecialClass;

@interface OdditySpecialInfoTableViewHeaderView : UITableViewCell

-(void)setSpecialClass:(OdditySpecialClass *)specialClass;

@end
