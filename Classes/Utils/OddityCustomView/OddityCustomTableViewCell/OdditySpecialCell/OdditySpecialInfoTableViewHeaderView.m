//
//  OdditySpecialInfoTableViewHeaderView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"

#import "OddityCategorys.h"

#import "OdditySpecialInfoTableViewHeaderView.h"

#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OdditySpecialInfoTableViewHeaderView()

@property (nonatomic,strong)UIView *topBorderView;
@property (nonatomic,strong)UIView *bottomBorderView;

@property (nonatomic,strong)UIView *blockView;

@property(nonatomic,strong) UILabel *descLabel;

@end

@implementation OdditySpecialInfoTableViewHeaderView

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        self.contentView.backgroundColor = [UIColor color_t_6];
        
        _topBorderView = [[UIView alloc] init];
        _topBorderView.backgroundColor = [UIColor color_t_5];
        [self.contentView addSubview:_topBorderView];
        [_topBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@0.5);
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];
        
        _bottomBorderView = [[UIView alloc] init];
        _bottomBorderView.backgroundColor = [UIColor color_t_5];
        [self.contentView addSubview:_bottomBorderView];
        [_bottomBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@0.5);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];
        
        _blockView = [[UIView alloc] init];
        _blockView.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
        [self.contentView addSubview:_blockView];
        [_blockView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@15);
            make.width.equalTo(@4);
            
            make.left.equalTo(self.contentView);
            make.centerY.equalTo(self.contentView);
        }];
        
        
        _descLabel = [[UILabel alloc]init];
        _descLabel.numberOfLines = 0;
        [self.contentView addSubview:_descLabel];
        
        [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_blockView.mas_right).offset(8);
            
            make.centerY.equalTo(self.contentView);
        }];
        
    }
    return self;
}

-(void)setSpecialClass:(OdditySpecialClass *)specialClass{
    
    _descLabel.font = [UIFont font_t_6];
    _descLabel.textColor = [UIColor color_t_3];
    
    _descLabel.text = specialClass.name;
}

@end
