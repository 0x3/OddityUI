//
//  OdditySpecialTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

@class OdditySpecial;

@interface OdditySpecialTableViewCell : UITableViewCell

-(void)setSpecial:(OdditySpecial *)special;

@end
