//
//  OdditySpecialTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"

#import "OddityCategorys.h"

#import "OdditySpecialTableViewCell.h"

#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

@interface OdditySpecialTableViewCell()

@property(nonatomic,strong) UILabel *descLabel;
@property(nonatomic,strong) OddityUILabelPadding *tagLabel;

@property (nonatomic,strong)UIView *borderView;

@property (nonatomic,strong)OddityThemeImageView *coverImageView;

@end

@implementation OdditySpecialTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        self.contentView.backgroundColor = [UIColor color_t_9];
        
        _borderView = [[UIView alloc] init];
        _borderView.backgroundColor = [UIColor color_t_5];
        [self.contentView addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@0.5);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];
        
        
        _coverImageView = [[OddityThemeImageView alloc]init];
        [_coverImageView setPin_updateWithProgress:YES];
        _coverImageView.clipsToBounds = true;
        _coverImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.contentView addSubview:_coverImageView];
        [_coverImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            
            make.height.equalTo(_coverImageView.mas_width).multipliedBy((CGFloat)91/375);
        }];
        
        
        _descLabel = [[UILabel alloc]init];
        _descLabel.numberOfLines = 0;
        [self.contentView addSubview:_descLabel];
        
        [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_coverImageView.mas_bottom).offset(16);
            make.left.equalTo(@18);
            make.right.equalTo(@-18);
            make.bottom.equalTo(@-16);
        }];
        
        _tagLabel = [[OddityUILabelPadding alloc]init];
        _tagLabel.edgeInsets = UIEdgeInsetsMake(0, 1, 0, 1);
        _tagLabel.layer.cornerRadius = 2;
        _tagLabel.layer.borderWidth = 0.5;
        [self.contentView addSubview:_tagLabel];
        [_tagLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_coverImageView.mas_bottom).offset(18.5);
            make.left.equalTo(@18);
        }];
    }
    return self;
}

-(void)setSpecial:(OdditySpecial *)special{
    
    _borderView.backgroundColor = [UIColor color_t_5];
    self.contentView.backgroundColor = [UIColor color_t_9];
    
    _descLabel.font = [UIFont font_t_6];
    _descLabel.textColor = [UIColor color_t_2];
    
    _tagLabel.font = [UIFont font_t_9];
    _tagLabel.text = @"摘要";
    _tagLabel.textColor = [UIColor color_t_7];
    _tagLabel.layer.borderColor = [UIColor color_t_7].CGColor;
    
    [_coverImageView pin_setImageFromURL:[special coverImageUrl] placeholderImage: UIImage.oddity_theme_placeholder_image];
    
    NSString *descStr = [special descString];
    
    if ( descStr ) {
        
        _descLabel.text = descStr;
    }else{
        
        [_descLabel setHidden: true];
        [_tagLabel setHidden: true];
        
        [_coverImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            
            make.height.equalTo(_coverImageView.mas_width).multipliedBy((CGFloat)91/375);
            
            make.bottom.equalTo(@-16);
        }];
        
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.equalTo(@0.5);
            make.bottom.equalTo(self.contentView);
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
        }];
        
        [self layoutIfNeeded];
    }
}


@end
