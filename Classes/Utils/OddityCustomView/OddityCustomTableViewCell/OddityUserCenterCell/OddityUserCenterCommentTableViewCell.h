//
//  OddityUserCenterCommentTableViewCell.h
//  Pods
//
//  Created by 荆文征 on 2017/6/20.
//
//

#import <UIKit/UIKit.h>

@class OddityNewContentComment;

@protocol OddityUserCenterCommentTableViewCellDelegate <NSObject>

-(void)clickNtitle:(OddityNewContentComment *)comment;
-(void)clickDelete:(OddityNewContentComment *)comment;
-(void)clickCommentButton:(OddityNewContentComment *)comment animate:(OddityAnimationNumberView *)view button:(OddityBigButton *)button;

@end

@interface OddityUserCenterCommentTableViewCell : UITableViewCell

-(void)fill:(OddityNewContentComment *)comment;

@property (weak, nonatomic) id<OddityUserCenterCommentTableViewCellDelegate> delegate;
@end
