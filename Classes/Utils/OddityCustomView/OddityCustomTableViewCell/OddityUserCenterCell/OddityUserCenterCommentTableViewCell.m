//
//  OddityUserCenterCommentTableViewCell.m
//  Pods
//
//  Created by 荆文征 on 2017/6/20.
//
//

#import "OddityNewContentComment+Interface.h"

#import "OddityCategorys.h"
#import "OddityUserCenterCommentTableViewCell.h"

@interface OddityUserCenterCommentCenterView : UIView

@property(nonatomic,strong)UILabel *ntitleLabel; // 原文Label
@property(nonatomic,strong)UIButton *goImageView; // 前往图片

@end

@implementation OddityUserCenterCommentCenterView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        _goImageView = [UIButton buttonWithType:(UIButtonTypeSystem)];
        [_goImageView setImage:UIImage.oddity_setting_arrow_image forState:(UIControlStateNormal)];
        [self addSubview:_goImageView];
        
        _ntitleLabel = [[UILabel alloc] init];
        [self addSubview:_ntitleLabel];
        
        [_ntitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self);
            make.left.mas_equalTo(10);
            make.right.mas_equalTo(-28);
        }];
        
        [_goImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self);
            make.right.mas_equalTo(-10);
        }];
        
    }
    return self;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    [super touchesBegan:touches withEvent:event];
    
    self.backgroundColor = [UIColor.color_t_4 colorWithAlphaComponent:0.1];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesEnded:touches withEvent:event];
    
    self.backgroundColor = [UIColor.color_t_4 colorWithAlphaComponent:0.2];
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [super touchesCancelled:touches withEvent:event];
    
    self.backgroundColor = [UIColor.color_t_4 colorWithAlphaComponent:0.2];
}

@end

@interface OddityUserCenterCommentTableViewCell()


@property(nonatomic,strong)UILabel *contentLabel; // 评论内容的Label

@property(nonatomic,strong)OddityUserCenterCommentCenterView *centerView;


@property(nonatomic,strong)UIView *bottomView;
@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)OddityBigButton *deleteButton;
@property(nonatomic,strong)OddityBigButton *upflagButton; // 点赞按钮


@property(nonatomic,strong)UIView *borderView; // 下边框

@property(nonatomic,strong)OddityNewContentComment *comment;


@property (nonatomic,strong)UILabel *commentLabel;
@property (nonatomic,strong) OddityAnimationNumberView *commentLabelView;

@end

@implementation OddityUserCenterCommentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.contentView.backgroundColor = UIColor.color_t_6;
        
        
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentLabel];
        [_contentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(18);
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
        }];
        
        
        _centerView = [[OddityUserCenterCommentCenterView alloc] init];
        _centerView.layer.cornerRadius = 2;
        [self.contentView addSubview:_centerView];
        [_centerView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.top.mas_equalTo(_contentLabel.mas_bottom).offset(14);
            make.height.mas_equalTo(38);
        }];
        
        
        _bottomView = [[UIView alloc] init];
        [self.contentView addSubview:_bottomView];
        [_bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.right.bottom.mas_offset(0);
            make.top.mas_equalTo(_centerView.mas_bottom);
            make.height.mas_equalTo(38);
        }];
        
        
        _timeLabel = [[UILabel alloc] init];
        [self.bottomView addSubview:_timeLabel];
        
        _upflagButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
        [self.bottomView addSubview:_upflagButton];
        
        
        _deleteButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
        [_deleteButton setImage:UIImage.oddity_usercenter_comment_delete_image forState:(UIControlStateNormal)];
        [self.bottomView addSubview:_deleteButton];
        
        [_timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.centerY.mas_equalTo(self.bottomView);
            make.left.mas_equalTo(12);
        }];
        
        [_upflagButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self.bottomView).offset(-2);
            make.left.mas_equalTo(_timeLabel.mas_right).offset(10);
        }];
        
        [_deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self.bottomView);
            make.right.mas_equalTo(-12);
        }];
        
        
        
        
        _borderView = [[UIView alloc] init];
        [self.contentView addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(0.5);
        }];
        
        
        for (id g in _centerView.gestureRecognizers) { [_centerView removeGestureRecognizer:g]; }
        [_centerView addGestureRecognizer:[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickNtitleViewMethod)]];
        
        for (id tag in _deleteButton.allTargets) { [_deleteButton removeTarget:tag action:nil forControlEvents:(UIControlEventAllEvents)]; }
        [_deleteButton addTarget:self action:@selector(clickDeleteViewMethod) forControlEvents:(UIControlEventTouchUpInside)];
        
        for (id tag in _upflagButton.allTargets) { [_upflagButton removeTarget:tag action:nil forControlEvents:(UIControlEventAllEvents)]; }
        [_upflagButton addTarget:self action:@selector(clickVoteedViewMethod) forControlEvents:(UIControlEventTouchUpInside)];
        
        _commentLabel = [[UILabel alloc]init];
        [self.contentView addSubview:_commentLabel];
        [_commentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_upflagButton.mas_right).offset(4);
            make.bottom.equalTo(_upflagButton.mas_bottom).offset(1);
        }];
        
        _commentLabelView = [[OddityAnimationNumberView alloc] initWithFrame:(CGRectMake(0, 0, 300, 20))];
        _commentLabelView.numberFont = UIFont.font_t_7;
        [self.contentView addSubview:_commentLabelView];
        [_commentLabelView layoutIfNeeded];
        [_commentLabelView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.right.bottom.left.mas_equalTo(_commentLabel);
        }];
    }
    
    return self;
}

-(void)clickVoteedViewMethod{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickCommentButton:animate:button:)]) {
        
        [self.delegate clickCommentButton:self.comment animate:self.commentLabelView button:self.upflagButton];
    }
}

-(void)clickDeleteViewMethod{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickDelete:)]) {
        
        [self.delegate clickDelete:self.comment];
    }
}

-(void)clickNtitleViewMethod{

    if (self.delegate && [self.delegate respondsToSelector:@selector(clickNtitle:)]) {
        
        [self.delegate clickNtitle:self.comment];
    }
}

-(void)fill:(OddityNewContentComment *)comment{

    _comment = comment;
    
    self.backgroundColor = UIColor.color_t_6;
    self.contentView.backgroundColor = UIColor.color_t_6;
    
    self.contentLabel.attributedText = comment.userCenter_contentAttributedString;
    
    self.centerView.backgroundColor = [UIColor.color_t_4 colorWithAlphaComponent:0.2];
    self.centerView.ntitleLabel.attributedText = comment.userCenter_ntitleAttributedString;
    self.centerView.goImageView.tintColor = UIColor.color_t_3;
    
    self.timeLabel.attributedText = comment.userCenter_timeAttributedString;
    
    self.upflagButton.tintColor = comment.commentButtonTintColor;
    [self.upflagButton setImage:comment.commentButtonImage forState:(UIControlStateNormal)];
    
    
    [self.upflagButton setImage:UIImage.oddity_detail_up_normal_image forState:(UIControlStateNormal)];
    
    
    self.deleteButton.tintColor = UIColor.color_t_3;
    
    self.borderView.backgroundColor = UIColor.color_t_5;
    
    
    self.commentLabel.hidden = true;
    self.commentLabel.attributedText = comment.commentAttributedString;
    self.commentLabelView.hidden = comment.isHiddenCommentLabel;
    
    self.commentLabelView.numberColor = UIColor.color_t_3;
    self.commentLabelView.numberFont = UIFont.font_t_7;
    
    [self.commentLabelView setCurrentNumber:comment.commentAttributedString.string];
    
    self.upflagButton.tintColor = comment.commentButtonTintColor;
    [self.upflagButton setImage:comment.commentButtonImage forState:(UIControlStateNormal)];
    
    [_deleteButton setImage:UIImage.oddity_usercenter_comment_delete_image forState:(UIControlStateNormal)];
}

@end
