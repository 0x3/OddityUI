//
//  OddityDescInfoHeaderView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <UIKit/UIKit.h>

@interface OddityDescInfoHeaderView : UITableViewHeaderFooterView

@property(nonatomic,strong) UILabel *descLabel;

-(void)setDescStr:(NSString *)desc;

@end
