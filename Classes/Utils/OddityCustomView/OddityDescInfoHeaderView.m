//
//  OddityDescInfoHeaderView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityCategorys.h"
#import "OddityDescInfoHeaderView.h"

@interface OddityDescInfoHeaderView()


@property (nonatomic,strong) UIView *backView;
@property (nonatomic,strong) UIView *blockView;


@property (nonatomic,strong) UIView *toptBorder;

@property (nonatomic,strong) UIView *topBorder;
@property (nonatomic,strong) UIView *bottomBorder;

@end

@implementation OddityDescInfoHeaderView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
       
        _backView                     = [[UIView alloc]init];
        [self addSubview:_backView];
        _backView.backgroundColor     = [UIColor color_t_6];
        [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.mas_equalTo(38);
            make.left.with.right.with.bottom.mas_equalTo(self);
        }];
        
        _topBorder                    = [[UIView alloc]init];
        _topBorder.backgroundColor    = [UIColor color_t_5];
        [_backView addSubview:_topBorder];
        [_topBorder mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.mas_equalTo(0.5);
            make.left.with.right.with.top.mas_equalTo(self);
        }];
        
        
        _toptBorder                   = [[UIView alloc]init];
        _toptBorder.backgroundColor   = [UIColor color_t_5];
        [self addSubview:_toptBorder];
        [_toptBorder mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.mas_equalTo(0.5);
            make.left.with.right.with.top.mas_equalTo(self);
        }];
        
        _bottomBorder                 = [[UIView alloc]init];
        _bottomBorder.backgroundColor = [UIColor color_t_5];
        [_backView addSubview:_bottomBorder];
        [_bottomBorder mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.mas_equalTo(0.5);
            make.left.with.right.with.bottom.mas_equalTo(self);
        }];
        
        
        _blockView                    = [[UIView alloc]init];
        _blockView.backgroundColor    = [OddityUISetting shareOddityUISetting].mainTone;
        [self addSubview:_blockView];
        [_blockView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.centerY.mas_equalTo(_backView);
            make.width.mas_equalTo(4);
            make.height.mas_equalTo(15);
        }];
        
        _descLabel                    = [[UILabel alloc]init];
        _descLabel.font               = [UIFont font_t_6];
        _descLabel.textColor          = [UIColor color_t_3];
        [self addSubview:_descLabel];
        [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(_blockView.mas_right).offset(8);
            make.centerY.equalTo(_backView);
        }];
    }
    return self;
}

-(void)setDescStr:(NSString *)desc{
    
    _descLabel.text = desc;
    _toptBorder.backgroundColor   = [UIColor color_t_5];
    _bottomBorder.backgroundColor = [UIColor color_t_5];
    _backView.backgroundColor     = [UIColor color_t_6];
    _blockView.backgroundColor    = [OddityUISetting shareOddityUISetting].mainTone;
    
}

@end
