//
//  OddityDetailBottomInputView.h
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import <UIKit/UIKit.h>

@protocol OddityDetailBottomInputViewDelegate<NSObject>

-(void)didTapInputView;

-(void)didTapShareButton;
-(void)didTapCommentButton;
-(void)didTapCollectButton:(UIButton *)button;

@end

@interface OddityDetailBottomInputView : UIView

@property(nonatomic,weak) id<OddityDetailBottomInputViewDelegate> delegate;

-(void)refreshTheme;

-(void)makeEnable:(BOOL)enable;

-(void)bottomRefreshMethod:(OddityNewContent *)nContent currentIndex:(NSInteger)index;
@end
