//
//  OddityDetailBottomInputView.m
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//
#import "OddityModels.h"
#import "OddityCategorys.h"

#import <Masonry/Masonry.h>
#import "OddityDetailBottomInputView.h"

@interface OddityDetailBottomInputView ()

@property(nonatomic,strong)UILabel *descLabel;

@property(nonatomic,strong)UIView *inputView;
@property(nonatomic,strong)UIView *borderView;

@property(nonatomic,strong)UILabel *commentCountLabel;

@property(nonatomic,strong)OddityBigButton *shareButton;
@property(nonatomic,strong)OddityBigButton *commentButton;
@property(nonatomic,strong)OddityBigButton *collectButton;

@end

@implementation OddityDetailBottomInputView


-(void)makeEnable:(BOOL)enable{

    [UIView animateWithDuration:0.3 animations:^{
       
        if (!enable) {
            
            self.inputView.userInteractionEnabled = false;
            self.shareButton.enabled = false;
            self.commentButton.enabled = false;
            self.collectButton.enabled = false;
            
            self.shareButton.tintColor = UIColor.color_t_3;
            self.collectButton.tintColor = UIColor.color_t_3;
            self.commentButton.tintColor = UIColor.color_t_3;
            
            self.inputView.backgroundColor = UIColor.color_t_6;
        }else{
            
            self.inputView.userInteractionEnabled = true;
            
            self.shareButton.enabled = true;
            self.commentButton.enabled = true;
            self.collectButton.enabled = true;
            
            self.shareButton.tintColor = UIColor.color_t_2;
            self.collectButton.tintColor = UIColor.color_t_2;
            self.commentButton.tintColor = UIColor.color_t_2;
            
            self.inputView.backgroundColor = UIColor.color_t_5;
        }
    }];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _borderView = [[UIView alloc] init];
        [self addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.with.right.with.left.mas_equalTo(0);
            make.height.mas_equalTo(0.5);
        }];
        
        _shareButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
        [self addSubview:_shareButton];
        [self.shareButton setImage:UIImage.oddity_detail_share_image forState:(UIControlStateNormal)];
        [self.shareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.right.mas_equalTo(-12);
            make.centerY.mas_equalTo(self);
        }];
        
        if (OddityShareUser.defaultUser.isShowDetailCollec) {
            
            _collectButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
            [self addSubview:_collectButton];
            [self.collectButton setImage:UIImage.oddity_detail_collect_image forState:(UIControlStateNormal)];
            [self.collectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.right.mas_equalTo(_shareButton.mas_left).offset(-28);
                make.centerY.mas_equalTo(self);
            }];
            
            [_collectButton addTarget:self action:@selector(didTapCollectButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
        }
        

        _commentButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
        [self addSubview:_commentButton];
        [self.commentButton setImage:UIImage.oddity_detail_comment_image forState:(UIControlStateNormal)];
        [self.commentButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            if (OddityShareUser.defaultUser.isShowDetailCollec) {
                make.right.mas_equalTo(_collectButton.mas_left).offset(-28);
            }else{
                make.right.mas_equalTo(_shareButton.mas_left).offset(-28);
            }
            make.centerY.mas_equalTo(self);
        }];
        
        _commentCountLabel = [[UILabel alloc] init];
        _commentCountLabel.font = UIFont.font_t_7;
        _commentCountLabel.hidden = true;
        [self addSubview:_commentCountLabel];
        [_commentCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.top.mas_equalTo(_commentButton.mas_top).offset(-6);
            make.right.mas_equalTo(_commentButton.mas_right).offset(-2);
        }];
        
        _inputView = [[UIView alloc] init];
        _inputView.layer.cornerRadius = 15;
        _inputView.layer.borderWidth = 0.5;
        [self addSubview:_inputView];
        [_inputView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(12);
            make.centerY.mas_equalTo(self);
            make.height.mas_equalTo(32);
            make.right.mas_equalTo(_commentButton.mas_left).offset(-28);
        }];
        
        _descLabel = [[UILabel alloc] init];
        _descLabel.font = UIFont.font_t_6;
        _descLabel.text = @"说一下";
        [self.inputView addSubview:_descLabel];
        [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.centerY.mas_equalTo(_inputView);
            make.left.mas_equalTo(12);
        }];
        
        [_commentButton addTarget:self action:@selector(didTapCommentButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
        [_shareButton addTarget:self action:@selector(didTapShareButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
        [_inputView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapInputViewAction)]];
        
        [self refreshTheme];
    }
    return self;
}


-(void)didTapInputViewAction{

    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapInputView)]) {
        
        [self.delegate didTapInputView];
    }
}

-(void)didTapShareButtonAction{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapShareButton)]) {
        
        [self.delegate didTapShareButton];
    }
}

-(void)didTapCommentButtonAction{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapCommentButton)]) {
        
        [self.delegate didTapCommentButton];
    }
}

-(void)didTapCollectButtonAction:(UIButton *)button{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didTapCollectButton:)]) {
        
        [self.delegate didTapCollectButton:button];
    }
}



-(void)refreshTheme{
    
    self.commentCountLabel.backgroundColor = UIColor.color_t_6;
    self.commentCountLabel.textColor = OddityUISetting.shareOddityUISetting.mainTone;
    
    self.descLabel.textColor = UIColor.color_t_3;
    
    self.inputView.layer.borderWidth = 0.5;
    self.borderView.backgroundColor = UIColor.color_t_5;
    self.backgroundColor = UIColor.color_t_6;
    self.inputView.layer.borderColor = UIColor.color_t_5.CGColor;
    self.inputView.backgroundColor = UIColor.color_t_9;
    
    self.shareButton.tintColor = UIColor.color_t_2;
    self.collectButton.tintColor = UIColor.color_t_2;
    self.commentButton.tintColor = UIColor.color_t_2;
    
}

-(void)bottomRefreshMethod:(OddityNewContent *)nContent currentIndex:(NSInteger)index{

    _commentCountLabel.text = [NSString stringWithFormat:@"%@",[NSString commentFormatString:nContent.comment]];
    
    if (index == 0) {
        
        if (nContent.comment > 0) { // 如果评论大于 0
            
            self.commentCountLabel.hidden = false;
            [self.commentButton setImage:UIImage.oddity_detail_comment_image forState:(UIControlStateNormal)];
        }else{
            
            self.commentCountLabel.hidden = true;
            [self.commentButton setImage:UIImage.oddity_detail_no_comment_image forState:(UIControlStateNormal)];
        }
    }else{
        
        self.commentCountLabel.hidden = true;
        [self.commentButton setImage:UIImage.oddity_detail_to_post_image forState:(UIControlStateNormal)];
    }
    
    [_commentCountLabel layoutIfNeeded];
    
    self.collectButton.tintColor = nContent.collectButtonTintColor;
    [self.collectButton setImage:nContent.collectButtonImage forState:(UIControlStateNormal)];
}

@end
