//
//  OddityInsetFiled.h
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import <UIKit/UIKit.h>

@interface OddityInsetFiled : UITextField


/**
 初始化方法 根据 文字左侧可见

 @param text 文字的
 @param editing 正在编辑的饿
 @param placeholder pla的
 @return 返回输入视图
 */
- (instancetype)initTextLeft:(CGFloat)text editingLeft:(CGFloat)editing placeholderLeft:(CGFloat) placeholder;

@end
