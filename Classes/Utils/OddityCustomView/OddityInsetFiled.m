//
//  OddityInsetFiled.m
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import "OddityInsetFiled.h"

@interface OddityInsetFiled()

@property(assign,nonatomic)CGFloat textLeft;
@property(assign,nonatomic)CGFloat editingLeft;
@property(assign,nonatomic)CGFloat placeholderLeft;

@end

@implementation OddityInsetFiled

- (instancetype)initTextLeft:(CGFloat)text editingLeft:(CGFloat)editing placeholderLeft:(CGFloat) placeholder {

    self = [super initWithFrame:CGRectZero];
    
    if (self) {
        
        self.textLeft = text;
        self.editingLeft = editing;
        self.placeholderLeft = placeholder;
    }
    return self;
}


-(CGRect)placeholderRectForBounds:(CGRect)bounds{
    
    return CGRectInset(bounds, self.placeholderLeft, 0);
}

-(CGRect)editingRectForBounds:(CGRect)bounds{
    
    return CGRectInset(bounds, self.editingLeft, 0);
}

-(CGRect)textRectForBounds:(CGRect)bounds {
    
    return CGRectInset(bounds, self.textLeft, 0);
}

@end
