//
//  OddityNavigationBar.m
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//
#import "OdditySetting.h"
#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityNavigationBar.h"

@interface OddityNavigationBar()

@property(nonatomic,strong) UIView *borderView;

@end

@implementation OddityNavigationBar

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
        
        _borderView = [[UIView alloc]init];
        [self addSubview:_borderView];
        _borderView.backgroundColor = [OddityUISetting shareOddityUISetting].borderColor;
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@0.5);
        }];
    }
    return self;
}


-(void)refreshThemeMethod{
    
    self.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    _borderView.backgroundColor = [OddityUISetting shareOddityUISetting].borderColor;
}

@end
