//
//  OddityNavigationBarView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>
#import "OddityBigButton.h"

@interface OddityNavigationBarView : UIView

@property(nonatomic,strong) UIView *borderView;
@property(nonatomic,strong) OddityBigButton *cancelButton;

- (instancetype)init:(NSString *)title titleFont:(UIFont *)f block:(void(^)())b;

@end
