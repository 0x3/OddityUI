//
//  OddityNavigationBarView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OdditySetting.h"
#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityNavigationBarView.h"

@interface OddityNavigationBarView()

@property(nonatomic,strong) UILabel *titleLabel;

@end

typedef int (^ClickCancelButtonAction)();

@implementation OddityNavigationBarView

- (instancetype)init:(NSString *)title titleFont:(UIFont *)f block:(void(^)())b
{
    self = [super init];
    if (self) {
        
        self.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
        
        _borderView = [[UIView alloc]init];
        [self addSubview:_borderView];
        _borderView.backgroundColor = [OddityUISetting shareOddityUISetting].borderColor;
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@0.5);
        }];
        
        _cancelButton = [OddityBigButton buttonWithType:UIButtonTypeSystem];
        _cancelButton.tintColor = [OddityUISetting shareOddityUISetting].titleColor;
        [_cancelButton setImage:[UIImage oddityImage:@"BackArrow_black"] forState:UIControlStateNormal];
        [_cancelButton oddity_handleControlEvent:UIControlEventTouchUpInside withBlock:b];
        [self addSubview:_cancelButton];
        [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self);
            make.bottom.equalTo(self);
            make.width.equalTo(@44);
            make.height.equalTo(@44);
        }];
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.font = f;
        _titleLabel.text = title;
        _titleLabel.textColor = [OddityUISetting shareOddityUISetting].titleColor;
        [self addSubview:_titleLabel];
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self);
            make.centerY.equalTo(_cancelButton);
        }];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
        
    }
    return self;
}

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)refreshThemeMethod{
    
    self.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    _borderView.backgroundColor = [OddityUISetting shareOddityUISetting].borderColor;
    _cancelButton.tintColor = [OddityUISetting shareOddityUISetting].titleColor;
    _titleLabel.textColor = [OddityUISetting shareOddityUISetting].titleColor;
}

@end
