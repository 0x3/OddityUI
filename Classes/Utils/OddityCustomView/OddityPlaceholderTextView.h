//
//  OddityPlaceholderTextView.h
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import <UIKit/UIKit.h>

@interface OddityPlaceholderTextView : UITextView

/** 占位文字 */
@property (nonatomic, copy) NSString *placeholder;
/** 占位文字颜色 */
@property (nonatomic, strong) UIColor *placeholderColor;
@end
