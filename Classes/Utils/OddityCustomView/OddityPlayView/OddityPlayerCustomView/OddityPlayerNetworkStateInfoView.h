//
//  OddityPlayerNetworkStateInfoView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>

@interface OddityNetworkStateInfoView : UIView

/// 是否正在展示
@property(nonatomic,assign)BOOL isShow;

/// 返回视图
@property (nonatomic,strong,readonly)UIButton *cancelPlayButton;
/// 继续播放视图
@property (nonatomic,strong,readonly)UIButton *continuePlayButton;

/**
 隐藏网络提示视图
 */
-(void)hide;

/**
 展示网络状态 是不是又网络

 @param totalTime 总视频时长
 @param isW 是不是 移动网络
 */
-(void)show:(NSInteger)totalTime isWann:(NSInteger)isW;
@end

