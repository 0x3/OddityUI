//
//  OddityPlayerNetworkStateInfoView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <Masonry/Masonry.h>
#import "OddityPlayerNetworkStateInfoView.h"

@interface OddityNetworkStateInfoView ()

@property (nonatomic,strong)UILabel *messInfoLabel;
@property (nonatomic,strong)UILabel *descInfoLabel;



@end

@implementation OddityNetworkStateInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        _isShow = false;
        self.alpha = 0;
        
        self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
        
        _descInfoLabel = [[UILabel alloc]init];
        _descInfoLabel.text = @"视频时长: 04:30";
        _descInfoLabel.textAlignment = NSTextAlignmentCenter;
        _descInfoLabel.font = [UIFont systemFontOfSize:12];
        _descInfoLabel.textColor = [UIColor whiteColor];
        [self addSubview:_descInfoLabel];
        [_descInfoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self).offset(-19);
            make.centerX.equalTo(self);
        }];
        
        _messInfoLabel = [[UILabel alloc]init];
        _messInfoLabel.text = @"正在使用非WIFI网络,播放将产生流量";
        _messInfoLabel.font = [UIFont systemFontOfSize:16];
        _messInfoLabel.textColor = [UIColor whiteColor];
        _messInfoLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_messInfoLabel];
        [_messInfoLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self);
            make.bottom.equalTo(_descInfoLabel.mas_top).offset(-12);
        }];
        
        _cancelPlayButton = [[UIButton alloc]init];
        _cancelPlayButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_cancelPlayButton setTitle:@"取消播放" forState:(UIControlStateNormal)];
        [_cancelPlayButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _cancelPlayButton.layer.borderColor = [[UIColor whiteColor] CGColor];
        _cancelPlayButton.layer.borderWidth = 0.5;
        _cancelPlayButton.layer.cornerRadius = 2;
        [self addSubview:_cancelPlayButton];
        [_cancelPlayButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.width.equalTo(@82);
            make.height.equalTo(@31);
            make.centerY.equalTo(self).offset(30);
            make.centerX.equalTo(self).offset(50);
        }];
        
        _continuePlayButton = [[UIButton alloc]init];
        _continuePlayButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_continuePlayButton setTitle:@"继续播放" forState:(UIControlStateNormal)];
        [_continuePlayButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        _continuePlayButton.layer.borderColor = [[UIColor whiteColor] CGColor];
        _continuePlayButton.layer.borderWidth = 0.5;
        _continuePlayButton.layer.cornerRadius = 2;
        [self addSubview:_continuePlayButton];
        [_continuePlayButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.width.equalTo(@82);
            make.height.equalTo(@31);
            make.centerY.equalTo(self).offset(30);
            make.centerX.equalTo(self).offset(-50);
        }];
    }
    return self;
}

/// isW
/// 0 网络为 移动网络提醒用户流量费用
/// 1 网络错误
/// 2 未知错误
-(void)show:(NSInteger)totalTime isWann:(NSInteger)isW{
    
    self.alpha = 1;
    
    _descInfoLabel.text = @" ";
    _messInfoLabel.text = @" ";
    
    _isShow = true;
    
    NSInteger durMin = totalTime / 60;//总秒
    NSInteger durSec = totalTime % 60;//总分钟
    
    NSString *infoStr = @"";
    
    if (isW == 0) {
        
        infoStr = @"正在使用非WIFI网络,播放将产生流量";
    }else if(isW == 1){
        
        infoStr = @"播放失败，请检查网络";
    }else if(isW == 2){
        
        infoStr = @"播放失败";
    }
    
    NSString *durStr = @" ";
    
    if (totalTime > 0) {
        durStr = [NSString stringWithFormat:@"%02zd:%02zd", durMin, durSec];
        durStr = [NSString stringWithFormat:@"视频时长: %@",durStr];
    }
    
    _descInfoLabel.text = durStr;
    _messInfoLabel.text = infoStr;
    
    self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
    
    [self.descInfoLabel layoutIfNeeded];
    [self.messInfoLabel layoutIfNeeded];
    [self layoutIfNeeded];
}

-(void)hide{
    
    NSLog(@"隐藏错误");
    self.alpha = 0;
    
    _isShow = false;
}

@end

