//
//  OddityPlayerNormalTopShowInfoView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>

/// 普通的上方信息视图
@interface OddityNormalTopShowInfoView : UIView

/// 是否锁定
@property(nonatomic,assign)BOOL isLock;

/// 标题Label
@property(nonatomic,strong,readonly)UILabel *titleLabel;

/// 更多按钮
@property(nonatomic,strong,readonly)OddityBigButton *moreButton;
/// 锁定按钮
@property(nonatomic,strong,readonly)OddityBigButton *lockButton;
/// 返回按钮
@property(nonatomic,strong,readonly)OddityBigButton *cancelButton;
/// 播放列表按钮
@property(nonatomic,strong,readonly)OddityBigButton *playListButton;

@end
