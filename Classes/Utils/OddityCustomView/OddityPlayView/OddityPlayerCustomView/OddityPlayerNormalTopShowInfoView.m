//
//  OddityPlayerNormalTopShowInfoView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityPlayerNormalTopShowInfoView.h"

@interface OddityNormalTopShowInfoView()

@end

@implementation OddityNormalTopShowInfoView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        _cancelButton = [[OddityBigButton alloc]init];
        [_cancelButton setImage:[UIImage oddityImage:@"video_back"] forState:(UIControlStateNormal)];
        [self addSubview:_cancelButton];
        [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self);
            make.left.equalTo(@(15));
        }];
        
        _moreButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
        _moreButton.tintColor = [UIColor whiteColor];
        [_moreButton setImage:[UIImage oddityImage:@"video_share_black"] forState:(UIControlStateNormal)];
        [self addSubview:_moreButton];
        [_moreButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self);
            make.right.equalTo(@(-15));
        }];
        
        if (![OdditySetting shareOdditySetting].isDontWantToShare) {
            
            _moreButton.hidden = false;
            
            _lockButton = [[OddityBigButton alloc]init];
            [self addSubview:_lockButton];
            [_lockButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.equalTo(self);
                make.right.equalTo(_moreButton.mas_left).offset(-18);
            }];
        }else{
            
            _moreButton.hidden = true;
            
            _lockButton = [[OddityBigButton alloc]init];
            [self addSubview:_lockButton];
            [_lockButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.equalTo(self);
                make.right.equalTo(@(-15));
            }];
        }
        
        _playListButton = [[OddityBigButton alloc] init];
        _playListButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_playListButton setTitle:@"视频列表" forState:(UIControlStateNormal)];
        [_playListButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [_playListButton setTitleColor:[OddityUISetting shareOddityUISetting].mainTone forState:(UIControlStateSelected)];
        [self addSubview:_playListButton];
        [_playListButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.centerY.equalTo(self.lockButton);
            make.right.equalTo(self.lockButton.mas_left).offset(-18);
        }];
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_titleLabel];
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self);
            make.right.equalTo(@-180);
            make.left.equalTo(@39);
        }];
        
        
        self.isLock = false;
        
        [self layoutIfNeeded];
    }
    return self;
}

-(void)setIsLock:(BOOL)isLock{
    
    _isLock = isLock;
    
    if (isLock) {
        
        [_lockButton setImage:[UIImage oddityImage:@"video_lock"] forState:(UIControlStateNormal)];
    }else{
        
        [_lockButton setImage:[UIImage oddityImage:@"video_unlock"] forState:(UIControlStateNormal)];
    }
}

@end
