//
//  OddityPlayerViewNormalBottomView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>
#import "OddityPlayerViewThreeHeightSlider.h"

/// 普通的进度条的展示
@interface OddityPlayerViewNormalBottomView : UIView
/// 是否静音
@property (nonatomic, assign) BOOL isMute;
/// 是否拖拽
@property (nonatomic, assign) BOOL isDragged;
/// 是否拖拽
@property (nonatomic, assign) BOOL isFullScreen;

/// 音量按钮
@property( nonatomic,strong ,readonly) OddityBigButton *volumeButton;
/// 打开按钮
@property( nonatomic,strong ,readonly) OddityBigButton *openButton;
/// 进度条对象
@property ( nonatomic,strong,readonly) OddityPlayerViewThreeHeightSlider *progressSlider;


/**
 设置缓存的位置

 @param value 进度
 */
- (void)playerCacheProgress:(CGFloat)value;


/**
 设置下方显示 文字

 @param currentTime 当前播放时间
 @param totalTime 总时长
 @param value 进度
 */
- (void)playerCurrentTime:(NSInteger)currentTime totalTime:(NSInteger)totalTime sliderValue:(CGFloat)value;

@end
