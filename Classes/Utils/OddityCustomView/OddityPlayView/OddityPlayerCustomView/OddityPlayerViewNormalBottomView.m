//
//  OddityPlayerViewNormalBottomView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityPlayerViewNormalBottomView.h"

@interface OddityPlayerViewNormalBottomView()

@property ( nonatomic,strong) UIProgressView *cacheProgress;


@property ( nonatomic,strong) UILabel *totalTimeLabel;
@property ( nonatomic,strong) UILabel *currentTimeLabel;


@property( nonatomic,strong ) UIImageView *backView;
@end

@implementation OddityPlayerViewNormalBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _backView = [[UIImageView alloc]initWithImage:[UIImage oddityImage:@"video_bg_black"]];
        [self addSubview:_backView];
        
        _volumeButton = [[OddityBigButton alloc] init];
        [_volumeButton setImage:[UIImage oddityImage:@"video_volume"] forState:(UIControlStateNormal)];
        [self addSubview:_volumeButton];
        
        _currentTimeLabel = [[UILabel alloc]init];
        _currentTimeLabel.text = @"00:00";
        _currentTimeLabel.font = [UIFont systemFontOfSize:12];
        _currentTimeLabel.textColor = [UIColor whiteColor];
        [self addSubview:_currentTimeLabel];
        
        _openButton = [[OddityBigButton alloc] init];
        [_openButton setImage:[UIImage oddityImage:@"video_max"] forState:(UIControlStateNormal)];
        [self addSubview:_openButton];
        
        _totalTimeLabel = [[UILabel alloc]init];
        _totalTimeLabel.text = @"00:00";
        _totalTimeLabel.font = [UIFont systemFontOfSize:12];
        _totalTimeLabel.textColor = [UIColor whiteColor];
        [self addSubview:_totalTimeLabel];
        
        _cacheProgress = [[UIProgressView alloc]init];
        _cacheProgress.trackTintColor = [UIColor color_t_11];
        _cacheProgress.progressTintColor = [UIColor color_t_4];
        [self addSubview:_cacheProgress];
        
        _progressSlider = [[OddityPlayerViewThreeHeightSlider alloc]init];
        _progressSlider.minimumValue = 0;
        _progressSlider.maximumValue = 1;
        _progressSlider.minimumTrackTintColor = [UIColor color_t_7];
        _progressSlider.maximumTrackTintColor = [UIColor clearColor];
        [_progressSlider setThumbImage:[UIImage oddityImage:@"video_huakuai"] forState:(UIControlStateNormal)];
        
        [self addSubview:_progressSlider];
        
        [self layoutMakeMethod];
        
        self.isFullScreen = false;
        self.isMute = false;
    }
    return self;
}

-(void)layoutMakeMethod{
    
    [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@0);
        make.bottom.equalTo(@0);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
    }];
    
    [_volumeButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self);
        make.left.equalTo(@12);
    }];
    
    [_currentTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(self);
        make.left.equalTo(_volumeButton.mas_right).offset(14);
    }];
    
    [_openButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(@-12);
        make.centerY.equalTo(self);
    }];
    
    [_totalTimeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(_openButton.mas_left).offset(-14);
        make.centerY.equalTo(self);
    }];
    
    [_cacheProgress mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(_currentTimeLabel.mas_right).offset(10);
        make.right.equalTo(_totalTimeLabel.mas_left).offset(-10);
        make.centerY.equalTo(self);
    }];
    
    [_progressSlider mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(_cacheProgress);
        make.right.equalTo(_cacheProgress);
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
            make.centerY.equalTo(self).offset(-1.5);
        }else if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            make.centerY.equalTo(self).offset(-0.5);
        }else{
            make.centerY.equalTo(self);
        }
    }];
}





- (void)playerCacheProgress:(CGFloat)value {
    
    _cacheProgress.progress = value;
}

- (void)playerCurrentTime:(NSInteger)currentTime totalTime:(NSInteger)totalTime sliderValue:(CGFloat)value {
    NSInteger proMin = currentTime / 60;//当前秒
    NSInteger proSec = currentTime % 60;//当前分钟
    NSInteger durMin = totalTime / 60;//总秒
    NSInteger durSec = totalTime % 60;//总分钟
    if (!self.isDragged) {
        self.progressSlider.value           = value;
    }
    // 更新总时间
    self.totalTimeLabel.text = [NSString stringWithFormat:@"%02zd:%02zd", durMin, durSec];
    self.currentTimeLabel.text       = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];
    
    [self layoutIfNeeded];
    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
}

-(void)setIsFullScreen:(BOOL)isFullScreen{
    
    if (_isFullScreen == isFullScreen) {
        
        return;
    }
    
    _isFullScreen = isFullScreen;
    
    if (isFullScreen) {
        
        [_openButton setImage:[UIImage oddityImage:@"video_mini"] forState:(UIControlStateNormal)];
    }else{
        
        [_openButton setImage:[UIImage oddityImage:@"video_max"] forState:(UIControlStateNormal)];
    }
    
    [[UIViewController oddityCurrentViewController] setNeedsStatusBarAppearanceUpdate];
}

-(void)setIsMute:(BOOL)isMute{
    
    if (_isMute == isMute) {
        
        return;
    }
    _isMute = isMute;
    
    if (isMute) {
        
        [_volumeButton setImage:[UIImage oddityImage:@"video_mute"] forState:(UIControlStateNormal)];
        
    }else{
        
        [_volumeButton setImage:[UIImage oddityImage:@"video_volume"] forState:(UIControlStateNormal)];
    }
}

@end
