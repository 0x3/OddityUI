//
//  OddityPlayerViewSmallBottomView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>

@interface OddityPlayerViewSmallBottomView : UIView


/**
 时间进度视频
 */
@property ( nonatomic,strong,readonly) UIProgressView *timerProgress;


/**
 当前播放的事件进度

 @param value 进度
 */
- (void)playerCurrentTime:(CGFloat)value;


/**
 当前缓存的进度

 @param value 进度
 */
- (void)playerCacheProgress:(CGFloat)value;

@end
