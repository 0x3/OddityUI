//
//  OddityPlayerViewSmallBottomView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityPlayerViewSmallBottomView.h"

@interface OddityPlayerViewSmallBottomView ()

@property ( nonatomic,strong) UIProgressView *cacheProgress;
@end

@implementation OddityPlayerViewSmallBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _cacheProgress = [[UIProgressView alloc]init];
        _cacheProgress.trackTintColor = [UIColor color_t_11];
        _cacheProgress.progressTintColor = [UIColor color_t_4];
        [self addSubview:_cacheProgress];
        
        _timerProgress = [[UIProgressView alloc]init];
        _timerProgress.trackTintColor = [UIColor clearColor];
        _timerProgress.progressTintColor = [UIColor color_t_7];
        [self addSubview:_timerProgress];
        
        [self layoutMakeMethod];
    }
    return self;
}

-(void)layoutMakeMethod{
    
    [_cacheProgress mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(@0);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        make.height.equalTo(@1);
    }];
    
    [_timerProgress mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(@0);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        make.height.equalTo(@1);
    }];
}

- (void)playerCacheProgress:(CGFloat)value {
    
    _cacheProgress.progress = value;
}

- (void)playerCurrentTime:(CGFloat)value {
    
    _timerProgress.progress = value;
}

@end
