//
//  OddityPlayerViewSoundView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>
#import "OddityPlayerView.h"
#import <MediaPlayer/MediaPlayer.h>

typedef NS_ENUM(NSInteger, OddityDragPlayViewType) {
    
    OddityDragPlayViewTypeNormal = -1,
    OddityDragPlayViewTypeForward = 0,
    OddityDragPlayViewTypeBackward = 1,
    OddityDragPlayViewTypeVolume = 2,
    OddityDragPlayViewTypeBrightness = 3
};


/// 关于声音 亮度 等操作的提示 视图
@interface OdditySoundView : UIView


/**
 获得 单利 模式下的 音量调节视图

 @return 对象
 */
+(instancetype)shareSoundView;

/**
 获得单例模式 的 系统音量控制视图

 @return 视图
 */
+(MPVolumeView *) shareVolumeView;


/**
 展示进度并且根据设置进度

 @param view 展示的view
 @param val 进度
 */
+ (void)showViewInView:(UIView *)view value:(CGFloat)val;

/**
 显示在uiview上

 @param type 类型
 @param val 进度
 */
- (void)showViewFor:(OddityDragPlayViewType)type value:(CGFloat)val;

/**
 设置快进状态下的一些参数

 @param draggedTime 拖拽的时间
 @param totalTime 总时长
 @param forawrd 是否快进
 */
- (void)playerDraggedTime:(NSInteger)draggedTime totalTime:(NSInteger)totalTime isForward:(BOOL)forawrd;

@end

