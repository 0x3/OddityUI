//
//  OddityPlayerViewSoundView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityPlayerViewSoundView.h"

@interface OdditySoundView ()

@property(nonatomic,strong)UILabel *timeLabel;
@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UIProgressView *progressView;

@end

@implementation OdditySoundView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.layer.cornerRadius = 4;
        self.clipsToBounds = true;
        
        _imageView = [[UIImageView alloc]init];
        _imageView.image = [UIImage oddityImage:@"video_volume2"];
        [self addSubview:_imageView];
        [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self);
            make.top.equalTo(@16);
        }];
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.text = @"00:00/00:00";
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont systemFontOfSize:13];
        [self addSubview:_timeLabel];
        [_timeLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_imageView.mas_bottom).offset(14);
            make.centerX.equalTo(self);
        }];
        
        _progressView = [[UIProgressView alloc]init];
        [self addSubview:_progressView];
        _progressView.trackTintColor =  [[UIColor whiteColor] colorWithAlphaComponent:0.2];
        _progressView.progressTintColor = [UIColor whiteColor];
        [_progressView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(@14);
            make.right.equalTo(@-14);
            make.width.equalTo(@91);
            make.height.equalTo(@2);
            make.top.equalTo(_timeLabel.mas_bottom).offset(14);
            make.bottom.equalTo(@-14);
        }];
    }
    return self;
}

+(MPVolumeView *)shareVolumeView{
    static MPVolumeView* volumeView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        volumeView = [[MPVolumeView alloc]initWithFrame:CGRectMake(-100, -100, 100, 100)];
    });
    
    return volumeView;
}

+(instancetype)shareSoundView{
    static OdditySoundView* soundView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        soundView = [[OdditySoundView alloc]init];;
    });
    
    return soundView;
}


+(void)showViewInView:(UIView *)view value:(CGFloat)val{
    
    [self cancelAutoFadeOutControlView];
    
    [OdditySoundView shareSoundView].alpha = 1;
    
    [OdditySoundView shareSoundView].imageView.image = [UIImage oddityImage:@"video_volume2"];
    
    [OdditySoundView shareSoundView].timeLabel.text = @"音量";
    [OdditySoundView shareSoundView].progressView.progress = val;
    
    [OdditySoundView shareSoundView].backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.8];
    
    [view addSubview:[OdditySoundView shareSoundView]];
    [[OdditySoundView shareSoundView] mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(view);
    }];
    
    [view layoutIfNeeded];
    
    [self autoFadeOutControlView];
}

+ (void)autoFadeOutControlView {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(playerHideControlView) object:nil];
    [self performSelector:@selector(playerHideControlView) withObject:nil afterDelay:3.0f];
}

+ (void)cancelAutoFadeOutControlView {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(playerHideControlView) object:nil];
}

+ (void)playerHideControlView{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [OdditySoundView shareSoundView].alpha = 0;
    }];
}

-(void)showViewFor:(OddityDragPlayViewType)type value:(CGFloat)val{
    
    switch (type) {
        case OddityDragPlayViewTypeVolume:{
            
            self.imageView.image = [UIImage oddityImage:@"video_volume2"];
            
            self.timeLabel.text = @"音量";
            self.progressView.progress = val;
        }
            break;
        case OddityDragPlayViewTypeBrightness:{
            
            self.imageView.image = [UIImage oddityImage:@"video_light"];
            
            self.timeLabel.text = @"亮度";
            self.progressView.progress = val;
        }
            break;
        default:
            break;
    }
    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
}

- (void)playerDraggedTime:(NSInteger)draggedTime totalTime:(NSInteger)totalTime isForward:(BOOL)forawrd  {
    
    NSInteger proMin = draggedTime / 60;//当前秒
    NSInteger proSec = draggedTime % 60;//当前分钟
    
    //duration 总时长
    NSInteger durMin = totalTime / 60;//总秒
    NSInteger durSec = totalTime % 60;//总分钟
    
    NSString *currentTimeStr = [NSString stringWithFormat:@"%02zd:%02zd", proMin, proSec];
    NSString *totalTimeStr   = [NSString stringWithFormat:@"%02zd:%02zd", durMin, durSec];
    CGFloat  draggedValue    = (CGFloat)draggedTime/(CGFloat)totalTime;
    NSString *timeStr        = [NSString stringWithFormat:@"%@ / %@", currentTimeStr, totalTimeStr];
    
    if (forawrd) {
        self.imageView.image = [UIImage oddityImage:@"video_fast_forward"];
    } else {
        self.imageView.image = [UIImage oddityImage:@"video_fast_backward"];
    }
    
    self.timeLabel.text = timeStr;
    self.progressView.progress = draggedValue;
    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
}

@end
