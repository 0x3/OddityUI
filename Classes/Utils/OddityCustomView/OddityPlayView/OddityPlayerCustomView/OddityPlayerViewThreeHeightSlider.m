//
//  OddityPlayerViewThreeHeightSlider.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityPlayerViewThreeHeightSlider.h"

@implementation OddityPlayerViewThreeHeightSlider

-(CGRect)trackRectForBounds:(CGRect)bounds{
    
    CGRect rect = [super trackRectForBounds:bounds];
    
    rect.size.height = 3;
    
    return rect;
}

-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event{
    
    CGRect btnBounds = self.bounds;
    
    btnBounds = CGRectInset(btnBounds, -10, -10);
    
    return CGRectContainsPoint(btnBounds, point);
}

@end
