//
//  OddityPlaylistView.h
//  Pods
//
//  Created by 荆文征 on 2017/5/9.
//
//

#import <UIKit/UIKit.h>

@interface OddityPlaylistView : UIView

@property(nonatomic,strong)UICollectionView *collectionView;

@end
