//
//  OddityPlaylistView.m
//  Pods
//
//  Created by 荆文征 on 2017/5/9.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>

#import "OddityPlaylistView.h"

#import "OddityPlaylistCollectionViewCell.h"

@interface OddityPlaylistView()

@end

@implementation OddityPlaylistView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self makeCollectionView];
        
    }
    return self;
}

-(void)makeCollectionView{

    UICollectionViewFlowLayout *customLayout = [[UICollectionViewFlowLayout alloc] init];
    
    customLayout.minimumLineSpacing = 8;
    customLayout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8);
    customLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    customLayout.itemSize = CGSizeMake(301-16, 81);
    
    _collectionView = [[UICollectionView alloc] initWithFrame:(CGRectZero) collectionViewLayout:customLayout];
    [_collectionView registerClass:[OddityPlaylistCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    _collectionView.backgroundColor = [UIColor color_t_14];
    [self addSubview:_collectionView];
    [_collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.with.bottom.with.left.with.right.mas_equalTo(0);
    }];
    
    self.backgroundColor = [UIColor color_t_14];
    self.userInteractionEnabled = true;
}
@end
