//
//  OddityPlayerObject.h
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import <Foundation/Foundation.h>

@interface OddityPlayerObject : NSObject


/**
 新闻id
 
 用于前往新闻详情页时 所需要填写的字段
 */
@property (nonatomic,assign,readonly) int nid;


/**
 新闻对象
 
 用于各种显示模式的 判断条件
 */
@property (nonatomic,strong,readonly) id object;


/**
 视频播放的url
 */
@property (nonatomic,strong,readonly) NSString *url;


/**
 食品的 标题
 */
@property (nonatomic,strong,readonly) NSString *title;

-(instancetype)initWith:(int)nid VideoUrl:(NSString *)url Title:(NSString *)title Objtect:(id)obj;

@end
