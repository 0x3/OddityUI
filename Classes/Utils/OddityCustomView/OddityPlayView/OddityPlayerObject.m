//
//  OddityPlayerObject.m
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import "OddityPlayerObject.h"

@implementation OddityPlayerObject

-(instancetype)initWith:(int)nid VideoUrl:(NSString *)url Title:(NSString *)title Objtect:(id)obj{
    
    self = [super init];
    
    if (self) {
        
        _nid = nid;
        _url = url;
        _title = title;
        _object = obj;
    }
    
    return self;
}

@end

