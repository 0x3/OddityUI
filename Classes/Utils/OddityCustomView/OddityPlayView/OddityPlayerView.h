//
//  OddityPlayerView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityPlaylistView.h"

#import <UIKit/UIKit.h>
#import "OddityPlayerObject.h"
#import "OddityPlayerViewInterface.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "OddityMainManagerViewController.h"

#define isPlayList true


/// 网络发生变化事件儿
FOUNDATION_EXPORT NSNotificationName const oddityNetworkStateChanged;
/// 小视频状态的单击事件儿
FOUNDATION_EXPORT NSNotificationName const odditySinTapPlayViewWithSmall;
/// 视频完成播放的事件儿
FOUNDATION_EXPORT NSNotificationName const oddityVideoIsFinishPlayerEd;



@interface OddityPlayerView : UIView<OddityPlayerViewInterface>

/// 小屏幕状态下的 相对于屏幕的位置
@property (nonatomic, assign,readonly) CGRect smallRelativeToScreenFrame;

/// 视频视图上的 手指滑动手势
@property (nonatomic, strong,readonly) UIPanGestureRecognizer *singlePan;

/// 老播放视图背景  用户全屏恢复的调用
@property (nonatomic, strong,readwrite) UIView *oldPlayBackView;

/// 播放列表视图
@property(nonatomic,strong)OddityPlaylistView *playListView;
/// 视频播放对象
@property (nonatomic, strong,readonly) OddityPlayerObject *videoObject;

/// 当前播放的时间
@property (nonatomic, assign,readonly) NSInteger currentTime;

/// 是否静音
@property (nonatomic, assign,readonly) BOOL isMute;
/// 是否拖拽
@property (nonatomic, assign,readonly) BOOL isDragged;
/// 是不是全屏
@property (nonatomic, assign,readonly) BOOL isFullScreen;
/// 是不是全屏
@property (nonatomic, assign,readonly) BOOL isReadyToPlay;
/// 是否正在旋转屏幕
@property (nonatomic, assign,readonly) BOOL isRotatingScreen;
/// 是否显示播放器
@property (nonatomic, assign,readonly) BOOL isPlayerViewShow;

/// 当前食品的播放状态
@property (nonatomic,assign,readonly) enum OddityPlayViewState playViewState;

@property(nonatomic,strong)OddityMainManagerViewController *mainManagerViewController;

-(instancetype)playWithPlayerObject:(OddityPlayerObject *)object;

-(void)recoveryPlayViewNoramlState;
/**
 取消静音
 */
-(void)makeNoMuteMthod;
@end

