//
//  OddityPlayerView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityPlayerView.h"

#import "OddityShareViewController.h"
#import "OddityNewsDetailViewController.h"

#import "OddityMMMaterialDesignSpinner.h"

#import "OddityManagerInfoMationUtil.h"

#import "OddityNewArrayListViewController.h"

#import "OddityPlayerViewSoundView.h"
#import "OddityPlayerViewSmallBottomView.h"
#import "OddityPlayerNetworkStateInfoView.h"
#import "OddityPlayerViewNormalBottomView.h"
#import "OddityPlayerNormalTopShowInfoView.h"

typedef NS_ENUM(NSInteger, OddityPlayState) {
    
    OddityPlayStateFailed = -1,
    OddityPlayStateBuffering = 0,
    OddityPlayStatePlaying = 1,
    OddityPlayStateStopped = 2,
    OddityPlayStatePause = 3
};

NSNotificationName const oddityNetworkStateChanged =  @"oddityNetworkChange";
NSNotificationName const oddityVideoIsFinishPlayerEd =  @"oddityVideoIsFinishPlayerEd";
NSNotificationName const odditySinTapPlayViewWithSmall =  @"odditySinTapPlayViewWithSmall";


//static const CGFloat OddityPlayerScale = 1.0f/3.0f;

/// 自动隐藏时间
static const CGFloat OddityPlayerAnimationTimeInterval             = 5.0f;
static const CGFloat OddityPlayerControlBarAutoFadeOutTimeInterval = 0.35f;


@interface OddityPlayerView ()<UIGestureRecognizerDelegate>

/// 即使在流量网络环境下 仍然继续播放
@property (nonatomic, assign) BOOL isContinueWithWaan;

/// 视频新闻对象id
@property (nonatomic, assign) int nid;
/// 视频新闻对象
@property (nonatomic, strong) id object;
/// 视频新闻对象url
@property (nonatomic, strong) NSString *url;
/// 视频的标题
@property (nonatomic, strong) NSString *title;


/// 小视频状态下的关闭按钮
@property (nonatomic, strong) OddityBigButton *smallCloseButton;

/// 根据视频的大小 来决定 Small 的视频比例
@property (nonatomic,assign) CGRect videoRect;

/// 记录小屏幕视频用户pan的位置
@property (nonatomic,assign) CGPoint sumPoint;

/// 是否在播放
@property (nonatomic, assign) BOOL isPlaying;

/// 是否因为暂停
@property (nonatomic, assign) BOOL isPauseForDidEnterBackground;
/// 是否展示时间进度条等
@property (nonatomic, assign) BOOL isShowing;
/// 是否展示播放列表
@property (nonatomic, assign) BOOL isShowPlayList;

/// 需要快进 和 需要快进到的时间
@property (nonatomic, assign) BOOL isNeedToSeek;
@property (nonatomic, assign) NSInteger seekTime;

/// 用户滑动的类型
@property (nonatomic,assign) OddityDragPlayViewType dragPlayViewType;
/// 播放器播放状态
@property (nonatomic,assign) OddityPlayState playState;

@property (nonatomic, strong) OddityMMMaterialDesignSpinner *activity;

/// 监听事件变化
@property (nonatomic, strong) id timeObserve;

/// 视频时间相关
@property (nonatomic, assign) CGFloat sumTime; // -> 仅用于 快进时的 时间记录
@property (nonatomic, assign) CGFloat totalTime; // -> 视频的总时长
@property (nonatomic, assign) CGFloat cacheTime; // -> 当前缓存的时长

/// 系统音量滑杆
@property (nonatomic, strong) UISlider *volumeViewSlider;
@property (nonatomic, strong) UIButton *playOtPauseButton;
@property (nonatomic, strong) UITapGestureRecognizer *singleTap;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTap;

/// 播放器相关
@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic,strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;

/// 声音视图
@property(nonatomic,strong)OdditySoundView *soundView;

@property(nonatomic,strong)UIImageView *topBackView;
/// 普通状态下的下方视图
@property(nonatomic,strong)OddityPlayerViewNormalBottomView *nbottomView;
@property(nonatomic,strong)OddityNetworkStateInfoView *networkStateInfoView;
@property(nonatomic,strong)OddityNormalTopShowInfoView *normalTopShowInfoView;
/// Small 状态下的视图
@property(nonatomic,strong)OddityPlayerViewSmallBottomView *sbottomView;


@end

@implementation OddityPlayerView

static OddityPlayerView *playView;

-(BOOL)isFullScreen{
    
    return self.nbottomView.isFullScreen;
}

-(BOOL)isMute{
    
    return self.nbottomView.isMute;
}

-(CGRect)smallRelativeToScreenFrame{
    return CGRectMake(_sumPoint.x, _sumPoint.y, CGRectGetWidth(self.videoRect), CGRectGetHeight(self.videoRect));
}

-(BOOL)isDragged{
    
    return self.nbottomView.isDragged;
}


+(instancetype)sharePlayerView{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        playView = [[OddityPlayerView alloc]init];
        playView.layer.zPosition = MAXFLOAT;
        [playView initForLayoutMethod];
    });
    
    return playView;
}

+(void)showSoundViewInView:(UIView *)view value:(CGFloat)val{
    
    [OdditySoundView showViewInView:view value:val];
}



-(void)toNeedJudgmentState{
    
    _playViewState = OddityPlayViewStateNeedJudgment;
}


/// 设置播放器的播放模式
-(void)toState:(OddityPlayViewState)state inView:(UIView *)view{
    
    /// 如果正在转屏 或者 当前正在为全屏 不进行操作
    if (self.isRotatingScreen ) { return; }
    
    [self GoToState:state inView:view];
}


-(void)GoToState:(OddityPlayViewState)state inView:(UIView *)view{
    
    if ((state == self.playViewState || ( state == OddityPlayViewStateNormal && view == nil )) || !_isPlayerViewShow) { return ;}
    
    _playViewState = state;
    
    self.smallCloseButton.alpha = (state == OddityPlayViewStateSmall ? 1 : 0); // 隐藏点击关闭按钮
    
    switch (state) {
        case OddityPlayViewStateNormal:{
            
            self.hidden = false;
            
            [view.superview addSubview:self];
            [view.superview bringSubviewToFront:self];
            
            self.oldPlayBackView = view; /// 设置该属性 在从全屏回到 normal的时候 使用
            
            [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.edges.equalTo(view).insets(UIEdgeInsetsZero);
            }];
            
            [self layoutSubviews];
        }
            break;
        case OddityPlayViewStateSmall:{
            
            if (self.player.rate == 0 && !self.isPlaying) {
                
                [self Pause];
                self.hidden = true;
            }else{
                
                [self playerHideControlView];
                [[[UIApplication sharedApplication]keyWindow] addSubview:self];
                [[[UIApplication sharedApplication]keyWindow] bringSubviewToFront:self];
                
                [self updateSmallVideo];
            }
        }
            break;
        case OddityPlayViewStateFull:{
            
        }
            break;
        default:
            break;
    }
    self.normalTopShowInfoView.playListButton.hidden = true;
    
#if isPlayList
    
    //    BOOL isNewDetailViewController = [[UIViewController oddityCurrentViewController] isKindOfClass:[OddityNewsDetailViewController class]];
    //
    //    BOOL isVideoViewController = self.mainManagerViewController.currentViewController.channel.channel.isVideoChannel;
    //
    //    BOOL isFullStatePlayer = state == OddityPlayViewStateFull;
    
    self.normalTopShowInfoView.playListButton.hidden = [[UIViewController oddityCurrentViewController] isKindOfClass:[OddityNewsDetailViewController class]] || !self.mainManagerViewController.currentViewController.channel.channel.isVideoChannel || state != OddityPlayViewStateFull;
#endif
    
    if (state == OddityPlayViewStateFull) {
        
        [self playerShowControlView];
        [self autoFadeOutControlView];
    }else{
        
        [self playerHideControlView];
    }
    
    self.playOtPauseButton.hidden = state == OddityPlayViewStateSmall;
    self.networkStateInfoView.hidden = state == OddityPlayViewStateSmall;
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

/// 更新小试图的大小
-(void)abimateUpdateSmallVideo{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self updateSmallVideo];
    }];
}


-(void)goDetailViewController{
    
    OddityPlayerView *playView = self;
    
    OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc]initWithNid:playView.nid isVideo:true];
    
    playView.smallCloseButton.alpha = 0;
    
    [newDACViewController configVideoUrl:playView.url Object:playView.object];
    
    if (playView.object && [playView.object isKindOfClass:[OddityNew class]]) {
        
        OddityNew *nobj = (OddityNew *)playView.object;
        
        [newDACViewController makeLogType:nobj.logtype AndLogChid:nobj.logchid cid:nobj.channel];
    }
    
    [[UIViewController oddityCurrentViewController] presentViewController:newDACViewController animated:true completion:nil];
}


-(void)updateSmallVideo{
    
    if (self.playViewState != OddityPlayViewStateSmall || !self.superview) { return ;}
    
    if (_sumPoint.x >= 0 || _sumPoint.y >= 0 ) {
        
        _sumPoint = CGPointMake(-8, -8);
    }
    
    [self mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(@(_sumPoint.y));
        make.right.equalTo(@(_sumPoint.x));
        
        make.width.mas_equalTo(188);
        make.height.mas_equalTo(106);
    }];
    
//    if (CGRectEqualToRect(self.videoRect, CGRectZero)) {
//        
//        [self mas_updateConstraints:^(MASConstraintMaker *make) {
//            
//            make.width.equalTo([[UIApplication sharedApplication]keyWindow]).multipliedBy(OddityPlayerScale);
//            make.height.equalTo(self.mas_width);
//        }];
//    }else{
//        
//        CGFloat width = CGRectGetWidth(self.videoRect);
//        CGFloat height = CGRectGetHeight(self.videoRect);
//        
//        if (height < width) {
//            
//            [self mas_updateConstraints:^(MASConstraintMaker *make) {
//                
//                make.width.equalTo([[UIApplication sharedApplication]keyWindow].mas_width).multipliedBy(OddityPlayerScale);
//                make.height.equalTo(self.mas_width).multipliedBy((CGFloat)height/width);
//            }];
//        }else{
//            
//            [self mas_updateConstraints:^(MASConstraintMaker *make) {
//                
//                make.height.equalTo([[UIApplication sharedApplication]keyWindow].mas_width).multipliedBy(OddityPlayerScale);
//                make.width.equalTo(self.mas_height).multipliedBy((CGFloat)width/height);
//            }];
//        }
//    }
    
    [[[UIApplication sharedApplication]keyWindow] layoutSubviews];
    
    [self refreshSmallPlayerViewLocation];
}




/// 重置
-(void)reset{
    
    _isReadyToPlay = false;
    
    self.backgroundColor = [UIColor blackColor];
    
    self.isPauseForDidEnterBackground = false;
    
    self.playState = OddityPlayStateStopped;

    [self Pause];
    [self.playerLayer removeFromSuperlayer];
//    [self.player replaceCurrentItemWithPlayerItem:nil];
    
    _totalTime = -1;
    _cacheTime = -1;
    _currentTime = -1;
    
//    self.object = nil;
//    self.playerItem = nil;
//    self.player = nil;
//    self.playerLayer = nil;
    
    self.videoRect = CGRectZero;
    
    [self.sbottomView playerCurrentTime:0];
    [self.nbottomView playerCurrentTime:0 totalTime:0 sliderValue:0];
    
    
    [self.sbottomView playerCacheProgress:0];
    [self.nbottomView playerCacheProgress:0];
    
    [self toNeedJudgmentState];
    
//    [super layoutSubviews];
//    [self layoutIfNeeded];
}


-(instancetype)playWithPlayerObject:(OddityPlayerObject *)object{
    
    [self reset];
    
    [self toNeedJudgmentState];
    
    _videoObject = object;
    
    _nid = object.nid;
    _url = object.url;
    _object = object.object;
    _title = object.title;
    
    _isPlayerViewShow = true;
    self.normalTopShowInfoView.titleLabel.text = _title;
    
    switch ([[OddityManagerInfoMationUtil managerInfoMation].networkCode intValue]) {
        case 0:{ // 位置网络
        
            [self show:0 isWann:1];
            return self;
        }
            break;
        case 2:case 3:case 4:{ // 流量网络下
            
            if (!self.isContinueWithWaan) {
                
                [self show:0 isWann:0];
                
                return self;
            }
        }
            break;
            
        default:
            break;
    }
    
    NSURL *videoUrl =  [[NSURL alloc]initWithString:_url];
    self.playerItem = [[AVPlayerItem alloc]initWithURL:videoUrl];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    self.player.muted = self.nbottomView.isMute;
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    self.playerLayer.backgroundColor = [UIColor blackColor].CGColor;
    
    [self createTimer];
    
    self.playState = OddityPlayStateBuffering;
    [self Play];
    
    [self.playListView.collectionView reloadData];
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self layoutIfNeeded];
    self.playerLayer.frame = self.bounds;
}

-(void)initForLayoutMethod{
    
    self.clipsToBounds = true;
    
    _nbottomView = [[OddityPlayerViewNormalBottomView alloc]init];
    _nbottomView.alpha = 0;
    [self addSubview:_nbottomView];
    
    
    [_nbottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(@0);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        make.height.equalTo(@40);
    }];
    
    _sbottomView = [[OddityPlayerViewSmallBottomView alloc]init];
    _sbottomView.alpha = 0;
    [self addSubview:_sbottomView];
    
    [_sbottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(@0);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        make.height.equalTo(@1);
    }];
    
    _soundView = [[OdditySoundView alloc]init];
    _soundView.alpha = 0;
    [self addSubview:_soundView];
    [_soundView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(self);
    }];
    _playOtPauseButton = [[UIButton alloc]init];
    
    _topBackView = [[UIImageView alloc]init];
    _topBackView.userInteractionEnabled = true;
    _topBackView.image = [UIImage oddityImage:@"video_top_shadow"];
    [self addSubview:_topBackView];
    [_topBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@(0));
        make.height.equalTo(@(64));
        make.left.equalTo(self);
        make.right.equalTo(self);
    }];
    
    _normalTopShowInfoView = [[OddityNormalTopShowInfoView alloc] init];
    [self.topBackView addSubview:_normalTopShowInfoView];
    [_normalTopShowInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@(20));
        make.left.equalTo(@(0));
        make.right.equalTo(@(0));
        
        make.bottom.equalTo(@(0));
    }];
    
    
    _networkStateInfoView = [[OddityNetworkStateInfoView alloc]init];
    _networkStateInfoView.alpha = 0;
    [self addSubview:_networkStateInfoView];
    [_networkStateInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).insets(UIEdgeInsetsZero);
    }];
    
    _activity = [[OddityMMMaterialDesignSpinner alloc] init];
    _activity.lineWidth = 1;
    _activity.duration  = 1;
    _activity.tintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
    [self addSubview:_activity];
    [self.activity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.width.with.height.mas_equalTo(45);
    }];
    
    _smallCloseButton = [[OddityBigButton alloc]init];
    [_smallCloseButton setImage:[UIImage oddityImage:@"video_close"] forState:(UIControlStateNormal)];
    [self addSubview:_smallCloseButton];
    [_smallCloseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(@0);
        make.left.equalTo(@0);
        make.width.equalTo(@26);
        make.height.equalTo(@26);
    }];
    
    _playOtPauseButton.alpha = 0;
    [_playOtPauseButton setImage:[UIImage oddity_video_play_image] forState:(UIControlStateNormal)];
    [_playOtPauseButton addTarget:self action:@selector(doubleTapAction) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:_playOtPauseButton];
    [_playOtPauseButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(self);
    }];
    
    _playViewState = OddityPlayViewStateNeedJudgment;
    
    [self configureVolume];
    
    
    _playListView = [[OddityPlaylistView alloc] init];

#if isPlayList
    [self addSubview:_playListView];
    [_playListView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(64);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-3);
        make.width.mas_equalTo(301);
    }];
#endif
    
    [self createControlSelectorPlayView];
    [self createSelfNotificationsPlayView];
    [self createGestureRecognizerTargetPlayView];
    
    [self layoutIfNeeded];
    
    _playListView.transform = CGAffineTransformTranslate(_playListView.transform, CGRectGetWidth(_playListView.frame), 0);
}

















-(void)toEmptyState{
    
    [self reset];
    _isPlayerViewShow = false;
}



-(void)Play{
    
    [self.networkStateInfoView hide];
    
    [self HidePlayListViewMethod];
    
    self.isPlaying = true;
    self.playState = OddityPlayStatePlaying;
    [self.player play];
    [self playerHideControlView];
}


-(void)Pause{
    
    self.isPlaying = false;
    self.playState = OddityPlayStatePause;
    [self.player pause];
    if (self.playViewState == OddityPlayViewStateNormal) {
        [self playerShowControlView];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[AVAudioSession sharedInstance] setActive:false withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    });
}


-(void)setIsPlaying:(BOOL)isPlaying{
    
    _isPlaying = isPlaying;
    
    if (_isPlaying) {
        
        [_playOtPauseButton setImage:[UIImage oddity_video_pause_image] forState:(UIControlStateNormal)];
    }else{
        
        [_playOtPauseButton setImage:[UIImage oddity_video_play_image] forState:(UIControlStateNormal)];
    }
}




-(void)setPlayState:(OddityPlayState)playState{
    
    if (playState == OddityPlayStateBuffering) {
        
        self.playOtPauseButton.alpha = 0;
        
        [self.activity startAnimating];
    }else{
        
        if (_isShowing) {
            
            self.playOtPauseButton.alpha = 1;
        }
        
        [self.activity stopAnimating];
    }
}





/**
 *  自动隐藏 控制 视图
 */
- (void)autoFadeOutControlView {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(playerHideControlView) object:nil];
    [self performSelector:@selector(playerHideControlView) withObject:nil afterDelay:OddityPlayerAnimationTimeInterval];
}

/**
 取消隐藏 控制视图
 */
- (void)cancelAutoFadeOutControlView {
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(playerHideControlView) object:nil];
}


/**
 显示 控制视图
 */
- (void)playerShowControlView {
    if (self.isShowPlayList) {
        return;
    }
    _isShowing = true;
    __weak typeof(self) weakSelf = self;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(playerHideControlView) object:nil];
    [UIView animateWithDuration:OddityPlayerControlBarAutoFadeOutTimeInterval animations:^{
        
        weakSelf.nbottomView.alpha = 1;
        weakSelf.sbottomView.alpha = 0;
        weakSelf.playOtPauseButton.alpha = 1;
        
//        weakSelf.sbottomView.transform = CGAffineTransformTranslate(weakSelf.sbottomView.transform, 0, CGRectGetHeight(weakSelf.sbottomView.frame));
        weakSelf.nbottomView.transform = CGAffineTransformIdentity;
        
        if (self.playViewState == OddityPlayViewStateFull) {
            
            weakSelf.topBackView.transform = CGAffineTransformIdentity;
        }else{
            
            weakSelf.topBackView.transform = CGAffineTransformTranslate(weakSelf.topBackView.transform, 0, -84);
        }
        
    }completion:^(BOOL finished) {
        
    }];
    
    [self bringSubviewToFront:self.nbottomView];
    [self bringSubviewToFront:self.topBackView];
    
    [self needsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    [self layoutIfNeeded];
}


/**
 隐藏控制视图
 */
- (void)playerHideControlView {
    if (self.isShowPlayList) {
        
        self.playOtPauseButton.alpha = 0;
        
        return;
    }
    /// 如果有这个东西就隐藏
    if ([OdditySoundView shareSoundView].superview) {
        
        [[OdditySoundView shareSoundView] removeFromSuperview];
    }
    
    _isShowing = false;
    
    __weak typeof(self) weakSelf = self;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(playerHideControlView) object:nil];
    [UIView animateWithDuration:OddityPlayerControlBarAutoFadeOutTimeInterval animations:^{
        
        weakSelf.nbottomView.alpha = 0;
        weakSelf.sbottomView.alpha = 1;
        weakSelf.playOtPauseButton.alpha = 0;
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            weakSelf.topBackView.transform = CGAffineTransformTranslate(weakSelf.topBackView.transform, 0, -CGRectGetHeight(weakSelf.topBackView.frame));
        }else{
            weakSelf.topBackView.transform = CGAffineTransformTranslate(weakSelf.topBackView.transform, 0, -104);
        }
        weakSelf.nbottomView.transform = CGAffineTransformTranslate(weakSelf.nbottomView.transform, 0, CGRectGetHeight(weakSelf.nbottomView.frame));
//        weakSelf.sbottomView.transform = CGAffineTransformIdentity;
        
    }completion:^(BOOL finished) {
        
    }];
    
    [self layoutIfNeeded];
}


-(void)waitForReadyToPlay:(NSInteger)durationTime{
    
    self.isNeedToSeek = true;
    self.seekTime = durationTime;
}

- (void)seekToTime:(NSInteger)dragedSeconds completionHandler:(void (^)(BOOL finished))completionHandler {
    if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        [self Pause];
        CMTime dragedCMTime = CMTimeMake(dragedSeconds, 1); //kCMTimeZero
        __weak typeof(self) weakSelf = self;
        [self.player seekToTime:dragedCMTime toleranceBefore:CMTimeMake(1,1) toleranceAfter:CMTimeMake(1,1) completionHandler:^(BOOL finished) {
            // 视频跳转回调
            weakSelf.playState = OddityPlayStateBuffering;
            [weakSelf Play];
            weakSelf.nbottomView.isDragged = false;
            if (completionHandler) { completionHandler(finished); }
        }];
    }
}






/// isW
/// 0 网络为 移动网络提醒用户流量费用
/// 1 网络错误
/// 2 未知错误
-(void)show:(NSInteger)totalTime isWann:(NSInteger)isW{
    
    [self playerHideControlView];
    
    [self bringSubviewToFront:self.networkStateInfoView];
    
    [self.networkStateInfoView show:totalTime isWann:isW];
}


#pragma mark: 按钮点击方法

/**
 集中处理播放器中的点击时间
 */
-(void)createControlSelectorPlayView{
    
    [_nbottomView.progressSlider addTarget:self action:@selector(progressSliderTouchBegan:) forControlEvents:UIControlEventTouchDown];
    [_nbottomView.progressSlider addTarget:self action:@selector(progressSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_nbottomView.progressSlider addTarget:self action:@selector(progressSliderTouchEnded:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    
    [_normalTopShowInfoView.lockButton addTarget:self action:@selector(videoRatioLockAction) forControlEvents:UIControlEventTouchUpInside];
    [_normalTopShowInfoView.lockButton addTarget:self action:@selector(playViewButtonBeginAction) forControlEvents:UIControlEventTouchDown];
    [_normalTopShowInfoView.lockButton addTarget:self action:@selector(playViewButtonEndAction) forControlEvents: UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    
    [_normalTopShowInfoView.playListButton addTarget:self action:@selector(playListClickAction) forControlEvents:UIControlEventTouchUpInside];
    [_normalTopShowInfoView.playListButton addTarget:self action:@selector(playViewButtonBeginAction) forControlEvents:UIControlEventTouchDown];
    [_normalTopShowInfoView.playListButton addTarget:self action:@selector(playViewButtonEndAction) forControlEvents: UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    
    [_normalTopShowInfoView.cancelButton addTarget:self action:@selector(cancelButtonFinishAction) forControlEvents:UIControlEventTouchUpInside];
    [_normalTopShowInfoView.cancelButton addTarget:self action:@selector(playViewButtonBeginAction) forControlEvents:UIControlEventTouchDown];
    [_normalTopShowInfoView.cancelButton addTarget:self action:@selector(playViewButtonEndAction) forControlEvents: UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    
    [_nbottomView.openButton addTarget:self action:@selector(fullScreenButtonActionMethod) forControlEvents:UIControlEventTouchUpInside];
    [_nbottomView.openButton addTarget:self action:@selector(playViewButtonBeginAction) forControlEvents:UIControlEventTouchDown];
    [_nbottomView.openButton addTarget:self action:@selector(playViewButtonEndAction) forControlEvents: UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    
    [_normalTopShowInfoView.moreButton addTarget:self action:@selector(cancelFullScreenAndShareActionMethod) forControlEvents:UIControlEventTouchUpInside];
    [_normalTopShowInfoView.moreButton addTarget:self action:@selector(playViewButtonBeginAction) forControlEvents:UIControlEventTouchDown];
    [_normalTopShowInfoView.moreButton addTarget:self action:@selector(playViewButtonEndAction) forControlEvents: UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    
    [_nbottomView.volumeButton addTarget:self action:@selector(volumeButtonTouchAction) forControlEvents:UIControlEventTouchUpInside];
    [_nbottomView.volumeButton addTarget:self action:@selector(playViewButtonBeginAction) forControlEvents:UIControlEventTouchDown];
    [_nbottomView.volumeButton addTarget:self action:@selector(playViewButtonEndAction) forControlEvents: UIControlEventTouchCancel | UIControlEventTouchUpOutside];
    
    
    [_smallCloseButton addTarget:self action:@selector(closePlayerView) forControlEvents:(UIControlEventTouchUpInside)];
    
    [_networkStateInfoView.cancelPlayButton addTarget:self action:@selector(closePlayerView) forControlEvents:UIControlEventTouchUpInside];
    [_networkStateInfoView.continuePlayButton addTarget:self action:@selector(continuePlayAction) forControlEvents:UIControlEventTouchUpInside];
}


/**
 播放列表按钮 点击事件
 */
-(void)playListClickAction{

    if (self.isShowPlayList) {
        
        [self HidePlayListViewMethod];
    }else{
    
        [self ShowPlayListViewMethod];
    }
}

/**
 点击网络错误显示视图的 继续播放按钮
 */
-(void)continuePlayAction{
    
    self.isContinueWithWaan = true;
    
    [self.networkStateInfoView hide];
    
    NSURL *videoUrl =  [[NSURL alloc]initWithString:_url];
    self.playerItem = [[AVPlayerItem alloc]initWithURL:videoUrl];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    self.player.muted = self.nbottomView.isMute;
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    self.playerLayer.backgroundColor = [UIColor blackColor].CGColor;
    
    [self createTimer];
    
    self.playState = OddityPlayStateBuffering;
    [self Play];
}

-(void)makeNoMuteMthod{

    self.nbottomView.isMute = false;
    
    self.player.muted = self.nbottomView.isMute;
}

-(void)volumeButtonTouchAction{
    
    self.nbottomView.isMute = !self.nbottomView.isMute;
    
    self.player.muted = self.nbottomView.isMute;
}

/// 进度条开始按下
- (void)progressSliderTouchBegan:(OddityPlayerViewThreeHeightSlider *)sender {
    
    [self beginSeekTime];
}

/// 进度条值发生变化
- (void)progressSliderValueChanged:(OddityPlayerViewThreeHeightSlider *)sender {
    
    [self changeSeekTime:sender.value];
}

/// 进度条 结束按下
- (void)progressSliderTouchEnded:(OddityPlayerViewThreeHeightSlider *)sender {
    
    [self endSeekTime:sender.value];
}


/**
 播放视图统一处理办法: 处理按钮点击开始
 */
-(void)playViewButtonBeginAction{
    
    [self cancelAutoFadeOutControlView];
}

/**
 播放视图统一处理办法: 处理按钮点击结束
 */
-(void)playViewButtonEndAction{
    
    [self autoFadeOutControlView];
}


/**
 视频 正常状态下 锁定按钮的 点击事件儿
 */
-(void)videoRatioLockAction{
    
    self.normalTopShowInfoView.isLock = !self.normalTopShowInfoView.isLock;
    [self autoFadeOutControlView];
}

/**
 视频视图 上方信息 展示视图 返回按钮 点击 触发事件
 */
-(void)cancelButtonFinishAction{
    
    
    [self autoFadeOutControlView];
    self.nbottomView.isFullScreen = false;
    self.normalTopShowInfoView.isLock = false;
    
    [self makeOrientationMethod:(UIInterfaceOrientationPortrait)];
}

/**
 普通状态下点击 全屏 按钮的 触发 事件儿
 */
-(void)fullScreenButtonActionMethod{
    
    [self autoFadeOutControlView];
    self.normalTopShowInfoView.isLock = false;
    self.nbottomView.isFullScreen = !self.nbottomView.isFullScreen;
    
    if (self.nbottomView.isFullScreen) {
        [self makeOrientationMethod:(UIInterfaceOrientationLandscapeRight)];
    }else{
        [self makeOrientationMethod:(UIInterfaceOrientationPortrait)];
    }
}

/**
 点击小视频 状态下的 关闭按钮 触发的事件儿
 */
-(void)closePlayerView{
    
    [self recoveryPlayViewNoramlState];
    
    [self Pause];
    
    [self removeFromSuperview];
    
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [weakSelf toEmptyState];
        
        [[AVAudioSession sharedInstance] setActive:false withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    });
    
}


/**
 横屏的时候 处理该方法
 
 将视频回复成原来的模样
 */
-(void)recoveryPlayViewNoramlState{
    
    [self autoFadeOutControlView];
    self.normalTopShowInfoView.isLock = false;
    self.nbottomView.isFullScreen = !self.nbottomView.isFullScreen;
    
    [self makeOrientationMethod:(UIInterfaceOrientationPortrait)];
}

/**
 变正常播放状态 并且进入分享视图
 */
-(void)cancelFullScreenAndShareActionMethod{
    
    [self recoveryPlayViewNoramlState];
    
    [[[OddityShareViewController alloc] initWithStyle:(OddityShareStyleNew) withObj:self.object] presentViewControllerWithCurrent];
}


#pragma mark: 手势处理方法

/**
 *  创建手势操作
 */
- (void)createGestureRecognizerTargetPlayView {
    // 单击
    _singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapAction:)];
    _singleTap.delegate                = self;
    _singleTap.numberOfTouchesRequired = 1; //手指数
    _singleTap.numberOfTapsRequired    = 1;
    [self addGestureRecognizer: _singleTap];
    
    // 双击(播放/暂停)
    _doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapAction)];
    _doubleTap.delegate                = self;
    _doubleTap.numberOfTouchesRequired = 1; //手指数
    _doubleTap.numberOfTapsRequired    = 2;
    [self addGestureRecognizer:_doubleTap];
    
    _singlePan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(singlePanAction:)];
    _singlePan.delegate                = self;
    [self addGestureRecognizer:_singlePan];
    
    
    // 解决点击当前view时候响应其他控件事件
    [_singleTap setDelaysTouchesBegan:YES];
    [_doubleTap setDelaysTouchesBegan:YES];
    // 双击失败响应单击事件
    [_singleTap requireGestureRecognizerToFail:_doubleTap];
}


/**
 在手势还没有开始的时候 根据一些条件判断是不是需要 开始 这个手势操作
 
 @param gestureRecognizer 手势对象
 @return 是否准备开始处理这个手势
 */
-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    if (self.singleTap == gestureRecognizer && self.isShowPlayList && CGRectContainsPoint(self.playListView.frame, [gestureRecognizer locationInView:gestureRecognizer.view])) {
        
        return false;
    }
    
    /// 判断如果在列表页 并且 用户竖向 滑动，就置为 不可滑动
    if (![[UIViewController oddityCurrentViewController] isKindOfClass:[OddityNewsDetailViewController class]] && gestureRecognizer == self.singlePan && self.playViewState == OddityPlayViewStateNormal) {
        
        CGPoint velocity = [_singlePan velocityInView:self];
        
        if (ABS(velocity.x) < ABS(velocity.y)) { /// 如果是横向滑动的那么 就是 快进快退
            
            return false;
        }
    }
    
    if (gestureRecognizer == _doubleTap && _playViewState == OddityPlayViewStateSmall ) {
        
        return false;
    }
    
    return true;
}


/**
 单击 视频播放器
 
 @param gesture <#gesture description#>
 */
- (void)singleTapAction:(UIGestureRecognizer *)gesture {
    
    if (self.isShowPlayList) {
        
        [self HidePlayListViewMethod];
        
        return;
    }
    
    if (_playViewState == OddityPlayViewStateSmall) {
        
        [self goDetailViewController];
        
        return;
    }
    
    if (_isShowing) {
        
        [self playerHideControlView];
    }else{
        
        [self playerShowControlView];
        [self autoFadeOutControlView];
    }
}


/**
 双击播放器视图
 */
- (void)doubleTapAction {
    
    if (_isPlaying) {
        
        [self Pause];
    }else{
        
        [self Play];
    }
}


/**
 单手指滑动视频视图
 
 @param gesture 手势对象
 */
-(void)singlePanAction:(UIPanGestureRecognizer *)gesture {
    
    if (_playViewState == OddityPlayViewStateSmall) {
        
        [self smallStatePanActionMethod:gesture];
        
        return;
    }
    // 我们要响应水平移动和垂直移动
    // 根据上次和本次移动的位置，算出一个速率的point
    CGPoint veloctyPoint = [gesture velocityInView:self];
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
            
            [self judgeDragPlayViewType:gesture];
        }
            break;
            
        case UIGestureRecognizerStateChanged:{
            
            [self reactionForDragPlayViewVelocty:veloctyPoint];
        }
            break;
        case UIGestureRecognizerStateEnded:{
            
            [self completeForDragPlayView];
        }
            break;
        default:
            break;
    }
}


/**
 小窗口的情况下 在滑动的 时候 就做一个滑动位置的 计算
 
 @param gesture 手势对象
 */
-(void)smallStatePanActionMethod:(UIPanGestureRecognizer *)gesture{
    
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:{
            
            //            _sumPoint = CGPointMake(self.transform.tx, self.transform.ty);
        }
            break;
        case UIGestureRecognizerStateChanged:{
            
            CGPoint tran = [gesture translationInView:self];
            
            [self mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.bottom.equalTo(@(_sumPoint.y + tran.y));
                make.right.equalTo(@(_sumPoint.x + tran.x));
            }];
            
            [[UIApplication sharedApplication].keyWindow layoutIfNeeded];
        }
            break;
        default:{
            [self refreshSmallPlayerViewLocation];
        }
            break;
    }
}


/**
 刷新小状态视频的位置 防止被遮挡
 */
-(void)refreshSmallPlayerViewLocation{
    
    CGRect videoFrame = [[OddityPlayerView sharePlayerView] convertRect:[OddityPlayerView sharePlayerView].bounds toView:[[UIApplication sharedApplication]keyWindow]];
    
    CGSize screenSize = [[UIScreen mainScreen]bounds].size;
    
    CGFloat rightCnstraint = -(screenSize.width - CGRectGetMaxX(self.frame));
    CGFloat bottomCnstraint = -(screenSize.height - CGRectGetMaxY(self.frame));
    
    if (CGRectGetMinX(videoFrame) < (screenSize.width - CGRectGetMaxX(videoFrame))) {
        
        rightCnstraint = -(screenSize.width-CGRectGetWidth(videoFrame)-OddityUISetting.shareOddityUISetting.smallVideoInsets.left);
    }else{
        
        rightCnstraint = -OddityUISetting.shareOddityUISetting.smallVideoInsets.right;
    }
    
    if (CGRectGetMinY(videoFrame) < OddityUISetting.shareOddityUISetting.smallVideoInsets.top) {
        
        bottomCnstraint = -(screenSize.height-OddityUISetting.shareOddityUISetting.smallVideoInsets.top-CGRectGetHeight(videoFrame));
    }
    
    if ((screenSize.height - CGRectGetMaxY(videoFrame)) < OddityUISetting.shareOddityUISetting.smallVideoInsets.bottom) {
        
        bottomCnstraint = -OddityUISetting.shareOddityUISetting.smallVideoInsets.bottom;
    }
    
    _sumPoint = CGPointMake(rightCnstraint, bottomCnstraint);
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(rightCnstraint);
        make.bottom.mas_equalTo(bottomCnstraint);
    }];
    
    
    [UIView animateWithDuration:0.2 animations:^{
        
        [[UIApplication sharedApplication].keyWindow layoutIfNeeded];
    }];
}

/**
 根据用户滑动的手势 判断用户想要做什么 并且吧这些东西 记录在 _dragPlayViewType
 
 @param gesture 手势对象
 */
-(void)judgeDragPlayViewType:(UIPanGestureRecognizer *)gesture{
    /// 用户的 translation
    CGPoint velocity = [gesture velocityInView:self];
    
    [self playerShowControlView]; /// 显示视频模块
    
    if (ABS(velocity.x) > ABS(velocity.y)) { /// 如果是横向滑动的那么 就是 快进快退
        
        _sumTime = _player.currentTime.value/_player.currentTime.timescale;
        
        _dragPlayViewType = OddityDragPlayViewTypeForward;
        
    }else{ /// 如果是竖向滑动的那么 就是 音量 或者 亮度
        
        CGPoint location = [gesture locationInView:self];
        
        if (CGRectGetWidth(gesture.view.frame)/2 > location.x) { // 如果用户是在 视频界面的 左侧
            
            _dragPlayViewType = OddityDragPlayViewTypeBrightness;
        }else{ // 如果用户是在 视频界面 的 右侧
            
            _dragPlayViewType = OddityDragPlayViewTypeVolume;
        }
    }
    
    [self beginSeekTime];
}


/**
 对在 视频视图 滑动的操作 做出反应
 
 @param vel 进度
 */
-(void)reactionForDragPlayViewVelocty:(CGPoint)vel{
    
    switch (self.dragPlayViewType) {
        case OddityDragPlayViewTypeBrightness:{
            
            [UIScreen mainScreen].brightness -= vel.y / 10000;
            
            [_soundView showViewFor: _dragPlayViewType value:[UIScreen mainScreen].brightness];
        }
            break;
        case OddityDragPlayViewTypeVolume:{
            
            _volumeViewSlider.value -= vel.y / 10000;
            [self.soundView showViewFor:_dragPlayViewType value: _volumeViewSlider.value];
        }
            break;
        case OddityDragPlayViewTypeBackward:case OddityDragPlayViewTypeForward:{
            
            self.sumTime += vel.x / 200;
            
            // 需要限定sumTime的范围
            CMTime totalTime           = self.playerItem.duration;
            CGFloat totalMovieDuration = (CGFloat)totalTime.value/totalTime.timescale;
            if (self.sumTime > totalMovieDuration) { self.sumTime = totalMovieDuration;}
            if (self.sumTime < 0) { self.sumTime = 0; }
            
            CGFloat  draggedValue    = (CGFloat)self.sumTime/(CGFloat)totalMovieDuration;
            
            [self changeSeekTime:draggedValue];
        }
            break;
        default:
            break;
    }
}


/**
 完成滑动
 */
-(void)completeForDragPlayView{
    
    switch (self.dragPlayViewType) {
        case OddityDragPlayViewTypeBackward:case OddityDragPlayViewTypeForward:{
            
            [self endSeekTime:self.nbottomView.progressSlider.value];
        }
            break;
        default:
            break;
    }
    
    [self endSoundView];
    self.dragPlayViewType = OddityDragPlayViewTypeNormal;
}


/**
 结束滑动的时候 隐藏 进度提示视图
 */
-(void)endSoundView{
    
    self.nbottomView.isDragged = false;
    [self autoFadeOutControlView];
    _soundView.alpha = 0;
    self.playOtPauseButton.alpha = 1;
}

/**
 开始改变 时间的进度
 */
-(void)beginSeekTime{
    
    [self cancelAutoFadeOutControlView];
    self.playOtPauseButton.alpha = 0;
    self.nbottomView.isDragged = true;
    _soundView.alpha = 1;
    _soundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8];
}


/**
 正在改变 时间的 进度
 
 @param progress 进度
 */
-(void)changeSeekTime:(CGFloat) progress{
    
    [self cancelAutoFadeOutControlView];
    if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        
        BOOL style = false;
        
        CGFloat value   = progress - self.sbottomView.timerProgress.progress;
        
        if (value > 0) { style = YES; }
        if (value < 0) { style = NO; }
        if (value == 0) { return; }
        
        CGFloat totalTime     = (CGFloat)_playerItem.duration.value / _playerItem.duration.timescale;
        
        CGFloat dragedSeconds = floorf(totalTime * progress);
        
        CGFloat  draggedValue    = (CGFloat)dragedSeconds/(CGFloat)totalTime;
        
        self.nbottomView.progressSlider.value = draggedValue;
        [self.soundView playerDraggedTime:dragedSeconds totalTime:totalTime isForward:style];
    }
}


/**
 结束改变 时间的 进度
 
 @param progress 进度
 */
-(void)endSeekTime:(CGFloat) progress{
    
    [self endSoundView];
    
    if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        // 视频总时间长度
        CGFloat total           = (CGFloat)_playerItem.duration.value / _playerItem.duration.timescale;
        //计算出拖动的当前秒数
        NSInteger dragedSeconds = floorf(total * progress);
        [self seekToTime:dragedSeconds completionHandler:nil];
    }
}


#pragma mark: 监听一些通知

/**
 *  添加观察者、通知
 */
- (void)createSelfNotificationsPlayView {
    // app退到后台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationWillResignActiveNotification object:nil];
    // app进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterPlayground) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    // 监听耳机插入和拔掉通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListenerCallback:) name:AVAudioSessionRouteChangeNotification object:nil];
    
    // 监测设备方向
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDeviceOrientationChange) name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(netWorkIsChangedMethod) name:oddityNetworkStateChanged object:nil];
    
    
    //使用NSNotificationCenter 鍵盤出現時
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAudioSessionInterruption:) name:AVAudioSessionInterruptionNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMediaServicesReset) name:AVAudioSessionMediaServicesWereResetNotification object:nil];
}

- (void)handleMediaServicesReset {
    
    [self Pause];
}

- (void)handleAudioSessionInterruption:(NSNotification*)notification {
    
    NSNumber *interruptionType = [[notification userInfo] objectForKey:AVAudioSessionInterruptionTypeKey];
    NSNumber *interruptionOption = [[notification userInfo] objectForKey:AVAudioSessionInterruptionOptionKey];
    
    switch (interruptionType.unsignedIntegerValue) {
        case AVAudioSessionInterruptionTypeBegan:{
            [self Pause];
        } break;
        case AVAudioSessionInterruptionTypeEnded:{
            if (interruptionOption.unsignedIntegerValue == AVAudioSessionInterruptionOptionShouldResume) {
                [self Play];
            }
        } break;
        default:
            break;
    }
}

/**
 处理播放器党，播放器 处于键盘弹出的情况下 进行向上移动的操作
 
 @param aNotification ios 的通知机制的对象
 */
- (void)keyboardWasShown:(NSNotification*)aNotification{
    
    if (!self.isPlayerViewShow || _playViewState != OddityPlayViewStateSmall) {
        return;
    }
    
    CGSize kbSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
    
    CGRect selfBounds= [self convertRect:self.bounds toView:[UIApplication sharedApplication].keyWindow];
    
    CGFloat trueBottomSpace = oddityUIScreenHeight-CGRectGetMaxY(selfBounds);
    
    if (ABS(trueBottomSpace) >= kbSize.height+8) {
        
        return;
    }
    
    _sumPoint = CGPointMake(_sumPoint.x,-kbSize.height-8);
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(-kbSize.height-8);
    }];
    
    [[UIApplication sharedApplication].keyWindow layoutIfNeeded];
}

/**
 *  耳机插入、拔出事件
 */
- (void)audioRouteChangeListenerCallback:(NSNotification*)notification {
    NSDictionary *interuptionDict = notification.userInfo;
    
    NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
    
    switch (routeChangeReason) {
            
        case AVAudioSessionRouteChangeReasonNewDeviceAvailable:
            // 耳机插入
            if (self.isPlayerViewShow) {
                
                self.volumeViewSlider.value = 0.1;
            }
            break;
            
        case AVAudioSessionRouteChangeReasonOldDeviceUnavailable:
        {
            // 耳机拔掉
            // 拔掉耳机继续播放
            [self Pause];
        }
            break;
            
        case AVAudioSessionRouteChangeReasonCategoryChange:
            // called at start - also when other audio wants to play
            NSLog(@"AVAudioSessionRouteChangeReasonCategoryChange");
            break;
    }
}

/// 应用进入 后台
-(void)appDidEnterBackground{
    
    if (self.isPlaying) {
        
        _isPauseForDidEnterBackground = true;
        [self Pause];
    }
    
}

/// 应用进入 前台
-(void)appDidEnterPlayground{
    
    if (_isPauseForDidEnterBackground) {
        
        _isPauseForDidEnterBackground = false;
        
        [self Play];
    }
}


/// 网络状态 发生了变化
-(void)netWorkIsChangedMethod{
    
    if ([AFNetworkReachabilityManager sharedManager].reachable) {
        
        if ([AFNetworkReachabilityManager sharedManager].isReachableViaWWAN){
            
            [self Pause];
            [self show:(NSInteger)(self.totalTime) isWann:0];
        }
        
    } else {
        
        NSLog(@"网络不可用，但是不需要理会");
    }
}


/**
 *  屏幕方向发生变化会调用这里
 */
- (void)onDeviceOrientationChange {
    if (!self.isPlayerViewShow) {return;}
    if (!self.player) { return; }
    if (self.playViewState == OddityPlayViewStateSmall ) { return; }
    if (self.normalTopShowInfoView.isLock) { return; }
    UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    if (orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationUnknown ) { return; }
    [self makeOrientationMethod:interfaceOrientation];
}


/**
 设置屏幕的旋转 抽象方法
 
 @param interfaceOrientation 屏幕的方向
 */
-(void)makeOrientationMethod:(UIInterfaceOrientation)interfaceOrientation{
    
    if (self.isShowPlayList) {
        
        [self HidePlayListViewMethod];
    }
    
    _isRotatingScreen = true;
    
    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortraitUpsideDown:{
        }
            break;
        case UIInterfaceOrientationPortrait:{
            self.nbottomView.isFullScreen = false;
            [self toOrientation:interfaceOrientation];
            
            UIWindow *currentWindow = UIApplication.sharedApplication.keyWindow;
            
            CGRect newFrame = currentWindow.frame;
            CGRect oldFrame = [self.oldPlayBackView convertRect:self.oldPlayBackView.frame toView:currentWindow];
            
            if (CGRectIntersectsRect(newFrame, oldFrame)) {
                
                [self GoToState:(OddityPlayViewStateNormal) inView:self.oldPlayBackView];
            }else{
            
                [self GoToState:(OddityPlayViewStateSmall) inView:nil];
            }
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:case UIInterfaceOrientationLandscapeRight:{
            self.nbottomView.isFullScreen = true;
            [self GoToState:OddityPlayViewStateFull inView:nil];
            [self toOrientation:interfaceOrientation];
        }
            break;
        default:
            break;
    }
    _isRotatingScreen = false;

}


/**
 设置屏幕朝向的方向
 
 @param orientation 方向
 */
- (void)toOrientation:(UIInterfaceOrientation)orientation {
    // 获取到当前状态条的方向
    UIInterfaceOrientation currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
    // 判断如果当前方向和要旋转的方向一致,那么不做任何操作
    if (currentOrientation == orientation) { return; }
    
    // 根据要旋转的方向,使用Masonry重新修改限制
    if (orientation != UIInterfaceOrientationPortrait) {//
        // 这个地方加判断是为了从全屏的一侧,直接到全屏的另一侧不用修改限制,否则会出错;
        if (currentOrientation == UIInterfaceOrientationPortrait) {
            [self removeFromSuperview];
            [[UIApplication sharedApplication].keyWindow addSubview:self];
            [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(oddityUIScreenHeight));
                make.height.equalTo(@(oddityUIScreenWidth));
                make.center.equalTo([UIApplication sharedApplication].keyWindow);
            }];
        }
    }
    // iOS6.0之后,设置状态条的方法能使用的前提是shouldAutorotate为NO,也就是说这个视图控制器内,旋转要关掉;
    // 也就是说在实现这个方法的时候-(BOOL)shouldAutorotate返回值要为NO
    [[UIApplication sharedApplication] setStatusBarOrientation:orientation animated:false];
    // 获取旋转状态条需要的时间:
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    // 更改了状态条的方向,但是设备方向UIInterfaceOrientation还是正方向的,这就要设置给你播放视频的视图的方向设置旋转
    // 给你的播放视频的view视图设置旋转
    self.transform = CGAffineTransformIdentity;
    self.transform = [self getTransformRotationAngle];
    // 开始旋转
    [UIView commitAnimations];
}

/**
 * 获取变换的旋转角度
 *
 * @return 角度
 */
- (CGAffineTransform)getTransformRotationAngle {
    // 状态条的方向已经设置过,所以这个就是你想要旋转的方向
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    // 根据要进行旋转的方向来计算旋转的角度
    if (orientation == UIInterfaceOrientationPortrait) {
        return CGAffineTransformIdentity;
    } else if (orientation == UIInterfaceOrientationLandscapeLeft){
        return CGAffineTransformMakeRotation(-M_PI_2);
    } else if(orientation == UIInterfaceOrientationLandscapeRight){
        return CGAffineTransformMakeRotation(M_PI_2);
    }
    return CGAffineTransformIdentity;
}


#pragma mark: 监听视频变化 音量控制 进度
- (void)setPlayerItem:(AVPlayerItem *)playerItem {
    if (_playerItem == playerItem) {return;}
    
    if (_playerItem) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:_playerItem];
        [_playerItem removeObserver:self forKeyPath:@"status"];
        [_playerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
        [_playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        [_playerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    }
    _playerItem = playerItem;
    if (playerItem) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
        [playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
        [playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
        // 缓冲区空了，需要等待数据
        [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
        // 缓冲区有足够数据可以播放了
        [playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew context:nil];
    }
}

/// 播放完毕
-(void)moviePlayDidEnd:(NSNotification *)notifi{
    
    [self Pause];
    self.playState = OddityPlayStateStopped;
    
    [[NSNotificationCenter defaultCenter]postNotificationName:oddityVideoIsFinishPlayerEd object:nil];
}


- (NSTimeInterval)availableDuration {
    NSArray *loadedTimeRanges = [[_player currentItem] loadedTimeRanges];
    CMTimeRange timeRange     = [loadedTimeRanges.firstObject CMTimeRangeValue];// 获取缓冲区域
    float startSeconds        = CMTimeGetSeconds(timeRange.start);
    float durationSeconds     = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result     = startSeconds + durationSeconds;// 计算缓冲总进度
    return result;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (object != self.player.currentItem) { return; }
    
    if ([keyPath isEqualToString:@"status"]) {
        
        if (self.player.currentItem.status == AVPlayerItemStatusReadyToPlay) {
            
            /// 如果需要快进的话
            if (self.isNeedToSeek) {
                self.isNeedToSeek = false;
                [self seekToTime:self.seekTime completionHandler:nil];
            }
            
            _isReadyToPlay = true;
            
            self.videoRect = self.playerLayer.videoRect;
            
            [self setNeedsLayout];
            [self layoutIfNeeded];
            [self.layer insertSublayer:self.playerLayer atIndex:0];
            
            [self abimateUpdateSmallVideo];
            
        } else if (self.player.currentItem.status == AVPlayerItemStatusFailed) {
            
            self.playState = OddityPlayStateFailed;
            
            if (!AFNetworkReachabilityManager.sharedManager.reachable || self.cacheTime <= 0) {
                
                [self Pause];
                [self show:self.totalTime isWann:2];
            }
        }
        
    } else if ([keyPath isEqualToString:@"loadedTimeRanges"]) {
        
        // 计算缓冲进度
        self.cacheTime = [self availableDuration];
        CMTime duration             = self.playerItem.duration;
        CGFloat totalDuration       = CMTimeGetSeconds(duration);
        [self.sbottomView playerCacheProgress:self.cacheTime / totalDuration];
        [self.nbottomView playerCacheProgress:self.cacheTime / totalDuration];
        
        
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        
        if (self.playerItem.playbackBufferEmpty) {
            
            self.playState = OddityPlayStateBuffering;
        }
        
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        
        if (!self.playerItem.playbackLikelyToKeepUp && ![AFNetworkReachabilityManager sharedManager].reachable && !self.networkStateInfoView.isShow) {
            
            [self show:self.totalTime isWann:2];
        }
        
        if (self.playerItem.playbackLikelyToKeepUp && [AFNetworkReachabilityManager sharedManager].reachable && self.networkStateInfoView.isShow) {
            
            [self.networkStateInfoView hide];
        }
        
        if (self.playerItem.playbackLikelyToKeepUp && self.playState == OddityPlayStateBuffering){
            
            self.playState = OddityPlayStatePlaying;
        }
    }
}

/**
 *  创建时间
 */
- (void)createTimer {
    __weak typeof(self) weakSelf = self;
    self.timeObserve = [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(1, 1) queue:nil usingBlock:^(CMTime time){
        AVPlayerItem *currentItem = weakSelf.playerItem;
        NSArray *loadedRanges = currentItem.seekableTimeRanges;
        if (loadedRanges.count > 0 && currentItem.duration.timescale != 0) {
            _currentTime = (NSInteger)CMTimeGetSeconds([currentItem currentTime]);
            weakSelf.totalTime     = (CGFloat)currentItem.duration.value / currentItem.duration.timescale;
            CGFloat value         = CMTimeGetSeconds([currentItem currentTime]) / weakSelf.totalTime;
            [weakSelf.sbottomView playerCurrentTime:value];
            [weakSelf.nbottomView playerCurrentTime:weakSelf.currentTime totalTime:weakSelf.totalTime sliderValue:value];
        }
    }];
}


/**
 *  获取系统音量
 */
- (void)configureVolume {
    _volumeViewSlider = nil;
    
    MPVolumeView *volumeView = [[MPVolumeView alloc]initWithFrame:CGRectMake(-100, -100, 100, 100)];
    [self addSubview:volumeView];
    for (UIView *view in [volumeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            _volumeViewSlider = (UISlider *)view;
            break;
        }
    }
    
    // 使用这个category的应用不会随着手机静音键打开而静音，可在手机静音下播放声音
    NSError *setCategoryError = nil;
    BOOL success = [[AVAudioSession sharedInstance]
                    setCategory: AVAudioSessionCategoryPlayback
                    error: &setCategoryError];
    
    if (!success) { /* handle the error in setCategoryError */ }
}



#pragma mark: 播放列表相关

-(void)ShowPlayListViewMethod{

    [self.playListView.collectionView reloadData];
    
    [self cancelAutoFadeOutControlView];
    
    self.sbottomView.alpha = 0;
    self.nbottomView.alpha = 1;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.playOtPauseButton.alpha = 0;
        self.sbottomView.alpha = 1;
        _playListView.transform = CGAffineTransformIdentity;
        self.nbottomView.transform = CGAffineTransformTranslate(self.nbottomView.transform, 0, CGRectGetHeight(self.nbottomView.frame));
    }];
    
    self.isShowPlayList = true;
    
    self.normalTopShowInfoView.playListButton.selected = self.isShowPlayList;
    
    NSInteger index = [self.mainManagerViewController.currentViewController.videoFilterResults indexOfObject:self.videoObject.object];
    
    if (index != NSNotFound) {
        
        [self.playListView.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:(UICollectionViewScrollPositionTop) animated:false];
    }
    
    self.playListView.backgroundColor = UIColor.color_t_14;
}

-(void)HidePlayListViewMethod{
    
    self.isShowPlayList = false;
    
    self.playOtPauseButton.alpha = 1;
    
    self.normalTopShowInfoView.playListButton.selected = self.isShowPlayList;
    
    self.nbottomView.alpha = 1;
    self.sbottomView.alpha = 1;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.sbottomView.alpha = 0;
        
        if (ODDITY_SYSTEM_LESS_THAN(8.0)){
        
            _playListView.transform = CGAffineTransformTranslate(_playListView.transform, CGRectGetWidth(_playListView.frame)*2, 0);
        }else{
        
            _playListView.transform = CGAffineTransformTranslate(_playListView.transform, CGRectGetWidth(_playListView.frame), 0);
        }
        
        self.nbottomView.transform = CGAffineTransformIdentity;
    }];
    
    
    self.sbottomView.alpha = 0;
    self.nbottomView.alpha = 1;
    
    [self autoFadeOutControlView];
}

@end

