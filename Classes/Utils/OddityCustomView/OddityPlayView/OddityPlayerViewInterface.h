//
//  OddityPlayerViewInterface.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityPlayerView.h"

typedef NS_ENUM(NSInteger, OddityPlayViewState) {
    
    OddityPlayViewStateNeedJudgment = -1,
    OddityPlayViewStateNormal = 0,
    OddityPlayViewStateSmall = 1,
    OddityPlayViewStateFull = 2
};

@protocol OddityPlayerViewInterface <NSObject>

/**
 获取单例模式下的 播放器
 
 @return 播放器
 */
+(instancetype)sharePlayerView;


/**
 在需要强制转换视频到 某一个 状态的时候，为了保证百分之百的变化状态需要调用该方法。
 */
-(void)toNeedJudgmentState;


/**
 把视频转换到 某一个状态
 
 @param state 视频状态
 @param view 播放的背景视图
 */
-(void)toState:(OddityPlayViewState)state inView:(UIView *)view;

/**
 点击小视频 状态下的 关闭按钮 触发的事件儿
 */
-(void)closePlayerView;

/**
 把视频空间 置空
 */
-(void)toEmptyState;

/**
 开始播放视频
 */
-(void)Play;

/**
 暂停播放视频
 */
-(void)Pause;

- (void)autoFadeOutControlView;

- (void)playerHideControlView;

- (void)playerShowControlView;

/**
 告诉视频在他加载完成之后，需要跳转到该位置
 
 @param durationTime 跳转到的视频播放时间
 */
-(void)waitForReadyToPlay:(NSInteger)durationTime;


/**
 显示 音量 亮度 进度 显示调节按钮
 
 @param view 要显示到的 UIView
 @param val 进度值
 */
+(void)showSoundViewInView:(UIView *)view value:(CGFloat)val;

/**
 快进时间

 @param dragedSeconds 需要快进的时间未知
 @param completionHandler 完成快进的操作回调
 */
- (void)seekToTime:(NSInteger)dragedSeconds completionHandler:(void (^)(BOOL finished))completionHandler;
@end
