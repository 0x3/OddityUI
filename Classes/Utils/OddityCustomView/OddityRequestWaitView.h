//
//  OddityRequestWaitView.h
//  Pods
//
//  Created by 荆文征 on 2017/6/15.
//
//

#import <UIKit/UIKit.h>


/**
 等待视图的模式

 - OddityRequestWaitAlertModeAnimateWithLabel: 转圈加信息显示
 - OddityRequestWaitAlertModeAnimate: 转圈
 - OddityRequestWaitAlertModeLabel: 信息显示
 */
typedef NS_OPTIONS(NSInteger, OddityRequestWaitAlertMode) {
    OddityRequestWaitAlertModeAnimateWithLabel = 1,
    OddityRequestWaitAlertModeAnimate = 2,
    OddityRequestWaitAlertModeLabel = 3,
};

@interface OddityRequestWaitView : UIView

-(void)show:(NSString *)message;

-(void)show:(NSString *)message mode:(OddityRequestWaitAlertMode)mode;

@end
