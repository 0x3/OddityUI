//
//  OddityRequestWaitView.m
//  Pods
//
//  Created by 荆文征 on 2017/6/15.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityRequestWaitView.h"

@interface OddityRequestWaitView ()

@property(nonatomic,strong) UILabel *waitLabel;
@property(nonatomic,strong) UIActivityIndicatorView *waitIndicatorView;

@end

@implementation OddityRequestWaitView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        _waitIndicatorView = [[UIActivityIndicatorView alloc] init];
        _waitIndicatorView.hidesWhenStopped = true;
        _waitIndicatorView.activityIndicatorViewStyle = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNight ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray;
        [self addSubview:_waitIndicatorView];
        
        _waitLabel = [[UILabel alloc] init];
        _waitLabel.font = UIFont.font_t_6;
        _waitLabel.textColor = UIColor.color_t_2;
        [self addSubview:_waitLabel];
    }
    return self;
}

-(void)show:(NSString *)message{

    self.waitLabel.text = message;
    
}

-(void)show:(NSString *)message mode:(OddityRequestWaitAlertMode)mode{

    self.waitLabel.text = message;
    
    switch (mode) {
        case OddityRequestWaitAlertModeLabel:{
        
            self.waitLabel.hidden = false;
            self.waitIndicatorView.hidden = true;
            
            [self.waitLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.center.mas_equalTo(self);
            }];
        }
            break;
        case OddityRequestWaitAlertModeAnimateWithLabel:{
            
            self.waitLabel.hidden = false;
            self.waitIndicatorView.hidden = false;
            [self.waitIndicatorView startAnimating];
            
            [self.waitLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerX.mas_equalTo(self);
                make.centerY.mas_equalTo(-16);
            }];
            
            [self.waitIndicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerX.mas_equalTo(self);
                make.centerY.mas_equalTo(16);
            }];
        }
            break;
        case OddityRequestWaitAlertModeAnimate:{
            
            self.waitLabel.hidden = true;
            self.waitIndicatorView.hidden = false;
            [self.waitIndicatorView startAnimating];
            
            [self.waitIndicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.center.mas_equalTo(self);
            }];
        }
            break;
    }
    
    [self layoutIfNeeded];
}

@end
