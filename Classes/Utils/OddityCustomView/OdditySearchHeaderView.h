//
//  OdditySearchHeaderView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <UIKit/UIKit.h>
#import "OdditySearchTextField.h"

@interface OdditySearchHeaderView : UIView

/// 搜索输入框
@property(nonatomic,strong)OdditySearchTextField *inputTextFiled;

-(void)makeFeed;
@end
