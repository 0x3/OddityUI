//
//  OdditySearchHeaderView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityUISetting.h"
#import <Masonry/Masonry.h>
#import "OddityCategorys.h"
#import "OdditySearchHeaderView.h"

@interface OdditySearchHeaderView()

@property(nonatomic,strong) UIImageView * iconImageView;
@end

@implementation OdditySearchHeaderView


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor redColor];
        
        _inputTextFiled = [[OdditySearchTextField alloc] init];
        _inputTextFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
        _inputTextFiled.placeholder = @"搜索";
        _inputTextFiled.font = [UIFont font_t_6];
        self.layer.borderWidth = 0.5;
        _inputTextFiled.layer.cornerRadius = 14;
        
        
        self.iconImageView = [[UIImageView alloc] initWithImage:UIImage.oddity_search_search_image];
        self.iconImageView.tag = 110;
        _inputTextFiled.leftView = self.iconImageView;
        _inputTextFiled.leftViewMode = UITextFieldViewModeAlways;
        _inputTextFiled.edgeInsets = UIEdgeInsetsMake(0, 12, 0, 6);
        _inputTextFiled.textColor = [UIColor color_t_3];
        _inputTextFiled.tintColor = [UIColor color_t_3];
        
        _inputTextFiled.returnKeyType = UIReturnKeySearch;
        _inputTextFiled.enablesReturnKeyAutomatically = true;
        
        [self addSubview:_inputTextFiled];
        [_inputTextFiled mas_makeConstraints:^(MASConstraintMaker *make) {
           
            make.centerY.equalTo(self);
            make.height.mas_equalTo(28);
            make.right.mas_equalTo(-12);
            make.left.mas_equalTo(12);
        }];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    }
    return self;
}


-(void)refreshThemeMethod{
    
    switch ([[OddityUISetting shareOddityUISetting] oddityThemeMode]) {
        case OddityThemeModeNight:
            
            _inputTextFiled.keyboardAppearance = UIKeyboardAppearanceDark;
            break;
            
        case OddityThemeModeNormal:
            
            _inputTextFiled.keyboardAppearance = UIKeyboardAppearanceLight;
            break;
    }
}


-(void)makeFeed{

    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_6],
                             NSForegroundColorAttributeName: [UIColor color_t_4]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:@"搜索" attributes:attris];
    
    [self.inputTextFiled setAttributedPlaceholder:attriString];
    
    self.inputTextFiled.backgroundColor = [UIColor color_t_9];
    self.backgroundColor = [UIColor color_t_6];
    self.inputTextFiled.textColor = [UIColor color_t_2];
    self.layer.borderColor = [UIColor color_t_5].CGColor;
    
    self.iconImageView.image = UIImage.oddity_search_search_image;
}

@end
