//
//  OdditySearchTextField.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityUISetting.h"
#import "OdditySearchTextField.h"

@implementation OdditySearchTextField

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsZero;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
        [self refreshThemeMethod];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.edgeInsets = UIEdgeInsetsZero;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
        [self refreshThemeMethod];
    }
    return self;
}

-(void)refreshThemeMethod{
    
    self.keyboardAppearance = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNight ? UIKeyboardAppearanceDark : UIKeyboardAppearanceDefault;
}



- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect iconRect = [super textRectForBounds:bounds];
    iconRect.origin.y += self.edgeInsets.top;// 右偏10
    iconRect.origin.x += self.edgeInsets.right;// 右偏10
    return iconRect;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    CGRect iconRect = [super editingRectForBounds:bounds];
    iconRect.origin.y += self.edgeInsets.top;// 右偏10
    iconRect.origin.x += self.edgeInsets.right;// 右偏10
    return iconRect;
}

-(CGRect)leftViewRectForBounds:(CGRect)bounds{
    
    CGRect iconRect = [super leftViewRectForBounds:bounds];
    iconRect.origin.y += self.edgeInsets.top;// 右偏10
    iconRect.origin.x += self.edgeInsets.left;// 右偏10
    return iconRect;
}

@end
