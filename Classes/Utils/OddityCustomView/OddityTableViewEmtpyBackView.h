//
//  OddityTableViewEmtpyBackView.h
//  Pods
//
//  Created by 荆文征 on 2017/6/22.
//
//

#import <UIKit/UIKit.h>


/**
 数据为空的站位图

 - OddityTableViewEmtpyBackViewModeComment: 我的评论页面没有评论
 - OddityTableViewEmtpyBackViewModeCollect: 我的收藏没有内容
 - OddityTableViewEmtpyBackViewModeMessage: 我的消息 没有消息
 - OddityTableViewEmtpyBackViewModeSofa: 评论详情页 没有评论抢沙发
 */
typedef NS_OPTIONS(NSInteger, OddityTableViewEmtpyBackViewMode) {
    OddityTableViewEmtpyBackViewModeComment = 1,
    OddityTableViewEmtpyBackViewModeCollect = 2,
    OddityTableViewEmtpyBackViewModeMessage = 3,
    OddityTableViewEmtpyBackViewModeSofa = 4,
    OddityTableViewEmtpyBackViewModeSearch = 5
};

@interface OddityTableViewEmtpyBackView : UIView

-(instancetype)refresh;
-(instancetype)initWithMode:(OddityTableViewEmtpyBackViewMode)mode offset:(CGFloat)offset;

@end
