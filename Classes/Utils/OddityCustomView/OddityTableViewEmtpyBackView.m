//
//  OddityTableViewEmtpyBackView.m
//  Pods
//
//  Created by 荆文征 on 2017/6/22.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityTableViewEmtpyBackView.h"

@interface OddityTableViewEmtpyBackView()

@property (nonatomic,strong) UILabel *descLabel;
@property (nonatomic,strong) UIImageView *descImageView;

@property(nonatomic,assign) OddityTableViewEmtpyBackViewMode mode;
@end

@implementation OddityTableViewEmtpyBackView

-(instancetype)initWithMode:(OddityTableViewEmtpyBackViewMode)mode offset:(CGFloat)offset{
    
    self = [super initWithFrame:CGRectZero];
    if (self) {
    
        _mode = mode;
        
        _descImageView = [[UIImageView alloc]init];
        [self addSubview:_descImageView];
        [_descImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self);
            make.centerY.equalTo(self).offset(offset);
            
        }];
        
        _descLabel = [[UILabel alloc] init];
        _descLabel.numberOfLines = 0;
        _descLabel.font = [UIFont font_t_4];
        _descLabel.text  = @"暂无评论";
        [self addSubview:_descLabel];
        
        [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_descImageView.mas_bottom);
            make.centerX.equalTo(self);
        }];
        
        switch (self.mode) {
            case OddityTableViewEmtpyBackViewModeSofa:{
                
                _descLabel.text  = @"快来抢沙发吧";
            }
                break;
            case OddityTableViewEmtpyBackViewModeComment:{
                
                _descLabel.text  = @"还没有评论哦，抓紧去评论吧";
            }
                break;
            case OddityTableViewEmtpyBackViewModeCollect:{
                
                _descLabel.text  = @"没有任何收藏哦";
            }
                break;
            case OddityTableViewEmtpyBackViewModeMessage:{
                
                _descLabel.text  = @"没有收到任何消息哦";
            }
                break;
            case OddityTableViewEmtpyBackViewModeSearch:{
            
                _descLabel.text  = @"没有搜搜索到任何内容";
            }
                break;
        }
    }
    return self;
}

-(instancetype)refresh{

    _descLabel.textColor = [UIColor color_t_3];
    self.backgroundColor = [UIColor color_t_6];
    
    switch (self.mode) {
        case OddityTableViewEmtpyBackViewModeSofa:{
        
            self.descImageView.image = UIImage.oddity_emtpy_sofa_image;
        }
            break;
        case OddityTableViewEmtpyBackViewModeComment:{
            
            self.descImageView.image = UIImage.oddity_emtpy_mycomment_image;
        }
            break;
        case OddityTableViewEmtpyBackViewModeCollect:{
            
            self.descImageView.image = UIImage.oddity_emtpy_collect_image;
        }
            break;
        case OddityTableViewEmtpyBackViewModeMessage:{
            
            self.descImageView.image = UIImage.oddity_emtpy_message_image;
        }
            break;
        case OddityTableViewEmtpyBackViewModeSearch:{
            
            self.descImageView.image = UIImage.oddity_emtpy_search_image;
        }
            break;
    }
    
    [self layoutIfNeeded];
    
    return self;
}

@end
