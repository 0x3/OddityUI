//
//  OddityThemeImageView.m
//  Pods
//
//  Created by 荆文征 on 2017/5/11.
//
//
#import "OdditySetting.h"
#import <Masonry/Masonry.h>
#import "OddityThemeImageView.h"

@interface OddityThemeImageView()

@property(nonatomic,strong)UIView *nightModeView;

@end

@implementation OddityThemeImageView

-(instancetype)initWithImage:(UIImage *)image{

    self = [super initWithImage:image];
    if (self) {
        
        _nightModeView = [[UIView alloc] init];
        _nightModeView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.25];
        _nightModeView.userInteractionEnabled = false;
        [self addSubview:_nightModeView];
        [_nightModeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
    
        _nightModeView = [[UIView alloc] init];
        _nightModeView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.25];
        _nightModeView.userInteractionEnabled = false;
        [self addSubview:_nightModeView];
        [_nightModeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
    }
    return self;
}

-(void)layoutSubviews{

    [super layoutSubviews];
    
    _nightModeView.hidden = [OddityUISetting shareOddityUISetting].oddityThemeMode == OddityThemeModeNormal;
}

@end


@interface OddityThemeCImageView()

@property(nonatomic,strong)UIView *nightModeView;

@end

@implementation OddityThemeCImageView

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        _nightModeView = [[UIView alloc] init];
        _nightModeView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.25];
        _nightModeView.userInteractionEnabled = false;
        [self addSubview:_nightModeView];
        [_nightModeView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(0);
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
        }];
    }
    return self;
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    _nightModeView.clipsToBounds = true;
    _nightModeView.layer.cornerRadius = CGRectGetWidth(_nightModeView.frame)/2;
    _nightModeView.hidden = [OddityUISetting shareOddityUISetting].oddityThemeMode == OddityThemeModeNormal;
}

@end
