//
//  OddityUILabelPadding.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>

@interface OddityUILabelPadding : UILabel
@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@end
