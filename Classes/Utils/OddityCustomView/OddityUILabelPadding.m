//
//  OddityUILabelPadding.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityUILabelPadding.h"


@implementation OddityUILabelPadding

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

-(CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines{
    
    CGRect rect = [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
    rect.origin.x    -= _edgeInsets.left;
    rect.origin.y    -= _edgeInsets.top;
    rect.size.width  += (_edgeInsets.left + _edgeInsets.right);
    rect.size.height += (_edgeInsets.top + _edgeInsets.bottom);
    return rect;
}

@end
