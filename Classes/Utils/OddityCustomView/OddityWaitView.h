//
//  OddityWaitView.h
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import <UIKit/UIKit.h>

@interface OddityWaitView : UIView

-(void)goNormaleStyle;

-(void)goErrorStyle:(void(^)())click;

@end
