//
//  OddityWaitView.m
//  Pods
//
//  Created by 荆文征 on 2017/4/24.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityWaitView.h"

@interface OddityWaitView()

@property (nonatomic, strong) UILabel *label;

@property (nonatomic, strong) UILabel *label2;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *imageView1;
@property (nonatomic, strong) UIImageView *imageView2;

@property (nonatomic, strong) UITapGestureRecognizer *tapGest;


@end

@implementation OddityWaitView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor color_t_6];
        
        
        _imageView = [[UIImageView alloc] init];
        _imageView.image = [UIImage oddityImage:@"正在加载纵向_01"];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_imageView];
        
        _imageView1 = [[UIImageView alloc] init];
        _imageView1.image = [UIImage oddityImage:@"正在加载横向_01"];
        _imageView1.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_imageView1];
        
        _imageView2 = [[UIImageView alloc] init];
        _imageView2.image = [UIImage oddityImage:@"正在加载横向_01"];
        _imageView2.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_imageView2];
        
        _imageView.clipsToBounds = true;
        _imageView1.clipsToBounds = true;
        
        _label = [[UILabel alloc]init];
        _label.font = [UIFont systemFontOfSize:12];
        _label.textColor = [UIColor color_t_3];
        _label.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_label];
        
        [_imageView2 mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self).offset(-28);
            make.centerX.equalTo(self);
        }];
        
        [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self).offset(-28);
            make.centerX.equalTo(self);
            make.width.equalTo(@45);
            make.height.equalTo(@30);
        }];
        
        [_imageView1 mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.equalTo(self).offset(-28);
            make.centerX.equalTo(self);
            
            make.width.equalTo(@100);
            make.height.equalTo(@50);
            
        }];
        
        [_label mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self);
            make.centerY.equalTo(self.imageView.mas_bottom).offset(20);
        }];
        
        
        
        _label2 = [[UILabel alloc]init];
        _label2.font = [UIFont systemFontOfSize:12];
        _label2.textColor = [UIColor lightGrayColor];
        _label2.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:_label2];
        [_label2 mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self);
            make.centerY.equalTo(self.imageView2.mas_bottom).offset(20);
        }];
        
        
        
        [self layoutIfNeeded];
        
        
        [self goNormaleStyle];
    }
    return self;
}

-(void)goNormaleStyle{
    
    self.label.hidden = false;
    self.label2.hidden = true;
    
    self.imageView.hidden = false;
    self.imageView1.hidden = false;
    self.imageView2.hidden = true;
    
    _label.text = @"正在努力加载...";
    
    UIImage *image1 = [UIImage oddityImage:@"正在加载纵向_01" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image2 = [UIImage oddityImage:@"正在加载纵向_02" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image3 = [UIImage oddityImage:@"正在加载纵向_03" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image4 = [UIImage oddityImage:@"正在加载纵向_04" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image5 = [UIImage oddityImage:@"正在加载纵向_05" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image6 = [UIImage oddityImage:@"正在加载纵向_06" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image7 = [UIImage oddityImage:@"正在加载纵向_07" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image8 = [UIImage oddityImage:@"正在加载纵向_08" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    UIImage *image9 = [UIImage oddityImage:@"正在加载纵向_09" byColor:[OddityUISetting shareOddityUISetting].mainTone];
    
    _imageView.animationDuration = 1;
    _imageView.animationImages = @[image1,image2,image3,image4,image5,image6,image7,image8,image9];
    [_imageView startAnimating];
    
    UIImage *image_1 = [UIImage oddityImage:@"正在加载横向_01"];
    UIImage *image_2 = [UIImage oddityImage:@"正在加载横向_02"];
    UIImage *image_3 = [UIImage oddityImage:@"正在加载横向_03"];
    UIImage *image_4 = [UIImage oddityImage:@"正在加载横向_04"];
    UIImage *image_5 = [UIImage oddityImage:@"正在加载横向_05"];
    UIImage *image_6 = [UIImage oddityImage:@"正在加载横向_06"];
    UIImage *image_7 = [UIImage oddityImage:@"正在加载横向_07"];
    UIImage *image_8 = [UIImage oddityImage:@"正在加载横向_08"];
    UIImage *image_9 = [UIImage oddityImage:@"正在加载横向_09"];
    
    _imageView1.animationDuration = 1;
    _imageView1.animationImages = @[image_1,image_2,image_3,image_4,image_5,image_6,image_7,image_8,image_9];
    [_imageView1 startAnimating];
}


-(void)goErrorStyle:(void(^)())click{
    
    self.imageView.hidden = true;
    self.imageView1.hidden = true;
    self.imageView2.hidden = false;
    
    self.label.hidden = true;
    self.label2.hidden = false;
    
    if (!_tapGest) {
        
        _tapGest = [UITapGestureRecognizer oddity_recognizerWithHandler:^(UIGestureRecognizer *gesture) {
            
            [self removeGestureRecognizer:_tapGest];
            _tapGest = nil;
            click();
            
            [self goNormaleStyle];
        }];
        
        [self addGestureRecognizer:_tapGest];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        _label2.text = @"加载失败，点击重试";
        _imageView2.image = UIImage.oddity_waitview_error_image;
        
        [_imageView2 layoutIfNeeded];
    });
}

@end
