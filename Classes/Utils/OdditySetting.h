//
//  OdditySetting.h
//  Pods
//
//  Created by 荆文征 on 2017/4/6.
//
//

#import <UIKit/UIKit.h>
#import "OddityUISetting.h"

@class OdditySetting,OddityShareObject,OddityUISetting;
@protocol OdditySettingDelegate;


/**
 分享类型对象

 - OddityShareModeWeChatFriends: 分享到微信好友
 - OddityShareModeQQFriends: QQ好友
 - OddityShareModeSina: 新浪微博
 - OddityShareModeEmail: 邮件
 - OddityShareModeSMS: 短信
 - OddityShareModeCopyLink: 拷贝链接
 */
typedef NS_ENUM(NSInteger, OddityShareMode) {
    OddityShareModeWeChatmoMents = 2,
    OddityShareModeWeChatFriends = 1,
    OddityShareModeQQFriends = 4,
    OddityShareModeSina = 0,
    OddityShareModeEmail = 14,
    OddityShareModeSMS = 13,
    OddityShareModeCopyLink = 5,
    OddityShareModeTheme = 6,
    OddityShareModeFontSize = 7,
};


/**
 UMengShare 根据 OddityShareMode 获得 相对应的分享平台
 
 @param mode OddityShareMode
 @return 平台编码
 */
extern int ODDITY_UMENG_SHARE_PLATFORM(OddityShareMode mode);

/**
 ShareSDK 根据 OddityShareMode 获得 相对应的分享平台
 
 @param mode OddityShareMode
 @return 平台编码
 */
extern int ODDITY_SHARE_SDK_PLATFORM(OddityShareMode mode);


@interface OdditySetting : NSObject


/**
 *  设置 Delegate
 */
@property(nonatomic,weak) id<OdditySettingDelegate> delegate;


/**
 *  是否隐藏首定。默认为fasle
 *  在为false的情况下，会根据用户是否第一次进入app来决定是不是显示。
 *
 */
@property(assign,nonatomic) BOOL isHiddenFirstSubscribe;


/**
 是否可以 滑动来切换子频道
 */
@property(assign,nonatomic) BOOL isCanScorllChangeSubChannel;

/**
 *  新闻详情页是否缓存图片内容
 *  搞笑频道中含有大量 gif ，可能会使应用短时间内增长到一个很夸张的大小，使用请做好缓存清楚工作
 *  。好处是：用户即使在断网的状态还是可以查看到已经查阅过的新闻。
 *
 */
@property(assign,nonatomic) BOOL isCacheDetailImage NS_AVAILABLE_IOS(9_0);

/**
 *  是否在点击视频小窗口视图的时候 先判断一下是否可以滑动到当前播放的UItablViewCell。
 *  如果可以滑动到那么优先滑动到当前播放的 UITableViewCell 否则直接进入视频播放详情页
 *
 */
//@property(assign,nonatomic) BOOL isClickSmallVideoPlayerViewJudgeToDetailViewController;

/**
 *  需要删除 也就是 永久不向用户展示的 频道集合
 */
@property(strong,nonatomic) NSArray *deleteArray __attribute((deprecated("逻辑处理发生变化 该参数 不再处理")));

/**
 *  默认第一次加载完成后数据 隐藏 用户根据需求选择展示的集合
 */
@property(strong,nonatomic) NSArray *hiddenArray __attribute((deprecated("逻辑处理发生变化 该参数 不再处理")));

/**
 *  默认 的 第一次 的排序，仅生效一次
 */
@property(strong,nonatomic) NSArray *sortArray __attribute((deprecated("逻辑处理发生变化 该参数 不再处理")));

/**
 *  修改名称的方法啊 会一直影响
 */
@property(strong,nonatomic) NSDictionary *renameDictionary __attribute((deprecated("逻辑处理发生变化 该参数 不再处理")));

/**
 *  分享集合 设置 
 *  分为 ： [[OddityShareObject-OddityShareObject],[OddityShareObject-OddityShareObject]]
 *  分为 ： [OddityShareObject-OddityShareObject-OddityShareObject-OddityShareObject]
 */
@property(strong,nonatomic) NSArray *shareObjectArray;

/**
 *  不愿意分享 默认为 false
 *  true  不愿意分享新闻
 *  false 愿意分享新闻 需要针对这个问题 进行配置
 */
@property(assign,nonatomic) BOOL isDontWantToShare;

/**
 *  是否一直显示播放器 默认为 false
 *  true  一直都显示
 *  false 滑动频道后清除视频
 */
@property(assign,nonatomic) BOOL isWatchVideoAllTheTime;

/**
 *  单例的 设置对象
 *
 *  @return OdditySetting
 */
+(instancetype)shareOdditySetting;

/**
 *  确认开启收集 bug 方法
 *
 */
-(void)BugRequestCollectionMethod;


/**
 第三方登录

 @param mode 登陆的额方式  微信或者 新浪微博
 @param suid 第三方用户ID
 @param stoken 第三方用户 token
 @param sexp 第三方 用户认证过期时间
 @param uname 用户的名称
 @param sex 用户的性别
 @param ava 用户的头像
 @param completed 完成的回调
 */
-(void)thirdPartySign:(OddityShareMode)mode suid:(NSString *)suid stoken:(NSString *)stoken sexpires:(NSString *)sexp username:(NSString *)uname gender:(NSString *)sex avatar:(NSString *)ava complete:(void(^)(BOOL success,NSString *mess))completed;



/**
 根据用户完成的新闻ID，跳转到奇点提供的新闻详情展示页面

 @param nid 新闻 ID，例如:(21625236) 。如果为空 则不跳转到详情页 就停留在列表页
 @param channel 频道 ID，例如:(17 养生频道)
 @param title 频道 标题，例如:(养生 - 健康)
 @param viewController 依据的视图
 @param complete 再没有进入奇点资讯页面，sdk没有配置信息的时候，会进行配置。如果出现配置失败的时候，该回调调用，请开发者，设置相对应的提示信息。 如果为空 则采用默认的 提示信息
 */
-(void)toNewDetailWithNid:(id )nid channelId:(int)channel channelTitle:(NSString *)title presentingVC:(UIViewController *)viewController firstReuqestErrorBlock:(void (^)())complete;


/**
 回到根视图

 @param flag 是否dismiss的时候使用动画回退
 @param completion 是否完成回调
 */
-(void)backRootViewControllerAnimated: (BOOL)flag completion: (void (^ __nullable)(void))completion;

@end






















/**
 *  新闻的ViewController Delegate
 */
@protocol OdditySettingDelegate<NSObject>



@optional


/// -----------  新闻列表页打点


/**
 当用户开始请求新闻列表数据时 开始请求时 调用该方法

 @param cname 频道名称
 */
-(void)didBeginLoadingNewArrayAction:(NSString *_Nonnull)cname;


/**
 当用户 请求新闻列表数据成功的时 调用 该方法

 @param channelName 频道 名称
 */
-(void)didFinishRequestNewListAction:(NSString *_Nonnull)channelName;



/**
 请求新闻列表数据 失败的时候， 调用该方法

 @param channelName 频道 名称
 */
-(void)didFailRequestNewListAction:(NSString *_Nonnull)channelName;


/**
 用户切换频道

 @param fIndex from 频道的ID
 @param tIndex to   频道的ID
 */
-(void)didChangeChannelActionFromIndex:(NSInteger)fIndex toIndex:(NSInteger)tIndex;


/// -----------  新闻详情页打点


/**
 开始请求新闻详情

 @param nid 新闻 ID
 */
-(void)didEnterInNewContentAction:(int)nid;


/**
 当用户成功加载到新闻的数据时，调用该方法

 @param nid 新闻 ID
 */
-(void)didFinishRequestNewContentAction:(int)nid;


/**
 当用户请求新闻，加载数据失败时，调用该方法

 @param nid 新闻id
 */
-(void)didFailRequestNewContentAction:(int)nid;



/// -----------  新闻详情页打点


/**
 当用户第一次进入 应用的时，调用该放哪发噶
 */
-(void)didEnterInSDKAction;


/**
 用户 滑动屏幕 或者 切换 频道时，屏幕上出现了 整一屏幕 新的新闻时候
 调用该方法
 */
-(void)didLookUpAllNewScreen;



/// -----------  新闻详情页打点

/**
 *  [废弃打点 新闻列表] -> 用户滑动打点
 */
-(void)didScrollNewListAction:(NSString *_Nonnull)cname __attribute((deprecated("业务问题 该方法废弃")));

/**
 *  [废弃打点 新闻列表] -> 用户结束滑动
 */
-(void)didEndScrollNewListAction:(NSString *_Nonnull)cname __attribute((deprecated("业务问题 该方法废弃")));


/**
 *   初始化自定义跳转动画ViewController
 *
 *  @param mode   分享的类型 参见 OddityShareMode
 *  @param obj    分享的对象 参见 OddityShareObject
 *  @param viewController    分享的视图
 *
 */
-(void)didClickShareActionWithMode:(OddityShareMode)mode shareObject:(OddityShareObject *_Nonnull)obj shareViewController:(UIViewController *_Nonnull)viewController;


/**
 用户点击了 登陆按钮
 
 该方法，非 奇点资讯 本身应用调用无效 不需实现

 @param mode 登陆方式
 */
-(void)didClickThirdSignActionWithMode:(OddityShareMode)mode;


///&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  新增打点

/**
 当在黄历的新闻列表页面 点击新闻 进入新闻详情 调用该方法
 */
-(void)didJustToNewDetailViewControllerByHuangLi;

/**
 是否进入了 针对于 黄历天气 设置的新闻列表页面
 */
-(void)didAppearHuangLiLonelyNewArrayViewController;


/**
 针对于 黄历天气 设置的新闻列表页面 退出的方法
 */
-(void)didDismissHuangLiLonelyNewArrayViewController;

@end


@interface OddityShareObject : NSObject

/**
 *  分享对象的 图片 极可能为空
 */
@property(nonatomic,strong,readonly) UIImage * _Nullable shareImage;

/**
 *  分享对象的 标题
 */
@property(nonatomic,strong,readonly) NSString * _Nonnull shareTitle;

/**
 *  分享对象的 url string
 */
@property(nonatomic,strong,readonly) NSString * _Nonnull shareURLString;

/**
 *  分享对象的 图片 url string
 */
@property(nonatomic,strong,readonly) NSString * _Nonnull shareImageURLString;

-(instancetype _Nonnull)makeImage:(UIImage *_Nonnull)image;


-(instancetype _Nonnull )initWithTitle:(NSString *_Nonnull)title coverImageUrl:(NSString *_Nonnull)imageUrlString urlString:(NSString *_Nonnull)urlString;

@end



@interface OddityShareModeObject : NSObject

/**
 *  分享的模式
 */
@property(nonatomic,assign,readonly) OddityShareMode mode;

/**
 *  模式的名称
 */
@property(nonatomic,strong,readonly) NSString *_Nonnull modeIconName;

/**
 *  模块的图片
 */
@property(nonatomic,strong,readonly) UIImage *_Nonnull modeIconImage;

/**
 *  初始化一个模块
 */
- (instancetype _Nonnull)initWithMode:(OddityShareMode)mode;


/**
 刷新 Mode
 */
-(void)refreshMode;
@end
