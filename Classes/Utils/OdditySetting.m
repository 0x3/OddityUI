//
//  OdditySetting.m
//  Pods
//
//  Created by 荆文征 on 2017/4/6.
//
//

#import "OddityShareUser.h"
#import "UIImage+Theme.h"
#import "OdditySetting.h"
#import "OddityCategorys.h"
#import <KSCrash/KSCrashInstallationStandard.h>
#import "OddityHuangLiArrayListViewController.h"
#import "OddityNewsDetailViewController.h"
#import "OddityAlertViewViewController.h"

int ODDITY_UMENG_SHARE_PLATFORM(OddityShareMode mode) {
    
    return (int)mode;
}

int ODDITY_SHARE_SDK_PLATFORM(OddityShareMode mode){
    
    switch (mode) {
        case OddityShareModeSina:
            return 1;
        case OddityShareModeWeChatFriends:
            return 22;
        case OddityShareModeWeChatmoMents:
            return 23;
        case OddityShareModeEmail:
            return 18;
        case OddityShareModeSMS:
            return 19;
        case OddityShareModeQQFriends:
            return 24;
        default:
            return 0;
    }
}

@interface OdditySetting()

@property(nonatomic,strong) OddityWaitView *waitView;

@end

@implementation OdditySetting

+(instancetype)shareOdditySetting{
    
    static OdditySetting* odditySetting = nil;
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        odditySetting = [[self alloc] init] ;
    }) ;
    
    return odditySetting ;
}

-(void)BugRequestCollectionMethod{
    
    KSCrashInstallationStandard* installation = [KSCrashInstallationStandard sharedInstance];
    installation.url = [NSURL URLWithString:@"https://collector.bughd.com/kscrash?key=4b49c77f385e4b38a84eaa80eb0e81a0"];
    [installation install];
    [installation sendAllReportsWithCompletion:nil];
}

-(void)thirdPartySign:(OddityShareMode)mode suid:(NSString *)suid stoken:(NSString *)stoken sexpires:(NSString *)sexp username:(NSString *)uname gender:(NSString *)sex avatar:(NSString *)ava complete:(void (^)(BOOL, NSString *))completed{

    [NSNotificationCenter.defaultCenter postNotificationName:Oddity_Register_User_start_Notifition_Name_String object:nil];
    
    [OddityShareUser.defaultUser thirdPartySign:mode suid:suid stoken:stoken sexpires:sexp username:uname gender:sex avatar:ava complete:completed];
}


-(void)toNewDetailWithNid:(id )nid channelId:(int)channel channelTitle:(NSString *)title presentingVC:(UIViewController *)viewController firstReuqestErrorBlock:(void (^)())complete{
    
//    [viewController oddity_showWaitView:0];
    
    __weak typeof(self) weakSelf = self;
    if (!OddityShareUser.defaultUser.token) {
        
        [self show];
        
        [OddityShareUser requestToken:^(NSString * _Nullable token) {
            
            [OddityChannel reloadDataSourceSort:^(BOOL success) {
                
                [weakSelf hide];
                
                if (success) {
                    
                    [weakSelf executioner:nid channelId:channel channelTitle:title presentingVC:viewController];
                }else{
                    
                    if (complete) {
                        
                        complete();
                    }else{
                    
                        OddityAlertViewViewController *alertController = [[[OddityAlertViewViewController alloc] initWitch:@""] makeChange:@"首次配置失败，请确认网络后重试" after:1 block:^{
                            
                            [viewController dismissViewControllerAnimated:true completion:nil];
                        }];
                        
                        [viewController presentViewController:alertController animated:true completion:nil];
                    }
                }
            }];
        }];
    }else{
        
        [self executioner:nid channelId:channel channelTitle:title presentingVC:viewController];
    }
}
-(void)executioner:(id )nid channelId:(int)channel channelTitle:(NSString *)title presentingVC:(UIViewController *)viewController{

    OddityChannel *channelObj = [OddityChannel objectInRealm: RLMRealm.OddityRealm forPrimaryKey:@(channel)];
    
    UIViewController *baseViewController = viewController;
    
    if (channelObj) {
        
        OddityChannelNews *channelNews = [OddityChannelNews getObjectBy: channelObj];
        
        baseViewController = [[OddityHuangLiArrayListViewController alloc] initWithChannel:channelNews title:title clearBackColor:nid];
        
        [viewController presentViewController:baseViewController animated: !nid completion: nil];
    }
    
    if (nid) {
        
        OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc] initWithNid:[nid intValue] isVideo:false];
        
        //            [newDACViewController makeLogType:new.logtype AndLogChid:new.logchid cid: new.channel];
        
        [baseViewController presentViewController:newDACViewController animated:true completion: nil];
        
        
        [OddityLogObject createAppAction:OddityLogAtypeJumpDetail fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:false params:nil];
        
        if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didJustToNewDetailViewControllerByHuangLi)] ){
            
            [[OdditySetting shareOdditySetting].delegate didJustToNewDetailViewControllerByHuangLi];
        }
    }

}

-(void)backRootViewControllerAnimated: (BOOL)flag completion: (void (^ __nullable)(void))completion{

    UIViewController *viewController = UIViewController.oddityCurrentViewController;
    
    while (viewController.presentingViewController) {
        
        viewController = viewController.presentingViewController;
    }
    
    if (viewController) {
        
        [viewController dismissViewControllerAnimated:flag completion:completion];
    }else if(completion){
    
        completion();
    }
}

-(void)show{

    self.waitView = [[OddityWaitView alloc] init];
    [self.waitView goNormaleStyle];
    
    [UIApplication.sharedApplication.keyWindow addSubview:self.waitView];
    [self.waitView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.bottom.left.right.mas_equalTo(0);
    }];
}


-(void)hide{

    [self.waitView removeFromSuperview];
    self.waitView = nil;
}

-(NSArray *)shareObjectArray{

    if (_shareObjectArray) {
        
        for (id object in _shareObjectArray) {
            
            if ([object isKindOfClass:OddityShareModeObject.class]) {
                
                [((OddityShareModeObject *)object) refreshMode];
            }
            
            if ([object isKindOfClass:NSArray.class]) {
                
                for (OddityShareModeObject *obj in (NSArray *)object) {
                    
                    [obj refreshMode];
                }
            }
        }
        
        return _shareObjectArray;
    }
    
    OddityShareModeObject *wmoments = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeWeChatmoMents)];
    OddityShareModeObject *wfriends = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeWeChatFriends)];
    OddityShareModeObject *qfriends = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeQQFriends)];
    OddityShareModeObject *sina = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeSina)];
    
    NSArray *firstArray = @[wmoments,wfriends,qfriends,sina];
    
    OddityShareModeObject *sms = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeSMS)];
    OddityShareModeObject *email = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeEmail)];
    OddityShareModeObject *copylink = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeCopyLink)];
    OddityShareModeObject *night = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeTheme)];
    OddityShareModeObject *fontsize = [[OddityShareModeObject alloc] initWithMode:(OddityShareModeFontSize)];
    
    NSArray *sendArray = @[sms,email,copylink,night,fontsize];
    
    return @[firstArray,sendArray];
}


-(NSArray *)sortArray{

    NSMutableArray *copySortArray = [_sortArray mutableCopy];
    
    if ([copySortArray indexOfObject:@"奇点"] == NSNotFound) {
        
        [copySortArray insertObject:@"奇点" atIndex:0];
    }
    
    return copySortArray;
}

@end




@implementation OddityShareObject

-(instancetype)initWithTitle:(NSString *)title coverImageUrl:(NSString *)imageUrlString urlString:(NSString *)urlString{

    self = [super init];
    
    if (self) {
        
        _shareTitle = title;
        _shareURLString = urlString;
        _shareImageURLString = imageUrlString;
    }
    
    return self;
}


-(instancetype)makeImage:(UIImage *)image{

    _shareImage = image;
    
    return self;
}


-(NSString *)description{

    return [NSString stringWithFormat:@"title:%@ \nimageUrl:%@ \nurl:%@ \n:%@",self.shareTitle,self.shareImageURLString,self.shareURLString,self.shareImage];
}

@end


@implementation OddityShareModeObject

- (instancetype)initWithMode:(OddityShareMode)mode
{
    self = [super init];
    if (self) {
        
        _mode = mode;
        
        [self refreshMode];
    }
    return self;
}


-(void)refreshMode{

    switch (self.mode) {
        case OddityShareModeWeChatmoMents:{
            
            _modeIconName = @"微信朋友圈";
            _modeIconImage = [UIImage oddity_share_circle_image];
        }
            break;
            
        case OddityShareModeWeChatFriends:{
            
            _modeIconName = @"微信好友";
            _modeIconImage = [UIImage oddity_share_wechat_image];
        }
            break;
        case OddityShareModeQQFriends:{
            
            _modeIconName = @"QQ好友";
            _modeIconImage = [UIImage oddity_share_qq_image];
        }
            break;
        case OddityShareModeSina:{
            
            _modeIconName = @"新浪微博";
            _modeIconImage = [UIImage oddity_share_sina_image];
        }
            break;
        case OddityShareModeEmail:{
            
            _modeIconName = @"邮件";
            _modeIconImage = [UIImage oddity_share_email_image];
        }
            break;
        case OddityShareModeSMS:{
            
            _modeIconName = @"短信";
            _modeIconImage = [UIImage oddity_share_message_image];
        }
            break;
        case OddityShareModeCopyLink:{
            
            _modeIconName = @"转发链接";
            _modeIconImage = [UIImage oddity_share_link_image];
        }
            break;
        case OddityShareModeFontSize:{
            
            _modeIconName = @"字体大小";
            _modeIconImage = [UIImage oddity_share_fontsize_image];
        }
            break;
        case OddityShareModeTheme:{
            
            _modeIconName = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNight ? @"日间模式" : @"夜间模式" ;
            _modeIconImage = [UIImage oddity_share_theme_image];
        }
            break;
    }
}

@end
