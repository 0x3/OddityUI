//
//  OddityUISetting.h
//  Pods
//
//  Created by 荆文征 on 2017/5/12.
//
//

#import <UIKit/UIKit.h>

/**
 奇点主题
 
 - OddityThemeModeNormal: 普通模式
 - OddityThemeModeNight: 夜间模式
 */
typedef NS_ENUM(NSUInteger, OddityThemeMode) {
    OddityThemeModeNormal = 0,
    OddityThemeModeNight = 1
};


/**
 奇点字体大小控制
 
 - OddityFontModeNormal: 普通字体大小
 - OddityFontModeBig: 大字体
 - OddityFontModeBigPlus: 超大字体
 */
typedef NS_ENUM(NSUInteger, OddityFontMode) {
    OddityFontModeNormal = 0,
    OddityFontModeBig = 1,
    OddityFontModeBigPlus = 2,
};

@interface OddityUISetting : NSObject


/**
 *  当前的 sdk 主色调
 */
@property(strong,nonatomic,readonly)UIColor* mainTone;


/**
 *  当前的 sdk 标题颜色 仅指 Navgation
 */
@property(strong,nonatomic,readonly)UIColor* titleColor;


/**
 *  当前的 sdk 背景颜色 仅指 Navgation
 */
@property(strong,nonatomic,readonly)UIColor* backColor;


/**
 *  当前的 sdk 边框颜色 仅指 Navgation
 */
@property(strong,nonatomic,readonly)UIColor* borderColor;


/**
 *  当前的 sdk Status style
 */
@property(assign,nonatomic,readonly) UIStatusBarStyle statusStyle;

/**
 *  小屏幕视频模式的 margin 属性
 */
@property(assign,nonatomic) UIEdgeInsets smallVideoInsets;

/**
 *  奇点SDK的模式
 */
@property(assign,nonatomic) OddityThemeMode oddityThemeMode;

/**
 *  奇点字体大小
 */
@property(assign,nonatomic) OddityFontMode oddityFontMode;

/**
 *  单例的 设置对象
 *
 *  @return OdditySetting
 */
+(instancetype)shareOddityUISetting;


/**
 设置 sdk 主色调
 
 该色调会与项目很多地方产生关联 进行颜色的绑定显示
 
 @param mainTone 主色调颜色
 @param mode 主题样式
 */
-(void)setMainToneColor:(UIColor *)mainTone forThemeMode:(OddityThemeMode)mode;

/**
 设置 sdk 标题颜色
 
 该标题颜色 仅仅是设置navagtionbar上的标题颜色
 
 @param titleColor 标题颜色
 @param mode 主题样式
 */
-(void)setNavTitleColor:(UIColor *)titleColor forThemeMode:(OddityThemeMode)mode;

/**
 设置 sdk navgationBar 的背景颜色
 
 设置背景颜色
 
 @param backColor 背景颜色
 @param mode 主题样式
 */
-(void)setNavBackColor:(UIColor *)backColor forThemeMode:(OddityThemeMode)mode;

/**
 设置 sdk navgationBar 的下边狂颜色
 
 设置 navgationbar的下边框颜色
 
 @param borderColor 边框颜色
 @param mode 主题样式
 */
-(void)setNavBorderColor:(UIColor *)borderColor forThemeMode:(OddityThemeMode)mode;


/**
 设置 StatusBar 央视
 
 @param statusStyle statusStyle
 @param mode 主题样式
 */
-(void)setStatusBarStyle:(UIStatusBarStyle )statusStyle forThemeMode:(OddityThemeMode)mode;



/**
 根据 模式 获取 sdk 主色调
 
 该色调会与项目很多地方产生关联 进行颜色的绑定显示
 
 @param mode 主题样式
 */
-(UIColor *)getMainToneColorWith:(OddityThemeMode)mode;

/**
 根据 模式 获取 sdk 标题颜色
 
 该标题颜色 仅仅是设置navagtionbar上的标题颜色
 
 @param mode 主题样式
 */
-(UIColor *)getNavTitleColorWith:(OddityThemeMode)mode;

/**
 根据 模式 获取 sdk navgationBar 的背景颜色
 
 设置背景颜色
 
 @param mode 主题样式
 */
-(UIColor *)getNavBackColorWith:(OddityThemeMode)mode;

/**
 根据 模式 获取 sdk navgationBar 的下边狂颜色
 
 设置 navgationbar的下边框颜色
 
 @param mode 主题样式
 */
-(UIColor *)getNavBorderColorWith:(OddityThemeMode)mode;


/**
 根据 模式 获取 StatusBar 央视
 
 @param mode 主题样式
 */
-(UIStatusBarStyle )getStatusBarStyleWith:(OddityThemeMode)mode;

@end

