//
//  OddityUISetting.m
//  Pods
//
//  Created by 荆文征 on 2017/5/12.
//
//

#import "OddityUISetting.h"
#import "OddityLogObject.h"

#define OddityFontModeStringKey @"OddityFontModeStringKey"

#define OddityThemeModeStringKey @"OddityThemeModeStringKey"

@interface OddityUISetting()

@property(strong,nonatomic)UIColor* normalMainTone;
@property(strong,nonatomic)UIColor* normalTitleColor;
@property(strong,nonatomic)UIColor* normalBackColor;
@property(strong,nonatomic)UIColor* normalBorderColor;
@property(assign,nonatomic)UIStatusBarStyle normalStatusStyle;


@property(strong,nonatomic)UIColor* nightMainTone;
@property(strong,nonatomic)UIColor* nightTitleColor;
@property(strong,nonatomic)UIColor* nightBackColor;
@property(strong,nonatomic)UIColor* nightBorderColor;
@property(assign,nonatomic)UIStatusBarStyle nightStatusStyle;
@end

@implementation OddityUISetting

-(UIEdgeInsets)smallVideoInsets{

    return UIEdgeInsetsEqualToEdgeInsets(_smallVideoInsets, UIEdgeInsetsZero) ? UIEdgeInsetsMake(20, 8, 8, 8) : _smallVideoInsets;
}

-(void)setOddityThemeMode:(OddityThemeMode)oddityThemeMode{
    
    if (self.oddityThemeMode == oddityThemeMode) {
        return;
    }
    
    [OddityLogObject createAppAction:OddityLogAtypeChangeMode fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:true params:nil];
    
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)oddityThemeMode forKey:OddityThemeModeStringKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"oddityThemeModeChange" object:nil];
}

-(OddityThemeMode)oddityThemeMode{
    
    return [[NSUserDefaults standardUserDefaults] integerForKey:OddityThemeModeStringKey];
}


-(void)setOddityFontMode:(OddityFontMode)oddityFontMode{
    
    [OddityLogObject createAppAction:OddityLogAtypeFontSetting fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:true params:nil];
    
    [[NSUserDefaults standardUserDefaults] setInteger:(NSInteger)oddityFontMode forKey:OddityFontModeStringKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"oddityFontModeChange" object:nil];
}

-(OddityFontMode)oddityFontMode{
    
    return [[NSUserDefaults standardUserDefaults] integerForKey:OddityFontModeStringKey];
}


+(instancetype)shareOddityUISetting{
    
    static OddityUISetting* odditySetting = nil;
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        odditySetting = [[self alloc] init] ;
    });
    
    return odditySetting;
}

-(UIColor *)normalMainTone{

    return _normalMainTone ?: [UIColor redColor];
}

-(UIColor *)normalTitleColor{
    
    return _normalTitleColor ?: [UIColor colorWithRed:(CGFloat)(51.0f/255.0f) green:(CGFloat)(51.0f/255.0f) blue:(CGFloat)(51.0f/255.0f) alpha:1];
}

-(UIColor *)normalBackColor{
    
    return _normalBackColor ?: [UIColor whiteColor];
}

-(UIColor *)normalBorderColor{
    
    return _normalBorderColor ?: [UIColor colorWithRed:(CGFloat)(232.f/255.0f) green:(CGFloat)(232.f/255.0f) blue:(CGFloat)(232.f/255.0f) alpha:1];;
}

-(UIStatusBarStyle)normalStatusStyle{
    
    return _normalStatusStyle ?: UIStatusBarStyleDefault;
}


-(UIColor *)nightMainTone{
    
    return _nightMainTone ?: [UIColor colorWithRed:(CGFloat)(255.0f/255.0f) green:(CGFloat)(172.0f/255.0f) blue:(CGFloat)(54.0f/255.0f) alpha:1];
}

-(UIColor *)nightTitleColor{
    
    return _nightTitleColor ?: [UIColor whiteColor];
}

-(UIColor *)nightBackColor{
    
    return _nightBackColor ?: [UIColor colorWithRed:(CGFloat)(38.0f/255.0f) green:(CGFloat)(38.0f/255.0f) blue:(CGFloat)(38.0f/255.0f) alpha:1];
}

-(UIColor *)nightBorderColor{
    
    return _nightBorderColor ?: [UIColor clearColor];
}

-(UIStatusBarStyle)nightStatusStyle{
    
    return _nightStatusStyle ?: UIStatusBarStyleLightContent;
}


-(UIColor *)mainTone{

    switch (self.oddityThemeMode) {
        case OddityThemeModeNormal:
            return self.normalMainTone;
        case OddityThemeModeNight:
            return self.nightMainTone;
    }
}

-(UIColor *)titleColor{
    
    switch (self.oddityThemeMode) {
        case OddityThemeModeNormal:
            return self.normalTitleColor;
        case OddityThemeModeNight:
            return self.nightTitleColor;
    }
}

-(UIColor *)backColor{
    
    switch (self.oddityThemeMode) {
        case OddityThemeModeNormal:
            return self.normalBackColor;
        case OddityThemeModeNight:
            return self.nightBackColor;
    }
}

-(UIColor *)borderColor{
    
    switch (self.oddityThemeMode) {
        case OddityThemeModeNormal:
            return self.normalBorderColor;
        case OddityThemeModeNight:
            return self.nightBorderColor;
    }
}

-(UIStatusBarStyle )statusStyle{
    
    switch (self.oddityThemeMode) {
        case OddityThemeModeNormal:
            return self.normalStatusStyle;
        case OddityThemeModeNight:
            return self.nightStatusStyle;
    }
}


-(void)setMainToneColor:(UIColor *)mainTone forThemeMode:(OddityThemeMode)mode{

    switch (mode) {
        case OddityThemeModeNormal:{
            self.normalMainTone = mainTone;
        }
            break;
        case OddityThemeModeNight:{
            self.nightMainTone = mainTone;
        }
            break;
    }
}

-(void)setNavTitleColor:(UIColor *)titleColor forThemeMode:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            self.normalTitleColor = titleColor;
        }
            break;
        case OddityThemeModeNight:{
            self.nightTitleColor = titleColor;
        }
            break;
    }
}

-(void)setNavBackColor:(UIColor *)backColor forThemeMode:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            self.normalBackColor = backColor;
        }
            break;
        case OddityThemeModeNight:{
            self.nightBackColor = backColor;
        }
            break;
    }
}


-(void)setNavBorderColor:(UIColor *)borderColor forThemeMode:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            self.normalBorderColor = borderColor;
        }
            break;
        case OddityThemeModeNight:{
            self.nightBorderColor = borderColor;
        }
            break;
    }
}


-(void)setStatusBarStyle:(UIStatusBarStyle )statusStyle forThemeMode:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            self.normalStatusStyle = statusStyle;
        }
            break;
        case OddityThemeModeNight:{
            self.nightStatusStyle = statusStyle;
        }
            break;
    }
}





/**
 设置 sdk 主色调
 
 该色调会与项目很多地方产生关联 进行颜色的绑定显示
 
 @param mode 主题样式
 */
-(UIColor *)getMainToneColorWith:(OddityThemeMode)mode{

    switch (mode) {
        case OddityThemeModeNormal:{
            return self.normalMainTone;
        }
        case OddityThemeModeNight:{
            return self.nightMainTone;
        }
    }
}


-(UIColor *)getNavTitleColorWith:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            return self.normalTitleColor;
        }
        case OddityThemeModeNight:{
            return self.nightTitleColor;
        }
    }
}


-(UIColor *)getNavBackColorWith:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            return self.normalBackColor;
        }
        case OddityThemeModeNight:{
            return self.nightBackColor;
        }
    }
}


-(UIColor *)getNavBorderColorWith:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            return self.normalBorderColor;
        }
        case OddityThemeModeNight:{
            return self.nightBorderColor;
        }
    }
}

-(UIStatusBarStyle )getStatusBarStyleWith:(OddityThemeMode)mode{
    
    switch (mode) {
        case OddityThemeModeNormal:{
            return self.normalStatusStyle;
        }
        case OddityThemeModeNight:{
            return self.nightStatusStyle;
        }
    }
}



@end
