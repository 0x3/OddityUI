//
//  LBClearCacheTool.m
//  clearTest
//
//  Created by li  bo on 16/5/29.
//  Copyright © 2016年 li  bo. All rights reserved.
//

#import "OddityClearCacheTool.h"

#define fileManager [NSFileManager defaultManager]

@implementation OddityClearCacheTool


//获取path路径下文件夹大小
+ (NSString *)getCacheSizeWithFilePath:(NSString *)path{
    
    NSArray *subpathArray= [fileManager subpathsAtPath:path];
    
    NSString *filePath = nil;
    NSInteger totleSize=0;
    
    for (NSString *subpath in subpathArray) {
        
        filePath =[path stringByAppendingPathComponent:subpath];
        
        BOOL isDirectory = NO;
        
        BOOL isExist = [fileManager fileExistsAtPath:filePath isDirectory:&isDirectory];
        
        if (!isExist || isDirectory || [filePath containsString:@".DS"]) continue;
        NSDictionary *dict=   [fileManager attributesOfItemAtPath:filePath error:nil];
        
        NSInteger size=[dict[@"NSFileSize"] integerValue];
        totleSize+=size;
    }
    
    NSString *totleStr = nil;
    
    if (totleSize > 1000 * 1000) {
        
        totleStr = [NSString stringWithFormat:@"%.1fM",totleSize / 1000.0f /1000.0f];
    }else if (totleSize > 1000){
        totleStr = [NSString stringWithFormat:@"%.1fKB",totleSize / 1000.0f ];
        
    }else{
        totleStr = [NSString stringWithFormat:@"%.1fB",totleSize / 1.0f];
    }
    
    return totleStr;
    
    
}


//清除path文件夹下缓存大小
+ (BOOL)clearCacheWithFilePath:(NSString *)path{
    
    NSArray *subpathArray= [fileManager subpathsAtPath:path];
    
    NSError *error = nil;
    NSString *filePath = nil;
    
    for (NSString *subpath in subpathArray) {
        
        filePath =[path stringByAppendingPathComponent:subpath];
        
        [fileManager removeItemAtPath:filePath error:&error];
    }
    
    return YES;
    
}
@end
