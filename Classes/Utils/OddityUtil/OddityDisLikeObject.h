//
//  OddityDisLikeObject.h
//  Pods
//
//  Created by 荆文征 on 2017/7/19.
//
//

#import "OddityMainManagerViewController.h"
#import <Foundation/Foundation.h>

@interface OddityDisLikeObject : NSObject<OddityNewArrayListViewControllerDelegate>

+(instancetype)shareDisLikeObject;

@end
