//
//  OddityDisLikeObject.m
//  Pods
//
//  Created by 荆文征 on 2017/7/19.
//
//
#import "OddityShareUser.h"
#import "OddityAFAppDotNetAPIClient.h"

#import "OddityDisLikeObject.h"

#import "OddityLogObject.h"

#import "OddityNewBaseTableViewCell.h"
#import "OddityReasonViewController.h"
#import "UIViewController+Oddity.h"
#import "OddityMainManagerViewController.h"

@interface OddityDisLikeObject()<OddityReasonViewControllerDelegate>

@end

@implementation OddityDisLikeObject

+(instancetype)shareDisLikeObject{
    
    static OddityDisLikeObject* odditySetting = nil;
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        odditySetting = [[self alloc] init] ;
    }) ;
    
    return odditySetting ;
}

-(void)didClickDisLikeButton:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath cell:(UITableViewCell *)cell channels:(OddityChannelNews *)channel{

    if (indexPath && [cell isKindOfClass:OddityNewBaseTableViewCell.class]) {
        
        OddityNewBaseTableViewCell *ncell = (OddityNewBaseTableViewCell *)cell;
        
        NSArray *arr = @[@"不喜欢这条新闻",@"旧闻",@"内容低质",[NSString stringWithFormat:@"来源:%@",ncell.object.pname]];
        
        OddityReasonViewController *viewController = [[[OddityReasonViewController alloc]initWithReasonsArray:arr] fillMethod:(OddityNewBaseTableViewCell *)cell tableView:tableView indexPath:indexPath];
        
        viewController.nObj = ncell.object;
        viewController.channel = channel;
        viewController.delegate = self;
        
        [[UIViewController oddityCurrentViewController] presentViewController:viewController animated:true completion:nil];
    }
}

-(void)reasonViewControllerDidClickDisLikeButton:(UITableViewCell *)cell channels:(OddityChannelNews *)channel newObject:(OddityNew *)nObj{

    if ([cell isKindOfClass:[OddityNewBaseTableViewCell class]]) {
        
        OddityNew *nObject = ((OddityNewBaseTableViewCell *)cell).object;
        
        [channel toDeletNew:nObj];
        
        [self requestNewDataSource:nObject.nid reason:1];
    }
}

-(void)requestNewDataSource:(int)nid reason:(int)reason{
    
    
    OddityAFAppDotNetAPIClient *manager = [OddityAFAppDotNetAPIClient sharedClient];
    
    manager.requestSerializer = [AFJSONRequestSerializer new];
    
    NSInteger uid = OddityShareUser.defaultUser.uid;
    
    [manager.requestSerializer setValue:OddityShareUser.defaultUser.token forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"*" forHTTPHeaderField:@"X-Requested-With"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setValuesForKeysWithDictionary:@{
                                           @"nid":@(nid),
                                           @"uid":@(uid),
                                           @"reason": @(reason)
                                           }];
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
    [[manager POST:@"/v2/ns/hate" parameters: dict success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
#pragma clang diagnostic pop
        
        [OddityLogObject createAppAction:OddityLogAtypeBlockNews fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageFeedPage effective:true params:@{ @"nid": @(nid) }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [OddityLogObject createAppAction:OddityLogAtypeBlockNews fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageFeedPage effective:false params:@{ @"nid": @(nid) }];
    }] resume];
    
}


@end
