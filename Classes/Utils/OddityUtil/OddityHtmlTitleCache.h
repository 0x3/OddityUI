//
//  OddityHtmlTitleCache.h
//  Pods
//
//  Created by 荆文征 on 2017/4/27.
//
//

#import <Foundation/Foundation.h>

@interface OddityHtmlTitleCache : NSObject

+ (OddityHtmlTitleCache *)sharedCache;

-(NSAttributedString *)htmlTitleByString:(NSString *)title;

@end
