//
//  OddityHtmlTitleCache.m
//  Pods
//
//  Created by 荆文征 on 2017/4/27.
//
//

#import "OddityCategorys.h"
#import "OddityHtmlTitleCache.h"

@interface OddityHtmlTitleCache()

@property(nonatomic,strong)  NSCache *sharedCache;
@property(nonatomic,strong)  NSMutableDictionary *sharedDictionary;

@end

@implementation OddityHtmlTitleCache



+(OddityHtmlTitleCache *)sharedCache{

    static OddityHtmlTitleCache *sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[OddityHtmlTitleCache alloc]init];
        
        sharedManager.sharedCache = [[NSCache alloc] init];;
//        sharedManager.sharedDictionary = [NSMutableDictionary dictionary];
    });
    return sharedManager;
}


-(NSAttributedString *)htmlTitleByString:(NSString *)title{

    NSMutableAttributedString *attributedString;
    
    // 在cache 提取
    id viewController = [self.sharedCache objectForKey:title];
    if ( viewController) {
        attributedString = viewController;
    }else{
        attributedString = [[NSMutableAttributedString alloc] initWithData:[title dataUsingEncoding:NSUnicodeStringEncoding]  options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        if (attributedString) {
            
            [self.sharedCache setObject:attributedString forKey:title];
        }
    }
    [attributedString addAttributes:@{
                                      NSFontAttributeName:[UIFont font_t_new_title],
                                      } range:[attributedString.string fullRange]];
    
    return attributedString;
}

@end
