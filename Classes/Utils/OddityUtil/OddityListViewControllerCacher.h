//
//  OddityListViewControllerCacher.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>


@class OddityNewArrayListViewController,OddityChannel,OddityMainManagerViewController;

/**
 *  新闻列表的ViewController 缓存器
 *
 *  使用 `NSCache` 完成本缓存类的创建，使用 ManagerView的 Hash String 以及 频道的title 缓存缓存条件
 *  通过唯一表示缓存获取ViewController
 */
@interface OddityListViewControllerCacher : NSObject

/**
 *  通过 dispatch_one 单例模式，进行 NSCache 的获取
 *
 *  获取一个 `OddityListViewControllerCacher` 的单例对象
 *  @return `OddityListViewControllerCacher` 对象
 */
+ (OddityListViewControllerCacher*)sharedCache;



-(UIViewController *)getNewViewControllerBy:(OddityChannel *) channel viewController:(OddityMainManagerViewController *)vc;

-(OddityNewArrayListViewController *)getSubViewControllerBy:(OddityChannel *) channel subChannel:(OddityChannel *) subChannel viewController:(OddityMainManagerViewController *)vc;

@end
