//
//  OddityListViewControllerCacher.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"
#import "OddityNewArrayListViewController.h"
#import "OddityListViewControllerCacher.h"

#import "OdditySubNewArrayViewController.h"

@interface OddityListViewControllerCacher()

@property(nonatomic,strong)  NSCache *sharedCache;
@property(nonatomic,strong)  NSMutableDictionary *sharedDictionary;

@end

@implementation OddityListViewControllerCacher

+ (OddityListViewControllerCacher*)sharedCache {
    
    static OddityListViewControllerCacher *sharedManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[OddityListViewControllerCacher alloc]init];
        
        sharedManager.sharedCache = [[NSCache alloc] init];;
        sharedManager.sharedDictionary = [NSMutableDictionary dictionary];
    });
    return sharedManager;
}

-(OddityNewArrayListViewController *)getSubViewControllerBy:(OddityChannel *) channel subChannel:(OddityChannel *) subChannel viewController:(OddityMainManagerViewController *)vc{
    
    NSString *cacheKey = [NSString stringWithFormat:@"%d-%@",subChannel.id ?: channel.id,vc.uniqueIdentity];
    
    // 在cache 提取
    id viewController = [self.sharedCache objectForKey:cacheKey];
    if ( viewController) {
        
        return (OddityNewArrayListViewController *)viewController;
    }
    
    // 在字典中 提取
    viewController = [self.sharedDictionary objectForKey:cacheKey];
    if ( viewController) {
        
        [self.sharedCache setObject:viewController forKey:cacheKey];
        return (OddityNewArrayListViewController *)viewController;
    }
    
    OddityNewArrayListViewController *viewControllers = [[OddityNewArrayListViewController alloc] initWithChannel: [OddityChannelNews getObjectBy:subChannel ?: channel]];
//    viewControllers.subChannel = subChannel;
    /// 缓存
    [self.sharedCache setObject:viewControllers forKey:cacheKey];
    [self.sharedDictionary setObject:viewControllers forKey:cacheKey];
    
    viewControllers.delegate = vc;
    viewControllers.managerViewController = vc;
    
    return viewControllers;
}


-(UIViewController *)getNewViewControllerBy:(OddityChannel *) channel viewController:(OddityMainManagerViewController *)vc{
    
    NSString *cacheKey = [NSString stringWithFormat:@"%d-%@",channel.id,vc.uniqueIdentity];
    
    if (channel.has_children == 1) {
        
        cacheKey = [NSString stringWithFormat:@"%@-%@",cacheKey,@"subMain"];
    }
    
    // 在cache 提取
    id viewController = [self.sharedCache objectForKey:cacheKey];
    if ( viewController) {
        
        return (OddityNewArrayListViewController *)viewController;
    }
    
    // 在字典中 提取
    viewController = [self.sharedDictionary objectForKey:cacheKey];
    if ( viewController) {
        
        [self.sharedCache setObject:viewController forKey:cacheKey];
        return (OddityNewArrayListViewController *)viewController;
    }
    
//    UIViewController *viewController;
    
    if (channel.has_children == 1) {
        
        OdditySubNewArrayViewController *subArrayViewController = [[OdditySubNewArrayViewController alloc] initWith:[OddityChannelNews getObjectBy:channel] managerViewController:vc];
        
        viewController = subArrayViewController;
        
    }else{
        
        OddityNewArrayListViewController *newArrayListViewController = [[OddityNewArrayListViewController alloc] initWithChannel: [OddityChannelNews getObjectBy:channel]];
       
        newArrayListViewController.managerViewController = vc;
        
        viewController = newArrayListViewController;
    }
    
    
//    OdditySubNewArrayViewController.h
    
    /// 缓存
    [self.sharedCache setObject:viewController forKey:cacheKey];
    [self.sharedDictionary setObject:viewController forKey:cacheKey];
    
    return viewController;
}


@end


