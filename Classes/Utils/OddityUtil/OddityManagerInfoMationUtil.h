//
//  OddityManagerInfoMationUtil.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

/**
 *  管理对象
 *  该对象主要是为了广告，而获取用户的关于手机运营公司，或者手机的网络环，IP 以及地理位置等信息的获取。
 *  该对象需要app 像，用户询问要到 地理位置的 请求，否则不会上传地理位置的 经度 以及 纬度
 */
@interface OddityManagerInfoMationUtil : NSObject

/// 用户唯一表示 广告唯一表示
@property (strong, nonatomic,readonly) NSString *idfa;
/// 用户唯一表示
@property (strong, nonatomic,readonly) NSString *idfv;
/// 用户机型
@property (strong, nonatomic,readonly) NSString *phoneModel;
/// 手机ip
@property (strong, nonatomic,readonly) NSString *IPAdress;
/// 设备分辨率
@property (strong, nonatomic,readonly) NSString *device_size;

/// 应用版本
@property (strong, nonatomic,readonly) NSString *app_version;
/// 框架版本
@property (strong, nonatomic,readonly) NSString *framework_version;





/// 网络环境 0:未知 1:wifi 2:2G 3:3G 4:4G
@property (strong, nonatomic, readonly) NSString *networkCode;

/// 用户所在的城市
@property (strong, nonatomic, readonly) NSString *city;
/// 用户所在的街道
@property (strong, nonatomic, readonly) NSString *thoroughfare;
/// 用户所在的省
@property (strong, nonatomic, readonly) NSString *province;


/// 用户所在的经度
@property (strong, nonatomic, readonly) NSString *latitude;
/// 用户所在的纬度
@property (strong, nonatomic, readonly) NSString *longitude;

/**
 *   获取到一个单例模式下的 管理对象
 *  该对象是为了收集用户信息的一个对象 这个注释写的我是真的无聊啊～～～
 *
 *  @return 管理对象
 */
+(instancetype)managerInfoMation;

/**
 *   管理对象的 对象方法
 *   开始修改信息 并且完成数据的写入
 */
-(void)StartModfiyeInfoMation;

/**
 开发模式的 信息泄漏

 @return 泄漏
 */
-(NSArray *)getDevelopeDictParams;

/**
 获得广告来源的数据 所需要的参数集合

 @return 参数集合
 */
-(NSDictionary *)getAdSourceInfo;

/**
 *  获取上传日志的时候 日志根数据 字典对象
 *
 *  @return 字典对象
 */
-(NSDictionary *)getBasicInfoString;

/**
 *   获取广告请求的Base64数据字符串
 *
 *  @param aid 新闻的广告位的ID
 *
 *  @return base64 请求字符串
 */
-(NSString *)getAdsInfo:(NSString *) aid;

@end

