//
//  OddityManagerInfoMationUtil.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityLogObject.h"

#import "OddityModels.h"

#import "OddityPlayerView.h"

#import "OddityCategorys.h"

/// IP相关
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <sys/utsname.h>

/// 广告相关
#import <AdSupport/ASIdentifierManager.h>

/// 网络相关
#import <AFNetworking/AFNetworking.h>

/// 电话相关
#import<CoreTelephony/CTTelephonyNetworkInfo.h>
#import<CoreTelephony/CTCarrier.h>

#import <CoreLocation/CoreLocation.h>

#import "OddityManagerInfoMationUtil.h"


static OddityManagerInfoMationUtil* managerInfoMation = nil;

@interface OddityManagerInfoMationUtil ()<CLLocationManagerDelegate>

@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,strong) AFNetworkReachabilityManager *reachabilityManager;

//readwrite

@end

@implementation OddityManagerInfoMationUtil

@synthesize idfa = _idfa;
@synthesize phoneModel = _phoneModel;
@synthesize IPAdress = _IPAdress;
@synthesize device_size = _device_size;
@synthesize framework_version = _framework_version;

/**
 *  返回设备的 idfa
 *
 *  @return idfa字符串
 */
-(NSString *)idfa{
    
    return [[[ASIdentifierManager sharedManager]advertisingIdentifier] UUIDString];
}

-(NSString *)idfv{

    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}


/**
 *  返回 手机 型号
 *
 *  @return 手机型号 string
 */
-(NSString *)phoneModel{
    
    if (!_phoneModel) {
        
        _phoneModel = [self iphoneType];
    }
    
    return _phoneModel;
}

/**
 *  手机 ip 地址
 *
 *  @return IP地址字符串
 */
-(NSString *)IPAdress{
    
    if (!_IPAdress) {
        
        _IPAdress = [self deviceIPAdress];
    }
    
    return _IPAdress;
}

/**
 *  手机分辨率
 *
 *  @return 分辨率字符串
 */
-(NSString *)device_size{
    
    if (!_device_size) {
        
        _device_size = [NSString stringWithFormat:@"%@*%@",@((int)oddityUIScreenWidth*(int)[UIScreen mainScreen].scale),@((int)oddityUIScreenHeight*(int)[UIScreen mainScreen].scale)];
    }
    
    return self->_device_size;
}

-(NSString *)framework_version{

    if (!_framework_version) {
        
        NSDictionary *infoDictionary = [NSBundle.oddity_shareBundle infoDictionary];
        _framework_version = [infoDictionary valueForKey:@"CFBundleShortVersionString"];
    }
    
    return self->_framework_version;
}

+(instancetype)managerInfoMation{
    
    static dispatch_once_t onceToken ;
    dispatch_once(&onceToken, ^{
        managerInfoMation = [[self alloc] init] ;
    }) ;
    
    return managerInfoMation ;
}

// ctype	Int	是	渠道类型, 1：奇点资讯， 2：黄历天气，3：纹字锁频，4：猎鹰浏览器，5：白牌
-(NSDictionary *)getAdSourceInfo{

    
    
    NSDictionary *param = @{
                            @"uid": @(OddityShareUser.defaultUser.uid),
                            @"did": self.idfa,
                            @"ctype" : @(OddityShareUser.defaultUser.appCtype),
                            @"ptype" : @1,
                            @"aversion": self.framework_version,
                            @"ctime": @([[NSDate date] oddity_unixInteger])
                            };
    
    return param;
}


-(NSDictionary *)getBasicInfoString{
    
    NSDictionary *param = @{
                            @"uid": @(OddityShareUser.defaultUser.uid),
                            @"ctype" : OddityShareUser.defaultUser.appSourceString,
                            @"ptype" : @"IOS",
                            @"deviceid": self.idfa,
                            @"version_text": self.framework_version,
                            @"ctime": @([[NSDate date] oddity_unixInteger])
                            };
    
    return param;
}


-(NSString *)getAdsInfo:(NSString *) aid{
    
    
    
    NSString* phoneModel   = self.phoneModel;//方法在下面
    NSString* phoneVersion = [[UIDevice currentDevice] systemVersion];
    
    NSString *device_size  = self.device_size;
    NSString* IPAdress     = self.IPAdress;
    
    NSString *idfaori      = self.idfa;
    NSString *idfa         = [idfaori oddity_MD5];
    
    NSString *operator     = [self checkCarrier];
    
    NSMutableDictionary *device = [NSMutableDictionary dictionary];
    
    
    [device setValuesForKeysWithDictionary:@{
                                             @"density":@([self getDpi]),
                                             @"idfa":idfa,
                                             @"idfaori":idfaori,
                                             @"brand":[[UIDevice currentDevice] model],
                                             @"platform": phoneModel,
                                             @"os":@"2",
                                             @"os_version":phoneVersion,
                                             @"device_size":device_size,
                                             @"operator":operator,
                                             @"screen_orientation":@"1"
                                             }];
    
    NSString *imsi     = [self getIMSI];
    if (imsi) {
        
        [device setValue:imsi forKey:@"imsi"];
    }
    
    UIWebView* webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString* ua = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    
//    NSString *ua = [AFHTTPSessionManager.manager.requestSerializer valueForHTTPHeaderField:@"User-Agent"];
    if (ua) {
        
        [device setValue:ua forKey:@"ua"];
    }
    
    if(IPAdress){
        [device setValue:IPAdress forKey:@"ip"];
    }
    
    if(_networkCode){
        
        [device setValue:_networkCode forKey:@"network"];
    }
    
    if(_longitude && _latitude){
        
        [device setValue:_latitude forKey:@"latitude"];
        [device setValue:_longitude forKey:@"longitude"];
    }
    
    NSString *screenHeight = [NSString stringWithFormat:@"%@",@(oddityUIScreenHeight)];
    NSString *screenWidth  = [NSString stringWithFormat:@"%@",@(oddityUIScreenWidth)];
    NSString *timeSValue = [NSString stringWithFormat:@"%@",@((int)([NSDate date].timeIntervalSince1970))];
    
    NSArray *array = [NSArray arrayWithObject:@{
                                                @"aid" : aid,
                                                @"height":screenHeight,
                                                @"width":screenWidth,
                                                }];
    
    NSDictionary *dict = @{
                           @"version" : @"1.0",
                           @"ts":timeSValue,
                           @"impression": array,
                           @"device":device
                           };
    
    NSString *base64String = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    if (jsonData && !error) {
        base64String = [jsonData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    }
    
    return base64String;
}

-(float)getDpi{

    float scale = 1;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        scale = [[UIScreen mainScreen] scale];
    }
    
    return scale;
}


-(void)StartModfiyeInfoMation{
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [_locationManager requestWhenInUseAuthorization];
    }
    
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    [_locationManager startUpdatingLocation];
    
    _reachabilityManager = [AFNetworkReachabilityManager sharedManager];
    
    typeof(self) __weak weakSelf = self;
    [_reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:oddityNetworkStateChanged object:nil ];
        
        switch (status) {
                // 没有网络
            case AFNetworkReachabilityStatusUnknown:{
                
                _networkCode = @"0";
            }
                
                break;
                //不可达的网络(未连接)
            case AFNetworkReachabilityStatusNotReachable:{
                
                _networkCode = @"0";
            }
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:{
                
                CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
                
                NSString *currentStatus = info.currentRadioAccessTechnology;
                
                if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyGPRS"]) {
                    //                    netconnType = @"GPRS";
                    _networkCode = @"2";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyEdge"]) {
                    //                    netconnType = @"2.75G EDGE";
                    _networkCode = @"2";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyWCDMA"]){
                    //                    netconnType = @"3G";
                    _networkCode = @"3";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyHSDPA"]){
                    //                    netconnType = @"3.5G HSDPA";
                    _networkCode = @"3";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyHSUPA"]){
                    //                    netconnType = @"3.5G HSUPA";
                    _networkCode = @"3";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMA1x"]){
                    //                    netconnType = @"2G";
                    _networkCode = @"2";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORev0"]){
                    //                    netconnType = @"3G";
                    _networkCode = @"3";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevA"]){
                    //                    netconnType = @"3G";
                    _networkCode = @"3";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyCDMAEVDORevB"]){
                    //                    netconnType = @"3G";
                    _networkCode = @"3";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyeHRPD"]){
                    //                    netconnType = @"HRPD";
                    _networkCode = @"3";
                }else if ([currentStatus isEqualToString:@"CTRadioAccessTechnologyLTE"]){
                    //                    netconnType = @"4G";
                    _networkCode = @"4";
                }
            }
                
                break;
                
                //                NSLog(@"wifi的网络");
            case AFNetworkReachabilityStatusReachableViaWiFi:{
                _networkCode = @"1";
            }
                break;
            default:
                _networkCode = @"0";
                break;
        }
        
        if ([weakSelf.networkCode isEqual:@"0"]) {
         
            [weakSelf showNoNetWork];
        }
    }];
    
    [_reachabilityManager startMonitoring];
}

-(void)showNoNetWork{

    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = UIColor.color_t_13;
    
    UILabel *messageLabel = [[UILabel alloc] init];
    messageLabel.text = @"当前网络不可用，请检查网络设置";
    messageLabel.font = UIFont.font_t_6;
    messageLabel.textColor = UIColor.color_t_10;
    
    UIView *view = UIApplication.sharedApplication.keyWindow;
    [view addSubview:backView];
    [backView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(view);
    }];
    
    [backView addSubview:messageLabel];
    [messageLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(8);
        make.bottom.mas_equalTo(-8);
        make.left.mas_equalTo(16);
        make.right.mas_equalTo(-16);
    }];
    
    [view layoutIfNeeded];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [backView removeFromSuperview];
    });
}


-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    [manager stopUpdatingLocation];
    _latitude = [NSString stringWithFormat:@"%@", @(newLocation.coordinate.latitude)];
    _longitude = [NSString stringWithFormat:@"%@", @(newLocation.coordinate.longitude)];
    
    // 获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *array, NSError *error){
        if (array.count > 0){
            CLPlacemark *placemark = [array objectAtIndex:0];
            
            _city = placemark.locality;
            _thoroughfare = placemark.thoroughfare;
            _province = placemark.administrativeArea;
            
            static dispatch_once_t onceToken;
            dispatch_once(&onceToken, ^{
                
                [OddityNew firstuploadClickLogRequest];
            });
        }
    }];
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    [OddityNew firstuploadClickLogRequest];
}

- (NSString *)getIMSI{
    
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    
    CTCarrier *carrier = [info subscriberCellularProvider];
    
    NSString *mcc = [carrier mobileCountryCode];
    NSString *mnc = [carrier mobileNetworkCode];
    
    NSString *imsi;
    
    if (mcc && mnc) {
        
        imsi = [NSString stringWithFormat:@"%@%@", mcc, mnc];
    }
    
    
    return imsi;
}

- (NSString *)checkCarrier{
    
    NSString * operatorCode = @"0";
    
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    
    CTCarrier *carrier = [info subscriberCellularProvider];
    
    NSString *code = [carrier mobileNetworkCode];
    NSString *carrierName = [carrier carrierName];
    
    if ( code && carrierName)  {
        
        /// 中国移动
        if ( [code isEqualToString:@"00"] || [code isEqualToString:@"02"] || [code isEqualToString:@"07"] || [carrierName isEqualToString:@"中国移动"]){
            operatorCode = @"1";
        }
        
        /// 中国联通
        if ( [code isEqualToString:@"01"] || [code isEqualToString:@"06"] || [carrierName isEqualToString:@"中国联通"] ){
            operatorCode = @"2";
        }
        
        /// 中国电信
        if ( [code isEqualToString:@"03"] || [code isEqualToString:@"05"] || [carrierName isEqualToString:@"中国电信"] ){
            operatorCode = @"3";
        }
    }
    
    return operatorCode;
}


- (NSString *)deviceIPAdress {
    NSString *address;
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    success = getifaddrs(&interfaces);
    
    if (success == 0) { // 0 表示获取成功
        
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    freeifaddrs(interfaces);
    return address;
}

- (NSString *)iphoneType {
    
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    
    if ([platform isEqualToString:@"iPhone9,1"]) return @"iPhone 7";
    
    if ([platform isEqualToString:@"iPhone9,2"]) return @"iPhone 7 Plus";
    
    if ([platform isEqualToString:@"iPod1,1"])   return @"iPod Touch 1G";
    
    if ([platform isEqualToString:@"iPod2,1"])   return @"iPod Touch 2G";
    
    if ([platform isEqualToString:@"iPod3,1"])   return @"iPod Touch 3G";
    
    if ([platform isEqualToString:@"iPod4,1"])   return @"iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPod5,1"])   return @"iPod Touch 5G";
    
    if ([platform isEqualToString:@"iPad1,1"])   return @"iPad 1G";
    
    if ([platform isEqualToString:@"iPad2,1"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,2"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,3"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,4"])   return @"iPad 2";
    
    if ([platform isEqualToString:@"iPad2,5"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,6"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad2,7"])   return @"iPad Mini 1G";
    
    if ([platform isEqualToString:@"iPad3,1"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,2"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,3"])   return @"iPad 3";
    
    if ([platform isEqualToString:@"iPad3,4"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,5"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad3,6"])   return @"iPad 4";
    
    if ([platform isEqualToString:@"iPad4,1"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,2"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,3"])   return @"iPad Air";
    
    if ([platform isEqualToString:@"iPad4,4"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,5"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"iPad4,6"])   return @"iPad Mini 2G";
    
    if ([platform isEqualToString:@"i386"])      return @"iPhone Simulator";
    
    if ([platform isEqualToString:@"x86_64"])    return @"iPhone Simulator";
    
    return platform;
    
}



-(NSArray *)getDevelopeDictParams{
    
    
    
    
    
    
    
    
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [info subscriberCellularProvider];
    NSString *code = [carrier mobileNetworkCode];
    NSString *carrierName = [carrier carrierName];
    
    NSString *uidStr = [NSString stringWithFormat:@"%ld",(long)OddityShareUser.defaultUser.uid];
    
    NSMutableArray *array = [NSMutableArray array];
    
    [array addObject:@"用户信息"];
    
    
    if (uidStr) {
        
        [array addObject:@{
                           @"当前用户ID": uidStr
                           }];
    }
    
    
    if (OddityShareUser.defaultUser.password) {
    
        [array addObject:@{
                           @"当前用户密码": OddityShareUser.defaultUser.password,
                           }];
    }
    
    
    if (OddityShareUser.defaultUser.token) {
    
        [array addObject:@{
                           @"当前用户token": OddityShareUser.defaultUser.token,
                           }];
    }
    
    
    if(self.latitude){
    
        [array addObject:@{
                           @"所在经度": self.latitude,
                           }];
    }
    
    if (self.longitude) {
        
        [array addObject:@{
                           @"所在维度": self.longitude,
                           }];
    }
    
    if (self.city) {
        
        [array addObject:@{
                           @"所在城市": self.city,
                           }];
    }
    
    if (self.province) {
        
        [array addObject:@{
                           @"所在省份": self.province,
                           }];
    }
    
    
    if (self.thoroughfare) {
        
        [array addObject:@{
                           @"所在街道": self.thoroughfare
                           }];
    }
    
    
    [array addObject:@"用户设备"];
    
    if ( self.idfv) {
        
        [array addObject:@{
                           @"当前用户设备 - 新": self.idfv,
                           }];
    }
    
    
    if (self.idfa) {
        
        [array addObject:@{
                           @"当前用户设备 - 广告": self.idfa,
                           }];
    }
    
    if (self.framework_version) {
        
        
        [array addObject:@{
                           @"sdk版本型号": self.framework_version,
                           }];
    }
    
    [array addObject:@{
                       @"当前 unix 时间 - 毫秒": [NSDate date].oddity_unixString,
                       }];
    
    [array addObject:@{
                       @"手机名称": [[UIDevice currentDevice] name],
                       }];
    
    [array addObject:@{
                       @"手机型号 - 原始": [[UIDevice currentDevice] model],
                       }];
    
    [array addObject:@{
                       @"手机本地型号 - 原始": [[UIDevice currentDevice] localizedModel],
                       }];
    
    [array addObject:@{
                       @"系统型号": [[UIDevice currentDevice] systemName],
                       }];
    
    [array addObject:@{
                       @"手机名称": [[UIDevice currentDevice] name],
                       }];
    
    [array addObject:@{
                       @"系统版本": [[UIDevice currentDevice] systemVersion],
                       }];
    
    if (self.iphoneType) {
        
        [array addObject:@{
                           @"手机型号": self.iphoneType,
                           }];
    }
    
    if(self.deviceIPAdress){
    
        
        [array addObject:@{
                           @"网络IP地址" : self.deviceIPAdress,
                           }];
    }
    
    if (code) {
        
        [array addObject:@{
                           @"运营商代码" : code,
                           }];
    }
    
    if (carrierName) {
        
        [array addObject:@{
                           @"运营商名称" : carrierName,
                           }];
    }
    
    
    
    [array addObject:@"广告请求方式"];
    
    
    [array addObject:@{
                       @"当前获取广告的方式": self.userAdStr
                       }];
    
    
    [array addObject:@{
                       @"feed流获取广告的位置": [NSString stringWithFormat:@"第%@个",@(OddityShareUser.defaultUser.feedAdPos+1)]
                       }];
    
    [array addObject:@{
                       @"相关新闻获取广告的位置": [NSString stringWithFormat:@"第%@个",@(OddityShareUser.defaultUser.relatedAdPos+1)]
                       }];
    
    
    [array addObject:@"日志相关"];
    
    [array addObject:@{
                       @"共产生日志": [NSString stringWithFormat:@"%@个",@([OddityLogObject allObjectsInRealm:[RLMRealm OddityRealm]].count)]
                       }];
    [array addObject:@{
                       @"已上传日志": [NSString stringWithFormat:@"%@个",@([[OddityLogObject allObjectsInRealm:[RLMRealm OddityRealm]] objectsWhere:@"isUploaded = true"].count)]
                       }];
    [array addObject:@{
                       @"未上传日志": [NSString stringWithFormat:@"%@个",@([[OddityLogObject allObjectsInRealm:[RLMRealm OddityRealm]] objectsWhere:@"isUploaded = false"].count)]
                       }];
    
    return  array;
}


-(NSString *)userAdStr{

    switch (OddityShareUser.defaultUser.getAdsMode) {
        case OddityUserRequestAdsModeGDTApi:
            return @"广点通API";
        case OddityUserRequestAdsModeGDTSDK:
            return @"广点通SDK";
        case OddityUserRequestAdsModeYiFuSdk:
            return @"易付SDK";
    }
    
    return @"未知SDK";
}


@end

