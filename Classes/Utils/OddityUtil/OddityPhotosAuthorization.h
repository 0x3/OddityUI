//
//  OddityPhotosAuthorization.h
//  Pods
//
//  Created by 荆文征 on 2017/7/13.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, OddityPhotoAuthStatus)
{
    OddityPhotoAuthStatusSuccess          = 0,
    OddityPhotoAuthStatusAccessDenied     = 1,
    OddityPhotoAuthStatusNoAssets         = 2,
    OddityPhotoAuthStatusUnknown          = 3,
};

@interface OddityPhotosAuthorization : NSObject


- (void)checkAuthorizationStatus:(void(^)(OddityPhotoAuthStatus status))completion;

@end
