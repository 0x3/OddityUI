//
//  OddityPhotosAuthorization.m
//  Pods
//
//  Created by 荆文征 on 2017/7/13.
//
//

#import "OddityCategorys.h"

#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "OddityPhotosAuthorization.h"

@interface OddityPhotosAuthorization ()

@property (nonatomic, copy) void(^checkAuthorizationCompletion)(OddityPhotoAuthStatus status);

@end

@implementation OddityPhotosAuthorization

- (void)checkAuthorizationStatus:(void(^)(OddityPhotoAuthStatus status))completion{
    
    self.checkAuthorizationCompletion = completion;
    if (ODDITY_SYSTEM_GREAT_OR_EQUAL(8)) {
        
        [self checkAuthorizationStatus_AfteriOS8];
    }else{
        
        [self checkAuthorizationStatus_BeforeiOS8];
    }
}


- (void)requestAuthorization{
    
    if (ODDITY_SYSTEM_GREAT_OR_EQUAL(8)) {
    
        [self requestAuthorizationStatus_AfteriOS8];
    }else{
        
        [self requestAuthorizationStatus_BeforeiOS8];
    }
}

- (void)checkAuthorizationStatus_AfteriOS8
{
    PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
    switch (status)
    {
        case PHAuthorizationStatusNotDetermined:
            [self requestAuthorizationStatus_AfteriOS8];
            break;
        case PHAuthorizationStatusRestricted:
        case PHAuthorizationStatusDenied:
        {
            [self showAccessDenied];
            break;
        }
        case PHAuthorizationStatusAuthorized:
        default:
        {
            [self checkAuthorizationSuccess];
            break;
        }
    }
}

- (void)requestAuthorizationStatus_AfteriOS8
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status){
        dispatch_async(dispatch_get_main_queue(), ^{
            switch (status) {
                case PHAuthorizationStatusAuthorized:
                {
                    [self checkAuthorizationSuccess];
                    break;
                }
                default:
                {
                    [self showAccessDenied];
                    break;
                }
            }
        });
    }];
}

- (void)checkAuthorizationStatus_BeforeiOS8
{
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    
    ALAuthorizationStatus status = [ALAssetsLibrary authorizationStatus];
    switch (status)
    {
        case ALAuthorizationStatusNotDetermined:
            [self requestAuthorizationStatus_BeforeiOS8];
            break;
        case ALAuthorizationStatusRestricted:
        case ALAuthorizationStatusDenied:
        {
            [self showAccessDenied];
            break;
        }
        case ALAuthorizationStatusAuthorized:
        default:
        {
            [self checkAuthorizationSuccess];
            break;
        }
    }
#pragma clang diagnostic pop
}

- (void)requestAuthorizationStatus_BeforeiOS8{
    
    if (self.checkAuthorizationCompletion) {
        
        self.checkAuthorizationCompletion(OddityPhotoAuthStatusSuccess);
    }
}

- (void)checkAuthorizationSuccess{
    
    if (self.checkAuthorizationCompletion) {
        self.checkAuthorizationCompletion(OddityPhotoAuthStatusSuccess);
    }
}

- (void)showAccessDenied{
    
    if (self.checkAuthorizationCompletion) {
        self.checkAuthorizationCompletion(OddityPhotoAuthStatusAccessDenied);
    }
}

- (void)showNoAssets{
    
    if (self.checkAuthorizationCompletion) {
        self.checkAuthorizationCompletion(OddityPhotoAuthStatusNoAssets);
    }
}




@end
