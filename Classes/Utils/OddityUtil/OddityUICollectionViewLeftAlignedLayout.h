//
//  OddityUICollectionViewLeftAlignedLayout.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <UIKit/UIKit.h>

@interface OddityUICollectionViewLeftAlignedLayout : UICollectionViewFlowLayout

@end

/**
 *  Just a convenience protocol to keep things consistent.
 *  Someone could find it confusing for a delegate object to conform to UICollectionViewDelegateFlowLayout
 *  while using UICollectionViewLeftAlignedLayout.
 */
@protocol OddityUICollectionViewDelegateLeftAlignedLayout <UICollectionViewDelegateFlowLayout>

@end
