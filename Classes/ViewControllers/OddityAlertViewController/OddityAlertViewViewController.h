//
//  OddityAlertViewViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/6/19.
//
//

#import <UIKit/UIKit.h>

@interface OddityAlertViewViewController : UIViewController

@property(nonatomic,strong)UIView *alertView;

-(instancetype)initWitch:(NSString *)message;

-(OddityAlertViewViewController *)makeChange:(NSString *)message after:(double)seconds block:(dispatch_block_t)b;
@end
