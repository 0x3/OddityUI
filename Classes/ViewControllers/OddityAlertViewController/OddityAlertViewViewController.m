//
//  OddityAlertViewViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/19.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityAlertViewViewController.h"

@interface OddityAlertViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityAlertViewControllerDismissAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@implementation OddityAlertViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityAlertViewViewController *toViewController = (OddityAlertViewViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.6];
    }];
    
    toViewController.alertView.transform = CGAffineTransformTranslate(toViewController.alertView.transform, 0, 0);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.alertView.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end



@implementation OddityAlertViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityAlertViewViewController *fromViewController = (OddityAlertViewViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        fromViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
        fromViewController.alertView.transform = CGAffineTransformTranslate(fromViewController.alertView.transform, 0, 0);
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end



@interface OddityAlertViewViewController ()<UIViewControllerTransitioningDelegate>

@property(nonatomic,strong)NSString *message;

@property(nonatomic,strong)UILabel *messageLabel;
@property(nonatomic,strong)UIActivityIndicatorView *activityIndicatorView;


@property(nonatomic,strong)OddityAlertViewControllerPresentdAnimation *cPresentedViewControllerAnimatedTransitioning;
@property(nonatomic,strong)OddityAlertViewControllerDismissAnimation *cDismissViewControllerAnimatedTransitioning;
@end

@implementation OddityAlertViewViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _cPresentedViewControllerAnimatedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _cDismissViewControllerAnimatedTransitioning;
}

-(instancetype)initWitch:(NSString *)message{

    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        
        _message = message;
        
        _cDismissViewControllerAnimatedTransitioning  = [[OddityAlertViewControllerDismissAnimation alloc]init];
        _cPresentedViewControllerAnimatedTransitioning  = [[OddityAlertViewControllerPresentdAnimation alloc]init];
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
        
        self.view.backgroundColor = UIColor.clearColor;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _alertView = [[UIView alloc] init];
    _alertView.layer.cornerRadius = 8;
    _alertView.backgroundColor = UIColor.color_t_6;
    [self.view addSubview:_alertView];
    [_alertView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.center.mas_equalTo(self.view);
    }];
    
    UIActivityIndicatorViewStyle style = OddityUISetting.shareOddityUISetting.oddityThemeMode != OddityThemeModeNight ? UIActivityIndicatorViewStyleGray : UIActivityIndicatorViewStyleWhite;
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style];
    [_activityIndicatorView startAnimating];
    [self.alertView addSubview:_activityIndicatorView];
    [_activityIndicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.centerX.mas_equalTo(self.alertView);
        make.top.mas_equalTo(15);
    }];
    
    _messageLabel = [[UILabel alloc] init];
    _messageLabel.text = self.message;
    _messageLabel.font = UIFont.font_t_6;
    _messageLabel.numberOfLines = 0;
    _messageLabel.textColor = UIColor.color_t_2;
    _messageLabel.textAlignment = NSTextAlignmentCenter;
    [self.alertView addSubview:_messageLabel];
    [_messageLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_activityIndicatorView.mas_bottom).offset(30);
        make.bottom.mas_equalTo(-15);
        make.right.mas_equalTo(-12);
        make.left.mas_equalTo(12);
    }];
}

-(OddityAlertViewViewController *)makeChange:(NSString *)message after:(double)seconds block:(dispatch_block_t)b{
    
    _messageLabel.text = message;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.activityIndicatorView.hidden = true;
        [self.activityIndicatorView stopAnimating];
        [self.view layoutIfNeeded];
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [_alertView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.mas_equalTo(self.view);
        }];
        
        [_messageLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(16);
            make.bottom.mas_equalTo(-16);
            
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(seconds * NSEC_PER_SEC)), dispatch_get_main_queue(), b);
    }];
    
    return self;
}

@end
