
//
//  OddityDetailInputViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import "OddityShareUser.h"
#import "OddityUserLoginViewController.h"

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityRequestWaitView.h"
#import "OddityPlaceholderTextView.h"
#import "OddityDetailInputViewController.h"

#import "OddityLogObject.h"

@interface OddityDetailInputViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityDetailInputViewControllerDismissAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@implementation OddityDetailInputViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityDetailInputViewController *toViewController = (OddityDetailInputViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        [toViewController b];
        
        toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.6];
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end



@implementation OddityDetailInputViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    
    OddityDetailInputViewController *fromViewController = (OddityDetailInputViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        fromViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
        
        [fromViewController r];
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end


@interface OddityDetailInputViewController ()<UIViewControllerTransitioningDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>


@property(nonatomic,strong)OddityNewContent *nContent;

@property(nonatomic,strong)UITapGestureRecognizer *tap;

@property(nonatomic,strong)UIView *backView;

@property(nonatomic,strong)UIView *topView;

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UILabel *alertLabel;

@property(nonatomic,strong)UIButton *cancelButton;
@property(nonatomic,strong)UIButton *submitButton;

@property(nonatomic,strong)OddityPlaceholderTextView *inputTextView;

@property(nonatomic,strong)OddityDetailInputViewControllerPresentdAnimation *cPresentedViewControllerAnimatedTransitioning;
@property(nonatomic,strong)OddityDetailInputViewControllerDismissAnimation *cDismissViewControllerAnimatedTransitioning;

@property(nonnull,strong) OddityRequestWaitView *requestWaitView;

@end

@implementation OddityDetailInputViewController


-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}


-(instancetype)initWith:(OddityNewContent *)content{

    self = [super initWithNibName:nil bundle:nil];
    if(self) {
        
        self.nContent = content;
        
        _cDismissViewControllerAnimatedTransitioning  = [[OddityDetailInputViewControllerDismissAnimation alloc]init];
        _cPresentedViewControllerAnimatedTransitioning  = [[OddityDetailInputViewControllerPresentdAnimation alloc]init];
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _cPresentedViewControllerAnimatedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _cDismissViewControllerAnimatedTransitioning;
}


-(void)b{
    
    [self.inputTextView becomeFirstResponder];
}

-(void)r{
    self.backView.alpha = 0;
    [self.inputTextView resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self Layout];
}

-(void)Layout{
    
    _backView = [[UIView alloc] init];
    _backView.backgroundColor = UIColor.color_t_6;
    [self.view addSubview:_backView];
    [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.with.right.with.bottom.mas_equalTo(0);
    }];
    
    _topView = [[UIView alloc] init];
    [self.backView addSubview:_topView];
    [_topView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.with.top.with.right.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.textColor = UIColor.color_t_2;
    _titleLabel.font = UIFont.font_t_4;
    _titleLabel.text = @"写评论";
    [self.topView addSubview:_titleLabel];
    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.center.mas_equalTo(_topView);
    }];
    
    _cancelButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
    _cancelButton.titleLabel.font = UIFont.font_t_5;
    [_cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    [_cancelButton addTarget:self action:@selector(cancelViewController:) forControlEvents:(UIControlEventTouchUpInside)];
    [_cancelButton setTitleColor:UIColor.color_t_2 forState:(UIControlStateNormal)];
    [self.topView addSubview:_cancelButton];
    [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.mas_equalTo(_topView);
        make.left.mas_equalTo(12);
    }];
    
    _submitButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
    _submitButton.titleLabel.font = UIFont.font_t_5;
    [_submitButton setTitle:@"发布" forState:(UIControlStateNormal)];
    [_submitButton addTarget:self action:@selector(submitViewController) forControlEvents:(UIControlEventTouchUpInside)];
    [_submitButton setTitleColor:UIColor.color_t_2 forState:(UIControlStateNormal)];
    [_submitButton setTitleColor:UIColor.color_t_3 forState:(UIControlStateDisabled)];
    [self.topView addSubview:_submitButton];
    [_submitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_topView);
        make.right.mas_equalTo(-12);
    }];
    
    _inputTextView = [[OddityPlaceholderTextView alloc] init];
    _inputTextView.placeholder = @"说点什么吧";
    _inputTextView.layer.borderColor = UIColor.color_t_5.CGColor;
    _inputTextView.layer.borderWidth = 0.5;
    _inputTextView.backgroundColor = UIColor.color_t_9;
    _inputTextView.placeholderColor = UIColor.color_t_3;
    _inputTextView.textColor = UIColor.color_t_2;
    _inputTextView.keyboardAppearance = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNight ? UIKeyboardAppearanceDark: UIKeyboardAppearanceDefault;
    _inputTextView.font = UIFont.font_t_5;
    [self.backView addSubview:_inputTextView];
    [_inputTextView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_topView.mas_bottom);
        make.right.mas_equalTo(-12);
        make.left.mas_equalTo(12);
        make.height.mas_equalTo(80);
        make.bottom.mas_equalTo(-15);
    }];
    
    _alertLabel = [[UILabel alloc] init];
    _alertLabel.font = UIFont.font_t_7;
    _alertLabel.textColor = UIColor.color_t_7;
    _alertLabel.alpha = 0;
    [self.backView addSubview:_alertLabel];
    [_alertLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self.submitButton.mas_bottom).offset(-7);
        make.right.mas_equalTo(self.submitButton.mas_left).offset(-8);
    }];
    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelViewController:)];
    _tap.delegate = self;
    [self.view addGestureRecognizer:_tap];
    
    self.requestWaitView = [[OddityRequestWaitView alloc] init];
    self.requestWaitView.backgroundColor = self.backView.backgroundColor;
    self.requestWaitView.alpha = 0;
    [self.view addSubview:self.requestWaitView];
    [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.with.right.with.bottom.with.top.mas_equalTo(self.backView);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    self.inputTextView.delegate = self;
    
    self.inputTextView.text = self.nContent.commentPresentation;
    [self textViewDidChange:self.inputTextView];
    
    [self.view layoutIfNeeded];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    CGPoint point = [gestureRecognizer locationInView:self.view];
    
    if (CGRectContainsPoint(self.backView.frame, point)) {
        
        return false;
    }
    
    return true;
}

-(void)cancelViewController:(id)any{
    
    if ([any isKindOfClass:[UITapGestureRecognizer class]] && self.inputTextView.isFirstResponder) {
        
        [self.inputTextView resignFirstResponder];
        return;
    }
    
    if (_inputTextView.text.length <= 0 || ODDITY_SYSTEM_LESS_THAN(8)) {
        
        [self cancelMethod];
        
        return;
    }
    
    __weak __typeof__(self) weakSelf = self;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否保存" message:@"是否为该文章保存评论文稿" preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"保存并退出" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf savaAndCancelMethod];
        }];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"直接退出" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf cancelMethod];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [[UIViewController oddityCurrentViewController] presentViewController:alertController animated:true completion:nil];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否保存" message:@"是否为该文章保存评论文稿" delegate:nil cancelButtonTitle:@"直接退出" otherButtonTitles:@"保存并退出", nil];
        
        [alert oddity_showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex == 1) {
                
                [weakSelf savaAndCancelMethod];
            }
            
            if (buttonIndex == 0) {
                
                [weakSelf cancelMethod];
            }
            
        }];
    }
}

-(void)savaAndCancelMethod{
    
    [_nContent savaPresentation:self.inputTextView.text];
    
    [self.presentingViewController ?: self dismissViewControllerAnimated:true completion:nil];
}

-(void)cancelMethod{
    
    [_nContent savaPresentation:@""];
    
    [self.presentingViewController ?: self dismissViewControllerAnimated:true completion:nil];
}

-(void)keyboardWillShow:(NSNotification *)notification{
    
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [self.backView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(-CGRectGetHeight(frame));
    }];
    
    [self.view layoutIfNeeded];
}

-(void)keyboardWillHidden:(NSNotification *)notification{
    
    [self.backView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(0);
    }];
    
    [self.view layoutIfNeeded];
}

-(void)textViewDidChange:(UITextView *)textView{

    self.submitButton.enabled = textView.text.length > 0 && textView.text.length <= 1024;
    
    NSInteger surplusCount = 1024 - textView.text.length ;
    
    if (surplusCount > 10) {
        
        [UIView animateWithDuration:0.3 animations:^{
           
            _alertLabel.alpha = 0;
        }];
    }else{
        
        _alertLabel.text = [NSString stringWithFormat:@"还剩%ld",(long)surplusCount];
        _alertLabel.transform = CGAffineTransformTranslate(_alertLabel.transform, -10, 0);
        
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.35 initialSpringVelocity:0 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
            
            _alertLabel.alpha = 1;
            _alertLabel.transform = CGAffineTransformIdentity;
            
        } completion:nil];
    }
}


-(void)submitViewController{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.requestWaitView.alpha = 1;
        
    } completion:^(BOOL finished) {
        
        self.backView.alpha = 0;
        [self.requestWaitView show:@"正在请求" mode:(OddityRequestWaitAlertModeAnimateWithLabel)];
    }];
    
    __weak __typeof__(self) weakSelf = self;
    [UIView animateWithDuration:0.3 delay:0.3 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
        
        self.requestWaitView.layer.cornerRadius = 8;
        
        [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.mas_equalTo(self.view);
            make.size.mas_equalTo(CGSizeMake(150, 80));
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        [self.nContent createComment:self.inputTextView.text complete:^(BOOL success, NSString * _Nullable message) {
            
            [OddityLogObject createAppAction:OddityLogAtypeComment fromPageName:OddityLogPageDetailPage toPageName:OddityLogPageNullPage effective:success params:@{@"nid":@(weakSelf.nContent.nid)}];
            
            [weakSelf handleMethod:message finish:success];
        }];
    }];
}

-(void)handleMethod:(NSString *)title finish:(BOOL)fin{
    
    __weak __typeof__(self) weakSelf = self;
    
    if (fin) {
        [UIView animateWithDuration:0.3 animations:^{
            
            [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.center.mas_equalTo(self.view);
                make.size.mas_equalTo(CGSizeMake(150, 50));
            }];
            
            [self.requestWaitView show:title mode:(OddityRequestWaitAlertModeLabel)];
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            [self dismissViewControllerAnimated:true completion:nil];
        }];
        return;
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.mas_equalTo(self.view);
            make.size.mas_equalTo(CGSizeMake(150, 50));
        }];
        
        [self.requestWaitView show:title mode:(OddityRequestWaitAlertModeLabel)];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [weakSelf failMethod];
        });
        
    }];
}


-(void)failMethod{

    [self.requestWaitView show:@""];
    
    [UIView animateWithDuration:0.3 delay:0 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
        
        [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.right.with.bottom.with.top.mas_equalTo(self.backView);
        }];
        
        self.requestWaitView.layer.cornerRadius = 0;
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.backView.alpha = 1;
            self.requestWaitView.alpha = 0;
        }];
    }];
}

@end
