//
//  OddityEggsViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/5/16.
//
//

#import <Masonry/Masonry.h>
#import "OddityManagerInfoMationUtil.h"
#import "OddityEggsViewController.h"
#import "NSDictionary+Oddity.h"


#define OddityEddsCellIdentifier @"cell"
#define OddityEddsLineIdentifier @"line"


@interface OddityEggsViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) NSArray *dataSource;
@property (nonatomic,strong) UITableView *tableView;

@end

@implementation OddityEggsViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:(UIBarButtonItemStylePlain) target:self action:@selector(dismissViewController)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"复制整个信息" style:(UIBarButtonItemStylePlain) target:self action:@selector(copyAllDataSource)];
    
    _dataSource = [OddityManagerInfoMationUtil managerInfoMation].getDevelopeDictParams;
    
    _tableView = [[UITableView alloc ]init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    [_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view).insets(UIEdgeInsetsZero);
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.dataSource.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    id value = self.dataSource[indexPath.row];
    
    if ([value isKindOfClass:[NSString class]]) {
        NSString *dict = value;
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: OddityEddsLineIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault) reuseIdentifier: OddityEddsLineIdentifier];
        }
        cell.textLabel.text = dict;
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        return cell;
    }
    
    NSDictionary *dict = value;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: OddityEddsCellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleValue1) reuseIdentifier: OddityEddsCellIdentifier];
    }
    cell.textLabel.text = dict.allKeys.firstObject;
    cell.detailTextLabel.text = dict.allValues.firstObject;
    
    return cell;
}



- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    id value = self.dataSource[indexPath.row];
    
    if ([value isKindOfClass:[NSString class]]) {
    
        return false;
    }
    
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender{
    if (action == @selector(copy:)) {
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender{
    if (action == @selector(copy:)) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard]; // 黏贴板
        [pasteBoard setString:cell.detailTextLabel.text];
    }
}

-(void)copyAllDataSource{
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    for (id dict in self.dataSource) {
        
        if ([dict isKindOfClass:[NSString class]]) {
            continue;
        }
        [dictionary addEntriesFromDictionary:dict];
    }
    
    UIPasteboard *pasteBoard = [UIPasteboard generalPasteboard]; // 黏贴板
    [pasteBoard setString: [dictionary oddity_dataToJsonString]];
    
}

-(void)dismissViewController{

    [self dismissViewControllerAnimated:true completion:nil];
}

@end
