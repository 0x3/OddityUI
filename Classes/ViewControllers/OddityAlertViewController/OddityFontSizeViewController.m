//
//  OddityFontSizeViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/26.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityPlayerView.h"
#import <Masonry/Masonry.h>

#import "OddityFontSizeViewController.h"

@interface OddityFontSizeTopView : UIView

@property (nonatomic,strong) UIView *borderView;

@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,strong) OddityBigButton *cancelButton;

@end

@implementation OddityFontSizeTopView

-(instancetype)initWithFrame:(CGRect)frame{

    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.borderView = [[UIView alloc] init];
        self.borderView.backgroundColor = UIColor.color_t_5;
        [self addSubview:self.borderView];
        [self.borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.right.bottom.mas_equalTo(0);
            make.height.mas_equalTo(0.5);
        }];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.textColor = UIColor.color_t_2;
        self.titleLabel.text = @"字体大小";
        self.titleLabel.font = UIFont.font_t_3;
        [self addSubview:self.titleLabel];
        [self.titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.center.mas_equalTo(self);
        }];
        
        self.cancelButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
        self.cancelButton.titleLabel.font = UIFont.font_t_4;
        [self.cancelButton setTitleColor:UIColor.color_t_3 forState:(UIControlStateNormal)];
        [self.cancelButton setTitle:@"完成" forState:(UIControlStateNormal)];
        [self addSubview:self.cancelButton];
        [self.cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.right.mas_equalTo(-18);
            make.centerY.mas_equalTo(self);
        }];
    }
    
    return self;
}

@end







#define OddityTapSize CGSizeMake(50, 70)


@interface OddityFontSizeView : UIView

@property (nonatomic,strong) UIView *normalTapView;
@property (nonatomic,strong) UIView *bigTapView;
@property (nonatomic,strong) UIView *bigPlusTapView;



@property (nonatomic,strong) UIView *normalView;
@property (nonatomic,strong) UIView *bigView;
@property (nonatomic,strong) UIView *bigPlusView;

@property (nonatomic,strong) UIView *hPlusView;



@property (nonatomic,strong) UILabel *normalLabel;
@property (nonatomic,strong) UILabel *bigLabel;
@property (nonatomic,strong) UILabel *bigPlusLabel;

@property (nonatomic,strong) UIView *sliderView;

@property (nonatomic,strong) OddityBigButton *cancelButton;

@property (nonatomic,strong) UITapGestureRecognizer *normalTapGet;
@property (nonatomic,strong) UITapGestureRecognizer *bigTapGet;
@property (nonatomic,strong) UITapGestureRecognizer *bigPlusTapGet;


@end

@implementation OddityFontSizeView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.normalTapView = [[UIView alloc] init];
        self.bigTapView = [[UIView alloc] init];
        self.bigPlusTapView = [[UIView alloc] init];
        
        self.normalTapGet = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Tap:)];
        self.bigTapGet = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Tap:)];
        self.bigPlusTapGet = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Tap:)];
        
        [self.normalTapView addGestureRecognizer:self.normalTapGet];
        [self.bigTapView addGestureRecognizer:self.bigTapGet];
        [self.bigPlusTapView addGestureRecognizer:self.bigPlusTapGet];
        
        [self addSubview:self.normalTapView];
        [self addSubview:self.bigTapView];
        [self addSubview:self.bigPlusTapView];
        
        self.normalView = [[UIView alloc] init];
        self.normalView.backgroundColor = UIColor.color_t_5;
        [self addSubview:self.normalView];
        [self.normalView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.mas_equalTo(46);
            make.centerY.mas_equalTo(self).offset(10);
            make.height.mas_equalTo(12);
            make.width.mas_equalTo(1);
        }];
        

        self.bigView = [[UIView alloc] init];
        self.bigView.backgroundColor = UIColor.color_t_5;
        [self addSubview:self.bigView];
        [self.bigView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.mas_equalTo(self);
            make.centerY.mas_equalTo(self.normalView);
            make.height.mas_equalTo(self.normalView);
            make.width.mas_equalTo(self.normalView);
        }];
        
        
        self.bigPlusView = [[UIView alloc] init];
        self.bigPlusView.backgroundColor = UIColor.color_t_5;
        [self addSubview:self.bigPlusView];
        [self.bigPlusView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.right.mas_equalTo(-46);
            make.centerY.mas_equalTo(self.normalView);
            make.height.mas_equalTo(self.normalView);
            make.width.mas_equalTo(self.normalView);
        }];
        
        
        self.hPlusView = [[UIView alloc] init];
        self.hPlusView.backgroundColor = UIColor.color_t_5;
        [self addSubview:self.hPlusView];
        [self.hPlusView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.right.mas_equalTo(self.bigPlusView);
            make.left.mas_equalTo(self.normalView);
            
            make.centerY.mas_equalTo(self.normalView);
            make.height.mas_equalTo(1);
        }];
        
        
        self.normalLabel = [[UILabel alloc] init];
        self.normalLabel.text = @"标准";
        self.normalLabel.textColor = UIColor.color_t_2;
        self.normalLabel.font = UIFont.font_t_4;
        [self addSubview:self.normalLabel];
        [self.normalLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.bottom.mas_equalTo(self.normalView.mas_top).offset(-20);
            make.centerX.mas_equalTo(self.normalView);
        }];
        
        self.bigLabel = [[UILabel alloc] init];
        self.bigLabel.text = @"大";
        self.bigLabel.textColor = UIColor.color_t_2;
        self.bigLabel.font = UIFont.font_t_4;
        [self addSubview:self.bigLabel];
        [self.bigLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self.normalLabel);
            make.centerX.mas_equalTo(self.bigView);
        }];
        
        self.bigPlusLabel = [[UILabel alloc] init];
        self.bigPlusLabel.text = @"超大";
        self.bigPlusLabel.textColor = UIColor.color_t_2;
        self.bigPlusLabel.font = UIFont.font_t_4;
        [self addSubview:self.bigPlusLabel];
        [self.bigPlusLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerY.mas_equalTo(self.normalLabel);
            make.centerX.mas_equalTo(self.bigPlusView);
        }];
        

        self.sliderView = [[UIView alloc] init];
        self.sliderView.backgroundColor = UIColor.color_t_9;
        self.sliderView.layer.cornerRadius = 10;
        self.sliderView.layer.borderWidth = 1;
        self.sliderView.layer.borderColor = OddityUISetting.shareOddityUISetting.mainTone.CGColor;
        self.sliderView.clipsToBounds = true;
        [self addSubview:self.sliderView];
        [self.sliderView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.size.mas_equalTo(CGSizeMake(20, 20));
            make.center.mas_equalTo(self.normalView);
        }];
        
        [self.sliderView addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(pan:)]];
        
        [self toFontMode: OddityUISetting.shareOddityUISetting.oddityFontMode change:false];
        
        
        [self.normalTapView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.mas_equalTo(self.normalView);
            make.centerY.mas_equalTo(self.normalView).offset(-20);
            make.size.mas_equalTo(OddityTapSize);
        }];
        
        
        [self.bigTapView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.mas_equalTo(self.bigView);
            make.centerY.mas_equalTo(self.bigView).offset(-20);
            make.size.mas_equalTo(OddityTapSize);
        }];
        
        
        [self.bigPlusTapView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.mas_equalTo(self.bigPlusView);
            make.centerY.mas_equalTo(self.bigPlusView).offset(-20);
            make.size.mas_equalTo(OddityTapSize);
        }];
        
        [self layoutIfNeeded];
    }
    
    return self;
}

-(void)Tap:(UITapGestureRecognizer *)tap{


    if ([tap isEqual:self.normalTapGet]) {
        
        [self toFontMode:(OddityFontModeNormal) change:true];
    }
    
    
    if ([tap isEqual:self.bigTapGet]) {
        
        [self toFontMode:(OddityFontModeBig) change:true];
    }
    
    
    if ([tap isEqual:self.bigPlusTapGet]) {
        
        [self toFontMode:(OddityFontModeBigPlus) change:true];
    }
}


-(void)pan:(UIPanGestureRecognizer *)recognizer{

    CGPoint point = [recognizer locationInView:self];
    
    CGFloat silderLocation = point.x;
    
    CGFloat leftLocation = CGRectGetMaxX(self.normalView.frame);
    CGFloat centerLocation = CGRectGetMaxX(self.bigView.frame);
    CGFloat rightLocation = CGRectGetMaxX(self.bigPlusView.frame);
    
    if (recognizer.state == UIGestureRecognizerStateChanged) {
        
        if (silderLocation >= leftLocation-10 && silderLocation <= rightLocation-10) {
            
            [self.sliderView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.size.mas_equalTo(CGSizeMake(20, 20));
                make.centerY.mas_equalTo(self.normalView);
                make.left.mas_equalTo(silderLocation);
            }];
            
            [self.sliderView layoutIfNeeded];
        }
        
        return;
    }
    
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        
        if ( silderLocation > centerLocation) { // 右边
            
            if (ABS(silderLocation - centerLocation) > ABS(silderLocation - rightLocation)) { // 超大
                
                [self toFontMode:(OddityFontModeBigPlus) change:true];
                
            }else{ // 大
            
                [self toFontMode:(OddityFontModeBig) change:true];
            }
            
        }else{ // 左边
        
            if (ABS(silderLocation - centerLocation) > ABS(silderLocation - leftLocation)) { // 标准
                
                [self toFontMode:(OddityFontModeNormal) change:true];
                
            }else{ // 大
                
                [self toFontMode:(OddityFontModeBig) change:true];
            }
        }
        return;
    }
    
    if (recognizer.state == UIGestureRecognizerStateCancelled || recognizer.state == UIGestureRecognizerStateFailed) {
        
        [self toFontMode:(OddityUISetting.shareOddityUISetting.oddityFontMode) change:false];
    }
}

-(void)toFontMode:(OddityFontMode)mode change:(BOOL)chan{

    UIView *view;
    
    switch (mode) {
        case OddityFontModeBig:
            view = self.bigView;
            break;
        case OddityFontModeBigPlus:
            view = self.bigPlusView;
            break;
        case OddityFontModeNormal:
            view = self.normalView;
            break;
        default:
            view = self.normalView;
            break;
    }
    
    
    [UIView animateWithDuration:0.3 animations:^{
       
        [self.sliderView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.size.mas_equalTo(CGSizeMake(20, 20));
            make.centerY.mas_equalTo(self.normalView);
            make.centerX.mas_equalTo(view);
        }];
        
        [self layoutIfNeeded];
    }];
    
    if (chan && OddityUISetting.shareOddityUISetting.oddityFontMode != mode) {
        
        OddityUISetting.shareOddityUISetting.oddityFontMode = mode;
    }
    
}


@end











@interface OddityFontSizeViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityFontSizeViewControllerDismissAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityFontSizeViewController ()<UIViewControllerTransitioningDelegate,UIGestureRecognizerDelegate>

@property (nonatomic,strong) UIView *backView;

@property (nonatomic,strong) OddityFontSizeView *centerView;
@property (nonatomic,strong) OddityFontSizeTopView *topView;

@property (nonatomic,strong) OddityFontSizeViewControllerPresentdAnimation *cPresentedViewControllerAnimatedTransitioning;
@property (nonatomic,strong) OddityFontSizeViewControllerDismissAnimation *cDismissViewControllerAnimatedTransitioning;
@end

@implementation OddityFontSizeViewController

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        _cDismissViewControllerAnimatedTransitioning  = [[OddityFontSizeViewControllerDismissAnimation alloc]init];
        _cPresentedViewControllerAnimatedTransitioning  = [[OddityFontSizeViewControllerPresentdAnimation alloc]init];
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _cPresentedViewControllerAnimatedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _cDismissViewControllerAnimatedTransitioning;
}

-(void)viewDidLoad{

    [super viewDidLoad];
    
    self.backView = [[UIView alloc] init];
    self.backView.backgroundColor = UIColor.color_t_6;
    [self.view addSubview:self.backView];
    [self.backView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.bottom.right.mas_equalTo(0);
        make.height.mas_equalTo(40+29+10+40+40);
    }];
    
    self.topView = [[OddityFontSizeTopView alloc] init];
    [self.backView addSubview:self.topView];
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.right.left.mas_equalTo(0);
        make.height.mas_equalTo(44);
    }];
    
    [self.topView.cancelButton addTarget:self action:@selector(dismiss) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    self.centerView = [[OddityFontSizeView alloc] init];
    [self.backView addSubview:self.centerView];
    [self.centerView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(self.topView.mas_bottom);
        make.right.left.bottom.mas_equalTo(0);
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    [self.view layoutIfNeeded];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{

    return !CGRectContainsPoint(self.backView.frame, [gestureRecognizer locationInView:self.view]);
}

-(void)dismiss{

    [self dismissViewControllerAnimated:true completion:nil];
}

@end



@implementation OddityFontSizeViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityFontSizeViewController *toViewController = (OddityFontSizeViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
    
    toViewController.backView.transform = CGAffineTransformTranslate(toViewController.backView.transform, 0, CGRectGetHeight(toViewController.backView.frame));
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]  animations:^{
        
        toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.4];
        
        toViewController.backView.transform = CGAffineTransformIdentity;
        
        [toViewController.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
        
        if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
            
            [[[UIApplication sharedApplication]keyWindow] bringSubviewToFront:[OddityPlayerView sharePlayerView]];
        }
    }];
}

@end



@implementation OddityFontSizeViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    
    OddityFontSizeViewController *fromViewController = (OddityFontSizeViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        fromViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
        
        fromViewController.backView.transform = CGAffineTransformTranslate(fromViewController.backView.transform, 0, CGRectGetHeight(fromViewController.backView.frame));
        
        [fromViewController.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end
