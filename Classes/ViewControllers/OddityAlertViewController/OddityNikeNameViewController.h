//
//  OddityNikeNameViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/6/13.
//
//

#import <UIKit/UIKit.h>

@interface OddityNikeNameViewController : UIViewController

-(instancetype _Nullable )initWithOldUserName:(NSString *_Nullable)userName;

/**
 打开弹出框

 @param duration 动画执行时间
 @param completion 是否打开完成
 */
-(void)openAlertView:(NSTimeInterval)duration :(void (^ __nullable)(BOOL finished))completion;


/**
 关闭弹出框

 @param duration 动画执行时间
 @param completion 是否关闭完成
 */
-(void)closeAlertView:(NSTimeInterval)duration :(void (^ __nullable)(BOOL finished))completion;
@end
