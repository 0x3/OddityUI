//
//  OddityNikeNameViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/13.
//
//

#import "OddityInsetFiled.h"
#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityRequestWaitView.h"
#import "OddityNikeNameViewController.h"

#pragma mark : 视图切换动画

@interface OddityNikeNameViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityNikeNameViewControllerDismissAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@implementation OddityNikeNameViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityNikeNameViewController *toViewController = (OddityNikeNameViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]+0.3 animations:^{
        
        toViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.6];
    }];
    
    [toViewController openAlertView:[self transitionDuration:transitionContext] :^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end



@implementation OddityNikeNameViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    
    OddityNikeNameViewController *fromViewController = (OddityNikeNameViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]+0.3 animations:^{
       
        fromViewController.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0];
    }];
    
    [fromViewController closeAlertView:[self transitionDuration:transitionContext] :^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end

#pragma mark : 上方标题

@interface OddityNikeNameTitleView : UIView

@property(nonatomic,strong) UIView* bottomView;

@property(nonatomic,strong) UILabel* titleLabel;
@property(nonatomic,strong) UILabel* subTitleLabel;

@end

@implementation OddityNikeNameTitleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = UIColor.color_t_6;
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"设置昵称";
        _titleLabel.font = [UIFont font_t_3];
        _titleLabel.textColor = [UIColor color_t_2];
        [self addSubview:_titleLabel];
        
        _subTitleLabel = [[UILabel alloc] init];
        _subTitleLabel.text = @"长度限制： 2～20个字符";
        _subTitleLabel.font = [UIFont font_t_7];
        _subTitleLabel.textColor = [UIColor color_t_4];
        [self addSubview:_subTitleLabel];
        
        [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(8);
            make.centerX.mas_equalTo(self);
        }];
        
        [_subTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(_titleLabel.mas_bottom).offset(3);
            make.centerX.mas_equalTo(self);
        }];
        
        
        
        _bottomView = [[UIView alloc] init];
        [self addSubview: _bottomView];
        _bottomView.backgroundColor = UIColor.color_t_5;
        [_bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.right.with.bottom.mas_equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}


-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(2.0, 2.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

@end


#pragma mark : 中间输入视图

@interface OddityNikeNameInputView : UIView<UITextFieldDelegate>

-(void)setPlaAttri:(NSString *)pla;

@property(nonatomic,strong) OddityInsetFiled* textFiled;

@end

@implementation OddityNikeNameInputView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = UIColor.color_t_9;
        
        
        
        _textFiled = [[OddityInsetFiled alloc] initTextLeft:8 editingLeft:8 placeholderLeft:8];
        _textFiled.layer.cornerRadius = 2;
        _textFiled.delegate = self;
        _textFiled.layer.borderWidth = 0.5;
        _textFiled.attributedPlaceholder = [self attributedPlaceholder:@"请输入新的昵称"];
        _textFiled.layer.borderColor = UIColor.color_t_5.CGColor;
        _textFiled.keyboardAppearance = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNight ? UIKeyboardAppearanceDark : UIKeyboardAppearanceDefault;
//        _textFiled.font = UIFont.font_t_2;
        _textFiled.textColor = UIColor.color_t_2;
        [self addSubview:_textFiled];
        
        [_textFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.mas_equalTo(13);
            make.bottom.mas_equalTo(-13);
            make.left.mas_equalTo(11);
            make.right.mas_equalTo(-11);
            make.height.mas_equalTo(48);
        }];
    }
    return self;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    if ([string containsString:@" "]) {
        
        textField.transform = CGAffineTransformTranslate(textField.transform, -12, 0);
        
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.35 initialSpringVelocity:0 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
            
            textField.transform = CGAffineTransformIdentity;
        } completion:nil];
        
        return NO;
    }
    return YES;
}

-(void)setPlaAttri:(NSString *)pla{

    if (pla) {
        
        _textFiled.attributedPlaceholder = [self attributedPlaceholder:pla];
    }
}

-(NSAttributedString *)attributedPlaceholder:(NSString *)string {
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_4],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: string attributes:attris];
    
    return attriString;
}

@end


#pragma mark : 下方点击按钮

@interface OddityNikeNameButtonView : UIView

@property(nonatomic,strong) UIView* topView;
@property(nonatomic,strong) UIView* middleView;

@property(nonatomic,strong) UIButton* cancelButton;
@property(nonatomic,strong) UIButton* submitButton;

@end

@implementation OddityNikeNameButtonView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = UIColor.color_t_9;
        
        _cancelButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
        _cancelButton.titleLabel.font = UIFont.font_t_3;
        [_cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
        [_cancelButton setTitleColor:UIColor.color_t_2 forState:(UIControlStateNormal)];
        [self addSubview:_cancelButton];
        [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.with.top.with.bottom.mas_equalTo(0);
            make.width.mas_equalTo(self).multipliedBy(0.5);
        }];
        
        _submitButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
        _submitButton.titleLabel.font = UIFont.font_t_3;
        [_submitButton setTitle:@"确认" forState:(UIControlStateNormal)];
        [_submitButton setTitleColor:OddityUISetting.shareOddityUISetting.mainTone forState:(UIControlStateNormal)];
        [_submitButton setTitleColor:[OddityUISetting.shareOddityUISetting.mainTone colorWithAlphaComponent:0.3] forState:(UIControlStateDisabled)];
        [self addSubview:_submitButton];
        [_submitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.right.with.top.with.bottom.mas_equalTo(0);
            make.width.mas_equalTo(self).multipliedBy(0.5);
        }];
        
        _middleView = [[UIView alloc] init];
        _middleView.backgroundColor = UIColor.color_t_5;
        [self addSubview: _middleView];
        [_middleView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.center.mas_equalTo(self);
            make.height.mas_equalTo(26);
            make.width.mas_equalTo(0.5);
        }];
        
        _topView = [[UIView alloc] init];
        [self addSubview: _topView];
        _topView.backgroundColor = UIColor.color_t_5;
        [_topView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.right.with.top.mas_equalTo(self);
            make.height.mas_equalTo(0.5);
        }];
    }
    return self;
}


-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(2.0, 2.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

@end

#pragma mark : viewController

@interface OddityNikeNameViewController ()<UIViewControllerTransitioningDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong) NSString *userName;

@property(nonatomic,assign) BOOL isSubmit;

@property(nonatomic,strong) UITapGestureRecognizer *recognizer;

@property(nonatomic,strong) OddityRequestWaitView *requestWaitView;

@property(nonatomic,strong) OddityNikeNameTitleView *topView;
@property(nonatomic,strong) OddityNikeNameInputView *centerView;
@property(nonatomic,strong) OddityNikeNameButtonView *bottomView;

@property(nonatomic,strong)OddityNikeNameViewControllerPresentdAnimation *cPresentedViewControllerAnimatedTransitioning;
@property(nonatomic,strong)OddityNikeNameViewControllerDismissAnimation *cDismissViewControllerAnimatedTransitioning;
@end

@implementation OddityNikeNameViewController


-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(instancetype)initWithOldUserName:(NSString *)userName{
    
    self = [super initWithNibName:nil bundle:nil];
    if(self) {
        
        _userName = userName;
        
        _cDismissViewControllerAnimatedTransitioning  = [[OddityNikeNameViewControllerDismissAnimation alloc]init];
        _cPresentedViewControllerAnimatedTransitioning  = [[OddityNikeNameViewControllerPresentdAnimation alloc]init];
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _cPresentedViewControllerAnimatedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _cDismissViewControllerAnimatedTransitioning;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.6];
    
    _centerView = [[OddityNikeNameInputView alloc] init];
    [self.view addSubview:_centerView];
    
    _bottomView = [[OddityNikeNameButtonView alloc] init];
    [self.view addSubview:_bottomView];
    
    _topView = [[OddityNikeNameTitleView alloc] init];
    [self.view addSubview:_topView];
    
    self.requestWaitView = [[OddityRequestWaitView alloc] init];
    self.requestWaitView.alpha = 0;
    self.requestWaitView.layer.cornerRadius = 2;
    self.requestWaitView.clipsToBounds = true;
    self.requestWaitView.backgroundColor = UIColor.color_t_9;
    [self.view addSubview:self.requestWaitView];
    
    [self.centerView setPlaAttri:self.userName];
    
    self.bottomView.submitButton.enabled = false;
    
    [self.centerView.textFiled addTarget:self action:@selector(textFiledChange:) forControlEvents:(UIControlEventEditingChanged)];
    
    self.recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandleActionMethod)];
    self.recognizer.delegate = self;
    [self.view addGestureRecognizer: self.recognizer];
    [self.bottomView.cancelButton addTarget:self action:@selector(cencelMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [self.bottomView.submitButton addTarget:self action:@selector(submitMethod) forControlEvents:(UIControlEventTouchUpInside)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)textFiledChange:(UITextField *)text{

    self.bottomView.submitButton.enabled = text.text.length >= 2 && text.text.length <= 20;
}

-(void)cencelMethod{

    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)submitMethod{
    
    [self submitWaitView];
}


-(void)submitWaitView{
    
    self.isSubmit = true;
    
    [self.view bringSubviewToFront:self.requestWaitView];
    [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.with.top.with.right.mas_equalTo(self.topView);
        make.bottom.mas_equalTo(self.bottomView);
    }];
    
    [self.requestWaitView layoutIfNeeded];
    
    [self.requestWaitView show:@"" mode:(OddityRequestWaitAlertModeAnimate)];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.requestWaitView.alpha = 1;
    } completion:^(BOOL finished) {
        
        self.topView.hidden = true;
        self.centerView.hidden = true;
        self.bottomView.hidden = true;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.requestWaitView.layer.cornerRadius = 8;
            
            [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.center.mas_equalTo(self.view);
                make.size.mas_equalTo(CGSizeMake(80, 80));
            }];
            
            [self.view layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            [self dismissWaitViewTitle:@"请求失败" finish:true];
        }];
    }];
}


-(void)dismissWaitViewTitle:(NSString *)title finish:(BOOL)f{

    [self.requestWaitView show:title];
    [self.requestWaitView layoutIfNeeded];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.mas_equalTo(self.view);
            make.size.mas_equalTo(CGSizeMake(120, 50));
        }];
        
        [self.requestWaitView show:title mode:(OddityRequestWaitAlertModeLabel)];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        if (f) {
            
            [self finishRequestMethod];
        }else{
        
            [self failRequestMethod];
        }
        
    }];
}


-(void)finishRequestMethod {

    [self dismissViewControllerAnimated:true completion:nil];
}

/**
 请求成功方法
 */
-(void)failRequestMethod{

    
    [UIView animateWithDuration:0.3 delay:1.5 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
        
        [self.requestWaitView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.with.top.with.right.mas_equalTo(self.topView);
            make.bottom.mas_equalTo(self.bottomView);
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        [self.requestWaitView show:@""];
        [self.requestWaitView layoutIfNeeded];
        
        self.topView.hidden = false;
        self.centerView.hidden = false;
        self.bottomView.hidden = false;
        
        [UIView animateWithDuration:0.3 animations:^{
            
            self.requestWaitView.alpha = 0;
        }];
        
        self.isSubmit = false;
    }];
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    
    [self.centerView.textFiled becomeFirstResponder];
}

-(void)openAlertView:(NSTimeInterval)duration :(void (^ __nullable)(BOOL finished))completion{

    [self.centerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.width.right.mas_equalTo(self.topView);
        make.centerY.mas_equalTo(self.view);
    }];
    
    [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(28);
        make.right.mas_equalTo(-28);
        make.centerY.mas_equalTo(self.view);
        make.height.mas_equalTo(55);
    }];
    
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(52);
        make.left.width.right.mas_equalTo(self.topView);
        make.centerY.mas_equalTo(self.view);
    }];
    
    [self.view layoutIfNeeded];
    
    [UIView animateWithDuration:duration delay:0.1 options: UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.view.backgroundColor = [UIColor.blackColor colorWithAlphaComponent:0.8];
        
        [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(28);
            make.right.mas_equalTo(-28);
            make.bottom.mas_equalTo(self.centerView.mas_top);
            make.height.mas_equalTo(55);
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
    
    [UIView animateWithDuration:duration delay:0.3 options: UIViewAnimationOptionCurveEaseInOut animations:^{
        
        [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.mas_equalTo(52);
            make.left.width.right.mas_equalTo(self.topView);
            make.top.mas_equalTo(self.centerView.mas_bottom);
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        completion(finished);
    }];
}

-(void)closeAlertView:(NSTimeInterval)duration :(void (^ __nullable)(BOOL finished))completion{

    [self.centerView.textFiled resignFirstResponder];
    
    [UIView animateWithDuration:duration delay:0.3 options: UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.centerView.alpha = 0;
        self.topView.alpha = 0;
        self.bottomView.alpha = 0;
        
        [self.topView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(28);
            make.right.mas_equalTo(-28);
            make.centerY.mas_equalTo(self.centerView);
            make.height.mas_equalTo(55);
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        completion(finished);
    }];
    
    [UIView animateWithDuration:duration delay:0.1 options: UIViewAnimationOptionCurveEaseInOut animations:^{
        
        [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.height.mas_equalTo(52);
            make.left.width.right.mas_equalTo(self.topView);
            make.centerY.mas_equalTo(self.centerView);
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
    }];
}



#pragma mark: 键盘处理方法

-(void)keyboardWillShow:(NSNotification *)notification{
    
    CGRect frame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [self.centerView mas_updateConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.mas_equalTo(self.view).offset(-(CGRectGetHeight(frame)/2));
    }];
    
    [self.view layoutIfNeeded];
}

-(void)keyboardWillHidden:(NSNotification *)notification{
    
    [self.centerView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(self.view);
    }];
    
    [self.view layoutIfNeeded];
}


#pragma mark: UIGestureRecognizerDelegate

-(void)tapHandleActionMethod{
    
    if (self.centerView.textFiled.isFirstResponder) {
        
        [self.centerView.textFiled resignFirstResponder];
        
        return;
    }
    
    [self dismissViewControllerAnimated:true completion:nil];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{

    CGPoint point = [gestureRecognizer locationInView:self.view];
    
    if (CGRectContainsPoint(self.centerView.frame, point) || CGRectContainsPoint(self.topView.frame, point) || CGRectContainsPoint(self.bottomView.frame, point) || self.isSubmit) {
        
        return false;
    }
    
    return true;
}

@end
