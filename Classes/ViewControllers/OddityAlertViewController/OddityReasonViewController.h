//
//  OddityReasonViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/3/29.
//
//

#import <UIKit/UIKit.h>
#import "OddityChannelNews.h"

/**
 *  新闻的ViewController Delegate
 */
@protocol OddityReasonViewControllerDelegate<NSObject>

@required

-(void)reasonViewControllerDidClickDisLikeButton:(UITableViewCell *) cell channels:(OddityChannelNews *)channel newObject:(OddityNew *)nObj;

@end


@interface OddityReasonViewController : UIViewController


@property(nonatomic,strong)UITableViewCell *cell;
@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,weak) id<OddityReasonViewControllerDelegate> delegate;

@property(nonatomic,strong)OddityNew *nObj;
@property(nonatomic,strong)OddityChannelNews *channel;

@property(nonatomic,strong)UIView *backView;
@property(nonatomic,strong)UIView *clearView;

/**
 *  设置原因列表
 *
 *  @param reason 原因列表
 *
 *  @return viewController
 */
-(instancetype)initWithReasonsArray:(NSArray *)reason;

/**
 *   获取需要的高度
 *
 *  @param block 高度计算回调
 */
-(void)needHeightFinishBlock:(void(^)(CGFloat height))block;

/**
 *  填充数据
 *
 *  @param cell      要删除的那一个行
 *  @param tableView 删除的tableview
 *  @param index     要删除的IndexPath
 *
 *  @return 视图
 */
-(OddityReasonViewController *)fillMethod:(OddityNewBaseTableViewCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)index;

/**
 *  刷新表格的方法
 */
-(void)reloadTableView;

/**
 *  设置删除的行 删除线出现
 */
-(void)toDelete;

/**
 *  设置删除的行 删除线恢复
 */
-(void)toDisDelete;
@end







@interface OddityKTCenterFlowLayout : UICollectionViewFlowLayout

@end








@interface OddityReasonCell : UICollectionViewCell

@property (nonatomic, strong) OddityUILabelPadding *reasonLabel;

@end






@interface OddityiReasonView : UIView


@property(nonatomic,strong)UIButton *submitButton;
/**
 *  初始化方法 根据 原因集合
 *
 *  @param reasons 原因集合
 *
 *  @return 创建完成的对象
 */
-(instancetype)initWithReasonsArray:(NSArray *)reasons;

/**
 *  获得原因视图的高度
 *
 *  @param block 回调函数
 */
-(void)needHeightFinishBlock:(void(^)(CGFloat height))block;
@end







@interface OddityCustomReasonViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityCustomReasonViewControllerDismissAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end
