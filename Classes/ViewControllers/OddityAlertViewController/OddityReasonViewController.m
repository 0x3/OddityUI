//
//  OddityReasonViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/3/29.
//
//

#import "OddityCategorys.h"
#import "OddityCustomView.h"
#import "OddityPlayerView.h"
#import "OddityReasonViewController.h"


@interface OddityReasonViewController()<UIViewControllerTransitioningDelegate>

@property(nonatomic,strong)UITapGestureRecognizer *backTapGestureRecognizer;
@property(nonatomic,strong)UITapGestureRecognizer *clearTapGestureRecognizer;

@property(nonatomic,strong)NSIndexPath *indexPath;

@property(nonatomic,strong)OddityCustomReasonViewControllerPresentdAnimation *cPresentedViewControllerAnimatedTransitioning;
@property(nonatomic,strong)OddityCustomReasonViewControllerDismissAnimation *cDismissViewControllerAnimatedTransitioning;

@property(nonatomic,strong)NSArray *dataSources;

@property(nonatomic,strong)OddityiReasonView *reasonView;

@end

@implementation OddityReasonViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(instancetype)initWithReasonsArray:(NSArray *)reason{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {

        _backTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickToDismiss)];
        _clearTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickToDismiss)];
        
        _dataSources = reason;
        
        _cDismissViewControllerAnimatedTransitioning  = [[OddityCustomReasonViewControllerDismissAnimation alloc]init];
        _cPresentedViewControllerAnimatedTransitioning  = [[OddityCustomReasonViewControllerPresentdAnimation alloc]init];
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}


-(void)toDelete{

    if ([self.cell isKindOfClass:[OddityNewBaseTableViewCell class]]) {
        
        [((OddityNewBaseTableViewCell *)self.cell) toDelete];
    }
}

-(void)toDisDelete{

    if ([self.cell isKindOfClass:[OddityNewBaseTableViewCell class]]) {
        
        [((OddityNewBaseTableViewCell *)self.cell) toReset];
    }
}


-(void)clickToDismiss{

    [self dismissViewControllerAnimated:true completion:nil];
}


-(OddityReasonViewController *)fillMethod:(OddityNewBaseTableViewCell *)cell tableView:(UITableView *)tableView indexPath:(NSIndexPath *)index{

    _cell = cell;
    _indexPath = index;
    _tableView = tableView;
    
    return self;
}

-(void)reloadTableView{

    [self.tableView reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:(UITableViewRowAnimationNone)];
}

-(void)viewDidLoad{

    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    _backView = [[UIView alloc]init];
    [self.view addSubview:_backView];
    [_backView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.edges.equalTo(self.view).insets(UIEdgeInsetsZero);
    }];
    
    _reasonView = [[OddityiReasonView alloc]initWithReasonsArray: _dataSources ?: @[
                                                                                    NSString.oddity_reason_reason_dislike_string,
                                                                                    NSString.oddity_reason_reason_old_string,
                                                                                    NSString.oddity_reason_reason_low_string
                                                                                    ]];
    [self.view addSubview:_reasonView];
    [_reasonView.submitButton addTarget:self action:@selector(disLiketoDelete) forControlEvents:(UIControlEventTouchUpInside)];
    [_reasonView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.bottom.equalTo(@0);
        make.left.equalTo(@0);
        make.right.equalTo(@0);
    }];
    
    _clearView = [[UIView alloc]init];
    _clearView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_clearView];
    
    [self.backView addGestureRecognizer:_backTapGestureRecognizer];
    [self.clearView addGestureRecognizer:_clearTapGestureRecognizer];
}

-(void)disLiketoDelete{
    
    typeof(self) __weak weakSelf = self;
    [self dismissViewControllerAnimated:true completion:^{
       
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(reasonViewControllerDidClickDisLikeButton:channels:newObject:)] ){
            
            [weakSelf.delegate reasonViewControllerDidClickDisLikeButton:weakSelf.cell channels:self.channel newObject:self.nObj];
        }
    }];
}

-(void)needHeightFinishBlock:(void(^)(CGFloat height))block{

    [_reasonView needHeightFinishBlock:block];
}


-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{

    return _cPresentedViewControllerAnimatedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{

    return _cDismissViewControllerAnimatedTransitioning;
}

@end

@interface OddityCustomReasonViewControllerPresentdAnimation ()

@property(nonatomic,weak)OddityReasonViewController *toViewController;

@end

@implementation OddityCustomReasonViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    _toViewController = (OddityReasonViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    _toViewController.reasonView.hidden = true;
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:_toViewController.view];
    
    [self fix:_toViewController.cell tableView:_toViewController.tableView completion:^(CGFloat height) {
        
        _toViewController.cell.frame = [_toViewController.cell convertRect:_toViewController.cell.bounds toView:[UIApplication sharedApplication].keyWindow];
        
        
        [_toViewController.view insertSubview:_toViewController.cell belowSubview:_toViewController.clearView];
        [_toViewController.clearView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.equalTo(_toViewController.cell);
            make.right.equalTo(_toViewController.cell);
            make.bottom.equalTo(_toViewController.cell);
            make.left.equalTo(_toViewController.cell);
        }];
        
        [_toViewController.reasonView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            
            make.height.equalTo(@30);
            make.centerY.equalTo(_toViewController.cell);
        }];
        
        [_toViewController.view layoutIfNeeded];

        [_toViewController.reasonView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            
            make.height.equalTo(@(height));
            make.top.equalTo(_toViewController.cell.mas_bottom);
        }];
        
        _toViewController.reasonView.hidden = false;
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            
            [_toViewController toDelete];
            
            [_toViewController.view layoutIfNeeded];
            _toViewController.backView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.3];
            
        } completion:^(BOOL finished) {
            
            [transitionContext completeTransition:true];
            
            if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
                
                [[[UIApplication sharedApplication]keyWindow] bringSubviewToFront:[OddityPlayerView sharePlayerView]];
            }
        }];
    }];
}

-(void)fix:(UITableViewCell *)cell tableView:(UITableView *)tableView completion:(void (^ __nullable)(CGFloat height))completion{
    
    [_toViewController needHeightFinishBlock:^(CGFloat height) {
        
        CGRect cellFrame = [cell convertRect:cell.bounds toView:[UIApplication sharedApplication].keyWindow];
        
        if( CGRectGetMinY(cellFrame)-64 < 0 ){
            
            [UIView animateWithDuration:0.3 animations:^{
                
                [tableView setContentOffset:CGPointMake(tableView.contentOffset.x, tableView.contentOffset.y-ABS(CGRectGetMinY(cellFrame)-64))];
                
            } completion:^(BOOL finished) {
                
                completion(height);
            }];
            return;
        }
        
        
        CGFloat bottomSpace =  CGRectGetMaxY(cell.frame)+height-(tableView.contentOffset.y+tableView.frame.size.height);
        
        if (bottomSpace > 0){
            
            [UIView animateWithDuration:0.3 animations:^{
                
                [tableView setContentOffset:CGPointMake(tableView.contentOffset.x, tableView.contentOffset.y+bottomSpace)];
                
            } completion:^(BOOL finished) {
                
                completion(height);
            }];
            
            return;
        }
        completion(height);
    }];
}



@end



@implementation OddityCustomReasonViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityReasonViewController *fromViewController = (OddityReasonViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    [fromViewController.reasonView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        
        make.height.equalTo(@0);
        make.top.equalTo(fromViewController.cell.mas_bottom);
    }];
    
    [fromViewController.clearView removeFromSuperview];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        [fromViewController.view layoutIfNeeded];
        
        [fromViewController toDisDelete];
        
        fromViewController.backView.alpha = 0;
        
    } completion:^(BOOL finished) {
        
        [fromViewController reloadTableView];
        
        [transitionContext completeTransition: true];
    }];
}

@end



@interface OddityiReasonView()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UILabel *descLabel;

@property(nonatomic,strong)UICollectionView *collectionView;

@property(nonatomic,strong)NSArray *dataSources;

@end

@implementation OddityiReasonView


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(instancetype)initWithReasonsArray:(NSArray *)reasons{

    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        
        self.backgroundColor = [UIColor color_t_9];
        
        self.clipsToBounds = true;
        
        _dataSources = [NSArray arrayWithArray:reasons];
        
        _descLabel = [[UILabel alloc]init];
        [self addSubview:_descLabel];
        
        _descLabel.font = [UIFont systemFontOfSize:13];
        _descLabel.text = NSString.oddity_reason_reason_desc_string;
        _descLabel.textColor = [UIColor color_t_3];
        
        [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(self).offset(16);
            make.centerX.equalTo(self);
        }];
        
        _submitButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
        [_submitButton setTitle:NSString.oddity_reason_submit_button_title_string forState:(UIControlStateNormal)];
        
        _submitButton.backgroundColor = [UIColor color_t_7];
        [_submitButton setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        
        [self addSubview:_submitButton];
        [_submitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(@-8);
            make.centerX.equalTo(self);
            make.width.equalTo(@100);
            make.height.equalTo(@35);
        }];
        
        
        OddityKTCenterFlowLayout *flowLayout = [[OddityKTCenterFlowLayout alloc] init];
        
        flowLayout.minimumInteritemSpacing = 16.f;
        flowLayout.minimumLineSpacing = 16.f;
        
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        flowLayout.sectionInset = UIEdgeInsetsMake(8, 16, 8, 16);
        _collectionView = [[UICollectionView alloc]initWithFrame:(CGRectZero) collectionViewLayout:flowLayout];
        _collectionView.allowsMultipleSelection = true;
        _collectionView.backgroundColor = [UIColor color_t_9];
        [_collectionView registerClass:[OddityReasonCell class] forCellWithReuseIdentifier:@"cell"];
        
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        [self addSubview:_collectionView];
        
        [_collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_descLabel.mas_bottom).offset(8);
            
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            
            make.bottom.equalTo(_submitButton.mas_top).offset(-8);
        }];
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
        
        [self layoutIfNeeded];
    }
    
    return self;
}

-(void)refreshThemeMethod{

    [_collectionView reloadData];
    self.backgroundColor = [UIColor color_t_9];
    _collectionView.backgroundColor = [UIColor color_t_9];
    _submitButton.backgroundColor = [UIColor color_t_7];
}


-(void)needHeightFinishBlock:(void(^)(CGFloat height))block{

    [UIView animateWithDuration:0.3 animations:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_collectionView reloadData];
        });
        
    } completion:^(BOOL finished) {
        
        [_collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_descLabel.mas_bottom).offset(8);
            
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            
            make.bottom.equalTo(_submitButton.mas_top).offset(-8);
            
            make.height.equalTo(@(self.collectionView.collectionViewLayout.collectionViewContentSize.height));
        }];
        
        [self layoutIfNeeded];
        
        if (!block) {
            return;
        }
        
        block(self.frame.size.height);
    }];
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataSources.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OddityReasonCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.reasonLabel.text = [self.dataSources objectAtIndex:indexPath.row];
    
    if (cell.selected) {
        
        cell.reasonLabel.textColor = [UIColor color_t_7];
        cell.layer.borderColor = [UIColor color_t_7].CGColor;
    }else{
        
        cell.reasonLabel.textColor = [UIColor color_t_3];
        cell.layer.borderColor = [UIColor color_t_5].CGColor;
    }
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSStringDrawingOptions opts = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    
    NSDictionary *attributes = @{ NSFontAttributeName : [UIFont font_t_4] };
    
    CGSize size = [(NSString *)self.dataSources[indexPath.row] boundingRectWithSize:(CGSizeMake(800, 200)) options:opts attributes:attributes context:nil].size;
    
    
    return CGSizeMake(size.width+16, size.height+12);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OddityReasonCell *cell = (OddityReasonCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    cell.reasonLabel.textColor = [UIColor color_t_7];
    cell.layer.borderColor = [UIColor color_t_7].CGColor;
}


-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OddityReasonCell *cell = (OddityReasonCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    cell.reasonLabel.textColor = [UIColor color_t_3];
    cell.layer.borderColor = [UIColor color_t_5].CGColor;
}


@end



@interface OddityReasonCell ()

@end

@implementation OddityReasonCell


-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    
    if ( self ) {
        
        _reasonLabel = [[OddityUILabelPadding alloc]init];
        _reasonLabel.font = [UIFont font_t_4];
        _reasonLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:_reasonLabel];
        
        [_reasonLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.center.equalTo(self);
        }];
        
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor color_t_5].CGColor;
    }
    
    return self;
}

@end


@interface OddityKTCenterFlowLayout ()
@property (nonatomic) NSMutableDictionary *attrCache;
@end

@implementation OddityKTCenterFlowLayout

- (void)prepareLayout
{
    // Clear the attrCache
    self.attrCache = [NSMutableDictionary new];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *updatedAttributes = [NSMutableArray new];
    
    NSInteger sections = [self.collectionView numberOfSections];
    NSInteger s = 0;
    while (s < sections)
    {
        NSInteger rows = [self.collectionView numberOfItemsInSection:s];
        NSInteger r = 0;
        while (r < rows)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:r inSection:s];
            
            UICollectionViewLayoutAttributes *attrs = [self layoutAttributesForItemAtIndexPath:indexPath];
            
            if (attrs && CGRectIntersectsRect(attrs.frame, rect))
            {
                [updatedAttributes addObject:attrs];
            }
            
            UICollectionViewLayoutAttributes *headerAttrs =  [super layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                                                   atIndexPath:indexPath];
            
            if (headerAttrs && CGRectIntersectsRect(headerAttrs.frame, rect))
            {
                [updatedAttributes addObject:headerAttrs];
            }
            
            UICollectionViewLayoutAttributes *footerAttrs =  [super layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                                                                                   atIndexPath:indexPath];
            
            if (footerAttrs && CGRectIntersectsRect(footerAttrs.frame, rect))
            {
                [updatedAttributes addObject:footerAttrs];
            }
            
            r++;
        }
        s++;
    }
    
    return updatedAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.attrCache[indexPath])
    {
        return self.attrCache[indexPath];
    }
    
    // Find the other items in the same "row"
    NSMutableArray *rowBuddies = [NSMutableArray new];
    
    // Calculate the available width to center stuff within
    // sectionInset is NOT applicable here because a) we're centering stuff
    // and b) Flow layout has arranged the cells to respect the inset. We're
    // just hijacking the X position.
    CGFloat collectionViewWidth = CGRectGetWidth(self.collectionView.bounds) -
    self.collectionView.contentInset.left -
    self.collectionView.contentInset.right;
    
    // To find other items in the "row", we need a rect to check intersects against.
    // Take the item attributes frame (from vanilla flow layout), and stretch it out
    CGRect rowTestFrame = [super layoutAttributesForItemAtIndexPath:indexPath].frame;
    rowTestFrame.origin.x = 0;
    rowTestFrame.size.width = collectionViewWidth;
    
    NSInteger totalRows = [self.collectionView numberOfItemsInSection:indexPath.section];
    
    // From this item, work backwards to find the first item in the row
    // Decrement the row index until a) we get to 0, b) we reach a previous row
    NSInteger rowStartIDX = indexPath.row;
    while (true)
    {
        NSInteger prevIDX = rowStartIDX - 1;
        
        if (prevIDX < 0)
        {
            break;
        }
        
        NSIndexPath *prevPath = [NSIndexPath indexPathForRow:prevIDX inSection:indexPath.section];
        CGRect prevFrame = [super layoutAttributesForItemAtIndexPath:prevPath].frame;
        
        // If the item intersects the test frame, it's in the same row
        if (CGRectIntersectsRect(prevFrame, rowTestFrame))
            rowStartIDX = prevIDX;
        else
            // Found previous row, escape!
            break;
    }
    
    // Now, work back UP to find the last item in the row
    // For each item in the row, add it's attributes to rowBuddies
    NSInteger buddyIDX = rowStartIDX;
    while (true)
    {
        if (buddyIDX > (totalRows-1))
        {
            break;
        }
        
        NSIndexPath *buddyPath = [NSIndexPath indexPathForRow:buddyIDX inSection:indexPath.section];
        
        UICollectionViewLayoutAttributes *buddyAttributes = [super layoutAttributesForItemAtIndexPath:buddyPath];
        
        if (CGRectIntersectsRect(buddyAttributes.frame, rowTestFrame))
        {
            // If the item intersects the test frame, it's in the same row
            [rowBuddies addObject:[buddyAttributes copy]];
            buddyIDX++;
        }
        else
        {
            // Encountered next row
            break;
        }
    }
    
    id <UICollectionViewDelegateFlowLayout> flowDelegate = (id<UICollectionViewDelegateFlowLayout>) [[self collectionView] delegate];
    BOOL delegateSupportsInteritemSpacing = [flowDelegate respondsToSelector:@selector(collectionView:layout:minimumInteritemSpacingForSectionAtIndex:)];
    
    // x-x-x-x ... sum up the interim space
    CGFloat interitemSpacing = [self minimumInteritemSpacing];
    
    // Check for minimumInteritemSpacingForSectionAtIndex support
    if (delegateSupportsInteritemSpacing && rowBuddies.count > 0)
    {
        interitemSpacing = [flowDelegate collectionView:self.collectionView
                                                 layout:self
               minimumInteritemSpacingForSectionAtIndex:indexPath.section];
    }
    
    CGFloat aggregateInteritemSpacing = interitemSpacing * (rowBuddies.count -1);
    
    // Sum the width of all elements in the row
    CGFloat aggregateItemWidths = 0.f;
    for (UICollectionViewLayoutAttributes *itemAttributes in rowBuddies)
        aggregateItemWidths += CGRectGetWidth(itemAttributes.frame);
    
    // Build an alignment rect
    // |  |x-x-x-x|  |
    CGFloat alignmentWidth = aggregateItemWidths + aggregateInteritemSpacing;
    CGFloat alignmentXOffset = (collectionViewWidth - alignmentWidth) / 2.f;
    
    // Adjust each item's position to be centered
    CGRect previousFrame = CGRectZero;
    for (UICollectionViewLayoutAttributes *itemAttributes in rowBuddies)
    {
        CGRect itemFrame = itemAttributes.frame;
        
        if (CGRectEqualToRect(previousFrame, CGRectZero))
            itemFrame.origin.x = alignmentXOffset;
        else
            itemFrame.origin.x = CGRectGetMaxX(previousFrame) + interitemSpacing;
        
        itemAttributes.frame = itemFrame;
        previousFrame = itemFrame;
        
        // Finally, add it to the cache
        self.attrCache[itemAttributes.indexPath] = itemAttributes;
    }
    
    return self.attrCache[indexPath];
}

@end
