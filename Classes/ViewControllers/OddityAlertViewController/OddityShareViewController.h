//
//  OddityShareViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/13.
//
//

#import <UIKit/UIKit.h>


/**
 分享 类型

 - OddityShareStyleNewContent: 新闻详情
 - OddityShareStyleSpecial: 新闻专题
 - OddityShareStyleNew: 新闻
 */
typedef NS_ENUM(NSInteger, OddityShareStyle) {
    
    OddityShareStyleNewContent = 0,
    OddityShareStyleSpecial = 1,
    OddityShareStyleNew = 2
};

@interface OddityShareViewController : UIViewController

@property(nonatomic,strong)UIView *topBackView;
@property(nonatomic,strong)UIView *bottomBackView;


/**
 创建分享 UIViewController

 @param style OddityShareStyle
 @param obj 分享的对象
 @return 返回 VC
 */
-(instancetype)initWithStyle:(OddityShareStyle)style withObj:(id)obj;

@end


@interface OddityShareCollectionViewReusable : UICollectionReusableView

@end

@interface OddityShareCollectionViewCell : UICollectionViewCell

-(void)makeMode:(OddityShareModeObject *)object;

@end



@interface OddityCustomShareViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityCustomShareViewControllerDismissAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end
