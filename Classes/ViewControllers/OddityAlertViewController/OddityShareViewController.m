//
//  OddityShareViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/13.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import "OddityPlayerView.h"
#import <Masonry/Masonry.h>
#import "OdditySetting.h"

#import "OddityAlertViewViewController.h"

#import "OddityFontSizeViewController.h"

#import "OddityShareViewController.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

#define OddityShareTableViewViewHeight 236.0
#define OddityShareBackViewHeight OddityShareTableViewViewHeight+47.0+10.0


#define OddityShareSingleTableViewViewHeight 118.0
#define OddityShareSingleBackViewHeight OddityShareSingleTableViewViewHeight+47.0+10.0

#define OddityShareIsOnlySingleRow [OdditySetting.shareOdditySetting.shareObjectArray.firstObject isKindOfClass:OddityShareModeObject.class]

#define OdditySahreCollectionIdentifier @"shareCell"
#define OdditySahreReusableCollectionIdentifier @"shareReusable"

@protocol OddityShareUICollectionCellDelegate <NSObject>

-(void)userDidSelectShareModeObject:(OddityShareModeObject *)shareModeObject;

@end


@interface OddityShareUICollectionCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong) UICollectionView *collectionView;

@property (nonatomic,strong) NSArray<OddityShareModeObject *> *objects;

@property (weak, nonatomic) id<OddityShareUICollectionCellDelegate> delegate;

@end

@implementation OddityShareUICollectionCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 19.f;
        
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        flowLayout.sectionInset = UIEdgeInsetsMake(15, 19, 15, 19);
        _collectionView = [[UICollectionView alloc]initWithFrame:(CGRectZero) collectionViewLayout:flowLayout];
        _collectionView.showsHorizontalScrollIndicator = false;
        _collectionView.showsVerticalScrollIndicator = false;
        [self.contentView addSubview:_collectionView];
        [_collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.edges.equalTo(self.contentView).with.insets(UIEdgeInsetsZero);
        }];
        
        [self.collectionView registerClass:[OddityShareCollectionViewCell class] forCellWithReuseIdentifier:OdditySahreCollectionIdentifier];
        [self.collectionView registerClass:[OddityShareCollectionViewReusable class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:OdditySahreReusableCollectionIdentifier];
        
    }
    
    return self;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(userDidSelectShareModeObject:)]) {
        
        [self.delegate userDidSelectShareModeObject:self.objects[indexPath.row]];
    }
    

}

-(void)ReloadDataSource:(NSArray<OddityShareModeObject *> *)objects{

    self.objects = objects;
    
    _collectionView.backgroundColor = UIColor.color_t_6;
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    [_collectionView reloadData];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.objects.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    OddityShareCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:OdditySahreCollectionIdentifier forIndexPath:indexPath];
    
    [cell makeMode:self.objects[indexPath.row]];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake((oddityUIScreenWidth-19.0*5.0)/4.0, 88);
}

@end




@interface OddityShareViewController ()<UIViewControllerTransitioningDelegate,UITableViewDataSource,UITableViewDelegate,OddityShareUICollectionCellDelegate>

@property (nonatomic,assign) OddityShareStyle style;
@property (nonatomic,strong) id shareObj;

@property (nonatomic,strong) UIButton *cancelButton;

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) OddityCustomShareViewControllerPresentdAnimation *cPresentedViewControllerAnimatedTransitioning;
@property (nonatomic,strong) OddityCustomShareViewControllerDismissAnimation *cDismissViewControllerAnimatedTransitioning;

@end

@implementation OddityShareViewController


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{ 
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
}


-(instancetype)initWithStyle:(OddityShareStyle)style withObj:(id)obj{

    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        _style = style;
        _shareObj = obj;
        
        _cDismissViewControllerAnimatedTransitioning  = [[OddityCustomShareViewControllerDismissAnimation alloc]init];
        _cPresentedViewControllerAnimatedTransitioning  = [[OddityCustomShareViewControllerPresentdAnimation alloc]init];
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    return self;
}





-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _cPresentedViewControllerAnimatedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _cDismissViewControllerAnimatedTransitioning;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    _topBackView = [[UIView alloc] init];
    UITapGestureRecognizer *cancelTap = [[UITapGestureRecognizer alloc] init];
    [cancelTap addTarget:self action:@selector(cancelViewController)];
    [_topBackView addGestureRecognizer:cancelTap];
    
    _topBackView.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    [self.view addSubview:_topBackView];
    [_topBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    _bottomBackView = [[UIView alloc]init];
    _bottomBackView.backgroundColor = [UIColor color_t_6];
    [self.view addSubview:_bottomBackView];
    [_bottomBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        
        if (OddityShareIsOnlySingleRow) {
            
            make.height.equalTo(@(OddityShareSingleBackViewHeight));
        }else{
        
            make.height.equalTo(@(OddityShareBackViewHeight));
        }
    }];
    
    _cancelButton = [[UIButton alloc]init];
    [_cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    _cancelButton.titleLabel.font = [UIFont font_t_3];
    _cancelButton.backgroundColor = [UIColor color_t_5];
    [_cancelButton setTitleColor:[UIColor color_t_2] forState:(UIControlStateNormal)];
    [_bottomBackView addSubview:_cancelButton];
    [_cancelButton addTarget:self action:@selector(cancelViewController) forControlEvents:(UIControlEventTouchUpInside)];
    [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.bottom.equalTo(_bottomBackView);
        make.left.equalTo(_bottomBackView);
        make.right.equalTo(_bottomBackView);
        make.height.equalTo(@(47));
    }];
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.scrollEnabled = false;
    self.tableView.backgroundColor = UIColor.color_t_6;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[OddityShareUICollectionCell class] forCellReuseIdentifier:@"cell"];
    [self.bottomBackView addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.bottomBackView).mas_equalTo(10);
        make.left.equalTo(self.bottomBackView);
        make.right.equalTo(self.bottomBackView);
        
        make.bottom.equalTo(self.cancelButton.mas_top);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
}

-(void)refreshThemeMethod{
    
    [self.tableView reloadData];
    
    _bottomBackView.backgroundColor = [UIColor color_t_6];
    _cancelButton.backgroundColor = [UIColor color_t_5];
    [_cancelButton setTitleColor:[UIColor color_t_2] forState:(UIControlStateNormal)];
    _tableView.backgroundColor = self.bottomBackView.backgroundColor;
}


-(void)cancelViewController{

    [self dismissViewControllerAnimated:true completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (OddityShareIsOnlySingleRow) {
        
        return 1;
    }
    
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    OddityShareUICollectionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    
    if (OddityShareIsOnlySingleRow) {
        
        [cell ReloadDataSource:OdditySetting.shareOdditySetting.shareObjectArray];
    }else{
    
        [cell ReloadDataSource:OdditySetting.shareOdditySetting.shareObjectArray[indexPath.row]];
    }
    
    cell.delegate = self;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return OddityShareTableViewViewHeight/2;
}

-(void)userDidSelectShareModeObject:(OddityShareModeObject *)shareModeObject{
    
    __weak __typeof__(self) weakSelf = self;
    
    if (shareModeObject.mode == OddityShareModeTheme) {
        
        OddityUISetting.shareOddityUISetting.oddityThemeMode = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNormal ? OddityThemeModeNight : OddityThemeModeNormal;
        
        [self dismissViewControllerAnimated:true completion:nil];
        
        return;
    }
    
    if (shareModeObject.mode == OddityShareModeFontSize) {
        
        UIViewController *viewController = self.presentingViewController;
        
        [self dismissViewControllerAnimated:true completion:^{
            
            OddityFontSizeViewController *fontSize = [OddityFontSizeViewController.alloc init];
            
            [viewController presentViewController:fontSize animated:true completion:nil];
        }];
        
        return;
    }

    __block OddityShareObject *shareObject;
    
    switch (self.style) {
            
            case OddityShareStyleNewContent:{
                
                OddityNewContent *newContent = (OddityNewContent *)self.shareObj;
                
                [OddityLogObject createAppAction:OddityLogAtypeShare fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:false params:@{@"nid":@(newContent.nid)}];
                
                shareObject = [newContent getShareObject];
            }
            break;
            
            case OddityShareStyleSpecial:{
                
                OdditySpecial *special = (OdditySpecial *)self.shareObj;
                
                [OddityLogObject createAppAction:OddityLogAtypeShare fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:false params:@{@"tid":@(special.id)}];
                
                shareObject = [special getShareObject];
            }
            break;
            case OddityShareStyleNew:{
                
                OddityNew *new = (OddityNew *)self.shareObj;
                
                [OddityLogObject createAppAction:OddityLogAtypeShare fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:false params:@{@"nid":@(new.nid)}];
                
                shareObject = [new getShareObject];
            }
            break;
    }
    
    if (shareModeObject.mode == OddityShareModeCopyLink) {
        
        UIPasteboard.generalPasteboard.string = shareObject.shareURLString;
        
        UIViewController *viewController = self.presentingViewController;
        
        [self dismissViewControllerAnimated:true completion:^{
            
            OddityAlertViewViewController *alertController = [[OddityAlertViewViewController alloc] initWitch: @"复制完成"];
            
            [alertController makeChange:@"复制完成" after:1 block:^{
                
                [alertController dismissViewControllerAnimated:true completion:nil];
            }];
            
            [viewController presentViewController:alertController animated:true completion:nil];
        }];
        
        return;
    }
    
    [[PINRemoteImageManager sharedImageManager] downloadImageWithURL:[NSURL URLWithString:shareObject.shareImageURLString] completion:^(PINRemoteImageManagerResult * _Nonnull result) {
        
        shareObject = [shareObject makeImage:result.image];
    }];
    
    [self dismissViewControllerAnimated:true completion:^{
    
        if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didClickShareActionWithMode:shareObject:shareViewController:)] ){
            
            [[OdditySetting shareOdditySetting].delegate didClickShareActionWithMode:shareModeObject.mode shareObject:shareObject shareViewController:weakSelf.presentingViewController];
        }
    }];
}

@end


@interface OddityShareCollectionViewCell ()

@property(nonatomic,strong) UILabel *nameLabel;

@property(nonatomic,strong) UIImageView *iconImageView;

@end

@implementation OddityShareCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        _iconImageView = [[UIImageView alloc]init];
        [self.contentView addSubview:_iconImageView];
        [_iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.equalTo(self.contentView);
            make.centerX.mas_equalTo(self.contentView);
            make.height.mas_equalTo(66);
            make.width.mas_equalTo(66);
        }];
        
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = [UIFont font_t_7];
        _nameLabel.adjustsFontSizeToFitWidth = true;
        _nameLabel.minimumScaleFactor = 0.5;
        _nameLabel.textColor = [UIColor color_t_2];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_nameLabel];
        [_nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(self.contentView);
            make.right.equalTo(self.contentView);
            make.top.equalTo(self.iconImageView.mas_bottom).offset(10);
        }];
    }
    return self;
}

-(void)makeMode:(OddityShareModeObject *)object{

    self.nameLabel.text = object.modeIconName;
    self.iconImageView.image = object.modeIconImage;
}

@end


@interface OddityShareCollectionViewReusable()

@property(nonatomic,strong)UIView *borderView;

@end

@implementation OddityShareCollectionViewReusable

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor color_t_6];
        
        _borderView = [[UIView alloc] init];
        _borderView.backgroundColor = [UIColor color_t_6];
        [self addSubview:_borderView];
        [_borderView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(@(19));
            make.right.equalTo(@(-19));
            make.bottom.equalTo(@(0));
            make.height.equalTo(@(0.5));
        }];
    }
    return self;
}

@end


@implementation OddityCustomShareViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityShareViewController *toViewController = (OddityShareViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    toViewController.topBackView.alpha = 0;
    
    toViewController.bottomBackView.transform = CGAffineTransformTranslate(toViewController.bottomBackView.transform, 0, OddityShareBackViewHeight);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext]  animations:^{
        
        toViewController.topBackView.alpha = 1;
        
        toViewController.bottomBackView.transform = CGAffineTransformIdentity;
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
        
        if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
            
            [[[UIApplication sharedApplication]keyWindow] bringSubviewToFront:[OddityPlayerView sharePlayerView]];
        }
    }];
}

@end



@implementation OddityCustomShareViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    
    OddityShareViewController *fromViewController = (OddityShareViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        fromViewController.topBackView.alpha = 0;
        
        fromViewController.bottomBackView.transform = CGAffineTransformTranslate(fromViewController.bottomBackView.transform, 0, OddityShareBackViewHeight);
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: true];
    }];
}

@end
