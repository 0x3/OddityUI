//
//  OdditySubscribeViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/3/20.
//
//

#import <UIKit/UIKit.h>

@class OdditySubscribeObject,OdditySubscribeViewController,OdditySubscribeCollectionCell,OdditySubscribeSubmitButton;

@interface OdditySubscribeObject : NSObject

@property(nonatomic,assign)BOOL isSelect;

@property(nonatomic,strong)NSString *nameString;
@property(nonatomic,strong)UIImage *iconImage;

@property(nonatomic,strong)UIImage *buttonImage;

+(NSArray *)randomDataSourceArray;

/// 是否应该现实首订页面
+(BOOL)shouldShow;
@end

@interface OdditySubscribeViewController : UIViewController

@end

@interface OdditySubscribeCollectionCell : UICollectionViewCell

@property(nonatomic,strong)UIButton *subscribeSelectButton;

-(void)setSubscribeObject:(OdditySubscribeObject *)object;

@end


@interface OdditySubscribeSubmitButton : UIButton

-(void)selectBackColor:(BOOL)isSelect;
@end
