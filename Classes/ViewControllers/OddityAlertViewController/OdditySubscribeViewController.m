//
//  OdditySubscribeViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/3/20.
//
//
#import "OddityModels.h"
#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OdditySubscribeViewController.h"

#define OdditySubscribeCollectionMargenSpace 42.0


NSString *const SHARE_SHOW_MESSAGE = @"oddityshowmessageviewcontroller";


@interface OdditySubscribeViewController ()<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)UICollectionView *contentView;
@property(nonatomic,strong)OdditySubscribeSubmitButton *subButton;

@property(nonatomic,assign)UIStatusBarStyle style;

@property(nonatomic,strong)NSArray *dataSource;

@end

@implementation OdditySubscribeViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    self.style = [[UIApplication sharedApplication] statusBarStyle];
    
    [[UIApplication sharedApplication]setStatusBarStyle:(UIStatusBarStyleDefault)];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication]setStatusBarStyle:self.style];
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (OdditySubscribeObject *object in [_dataSource filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isSelect == true"]]) {
        
        [array addObject:object.nameString];
    }
    
    if (array.count > 0) {
    
        [OddityShareUser subscribe:array];
    }
    
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:SHARE_SHOW_MESSAGE];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor color_t_6];
    
    _titleLabel = [[UILabel alloc]init];
    _titleLabel.text = @"根据你的爱好为你推荐";
    _titleLabel.font = [UIFont systemFontOfSize:20];
    _titleLabel.textColor = [UIColor color_t_2];
    [self.view addSubview:_titleLabel];
    
    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo(self.view).offset(67);
        make.centerX.equalTo(self.view);
    }];
    
    
    _subButton = [[OdditySubscribeSubmitButton alloc] init];
    [self.view addSubview:_subButton];
    
    [_subButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.height.equalTo(@54);
    }];
    
    [_subButton oddity_handleControlEvent:(UIControlEventTouchUpInside) withBlock:^{
       
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    
    _dataSource =  [OdditySubscribeObject randomDataSourceArray];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 35, 0, 35);
    _contentView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _contentView.dataSource = self;
    _contentView.delegate = self;
    _contentView.backgroundColor = [UIColor color_t_6];
    [_contentView registerClass:[OdditySubscribeCollectionCell class] forCellWithReuseIdentifier:@"cell"];
    [self.view addSubview:_contentView];
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(@0);
        make.right.equalTo(@0);
        make.bottom.equalTo(self.subButton.mas_top);
        make.top.equalTo(self.titleLabel.mas_bottom);
    }];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return _dataSource.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    OdditySubscribeObject *object = _dataSource[indexPath.row];
    
    OdditySubscribeCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    [cell setSubscribeObject:object];
    
    [cell.subscribeSelectButton oddity_handleControlEvent:(UIControlEventTouchUpInside) withBlock:^{
       
        [self setSelectFor:object reloadBy:indexPath];
    }];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(55, 129);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    return UIEdgeInsetsMake(34, OdditySubscribeCollectionMargenSpace, 55, OdditySubscribeCollectionMargenSpace);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{

    return 39;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{

    CGFloat width = (oddityUIScreenWidth-2*OdditySubscribeCollectionMargenSpace-3*55)/2;
    
    return width;
}


-(void)setSelectFor:(OdditySubscribeObject *)object reloadBy:(NSIndexPath *)indexPath{

    object.isSelect = !object.isSelect;
    
    [self.contentView reloadItemsAtIndexPaths:@[indexPath]];
    
    BOOL isselect = [_dataSource filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isSelect == true"]].count > 0;
    
    [self.subButton selectBackColor:isselect];
}






@end



@interface OdditySubscribeCollectionCell ()

@property(nonatomic,strong)UILabel *subscribeICONnameLabel;
@property(nonatomic,strong)UIImageView *subscribeICONImageView;

@end

@implementation OdditySubscribeCollectionCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor color_t_6];
        
        _subscribeICONImageView = [[UIImageView alloc]init];
        [self.contentView addSubview:_subscribeICONImageView];
        
        [_subscribeICONImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.top.equalTo(@0);
            make.right.equalTo(@0);
            make.left.equalTo(@0);
            make.height.equalTo(@55);
        }];
        
        
        _subscribeICONnameLabel = [[UILabel alloc]init];
        _subscribeICONnameLabel.numberOfLines = 0;
        _subscribeICONnameLabel.textAlignment = NSTextAlignmentCenter;
        _subscribeICONnameLabel.textColor = [UIColor color_t_2];
        _subscribeICONnameLabel.font = [UIFont font_t_7];
        [self.contentView addSubview:_subscribeICONnameLabel];
        [_subscribeICONnameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.top.equalTo(_subscribeICONImageView.mas_bottom).offset(11);
        }];
        
        _subscribeSelectButton = [[UIButton alloc]init];
        [self.contentView addSubview:_subscribeSelectButton];
        [_subscribeSelectButton mas_remakeConstraints:^(MASConstraintMaker *make) {
           
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.bottom.equalTo(@0);
            make.height.equalTo(@19);
        }];
    }
    return self;
}

-(void)setSubscribeObject:(OdditySubscribeObject *)object{

    self.subscribeICONnameLabel.text = object.nameString;
    self.subscribeICONImageView.image = object.iconImage;
    [self.subscribeSelectButton setImage:object.buttonImage forState:(UIControlStateNormal)];
}

@end




@interface OdditySubscribeSubmitButton ()

@property(nonatomic,strong)UIColor *normalColor;
@property(nonatomic,strong)UIColor *selectColor;

@end

@implementation OdditySubscribeSubmitButton

-(UIColor *)selectColor{

    return [[UIColor color_t_1] colorWithAlphaComponent:0.8];
}

-(UIColor *)normalColor{

    return [[UIColor color_t_4] colorWithAlphaComponent:0.8];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = self.normalColor;
        [self setTitle:@"开始体验" forState:(UIControlStateNormal)];
        [self setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    }
    return self;
}

-(void)selectBackColor:(BOOL)isSelect{

    [UIView animateWithDuration:0.3 animations:^{
        
        self.backgroundColor = isSelect ? self.selectColor : self.normalColor;
    }];
}

@end


@implementation OdditySubscribeObject

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        
        _nameString = name;
    }
    return self;
}

-(UIImage *)iconImage{
    
    return [UIImage oddityImage:self.nameString];
}

-(UIImage *)buttonImage{
    
    return self.isSelect ? [UIImage oddityImage:@"对勾"] : [UIImage oddityImage:@"加号"];
}


+(BOOL)shouldShow{

    return ![[NSUserDefaults standardUserDefaults] boolForKey:SHARE_SHOW_MESSAGE];
}


//获取一个随机整数，范围在[from,to），包括from，包括to
+(int)getRandomNumber:(int)from to:(int)to{
    
    return (int)(from + (arc4random() % (to - from + 1)));
}


+(NSArray *)randomDataSourceArray{
    
    /// 随机集合
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *allArray = [[self getDataSourceArray] mutableCopy];
    NSMutableArray *resultsArray = [NSMutableArray array];
    
    int index = 9;
    
    while (index--) {
        
        int number = 0;
        
        do {
            number = [self getRandomNumber:0 to:53];
        } while ([array containsObject:@(number)]);
        
        [array addObject:@(number)];
        
        [resultsArray addObject:[allArray objectAtIndex:number]];
    }
    
    return resultsArray;
}

+(NSArray *)getDataSourceArray{
    
    NSMutableArray *array = [NSMutableArray array];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"36氪"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"别致"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"界面"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"快科技"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"内涵段子"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"果壳精选"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"财新周刊"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"三联生活周刊"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"AppSo"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"半糖"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"虎嗅"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"悦食家"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"和讯财经"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"毒舌指南"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"面包猎人"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"企鹅吃喝指南"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"HTO男人"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"好物"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"欢喜Fancy"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"爱范儿"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"国家地理"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"糗事百科"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"华尔街见闻"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"北美省钱快报"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"IT之家"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"威锋"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"任玩堂"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"钛媒体"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"悦食中国"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"网易财经"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"去哪儿旅游"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"太平洋电脑网"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"ZEALER"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"机核"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"体育疯"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"雷锋网"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"数字尾巴"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"节操精选"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"土巴兔装修"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"马蜂窝自由行"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"v 电影"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"果库"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"好好住"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"一人一城"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"无讼阅读"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"豆瓣一刻"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"手游那点事"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"一个"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"煎蛋"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"少数派"]];
    
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"严肃八卦"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"极客头条"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"豆瓣东西"]];
    [array addObject:[[OdditySubscribeObject alloc]initWithName:@"第一财经周"]];
    
    return array;
}

@end
