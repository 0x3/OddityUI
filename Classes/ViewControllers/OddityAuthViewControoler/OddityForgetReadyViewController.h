//
//  OddityForgetReadyViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import "OddityCustomTransitioning.h"

@interface OddityForgetReadyViewController : OddityCustomAnimationViewController

-(instancetype)initWithEmailString:(NSString *)email;
@end
