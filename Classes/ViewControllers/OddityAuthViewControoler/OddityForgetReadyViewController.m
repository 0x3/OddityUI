//
//  OddityForgetReadyViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/14.
//
//

#import "OddityAlertViewViewController.h"
#import "OddityShareUser.h"

#import "OddityInsetFiled.h"

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>
#import "OddityForgetReadyViewController.h"
#import "OddityForgetResultsViewController.h"

@interface OddityForgetReadyViewController ()

@property(nonatomic,strong)NSString *emailString;

@property(nonatomic,strong)UIView *contentView;
@property(nonatomic,strong)UIScrollView *scrollerView;

@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;

@property(nonatomic,strong)UIButton *submitButton;
@property(nonatomic,strong)OddityInsetFiled *textFiled;
@end

@implementation OddityForgetReadyViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(instancetype)initWithEmailString:(NSString *)email{

    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        
        _emailString = email;
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout]; // 布局方法
}

-(void)layout{
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"重置密码" titleFont:[UIFont font_t_3] block:^{
        
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _scrollerView = [[UIScrollView alloc] init];
    _scrollerView.alwaysBounceVertical = true;
    self.scrollView = _scrollerView;
    [self.view addSubview:_scrollerView];
    [_scrollerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_naviGationBar.mas_bottom);
        make.left.with.right.bottom.mas_equalTo(self.view);
    }];
    
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = [UIColor redColor];
    [self.scrollerView addSubview:_contentView];
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsZero);
        make.size.mas_equalTo(_scrollerView);
    }];
    
    _textFiled = [[OddityInsetFiled alloc] initTextLeft:12 editingLeft:12 placeholderLeft:12];
    _textFiled.placeholder = @"电子邮箱";
    _textFiled.text = self.emailString;
    _textFiled.layer.borderColor = UIColor.color_t_5.CGColor;
    _textFiled.layer.borderWidth = 0.5;
    _textFiled.layer.cornerRadius = 2;
    _textFiled.font = UIFont.font_t_4;
    _textFiled.textColor = UIColor.color_t_2;
    [self.contentView addSubview:_textFiled];
    [_textFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(20);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(44);
    }];
    
    _submitButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
    [self.contentView addSubview:_submitButton];
    _submitButton.layer.cornerRadius = 2;
    _submitButton.clipsToBounds = true;
    _submitButton.alpha = 0.8;
    _submitButton.enabled = false;
    _submitButton.titleLabel.font = UIFont.font_t_3;
    [_submitButton setTitleColor:UIColor.color_t_10 forState:(UIControlStateNormal)];
    [_submitButton setTitle:@"下一步" forState:(UIControlStateNormal)];
    [_submitButton addTarget:self action:@selector(resetPasswordMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:[UIColor color_t_4] sizes:(CGSizeMake(1, 1))] forState:(UIControlStateDisabled)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:OddityUISetting.shareOddityUISetting.mainTone sizes:(CGSizeMake(1, 1))] forState:(UIControlStateNormal)];
    [_submitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_textFiled.mas_bottom).offset(20);
        make.left.with.right.mas_equalTo(_textFiled);
        make.height.mas_equalTo(44);
    }];
    
    [_textFiled addTarget:self action:@selector(textDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    
    [self makeLayoutDecouplingMethod];
}

-(void)resetPasswordMethod{

    OddityAlertViewViewController *alertController = [[OddityAlertViewViewController alloc] initWitch:@"正在发送重置密码邮箱"];
    
    __weak typeof(self) weakSelf = self;
    [self presentViewController:alertController animated:true completion:^{
       
        [OddityShareUser.defaultUser resetPasswordMethod:weakSelf.textFiled.text complete:^(BOOL success, NSString *mess) {
            
            [alertController makeChange:mess after:1 block:^{
               
                [weakSelf dismissViewControllerAnimated:true completion:^{
                    
                    OddityForgetResultsViewController *viewController = [[OddityForgetResultsViewController alloc] initWithEmailString:weakSelf.textFiled.text];
                    
                    [weakSelf presentViewController:viewController animated:true completion:nil];
                }];
            }];

        }];
    }];
}

-(void)textDidChange:(UITextField *)textFiled{
    
    self.submitButton.enabled = _textFiled.text.isValidEmail;
}

-(void)makeLayoutDecouplingMethod{
    
    _textFiled.backgroundColor = UIColor.color_t_9;
    
    self.contentView.backgroundColor = [UIColor color_t_6];
    self.scrollerView.backgroundColor = [UIColor color_t_6];
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    
}

@end
