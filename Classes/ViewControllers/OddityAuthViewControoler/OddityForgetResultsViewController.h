//
//  OddityForgetResultsViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/6/19.
//
//

#import "OddityCustomAnimationViewController.h"

@interface OddityForgetResultsViewController : UIViewController

-(instancetype)initWithEmailString:(NSString *)email;
@end
