//
//  OddityForgetResultsViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/19.
//
//

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>
#import "OddityForgetResultsViewController.h"

@interface OddityForgetResultsViewController ()

@property(nonatomic,strong)NSString *emailString;

@property(nonatomic,strong)UIView *contentView;
@property(nonatomic,strong)UIScrollView *scrollerView;

@property(nonatomic,strong)UIButton *submitButton;

@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;

@property(nonatomic,strong)UILabel *descLabel;
@property(nonatomic,strong)UILabel *emailLabel;
@property(nonatomic,strong)UIView *emailBackView;

@end

@implementation OddityForgetResultsViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}


-(instancetype)initWithEmailString:(NSString *)email{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        
        _emailString = email;
    }
    
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout]; // 布局方法
}

-(void)layout{
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"重置密码" titleFont:[UIFont font_t_3] block:^{
        
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    
    _naviGationBar.cancelButton.hidden = true;
    
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _scrollerView = [[UIScrollView alloc] init];
    _scrollerView.alwaysBounceVertical = true;
    [self.view addSubview:_scrollerView];
    [_scrollerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_naviGationBar.mas_bottom);
        make.left.with.right.bottom.mas_equalTo(self.view);
    }];
    
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = [UIColor redColor];
    [self.scrollerView addSubview:_contentView];
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsZero);
        make.size.mas_equalTo(_scrollerView);
    }];
    
    _descLabel = [[UILabel alloc] init];
    _descLabel.textColor = UIColor.color_t_3;
    _descLabel.font = UIFont.font_t_4;
    _descLabel.text = @"已向您的邮箱发送了含有新密码的邮件";
    [self.contentView addSubview:_descLabel];
    [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_naviGationBar.mas_bottom).offset(43);
        make.centerX.mas_equalTo(self.view);
    }];
    
    _emailBackView = [[UIView alloc] init];
    _emailBackView.backgroundColor = UIColor.color_t_5;
    [self.contentView addSubview:_emailBackView];
    [_emailBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_descLabel.mas_bottom).offset(20);
        make.centerX.mas_equalTo(self.view);
    }];
    
    _emailLabel = [[UILabel alloc] init];
    _emailLabel.textColor = UIColor.color_t_3;
    _emailLabel.font = UIFont.font_t_4;
    _emailLabel.text = self.emailString;
    [self.emailBackView addSubview:_emailLabel];
    [_emailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.center.mas_equalTo(_emailBackView);
        make.top.mas_equalTo(10);
        make.left.mas_equalTo(20);
        make.right.mas_equalTo(-20);
        make.bottom.mas_equalTo(-10);
    }];
    
    _submitButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
    [self.contentView addSubview:_submitButton];
    _submitButton.layer.cornerRadius = 2;
    _submitButton.clipsToBounds = true;
    _submitButton.alpha = 0.8;
    _submitButton.titleLabel.font = UIFont.font_t_3;
    [_submitButton setTitleColor:UIColor.color_t_10 forState:(UIControlStateNormal)];
    [_submitButton setTitle:@"确认收到邮件" forState:(UIControlStateNormal)];
    [_submitButton addTarget:self action:@selector(resetPasswordMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:[UIColor color_t_4] sizes:(CGSizeMake(1, 1))] forState:(UIControlStateDisabled)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:OddityUISetting.shareOddityUISetting.mainTone sizes:(CGSizeMake(1, 1))] forState:(UIControlStateNormal)];
    [_submitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_emailBackView.mas_bottom).offset(62);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(44);
    }];
    
    [self makeLayoutDecouplingMethod];
}


-(void)makeLayoutDecouplingMethod{
    
    self.contentView.backgroundColor = [UIColor color_t_6];
    self.scrollerView.backgroundColor = [UIColor color_t_6];
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    
}

-(void)resetPasswordMethod{

    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:true completion:nil];
}

@end
