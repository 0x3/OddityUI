//
//  OddityUserLoginViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/12.
//
//

#import "OddityLogObject.h"

#import "OddityAlertViewViewController.h"

#import "OddityShareUser.h"

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>

#import "OddityForgetReadyViewController.h"

#import "OddityUserLoginViewController.h"

#import "OddityUserRegisterViewController.h"

@interface OddityUserLoginViewController ()<UITextFieldDelegate>


@property(nonatomic,strong)UIView *contentView;
@property(nonatomic,strong)UIScrollView *scrollerView;


@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;

// 输入框 问题

@property(nonatomic,strong)OdditySearchTextField *userNameInputTextFiled;
@property(nonatomic,strong)OdditySearchTextField *passWordInputTextFiled;
@property(nonatomic,strong)UIView *usernameBorderView;
@property(nonatomic,strong)UIView *passwordBorderView;

@property(nonatomic,strong)UIButton *submitButton;

// 忘记密码 和 立即注册
@property(nonatomic,strong)OddityBigButton *forgotButton;
@property(nonatomic,strong)OddityBigButton *registButton;


@property(nonatomic,strong)UILabel *descLabel;

@property(nonatomic,strong)UIView *leftView;
@property(nonatomic,strong)UIView *rightView;

@property(nonatomic,strong)UIView *leftBorderView;
@property(nonatomic,strong)UIView *rightBorderView;
@property(nonatomic,strong)UIView *middleBorderView;

@property(nonatomic,strong)UIButton *sinaButton;
@property(nonatomic,strong)UIButton *weChatButton;

@property(nonatomic,strong)UITapGestureRecognizer *tap;


@property(nonatomic,strong)OddityAlertViewViewController *alertViewController;


@property(nonatomic,strong)NSString *loginType;

@end

@implementation OddityUserLoginViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout]; // 布局方法
}

-(void)layout{
    
    __weak typeof(self) weakSelf = self;
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"登录" titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _scrollerView = [[UIScrollView alloc] init];
    _scrollerView.alwaysBounceVertical = true;
    [self.view addSubview:_scrollerView];
    [_scrollerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_naviGationBar.mas_bottom);
        make.left.with.right.bottom.mas_equalTo(self.view);
    }];
    
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = [UIColor redColor];
    [self.scrollerView addSubview:_contentView];
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsZero);
        make.size.mas_equalTo(_scrollerView);
    }];
    
    
    
    _userNameInputTextFiled = [[OdditySearchTextField alloc] init];
    _userNameInputTextFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
    _userNameInputTextFiled.attributedPlaceholder = [self attributedPlaceholder:@"邮箱账号/用户名"];
    _userNameInputTextFiled.font = [UIFont font_t_4];

    [self.contentView addSubview:_userNameInputTextFiled];
    [_userNameInputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(41);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(29);
    }];
    
    _usernameBorderView = [[UIView alloc] init];
    [self.contentView addSubview:_usernameBorderView];
    [_usernameBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_userNameInputTextFiled.mas_bottom);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(0.5);
    }];
    
    _passWordInputTextFiled = [[OdditySearchTextField alloc] init];
    _passWordInputTextFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
    _passWordInputTextFiled.attributedPlaceholder = [self attributedPlaceholder:@"密码"];
    
    _passWordInputTextFiled.font = [UIFont font_t_4];
    _passWordInputTextFiled.secureTextEntry = true;
    
    [self.contentView addSubview:_passWordInputTextFiled];
    [_passWordInputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_usernameBorderView.mas_bottom).mas_offset(41);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(29);
    }];
    
    _passwordBorderView = [[UIView alloc] init];
    [self.contentView addSubview:_passwordBorderView];
    [_passwordBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_passWordInputTextFiled.mas_bottom);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(0.5);
    }];
    
    
    _submitButton = [[UIButton alloc] init];
    _submitButton.enabled = false;
    _submitButton.layer.cornerRadius = 2;
    _submitButton.clipsToBounds = true;
    [_submitButton setTitle:@"进入" forState:(UIControlStateNormal)];
    [_submitButton setTitleColor:[UIColor color_t_10] forState:(UIControlStateNormal)];
    _submitButton.titleLabel.font = [UIFont font_t_3];
    [_submitButton addTarget:self action:@selector(localLogin) forControlEvents:(UIControlEventTouchUpInside)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:[UIColor color_t_4] sizes:(CGSizeMake(1, 1))] forState:(UIControlStateDisabled)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:OddityUISetting.shareOddityUISetting.mainTone sizes:(CGSizeMake(1, 1))] forState:(UIControlStateNormal)];
    
    [self.contentView addSubview:_submitButton];
    [_submitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_passwordBorderView.mas_bottom).offset(32);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(44);
    }];
    
    
    _forgotButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
    _forgotButton.titleLabel.font = [UIFont font_t_6];
    [_forgotButton setTitle:@"忘记密码" forState:(UIControlStateNormal)];
    [_forgotButton setTitleColor:[UIColor color_t_3] forState:(UIControlStateNormal)];
    [_forgotButton addTarget:self action:@selector(forgetPassword) forControlEvents:(UIControlEventTouchUpInside)];
    [self.contentView addSubview:_forgotButton];
    [_forgotButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(12);
        make.top.mas_equalTo(_submitButton.mas_bottom).offset(8);
    }];
    
    
    _registButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
    _registButton.titleLabel.font = [UIFont font_t_6];
    [_registButton setTitle:@"立即注册" forState:(UIControlStateNormal)];
    [_registButton setTitleColor:[UIColor color_t_3] forState:(UIControlStateNormal)];
    [_registButton addTarget:self action:@selector(registUserMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [self.contentView addSubview:_registButton];
    [_registButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(-12);
        make.top.mas_equalTo(_submitButton.mas_bottom).offset(8);
    }];
    
    _leftView = [[UIView alloc] init];
    [self.contentView addSubview:_leftView];
    
    _rightView = [[UIView alloc] init];
    [self.contentView addSubview:_rightView];
    
    
    _middleBorderView = [[UIView alloc] init];
    [self.contentView addSubview:_middleBorderView];
    
    _sinaButton = [[UIButton alloc] init];
    [self.contentView addSubview:_sinaButton];
    
    _weChatButton = [[UIButton alloc] init];
    [self.contentView addSubview:_weChatButton];
    
    _leftBorderView = [[UIView alloc] init];
    [self.contentView addSubview:_leftBorderView];

    _rightBorderView = [[UIView alloc] init];
    [self.contentView addSubview:_rightBorderView];
    
    _descLabel = [[UILabel alloc] init];
    [self.contentView addSubview:_descLabel];
    
    [_leftView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(self.contentView.mas_width).multipliedBy((CGFloat)1/2);
    }];
    
    
    [_rightView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.height.mas_equalTo(0.5);
        make.bottom.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.width.mas_equalTo(self.contentView.mas_width).multipliedBy((CGFloat)1/2);
    }];
    
    
    _sinaButton.titleLabel.font = [UIFont font_t_6];
    _weChatButton.titleLabel.font = [UIFont font_t_6];

    _weChatButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
    [_weChatButton setImage:[UIImage oddity_auth_wechat_image] forState:(UIControlStateNormal)];
    [_weChatButton setTitle:@"微信登录" forState:(UIControlStateNormal)];
    [_weChatButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(_leftView);
        make.bottom.mas_equalTo(_rightView.mas_top).offset(-50);
    }];
    
    _sinaButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
    [_sinaButton setImage:[UIImage oddity_auth_sina_image] forState:(UIControlStateNormal)];
    [_sinaButton setTitle:@"微博登录" forState:(UIControlStateNormal)];
    [_sinaButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.centerX.mas_equalTo(_rightView);
        make.bottom.mas_equalTo(_rightView.mas_top).offset(-50);
    }];
    
    [_middleBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.width.mas_equalTo(0.5);
        make.height.mas_equalTo(22);
        make.centerY.mas_equalTo(_sinaButton);
        make.centerX.mas_equalTo(self.contentView);
    }];
    

    _descLabel.font = [UIFont font_t_6];
    _descLabel.text = @"其他登录方式";
    [_descLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(_sinaButton.mas_top).offset(-31);
    }];
    

    [_leftBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(12);
        make.height.mas_equalTo(0.5);
        make.centerY.mas_equalTo(_descLabel);
        make.right.mas_equalTo(_descLabel.mas_left).offset(-10);
    }];
    
    [_rightBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(12);
        make.height.mas_equalTo(0.5);
        make.centerY.mas_equalTo(_descLabel);
        make.left.mas_equalTo(_descLabel.mas_right).offset(10);
    }];

    
    _tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topDismissKeyBoard)];
    [self.contentView addGestureRecognizer:_tap];
    
    
    
    [_weChatButton addTarget:self action:@selector(weChatClick) forControlEvents:(UIControlEventTouchUpInside)];
    [_sinaButton addTarget:self action:@selector(sinaClick) forControlEvents:(UIControlEventTouchUpInside)];
    
    [_userNameInputTextFiled addTarget:self action:@selector(textDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    [_passWordInputTextFiled addTarget:self action:@selector(textDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    
    [self makeLayoutDecouplingMethod];
    
    self.scrollerView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(registUserStart) name:Oddity_Register_User_start_Notifition_Name_String object:nil];
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(registUserSuccess) name:Oddity_Register_User_success_Notifition_Name_String object:nil];
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(registUserFail) name:Oddity_Register_User_fail_Notifition_Name_String object:nil];
}

-(void)registUserStart{

    self.alertViewController = [[OddityAlertViewViewController alloc] initWitch:@"注册中"];
    
    [self presentViewController:self.alertViewController animated:true completion:nil];
}

-(void)registUserSuccess{
    
    if (self.alertViewController) {
        
        __weak typeof(self) weakSelf = self;
        [self.alertViewController makeChange:@"注册成功" after:1 block:^{
           
            [OddityLogObject createAppAction:OddityLogAtypeLogin fromPageName:OddityLogPageLoginPage toPageName:OddityLogPageNullPage effective:false params:@{@"type":self.loginType}];
            
            [weakSelf.presentingViewController dismissViewControllerAnimated:true completion:nil];
        }];
    }
}

-(void)registUserFail{
    
    __weak typeof(self) weakSelf = self;
    [self.alertViewController makeChange:@"注册失败" after:1 block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
}

-(void)localLogin{

    OddityAlertViewViewController *alertController = [[OddityAlertViewViewController alloc] initWitch:@"登陆中"];
    
    [self topDismissKeyBoard];
    
    __weak typeof(self) weakSelf = self;
    [self presentViewController: alertController animated:true completion:^{
       
        [OddityShareUser.defaultUser localLogin:weakSelf.userNameInputTextFiled.text password:weakSelf.passWordInputTextFiled.text complete:^(BOOL success, NSString *mess) {
           
            [alertController makeChange:mess after:1 block:^{
               
                [weakSelf dismissViewControllerAnimated:true completion:^{
                    
                    if (success) {
                        
                        [OddityLogObject createAppAction:OddityLogAtypeLogin fromPageName:OddityLogPageLoginPage toPageName:OddityLogPageNullPage effective:false params:@{@"type":@"local"}];
                        
                        [weakSelf dismissViewControllerAnimated:true completion:nil];
                    }
                }];
            }];
        }];
    }];
}

-(void)forgetPassword{
    
    OddityForgetReadyViewController *viewController = [[OddityForgetReadyViewController alloc] initWithEmailString: self.userNameInputTextFiled.text.isValidEmail ? self.userNameInputTextFiled.text : @"" ];
    
    [self presentViewController:viewController animated:true completion:nil];
}

-(void)registUserMethod{
    
    OddityUserRegisterViewController *viewController = [[OddityUserRegisterViewController alloc] init];
    
    [self presentViewController:viewController animated:true completion:nil];
}

-(void)makeLayoutDecouplingMethod{
    
    self.descLabel.textColor = [UIColor color_t_3];
    self.leftBorderView.backgroundColor = [UIColor color_t_5];
    self.rightBorderView.backgroundColor = [UIColor color_t_5];
    self.middleBorderView.backgroundColor = [UIColor color_t_5];
    
    [self.weChatButton setTitleColor:[UIColor color_t_3] forState:(UIControlStateNormal)];
    [self.sinaButton setTitleColor:[UIColor color_t_3] forState:(UIControlStateNormal)];
    
    self.userNameInputTextFiled.textColor = [UIColor color_t_2];
    self.passWordInputTextFiled.textColor = [UIColor color_t_2];
    
    self.usernameBorderView.backgroundColor = [UIColor color_t_5];
    self.passwordBorderView.backgroundColor = [UIColor color_t_5];
    self.contentView.backgroundColor = [UIColor color_t_6];
    self.scrollerView.backgroundColor = [UIColor color_t_6];
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    UIImageView * passwordICONImageView = [[UIImageView alloc] initWithImage:[UIImage oddity_auth_password_image]];
    _passWordInputTextFiled.leftView = passwordICONImageView;
    _passWordInputTextFiled.leftViewMode = UITextFieldViewModeAlways;
    _passWordInputTextFiled.edgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
    
    
    UIImageView * iconImageView = [[UIImageView alloc] initWithImage:[UIImage oddity_auth_username_image]];
    _userNameInputTextFiled.leftView = iconImageView;
    _userNameInputTextFiled.leftViewMode = UITextFieldViewModeAlways;
    _userNameInputTextFiled.edgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
}

-(NSAttributedString *)attributedPlaceholder:(NSString *)string {

    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_4],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: string attributes:attris];
    
    return attriString;
}

-(void)topDismissKeyBoard{

    [self.passWordInputTextFiled resignFirstResponder];
    [self.userNameInputTextFiled resignFirstResponder];
}

-(void)textDidChange:(UITextField *)textFiled{
    
    self.submitButton.enabled = _userNameInputTextFiled.text.isValidEmail && _passWordInputTextFiled.text.length >= 6;
}

-(void)weChatClick{
    
    self.loginType = @"wechat";
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didClickThirdSignActionWithMode:)] ){
        
        [[OdditySetting shareOdditySetting].delegate didClickThirdSignActionWithMode:(OddityShareModeWeChatFriends)];
    }
}


-(void)sinaClick{
    
    self.loginType = @"weibo";
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didClickThirdSignActionWithMode:)] ){
        
        [[OdditySetting shareOdditySetting].delegate didClickThirdSignActionWithMode:(OddityShareModeSina)];
    }
}
@end
