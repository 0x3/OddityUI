//
//  OddityUserRegisterViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/19.
//
//

#import "OddityShareUser.h"

#import "OddityInsetFiled.h"

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>
#import "OddityUserRegisterViewController.h"

#import "OddityAlertViewViewController.h"

@interface OddityUserRegisterViewController ()

@property(nonatomic,strong)UIView *userNameInputBorderView;
@property(nonatomic,strong)UIView *emailStrInputBorderView;
@property(nonatomic,strong)UIView *passWordInputBorderView;


@property(nonatomic,strong)OdditySearchTextField *userNameInputTextFiled;
@property(nonatomic,strong)OdditySearchTextField *emailStrInputTextFiled;
@property(nonatomic,strong)OdditySearchTextField *passWordInputTextFiled;

@property(nonatomic,strong)UIButton *submitButton;

@property(nonatomic,strong)UIView *contentView;
@property(nonatomic,strong)UIScrollView *scrollerView;

@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;
@end

@implementation OddityUserRegisterViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout]; // 布局方法
}

-(void)layout{
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"注册" titleFont:[UIFont font_t_3] block:^{
        
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _scrollerView = [[UIScrollView alloc] init];
    _scrollerView.alwaysBounceVertical = true;
    self.scrollView = _scrollerView;
    self.scrollerView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:_scrollerView];
    [_scrollerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_naviGationBar.mas_bottom);
        make.left.with.right.bottom.mas_equalTo(self.view);
    }];
    
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = [UIColor redColor];
    [self.scrollerView addSubview:_contentView];
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsZero);
        make.size.mas_equalTo(_scrollerView);
    }];
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(topDismissKeyBoard)]];
    
    _userNameInputTextFiled = [self makeTextInputViewMethod:@"用户名（5～20个字符，由字母和数字组成）"];
    _emailStrInputTextFiled = [self makeTextInputViewMethod:@"电子邮箱"];
    _passWordInputTextFiled = [self makeTextInputViewMethod:@"密码（5～20）"];
    
    _userNameInputBorderView = [[UIView alloc] init];
    _emailStrInputBorderView = [[UIView alloc] init];
    _passWordInputBorderView = [[UIView alloc] init];
    
    [self.contentView addSubview:_userNameInputTextFiled];
    [self.contentView addSubview:_emailStrInputTextFiled];
    [self.contentView addSubview:_passWordInputTextFiled];
    
    [self.contentView addSubview:_userNameInputBorderView];
    [self.contentView addSubview:_emailStrInputBorderView];
    [self.contentView addSubview:_passWordInputBorderView];

    [_userNameInputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(41);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(29);
    }];
    
    [_userNameInputBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_userNameInputTextFiled.mas_bottom);
        make.left.right.mas_equalTo(_userNameInputTextFiled);
        make.height.mas_equalTo(0.5);
    }];
    
    [_emailStrInputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_userNameInputBorderView.mas_bottom).offset(41);
        make.left.right.height.mas_equalTo(_userNameInputTextFiled);
    }];
    
    [_emailStrInputBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_emailStrInputTextFiled.mas_bottom);
        make.left.right.mas_equalTo(_userNameInputTextFiled);
        make.height.mas_equalTo(0.5);
    }];
    
    
    [_passWordInputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_emailStrInputTextFiled.mas_bottom).offset(41);
        make.left.right.height.mas_equalTo(_userNameInputTextFiled);
    }];
    
    [_passWordInputBorderView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_passWordInputTextFiled.mas_bottom);
        make.left.right.mas_equalTo(_userNameInputTextFiled);
        make.height.mas_equalTo(0.5);
    }];
    
    
    _submitButton = [[UIButton alloc] init];
    _submitButton.enabled = false;
    _submitButton.layer.cornerRadius = 2;
    _submitButton.clipsToBounds = true;
    [_submitButton setTitle:@"注册" forState:(UIControlStateNormal)];
    [_submitButton setTitleColor:[UIColor color_t_10] forState:(UIControlStateNormal)];
    _submitButton.titleLabel.font = [UIFont font_t_3];
    [_submitButton addTarget:self action:@selector(submitActionMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:[UIColor color_t_4] sizes:(CGSizeMake(1, 1))] forState:(UIControlStateDisabled)];
    [_submitButton setBackgroundImage:[UIImage oddity_createImage:OddityUISetting.shareOddityUISetting.mainTone sizes:(CGSizeMake(1, 1))] forState:(UIControlStateNormal)];
    
    [self.contentView addSubview:_submitButton];
    [_submitButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(_passWordInputBorderView.mas_bottom).offset(32);
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.height.mas_equalTo(44);
    }];
    
    
    [_userNameInputTextFiled addTarget:self action:@selector(textDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    [_emailStrInputTextFiled addTarget:self action:@selector(textDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    [_passWordInputTextFiled addTarget:self action:@selector(textDidChange:) forControlEvents:(UIControlEventEditingChanged)];
    
    [self makeLayoutDecouplingMethod];
}

-(void)submitActionMethod{

    [self topDismissKeyBoard];
    
    OddityAlertViewViewController *alertController = [[OddityAlertViewViewController alloc] initWitch:@"注册中"];
    
    __weak typeof(self) weakSelf = self;
    [self presentViewController:alertController animated:true completion:^{
        
        [OddityShareUser.defaultUser localRegisterMethod:_userNameInputTextFiled.text password:_passWordInputTextFiled.text email:_emailStrInputTextFiled.text complete:^(BOOL success, NSString *mess) {
            
            [alertController makeChange:mess after:1 block:^{
                
                [alertController dismissViewControllerAnimated:true completion:^{
                    
                    if (success) {
                        
                        [weakSelf.presentingViewController.presentingViewController dismissViewControllerAnimated:true completion:nil];
                    }
                }];
            }];
        }];
    }];
}

-(void)textDidChange:(UITextField *)textFiled{
    
    BOOL usernameEnble =  _userNameInputTextFiled.text.length >= 5 && _userNameInputTextFiled.text.length <= 20;
    BOOL passwordEnble =  _passWordInputTextFiled.text.length >= 5 && _passWordInputTextFiled.text.length <= 20;
    
    self.submitButton.enabled = _emailStrInputTextFiled.text.isValidEmail && usernameEnble && passwordEnble;
}

-(void)topDismissKeyBoard{
    
    [self.emailStrInputTextFiled resignFirstResponder];
    [self.passWordInputTextFiled resignFirstResponder];
    [self.userNameInputTextFiled resignFirstResponder];
}

-(void)makeLayoutDecouplingMethod{
    
    self.userNameInputBorderView.backgroundColor = [UIColor color_t_5];
    self.emailStrInputBorderView.backgroundColor = [UIColor color_t_5];
    self.passWordInputBorderView.backgroundColor = [UIColor color_t_5];
    
    self.contentView.backgroundColor = [UIColor color_t_6];
    self.scrollerView.backgroundColor = [UIColor color_t_6];
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
}

-(OdditySearchTextField *)makeTextInputViewMethod:(NSString *)pass{

    OdditySearchTextField *inputTextFiled = [[OdditySearchTextField alloc] init];
    inputTextFiled.clearButtonMode = UITextFieldViewModeWhileEditing;
    inputTextFiled.attributedPlaceholder = [self attributedPlaceholder:pass];
    
    inputTextFiled.font = [UIFont font_t_4];
    inputTextFiled.textColor = UIColor.color_t_2;
    
    return inputTextFiled;
}

-(NSAttributedString *)attributedPlaceholder:(NSString *)string {
    
    NSDictionary *attris = @{
                             NSFontAttributeName:[UIFont font_t_4],
                             NSForegroundColorAttributeName: [UIColor color_t_3]
                             };
    
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString: string attributes:attris];
    
    return attriString;
}

@end
