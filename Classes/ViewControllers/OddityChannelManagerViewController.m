//
//  ChannelManagerViewController.m
//  Corn
//
//  Created by 荆文征 on 2017/2/8.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import "OddityCategorys.h"
#import "OddityCustomView.h"
#import <Realm/Realm.h>
#import "OddityPlayerView.h"
#import <Masonry/Masonry.h>

#import "OddityEggsViewController.h"

#import "OddityNewsArrayObject.h"

#import "OddityLXReorderableCollectionViewFlowLayout.h"

#import "OddityChannelManagerViewController.h"

#import "OddityChannelManagerCollectionViewCell.h"
#import "OddityChannelManagerCollectionReusableView.h"

@interface OddityChannelManagerViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,OddityLXReorderableCollectionViewDataSource,OddityLXReorderableCollectionViewDelegateFlowLayout>

@property(nonatomic,strong) OddityNavigationBarView *navVarView;
@property(nonatomic,strong) UICollectionView *collectionView;

@property(nonatomic,strong) RLMArray<OddityChannel *> *normalResults;
@property(nonatomic,strong) RLMArray<OddityChannel *> *deleteResults;

@end

@implementation OddityChannelManagerViewController

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    __weak typeof(self) weakSelf = self;
    _navVarView = [[OddityNavigationBarView alloc]init:NSString.oddity_manage_channel_title_string titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    
    [self.view addSubview:_navVarView];
    [_navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    
    
    UITapGestureRecognizer *ggTap = [[UITapGestureRecognizer alloc] init];
    ggTap.numberOfTapsRequired = 8;
    ggTap.numberOfTouchesRequired = 2;
    [_navVarView addGestureRecognizer:ggTap];
    [ggTap addTarget:self action:@selector(ggViewController)];
    
    _normalResults = [OddityNewsArrayObject channelManager].normalChannelArray;
    _deleteResults = [OddityNewsArrayObject channelManager].deleteChannelArray;
    
    OddityLXReorderableCollectionViewFlowLayout *flowLayout = [[OddityLXReorderableCollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    flowLayout.itemSize = CGSizeMake((oddityUIScreenWidth-2*12-2*18)/3, 44);
    flowLayout.sectionInset = UIEdgeInsetsMake(17, 12, 23, 12);
    flowLayout.minimumLineSpacing = 10;
    flowLayout.minimumInteritemSpacing = 10;
    flowLayout.headerReferenceSize = CGSizeMake(0, 38);
    
    if ([UICollectionViewFlowLayout instancesRespondToSelector:@selector(sectionHeadersPinToVisibleBounds)]) {
        
        flowLayout.sectionHeadersPinToVisibleBounds = true;
    }
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    _collectionView.backgroundColor = [UIColor color_t_6];
    [self.view addSubview:_collectionView];
    [_collectionView registerClass:[OddityChannelManagerCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    [_collectionView registerClass:[OddityChannelManagerCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusable"];
    [_collectionView registerClass:[OddityChannelManagerCollectionFReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"freusable"];
    [_collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_navVarView.mas_bottom);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;

    self.scrollView = _collectionView;
}



-(void)ggViewController{
    
    UINavigationController *eggsView =  [[UINavigationController alloc] initWithRootViewController:[[OddityEggsViewController alloc] init]];
    
    [[UIViewController oddityCurrentViewController] presentViewController:eggsView animated:true completion:nil];
}

-(void)refreshThemeMethod{
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    [self.collectionView reloadData];
    self.view.backgroundColor = [UIColor color_t_6];
    [self setNeedsStatusBarAppearanceUpdate];
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{

    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if (section == 0 ) {
    
        return _normalResults.count;
    }
    
    return _deleteResults.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    OddityChannelManagerCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    RLMArray *results = indexPath.section == 0 ? _normalResults : _deleteResults;
    
    [cell setChannel: [results objectAtIndex:indexPath.row]];
    
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{

    if (kind == UICollectionElementKindSectionHeader) {
        
        OddityChannelManagerCollectionReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"reusable" forIndexPath:indexPath];
        
        [reusableView setDescStr: indexPath.section == 0 ? NSString.oddity_manage_channel_my_title_string : NSString.oddity_manage_channel_hot_title_string];
        
        return reusableView;
    }
    
    OddityChannelManagerCollectionFReusableView *reusableView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"freusable" forIndexPath:indexPath];
    
    return reusableView;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   
    if (indexPath.section == 1) {
        
        [OddityLogObject createAppAction:OddityLogAtypeAddChannel fromPageName:OddityLogPageChannelPage toPageName:OddityLogPageNullPage effective:false params:nil];
    }
    
    self.collectionView.userInteractionEnabled = false;
    
    RLMArray *fromResults = indexPath.section == 0 ? _normalResults : _deleteResults;
    RLMArray *toResults = indexPath.section == 0 ? _deleteResults : _normalResults;
    
    OddityChannel *channel = [fromResults objectAtIndex:indexPath.row];
    
    [[OddityNewsArrayObject channelManager] appendChannel:channel isNormal:indexPath.section == 1];
    
    NSIndexPath *toIndexPath = [NSIndexPath indexPathForRow:[toResults indexOfObject:channel] inSection:indexPath.section == 0 ? 1 : 0];
    
    [self.collectionView performBatchUpdates:^{
        
        [self.collectionView moveItemAtIndexPath:indexPath toIndexPath:toIndexPath];
        
    } completion:^(BOOL finished) {
        
        [self.collectionView reloadItemsAtIndexPaths:@[toIndexPath]];
        
        self.collectionView.userInteractionEnabled = true;
        
        [OddityShareUser updateAdjustmentChannel];
    }];
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{

    return section == 0 ? CGSizeMake(0, 10) : CGSizeZero;
}

-(void)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath didMoveToIndexPath:(NSIndexPath *)toIndexPath{

    [[OddityNewsArrayObject channelManager] moveFromIndex:fromIndexPath.row toIndex:toIndexPath.row];
    
    [OddityShareUser updateAdjustmentChannel];
    
    [OddityLogObject createAppAction:OddityLogAtypeMoveChannel fromPageName:OddityLogPageChannelPage toPageName:OddityLogPageNullPage effective:false params:nil];
}

/// -> 设置第一个cell 不可选中

-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0 && indexPath.item == 0) { return false; }
    
    return true;
}

/// -> 设置第一个cell 或者 第一组 cell 不可移动

- (BOOL)collectionView:(UICollectionView *)collectionView itemAtIndexPath:(NSIndexPath *)fromIndexPath canMoveToIndexPath:(NSIndexPath *)toIndexPath{
    
    if ((toIndexPath.section == 0 && toIndexPath.item == 0) || toIndexPath.section == 1 ){ return false; }
    
    return true;
}

-(BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ((indexPath.section == 0 && indexPath.item == 0) || indexPath.section == 1 ){ return false; }
    
    return true;
}

@end
