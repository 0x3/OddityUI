//
//  OddityCustomAnimationViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/13.
//
//

#import <Foundation/Foundation.h>

/**
 *   自定义跳转动画执行 ViewController
 */
@interface OddityCustomAnimationViewController : UIViewController

/// 执行跳转动画时 所需要的 滑动视图
@property(nonatomic,weak) UIScrollView *scrollView;

/// 执行跳转动画 需要的手势对象
@property(nonatomic,strong) UIPanGestureRecognizer *panGestureRecognizer;

/**
 *   初始化自定义跳转动画ViewController
 *
 *  @param nibNameOrNil   无
 *  @param nibBundleOrNil 无
 *
 *  @return 自定义的 ViewController
 */
-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;
@end





