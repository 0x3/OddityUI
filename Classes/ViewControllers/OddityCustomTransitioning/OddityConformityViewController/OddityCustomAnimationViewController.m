//
//  OddityCustomAnimationViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/13.
//
//

#import "OddityCustomTransitioning.h"
#import "OddityCustomAnimationViewController.h"

@interface OddityCustomAnimationViewController()<UIGestureRecognizerDelegate,UIViewControllerTransitioningDelegate>

@property(nonatomic,strong) OddityCustomViewControllerPresentdAnimation *presentdAnimation;
@property(nonatomic,strong) OddityCustomViewControllerDismissedAnimation *dismissedAnimation;

@property(nonatomic,strong) OddityCustomViewControllerDrivenInteractiveTransition *interactiveTransitioning;
@end

@implementation OddityCustomAnimationViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        self.transitioningDelegate = self;
        self.modalPresentationCapturesStatusBarAppearance = true;
    }
    return self;
}


-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    _presentdAnimation = [[OddityCustomViewControllerPresentdAnimation alloc] init];
    _dismissedAnimation = [[OddityCustomViewControllerDismissedAnimation alloc] init];
    _interactiveTransitioning = [[OddityCustomViewControllerDrivenInteractiveTransition alloc]init];
    
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGestureRecognizerMethod:)];
    _panGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:_panGestureRecognizer];
}

-(void)setScrollView:(UIScrollView *)scrollView{
    
    _scrollView = scrollView;
    
    [_scrollView.panGestureRecognizer requireGestureRecognizerToFail:self.panGestureRecognizer];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    CGPoint point = [_panGestureRecognizer velocityInView:self.view];
    
    return (fabs(point.x) > fabs(point.y)) && point.x > 0;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _presentdAnimation ?: [[OddityCustomViewControllerPresentdAnimation alloc] init];
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _dismissedAnimation;
}

-(id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator{
    
    if (_dismissedAnimation.isInteraction) {
        
        return _interactiveTransitioning;
    }
    return nil;
}

-(void)handlePanGestureRecognizerMethod:(UIPanGestureRecognizer *)pan{
    
    CGPoint point = [pan translationInView:self.view];
    
    switch (pan.state) {
            
        case UIGestureRecognizerStateBegan:
            
            self.dismissedAnimation.isInteraction = true;
            
            [self dismissViewControllerAnimated:true completion:nil];
            break;
            
        case UIGestureRecognizerStateChanged:
            
            [_interactiveTransitioning updateInteractiveTransition: (CGFloat)point.x/CGRectGetWidth(self.view.frame)];
            break;
        default:
            
            _dismissedAnimation.isInteraction = false;
            
            CGFloat locationX = ABS(point.x);
            CGFloat velocityX = [pan velocityInView:self.view].x;
            
            if (velocityX >= 800 || locationX >= CGRectGetWidth(self.view.frame)/2) {
                
                [_interactiveTransitioning finishInteractiveTransition];
            }else{
                
                [_interactiveTransitioning cancelInteractiveTransition];
            }
            break;
    }
}

@end
