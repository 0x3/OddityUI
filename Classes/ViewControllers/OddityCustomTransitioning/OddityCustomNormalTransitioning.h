//
//  OddityCustomNormalTransitioning.h
//  Pods
//
//  Created by 荆文征 on 2017/4/13.
//
//

#import <Foundation/Foundation.h>

/// 公共的
@interface OddityNormalViewControllerDismissedAnimation : NSObject<UIViewControllerAnimatedTransitioning>

/// 是否需要渐进式 手势 跳转
@property(nonatomic,assign) BOOL isInteraction;

@end

/**
 *  自定义 present 动画 实现 模拟 IOS NavgationViewControll Push 风格跳转方式
 */
@interface OddityCustomViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end
/**
 *  自定义 dismiss 动画 实现 模拟 IOS NabGationViewControll Dismiss 风格 方式
 */
@interface OddityCustomViewControllerDismissedAnimation : OddityNormalViewControllerDismissedAnimation


@end
/**
 *   自定义跳转 渐进式的 跳转对象
 */
@interface OddityCustomViewControllerDrivenInteractiveTransition : UIPercentDrivenInteractiveTransition

@property (nonatomic,assign) BOOL noScale;

@end
