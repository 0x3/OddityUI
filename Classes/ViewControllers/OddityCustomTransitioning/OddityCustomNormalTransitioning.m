//
//  OddityCustomNormalTransitioning.m
//  Pods
//
//  Created by 荆文征 on 2017/4/13.
//
//

#import "OddityHuangLiArrayListViewController.h"

#import "OddityUserLoginViewController.h"

#import "OddityPlayerView.h"
#import "OddityCustomTransitioning.h"
#import "OddityMainManagerViewController.h"
#import "OddityCustomNormalTransitioning.h"

#import "OddityNewsDetailViewController.h"

@implementation OddityNormalViewControllerDismissedAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    
}

@end


@implementation OddityCustomViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *toViewController = [transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    toViewController.view.transform = CGAffineTransformTranslate(toViewController.view.transform, CGRectGetWidth(toViewController.view.frame), 0);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.view.transform = CGAffineTransformIdentity;
        
        if (![fromViewController isKindOfClass:[OddityUserLoginViewController class]] && ![fromViewController isKindOfClass:[OddityHuangLiArrayListViewController class]]) {
            
            fromViewController.view.transform = CGAffineTransformScale(fromViewController.view.transform, 0.95, 0.95);
        }
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition:true];
        
        if ([toViewController isKindOfClass:[OddityNewsDetailViewController class]] ) {
            
            OddityNewsDetailViewController *detailViewController = (OddityNewsDetailViewController *)toViewController;
            
            if (detailViewController.isVideo) {
                
                [detailViewController pagerTabStripViewController:detailViewController updateIndicatorFromIndex:0 toIndex:0 withProgressPercentage:1 indexWasChanged:true];
                
                return;
            }
        }
        
        if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
            
            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];
            
            [[[UIApplication sharedApplication]keyWindow] bringSubviewToFront:[OddityPlayerView sharePlayerView]];
        }
    }];
}

@end


@implementation OddityCustomViewControllerDismissedAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *toViewController = [transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    [containerView insertSubview:toViewController.view atIndex:0];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] <= 8.0) {
        if (![fromViewController isKindOfClass:[OddityUserLoginViewController class]] && ![fromViewController isKindOfClass:[OddityHuangLiArrayListViewController class]]) {
            toViewController.view.transform = CGAffineTransformScale(toViewController.view.transform, 0.95, 0.95);
        }
    }
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.view.transform = CGAffineTransformIdentity;
        fromViewController.view.transform = CGAffineTransformTranslate(fromViewController.view.transform, CGRectGetWidth(fromViewController.view.frame), 0);
        
        toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
        
    } completion:^(BOOL finished) {
        

        
        [transitionContext completeTransition: ![transitionContext transitionWasCancelled]];
        
        
        if ([fromViewController isKindOfClass:[OddityNewsDetailViewController class]] && ((OddityNewsDetailViewController *)fromViewController).justGoBack) {
            
            [OddityPlayerView.sharePlayerView closePlayerView];
        }else{
        
            if ([transitionContext transitionWasCancelled]) {
                
                [OddityTransitionHandleObject dismissUnifiedHandleMethod:fromViewController];
            }else{
                
                [OddityTransitionHandleObject dismissUnifiedHandleMethod:toViewController];
            }
        }
    }];
}

@end




@interface OddityCustomViewControllerDrivenInteractiveTransition()

@property(nonatomic,weak)UIViewController *toViewController;
@property(nonatomic,weak)UIViewController *fromViewController;

@property(nonatomic,weak)id<UIViewControllerContextTransitioning> transitionContext;

@end

@implementation OddityCustomViewControllerDrivenInteractiveTransition

-(void)startInteractiveTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
//    [super startInteractiveTransition:transitionContext];
    
    _fromViewController = [transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    if (self.fromIsVideoVCAndGOBack) {
        
        _transitionContext = transitionContext;
        _toViewController = [transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
        _fromViewController = [transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
        
//        _toViewController.view.frame = [transitionContext finalFrameForViewController:_toViewController];
        
        UIView *containerView = [transitionContext containerView];
        [containerView insertSubview:_toViewController.view atIndex:0];
    }else{
    
        [super startInteractiveTransition:transitionContext];
    }
    
    
}

-(void)updateInteractiveTransition:(CGFloat)percentComplete{
    
    if (self.fromIsVideoVCAndGOBack) {
        
        CGFloat progress = MAX(MIN(percentComplete, 1), 0);
        
        if (!self.noScale) {
            
            _toViewController.view.transform = CGAffineTransformMakeScale(0.95+(0.05*progress), 0.95+(0.05*progress));
        }
        
        _fromViewController.view.transform = CGAffineTransformMakeTranslation(CGRectGetWidth(_fromViewController.view.bounds)*progress, 0);
        
    }else{
        
        [super updateInteractiveTransition:percentComplete];
    }
}

-(void)cancelInteractiveTransition{
    
    if (self.fromIsVideoVCAndGOBack) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [self updateInteractiveTransition:0];
        } completion:^(BOOL finished) {
            
            [_transitionContext completeTransition:false];
        }];
    }else{
        
        [super cancelInteractiveTransition];
    }
}

-(void)finishInteractiveTransition{
    
    if (self.fromIsVideoVCAndGOBack) {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [self updateInteractiveTransition:1];
        } completion:^(BOOL finished) {

            if (((OddityNewsDetailViewController *)_fromViewController).justGoBack) {
                
                [OddityPlayerView.sharePlayerView closePlayerView];
            }else{
                
                [OddityTransitionHandleObject dismissUnifiedHandleMethod: self.toViewController];
            }
            
            [_transitionContext completeTransition:true];
        }];
    }else{
        
        [super finishInteractiveTransition];
    }
}

-(BOOL)fromIsVideoVCAndGOBack{

    if ([_fromViewController isKindOfClass:[OddityNewsDetailViewController class]]) {
        
        if (((OddityNewsDetailViewController *)_fromViewController).isVideo) {
            
            return true;
        }
    }
    
    return false;
}

@end
