//
//  OddityCustomSearchTransitioning.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <Foundation/Foundation.h>

@interface OddityCustomSearchSpursPresentedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@end


@interface OddityCustomSearchSpursDismissTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property(nonatomic,assign) BOOL isInteraction;

@end


@interface OddityCustomSearchResultPresentedTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@end


@interface OddityCustomSearchResultDismissTransitioning : NSObject <UIViewControllerAnimatedTransitioning>

@property(nonatomic,assign) BOOL isInteraction;

@end
