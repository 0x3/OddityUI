//
//  OddityCustomSearchTransitioning.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <Masonry/Masonry.h>
#import "OddityPlayerView.h"
#import "OdditySearchTextField.h"
#import "OddityCustomTransitioning.h"
#import "OddityNewArrayListViewController.h"
#import "OdditySearchSpursViewController.h"
#import "OddityMainManagerViewController.h"
#import "OddityCustomSearchTransitioning.h"
#import "OdditySearchResultViewController.h"

@implementation OddityCustomSearchSpursPresentedTransitioning

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OdditySearchSpursViewController *toViewController = (OdditySearchSpursViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    OddityNewArrayListViewController *newArrayList = toViewController.nArrayListViewController;
    
    newArrayList.searchHeaderView.alpha = 0;
    
    CGRect fromHeaderRect = [newArrayList.searchHeaderView convertRect:newArrayList.searchHeaderView.bounds toView: [UIApplication sharedApplication].keyWindow];
    
    toViewController.tableView.alpha = 0.2;
    toViewController.view.backgroundColor = [toViewController.view.backgroundColor colorWithAlphaComponent:0];
    
    [toViewController.navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo(toViewController.view).offset(64);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(CGRectGetWidth(fromHeaderRect));
        make.height.mas_equalTo(CGRectGetHeight(fromHeaderRect));
    }];
    
    [toViewController.searchHeaderView.inputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(12);
        make.right.mas_equalTo(-12);
        make.centerY.equalTo(toViewController.navVarView);
        make.height.mas_equalTo(28);
    }];
    
    [toViewController.searchHeaderView layoutIfNeeded];
    [toViewController.view layoutIfNeeded];
    
    [toViewController.searchHeaderView.inputTextFiled becomeFirstResponder];
    
    toViewController.searchHeaderView.backgroundColor = newArrayList.searchHeaderView.backgroundColor;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.view.backgroundColor = [toViewController.view.backgroundColor colorWithAlphaComponent:1];
        
        newArrayList.tableView.transform = CGAffineTransformTranslate(newArrayList.tableView.transform, 0, -44);
        
        [toViewController.navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.mas_equalTo(toViewController.mas_topLayoutGuide);
            make.left.with.right.mas_equalTo(0);
            make.height.height.mas_equalTo(44);
        }];
        
        [toViewController.searchHeaderView.inputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.equalTo(toViewController.navVarView.cancelButton.mas_right);
            make.right.mas_equalTo(-12);
            make.centerY.equalTo(toViewController.navVarView.cancelButton);
            make.height.mas_equalTo(28);
        }];
        
        [toViewController.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition:true];
        
        if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
            
            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];
            
            [[[UIApplication sharedApplication]keyWindow] bringSubviewToFront:[OddityPlayerView sharePlayerView]];
        }
    }];
    
    [UIView animateWithDuration:0.2 delay:0.3 options:(UIViewAnimationOptionAutoreverse) animations:^{
        
        toViewController.tableView.alpha = 1;
        
    } completion:nil];
}

@end

@implementation OddityCustomSearchSpursDismissTransitioning

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    UIViewController *toViewController = [transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    OdditySearchSpursViewController *fromViewController = (OdditySearchSpursViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    [containerView insertSubview:toViewController.view atIndex:0];
    
    OddityNewArrayListViewController *newArrayList = fromViewController.nArrayListViewController;
    CGRect fromHeaderRect = [newArrayList.searchHeaderView convertRect:newArrayList.searchHeaderView.bounds toView: [UIApplication sharedApplication].keyWindow];
    
    fromViewController.searchHeaderView.inputTextFiled.text = @"";
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        newArrayList.tableView.transform = CGAffineTransformIdentity;
        
        fromViewController.tableView.alpha = 0;
        fromViewController.view.backgroundColor = [fromViewController.view.backgroundColor colorWithAlphaComponent:0];
        
        [fromViewController.navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(fromViewController.view).offset(64);
            make.left.mas_equalTo(0);
            make.width.mas_equalTo(CGRectGetWidth(fromHeaderRect));
            make.height.mas_equalTo(CGRectGetHeight(fromHeaderRect));
        }];
        
        [fromViewController.searchHeaderView.inputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(12);
            make.right.mas_equalTo(-12);
            make.centerY.equalTo(fromViewController.navVarView);
            make.height.mas_equalTo(28);
        }];
        
        [fromViewController.searchHeaderView layoutIfNeeded];
        [fromViewController.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        newArrayList.searchHeaderView.alpha = 1;
        
        fromViewController.view.backgroundColor = [fromViewController.view.backgroundColor colorWithAlphaComponent:1];
        
        [transitionContext completeTransition: ![transitionContext transitionWasCancelled]];
        
        if ([transitionContext transitionWasCancelled]) {
            
            [OddityTransitionHandleObject dismissUnifiedHandleMethod:fromViewController];
        }else{
            
            [OddityTransitionHandleObject dismissUnifiedHandleMethod:toViewController];
        }
    }];
}

@end






@implementation OddityCustomSearchResultPresentedTransitioning

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{

    OdditySearchResultViewController *toViewController = (OdditySearchResultViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    OdditySearchSpursViewController *fromViewController = (OdditySearchSpursViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    UIColor *backColor = toViewController.view.backgroundColor;
    
    toViewController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    
    toViewController.tableView.transform = CGAffineTransformTranslate(toViewController.tableView.transform, CGRectGetWidth(toViewController.view.frame), 0);
    
    toViewController.searchHeaderView.inputTextFiled.text = fromViewController.searchHeaderView.inputTextFiled.text;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.tableView.transform = CGAffineTransformIdentity;
        
        toViewController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    } completion:^(BOOL finished) {
        
        toViewController.view.backgroundColor = backColor;
        
        [transitionContext completeTransition:true];
        
        if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
            
            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];
            
            [[[UIApplication sharedApplication]keyWindow] bringSubviewToFront:[OddityPlayerView sharePlayerView]];
        }
    }];
}

@end

@implementation OddityCustomSearchResultDismissTransitioning

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OdditySearchSpursViewController *toViewController = (OdditySearchSpursViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    OdditySearchResultViewController *fromViewController = (OdditySearchResultViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    toViewController.searchHeaderView.inputTextFiled.text = fromViewController.searchHeaderView.inputTextFiled.text;
    
    [containerView insertSubview:toViewController.view atIndex:0];
    
    fromViewController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    [fromViewController.searchHeaderView.inputTextFiled resignFirstResponder];
    
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        fromViewController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
        fromViewController.tableView.transform = CGAffineTransformTranslate(fromViewController.tableView.transform, CGRectGetWidth(fromViewController.view.frame), 0);
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: ![transitionContext transitionWasCancelled]];
        
        if ([transitionContext transitionWasCancelled]) {
            
            [OddityTransitionHandleObject dismissUnifiedHandleMethod:fromViewController];
        }else{
            
            [OddityTransitionHandleObject dismissUnifiedHandleMethod:toViewController];
        }
    }];
}

@end
