//
//  CustomTransitioning.h
//  Pods
//
//  Created by 荆文征 on 2017/4/13.
//
//

#ifndef CustomTransitioning_h
#define CustomTransitioning_h

#import "OddityCustomVideoTransitioning.h"
#import "OddityCustomNormalTransitioning.h"
#import "OddityCustomAnimationViewController.h"

#endif /* CustomTransitioning_h */


@interface OddityTransitionHandleObject : NSObject

+(void)dismissUnifiedHandleMethod:(UIViewController *)toViewController;

@end
