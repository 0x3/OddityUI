//
//  OddityCustomTransitioning.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>
#import "OddityPlayerView.h"
#import "OddityMainManagerViewController.h"
#import "OddityCustomTransitioning.h"

#import "UIViewController+Oddity.h"

#import "OddityNewsDetailViewController.h"
#import "OddityNewContentViewController.h"

@implementation OddityTransitionHandleObject

/// 返回方法统一处理方法
+(void)dismissUnifiedHandleMethod:(UIViewController *)toViewController{

    if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
    
        if ([toViewController isKindOfClass:[OddityMainManagerViewController class]]) {
            
            OddityMainManagerViewController *newViewController =  (OddityMainManagerViewController *)toViewController;
            
            [[OddityPlayerView sharePlayerView] toNeedJudgmentState];
            
            [newViewController refreshPlayViewState];
        }else if([toViewController isKindOfClass:[OddityNewsDetailViewController class]] && ((OddityNewsDetailViewController *)toViewController).isVideo){
            
            OddityNewsDetailViewController *nACViewController = (OddityNewsDetailViewController *)toViewController;
            
            [[OddityPlayerView sharePlayerView] waitForReadyToPlay:nACViewController.durationTime];
            
            OddityPlayerObject *playObject = [[OddityPlayerObject alloc] initWith:nACViewController.nid VideoUrl:nACViewController.videoUrl Title:nACViewController.nContent.title Objtect:nACViewController.videoObjt];
            [[OddityPlayerView sharePlayerView] playWithPlayerObject:playObject];
            
            [[OddityPlayerView sharePlayerView] toNeedJudgmentState];
            
            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateNormal) inView:nACViewController.contentViewController.playView];
            
        }else{
            
            [[OddityPlayerView sharePlayerView] toNeedJudgmentState];
            
            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];
        }
    }
}

@end
