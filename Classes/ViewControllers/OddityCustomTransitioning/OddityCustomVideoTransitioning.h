//
//  CustomViewControllerAnimatedTransitioning.h
//  Corn
//
//  Created by 荆文征 on 2017/2/18.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OddityCustomNormalTransitioning.h"

/**
 *  自定义视频 present 跳转动画
 */
@interface OddityCustomVideoViewControllerPresentdAnimation : NSObject <UIViewControllerAnimatedTransitioning>

@property(nonatomic,assign) CGRect viewControllerFromRect;

@end
/**
 *  自定义视频 dismiss 跳转动画
 */
@interface OddityCustomVideoViewControllerDismissAnimation : OddityNormalViewControllerDismissedAnimation

@end
/**
 *   自定义跳转 渐进式的 跳转对象
 */
@interface OddityCustomVideoViewControllerDrivenInteractiveTransition : UIPercentDrivenInteractiveTransition

@end
