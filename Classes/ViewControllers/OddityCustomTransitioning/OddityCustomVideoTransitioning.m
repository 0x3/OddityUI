//
//  CustomViewControllerAnimatedTransitioning.m
//  Corn
//
//  Created by 荆文征 on 2017/2/18.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import "OddityPlayerView.h"
#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityCustomTransitioning.h"
#import "OddityCustomVideoTransitioning.h"

#import "OddityNewsDetailViewController.h"
#import "OddityNewContentViewController.h"


@interface OddityCustomVideoViewControllerPresentdAnimation()

@property(nonatomic,strong) UIWindow *currentWindow;

@end

@implementation OddityCustomVideoViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{

    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{

    
    _currentWindow = [[UIApplication sharedApplication] keyWindow];
    
    OddityNewsDetailViewController *toViewController = (OddityNewsDetailViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    if (CGRectEqualToRect(self.viewControllerFromRect, CGRectZero)) {
        
        self.viewControllerFromRect = [OddityPlayerView.sharePlayerView convertRect:OddityPlayerView.sharePlayerView.bounds toView:UIApplication.sharedApplication.keyWindow];
    }

    [UIApplication.sharedApplication.keyWindow bringSubviewToFront:OddityPlayerView.sharePlayerView];
 
    [containerView addSubview:toViewController.view];
    
    toViewController.view.backgroundColor = [toViewController.view.backgroundColor colorWithAlphaComponent:0];
    
    /// 一些配制方法 具体实现查看方法内部注释
    [self makeVideoViewMethod:toViewController];
    
    toViewController.waitView.alpha = 0;
    toViewController.contentViewController.tableView.alpha = 0;
    
    toViewController.navVarView.alpha = 0;
    
    [UIView animateWithDuration:0.5 animations:^{
        
        toViewController.waitView.alpha = 1;
        toViewController.contentViewController.tableView.alpha = 1;
        
        [toViewController.contentViewController.playView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            if (ODDITY_SYSTEM_LESS_THAN(8)) {
                
                make.top.equalTo(toViewController.contentViewController.mas_topLayoutGuide).offset(-20);
            }else{
            
                make.top.equalTo(toViewController.contentViewController.mas_topLayoutGuide);
            }
            
            make.left.equalTo(toViewController.contentViewController.view);
            make.right.equalTo(toViewController.contentViewController.view);
            make.height.equalTo(toViewController.contentViewController.playView.mas_width).multipliedBy((CGFloat)211/375);
        }];
        
        [toViewController.contentViewController.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
       
        if ([OddityPlayerView sharePlayerView].playViewState == OddityPlayViewStateSmall) {
            
            [toViewController.contentViewController.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.top.equalTo(toViewController.contentViewController.playView.mas_bottom);
                make.left.equalTo(toViewController.contentViewController.view);
                make.right.equalTo(toViewController.contentViewController.view);
                make.bottom.equalTo(toViewController.contentViewController.view);
            }];
        }
        
        
        toViewController.view.backgroundColor = [toViewController.view.backgroundColor colorWithAlphaComponent:1];
        
        toViewController.navVarView.alpha = 1;
        
        [transitionContext completeTransition:true];
        
        [[OddityPlayerView sharePlayerView] toNeedJudgmentState];
        [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateNormal) inView:toViewController.contentViewController.playView];
    }];
}

/// 制作视频模块 抽象方法
-(void)makeVideoViewMethod:(OddityNewsDetailViewController *)toViewController{

    /// 将播放器详情页背景视图 按照 播放视图 相对于 UIWindow 的位置 设置约束
    [toViewController.contentViewController.playView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_currentWindow).offset(_viewControllerFromRect.origin.y);
        make.left.equalTo(_currentWindow).offset(_viewControllerFromRect.origin.x);
        
        make.width.equalTo(@(_viewControllerFromRect.size.width));
        make.height.equalTo(@(_viewControllerFromRect.size.height));
    }];
    
    /// 将播放器 放置于 播放图片上
    [toViewController.contentViewController.playView addSubview:[OddityPlayerView sharePlayerView]];
    [[OddityPlayerView sharePlayerView] mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(toViewController.contentViewController.playView);
        make.left.equalTo(toViewController.contentViewController.playView);
        make.right.equalTo(toViewController.contentViewController.playView);
        make.bottom.equalTo(toViewController.contentViewController.playView);
    }];
    
    /// 如果此时还在加载数据 ， 设置 wait view
    if (toViewController.waitView.superview) {
        
        if ([OddityPlayerView sharePlayerView].playViewState == OddityPlayViewStateSmall) {
            
            /// 如果 现在的播放器视图 为 缩小 的播放模式 设置它的左右为 播放背景左右
            [toViewController.waitView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.top.equalTo(toViewController.contentViewController.playView.mas_bottom);
                make.left.equalTo(toViewController.contentViewController.playView);
                make.right.equalTo(toViewController.contentViewController.playView);
                make.bottom.equalTo(toViewController.contentViewController.view);
            }];
        }else{
        
            /// 反之 默认设置
            [toViewController.waitView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.top.equalTo(toViewController.contentViewController.playView.mas_bottom);
                make.left.equalTo(toViewController.view);
                make.right.equalTo(toViewController.view);
                make.bottom.equalTo(toViewController.view);
            }];
        }
    }
    
    if ([OddityPlayerView sharePlayerView].playViewState == OddityPlayViewStateSmall) {
    
        /// 同 Waitview 情况
        [toViewController.contentViewController.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(toViewController.contentViewController.playView.mas_bottom);
            make.left.equalTo(toViewController.contentViewController.playView);
            make.right.equalTo(toViewController.contentViewController.playView);
            make.bottom.equalTo(toViewController.contentViewController.view);
        }];
    }
    
    [toViewController.contentViewController.view layoutIfNeeded];
}

@end

@interface OddityCustomVideoViewControllerDismissAnimation ()

@property(nonatomic,strong) UIWindow *currentWindow;
@property(nonatomic,assign) CGRect viewControllerFromRect;

@end

@implementation OddityCustomVideoViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{

    return 0.4;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    if (![OddityPlayerView sharePlayerView].isPlayerViewShow) { // 如果视频没有展示的话
        
        [self updateSmall:transitionContext];
        
        return;
    }
    
    _currentWindow = [[UIApplication sharedApplication] keyWindow];
    
    UIViewController *toViewController = [transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    OddityNewsDetailViewController *fromViewController = (OddityNewsDetailViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    _viewControllerFromRect = fromViewController.videoPresentdAnimation.viewControllerFromRect;
    
    fromViewController.view.backgroundColor = [fromViewController.view.backgroundColor colorWithAlphaComponent:0];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        
        fromViewController.contentViewController.playView.hidden = true;
    }
    
    [containerView insertSubview:toViewController.view atIndex:0];
    
    CGRect vcFromRect = [[OddityPlayerView sharePlayerView] convertRect:[OddityPlayerView sharePlayerView].bounds toView:[[UIApplication sharedApplication]keyWindow]];
    
    [containerView addSubview:[OddityPlayerView sharePlayerView]];
    [[OddityPlayerView sharePlayerView] mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_currentWindow).offset(vcFromRect.origin.y);
        make.left.equalTo(_currentWindow).offset(vcFromRect.origin.x);
        
        make.width.equalTo(@(vcFromRect.size.width));
        make.height.equalTo(@(vcFromRect.size.height));
    }];
    
    [containerView layoutIfNeeded];
    [fromViewController.contentViewController.view layoutIfNeeded];
    
    fromViewController.navVarView.alpha = 0;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        [fromViewController.contentViewController.playView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_currentWindow).offset(_viewControllerFromRect.origin.y);
            make.left.equalTo(_currentWindow).offset(_viewControllerFromRect.origin.x);
            
            make.width.equalTo(@(_viewControllerFromRect.size.width));
            make.height.equalTo(@(_viewControllerFromRect.size.height));
        }];
        
        [[OddityPlayerView sharePlayerView] mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_currentWindow).offset(_viewControllerFromRect.origin.y);
            make.left.equalTo(_currentWindow).offset(_viewControllerFromRect.origin.x);
            
            make.width.equalTo(@(_viewControllerFromRect.size.width));
            make.height.equalTo(@(_viewControllerFromRect.size.height));
        }];
        
        fromViewController.contentViewController.tableView.alpha = 0;
        
        [containerView layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition:true];
        
        [OddityTransitionHandleObject dismissUnifiedHandleMethod: toViewController];
    }];
}


-(void)updateSmall:(id<UIViewControllerContextTransitioning>)transitionContext{

    UIViewController *toViewController = [transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    UIView *containerView = [transitionContext containerView];
    
    [containerView insertSubview:toViewController.view atIndex:0];
    
    
    toViewController.view.transform = CGAffineTransformScale(toViewController.view.transform, 0.95, 0.95);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.view.transform = CGAffineTransformIdentity;
        fromViewController.view.transform = CGAffineTransformTranslate(fromViewController.view.transform, CGRectGetWidth(fromViewController.view.frame), 0);
        
        toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
        
    } completion:^(BOOL finished) {
        
        [transitionContext completeTransition: ![transitionContext transitionWasCancelled]];
    }];
}

@end


@interface OddityCustomVideoViewControllerDrivenInteractiveTransition()

@property(nonatomic,strong) UIView *containerView;

@property(nonatomic,strong) UIWindow *currentWindow;

@property(nonatomic,assign) CGRect fromVCFromRect;
@property(nonatomic,assign) CGRect viewControllerFromRect;

@property(nonatomic,weak)UIViewController *toViewController;
@property(nonatomic,weak)OddityNewsDetailViewController *fromViewController;

@property(nonatomic,weak)id<UIViewControllerContextTransitioning> transitionContext;

@end

@implementation OddityCustomVideoViewControllerDrivenInteractiveTransition

-(void)startInteractiveTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    _transitionContext = transitionContext;
    _containerView = [transitionContext containerView];
    _currentWindow = [[UIApplication sharedApplication] keyWindow];
    _toViewController = [transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    _fromViewController = [transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    _viewControllerFromRect = _fromViewController.videoPresentdAnimation.viewControllerFromRect; // 获取位置
    
    
    _fromViewController.view.backgroundColor = [_fromViewController.view.backgroundColor colorWithAlphaComponent:0];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        
        _fromViewController.contentViewController.playView.hidden = true;
    }
    
    [_containerView insertSubview:_toViewController.view atIndex:0];
    
    CGRect vcFromRect = [[OddityPlayerView sharePlayerView] convertRect:[OddityPlayerView sharePlayerView].bounds toView:[[UIApplication sharedApplication]keyWindow]];
    _fromVCFromRect = vcFromRect;
    [_containerView addSubview:[OddityPlayerView sharePlayerView]];
    [[OddityPlayerView sharePlayerView] mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_currentWindow).offset(vcFromRect.origin.y);
        make.left.equalTo(_currentWindow).offset(vcFromRect.origin.x);
        
        make.width.equalTo(@(vcFromRect.size.width));
        make.height.equalTo(@(vcFromRect.size.height));
    }];
    
    [_containerView layoutIfNeeded];
    [_fromViewController.contentViewController.view layoutIfNeeded];
    _fromViewController.navVarView.alpha = 0;
    
}

-(void)updateInteractiveTransition:(CGFloat)percentComplete{
    
    CGFloat progress = MAX(MIN(percentComplete, 1), 0);
    
    CGFloat fromTop =  CGRectGetMinY(self.fromVCFromRect);
    CGFloat toTop =  CGRectGetMinY(self.viewControllerFromRect);
    
    CGFloat fromLeft =  CGRectGetMinX(self.fromVCFromRect);
    CGFloat toLeft =  CGRectGetMinX(self.viewControllerFromRect);
    
    CGFloat fromWidth =  CGRectGetWidth(self.fromVCFromRect);
    CGFloat toWidth =  CGRectGetWidth(self.viewControllerFromRect);
    
    CGFloat fromHeight =  CGRectGetHeight(self.fromVCFromRect);
    CGFloat toHeight =  CGRectGetHeight(self.viewControllerFromRect);
    
    CGFloat top = fromTop + (toTop - fromTop)*progress;
    CGFloat left = fromLeft + (toLeft - fromLeft)*progress;
    
    CGFloat width = fromWidth + (toWidth - fromWidth)*progress;
    CGFloat height = fromHeight + (toHeight - fromHeight)*progress;
    
    if (_currentWindow == nil) {
        _currentWindow = [[UIApplication sharedApplication] keyWindow];
    }
    
    [_fromViewController.contentViewController.playView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_currentWindow).offset(top);
        make.left.equalTo(_currentWindow).offset(left);
        
        make.width.equalTo(@(width));
        make.height.equalTo(@(height));
    }];
    
    [[OddityPlayerView sharePlayerView] mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_currentWindow).offset(top);
        make.left.equalTo(_currentWindow).offset(left);
        
        make.width.equalTo(@(width));
        make.height.equalTo(@(height));
    }];
    
    
    [_fromViewController.contentViewController.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo([OddityPlayerView sharePlayerView].mas_bottom);
        make.left.equalTo(_fromViewController.contentViewController.view);
        make.right.equalTo(_fromViewController.contentViewController.view);
        make.bottom.equalTo(_fromViewController.contentViewController.view);
    }];
    
    _fromViewController.view.backgroundColor = [_fromViewController.view.backgroundColor colorWithAlphaComponent:1-progress];
    _fromViewController.contentViewController.tableView.alpha = 1-progress;
    
    [_fromViewController.contentViewController.view layoutIfNeeded];
    [_containerView layoutIfNeeded];
}

-(void)cancelInteractiveTransition{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self updateInteractiveTransition:0];
    } completion:^(BOOL finished) {
        
        [_transitionContext completeTransition:false];
        
        _fromViewController.contentViewController.tableView.alpha = 1;
        _fromViewController.navVarView.transform = CGAffineTransformIdentity;
        
        [_fromViewController.contentViewController.playView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_fromViewController.contentViewController.mas_topLayoutGuide);
            make.left.equalTo(_fromViewController.contentViewController.view);
            make.right.equalTo(_fromViewController.contentViewController.view);
            make.height.equalTo(_fromViewController.contentViewController.playView.mas_width).multipliedBy((CGFloat)211/375);
        }];
        
        /// 将播放器 放置于 播放图片上
        [_fromViewController.contentViewController.playView addSubview:[OddityPlayerView sharePlayerView]];
        [[OddityPlayerView sharePlayerView] mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_fromViewController.contentViewController.playView);
            make.left.equalTo(_fromViewController.contentViewController.playView);
            make.right.equalTo(_fromViewController.contentViewController.playView);
            make.bottom.equalTo(_fromViewController.contentViewController.playView);
        }];
        
        [_fromViewController.contentViewController.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_fromViewController.contentViewController.playView.mas_bottom);
            make.left.equalTo(_fromViewController.contentViewController.view);
            make.right.equalTo(_fromViewController.contentViewController.view);
            make.bottom.equalTo(_fromViewController.contentViewController.view);
        }];
        
        [_fromViewController pagerTabStripViewController:_fromViewController updateIndicatorFromIndex:0 toIndex:0 withProgressPercentage:1 indexWasChanged:true];
        
        _fromViewController.navVarView.alpha = 1;
        
        [[OddityPlayerView sharePlayerView] layoutIfNeeded];
        [_fromViewController.contentViewController.view layoutIfNeeded];
        [_fromViewController.contentViewController.playView layoutIfNeeded];
    }];
}

-(void)finishInteractiveTransition{
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self updateInteractiveTransition:1];
    } completion:^(BOOL finished) {
        
        [_transitionContext completeTransition:true];
        
        [OddityTransitionHandleObject dismissUnifiedHandleMethod: self.toViewController];
    }];
}

@end
