//
//  MainManagerViewController.h
//  Corn
//
//  Created by 荆文征 on 2017/2/7.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import <Realm/Realm.h>
#import <UIKit/UIKit.h>
#import "OdditySetting.h"
#import <JXLPagerTabStrip/XLButtonBarPagerTabStripViewController.h>

@protocol OddityNewArrayListViewControllerDelegate;

@class OddityNewArrayListViewController,OddityChannelNews;
/**
 *  新闻的ViewController Delegate
 */
@protocol OddityNewArrayListViewControllerDelegate<NSObject>

@required

/**
 *   用户点击了不喜欢按钮 所触发的时间
 *   我们根据用户的所选择的 新闻，进行展示相关视图的 信息
 *   用户选择理由并且选择 新闻的 去留
 *
 *  @param tableView `UItableView` 对象
 *  @param indexPath 用户点击的IndexPath 若用户选择了 留，那么需要该属性 进行 `UItableView` 的刷新
 *  @param cell      cell 对象， 首先用来展示，并且 如果用户选择了 去，我们需要根据 cell 中的 Object进行新闻的删除
 *  @param channel   频道对象
 */
-(void)didClickDisLikeButton:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath cell:(UITableViewCell *) cell channels:(OddityChannelNews *)channel;

@end

@interface OddityMainManagerViewController : XLButtonBarPagerTabStripViewController

/**
 *  视图的唯一表示
 */
@property(strong,nonatomic) NSString *uniqueIdentity;

/**
 *  视图的 Navagtion View
 */
@property (strong, nonatomic) UIView *navView;

/**
 *  是否消除
 */
@property(nonatomic,assign,readonly)BOOL isDisappear;


/**
 当前 Index
 */
@property(nonatomic,assign)NSString *currentIndexString;

/**
 *  请不要调用 该方法为刷新视频状态
 */
-(void)refreshPlayViewState;

/**
 当前展示的 ViewController

 @return 当前的新闻列表视图
 */
-(OddityNewArrayListViewController *)currentViewController;


/**
 播放视频

 @param arrayVC 播放视图的位置
 @param index 播放的视频位置
 */
-(void)playVideo:(OddityNewArrayListViewController *)arrayVC playIndex:(NSInteger)index;

@end

