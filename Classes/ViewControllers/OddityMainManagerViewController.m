//
//  MainManagerViewController.m
//  Corn
//
//  Created by 荆文征 on 2017/2/7.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import <objc/runtime.h>

#import "OddityModels.h"
#import "OddityUtils.h"
#import "OddityPlayerView.h"
#import "OddityCategorys.h"
#import <Masonry/Masonry.h>
#import "OddityCustomTableViewCell.h"
#import "OdditySubscribeViewController.h"
#import "OddityReasonViewController.h"

#import "OddityNewsDetailViewController.h"
#import "OddityNewArrayListViewController.h"
#import "OddityChannelManagerViewController.h"

#import "OddityMainManagerViewController.h"

#import "OddityNikeNameViewController.h"
#import "OddityUserCenterViewController.h"
#import "OddityUserLoginViewController.h"

#import "OdditySubNewArrayViewController.h"

#import "OddityGapViewController.h"

#import <PINCache/PINCache.h>
#import <PINRemoteImage/PINRemoteImage.h>

#define Oddity_Current_Index_String @"oddityCurrentIndexString"

@interface OddityMainManagerViewController ()<UICollectionViewDelegate>

/**
 *  视图的 忽略通知
 */
@property (nonatomic, retain) RLMNotificationToken *notificationToken;

@property(nonatomic,strong)  UIView *userCenterBackView;

@property(nonatomic,strong) UIButton *managerButton;
@property(nonatomic,strong) OddityThemeImageView *userCenterImageView;

@property(nonatomic,strong) NSDate *startAppUseDateTime;

@property(nonatomic,strong) RLMArray<OddityChannel *> *channels;


@end

@implementation OddityMainManagerViewController

-(BOOL)shouldAutorotate{

    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{

    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{

    return UIInterfaceOrientationPortrait;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(OddityNewArrayListViewController *)currentViewController{

    UIViewController *controller = [[self childViewControllersForPagerTabStripViewController:self] objectAtIndex:self.currentIndex];
    
    if ([controller isKindOfClass:[OdditySubNewArrayViewController class]]) {
        
        OdditySubNewArrayViewController *viewController = (OdditySubNewArrayViewController *)controller;
        
        return [[viewController childViewControllersForPagerTabStripViewController:viewController] objectAtIndex:viewController.currentIndex];
    }
    
    return (OddityNewArrayListViewController *)controller;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    _isDisappear = true;
    
    BOOL isMustHidden = ![OdditySetting shareOdditySetting].isWatchVideoAllTheTime;
    
    if (isMustHidden) {
        
        if (self.presentedViewController && [self.presentedViewController isKindOfClass:[OddityNewsDetailViewController class]] && ((OddityNewsDetailViewController *)self.presentedViewController).isVideo) {
            return;
        }
        
        UIViewController *viewController = [UIViewController oddityCurrentViewController];
        
        if (viewController && [viewController isKindOfClass:[OddityNewsDetailViewController class]] && ((OddityNewsDetailViewController *)viewController).isVideo) {
            return;
        }
        
        [[OddityPlayerView sharePlayerView] closePlayerView];
    }
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        [self initPubMethod];
    }
    return self;
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{

    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [self initPubMethod];
    }
    return self;
}

-(void)initPubMethod{

    
    _uniqueIdentity = [[NSUUID UUID] UUIDString];
    
    _channels = [OddityNewsArrayObject channelManager].normalChannelArray;
    
    self.automaticallyAdjustsScrollViewInsets = false;
}

-(void)viewDidLoad{
    
    [super viewDidLoad];
    
    [PINRemoteImageManager.sharedImageManager.cache.diskCache trimToSizeByDate:10 block:nil];
    
    [self SimpleAndCrude];
        
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
}

-(void)refreshThemeMethod{
    
    [self.buttonBarView reloadData];
    
    self.userCenterBackView.backgroundColor = OddityUISetting.shareOddityUISetting.backColor;
    
    [self setNeedsStatusBarAppearanceUpdate];
    self.containerView.backgroundColor = [UIColor color_t_9];
    
    self.managerButton.tintColor = [OddityUISetting shareOddityUISetting].titleColor;
    self.managerButton.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    
    if (OddityShareUser.defaultUser.isShowUserCenter) {
        
        [self.userCenterImageView pin_setImageFromURL:OddityShareUser.defaultUser.avatorUrl placeholderImage:UIImage.oddity_mainvc_userCenter_image];
    }
    
    self.navView.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    
    [self.userCenterImageView layoutSubviews];
    
    self.buttonBarView.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    self.buttonBarView.selectedBar.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
}

-(void)SimpleAndCrude{
    
    __weak typeof(self) weakSelf = self;
    _startAppUseDateTime = [NSDate date];
    
    [self oddity_showWaitView:0];
    
    [OddityShareUser requestToken:^(NSString *token) {
        
        if (token) {
            
            [weakSelf EnsureCompletionOfTasks];
        }else{
        
            [weakSelf.waitView goErrorStyle:^{
                
                [weakSelf SimpleAndCrude];
            }];
        }
    }];
}

-(void)EnsureCompletionOfTasks{
    
    __weak typeof(self) weakSelf = self;
    [OddityShareUser userConfigurationInformation];
    
    [self makeNotifition];
    
    [[OddityManagerInfoMationUtil managerInfoMation] StartModfiyeInfoMation];
    
    /// 打点 -> 检查日志 并且 如果没有上传，就上传日志
    [OddityLogObject checkoutAndUploadMethod];
    
    [OddityChannel reloadDataSourceSort:nil];
    
    self.isProgressiveIndicator = true;
    self.buttonBarView.selectedBarHeight = 2;
    
    self.buttonBarView.selectedBar.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
    self.skipIntermediateViewControllers = true;
    self.changeCurrentIndexProgressiveBlock = ^void(XLButtonBarViewCell *oldCell, XLButtonBarViewCell *newCell, CGFloat progressPercentage, BOOL changeCurrentIndex, BOOL animated){
        if (changeCurrentIndex) {
            
            newCell.label.textColor = [OddityUISetting shareOddityUISetting].mainTone;
            oldCell.label.textColor = [OddityUISetting shareOddityUISetting].titleColor;
        }
    };
    
    self.buttonBarView.selectedBarAlignment = XLSelectedBarAlignmentCenter;
    self.buttonBarView.shouldCellsFillAvailableWidth = true;
    self.buttonBarView.leftRightMargin = 6;
    
    [self.buttonBarView reloadData];
    
    ((UICollectionViewFlowLayout *)self.buttonBarView.collectionViewLayout).sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
    
    [self makeViewToShowForScreen];
    [self makeVolumeNotifitionMethod];
    [self makeVideoPlayFinishedMethod];
    
    self.notificationToken = [_channels addNotificationBlock:^(RLMArray<OddityChannel *> * _Nullable array, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if ( !error ) {
            
            if (error || !changes) { return; }
            
            if ( changes.deletions.count || changes.insertions.count || changes.modifications.count ) {
                
                [weakSelf reloadPagerTabStripView];
            }
        }
    }];
    
    OddityPlayerView.sharePlayerView.mainManagerViewController = self;
    
    self.containerView.delegate = self;
    self.buttonBarView.delegate = self;
}

-(void)setCurrentIndexString:(NSString *)currentIndexString{

    NSString *currenIndex =  objc_getAssociatedObject(self, Oddity_Current_Index_String) ?: @"0";
    
    if ([currenIndex isEqualToString:currentIndexString]) {  return; }
    
    [OddityLogObject createAppAction:OddityLogAtypeChangeChannel fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageFeedPage effective:false params:@{@"fchid":currenIndex,@"tchid":currentIndexString}];
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didChangeChannelActionFromIndex:toIndex:)]) {
        
        [[OdditySetting shareOdditySetting].delegate didChangeChannelActionFromIndex:[currenIndex integerValue] toIndex:[currentIndexString integerValue]];
    }
    
    objc_setAssociatedObject(self, Oddity_Current_Index_String, currentIndexString, OBJC_ASSOCIATION_COPY);
}

-(void)reloadCellForFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
    
    UICollectionViewCell *toCell = [self.buttonBarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:toIndex inSection:0]];
    UICollectionViewCell *fromCell = [self.buttonBarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:fromIndex inSection:0]];
    
    if ([toCell isKindOfClass:[XLButtonBarViewCell class]]) {
        
        ((XLButtonBarViewCell *)toCell).label.textColor = [OddityUISetting shareOddityUISetting].mainTone;
    }
    
    if ([fromCell isKindOfClass:[XLButtonBarViewCell class]]) {
        
        ((XLButtonBarViewCell *)fromCell).label.textColor = [OddityUISetting shareOddityUISetting].titleColor;
    }
}

/// 切换的对象

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (self.channels.count <= 0) { return; }
    
    [self moveToViewControllerAtIndex:indexPath.row];
    
    [self reloadCellForFromIndex:self.currentIndex toIndex:indexPath.row];
    
    self.currentIndexString = [NSString stringWithFormat:@"%d",((OddityNewArrayListViewController *)[self childViewControllersForPagerTabStripViewController:self][indexPath.row]).channel.channel.id];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{

    if (scrollView == self.containerView && [self.currentViewController isKindOfClass:[OddityNewArrayListViewController class]]) {
        
        self.currentIndexString = [NSString stringWithFormat:@"%d",self.currentViewController.channel.channel.id];
    }
}


/// 设置当视频播放完成之后
-(void)makeVideoPlayFinishedMethod{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayFinishedHandleMethod) name:oddityVideoIsFinishPlayerEd object:nil];
}

/// 处理当视频播放完成后 判断是需要 继续播放 还是需要 处理一些其他事情
-(void)videoPlayFinishedHandleMethod{
    
    if ([UIViewController.oddityCurrentViewController isKindOfClass:[OddityNewsDetailViewController class]] && OddityPlayerView.sharePlayerView.playViewState == OddityPlayViewStateFull) {
        
        [OddityPlayerView.sharePlayerView recoveryPlayViewNoramlState];
        
        [[OddityPlayerView sharePlayerView] seekToTime:0 completionHandler:^(BOOL finished) {
            
            [[OddityPlayerView sharePlayerView] Pause];
        }];
        
        return;
    }
    
    OddityNewArrayListViewController *arrayVC = [self currentViewController];

    BOOL isInMainViewController = !self.isDisappear && !self.presentedViewController && ![[UIViewController oddityCurrentViewController] isKindOfClass:[OddityNewsDetailViewController class]];
    
    if (arrayVC && arrayVC.channel.channel.isVideoChannel && isInMainViewController) {
        
        NSInteger index =  [arrayVC.videoFilterResults indexOfObject:[OddityPlayerView sharePlayerView].videoObject.object];
        
        if (index != NSNotFound && arrayVC.videoFilterResults.count > index+1) { // 继续播放处理
            
            [self playVideo:arrayVC playIndex:index+1];
            
            return;
        }
    }
    
    /// 处理不需要继续向下播放的视图
    if ([OddityPlayerView sharePlayerView].playViewState == OddityPlayViewStateSmall ) {
        
        [[OddityPlayerView sharePlayerView] toEmptyState];
        
        [UIView animateWithDuration:0.3 animations:^{
            
            [OddityPlayerView sharePlayerView].alpha = 0;
        } completion:^(BOOL finished) {
            
            [[OddityPlayerView sharePlayerView] removeFromSuperview];
            
            [OddityPlayerView sharePlayerView].alpha = 1;
        }];
    }else{
        
        [[OddityPlayerView sharePlayerView] seekToTime:0 completionHandler:^(BOOL finished) {
            
            [[OddityPlayerView sharePlayerView] Pause];
        }];
    }
}

-(void)playVideo:(OddityNewArrayListViewController *)arrayVC playIndex:(NSInteger)index{

    OddityNew *new =  [arrayVC.videoFilterResults objectAtIndex:index];
    
    switch ([OddityPlayerView sharePlayerView].playViewState) {
            
        case OddityPlayViewStateNormal: case OddityPlayViewStateFull:{
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[arrayVC.results indexOfObject:new] inSection:0];
            
            arrayVC.isAutoPlayVideoWithFullState = true;
            
            [UIView animateWithDuration:0.6 animations:^{
                
                [arrayVC.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:(UITableViewScrollPositionTop) animated:false];
                
            } completion:^(BOOL finished) {
                
                arrayVC.isAutoPlayVideoWithFullState = false;
                
                OddityNewVideoTableViewCell *ncell = [arrayVC.tableView cellForRowAtIndexPath:indexPath];
                
                [self autoPlayVideoMethod:new backView: ncell.firstImageView];
                
                [arrayVC autoScrollerCollectionView]; // 自动滑动
            }];
        }
            break;
        case OddityPlayViewStateSmall:{
            
            [self autoPlayVideoMethod:new backView: nil];
            
            [self refreshPlayViewState];
        }
            break;
        default:
            break;
    }
}


-(void)autoPlayVideoMethod:(OddityNew *)newObj backView:(UIView *)bv{
    
    OddityPlayerView *playView = [OddityPlayerView sharePlayerView];
    OddityPlayViewState state = playView.playViewState;
    /// 打点 -> 点击新闻打点
    [OddityLogObject createNewsClick:newObj.nid chid: newObj.channel logtype:newObj.logtype logchid:newObj.logchid source: OdditySourceFeedName];
    
    
    OddityPlayerObject *playObject = [[OddityPlayerObject alloc] initWith:newObj.nid VideoUrl:newObj.videourl Title:newObj.title Objtect:newObj];
    [[OddityPlayerView sharePlayerView] playWithPlayerObject:playObject];
    
    [playView toState:state inView:bv];
    playView.oldPlayBackView = bv;
}

-(void)makeVolumeNotifitionMethod{
    
    MPVolumeView *volumeView = [[MPVolumeView alloc]initWithFrame:CGRectMake(-100, -100, 100, 100)];
    [self.view addSubview:volumeView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
}


- (void)volumeChanged:(NSNotification *)notification {
    
    OddityPlayerView *playView = [OddityPlayerView sharePlayerView];
    
    [playView makeNoMuteMthod];
    
    BOOL isNormalDraged = !playView.isDragged && playView.playViewState != OddityPlayViewStateSmall; /// 是否在不是小视图的并且没有拖拽
    
    if (playView.isPlayerViewShow && (playView.playViewState == OddityPlayViewStateSmall || isNormalDraged)) {
        
        CGFloat currVloume = [[[notification userInfo] objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"] floatValue];
        
        if (playView.playViewState == OddityPlayViewStateSmall) {
            
            [OddityPlayerView showSoundViewInView:UIApplication.sharedApplication.keyWindow value:currVloume];
        }else{
        
            [OddityPlayerView showSoundViewInView:playView value:currVloume];
        }
        
    }
}

/// 刷新播放视图的
-(void)refreshPlayViewState{

    if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
        
        [[OddityPlayerView sharePlayerView] toNeedJudgmentState];
        [[self currentViewController] rectifyPlayViewPointMethod];
    }
}

-(void)pagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController updateIndicatorFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex withProgressPercentage:(CGFloat)progressPercentage indexWasChanged:(BOOL)indexWasChanged{

    [super pagerTabStripViewController:pagerTabStripViewController updateIndicatorFromIndex:fromIndex toIndex:toIndex withProgressPercentage:progressPercentage indexWasChanged:indexWasChanged];

    if (indexWasChanged) {
        
        if (![OdditySetting shareOdditySetting].isWatchVideoAllTheTime) {
            
            [[OddityPlayerView sharePlayerView] closePlayerView];
        }
        
        [self refreshPlayViewState];
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
    
        XLButtonBarViewCell *oldCell = (XLButtonBarViewCell *)[self.buttonBarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:fromIndex inSection:0]];
        XLButtonBarViewCell *newCell = (XLButtonBarViewCell *)[self.buttonBarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:toIndex inSection:0]];
        
        UIColor *oldColor = [OddityUISetting shareOddityUISetting].titleColor;;
        UIColor *newColor = [OddityUISetting shareOddityUISetting].mainTone;
        
        oldCell.label.textColor = [UIColor oddity_interpolateHSVColorFrom: newColor to: oldColor withFraction:progressPercentage];
        newCell.label.textColor = [UIColor oddity_interpolateHSVColorFrom: oldColor to: newColor withFraction:progressPercentage];
    }
}

-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (OddityChannel *channel in _channels) {
        
        [array addObject:[[OddityListViewControllerCacher sharedCache] getNewViewControllerBy:channel viewController:self]];
    }
    
    if (array.count <= 0 ) {
    
        OddityGapViewController *gapVC = [[OddityGapViewController alloc] init];
        
        return @[gapVC];
    }
    
    return array;
}

-(void)makeViewToShowForScreen{

    _navView = [[UIView alloc]init];
    [self.view addSubview:_navView];
    [_navView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    _navView.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    
    _managerButton = [UIButton buttonWithType:UIButtonTypeSystem];
    _managerButton.tintColor = [OddityUISetting shareOddityUISetting].titleColor;
    _managerButton.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    
    [_managerButton setImage:[UIImage oddity_mainvc_addchannel_image] forState:UIControlStateNormal];
    [_navView addSubview:_managerButton];
    
    [_managerButton addTarget:self action:@selector(clickManagerButtonMethod) forControlEvents:(UIControlEventTouchUpInside)];
    
    [_managerButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.bottom.equalTo(@-1);
        make.right.equalTo(@0);
        make.width.equalTo(@44);
        make.height.equalTo(@44);
    }];
    
    [self.buttonBarView removeFromSuperview];
    
    ((UICollectionViewFlowLayout *)self.buttonBarView.collectionViewLayout).sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    [_navView addSubview:self.buttonBarView];
    
    if (OddityShareUser.defaultUser.isShowUserCenter) { // 如果需要展示个人中心
        
        self.userCenterBackView = [[UIView alloc] init];
        self.userCenterBackView.backgroundColor = OddityUISetting.shareOddityUISetting.backColor;
        [self.navView addSubview:self.userCenterBackView];
        [self.userCenterBackView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickUserCenterImageView)]];
        [self.userCenterBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.bottom.equalTo(@-1);
            make.left.equalTo(@0);
            make.width.equalTo(@44);
            make.height.equalTo(@44);
        }];
        
        self.userCenterImageView = [[OddityThemeImageView alloc] init];
        self.userCenterImageView.clipsToBounds = true;
        self.userCenterImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.userCenterImageView.layer.cornerRadius = 14;
        [self.userCenterBackView addSubview:self.userCenterImageView];
        [self.userCenterImageView pin_setImageFromURL:OddityShareUser.defaultUser.avatorUrl placeholderImage:UIImage.oddity_mainvc_userCenter_image];
        [self.userCenterImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.centerX.equalTo(self.userCenterBackView);
            make.centerY.equalTo(self.userCenterBackView);
            make.width.equalTo(@28);
            make.height.equalTo(@28);
        }];
    }
    
    
    self.buttonBarView.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    
    [self.buttonBarView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.equalTo(@-1);
        make.right.equalTo(_managerButton.mas_left);
        
        if (OddityShareUser.defaultUser.isShowUserCenter) {
            
            make.left.equalTo(_userCenterImageView.mas_right);
        }else{
            
            make.left.equalTo(_navView);
        }
        
        make.height.equalTo(@44);
    }];
    
    [self.containerView removeFromSuperview];
    [self.view addSubview:self.containerView];
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo(_navView.mas_bottom);
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    [self.view layoutIfNeeded];
}

-(void)clickManagerButtonMethod{
    
    if (self.channels.count <= 0) { return; }
    
    [OddityLogObject createAppAction:OddityLogAtypeChannelPageClick fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageChannelPage effective:false params:nil];
    
    [[UIViewController oddityCurrentViewController] presentViewController:[[OddityChannelManagerViewController alloc]init] animated:true completion:nil];
}

-(void)clickUserCenterImageView{
    
    if (self.channels.count <= 0) { return; }
    
    if (OddityShareUser.defaultUser.utype == 2) {
        
        [self presentViewController:[[OddityUserLoginViewController alloc]init] animated:true completion:nil]; // 登陆
    }else{
        
        [OddityLogObject createAppAction:OddityLogAtypeProfileClick fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageUserCenterPage effective:false params:nil];
        
        [self presentViewController:[[OddityUserCenterViewController alloc]init] animated:true completion:nil]; // 个人中心
    }
}

-(void)makeNotifition{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationWillResignActiveNotification object:nil];
    // app进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterPlayground) name:UIApplicationDidBecomeActiveNotification object:nil];
}


/**
 应用进入不活跃状态
 */
-(void)appDidEnterBackground{

    long long endTime = [[NSDate date] oddity_unixInteger];
    long long startTime = [self.startAppUseDateTime oddity_unixInteger];
    
    // 打点 -> 应用时长打点
    [OddityLogObject createAppUse: startTime endTime:endTime useTime:endTime-startTime];
    
    self.startAppUseDateTime = NSDate.date;
}


/**
 应用进入活跃状态
 */
-(void)appDidEnterPlayground{

    self.startAppUseDateTime = [NSDate date];
}


/**
 修改 presentViewControllerWithCurrent

 @param animated 动画
 */
-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    
    if ([OddityPlayerView sharePlayerView].isPlayerViewShow) {
        
        [self refreshPlayViewState];
    }
    
    if ([OdditySubscribeObject shouldShow] && ![OdditySetting shareOdditySetting].isHiddenFirstSubscribe) {
        
        OdditySubscribeViewController *viewController = [[OdditySubscribeViewController alloc]init];
        
        [viewController presentViewControllerWithCurrent];
    }
}

/**
 应用本也进入活跃状改

 @param animated 动画
 */
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    _isDisappear = false;
    
    if (OddityShareUser.defaultUser.isShowUserCenter) {
        
        [self.userCenterImageView pin_setImageFromURL:OddityShareUser.defaultUser.avatorUrl placeholderImage:UIImage.oddity_mainvc_userCenter_image];
    }
    
    if (!self.presentedViewController) {
        
        self.startAppUseDateTime = [NSDate date];
    }
}


/**
 应用离开本页面  进入不活跃状态

 @param animated <#animated description#>
 */
-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    
    if (!self.presentedViewController) {
        
        [self appDidEnterBackground];
    }
}

@end


