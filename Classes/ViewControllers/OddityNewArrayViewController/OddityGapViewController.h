//
//  OddityGapViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/7/7.
//
//

#import <UIKit/UIKit.h>
#import <JXLPagerTabStrip/XLButtonBarPagerTabStripViewController.h>

@interface OddityGapViewController : UIViewController<XLPagerTabStripChildItem>

@end
