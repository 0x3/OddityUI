//
//  OddityGapViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/7/7.
//
//

#import <Masonry/Masonry.h>
#import "OddityGapViewController.h"
#import "OddityNewReserveTableViewCell.h"

@interface OddityGapViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;

@property(nonatomic,strong) UITableView *tableView;

@end

@implementation OddityGapViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tableView = [[UITableView alloc] initWithFrame:(CGRectZero) style:(UITableViewStylePlain)];
    
    [self.tableView registerClass:[OddityNewReserveTableViewCell class] forCellReuseIdentifier:@"nocontent"];
    
    self.tableView.scrollEnabled = false;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.view addSubview:self.tableView];
    
    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 20;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nocontent"];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 107;
}

- (NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController {
    
    return @"奇点";
}

@end



