//
//  OddityHuangLiArrayListViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/6/29.
//
//

#import "OddityNewArrayListViewController.h"

@interface OddityHuangLiArrayListViewController : OddityNewArrayListViewController

- (instancetype)initWithChannel:(OddityChannelNews *)channel title:(NSString *)title clearBackColor:(BOOL)cbc;
@end
