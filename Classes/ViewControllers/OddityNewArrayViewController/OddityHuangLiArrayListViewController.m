//
//  OddityHuangLiArrayListViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/29.
//
//

#import "OddityLogObject.h"

#import "OddityCategorys.h"

#import <Masonry/Masonry.h>

#import "OddityNewsDetailViewController.h"
#import "OddityNavigationBarView.h"
#import "OddityReasonViewController.h"

#import "OddityHuangLiArrayListViewController.h"

@interface OddityHuangLiArrayListViewController ()

@property(nonatomic,assign) BOOL clearBackColor;
@property(nonatomic,strong) NSString *otitle;

@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) OddityNavigationBarView *navVarView;

@property(nonatomic,strong) UIView *replaceView;
@end

@implementation OddityHuangLiArrayListViewController

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    
    [OddityLogObject createAppAction:OddityLogAtypeBackFeed fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:false params:nil];
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didAppearHuangLiLonelyNewArrayViewController)] ){
        
        [[OdditySetting shareOdditySetting].delegate didAppearHuangLiLonelyNewArrayViewController];
    }
}

-(void)refreshThemeMethod{

    [super refreshThemeMethod];
    
    self.view.backgroundColor = OddityUISetting.shareOddityUISetting.backColor;
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (instancetype)initWithChannel:(OddityChannelNews *)channel title:(NSString *)title clearBackColor:(BOOL)cbc{

    self = [super initWithChannel:channel];
    
    if (self) {
        
        self.otitle = title;
        
        self.clearBackColor = cbc;
        
        self.modalPresentationStyle = UIModalPresentationCustom;
    }
    
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.replaceView = self.view;
    self.view = [UIView.alloc  init];
    [self.view addSubview:self.replaceView];
    self.view.backgroundColor = OddityUISetting.shareOddityUISetting.backColor;
    
    __weak typeof(self) weakSelf = self;
    self.navVarView = [[OddityNavigationBarView alloc]init:self.otitle titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
        
        [OddityLogObject createAppAction:OddityLogAtypeJumpBack fromPageName:OddityLogPageNullPage toPageName:OddityLogPageNullPage effective:false params:nil];
        
        if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didDismissHuangLiLonelyNewArrayViewController)] ){
            
            [[OdditySetting shareOdditySetting].delegate didDismissHuangLiLonelyNewArrayViewController];
        }
    }];
    [self.view addSubview:self.navVarView];
    
    [self.navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@44);
    }];

    [self.replaceView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.navVarView.mas_bottom);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    if (self.clearBackColor) {
        
        self.view.alpha = 0;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            weakSelf.view.alpha = 1;
        });
    }
    
    [self.view layoutIfNeeded];
}

@end
