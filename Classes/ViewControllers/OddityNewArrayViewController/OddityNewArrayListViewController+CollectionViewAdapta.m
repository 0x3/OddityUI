//
//  OddityNewArrayListViewController+CollectionViewAdapta.m
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import "RLMRealm+Oddity.h"
#import "UIViewController+Oddity.h"

#import "UIColor+Theme.h"
#import "OddityPlayerView.h"
#import <MJRefresh/MJRefresh.h>
#import "OddityPlaylistCollectionViewCell.h"
#import "OdditySubChannelCollectionViewCell.h"
#import "OddityNewArrayListViewController+CollectionViewAdapta.h"

@implementation OddityNewArrayListViewController (CollectionViewAdapta)

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return self.videoFilterResults.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    OddityNew *nObj = self.videoFilterResults[indexPath.row];
    
    OddityPlaylistCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    [cell setModel:nObj];
    
    NSInteger playIndex = [self.videoFilterResults indexOfObject:[OddityPlayerView sharePlayerView].videoObject.object];
    
    cell.backgroundColor = [[UIColor color_t_13] colorWithAlphaComponent:0.6];
    
    cell.willPlayLabel.hidden = true;
    
    if (indexPath.row == playIndex) {
        
        cell.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
        
    }else if(indexPath.row == playIndex+1){
    
        
        cell.willPlayLabel.hidden = false;
        cell.backgroundColor = [[OddityUISetting shareOddityUISetting].mainTone colorWithAlphaComponent:0.4];
    }
    
    return cell;
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    return CGSizeMake(301-16, 81);
}



-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
    
    [self.managerViewController playVideo:self playIndex:indexPath.row];
    
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:(UICollectionViewScrollPositionTop) animated:true];
}


@end
