//
//  OddityNewArrayListViewController+tableViewDataSource.m
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import "OddityManagerInfoMationUtil.h"

#import "OddityCategorys.h"
#import "OddityShareViewController.h"
#import "OddityCustomTableViewCell.h"
#import "OddityNewArrayListViewController+tableViewDataSource.h"

@implementation OddityNewArrayListViewController (tableViewDataSource)

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.results ? self.results.count : 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OddityNew *new;
    
    if (self.results.count > indexPath.row) {
        
        new = [self.results objectAtIndex:indexPath.row];
    }
    
    typeof(self) __weak weakSelf = self;
    
    if (new) {
        
//        OddityPrerryCellStyle style = new.cellStyle;
//        
//        if (style == PrerryCellStyleBigAds || style == PrerryCellStyleOneAds || style == PrerryCellStyleThreeAds) {
//            
//            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//            
//            GDTNativeAd *ad = [GDTNativeAd initGDTNativeAdWithAdMode:(OddityAdvertisementPlacementModeFeedBig)];
//            
//            [new ExposureAdvertisement:cell nativeAD: ad];
//        }
    }
    
    
    UITableViewCell *cell = [self tableViewCell:tableView froNew:new];
    
    if ([cell isKindOfClass:[OddityNewBaseTableViewCell class]]) {
        
        if (new.rtype == 4) {
            
            [((OddityNewBaseTableViewCell *) cell) hiddenCloseButton];
        }
        
        [((OddityNewBaseTableViewCell *) cell).dislikeButton oddity_handleControlEvent:(UIControlEventTouchUpInside) withBlock:^{
            
            if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(didClickDisLikeButton:forIndexPath:cell:channels:)]) {
                
                [weakSelf.delegate didClickDisLikeButton:tableView forIndexPath:[tableView indexPathForCell:cell] cell:cell channels:self.channel];
            }
        }];
    }
    
    if ([cell isKindOfClass:[OddityNewVideoTableViewCell class]]) {
        
        OddityNewVideoTableViewCell *ncell = ((OddityNewVideoTableViewCell *) cell);
        
        [ncell.moreButton oddity_handleControlEvent:UIControlEventTouchUpInside withBlock:^{
            
            [[[OddityShareViewController alloc] initWithStyle:(OddityShareStyleNew) withObj:new] presentViewControllerWithCurrent];
        }];
        
        [ncell.playButton oddity_handleControlEvent:(UIControlEventTouchUpInside) withBlock:^{
            
            OddityNewVideoTableViewCell *ncell = (OddityNewVideoTableViewCell *) cell;
            
            [ncell playVideoGogo:weakSelf.channel.channel.cname];
            
            if (self.channel.channel.isVideoChannel) {
                
                [self autoScrollerCollectionView];
            }
            
#if isPlayList
            
            [self configPlayerViewPlayListView];
#endif
            
            [OddityLogObject createAppAction:OddityLogAtypeFeedVideoPlay fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageNullPage effective:true params:@{@"nid":@(new.nid)}];
            
        }];
    }
    
    if (new.rtype == 6 && self.channel.channel.isVideoChannel && [cell isKindOfClass:[OddityNewBaseTableViewCell class ]]) {
        
        ((OddityNewBaseTableViewCell *)cell).tagLabel.hidden = true;
    }
    
    return cell;
}


- (UITableViewCell *)tableViewCell:(UITableView *)tableView froNew:(OddityNew *)new {
    
    if (!new) {
        
        OddityNewReserveTableViewCell *cell = (OddityNewReserveTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"nocontent"];
        
        return cell;
    }
    
    OddityPrerryCellStyle style = [new cellStyle];
    
    if (style == PrerryCellStyleRefresh) {
        
        OddityRefreshTableViewCell *cell = (OddityRefreshTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"refresh"];
        
        cell.backgroundColor = [UIColor color_t_6];
        
        return cell;
    }
    
    if (style == PrerryCellStyleOne) {
        
        OddityNewOneTableViewCell *cell = (OddityNewOneTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"onecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    
    if (style == PrerryCellStyleOneAds) {
        
        OddityNewOneAdsTableViewCell *cell = (OddityNewOneAdsTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"oneadscell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    
    if (style == PrerryCellStyleSpecial) {
        
        OddityNewSpecialTableViewCell *cell = (OddityNewSpecialTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"special"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleTwo) {
        
        OddityNewTwoTableViewCell *cell = (OddityNewTwoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"twocell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    
    
    if (style == PrerryCellStyleThree || style == PrerryCellStyleThreeAds) {
        
        OddityNewThreeTableViewCell *cell = (OddityNewThreeTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"threecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleBig) {
        
        OddityCoverImageTableViewCell *cell = (OddityCoverImageTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"bigcell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleBigAds) {
        
        OddityCoverAdsImageTableViewCell *cell = (OddityCoverAdsImageTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"bigadscell"];
        
        [cell setConfig:new];
        
        if (self.channel.channel.isVideoChannel) {
            
            [cell makeFullMode];
        }
        
        return cell;
    }
    
    if (style == PrerryCellStyleVideo) {
        
        OddityNewVideoTableViewCell *cell = (OddityNewVideoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"video"];
        
        [cell setConfig:new];
        
        if (self.channel.channel.isVideoChannel) {
            
            [cell makeFullMode];
        }
        
        return cell;
    }
    
    OddityNewBaseTableViewCell *cell = (OddityNewBaseTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"basecell"];
    
    [cell setConfig:new];
    
    return cell;
}

@end
