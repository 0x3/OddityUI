//
//  OddityNewArrayListViewController+tableViewDelegate.h
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import "OddityNewArrayListViewController.h"

@interface OddityNewArrayListViewController (tableViewDelegate)<UITableViewDelegate>

/**
 *  检查是否需要制作为 新的信息屏幕 打点
 */
- (void)checkOutHaveToMakeAllNewScreen;
@end
