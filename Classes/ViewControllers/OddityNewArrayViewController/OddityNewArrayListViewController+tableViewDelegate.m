//
//  OddityNewArrayListViewController+tableViewDelegate.m
//  Pods
//
//  Created by 荆文征 on 2017/5/8.
//
//

#import "OddityCategorys.h"
#import "OdditySpecialViewController.h"
#import "OddityNewsDetailViewController.h"
#import "UITableView+FDTemplateLayoutCell.h"
#import "OddityNewArrayListViewController+tableViewDelegate.h"

@implementation OddityNewArrayListViewController (tableViewDelegate)

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (scrollView != self.tableView) { return; }
    
    [self AdjustSearchViewPostion];
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didEndScrollNewListAction:)]) {
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        
        [[OdditySetting shareOdditySetting].delegate didEndScrollNewListAction:self.channel.channel.cname];
#pragma clang diagnostic pop
    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if (scrollView != self.tableView) { return; }
    
    [self AdjustSearchViewPostion];
}

/**
 自动调整 ContentOffset 来隐藏 或者 显示 search Header View
 */
- (void)AdjustSearchViewPostion {
    
    CGFloat contentOffestY = self.tableView.contentOffset.y;
    CGFloat height = CGRectGetHeight(self.tableView.tableHeaderView.frame);
    
    if (contentOffestY >= 0 && contentOffestY <= height) {
        
        if (contentOffestY > height / 2) {
            
            [self.tableView setContentOffset:CGPointMake(0, height) animated:true];
        } else {
            
            [self.tableView setContentOffset:CGPointZero animated:true];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView != self.tableView) { return; }
    
    if (scrollView.panGestureRecognizer.numberOfTouches > 0) {
     
        [self hiddenMessageViewMethod];
    }
    
    [self rectifyPlayViewPointMethod];
    
    [self checkOutHaveToMakeAllNewScreen];
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didScrollNewListAction:)]) {
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wdeprecated-declarations"
        [[OdditySetting shareOdditySetting].delegate didScrollNewListAction:self.channel.channel.cname];
#pragma clang diagnostic pop
    }
}

/**
 *  检查是否需要制作为 新的信息屏幕 打点
 */
- (void)checkOutHaveToMakeAllNewScreen {
    
    __weak typeof(self) weakSelf = self;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [OddityNewsArrayObject checkWhetherToLogArray:[weakSelf.tableView oddity_depriveArray]];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [weakSelf uploadLogAdvMethodGoes];
    });
}


/**
 上传 广告的问题
 */
-(void)uploadLogAdvMethodGoes{

    for (UITableViewCell *cell in self.tableView.visibleCells) {
        
        if ([cell isKindOfClass:OddityNewBaseTableViewCell.class]) {
            
            OddityNew *new = ((OddityNewBaseTableViewCell *)cell).object;
            
            OddityPrerryCellStyle style = new.cellStyle;
            
            if (style == PrerryCellStyleBigAds || style == PrerryCellStyleOneAds || style == PrerryCellStyleThreeAds) {
                
                CGRect cellRect = [cell convertRect:cell.bounds toView: UIApplication.sharedApplication.keyWindow];
                
                if (CGRectGetMinY(cellRect) <= CGRectGetHeight(UIScreen.mainScreen.bounds) - CGRectGetHeight(cellRect)/2) {
                    
                    if (![OddityNewsArrayObject checkWhetherToUpload: new]) {
                        
                        [new ExposureAdvertisement:cell nativeAD: [GDTNativeAd initGDTNativeAdWithAdMode:(OddityAdvertisementPlacementModeFeedBig)]];
                    }
                }
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (ODDITY_SYSTEM_GREAT_OR_EQUAL(9.0)) {
        
        return UITableViewAutomaticDimension;
    } else {
        
        return [self tableViewHeightCalendar:tableView forIndexPath:indexPath];
    }
}

- (CGFloat)tableViewHeightCalendar:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath {
    
    OddityNew *new;
    
    if (self.results.count > indexPath.row) {
        
        new = [self.results objectAtIndex:indexPath.row];
    }
    
    if (!new) {
        
        return [tableView fd_heightForCellWithIdentifier:@"nocontent" configuration:^(id cell) {
            // configurations
        }];
    }
    
    OddityPrerryCellStyle style = [new cellStyle];
    
    if (style == PrerryCellStyleRefresh) {
        
        return [tableView fd_heightForCellWithIdentifier:@"refresh" configuration:^(id cell) {
            // configurations
        }];
    }
    
    if (style == PrerryCellStyleSpecial) {
        
        return [tableView fd_heightForCellWithIdentifier:@"special" configuration:^(id cell) {
            
            [(OddityNewSpecialTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleOne) {
        
        return [tableView fd_heightForCellWithIdentifier:@"onecell" configuration:^(id cell) {
            
            [(OddityNewOneTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleOneAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"oneadscell" configuration:^(id cell) {
            
            [(OddityNewOneAdsTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleTwo) {
        
        return [tableView fd_heightForCellWithIdentifier:@"twocell" configuration:^(id cell) {
            
            [(OddityNewTwoTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleThree || style == PrerryCellStyleThreeAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"threecell" configuration:^(id cell) {
            
            [(OddityNewThreeTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleBig) {
        
        return [tableView fd_heightForCellWithIdentifier:@"bigcell" configuration:^(id cell) {
            
            [(OddityCoverImageTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleBigAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"bigadscell" configuration:^(id cell) {
            
            [(OddityCoverAdsImageTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleVideo) {
        
        return [tableView fd_heightForCellWithIdentifier:@"video" configuration:^(id cell) {
            
            [(OddityNewVideoTableViewCell *) cell setConfig:new];
        }];
    }
    
    
    return [tableView fd_heightForCellWithIdentifier:@"basecell" configuration:^(id cell) {
        
        [(OddityNewBaseTableViewCell *) cell setConfig:new];
    }];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.results.count > indexPath.row) {
        
        OddityNew *new = [self.results objectAtIndex:indexPath.row];
        
        OddityPrerryCellStyle style = new.cellStyle;
        

        if (style == PrerryCellStyleVideo) {
            
            OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc] initWithNid:new.nid isVideo:true managerViewController:self.managerViewController];
            
            [newDACViewController configVideoUrl:new.videourl Object:new];
            [newDACViewController makeLogType:new.logtype AndLogChid:new.logchid cid:self.channel.channel.id];
            
            if (ODDITY_SYSTEM_LESS_THAN(8)) {
                
                newDACViewController.videoPresentdAnimation.viewControllerFromRect = [OddityPlayerView.sharePlayerView convertRect:OddityPlayerView.sharePlayerView.bounds toView:UIApplication.sharedApplication.keyWindow];
            }
            
            /// 如果 当前的播放器没有显示 活着 当前播放器的 播放对象 不是现在的 点击的 新闻对象，那么都需要重新播放
            if (![[OddityPlayerView sharePlayerView].videoObject.object isEqual:new] || ![OddityPlayerView sharePlayerView].isPlayerViewShow) {
                
#if isPlayList
                
                [self configPlayerViewPlayListView];
#endif
                
                OddityNewVideoTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                
                /// 打点 -> 点击新闻打点
                [OddityLogObject createNewsClick:new.nid chid: new.channel logtype:new.logtype logchid:new.logchid source:OdditySourceFeedName];
                [cell playVideoGogo:self.channel.channel.cname];
                
                if (self.channel.channel.isVideoChannel) {
                    
                    [self autoScrollerCollectionView];
                }
            }
            
            [UIViewController.oddityCurrentViewController presentViewController:newDACViewController animated:true completion:^{
                
                [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
            }];
            
            return;
        }
        
        if (style == PrerryCellStyleRefresh) {
            
            [UIView animateWithDuration:0.3 animations:^{
                
                self.tableView.contentOffset = CGPointZero;
                [self.tableView.mj_header beginRefreshing];
                
            }                completion:^(BOOL finished) {
                
            }];
            
            return;
        }
        
        if (style == PrerryCellStyleBigAds || style == PrerryCellStyleOneAds || style == PrerryCellStyleThreeAds) {
            
            // 打点 -> 点击广告打点
            [OddityLogObject createAdsClick:new];
            
            /// 广告处理
            [new ClickAdvertising:tableView indexPath:indexPath nativeAD:[GDTNativeAd initGDTNativeAdWithAdId:new.advertisementId] viewController:self];
            
            return;
        }
        
        if (new.rtype == 4) {
            
            OdditySpecialViewController *sp = [[OdditySpecialViewController alloc] initWithTid:new.nid mainManagerVC:self.managerViewController];
            
            /// 打点 -> 点击新闻打点
            [OddityLogObject createNewsClick:new.nid chid:new.channel logtype:new.logtype logchid:new.logchid source:OdditySourceFeedName];
            
            [[UIViewController oddityCurrentViewController] presentViewController:sp animated:true completion:^{
                
                [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
            }];
            
            return;
        }
        
        OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc] initWithNid:new.nid isVideo:(new.cellStyle == PrerryCellStyleVideo)];
        
        [newDACViewController makeLogType:new.logtype AndLogChid:new.logchid cid: new.channel];
        
        [[UIViewController oddityCurrentViewController] presentViewController:newDACViewController animated:true completion:^{
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
        }];
    }
}

// Objective-C version
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    // remove bottom extra 20px space.
    return CGFLOAT_MIN;
}


@end
