//
//  OddityNewArrayListViewController.h
//  Corn
//
//  Created by 荆文征 on 2017/2/8.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import "OddityModels.h"
#import <UIKit/UIKit.h>
#import "OdditySearchHeaderView.h"
#import "OddityMainManagerViewController.h"
#import <JXLPagerTabStrip/XLButtonBarPagerTabStripViewController.h>


@interface OddityNewArrayListViewController : UIViewController<XLPagerTabStripChildItem>

@property (nonatomic,strong) OdditySearchHeaderView *searchHeaderView;

@property (nonatomic,assign) BOOL isAutoPlayVideoWithFullState;

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) RLMArray<OddityNew *> *results;
@property (nonatomic,strong) RLMResults<OddityNew *> *videoFilterResults;

@property(nonatomic,weak) id<OddityNewArrayListViewControllerDelegate> delegate;

@property(nonatomic,strong) OddityChannelNews *channel;

@property(nonatomic,weak) OddityMainManagerViewController *managerViewController;

@property(nonatomic, strong) RLMNotificationToken *notificationToken;
/**
 舒心视频的位置的方法
 */
-(void)rectifyPlayViewPointMethod;


/**
 根据对象获取 对象所在 indexPath 对象

 @param obj 对象
 @return 所在 的 indexPath
 */
-(NSIndexPath *)findIndexPathWithIdObject:(id)obj;

/**
 初始化 根据 频道过渡对象

 @param channel 过度对象
 @return 实例对象
 */
- (instancetype)initWithChannel:(OddityChannelNews *)channel;


/**
 自动滑动到播放的视频哪里
 */
-(void) autoScrollerCollectionView;


/**
 刷新数据
 */
- (void)RefreshMethod;


/**
 刷新 主题
 */
-(void)refreshThemeMethod;

/**
 配置播放列表的方法
 */
-(void)configPlayerViewPlayListView;

- (void)hiddenMessageViewMethod;
@end
