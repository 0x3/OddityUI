//
//  NewArrayListViewController.m
//  Corn
//
//  Created by 荆文征 on 2017/2/8.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import "OddityDisLikeObject.h"

#import "OddityCategorys.h"
#import "OddityPlayerView.h"
#import <Realm/Realm.h>
#import "OddityCustomView.h"
#import <Masonry/Masonry.h>
#import "OddityCustomTableViewCell.h"
#import "OddityShareViewController.h"
#import "OdditySpecialViewController.h"

#import "OdditySubChannelCollectionViewCell.h"

#import "OddityNewArrayListViewController.h"

#import "OdditySearchSpursViewController.h" // 搜索页面
#import "OddityNewAdsDetailViewController.h" // 广告新闻详情
#import "OddityNewsDetailViewController.h" // 新闻详情

#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

#import "OddityCollectionViewSpringFlowLayout.h"

#import "OddityNew+Advertisement.h"

#import "OddityNewArrayListViewController+tableViewDelegate.h"
#import "OddityNewArrayListViewController+tableViewDataSource.h"
#import "OddityNewArrayListViewController+CollectionViewAdapta.h"

#define OddityMesssageViewHeight 32.0f

@interface OddityNewArrayListViewController () <UITextFieldDelegate>

/// 判断该页面是否 布置了  uitableview 也就是说 第一次 reloaddata方法结束
@property(nonatomic, assign) BOOL isReloadDataed;

/// 是否第一次加载数据完成 无论是成功还是失败
@property(nonatomic, assign) BOOL requestFirstNewDataFinish;

@property(nonatomic, strong) UILabel *messageLabel;
@property(nonatomic, strong) UIView *messageBackView;

@property(nonatomic, strong) RLMNotificationToken *subNotificationToken;
@end

@implementation OddityNewArrayListViewController


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle {

    if ([OddityPlayerView sharePlayerView].isFullScreen) {

        return UIStatusBarStyleLightContent;
    }

    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

- (instancetype)initWithChannel:(OddityChannelNews *)channel {
    
    self = [super initWithNibName:nil bundle:nil];

    if (self) {

        self.channel = channel;
    }

    return self;
}

-(void)refreshFontModeMethod{
    
    [self.tableView reloadData];
}

-(void)refreshThemeMethod{
    
    self.view.backgroundColor = [UIColor color_t_9];
    self.tableView.backgroundColor = [UIColor color_t_9];
    
    [_searchHeaderView makeFeed];
    
    [self.tableView reloadData];
    
    [self scrollViewDidScroll:self.tableView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self makeMessageLabel];
    
    _tableView = [[UITableView alloc] initWithFrame:(CGRectZero) style:(UITableViewStylePlain)];
    
    self.view.backgroundColor = [UIColor color_t_9];
    self.tableView.backgroundColor = [UIColor color_t_9];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil]; // 主题发生变化
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFontModeMethod) name:@"oddityFontModeChange" object:nil]; // 字体大小发生变化
    
    
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];

    [_tableView registerClass:[OddityNewVideoTableViewCell class] forCellReuseIdentifier:@"video"];
    [_tableView registerClass:[OddityNewSpecialTableViewCell class] forCellReuseIdentifier:@"special"];
    [_tableView registerClass:[OddityRefreshTableViewCell class] forCellReuseIdentifier:@"refresh"];
    [_tableView registerClass:[OddityNewReserveTableViewCell class] forCellReuseIdentifier:@"nocontent"];
    [_tableView registerClass:[OddityNewTwoTableViewCell class] forCellReuseIdentifier:@"twocell"];
    [_tableView registerClass:[OddityNewThreeTableViewCell class] forCellReuseIdentifier:@"threecell"];
    [_tableView registerClass:[OddityNewBaseTableViewCell class] forCellReuseIdentifier:@"basecell"];
    
    [_tableView registerClass:[OddityNewOneAdsTableViewCell class] forCellReuseIdentifier:@"oneadscell"];
    [_tableView registerClass:[OddityNewOneTableViewCell class] forCellReuseIdentifier:@"onecell"];
    
    [_tableView registerClass:[OddityCoverAdsImageTableViewCell class] forCellReuseIdentifier:@"bigadscell"];
    [_tableView registerClass:[OddityCoverImageTableViewCell class] forCellReuseIdentifier:@"bigcell"];


    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    [_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {

        make.edges.equalTo(self.view).insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];

    if (ODDITY_SYSTEM_GREAT_OR_EQUAL(9.0)) {

        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }

    [_tableView reloadData];

    // 在滑动视频的时候 如果 视频 不要滑动 就 滑动 tableView
    [self.tableView.panGestureRecognizer requireGestureRecognizerToFail:[OddityPlayerView sharePlayerView].singlePan];

//    [self configureSubChannelView];
    
#if isPlayList
    [self configPlayerViewPlayListView];
#endif
    
    self.tableView.showsVerticalScrollIndicator = false;
    
    self.delegate = OddityDisLikeObject.shareDisLikeObject;
    [self.view layoutIfNeeded];
}

-(void)makeMessageLabel{

    self.messageBackView = [[UIView alloc] init];
    [self.view addSubview:self.messageBackView];

    
    self.messageLabel = [[UILabel alloc] init];
    self.messageLabel.textColor = [UIColor whiteColor];
    self.messageLabel.font = [UIFont systemFontOfSize:14];
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.messageLabel];
    [self.messageLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view);
        make.height.mas_equalTo(OddityMesssageViewHeight);
        
        make.left.equalTo(self.view);
        make.centerX.mas_equalTo(self.view);
    }];
    
    [self.messageBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view);
        make.height.mas_equalTo(OddityMesssageViewHeight);
        
        make.left.equalTo(self.view);
        make.centerX.mas_equalTo(self.view);
    }];
}


-(void)configPlayerViewPlayListView{

    if (self.channel.channel.isVideoChannel) {
        
        OddityPlayerView.sharePlayerView.playListView.collectionView.dataSource = self;
        OddityPlayerView.sharePlayerView.playListView.collectionView.delegate = self;
        
        [OddityPlayerView.sharePlayerView.playListView.collectionView reloadData];
    }
}

-(void)autoScrollerCollectionView{

#if isPlayList
    
    NSInteger index = [self.videoFilterResults indexOfObject:[OddityPlayerView sharePlayerView].videoObject.object];
    
    if (index != NSNotFound) {
        
        [[OddityPlayerView sharePlayerView].playListView.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:(UICollectionViewScrollPositionTop) animated:true];
    }
#endif
}

- (void)configureSearchView {

    if (self.channel.channel.isVideoChannel || _searchHeaderView) {
        return;
    }
    
    _searchHeaderView = [[OdditySearchHeaderView alloc] initWithFrame:(CGRectMake(0, 0, 0, 44))];
    
    [_searchHeaderView makeFeed];
    
    _searchHeaderView.inputTextFiled.delegate = self;
    
    self.tableView.tableHeaderView = _searchHeaderView;
    self.tableView.contentOffset = CGPointMake(0, 44);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if ([OddityPlayerView sharePlayerView].isPlayerViewShow && ![OdditySetting shareOdditySetting].isWatchVideoAllTheTime) {
        
        [[OddityPlayerView sharePlayerView] closePlayerView];
    }
    
    OdditySearchSpursViewController *searchSpursViewController = [[OdditySearchSpursViewController alloc] initWithFromViewController:self];

    [[UIViewController oddityCurrentViewController] presentViewController:searchSpursViewController animated:true completion:nil];

    return false;
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];

    [self autoToRefreshDataSourceMethod:true];
}

- (void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];

    [self autoToRefreshDataSourceMethod:false];
}


- (void)autoToRefreshDataSourceMethod:(BOOL)refresh {

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(FirstRefreshDataSourceMethod) object:nil];
    if (refresh) {
        [self performSelector:@selector(FirstRefreshDataSourceMethod) withObject:nil afterDelay:0.5];
    }
}

- (void)FirstRefreshDataSourceMethod {

    if (self.isReloadDataed) {return;}

    /// 设置已经第一次请求完成了
    self.isReloadDataed = true;

    /// 制作数据变化消息方法
    [self MakeDataSourcesNotifications];

    /// 刷新
    if (_results.count <= 0) {

        [self oddity_showWaitView:0];

        [self LoadMethod];
    } else {

        [self RefreshMethod];
        
        [self configureSearchView];
    }
}


-(void)showMessageViewMethod{
    
    self.tableView.mj_header.alpha = 0;
    
    [self.messageLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(0);
    }];
    
    [self.messageBackView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(self.view).offset(100);
    }];
    
    [self.view bringSubviewToFront:self.messageBackView];
    [self.view bringSubviewToFront:self.messageLabel];
    
    [self.view layoutIfNeeded];

    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(OddityMesssageViewHeight);
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        if (self.requestFirstNewDataFinish) {
            
            self.tableView.contentOffset = CGPointZero;
        }
        
        [self.view layoutIfNeeded];
    }];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.messageBackView mas_updateConstraints:^(MASConstraintMaker *make) {
            
            make.left.mas_equalTo(self.view);
        }];
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL finished) {
        
        self.tableView.mj_header.alpha = 1;
        
        self.requestFirstNewDataFinish = true;
        
        [self performSelector:@selector(hiddenMessageViewMethod) withObject:nil afterDelay:1];
    }];
}

/// 隐藏消息内容
- (void)hiddenMessageViewMethod {

    if (self.messageLabel.frame.origin.y == OddityMesssageViewHeight) {
        return;
    }
    
    [self.tableView mas_updateConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(0);
    }];
    
    [self.messageLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(-OddityMesssageViewHeight);
    }];
    
    [self.messageBackView mas_updateConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(-OddityMesssageViewHeight);
    }];
    
    [UIView animateWithDuration:0.3 animations:^{ [self.view layoutIfNeeded]; } completion:nil];
}


- (void)RefreshMethod {

    NSDate *currentDate = [NSDate date];
    NSDate *createDate = [NSDate date];

    OddityNew *firstNew = [_results objectsWhere:@"isidentification = 0"].firstObject;

    if (firstNew) {

        createDate = firstNew.ptimes;
        currentDate = [NSDate oddity_stringToDate:firstNew.ptime];
    }
    
    typeof(self) __weak weakSelf = self;
    [self.channel requestNewDataSource:true reqDate:currentDate creDate:createDate complete:^(BOOL success, BOOL refresh, NSString *message, UIColor *backColor) {
        
        [weakSelf handleRequestNew:success refresh:refresh message:message backColor:backColor];
    }];
}


- (void)LoadMethod {

    NSDate *currentDate = [NSDate date];
    NSDate *createDate = [NSDate date];

    OddityNew *lastNew = [_results objectsWhere:@"isidentification = 0"].lastObject;

    if (lastNew) {

        createDate = lastNew.ptimes;
        currentDate = [NSDate oddity_stringToDate:lastNew.ptime];
    }
    
    typeof(self) __weak weakSelf = self;
    [self.channel requestNewDataSource:false reqDate:currentDate creDate:createDate complete:^(BOOL success, BOOL refresh, NSString *message, UIColor *backColor) {
        
        [weakSelf handleRequestNew:success refresh:refresh message:message backColor:backColor];
    }];
}

-(void)handleRequestNew:(BOOL)success refresh:(BOOL)re message:(NSString *)mess backColor:(UIColor *)color{
    
    // 打点 -> 用户动作 上拉刷新feed流 或者 下拉加载feed流
    [OddityLogObject createAppAction:re ? OddityLogAtypeRefreshFeed : OddityLogAtypeLoadFeed fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageFeedPage effective:success params:@{ @"chid": @(self.channel.channel.id) }];
    
    typeof(self) __weak weakSelf = self;
    /// 第一次请求失败
    if (!success && self.results.count <= 0) {
        
        [self.waitView goErrorStyle:^{
            
            [weakSelf LoadMethod];
        }];
        
        return;
    }
    
    [self oddity_hiddenWaitView];
    
    /// 请求失败 消失刷新视图
    if (!success) {
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }
    
    self.messageLabel.text = mess;
    self.messageLabel.textColor = UIColor.color_t_12;
    self.messageBackView.backgroundColor = color;
    
    if (!re) {return;}
    
    [self showMessageViewMethod];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hiddenMessageViewMethod) object:nil];
}



- (NSIndexPath *)findIndexPathWithIdObject:(id)obj {

    NSInteger index = [self.results indexOfObject:obj];

    if (index != NSNotFound) {

        return [NSIndexPath indexPathForRow:index inSection:0];
    }

    return nil;
}


- (void)MakeDataSourcesNotifications {

    [self.channel carvNewArrlyResults];
    
    _results = self.channel.pressListArray;
    _videoFilterResults = [self.results objectsWhere:@"rtype != 3 AND isidentification != 1"];

    [_tableView reloadData];

    __weak typeof(self) weakSelf = self;

    self.notificationToken = [_results addNotificationBlock:^(RLMArray<OddityNew *> * _Nullable array, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        UITableView *tableView = weakSelf.tableView;
        
        if (!changes) {
            [tableView reloadData];
            return;
        }
        
        if (changes.insertions.count > 0) {
            
            [weakSelf.tableView.mj_footer endRefreshing];
            [weakSelf.tableView.mj_header endRefreshing];
        }
        
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:[changes insertionsInSection:0] withRowAnimation:UITableViewRowAnimationNone];
        [tableView deleteRowsAtIndexPaths:[changes deletionsInSection:0] withRowAnimation:(UITableViewRowAnimationMiddle)];
        [tableView reloadRowsAtIndexPaths:[changes modificationsInSection:0] withRowAnimation:(UITableViewRowAnimationNone)];
        [tableView endUpdates];
        
        
        [weakSelf configureSearchView];
        
        [weakSelf rectifyPlayViewPointMethod];
    }];
    
    _tableView.mj_header = [OddityCustomRefreshHeaderView headerWithRefreshingBlock:^{

        [weakSelf RefreshMethod];
    }];

    _tableView.mj_footer = [OddityCustomRefreshFooterView footerWithRefreshingBlock:^{

        [weakSelf LoadMethod];
    }];

    [self checkOutHaveToMakeAllNewScreen];
}

- (void)rectifyPlayViewPointMethod {

    if (self.managerViewController.isDisappear || [OddityPlayerView sharePlayerView].playViewState == OddityPlayViewStateFull) {

        return;
    }

    __weak typeof(self) weakSelf = self;

    dispatch_async(dispatch_get_main_queue(), ^{

        if (weakSelf.isAutoPlayVideoWithFullState) {

            return;
        }

        if (!weakSelf.isReloadDataed) { // 在新闻没有加载完成 不至于崩溃

            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];

            return;
        }

        if (![OddityPlayerView sharePlayerView].videoObject.object) {
            return;
        }
        
        NSUInteger index = [weakSelf.results indexOfObject:[OddityPlayerView sharePlayerView].videoObject.object];

        if (index != NSNotFound) {

            UITableViewCell *cell = [weakSelf.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];

            BOOL hasCell = [weakSelf.tableView.visibleCells containsObject:cell];

            if (hasCell && [OddityPlayerView sharePlayerView].playViewState != OddityPlayViewStateNormal) {

                [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateNormal) inView:((OddityNewVideoTableViewCell *) cell).firstImageView];

                return;
            }

            if (!hasCell && [OddityPlayerView sharePlayerView].playViewState != OddityPlayViewStateSmall) {

                [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];

                return;
            }
        } else {

            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];
        }
    });
}




- (NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController {
    
    if (self.channel.channel.has_children == 1) {
        
        return @"全部";
    }
    
    return self.channel.channel.cname ?: @"奇点";
}

- (UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{

    return [OddityUISetting shareOddityUISetting].titleColor;
}

@end


