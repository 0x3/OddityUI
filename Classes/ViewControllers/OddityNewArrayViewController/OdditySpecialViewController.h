//
//  SpecialViewController.h
//  Corn
//
//  Created by 荆文征 on 2017/2/13.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OddityCustomTransitioning.h"
#import "OddityMainManagerViewController.h"

@interface OdditySpecialViewController : OddityCustomAnimationViewController

-(instancetype)initWithTid:(NSInteger)tid mainManagerVC:(OddityMainManagerViewController *)mmv;

@property(nonatomic,weak) OddityMainManagerViewController *managerViewController;
@end
