//
//  SpecialViewController.m
//  Corn
//
//  Created by 荆文征 on 2017/2/13.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import "OddityModels.h"
#import "OddityCategorys.h"
#import <Realm/Realm.h>
#import "OddityPlayerView.h"
#import "OddityCustomView.h"
#import "OddityShareViewController.h"
#import "OdditySpecialViewController.h"
#import "OddityCustomTableViewCell.h"

#import "OddityNewsDetailViewController.h"

#import "UITableView+FDTemplateLayoutCell.h"

@interface OdditySpecialViewController ()<UITableViewDelegate,UITableViewDataSource>
/// 专题ID
@property(nonatomic,assign) NSInteger tid;

@property (nonatomic, retain) RLMNotificationToken *notificationToken;

@property(nonatomic,strong) OdditySpecial *special;
@property(nonatomic,strong) OddityNavigationBarView *navVarView;

@property (nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) UIButton *moreShareButton;

@property(nonatomic,strong)NSUserActivity *activity;
@end

@implementation OdditySpecialViewController

-(void)dealloc{

    [self.notificationToken stop];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    [self makeHandOffMethod];
}

-(void)makeHandOffMethod{
    
    if (self.special) {
        
        self.activity = [[NSUserActivity alloc] initWithActivityType:@"oddity.handoff.shareurl"];
        
        self.activity.webpageURL = [NSURL URLWithString: self.special.getShareObject.shareURLString];
        
        [self.activity becomeCurrent];
    }
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(instancetype)initWithTid:(NSInteger)tid mainManagerVC:(OddityMainManagerViewController *)mmv{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        _tid = tid;
        _managerViewController = mmv;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    
    typeof(self) __weak weakSelf = self;
    _navVarView = [[OddityNavigationBarView alloc]init:@"" titleFont:[UIFont font_t_6] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_navVarView];
    [_navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    
    _tableView = [[UITableView alloc]init];
    [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    
    [_tableView registerClass:[OdditySpecialInfoTableViewHeaderView class] forCellReuseIdentifier:@"header"];
    [_tableView registerClass:[OdditySpecialTableViewCell class] forCellReuseIdentifier:@"special"];
    [_tableView registerClass:[OddityNewTwoTableViewCell class] forCellReuseIdentifier:@"twocell"];
    [_tableView registerClass:[OddityNewThreeTableViewCell class] forCellReuseIdentifier:@"threecell"];
    [_tableView registerClass:[OddityNewBaseTableViewCell class] forCellReuseIdentifier:@"basecell"];
    [_tableView registerClass:[OddityNewOneTableViewCell class] forCellReuseIdentifier:@"onecell"];
    [_tableView registerClass:[OddityCoverImageTableViewCell class] forCellReuseIdentifier:@"bigcell"];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    _tableView.backgroundColor = [UIColor color_t_5];
    
    [self.view addSubview:_tableView];
    [_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.navVarView.mas_bottom);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    _moreShareButton = [[UIButton alloc] init];
    _moreShareButton.alpha = 0;
    _moreShareButton.hidden = [OdditySetting shareOdditySetting].isDontWantToShare;
    [_moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:[OddityUISetting shareOddityUISetting].titleColor] forState:(UIControlStateNormal)];
    [self.navVarView addSubview:_moreShareButton];
    [_moreShareButton addTarget:self action:@selector(shareButtonMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [_moreShareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.navVarView);
        make.bottom.equalTo(self.navVarView);
        
        make.width.equalTo(@(44));
        make.height.equalTo(@(44));
    }];
    
    [self RequestTid];
    
    self.scrollView = self.tableView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFontModeMethod) name:@"oddityFontModeChange" object:nil];
}

-(void)refreshFontModeMethod{
    
    [self.tableView reloadData];
}

-(void)refreshThemeMethod{
    
    self.view.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    [self.tableView reloadData];
    _tableView.backgroundColor = [UIColor color_t_6];
    
    [_moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:[OddityUISetting shareOddityUISetting].titleColor] forState:(UIControlStateNormal)];
}

/// 处理点击分享按钮
-(void)shareButtonMethod{
    
    OddityShareViewController *shareViewController = [[OddityShareViewController alloc] initWithStyle:(OddityShareStyleSpecial) withObj:self.special];
    
    [[UIViewController oddityCurrentViewController] presentViewController:shareViewController animated:true completion:nil];
}

-(void)RequestTid{
    
    typeof(self) __weak weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [weakSelf oddity_showWaitView:64];
        [OdditySpecial RequestSpecialByTid:_tid finishBlock:^(OdditySpecial *special) {
            
            [weakSelf setSpecialMethod:special];
            
            
        } failBlock:^{
            
            [weakSelf.waitView goErrorStyle:^{
                
                [weakSelf RequestTid];
            }];
        }];
    });
}

-(void)setSpecialMethod:(OdditySpecial *)special{

    _moreShareButton.alpha = 1;
    
    self.special = special;
    
    [self makeHandOffMethod];
    
    __weak typeof(self) weakSelf = self;
    _notificationToken = [self.special.specialClasses addNotificationBlock:^(RLMArray<OdditySpecialClass *> * _Nullable array, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
       
        if (error) {
            NSLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        UITableView *tableView = weakSelf.tableView;
        if (!changes) {
            [tableView reloadData];
            return;
        }
        
        [weakSelf.tableView reloadData];
    }];
    
    [self oddity_hiddenWaitView];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    [_tableView reloadData];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    if (!_special ) {
        return 0;
    }
    
    return _special.specialClasses.count+1 ?: 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if ( section == 0  ) { return 1; }
    
    return _special.specialClasses[section-1].pressListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if ( indexPath.section == 0 && indexPath.row == 0) {
    
        OdditySpecialTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"special"];
        
        [cell setSpecial:self.special];
        
        return cell;
    }
    
    OddityNew *new = _special.specialClasses[indexPath.section-1].pressListArray[indexPath.row];
    
    OddityNewBaseTableViewCell *cell = (OddityNewBaseTableViewCell *)[self tableViewCell:tableView froNew:new];
    
    [cell bottomMakeHeight];
    
    [cell hiddenIconView];
    
    [cell hiddenCloseButton];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if ( section == 0  ) { return 0; }
    
    return 39;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if ( section == [self numberOfSectionsInTableView:tableView]-1 ) { return 0; }
    
    return 8;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

    UIView *view = [[UIView alloc] init];
    
    view.backgroundColor = [UIColor color_t_5];
    
    return view;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if (section == 0 ) { return nil; }
    
    OdditySpecialInfoTableViewHeaderView *cell = (OdditySpecialInfoTableViewHeaderView *)[tableView dequeueReusableCellWithIdentifier:@"header"];
    
    [cell setSpecialClass:_special.specialClasses[section-1]];
    
    return cell.contentView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        return UITableViewAutomaticDimension;
    }else{
    
        if ( indexPath.section == 0) {
            
            return [tableView fd_heightForCellWithIdentifier:@"special" configuration:^(id cell) {
                
                [(OdditySpecialTableViewCell *)cell setSpecial:self.special];
            }];
        }
        
        OddityNew *new = self.special.specialClasses[(int)indexPath.section-1].pressListArray[indexPath.row];
        
        return [self tableViewHeightCalendar:tableView froNew:new];
    }
}

-(CGFloat)tableViewHeightCalendar:(UITableView *)tableView froNew:(OddityNew *)new{
    
    OddityPrerryCellStyle style = [new cellStyle];
    
    if (style == PrerryCellStyleRefresh){
        
        return [tableView fd_heightForCellWithIdentifier:@"refresh" configuration:^(id cell) {
            // configurations
        }];
    }
    
    if (style == PrerryCellStyleOne || style == PrerryCellStyleOneAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"onecell" configuration:^(id cell) {
            
            [(OddityNewOneTableViewCell *)cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleTwo || style == PrerryCellStyleThree || style == PrerryCellStyleThreeAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"threecell" configuration:^(id cell) {
            
            [(OddityNewThreeTableViewCell *)cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleBig || style == PrerryCellStyleBigAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"bigcell" configuration:^(id cell) {
            
            [(OddityCoverImageTableViewCell *)cell setConfig:new];
        }];
    }
    
    return [tableView fd_heightForCellWithIdentifier:@"basecell" configuration:^(id cell) {
        
        [(OddityNewBaseTableViewCell *)cell setConfig:new];
    }];
}



-(UITableViewCell *)tableViewCell:(UITableView *)tableView froNew:(OddityNew *)new {
    
    OddityPrerryCellStyle style = [new cellStyle];
    

    if (style == PrerryCellStyleOne || style == PrerryCellStyleOneAds) {
        
        OddityNewOneTableViewCell *cell = (OddityNewOneTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"onecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleTwo || style == PrerryCellStyleThree || style == PrerryCellStyleThreeAds) {
        
        OddityNewThreeTableViewCell *cell = (OddityNewThreeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"threecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleBig || style == PrerryCellStyleBigAds) {
        
        OddityCoverImageTableViewCell *cell = (OddityCoverImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bigcell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    OddityNewBaseTableViewCell *cell = (OddityNewBaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"basecell"];
    
    [cell setConfig:new];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 ) { return ; }
    
    OddityNew *new = _special.specialClasses[indexPath.section-1].pressListArray[indexPath.row];
    
    OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc]initWithNid:new.nid isVideo:(new.cellStyle == PrerryCellStyleVideo)];
    
    [newDACViewController makeLogType:new.logtype AndLogChid:new.logchid cid:new.channel];
    
    [[UIViewController oddityCurrentViewController] presentViewController:newDACViewController animated:true completion:^{
       
        [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
    }];
}

@end
