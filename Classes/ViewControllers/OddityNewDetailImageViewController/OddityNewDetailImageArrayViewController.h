//
//  OddityNewDetailImageArrayViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/7/4.
//
//

#import "OddityNewDetailImageViewController.h"

#import "OddityNewsDetailViewController.h"

#import "OddityNewDetailImageArrayViewController.h"
#import <JXLPagerTabStrip/XLButtonBarPagerTabStripViewController.h>

@class OddityNewDetailImageViewController;

@interface OddityNewDetailImageArrayViewController : XLButtonBarPagerTabStripViewController

@property(nonatomic,assign,readonly) CGRect fromRect;
@property(nonatomic,assign,readonly) int willCurrentIndex;

@property(nonatomic,strong)UIView *bottomView;

@property(nonatomic,strong,readonly) OddityNewDetailImageViewController *curretnViewController;

-(instancetype)initWithImageArray:(NSArray *)imgArray currentIndex:(int)index currentFrame:(CGRect)frame fromViewController:(OddityNewsDetailViewController *)viewController;

@end
