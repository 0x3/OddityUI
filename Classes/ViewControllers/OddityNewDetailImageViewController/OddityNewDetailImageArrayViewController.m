//
//  OddityNewDetailImageArrayViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/7/4.
//
//

#import <PINCache/PINCache.h>

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>

#import "OddityPhotosAuthorization.h"

#import "OddityNewContentViewController.h"

#import <PinRemoteImage/PINRemoteImage.h>
#import "OddityNewDetailImageArrayViewController.h"

#import "OddityAlertViewViewController.h"

#import "OddityNewDetailImageArrayViewControllerTransitioning.h"

@interface OddityNewDetailImageArrayViewController ()<UIViewControllerTransitioningDelegate,XLPagerTabStripViewControllerDelegate>

@property(nonatomic,strong) NSArray *imageArray;
@property(nonatomic,strong) NSArray *cacheImageViewControllerArray;

@property(nonatomic,weak) OddityNewsDetailViewController *newsDetailViewController;

@property(nonatomic,strong)OddityNewDetailImageArrayViewControllerDismissAnimation *cDismissViewControllerAnimatedTransitioning;
@property(nonatomic,strong)OddityNewDetailImageArrayViewControllerPresentdAnimation *cPresentedViewControllerAnimatedTransitioning;

@property(nonatomic,strong)UILabel *currentLabel;
@property(nonatomic,strong)UILabel *sumCountLabel;

@property(nonatomic,strong)OddityBigButton *downLoadButton;
@end

@implementation OddityNewDetailImageArrayViewController

-(OddityNewDetailImageViewController *)curretnViewController{

    return [[self childViewControllersForPagerTabStripViewController:self] objectAtIndex:self.currentIndex];
}

-(instancetype)initWithImageArray:(NSArray *)imgArray currentIndex:(int)index currentFrame:(CGRect)frame fromViewController:(OddityNewsDetailViewController *)viewController{

    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        
        _fromRect = frame;
        _willCurrentIndex = index;
        
        _newsDetailViewController = viewController;
        
        _cDismissViewControllerAnimatedTransitioning  = [[OddityNewDetailImageArrayViewControllerDismissAnimation alloc]init];
        _cPresentedViewControllerAnimatedTransitioning  = [[OddityNewDetailImageArrayViewControllerPresentdAnimation alloc]init];
        
        NSMutableArray *array = [NSMutableArray array];
        
        for (NSString *imgStr in imgArray) {
            
            [array addObject:[OddityNewDetailImageObject.alloc init:imgStr]];
        }
        
        self.imageArray = array;
        
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    
    return self;
}



-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _cPresentedViewControllerAnimatedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _cDismissViewControllerAnimatedTransitioning;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = UIColor.color_t_13;
    
    self.delegate = self;
    
    self.buttonBarView.hidden = true;
    
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    
    if (self.willCurrentIndex != self.currentIndex) {
        
        [self moveToViewControllerAtIndex:self.willCurrentIndex animated:false];
    }
    
    self.isProgressiveIndicator = true;
    
    self.bottomView = [[UIView alloc] init];
    self.bottomView.backgroundColor = [UIColor.color_t_13 colorWithAlphaComponent:0.3];
    [self.view addSubview:self.bottomView];
    [self.bottomView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.height.mas_equalTo(42);
        make.bottom.left.right.mas_equalTo(0);
    }];
    
    self.currentLabel = [[UILabel alloc] init];
    self.currentLabel.textColor = UIColor.color_t_11;
    self.currentLabel.font = UIFont.font_t_2;
    [self.bottomView addSubview:self.currentLabel];
    [self.currentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(12);
        make.centerY.mas_equalTo(self.bottomView);
    }];
    
    self.sumCountLabel = [[UILabel alloc] init];
    self.sumCountLabel.textColor = UIColor.color_t_11;
    self.sumCountLabel.font = UIFont.font_t_6;
    [self.bottomView addSubview:self.sumCountLabel];
    [self.sumCountLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(self.currentLabel.mas_right).offset(2);
        make.bottom.mas_equalTo(self.currentLabel).offset(-2);
    }];
    
    self.downLoadButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
    self.downLoadButton.tintColor = UIColor.color_t_11;
    [self.downLoadButton addTarget:self action:@selector(downloadimage) forControlEvents:(UIControlEventTouchUpInside)];
    [self.downLoadButton setImage:UIImage.oddity_detail_download_image forState:(UIControlStateNormal)];
    [self.bottomView addSubview:self.downLoadButton];
    [self.downLoadButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.width.mas_equalTo(42);
        make.top.bottom.right.mas_equalTo(0);
    }];
    
    [self makeCurrentlabel];
    
    [self.view layoutIfNeeded];
}

-(void)downloadimage{
    
    typeof(self) __weak weakSelf = self;
    
    if (!self.curretnViewController.imageView.image && !self.curretnViewController.imageView.animatedImage) {
        
        NSString *mess = @"没有下载完毕，请稍等再保存";
        
        OddityAlertViewViewController *alertController = [[[OddityAlertViewViewController alloc] initWitch:mess] makeChange:mess after:0.5 block:^{
            
            [weakSelf dismissViewControllerAnimated:true completion:nil];
        }];
        
        [weakSelf presentViewController:alertController animated:true completion:nil];
        
        return;
    }
    
    [[OddityPhotosAuthorization.alloc init] checkAuthorizationStatus:^(OddityPhotoAuthStatus status) {
       
        switch (status) {
            case OddityPhotoAuthStatusSuccess:{
            
                [weakSelf SaveImageToAsset];
            }
                break;
            default:{
            
                OddityAlertViewViewController *alertController = [[[OddityAlertViewViewController alloc] initWitch:@""] makeChange: @"保存失败"  after:0.8 block:^{
                    
                    [weakSelf dismissViewControllerAnimated:true completion:nil];
                }];
                
                [weakSelf presentViewController:alertController animated:true completion:nil];
            }
                break;
        }
    }];
    
    
}

-(void)SaveImageToAsset{
    
    typeof(self) __weak weakSelf = self;
    
    NSData *data;
    
    if (self.curretnViewController.imageView.image) {
        
        data = UIImagePNGRepresentation(self.curretnViewController.imageView.image);
    }
    
    if (self.curretnViewController.imageView.animatedImage) {
        
        data = self.curretnViewController.imageView.animatedImage.data;
    }
    
    [UIImage oddity_savaImage: data completionHandler:^(BOOL success, NSError * _Nullable error, NSString * _Nullable message) {
        
        OddityAlertViewViewController *alertController = [[[OddityAlertViewViewController alloc] initWitch:message] makeChange: message  after:0.8 block:^{
            
            [weakSelf dismissViewControllerAnimated:true completion:nil];
        }];
        
        [weakSelf presentViewController:alertController animated:true completion:nil];
    }];
}

-(void)makeCurrentlabel{

    self.currentLabel.text = [NSString stringWithFormat:@"%u",self.currentIndex+1];
    self.sumCountLabel.text = [NSString stringWithFormat:@"/%lu",(unsigned long)self.imageArray.count];
}

-(void)pagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController updateIndicatorFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex withProgressPercentage:(CGFloat)progressPercentage indexWasChanged:(BOOL)indexWasChanged{

    if (indexWasChanged) {
        
        [self makeCurrentlabel];
        [self.newsDetailViewController.contentViewController scrollWebViewImageForIndex:(int)self.currentIndex];
    }
}


-(NSArray *)onlyOneArrayViewController{

    if (self.cacheImageViewControllerArray) {
        
        return self.cacheImageViewControllerArray;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    
    for (OddityNewDetailImageObject *imgObject in self.imageArray) {
        
        UIViewController *viewController = [OddityNewDetailImageViewController.alloc initWithMediaObject:imgObject withSuperViewController:self];
        
        viewController.view.backgroundColor = viewController.view.backgroundColor;
        
        [array addObject: viewController];
    }
    
    self.cacheImageViewControllerArray = array;
    
    return array;
}

-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{

    return self.onlyOneArrayViewController;
}


@end
