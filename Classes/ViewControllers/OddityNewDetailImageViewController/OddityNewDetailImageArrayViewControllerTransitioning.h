//
//  OddityNewDetailImageArrayViewControllerTransitioning.h
//  Pods
//
//  Created by 荆文征 on 2017/7/4.
//
//

#import <Foundation/Foundation.h>

@interface OddityNewDetailImageArrayViewControllerPresentdAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end

@interface OddityNewDetailImageArrayViewControllerDismissAnimation : NSObject<UIViewControllerAnimatedTransitioning>

@end
