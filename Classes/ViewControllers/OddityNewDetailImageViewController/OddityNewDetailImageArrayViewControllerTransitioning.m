//
//  OddityNewDetailImageArrayViewControllerTransitioning.m
//  Pods
//
//  Created by 荆文征 on 2017/7/4.
//
//

#import "OddityNewContentViewController.h"

#import "OddityNewsDetailViewController.h"
#import "OddityNewDetailImageArrayViewController.h"

#import "OddityNewDetailImageArrayViewControllerTransitioning.h"


UIView *MHStatusBar(void) {
    NSString *key = [NSString.alloc initWithData:[NSData dataWithBytes:(unsigned char []){0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x42, 0x61, 0x72} length:9] encoding:NSASCIIStringEncoding];
    id object = UIApplication.sharedApplication;
    UIView *statusBar;
    if ([object respondsToSelector:NSSelectorFromString(key)]) {
        statusBar = [object valueForKey:key];
    }
    return statusBar;
}


@interface OddityNewDetailImageArrayViewControllerPresentdAnimation ()

@property(nonatomic,strong) UIImageView *imageView;
@property(nonatomic,strong) UIScrollView *scrollView;

@end

@implementation OddityNewDetailImageArrayViewControllerPresentdAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.3;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityNewsDetailViewController *fromViewController = (OddityNewsDetailViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    OddityNewDetailImageArrayViewController *toViewController = (OddityNewDetailImageArrayViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    
    UIView *containerView = [transitionContext containerView];
    
    [containerView addSubview:toViewController.view];
    
    self.imageView = toViewController.curretnViewController.imageView;
    self.scrollView = toViewController.curretnViewController.scrollView;
    
    [containerView addSubview:self.imageView];
    
    self.imageView.frame = [fromViewController.containerView convertRect:toViewController.fromRect toView:toViewController.view];
    
    toViewController.view.backgroundColor = [toViewController.view.backgroundColor colorWithAlphaComponent: 0];
    
    [fromViewController.contentViewController makeHidden:true Index:toViewController.willCurrentIndex];
    
    toViewController.bottomView.alpha = 0;
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        
        toViewController.bottomView.alpha = 1;
        
        [self toRect];
    
        MHStatusBar().alpha = 0;
        
        toViewController.view.backgroundColor = [toViewController.view.backgroundColor colorWithAlphaComponent: 1];
        
    } completion:^(BOOL finished) {
        
        [fromViewController.contentViewController makeHidden:false Index:toViewController.willCurrentIndex];
        
        [transitionContext completeTransition: true];
        
        [toViewController.curretnViewController.imageView removeFromSuperview];
        [toViewController.curretnViewController.scrollView addSubview: toViewController.curretnViewController.imageView];
        self.imageView.frame = toViewController.curretnViewController.view.bounds;
    }];
}


-(void)toRect{

    if (self.imageView.image) {
        
        CGRect frame  = AVMakeRectWithAspectRatioInsideRect(self.imageView.image.size,CGRectMake(0, 0, self.scrollView.contentSize.width, self.scrollView.contentSize.height));
        
        if (self.scrollView.contentSize.width==0 && self.scrollView.contentSize.height==0) {
            frame = AVMakeRectWithAspectRatioInsideRect(self.imageView.image.size,UIScreen.mainScreen.bounds);
        }
        
        CGSize boundsSize = self.scrollView.bounds.size;
        
        CGRect frameToCenter = CGRectMake(0,0 , frame.size.width, frame.size.height);
        
        if (frameToCenter.size.width < boundsSize.width) {
            frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
        }
        else {
            frameToCenter.origin.x = 0;
        }if (frameToCenter.size.height < boundsSize.height) {
            frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
        }
        else {
            frameToCenter.origin.y = 0;
        }
        
        self.imageView.frame = frameToCenter;
        
        self.imageView.center = CGPointMake(CGRectGetMidX(UIScreen.mainScreen.bounds), CGRectGetMidY(UIScreen.mainScreen.bounds));
    }
}

@end



@implementation OddityNewDetailImageArrayViewControllerDismissAnimation

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.3;
}

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    OddityNewsDetailViewController *toViewController = (OddityNewsDetailViewController *)[transitionContext viewControllerForKey:(UITransitionContextToViewControllerKey)];
    OddityNewDetailImageArrayViewController *fromViewController = (OddityNewDetailImageArrayViewController *)[transitionContext viewControllerForKey:(UITransitionContextFromViewControllerKey)];
    
    [toViewController.contentViewController makeHidden:true Index:(int)fromViewController.currentIndex];
    
    [toViewController.contentViewController getImageCurrentFrameWithImageIndex:(int)fromViewController.currentIndex complete:^(CGRect rect) {
       
        rect.origin.y = rect.origin.y-toViewController.contentViewController.tableView.contentOffset.y;
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            
            fromViewController.bottomView.alpha = 0;
            
            fromViewController.curretnViewController.scrollView.zoomScale = 1;
            
            MHStatusBar().alpha = 1;
            
            fromViewController.view.backgroundColor = [fromViewController.view.backgroundColor colorWithAlphaComponent: 0];
            
            fromViewController.curretnViewController.imageView.frame = [toViewController.containerView convertRect: rect toView:toViewController.view];
            
        } completion:^(BOOL finished) {
            
            fromViewController.curretnViewController.imageView.hidden = true;
            
            [transitionContext completeTransition: true];
        }];
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [toViewController.contentViewController makeHidden:false Index:(int)fromViewController.currentIndex];
    });
}

@end
