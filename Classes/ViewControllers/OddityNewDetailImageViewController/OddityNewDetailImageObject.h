//
//  OddityNewDetailImageObject.h
//  Pods
//
//  Created by 荆文征 on 2017/7/4.
//
//

#import <Foundation/Foundation.h>

@interface OddityNewDetailImageObject : NSObject

@property(nonatomic,strong,readonly) NSURL *imgUrl;
@property(nonatomic,assign,readonly) CGFloat imgWidth;
@property(nonatomic,assign,readonly) CGFloat imgHeight;

@property(nonatomic,strong,readonly) NSString *urlString;

- (instancetype)init:(NSString *)urlString;

@end
