//
//  OddityNewDetailImageObject.m
//  Pods
//
//  Created by 荆文征 on 2017/7/4.
//
//

#import "NSString+Oddity.h"

#import "OddityNewDetailImageObject.h"

@interface OddityNewDetailImageObject()

@property(nonatomic,strong)NSArray *regxArray;

@end

@implementation OddityNewDetailImageObject

- (instancetype)init:(NSString *)urlString{
    
    self = [super init];
    if (self) {
        
        _urlString = urlString;
    }
    return self;
}

-(NSURL *)imgUrl{
    
    return [NSURL URLWithString:_urlString];
}


-(CGFloat)imgWidth{

    return [self.regxArray[1] floatValue];
}

-(CGFloat)imgHeight{

    return [self.regxArray[2] floatValue];
}

-(NSArray *)regxArray{

    if (_regxArray) {
        
        return _regxArray;
    }
    
    NSArray *strArray = [self.urlString oddity_findStringsRegex:@"_(\\d+)X(\\d+)."];
    
    return strArray;
}

@end
