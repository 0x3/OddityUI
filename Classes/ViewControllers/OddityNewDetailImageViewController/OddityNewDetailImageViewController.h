//
//  OddityNewDetailImageViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/7/3.
//
//

#import <UIKit/UIKit.h>
#import "OddityNewDetailImageObject.h"
#import <PinRemoteImage/PINRemoteImage.h>
#import "OddityNewDetailImageArrayViewController.h"

@class OddityNewDetailImageArrayViewController;

@interface OddityNewDetailImageViewController : UIViewController

@property (strong,nonatomic) UIScrollView *scrollView;
@property (strong,nonatomic) FLAnimatedImageView *imageView;

-(instancetype)initWithMediaObject:(OddityNewDetailImageObject *)object withSuperViewController:(OddityNewDetailImageArrayViewController *)viewController;

- (void)centerImageView;
@end
