//
//  OddityNewDetailImageViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/7/3.
//
//

#import "OddityCategorys.h"
#import <Masonry/Masonry.h>

#import "OddityNewDetailImageViewController.h"
#import "OddityMMMaterialDesignSpinner.h"

@interface OddityNewDetailImageViewController ()<UIScrollViewDelegate,XLPagerTabStripChildItem>

@property (nonatomic) UIPanGestureRecognizer *pan;
@property (nonatomic) UIPinchGestureRecognizer *pinch;

@property (nonatomic, strong) OddityMMMaterialDesignSpinner *activity;

@property (strong,nonatomic) OddityNewDetailImageObject *mediaObject;
@property (weak,nonatomic) OddityNewDetailImageArrayViewController *superViewController;

@end

@implementation OddityNewDetailImageViewController

-(instancetype)initWithMediaObject:(OddityNewDetailImageObject *)object withSuperViewController:(OddityNewDetailImageArrayViewController *)viewController{

    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        
        self.mediaObject = object;
        self.superViewController = viewController;
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    [self seUptImage];
    [self.scrollView setZoomScale:self.scrollView.minimumZoomScale];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setupScrollView];
    [self setlayout];
    [self setupImageView];
    [self setupGestureRecognizers];
}

-(void)seUptImage{

    [self.activity startAnimating];
    
    [self.imageView pin_setImageFromURL:self.mediaObject.imgUrl completion:^(PINRemoteImageManagerResult * _Nonnull result) {
        
        [self.activity stopAnimating];
    }];
}

-(void)setlayout{
    
    _activity = [[OddityMMMaterialDesignSpinner alloc] init];
    _activity.lineWidth = 1;
    _activity.duration  = 1;
    _activity.tintColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9];
    [self.view addSubview:_activity];
    [self.activity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.width.with.height.mas_equalTo(45);
    }];
}

- (void)setupGestureRecognizers {
    
    UITapGestureRecognizer *doubleTap = [UITapGestureRecognizer.alloc initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTap.numberOfTapsRequired =2;
    
    UITapGestureRecognizer *imageTap =[UITapGestureRecognizer.alloc initWithTarget:self action:@selector(handelImageTap:)];
    imageTap.numberOfTapsRequired =1;
    
    [self.imageView addGestureRecognizer:doubleTap];

    [self.view addGestureRecognizer:imageTap];
    [imageTap requireGestureRecognizerToFail: doubleTap];
}


- (void)handelImageTap:(UIGestureRecognizer *)gestureRecognizer {

    [self.superViewController dismissViewControllerAnimated:true completion:nil];
}

- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {

    if (self.scrollView.zoomScale >1) {
        [self.scrollView setZoomScale:1 animated:YES];
        return;
    }
    [self centerImageView];
    
    CGRect zoomRect;
    CGFloat newZoomScale = (self.scrollView.maximumZoomScale);
    CGPoint touchPoint = [gestureRecognizer locationInView:gestureRecognizer.view];
    
    zoomRect.size.height = [self.imageView frame].size.height / newZoomScale;
    zoomRect.size.width  = [self.imageView frame].size.width  / newZoomScale;
    
    touchPoint = [self.scrollView convertPoint:touchPoint fromView:self.imageView];
    
    zoomRect.origin.x    = touchPoint.x - ((zoomRect.size.width / 2.0));
    zoomRect.origin.y    = touchPoint.y - ((zoomRect.size.height / 2.0));
    
    [self.scrollView zoomToRect:zoomRect animated:YES];
}


#pragma mark - Setup methods
- (void)setupScrollView {
    self.scrollView = [UIScrollView.alloc initWithFrame:self.view.bounds];
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin;
    self.scrollView.delegate = self;
    self.scrollView.tag = 406;
    self.scrollView.maximumZoomScale = 3;
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.userInteractionEnabled = YES;
    [self.view addSubview:self.scrollView];
    [self.scrollView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)setupImageView {
    self.imageView = [FLAnimatedImageView.alloc initWithFrame:self.view.bounds];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.clipsToBounds = YES;
    self.imageView.userInteractionEnabled = YES;
    [self.scrollView addSubview:self.imageView];
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{

    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    
    [self centerImageView];
}

- (void)centerImageView {
    if (self.imageView.image) {
        
        CGRect frame  = AVMakeRectWithAspectRatioInsideRect(self.imageView.image.size,CGRectMake(0, 0, self.scrollView.contentSize.width, self.scrollView.contentSize.height));
        
        if (self.scrollView.contentSize.width==0 && self.scrollView.contentSize.height==0) {
            frame = AVMakeRectWithAspectRatioInsideRect(self.imageView.image.size,self.scrollView.bounds);
        }
        
        CGSize boundsSize = self.scrollView.bounds.size;
        
        CGRect frameToCenter = CGRectMake(0,0 , frame.size.width, frame.size.height);
        
        if (frameToCenter.size.width < boundsSize.width) {
            frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
        }
        else {
            frameToCenter.origin.x = 0;
        }if (frameToCenter.size.height < boundsSize.height) {
            frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
        }
        else {
            frameToCenter.origin.y = 0;
        }
        self.imageView.frame = frameToCenter;
    }
}


-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{ return @"刺激"; }

@end
