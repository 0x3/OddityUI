//
//  OddityContentAdnCommentBaseViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>
#import "OddityNewContent.h"
#import <JXLPagerTabStrip/XLButtonBarPagerTabStripViewController.h>

@class OddityNewsDetailViewController;

@interface OddityContentAdnCommentBaseViewController : UIViewController<XLPagerTabStripChildItem>

@property (nonatomic,weak)OddityNewsDetailViewController *superViewController;

@property (nonatomic,strong)OddityNewContent *nContent;
@property (nonatomic,strong)UITableView *tableView;

-(void)setContentAfterRequestFinished:(OddityNewContent *)newContent;

-(instancetype)initWithSuperViewController:(OddityNewsDetailViewController *)viewController;

@end

