//
//  OddityContentAdnCommentBaseViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCustomView.h"
#import <Masonry/Masonry.h>

#import "OddityNewsDetailViewController.h"

#import "OddityContentAdnCommentBaseViewController.h"

@implementation OddityContentAdnCommentBaseViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(instancetype)initWithSuperViewController:(OddityNewsDetailViewController *)viewController{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        self.superViewController = viewController;
    }
    
    return self;
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc]init];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    _tableView.backgroundColor = [UIColor color_t_6];
    
    [self.tableView registerClass:[OddityVideoTitleTableViewCell class] forCellReuseIdentifier:@"video"];
    [self.tableView registerClass:[OddityAdsImageTableViewCell class] forCellReuseIdentifier:@"ads"];
    [self.tableView registerClass:[OddityAboutNewTextTableViewCell class] forCellReuseIdentifier:@"text"];
    [self.tableView registerClass:[OddityAboutNewImageTableViewCell class] forCellReuseIdentifier:@"image"];
    [self.tableView registerClass:[OddityToMoreCommentTableViewCell class] forCellReuseIdentifier:@"toMoreC"];
    [self.tableView registerClass:[OddityDetailViewTableHeaderView class] forHeaderFooterViewReuseIdentifier:@"daccell"];
    [self.tableView registerClass:[OddityCommentTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView registerClass:[OddityContentInfoTableViewCell class] forCellReuseIdentifier:@"infocell"];
    
    
    typeof(self) __weak weakSelf = self;
    [_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.edges.equalTo(weakSelf.view).insets(UIEdgeInsetsZero);
    }];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    [self.tableView reloadData];
}

/**
 *  设置新闻详情对象
 *
 *  @param newContent 新闻想象
 */
-(void)setContentAfterRequestFinished:(OddityNewContent *)newContent{
    
    self.nContent = newContent;
}

-(NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    return @"奇点" ;
}

@end
