//
//  OddityNewAdsDetailViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>
#import "OddityCustomTransitioning.h"

/**
 *  新闻广告详情页面视图
 */
@interface OddityAdsNewDetailsViewController : OddityCustomAnimationViewController

/**
 *  创建一个请求的 request
 *
 *  @param urlRequest 请求request'
 *
 *  @return 广告新闻视图对象
 */
-(instancetype)initWithUrlReuqest:(NSURLRequest *)urlRequest;

/**
 *  获得链接直接跳转就可以
 *
 *  @param adsUrlString 跳转链接
 *
 *  @return 跳转链接
 */
-(instancetype)initWithAdsUrlString:(NSString *)adsUrlString;

/**
 *  创建一个需要先请求出来
 *
 *  {"data":{"clickid":"q4ulkwdpaaannaqwra5q","dstlink":"https://itunes.apple.com/cn/app/%E6%B6%88%E6%B6%88%E4%B9%90%E6%B5%B7%E6%BB%A8%E5%81%87%E6%97%A5-%E5%BC%80%E5%BF%83%E6%B6%88%E6%B6%88%E4%B9%90%E5%A7%8A%E5%A6%B9%E7%AF%87/id1115609641?mt=8&uo=4"},"ret":0}
 *  之后再根据实际情况进行跳转
 *  @param adsUrlString 数据请求链接
 *
 *  @return 广告新闻视图对象
 */
-(instancetype)initWithReqAdsUrlString:(NSString *)adsUrlString aboutNew:(OddityNew *)jouObj;


/**
 *  创建一个需要先请求出来
 *
 *  {"data":{"clickid":"q4ulkwdpaaannaqwra5q","dstlink":"https://itunes.apple.com/cn/app/%E6%B6%88%E6%B6%88%E4%B9%90%E6%B5%B7%E6%BB%A8%E5%81%87%E6%97%A5-%E5%BC%80%E5%BF%83%E6%B6%88%E6%B6%88%E4%B9%90%E5%A7%8A%E5%A6%B9%E7%AF%87/id1115609641?mt=8&uo=4"},"ret":0}
 *  之后再根据实际情况进行跳转
 *  @param adsUrlString 数据请求链接
 *
 *  @return 广告新闻视图对象
 */
-(instancetype)initWithReqAdsUrlString:(NSString *)adsUrlString aboutAboutNew:(OddityNewContentAbout *)aboutObject;
@end


