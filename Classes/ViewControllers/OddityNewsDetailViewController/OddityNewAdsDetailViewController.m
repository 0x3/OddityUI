//
//  OddityNewAdsDetailViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"

#import "OddityCategorys.h"

#import "OddityCustomView.h"
#import "OddityPlayerView.h"

#import <WebKit/WebKit.h>
#import <Masonry/Masonry.h>

#import "OddityNew+Advertisement.h"
#import "OddityNewContentAbout+Advertisement.h"

#import "OddityNewAdsDetailViewController.h"

@interface OddityAdsNewDetailsViewController()<WKNavigationDelegate,WKUIDelegate,UIWebViewDelegate>

@property(nonatomic,strong) WKWebView *wkWebView;

@property(nonatomic,strong) UIWebView *webView;

@property(nonatomic,strong) NSURLRequest *urlRequest;

@property(nonatomic,strong) OddityNavigationBarView *navVarView;

@property(nonatomic,assign) BOOL goAutoLink;

@end

@implementation OddityAdsNewDetailsViewController


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(instancetype)initWithUrlReuqest:(NSURLRequest *)urlRequest{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        _goAutoLink = true;
        
        _urlRequest = urlRequest;
    }
    
    return self;
}

-(instancetype)initWithAdsUrlString:(NSString *)adsUrlString{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        _goAutoLink = true;
        
        NSURL *handleStrUrl = [[NSURL alloc]initWithString:[adsUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet  URLQueryAllowedCharacterSet]]];
        if(!handleStrUrl) {
            handleStrUrl = [[NSURL alloc]initWithString:@"http://deeporiginalx.com/index.html"];
        }
        _urlRequest = [[NSURLRequest alloc]initWithURL:handleStrUrl];
    }
    
    return self;
}


-(instancetype)initWithReqAdsUrlString:(NSString *)adsUrlString aboutNew:(OddityNew *)jouObj{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        _goAutoLink = false;
        
        NSString *requestUrl =  [adsUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        [OddityNew getClickIdAndDstLink:requestUrl finishBlock:^(NSString *clickid, NSString *dstlink) {
            
            [jouObj uploadTrackArrayList:clickid];
            
            _urlRequest = [[NSURLRequest alloc]initWithURL:[[NSURL alloc]initWithString:dstlink]];
            
            [self goToClickLink];
            
        } failBlock:^{
            
            _urlRequest = [[NSURLRequest alloc]initWithURL:[[NSURL alloc]initWithString:@"http://deeporiginalx.com/index.html"]];
            
            [self goToClickLink];
        }];
    }
    
    return self;
}

-(instancetype)initWithReqAdsUrlString:(NSString *)adsUrlString aboutAboutNew:(OddityNewContentAbout *)aboutObject{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        _goAutoLink = false;
        
        NSString *requestUrl =  [adsUrlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        [OddityNew getClickIdAndDstLink:requestUrl finishBlock:^(NSString *clickid, NSString *dstlink) {
            
            [aboutObject uploadTrackArrayList:clickid];
            
            _urlRequest = [[NSURLRequest alloc]initWithURL:[[NSURL alloc]initWithString:dstlink]];
            
            [self goToClickLink];
            
        } failBlock:^{
            
            _urlRequest = [[NSURLRequest alloc]initWithURL:[[NSURL alloc]initWithString:@"http://deeporiginalx.com/index.html"]];
            
            [self goToClickLink];
        }];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor color_t_6];
    
    _navVarView = [[OddityNavigationBarView alloc]init:@"" titleFont:[UIFont font_t_3] block:^{
        
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_navVarView];
    [_navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@64);
    }];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
        
        _wkWebView = [[WKWebView alloc]initWithFrame:(CGRectZero) configuration:configuration];
        
        _wkWebView.UIDelegate = self;
        _wkWebView.navigationDelegate = self;
        
        [self.view addSubview:_wkWebView];
        [_wkWebView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_navVarView.mas_bottom);
            make.bottom.equalTo(@0);
            make.left.equalTo(@0);
            make.right.equalTo(@0);
        }];
        
        
    }else{
        
        _webView = [[UIWebView alloc]init];
        _webView.delegate = self;
        
        [self.view addSubview:_webView];
        [_webView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.top.equalTo(_navVarView.mas_bottom);
            make.bottom.equalTo(@0);
            make.left.equalTo(@0);
            make.right.equalTo(@0);
        }];
    }
    
    if (_goAutoLink) {
        
        [self goToClickLink];
    }
    
    
    [self oddity_showWaitView:64];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
}

-(void)refreshThemeMethod{
    
    self.view.backgroundColor = [UIColor color_t_6];
}

-(void)goToClickLink{
    
    if (_wkWebView) {
        
        [_wkWebView loadRequest:self.urlRequest];
    }
    
    if (_webView) {
        
        [_webView loadRequest:self.urlRequest];
    }
}


-(WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures{
    
    [self.wkWebView loadRequest:navigationAction.request];
    
    return nil;
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    [self oddity_hiddenWaitView];
}

-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
    [self.waitView goErrorStyle:^{
        
        [webView reload];
    }];
}

-(void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
#if TARGET_IPHONE_SIMULATOR
    
    NSLog(@"虚拟机可是没有App Store的");
    
    [self dismissViewControllerAnimated:true completion:nil];
    
#else
    
    
    if( error && error.userInfo[@"NSErrorFailingURLStringKey"]){
        
        NSString *url = error.userInfo[@"NSErrorFailingURLStringKey"];
        
        NSURL *openUrl =[[NSURL alloc]initWithString:url];
        
        if ([[UIApplication sharedApplication]canOpenURL:openUrl]){
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self dismissViewControllerAnimated:true completion:nil];
            });
            
            [[UIApplication sharedApplication]openURL:openUrl];
        }
    }
#endif
}

-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    
    decisionHandler(WKNavigationActionPolicyAllow);
}


-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self dismissViewControllerAnimated:true completion:nil];
    });
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [self oddity_hiddenWaitView];
}

@end
