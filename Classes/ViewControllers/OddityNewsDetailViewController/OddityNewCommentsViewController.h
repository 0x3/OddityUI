//
//  OddityNewCommentViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>

#import "OddityContentAdnCommentBaseViewController.h"

@interface OddityNewCommentsViewController : OddityContentAdnCommentBaseViewController<UITableViewDataSource,UITableViewDelegate>


/**
 更新评论 在评论完成之后
 */
-(void)refreshWithCommentSuccess;
@end
