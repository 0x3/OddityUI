//
//  OddityNewCommentViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//
#import "OddityCategorys.h"
#import <Masonry/Masonry.h>

#import "OddityCustomTableViewCell.h"

#import "OddityCustomView.h"

#import "OddityNewsDetailViewController.h"

#import "OddityNewCommentsViewController.h"

#import "UITableView+FDTemplateLayoutCell.h"

@implementation OddityNewCommentsViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(void)setContentAfterRequestFinished:(OddityNewContent *)newContent{
    
    [super setContentAfterRequestFinished:newContent];
    
    [self.tableView reloadData];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)refreshWithCommentSuccess{

    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:(UITableViewRowAnimationAutomatic)];
    
    OddityDetailViewTableHeaderView *cell = (OddityDetailViewTableHeaderView *)[self.tableView headerViewForSection:2];
    
    if (cell) { [cell setDescInfoString: [self.nContent NormalHeaderString]]; }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    if (self.superViewController.isVideo) {
        
        self.tableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0);
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFontModeMethod) name:@"oddityFontModeChange" object:nil];
}

-(void)refreshFontModeMethod{

    [self.tableView reloadData];
}

-(void)refreshThemeMethod{
    
    self.tableView.backgroundColor = [UIColor color_t_6];
    [self.tableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ( section == 0 && self.nContent ){ return 1; }
    
    if (section == 1) { return self.superViewController.hotCommits.count; }
    
    if (section == 2) {
        
        NSInteger rowCount = [self.tableView makeEmtpyView:self.superViewController.commits.count viewMode:OddityTableViewEmtpyBackViewModeSofa offset:32];
        
        CGFloat topFloat = self.superViewController.isVideo ? 44 : 0;
        
        if (rowCount <= 0) {
            
            CGRect rect = [self.nContent.titleAttributedString boundingRectWithSize:CGSizeMake(oddityUIScreenWidth-36, 10000) options:0 context:nil];
            
            self.tableView.contentInset = UIEdgeInsetsMake(topFloat, 0, CGRectGetHeight(rect)+400, 0);
            
        }else{
        
            self.tableView.contentInset = UIEdgeInsetsMake(topFloat, 0, 0, 0);
        }
        
        return rowCount;
    }
    
    return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        return UITableViewAutomaticDimension;
    }else{
        
        return [self tableViewHeightCalendar:tableView forIndexPath:indexPath];
    }
}



-(CGFloat)tableViewHeightCalendar:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath{
    

    if ( indexPath.section == 0 && indexPath.row == 0 ){
        
        return [tableView fd_heightForCellWithIdentifier:@"infocell" configuration:^(id cell) {
            
            [(OddityContentInfoTableViewCell *)cell setContent:self.nContent];
        }];
    }
    
    OddityNewContentComment *newContentComment;
    
    if (indexPath.section == 1){
        
        newContentComment = self.superViewController.hotCommits[indexPath.row];
    }
    
    if (indexPath.section == 2){
        
        newContentComment = self.superViewController.commits[indexPath.row];
    }
    
    return [tableView fd_heightForCellWithIdentifier:@"cell" configuration:^(id cell) {
        
        [(OddityCommentTableViewCell *)cell setNewContentComment:newContentComment];
    }];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ( indexPath.section == 0 && indexPath.row == 0 ){
        
        OddityContentInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infocell"];
        
        [cell setContent:self.nContent];
        
        return cell;
    }
    
    OddityNewContentComment *newContentComment;
    
    if (indexPath.section == 1){ newContentComment = self.superViewController.hotCommits[indexPath.row]; }
    
    if (indexPath.section == 2){ newContentComment = self.superViewController.commits[indexPath.row]; }
    
    OddityCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.delegate = self.superViewController;
    
    [cell setNewContentComment:newContentComment];
    
    return cell;
}




-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0){ return 0; }
    if (section == 1 && self.nContent && [self.nContent isShowHotHeader] ) { return 37; }
    if (section == 2 ) { return 37; }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    OddityDetailViewTableHeaderView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"daccell"];
    
    if ( self.nContent && self.nContent.comment <= 0 ) {
        
        [cell setDescInfoString: @"最新评论"];
    }else{
        
        if ( section == 1 ){ [cell setDescInfoString: [self.nContent HotHeaderString]]; }
        
        if ( section == 2 ){ [cell setDescInfoString: [self.nContent NormalHeaderString]]; }
    }
    
    return cell;
}

@end

