//
//  OddityNewsDetailViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

#import "OddityModels.h"
#import "OddityCustomView.h"

#import "OddityCustomTransitioning.h"
#import <JXLPagerTabStrip/XLButtonBarPagerTabStripViewController.h>

@class OddityNewContentViewController,OddityNewCommentsViewController,OddityNewContentAbout,OddityNewContentComment,OddityNewContentComment;

@interface OddityNewsDetailViewController : XLButtonBarPagerTabStripViewController<OddityCommentTableViewCellDelegate>

@property(nonatomic,strong) RLMNotificationToken *hotNotificationToken;
@property(nonatomic,strong) RLMNotificationToken *normalNotificationToken;

@property(nonatomic,strong) OddityCustomVideoViewControllerPresentdAnimation *videoPresentdAnimation;


@property(nonatomic,strong) OddityMainManagerViewController *mainManagerViewController;
@property(nonatomic,strong) OddityNewContentViewController *contentViewController;
@property(nonatomic,strong) OddityNewCommentsViewController *commentsViewController;

@property(nonatomic,strong) OddityNavigationBarView *navVarView;

@property(nonatomic,strong) OddityNewContent *nContent;
/// 相关新闻 数据源
@property (nonatomic,strong) RLMArray<OddityNewContentAbout *> *abouts;
/// 新闻评论 数据源
@property (nonatomic,strong) RLMResults<OddityNewContentComment *> *commits;
/// 热门评论的 数据源
@property (nonatomic,strong) RLMResults<OddityNewContentComment *> *hotCommits;

/// 该条新闻是否来源于 相关新闻的点击
@property(nonatomic,assign) BOOL isSourceAboutNewClick;
/// 是否播放视频
@property(nonatomic,assign) BOOL isVideo;

/// 视频id
@property(nonatomic,assign) int nid;
@property(nonatomic,strong) id videoObjt;
@property(nonatomic,assign) BOOL needRePlay;
@property(nonatomic,assign) BOOL justGoBack;
@property(nonatomic,strong) NSString *videoUrl;
@property(nonatomic,assign) NSInteger durationTime;

/// 用户日志上传相关数据
@property(nonatomic,assign) int cid;
@property(nonatomic,assign) int logtype;
@property(nonatomic,assign) int logchid;


-(void)rEqByNid:(int)nid;

/**
 *   初始化 OddityNewsDetailViewController 对象
 *
 *  @param nid 新闻id
 *  @param iv  是不是视频详情页面
 *
 *  @return OddityNewsDetailViewController 对象
 */
-(instancetype)initWithNid:(int)nid isVideo:(BOOL)iv;

/**
 *   初始化 OddityNewsDetailViewController 对象
 *
 *  @param nid 新闻id
 *  @param iv  是不是视频详情页面
 *  @param mainVC  主视图
 *
 *  @return OddityNewsDetailViewController 对象
 */
-(instancetype)initWithNid:(int)nid isVideo:(BOOL)iv managerViewController:(OddityMainManagerViewController *)mainVC;
/**
 *  配置用户日志上传
 *
 *  @param lt    lt description
 *  @param lc    lc description
 *  @param cid   频道id
 *
 *  @return OddityNewsDetailViewController 对象
 */
-(OddityNewsDetailViewController *)makeLogType:(int)lt AndLogChid:(int)lc cid:(int)cid;


/**
 *  配置视频数据
 *
 *  @param url 视频URL
 *  @param obj 用户播放对象
 *
 *  @return OddityNewsDetailViewController 对象
 */
-(OddityNewsDetailViewController *)configVideoUrl:(NSString *)url Object:(id)obj;

@end




