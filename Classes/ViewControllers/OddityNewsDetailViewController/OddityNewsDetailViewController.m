//
//  OddityNewsDetailViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityUserLoginViewController.h"
#import "OddityCategorys.h"
#import "OddityCustomView.h"
#import "OddityPlayerView.h"

#import "OddityShareViewController.h"

#import "OddityLogObject.h"

#import "OddityDetailBottomInputView.h"

#import "OddityManagerInfoMationUtil.h"

#import "OddityNewContentViewController.h"
#import "OddityNewCommentsViewController.h"

#import "OddityDetailInputViewController.h"

#import "OddityNewsDetailViewController.h"

#import "OddityAlertViewViewController.h"
#import "OddityNewDetailImageArrayViewController.h"

@interface OddityNewsDetailViewController ()<UIGestureRecognizerDelegate,UIViewControllerTransitioningDelegate,OddityDetailBottomInputViewDelegate,OddityNewContentViewControllerDelegate>

@property(nonatomic,assign) long long startReadDateTime;

@property(nonatomic,retain) RLMNotificationToken *aboutNotificationToken;

@property(nonatomic,strong) OddityBigButton *moreShareButton;

@property(nonatomic,strong) UIPanGestureRecognizer *waitPanGestureRecognizer;
@property(nonatomic,strong) UITapGestureRecognizer *smallTapGestureRecognizer;

@property(nonatomic,strong) OddityNormalViewControllerDismissedAnimation *nPresentdAnimation;
@property(nonatomic,strong) OddityNormalViewControllerDismissedAnimation *nDismissedAnimation;
@property(nonatomic,strong) UIPercentDrivenInteractiveTransition *nDismissedinteractiveTransitioning;

/// 自定义跳转动画集合
@property(nonatomic,strong) OddityCustomViewControllerPresentdAnimation *presentdAnimation;
@property(nonatomic,strong) OddityCustomViewControllerDismissedAnimation *dismissedAnimation;

@property(nonatomic,strong) OddityCustomViewControllerDrivenInteractiveTransition *interactiveTransitioning;
@property(nonatomic,strong) OddityCustomVideoViewControllerDrivenInteractiveTransition *videoInteractiveTransitioning;


@property(nonatomic,strong) OddityCustomVideoViewControllerDismissAnimation *videoDismissAnimation;

@property(nonatomic,strong) OddityDetailBottomInputView *bottomInputView;

@property(nonatomic,strong)NSUserActivity *activity;
@end


@implementation OddityNewsDetailViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(id<UIViewControllerAnimatedTransitioning>)nPresentdAnimation{
    
    if (self.isVideo && !self.justGoBack) {
        
        return self.videoPresentdAnimation;
    }
    
    return self.presentdAnimation;
}

/// 普通的Dismiss 集合对象
-(OddityNormalViewControllerDismissedAnimation *)nDismissedAnimation{
    
    if (self.isVideo) {
        
        if (OddityPlayerView.sharePlayerView.isPlayerViewShow && !self.justGoBack) {
            
            return self.videoDismissAnimation;
        }
    }
    
    return self.dismissedAnimation;
}

/// 获取 一个 渐进式
-(UIPercentDrivenInteractiveTransition *)nDismissedinteractiveTransitioning{
    
    if (self.isVideo) {
        
        if (OddityPlayerView.sharePlayerView.isPlayerViewShow && !self.justGoBack) {
            
            return self.videoInteractiveTransitioning;
        }
    }
    
    return self.interactiveTransitioning;
}

-(void)dealloc{
    
    [self.hotNotificationToken stop];
    [self.aboutNotificationToken stop];
    [self.normalNotificationToken stop];
    
    [[OddityPlayerView sharePlayerView] removeGestureRecognizer:self.smallTapGestureRecognizer];
    
    [self endTimeReckon];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setNContent:(OddityNewContent *)nContent{

    if (_nContent == nContent) {
        return;
    }
    
    _nContent = nContent;
    
    _startReadDateTime = [[NSDate date] oddity_unixInteger];
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
    [self makeHandOffMethod];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [self.activity invalidate];
    
    self.activity = nil;
}

-(void)makeHandOffMethod{
    
    if (self.nContent) {
        
        self.activity = [[NSUserActivity alloc] initWithActivityType:@"oddity.handoff.shareurl"];
        
        self.activity.webpageURL = [NSURL URLWithString: self.nContent.getShareObject.shareURLString];
        
        [self.activity becomeCurrent];
    }
}

-(void)endTimeReckon{
    
    long long endInt = [[NSDate date] oddity_unixInteger];
    
    if (self.isVideo) {
        
        // 打点 ->  新闻浏览时长打点
        [OddityLogObject createNewsRead:_nContent.nid beginTime:_startReadDateTime endTime:endInt readTime:endInt-_startReadDateTime percent: 1];
    }else{
    
        // 打点 ->  新闻浏览时长打点
        [OddityLogObject createNewsRead:_nContent.nid beginTime:_startReadDateTime endTime:endInt readTime:endInt-_startReadDateTime percent:self.nContent.percent];
    }
    
}


-(OddityNewsDetailViewController *)makeLogType:(int)lt AndLogChid:(int)lc cid:(int)cid{
    
    _logtype = lt;
    _logchid = lc;
    
    _cid = cid;
    
    return self;
}

-(OddityNewsDetailViewController *)configVideoUrl:(NSString *)url Object:(id)obj{
    
    self.videoUrl = url;
    self.videoObjt = obj;
    
    return self;
}

-(instancetype)initWithNid:(int)nid isVideo:(BOOL)iv{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if(self) {
        
        self.nid = nid;
        self.isVideo = iv;
        
        self.transitioningDelegate = self;
        self.modalPresentationCapturesStatusBarAppearance = true;
        
        /// 视频相关 视图切换对象
        _videoPresentdAnimation = [OddityCustomVideoViewControllerPresentdAnimation alloc];
        _videoDismissAnimation = [OddityCustomVideoViewControllerDismissAnimation alloc];
        _videoInteractiveTransitioning = [[OddityCustomVideoViewControllerDrivenInteractiveTransition alloc]init];
        
        /// 非视频相关 视图切换对象
        _presentdAnimation = [OddityCustomViewControllerPresentdAnimation alloc];
        _dismissedAnimation = [OddityCustomViewControllerDismissedAnimation alloc];
        _interactiveTransitioning = [[OddityCustomViewControllerDrivenInteractiveTransition alloc]init];
    }
    
    return self;
}

-(instancetype)initWithNid:(int)nid isVideo:(BOOL)iv managerViewController:(OddityMainManagerViewController *)mainVC{

    self = [self initWithNid:nid isVideo:iv];
    
    if (self) {
        
        _mainManagerViewController = mainVC;
    }
    
    return self;
}


-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return self.nPresentdAnimation;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return self.nDismissedAnimation;
}

-(id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator{
    
    if (self.nDismissedAnimation.isInteraction) {
        
        return self.nDismissedinteractiveTransitioning;
    }
    return nil;
}

-(void)handlePanGestureRecognizerMethod:(UIPanGestureRecognizer *)pan{
    
    CGPoint point = [pan translationInView:self.view];
    
    switch (pan.state) {
            
        case UIGestureRecognizerStateBegan:{
            
            CGPoint vepoint = [pan velocityInView:self.view];
            
            if (vepoint.x < 0 || self.containerView.contentOffset.x > 0) { return; }
            
            self.nDismissedAnimation.isInteraction = true;
            
            [self dismissViewControllerAnimated:true completion:nil];
        }
            break;
            
        case UIGestureRecognizerStateChanged:{
            
            if (!self.nDismissedAnimation.isInteraction) { return; }
            
            [self.nDismissedinteractiveTransitioning updateInteractiveTransition: (CGFloat)point.x/CGRectGetWidth(self.view.frame)];
        }
            break;
        default:{
            
            if (!self.nDismissedAnimation.isInteraction) { return; }
            
            self.nDismissedAnimation.isInteraction = false;
            
            CGFloat locationX = ABS(point.x);
            CGFloat velocityX = [pan velocityInView:self.view].x;
            
            if (velocityX >= 800 || locationX >= CGRectGetWidth(self.view.frame)/2) {
                
                [self.nDismissedinteractiveTransitioning finishInteractiveTransition];
            }else{
                
                [self.nDismissedinteractiveTransitioning cancelInteractiveTransition];
            }
        }
            break;
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    self.isProgressiveIndicator = true;
    
    self.waitView.userInteractionEnabled = true;
    
    _waitPanGestureRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePanGestureRecognizerMethod:)];
    _waitPanGestureRecognizer.delegate = self;
    [self.waitView addGestureRecognizer:_waitPanGestureRecognizer];
    
    if (self.isVideo) {
        _smallTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(moveToFirstViewController)];
        _smallTapGestureRecognizer.delegate = self;
        [[OddityPlayerView sharePlayerView]addGestureRecognizer:_smallTapGestureRecognizer];
    }
    
    [self.containerView.panGestureRecognizer addTarget:self action:@selector(handlePanGestureRecognizerMethod:)];
    
    [self productionLayout];
    
    [self rEqByNid:self.nid];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
}

-(void)refreshThemeMethod{
    
    [self.bottomInputView refreshTheme];
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    [_moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:[OddityUISetting shareOddityUISetting].titleColor] forState:(UIControlStateNormal)];
    
    [self pagerTabStripViewController:self updateIndicatorFromIndex:0 toIndex:self.currentIndex withProgressPercentage:1 indexWasChanged:true];
    
    [self setNeedsStatusBarAppearanceUpdate];
}


-(void)moveToFirstViewController{
    
    [self moveToViewControllerAtIndex:0];
}

-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    if (gestureRecognizer == _waitPanGestureRecognizer) {
        CGPoint point = [(UIPanGestureRecognizer *)gestureRecognizer translationInView:self.view];
        
        return (fabs(point.x) > fabs(point.y)) && point.x > 0;
    }
    
    if (gestureRecognizer == _smallTapGestureRecognizer) {
        
        return self.currentIndex == 1;
    }
    
    return false;
}

-(void)rEqByNid:(int)nid{
    
    if (self.isVideo) {
        
        [self oddity_showWaitView: CGRectGetMaxY(self.contentViewController.playView.frame)];
        
    }else{
    
        [self oddity_showWaitView:64];
    }
    
    [self.view bringSubviewToFront:self.bottomInputView];
    
    [self.aboutNotificationToken stop];
    [self.hotNotificationToken stop];
    [self.normalNotificationToken stop];
    
    self.aboutNotificationToken = nil;
    self.hotNotificationToken = nil;
    self.normalNotificationToken = nil;
    
    
    self.commits = nil;
    self.hotCommits = nil;
    self.abouts = nil;
    
    self.contentViewController.nContent = nil;
    self.commentsViewController.nContent = nil;
    
    self.contentViewController.tableView.mj_footer = nil;
    self.commentsViewController.tableView.mj_footer = nil;
    
    [self.contentViewController.tableView reloadData];
    [self.commentsViewController.tableView reloadData];
    
    [self.contentViewController.tableView setContentOffset:(CGPointZero)];
    [self.commentsViewController.tableView setContentOffset:(CGPointZero)];
    
    if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didEnterInNewContentAction:)] ){
        
        [[OdditySetting shareOdditySetting].delegate didEnterInNewContentAction:self.nid];
    }
    
    [self.bottomInputView makeEnable:false];
    
    typeof(self) __weak weakSelf = self;
    [OddityNewContent requestByNid:nid isVideo:self.isVideo finishBlock:^(OddityNewContent *new,BOOL isCache) {
        
        if (self.isSourceAboutNewClick) {
         
            // 打点 -> 用户动作 点击相关新闻
            [OddityLogObject createAppAction: OddityLogAtypeRelateClick fromPageName:OddityLogPageDetailPage toPageName:OddityLogPageDetailPage effective: !isCache params:@{
                                                                                                                                                                             @"nid": @(self.nid)
                                                                                                                                                                             }];
        }
        
        // 打点 -> 用户动作 点击详情页
        [OddityLogObject createAppAction:OddityLogAtypeDetailClick fromPageName:OddityLogPageFeedPage toPageName:OddityLogPageDetailPage effective:!isCache params:@{
                                                                                                                                                                     @"nid": @(new.nid),
                                                                                                                                                                     @"logchid": @(weakSelf.logchid)
                                                                                                                                                                     }];
        
        /// 打点 -> 点击新闻打点
        [OddityLogObject createNewsClick:new.nid chid:self.cid logtype:self.logtype logchid:self.logchid source: self.isSourceAboutNewClick ? OdditySourcerelateName : OdditySourceFeedName];
        
        [weakSelf setContentAfterRequestFinished:[new clearSelf]];
        
        if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didFinishRequestNewContentAction:)] ){
            
            [[OdditySetting shareOdditySetting].delegate didFinishRequestNewContentAction:weakSelf.nid];
        }
        
    } failBlock:^{
        
        if ([OdditySetting shareOdditySetting].delegate && [[OdditySetting shareOdditySetting].delegate respondsToSelector:@selector(didFailRequestNewContentAction:)] ){
            
            [[OdditySetting shareOdditySetting].delegate didFailRequestNewContentAction:weakSelf.nid];
        }
        
        [weakSelf.waitView goErrorStyle:^{
            
            [weakSelf rEqByNid:nid];
        }];
    }];
}


-(void)setContentAfterRequestFinished:(OddityNewContent *)nContent{
    
    [self.bottomInputView bottomRefreshMethod:nContent currentIndex:self.currentIndex];
    
    /// 加载完成后 展示 分享按钮
    _moreShareButton.alpha = 1;
    [self.bottomInputView makeEnable:true];
    
    [self playVideo:nContent];
    
    self.nContent = nContent;
    
    [self makeHandOffMethod];
    
    [nContent toReadedMethod];
    
    self.commits = [nContent.commits sortedResultsUsingKeyPath:@"ctimes" ascending:false];
    self.hotCommits = [nContent.hotCommits sortedResultsUsingKeyPath:@"commend" ascending:false];
    self.abouts = nContent.aboutNewList;
    
    [_contentViewController setContentAfterRequestFinished:nContent];
    [_commentsViewController setContentAfterRequestFinished:nContent];
    
    typeof(self) __weak weakSelf = self;
    
    [NSNotificationCenter.defaultCenter removeObserver:self name:@"requestCommentscomplete" object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:self name:@"requestAboutscomplete" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestCompleteComments:) name:@"requestCommentscomplete" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestCompleteAbouts:) name:@"requestAboutscomplete" object:nil];
    
    self.normalNotificationToken  = [self.commits addNotificationBlock:^(RLMResults<OddityNewContentComment *> *results, RLMCollectionChange *changes, NSError *error) {
        
        /// 刷新下方输入框内的 评论数目
        [weakSelf.bottomInputView bottomRefreshMethod:weakSelf.nContent currentIndex:weakSelf.currentIndex];
        [weakSelf.contentViewController refreshWithCommentSuccess];
        
        if (error || !weakSelf.commentsViewController.tableView || !changes) { return; }
        UITableView *tableView = weakSelf.commentsViewController.tableView;
        
        [tableView reloadData];
        
        [weakSelf.commentsViewController refreshWithCommentSuccess];
        
        [weakSelf makeCommentsViewControllerFooterView:nContent];
    }];
    
    self.hotNotificationToken  = [self.hotCommits addNotificationBlock:^(RLMResults<OddityNewContentComment *> *results, RLMCollectionChange *changes, NSError *error) {
        if (error || !changes ) { return; }
        
        if( weakSelf.commentsViewController.tableView ) {
            
            UITableView *tableView = weakSelf.commentsViewController.tableView;
            
            [tableView reloadData];
            
        }
        
        if( weakSelf.contentViewController.tableView ) {
            
            UITableView *tableView = weakSelf.contentViewController.tableView;
            [tableView beginUpdates];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:(UITableViewRowAnimationNone)];
            [tableView endUpdates];
        }
        
        [weakSelf makeCommentsViewControllerFooterView:nContent];
    }];
    
    
    
    self.aboutNotificationToken  = [self.abouts addNotificationBlock:^(RLMArray<OddityNewContentAbout *> * _Nullable array, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        if (error || !changes || !weakSelf.contentViewController.tableView ) { return; }
        UITableView *tableView = weakSelf.contentViewController.tableView;
        
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:[changes deletionsInSection:3] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView insertRowsAtIndexPaths:[changes insertionsInSection:3] withRowAnimation:UITableViewRowAnimationNone];
        [tableView reloadRowsAtIndexPaths:[changes modificationsInSection:3] withRowAnimation:UITableViewRowAnimationNone];
        [tableView endUpdates];
        
//        [tableView reloadData];
        
        [weakSelf makeDetailViewControllerFooterView:nContent];
    }];
    
    [nContent requestComments:false];
    [nContent requestComments:true];
    [nContent requestAbout];
}


/**
 请求 评论完成

 @param notification 消息对象
 */
-(void)requestCompleteComments:(NSNotification *)notification{

    if (!self.commentsViewController.tableView.mj_footer) { return; }
    
    if ( [notification.userInfo[@"nomore"] boolValue] ){
        
        [self.commentsViewController.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        
        [self.commentsViewController.tableView.mj_footer endRefreshing];
    }
}


/**
 相关新闻 请求完成

 @param notification 消息对象
 */
-(void)requestCompleteAbouts:(NSNotification *)notification{

    if (!self.contentViewController.tableView.mj_footer) { return; }
    
    if ( [notification.userInfo[@"nomore"] boolValue] ){
        
        [self.contentViewController.tableView.mj_footer endRefreshingWithNoMoreData];
    }else{
        
        [self.contentViewController.tableView.mj_footer endRefreshing];
    }
}


-(void)playVideo:(OddityNewContent *)nContent{
    
    if (self.isVideo && self.needRePlay) {
        
        self.videoUrl = nContent.videourl;
        
        
        OddityPlayerObject *playObject = [[OddityPlayerObject alloc] initWith:self.nid VideoUrl:self.videoUrl Title:nContent.title Objtect:self.videoObjt];
        [[OddityPlayerView sharePlayerView] playWithPlayerObject:playObject];
    }
}

-(void)pagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController updateIndicatorFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex withProgressPercentage:(CGFloat)progressPercentage indexWasChanged:(BOOL)indexWasChanged{
    
    
    if (_isVideo ) {
        
        if (toIndex == 0) {
            
            UIColor *color =  [UIColor oddity_interpolateHSVColorFrom:[UIColor whiteColor] to:[OddityUISetting shareOddityUISetting].titleColor withFraction:(CGFloat)1-progressPercentage];
            
            
            self.navVarView.cancelButton.tintColor = color;
            [self.moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:color] forState:(UIControlStateNormal)];
            
            
            self.navVarView.backgroundColor = [[OddityUISetting shareOddityUISetting].backColor colorWithAlphaComponent:(CGFloat)1-progressPercentage];
        }else{
            
            UIColor *color =  [UIColor oddity_interpolateHSVColorFrom:[UIColor whiteColor] to:[OddityUISetting shareOddityUISetting].titleColor withFraction:progressPercentage];
            
            
            self.navVarView.cancelButton.tintColor = color;
            [self.moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:color] forState:(UIControlStateNormal)];
            
            self.navVarView.backgroundColor = [[OddityUISetting shareOddityUISetting].backColor colorWithAlphaComponent:progressPercentage];
        }
    }
    
    if (indexWasChanged || ((ODDITY_SYSTEM_LESS_THAN(8)) && progressPercentage == 1)) {
        
        [self.bottomInputView bottomRefreshMethod:self.nContent currentIndex:self.currentIndex];
        
        if (_isVideo) {
            
            if (self.currentIndex == 0 ) {
                
                self.navVarView.cancelButton.tintColor = [UIColor whiteColor];
                [self.moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:[UIColor whiteColor]] forState:(UIControlStateNormal)];
                
                self.navVarView.backgroundColor = [UIColor clearColor];
            }else{
                
                
                self.navVarView.cancelButton.tintColor = [OddityUISetting shareOddityUISetting].titleColor;
                [self.moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:[OddityUISetting shareOddityUISetting].titleColor] forState:(UIControlStateNormal)];
                
                self.navVarView.backgroundColor = [OddityUISetting shareOddityUISetting].backColor;
            }
        }
        
        [[OddityPlayerView sharePlayerView] toNeedJudgmentState];
        
        if (self.nContent && toIndex == 1 && self.commentsViewController.tableView) {
            
            [self makeCommentsViewControllerFooterView:self.nContent];
        }
        
        if (self.currentIndex == 1) {
            
            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateSmall) inView:nil];
        }else{
            
            [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateNormal) inView:self.contentViewController.playView];
        }
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
}



-(void)toAnimateToSmall:(BOOL)toSmall progress:(CGFloat)pro{
    
    CGRect normalVideoFrame = [self.contentViewController.playView convertRect:self.contentViewController.playView.bounds toView:[[UIApplication sharedApplication]keyWindow]];
    
    CGRect smallVideoFrame = [OddityPlayerView sharePlayerView].smallRelativeToScreenFrame;
    
    if (toSmall) {
        
        CGFloat top = CGRectGetMinY(normalVideoFrame)+(CGRectGetMinY(smallVideoFrame)-CGRectGetMinY(normalVideoFrame))*pro;
        CGFloat left = CGRectGetMinX(normalVideoFrame)+(CGRectGetMinX(smallVideoFrame)-CGRectGetMinX(normalVideoFrame))*pro;
        
        CGFloat width = CGRectGetWidth(normalVideoFrame)+(CGRectGetWidth(normalVideoFrame)-CGRectGetWidth(smallVideoFrame))*pro;
        CGFloat height = CGRectGetHeight(normalVideoFrame)+(CGRectGetHeight(normalVideoFrame)-CGRectGetHeight(smallVideoFrame))*pro;
        
        [[OddityPlayerView sharePlayerView] mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.width.equalTo(@(width));
            make.height.equalTo(@(height));
            
            make.top.equalTo(@(top));
            make.left.equalTo(@(left));
        }];
    }
}






/**
 *  创建 普通新闻 的 下拉刷新 尾部
 *
 *  @param nContent 新闻详情
 */
-(void)makeCommentsViewControllerFooterView:(OddityNewContent *)nContent{
    
    if ((_hotCommits.count > 0 || _commits.count > 0) && !self.commentsViewController.tableView.mj_footer) {
        
        self.commentsViewController.tableView.mj_footer = [OddityCustomRefreshFooterView footerWithRefreshingBlock:^{
            
            [nContent requestComments:false];
        }];
    }
}

/**
 *  制作详情 相关新闻 下拉刷新 尾部
 *
 *  @param nContent 新闻详情
 */
-(void)makeDetailViewControllerFooterView:(OddityNewContent *)nContent{
    
    if (_abouts.count > 0 && !self.contentViewController.tableView.mj_footer) {
        
        self.contentViewController.tableView.mj_footer = [OddityCustomRefreshFooterView footerWithRefreshingBlock:^{
            
            [nContent requestAbout];
        }];
    }
}


/**
 *  创建 布局
 */
-(void)productionLayout{
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.buttonBarView.hidden = true;
    
    __weak typeof(self) weakSelf = self;
    _navVarView = [[OddityNavigationBarView alloc]init:@"" titleFont:[UIFont font_t_9] block:^{
        
        if ( weakSelf.currentIndex == 0 ) {
            
            [weakSelf dismissViewControllerAnimated:true completion:nil];
        }else{
            
            [weakSelf moveToViewControllerAtIndex:0];
        }
    }];
    [self.view addSubview:_navVarView];
    
    if (self.isVideo) {
        
        _navVarView.borderView.hidden = true;
    }else{
        _navVarView.borderView.hidden = false;
    }
    
    self.containerView.bounces = false;
    
    [self.navVarView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(@44);
    }];
    
    _bottomInputView = [[OddityDetailBottomInputView alloc] init];
    _bottomInputView.delegate = self;
    [self.view addSubview:_bottomInputView];
    [_bottomInputView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.with.right.with.bottom.mas_equalTo(0);
        make.height.mas_equalTo(46);
    }];
    
    
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        if (self.isVideo) {
            make.top.equalTo(self.mas_topLayoutGuide);
        }else{
            make.top.equalTo(self.navVarView.mas_bottom);
        }
        
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(_bottomInputView.mas_top);
    }];
    
    
    _moreShareButton = [[OddityBigButton alloc]  init];
    _moreShareButton.alpha = 0;
    _moreShareButton.hidden = [OdditySetting shareOdditySetting].isDontWantToShare;
    [_moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:[OddityUISetting shareOddityUISetting].titleColor] forState:(UIControlStateNormal)];
    [self.navVarView addSubview:_moreShareButton];
    [_moreShareButton addTarget:self action:@selector(shareButtonMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [_moreShareButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.right.equalTo(self.navVarView);
        make.bottom.equalTo(self.navVarView);
        
        make.width.equalTo(@(44));
        make.height.equalTo(@(44));
    }];
    
    if ( _isVideo) {
    
        self.navVarView.cancelButton.tintColor = [UIColor whiteColor];
        [self.moreShareButton setImage:[UIImage oddityImage:@"video_share_white" byColor:[UIColor whiteColor]] forState:(UIControlStateNormal)];
    }
    
    [self.view layoutIfNeeded];
}

/// 处理点击分享按钮
-(void)shareButtonMethod{
    
    OddityShareViewController *shareViewController = [[OddityShareViewController alloc] initWithStyle:(OddityShareStyleNewContent) withObj:self.nContent];
    
    [self presentViewController:shareViewController animated:true completion:nil];
}


-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    
    _contentViewController = [[OddityNewContentViewController alloc]initWithSuperViewController:self];
    _contentViewController.delegate = self;
    _commentsViewController = [[OddityNewCommentsViewController alloc]initWithSuperViewController:self];
    
    return @[_contentViewController,_commentsViewController];
}



-(void)didTapInputView{

    OddityDetailInputViewController *inputViewController = [[OddityDetailInputViewController alloc] initWith:self.nContent];
    
    [self presentViewController:inputViewController animated:true completion:nil];
}

-(void)didTapShareButton{
    
    OddityShareViewController *shareViewController = [[OddityShareViewController alloc] initWithStyle:(OddityShareStyleNewContent) withObj:self.nContent];
    
    [self presentViewController:shareViewController animated:true completion:nil];
}
-(void)didTapCommentButton{
    
    if (self.currentIndex == 0) {
        
        // 打点 -> 用户动作 点击获取评论
        [OddityLogObject createAppAction: OddityLogAtypeCommentClick fromPageName:OddityLogPageDetailPage toPageName:OddityLogPageMyCommentPage effective: true params:@{ @"nid": @(self.nid) }];
    }
    
    [self moveToViewControllerAtIndex: self.currentIndex == 0 ? 1 : 0 ];
}

-(void)didTapCollectButton:(UIButton *)button{
    
    if (OddityShareUser.defaultUser.utype == 2) {
        
        [self presentViewController:[[OddityUserLoginViewController alloc]init] animated:true completion:nil]; // 登陆
        
        return;
    }
    
    typeof(self) __weak weakSelf = self;
    [self.nContent collect:^(BOOL success, NSString * _Nullable mess) {
        
        OddityAlertViewViewController *alertController = [[[OddityAlertViewViewController alloc] initWitch:mess] makeChange:mess after:0.5 block:^{
        
            [weakSelf dismissViewControllerAnimated:true completion:nil];
        }];
        
        button.tintColor = weakSelf.nContent.collectButtonTintColor;
        [button setImage: weakSelf.nContent.collectButtonImage forState:(UIControlStateNormal)];
        
        [weakSelf presentViewController:alertController animated:true completion:nil];
    }];
}

-(void)clickCommentButton:(OddityNewContentComment *)comment animate:(OddityAnimationNumberView *)view button:(OddityBigButton *)button{
    
    view.alpha = 1;
    view.hidden = false;
    
    view.currentNumber = comment.willShowNumberString;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        button.transform = CGAffineTransformScale(button.transform, 0.7, 0.7);
    }];
    
    typeof(self) __weak weakSelf = self;
    [comment vote:@[self.hotNotificationToken,self.normalNotificationToken] complete:^(OddityNewContentComment * _Nullable comm) {
        
        if (view.currentNumber != comm.commentAttributedString.string) {
            
            view.currentNumber = comm.commentAttributedString.string;
        }
        
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.35 initialSpringVelocity:0 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
            
            view.alpha = comm.commend <= 0 ? 0 : 1;
            view.hidden = comm.commend <= 0;
            
            [button setImage:comm.commentButtonImage forState:(UIControlStateNormal)];
            button.tintColor = comm.commentButtonTintColor;
            
            button.transform = CGAffineTransformIdentity;
            [button layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
            [weakSelf reloadMethod:comm];
            
            view.alpha = comm.commend <= 0 ? 0 : 1;
            view.hidden = comm.commend <= 0;
        }];
    }];
}

-(void)reloadMethod:(OddityNewContentComment *)comment{

    if (self.commentsViewController.tableView) {
        
        [self clickCommentButtonHandleMethod:comment tableView:self.commentsViewController.tableView];
    }
    
    if (self.contentViewController.tableView) {
        
        [self clickCommentButtonHandleMethod:comment tableView:self.contentViewController.tableView];
    }
}

-(void)clickCommentButtonHandleMethod:(OddityNewContentComment *)comment tableView:(UITableView *)tableView{

    NSMutableArray *array = [NSMutableArray array];
    
    for (UITableViewCell *cell in tableView.visibleCells) {
        
        if ([cell isKindOfClass:[OddityCommentTableViewCell class]] && [comment isEqual:((OddityCommentTableViewCell *)cell).comm]) {
            
            OddityAnimationNumberView *label = (OddityAnimationNumberView *)[(OddityCommentTableViewCell *)cell viewWithTag:10085];
            
            if (![label.currentNumber isEqualToString:comment.commentAttributedString.string]) {
                
                [array addObject:[tableView indexPathForCell:cell]];
            }
        }
    }
    
    if (array.count > 0) {
        
        [tableView reloadRowsAtIndexPaths:array withRowAnimation:(UITableViewRowAnimationNone)];
    }
}


-(void)didClickNewContentImage:(NSArray<NSString *> *)imgUrlArray index:(int)index clickFrame:(CGRect)rect{

    OddityNewDetailImageArrayViewController *imageArrayViewController = [[OddityNewDetailImageArrayViewController alloc] initWithImageArray:imgUrlArray currentIndex:index currentFrame:rect fromViewController:self];
    
    [self presentViewController:imageArrayViewController animated:true completion:nil];
}

@end




