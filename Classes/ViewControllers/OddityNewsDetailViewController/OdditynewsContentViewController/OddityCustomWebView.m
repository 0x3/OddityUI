//
//  OddityCustomWebView.m
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//

#import "OddityCustomWebView.h"

@implementation OddityCustomWebView

-(instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.scrollView.delegate = self;
    }
    return self;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [UIMenuController sharedMenuController].menuVisible = false;
}

@end
