//
//  OddityCustomWkWebView.h
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//

#import <WebKit/WebKit.h>

@interface OddityCustomWkWebView : WKWebView

@property (nonatomic,assign) CGPoint currentOffset;

@end
