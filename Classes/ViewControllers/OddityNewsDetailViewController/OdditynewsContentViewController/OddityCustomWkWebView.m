//
//  OddityCustomWkWebView.m
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//

#import "OddityCustomWkWebView.h"

@interface OddityCustomWkWebView()<UIScrollViewDelegate>

@end

@implementation OddityCustomWkWebView

-(instancetype)initWithFrame:(CGRect)frame configuration:(WKWebViewConfiguration *)configuration{
    
    self = [super initWithFrame:frame configuration:configuration];
    if (self) {
        
        self.scrollView.delegate = self;
    }
    return self;
    
}

-(void)setCurrentOffset:(CGPoint)currentOffset{
    
    _currentOffset = currentOffset;
    
    self.scrollView.contentOffset = _currentOffset;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self.scrollView setContentOffset:_currentOffset];
    
    [UIMenuController sharedMenuController].menuVisible = false;
}


@end
