//
//  OddityNewContentViewController+UIWeb.h
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//

#import "OddityNewContentViewController.h"

@interface OddityNewContentViewController (UIWeb)<UIWebViewDelegate>

-(void)fixHeightFoWebView;
@end
