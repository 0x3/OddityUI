//
//  OddityNewContentViewController+UIWeb.m
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//

#import <Masonry/Masonry.h>
#import "OddityCategorys.h"
#import "OddityNewsDetailViewController.h"
#import "OddityNewAdsDetailViewController.h"

#import "OddityNewContentViewController+UIWeb.h"

#define OddityWebViewContextKeyPathString @"documentView.webView.mainFrame.javaScriptContext"

@implementation OddityNewContentViewController (UIWeb)

-(void)fixHeightFoWebView{
    
    CGFloat height =  [[self.webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('section').offsetHeight"] floatValue];
    
    if ( self.headerView.frame.size.height != height + 25 ){
        
        CGRect temp = self.headerView.frame;
        temp.size.height = height+25;
        
        self.headerView.frame  = temp;
        
        self.tableView.tableHeaderView = self.headerView;
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
    typeof(self) __weak weakSelf = self;
    
    self.context =[webView valueForKeyPath:OddityWebViewContextKeyPathString];
    
    self.context[@"clickImage"] = ^(int index, NSString *imgframe) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf clickDetailWebViewImage:index frame:CGRectFromString([NSString stringWithFormat:@"%@",imgframe])];
        });
    };
    
    self.context[@"scrollToLoadImage"] = ^(int index, NSString *imgUrl) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf HandleUrlAndIndex:imgUrl :index];
        });
    };
    
    self.context.exceptionHandler = ^(JSContext *context, JSValue *exceptionValue) {
        
        context.exception = exceptionValue;
        
        NSLog(@"异常信息：%@", exceptionValue);
    };
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    if (error.code == -999) {
        return;
    }
    
    [self.superViewController.waitView goErrorStyle:^{
        
        [webView reload];
    }];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [self fixHeightFoWebView];
    
    [self scrollViewDidScroll:self.tableView];
    
    [self.superViewController oddity_hiddenWaitView];
}


-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否跳转" message:@"跳转查看更多相关资讯" preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                
                OddityAdsNewDetailsViewController *adsVC = [[OddityAdsNewDetailsViewController alloc]initWithUrlReuqest:request];
                
                [[UIViewController oddityCurrentViewController] presentViewController:adsVC animated:true completion:nil];
            }];
            
            [alertController addAction:cancelAction];
            [alertController addAction:okAction];
            
            [[UIViewController oddityCurrentViewController] presentViewController:alertController animated:true completion:nil];
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否跳转" message:@"跳转查看更多相关资讯" delegate:nil cancelButtonTitle:@"返回" otherButtonTitles:@"好的", nil];
            
            [alert oddity_showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
                
                if (buttonIndex == 1) {
                    
                    OddityAdsNewDetailsViewController *adsVC = [[OddityAdsNewDetailsViewController alloc]initWithUrlReuqest:request];
                    
                    [[UIViewController oddityCurrentViewController] presentViewController:adsVC animated:true completion:nil];
                }
            }];
        }
        
        return false;
    }
    
    return true;
}
@end
