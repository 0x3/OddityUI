//
//  OddityNewContentViewController+WKWeb.h
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//

#import "OddityNewContentViewController.h"

@interface OddityNewContentViewController (WKWeb)<WKScriptMessageHandler,WKNavigationDelegate>

-(void)adaptionWebViewHeightMethod:(void (^ _Nullable)())completionHandler;
@end
