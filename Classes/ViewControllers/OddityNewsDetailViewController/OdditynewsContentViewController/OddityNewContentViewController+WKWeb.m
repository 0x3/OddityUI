//
//  OddityNewContentViewController+WKWeb.m
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//

#import <Masonry/Masonry.h>
#import "OddityCategorys.h"
#import "OddityNewsDetailViewController.h"
#import "OddityNewAdsDetailViewController.h"

#import "OddityNewContentViewController+WKWeb.h"

@implementation OddityNewContentViewController (WKWeb)

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    
    self.tableView.tableHeaderView = self.headerView;
    self.headerView.userInteractionEnabled = true;
    [self.headerView addSubview:self.wkWebView ];
    [self.wkWebView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view.mas_top);
        make.left.equalTo(self.headerView.mas_left);
        make.right.equalTo(self.headerView.mas_right);
        make.height.equalTo(self.tableView.mas_height);
    }];
    
    
    typeof(self) __weak weakSelf = self;
    [self adaptionWebViewHeightMethod:^{
        
        [weakSelf.superViewController oddity_hiddenWaitView];
        [weakSelf scrollViewDidScroll:weakSelf.tableView];
    }];
}



-(void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    
    [self.superViewController.waitView goErrorStyle:^{
        
        [webView reload];
    }];
}

-(void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
    
    [self.superViewController.waitView goErrorStyle:^{
        
        [webView reload];
    }];
}

-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    
    
    if (navigationAction.navigationType == WKNavigationTypeLinkActivated) {
        
        typeof(self) __weak weakSelf = self;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否跳转" message:@"跳转查看更多相关资讯" preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            
            OddityAdsNewDetailsViewController *adsVC = [[OddityAdsNewDetailsViewController alloc]initWithUrlReuqest:navigationAction.request];
            
            [weakSelf presentViewController:adsVC animated:true completion:nil];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:true completion:nil];
        
        decisionHandler(WKNavigationActionPolicyCancel);
        
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
}

-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    
    if( [message.body[@"type"] integerValue] == 0 ) {
        
        [self adaptionWebViewHeightMethod:nil];
    }
    
    if( [message.body[@"type"] integerValue] == 3 ) {
        
        NSString *url = message.body[@"url"];
        int index = [message.body[@"index"] intValue];
        
        if (url && url) {
            
            [self HandleUrlAndIndex:url :index];
        }
    }
    
    if ([message.body[@"type"] integerValue] == 1) {
        
        /// 点击占位
        [self clickDetailWebViewImage:[message.body[@"index"] intValue] frame:CGRectFromString([NSString stringWithFormat:@"%@",message.body[@"imgframe"]])];
    }
}

-(void)adaptionWebViewHeightMethod:(void (^ _Nullable)())completionHandler{
    
    typeof(self) __weak weakSelf = self;
    [self.wkWebView evaluateJavaScript:@"document.getElementById('section').offsetHeight" completionHandler:^(id _Nullable data, NSError * _Nullable error) {
        
        CGFloat height = [data floatValue];
        
        if ( weakSelf.headerView.frame.size.height != height + 25 && height > 0){
            
            CGRect temp = weakSelf.headerView.frame;
            temp.size.height = height+25;
            weakSelf.headerView.frame  = temp;
            weakSelf.tableView.tableHeaderView = weakSelf.headerView;
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if(completionHandler) {
                completionHandler();
            }
        });
    }];
}


@end
