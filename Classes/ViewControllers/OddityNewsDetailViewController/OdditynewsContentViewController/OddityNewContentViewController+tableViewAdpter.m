//
//  OddityNewContentViewController+tableViewAdpter.m
//  Pods
//
//  Created by 荆文征 on 2017/7/5.
//
//
#import "RLMRealm+Oddity.h"
#import "OddityLogObject.h"

#import "OddityNew+Advertisement.h"

#import "UIButton+Oddity.h"

#import "OddityNew.h"
#import "OddityCustomTableViewCell.h"
#import "UITableView+FDTemplateLayoutCell.h"

#import "OddityNewsDetailViewController.h"
#import "OddityNewArrayListViewController.h"

#import "OddityNewContentViewController+tableViewAdpter.h"

@implementation OddityNewContentViewController (tableViewAdpter)

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        return UITableViewAutomaticDimension;
    }else{
        
        return [self tableViewHeightCalendar:tableView forIndexPath:indexPath];
    }
}


-(UITableViewCell *) makeCellFor:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0 && self.nContent) {
        
        OddityVideoTitleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"video"];
        
        [cell configCell:self.nContent.title];
        
        return cell;
    }
    
    if (indexPath.section == 1 && self.journalism) {
        
        OddityAdsImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ads"];
        
        /**
         *  默认不上传 因为webview 默认广告显示，降低 展示率 和 点击率的 比例 增加收入。
         */
        if (!self.webView.isLoading && !self.wkWebView.isLoading) {
            
            GDTNativeAd *ad = [GDTNativeAd initGDTNativeAdWithAdId:self.journalism.advertisementId];
            
            [self.journalism ExposureAdvertisement:cell nativeAD:ad];
        }
        
        
        [cell setNewObject:self.journalism];
        
        return cell;
    }
    
    /**
     *  显示热门评论的 UITableViewCell
     */
    if( indexPath.section == 2 && self.nContent.hotCommits.count > indexPath.row ) {
        
        if( indexPath.row == 3 ) {
            
            OddityToMoreCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"toMoreC"];
            
            [cell refresh];
            
            typeof(self) __weak weakSelf = self;
            [(UIButton *)[cell viewWithTag:123] oddity_handleControlEvent:(UIControlEventTouchUpInside) withBlock:^{
                
                // 打点 -> 用户动作 点击获取评论
                [OddityLogObject createAppAction: OddityLogAtypeCommentClick fromPageName:OddityLogPageDetailPage toPageName:OddityLogPageMyCommentPage effective: true params:@{ @"nid": @(weakSelf.superViewController.nid) }];
                
                [weakSelf.superViewController moveToViewControllerAtIndex:1];
            }];
            
            return cell;
        }
        
        OddityCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        
        cell.delegate = self.superViewController;
        
        [cell setNewContentComment:self.nContent.hotCommits[indexPath.row]];
        
        return cell;
    }
    
    /**
     *  显示相关新闻Cell
     */
    if( indexPath.section == 3 && self.nContent.aboutNewList.count > indexPath.row ) {
        
        OddityNewContentAbout *about = self.nContent.aboutNewList[indexPath.row];
        
        UITableViewCell *rcell;
        
        if ( [about cellStyle] == AboutNewStyleText ) {
            
            OddityAboutNewTextTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"text"];
            
            [cell setAboutNew:about];
            
            rcell = cell;
        }
        
        if ( [about cellStyle] == AboutNewStyleImage ) {
            
            OddityAboutNewImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"image"];
            
            [cell setAboutNew:about isVideo:self.superViewController.isVideo];
            
            rcell = cell;
        }
        
        if (about.isAdsAbout) {
            
            GDTNativeAd *ad = [GDTNativeAd initGDTNativeAdWithAdId:about.advertisementId];
            
            [about ExposureAdvertisement:rcell nativeAD:ad];
        }
        
        return rcell;
    }
    
    return nil;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    /**
     *  如果当前为视频详情 并且 为 seciton = 0
     */
    if( section == 0 && self.superViewController.isVideo && self.nContent) {
        
        return 1;
    }
    
    /**
     *  如果section == 1 并且当前页面的广告新闻不为空
     */
    if ( section == 1 && self.journalism ) {
        
        return 1;
    }
    
    /**
     *  如果当前的 热门评论 大于 0 ，就显示 。如果大于4 则只显示4行
     */
    if ( section == 2 && self.nContent){
        
        return self.nContent.hotCommits.count > 4 ? 4 : self.nContent.hotCommits.count;
    }
    
    /**
     *  如果为第三行 那么显示相关新闻的个数
     */
    if( section == 3 && self.nContent) {
        
        return self.nContent.aboutNewList.count;
    }
    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return [self makeCellFor:tableView cellForRowAtIndexPath:indexPath];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if ( section == 2 && self.nContent.hotCommits.count > 0 ) {
        
        return 37;
    }
    
    if ( section == 3 && self.nContent.aboutNewList.count > 0 ) {
        
        return 37;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    OddityDetailViewTableHeaderView *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"daccell"];
    
    if ( section == 2 ){
        
        [cell setDescInfoString: [self.nContent HotHeaderString]];
    }
    
    if ( section == 3 ){
        
        [cell setDescInfoString: self.superViewController.isVideo ? @"相关视频" : @"相关推荐"];
    }
    
    return cell;
}


-(CGFloat)tableViewHeightCalendar:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath{
    
    
    typeof(self) __weak weakSelf = self;
    
    if (indexPath.section == 0 && self.nContent) {
        
        return [tableView fd_heightForCellWithIdentifier:@"video" configuration:^(id cell) {
            
            [(OddityVideoTitleTableViewCell *)cell configCell:weakSelf.nContent.title];
        }];
    }
    
    if (indexPath.section == 1 && self.journalism) {
        
        return [tableView fd_heightForCellWithIdentifier:@"ads" configuration:^(id cell) {
            
            [(OddityAdsImageTableViewCell *)cell setNewObject:weakSelf.journalism];
        }];
    }
    
    /**
     *  显示热门评论的 UITableViewCell
     */
    if( indexPath.section == 2 && self.nContent.hotCommits.count > indexPath.row ) {
        
        if( indexPath.row == 3 ) {
            
            return [tableView fd_heightForCellWithIdentifier:@"toMoreC" configuration:^(id cell) {
                
            }];
        }
        
        return [tableView fd_heightForCellWithIdentifier:@"cell" configuration:^(id cell) {
            
            [(OddityCommentTableViewCell *)cell setNewContentComment:weakSelf.nContent.hotCommits[indexPath.row]];
        }];
    }
    
    /**
     *  显示相关新闻Cell
     */
    if( indexPath.section == 3 && self.nContent.aboutNewList.count > indexPath.row ) {
        
        OddityNewContentAbout *about = self.nContent.aboutNewList[indexPath.row];
        
        if ( [about cellStyle] == AboutNewStyleText ) {
            
            return [tableView fd_heightForCellWithIdentifier:@"text" configuration:^(id cell) {
                
                [(OddityAboutNewTextTableViewCell *)cell setAboutNew:about];
            }];
        }
        
        [OddityLogObject createRelateNewShow:about channel:self.nContent.channel];
        
        return [tableView fd_heightForCellWithIdentifier:@"image" configuration:^(id cell) {
            
            [(OddityAboutNewImageTableViewCell *)cell setAboutNew:about isVideo:weakSelf.superViewController.isVideo];
        }];
    }
    
    return 100;
}




-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ( indexPath.section == 1 && self.journalism ) {
        
        
        // 打点 -> 点击广告打点
        [OddityLogObject createAdsClick:self.journalism];
        
        GDTNativeAd *ad = [GDTNativeAd initGDTNativeAdWithAdId:self.journalism.advertisementId];
        
        [self.journalism ClickAdvertising:tableView indexPath:indexPath nativeAD:ad viewController:self.superViewController];
        
        if ([OddityPlayerView sharePlayerView].isPlayerViewShow && ![OdditySetting shareOdditySetting].isWatchVideoAllTheTime) {
            
            [[OddityPlayerView sharePlayerView] closePlayerView];
        }
        
        return;
    }
    
    if( indexPath.section == 3 && self.nContent.aboutNewList.count > indexPath.row) {
        
        OddityNewContentAbout *about = self.nContent.aboutNewList[indexPath.row];
        
        
        if (about.isAdsAbout) {
            
            // 打点 -> 点击广告打点
            [OddityLogObject createAdsClick:about];
            
            [about ClickAdvertising:tableView indexPath:indexPath nativeAD: [GDTNativeAd initGDTNativeAdWithAdId:about.advertisementId] viewController:self.superViewController];
            
            if ([OddityPlayerView sharePlayerView].isPlayerViewShow && ![OdditySetting shareOdditySetting].isWatchVideoAllTheTime) {
                
                [[OddityPlayerView sharePlayerView] closePlayerView];
            }
            
            return;
        }
        
        if (self.superViewController.isVideo) {
            
            self.superViewController.isSourceAboutNewClick = true;
            self.superViewController.needRePlay = true;
            self.superViewController.justGoBack = true;
            self.superViewController.videoObjt = nil;
            [self refreshVideoObjecyAndFrame:about.nid];
            
            [self.superViewController rEqByNid:about.nid];
            
            return;
        }
        
        OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc]initWithNid:[about getTrueNewId] isVideo:self.superViewController.isVideo];
        
        newDACViewController.isSourceAboutNewClick = true;
        [newDACViewController makeLogType:about.logtype AndLogChid:about.logchid cid: 0];
        
        [self.superViewController presentViewController:newDACViewController animated:true completion:^{
            
            [about toReadedMethod];
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
        }];
    }
}


-(void)refreshVideoObjecyAndFrame:(int)nid{
    
    OddityNew *new = [OddityNew objectInRealm:RLMRealm.OddityRealm forPrimaryKey:@(nid)];
    
    if (new) {
        
        self.superViewController.videoObjt = new;
        
        
        if (self.superViewController.mainManagerViewController) {
            
            OddityNewArrayListViewController *viewController = self.superViewController.mainManagerViewController.currentViewController;
            
            OddityNewVideoTableViewCell *ncell;
            
            for (UITableViewCell *cell in viewController.tableView.visibleCells) {
                
                if ([cell isKindOfClass:[OddityNewVideoTableViewCell class]]) {
                    
                    if ([((OddityNewVideoTableViewCell *)cell).object isEqual:new]) {
                        
                        ncell = (OddityNewVideoTableViewCell *)cell;
                        
                        break;
                    }
                }
            }
            
            if (ncell) {
                
                self.superViewController.videoPresentdAnimation.viewControllerFromRect = [ncell.firstImageView convertRect:ncell.firstImageView.bounds toView:UIApplication.sharedApplication.keyWindow];
                
                self.superViewController.justGoBack = false;
            }
        }
    }
}


@end
