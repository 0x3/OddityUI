//
//  OddityNewContentViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityCustomWebView.h"
#import "OddityCustomWkWebView.h"


#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "OddityThemeImageView.h"
#import <JavaScriptCore/JavaScriptCore.h>

#import "OddityContentAdnCommentBaseViewController.h"

@protocol OddityNewContentViewControllerDelegate <NSObject>

-(void)didClickNewContentImage:(NSArray<NSString *> *_Nullable)imgUrlArray index:(int)index clickFrame:(CGRect)rect;

@end



@interface OddityNewContentViewController : OddityContentAdnCommentBaseViewController

@property(nonatomic,strong) OddityNew * _Nullable journalism;

/// ios8 以下的 js 适配
@property(nonatomic,strong) JSContext * _Nullable context;

#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wnullability-completeness"
@property(nonatomic,weak) id <OddityNewContentViewControllerDelegate> delegate;
#pragma clang diagnostic pop

@property(nonatomic,strong) OddityCustomWkWebView * _Nonnull wkWebView;

@property(nonatomic,strong) OddityCustomWebView *_Nonnull webView;

@property(nonatomic,strong) UIView * _Nonnull headerView;

/// 视频播放背景视图
@property(nonatomic,strong) OddityThemeImageView * _Nonnull playView;

/// 刷新 Webview
-(void)reloadWebview;

-(instancetype _Nonnull )initWithSuperViewController:(OddityNewsDetailViewController * _Nonnull )viewController;


/**
 刷新 评论 个数的 的 描述
 */
-(void)refreshWithCommentSuccess;


/**
 隐藏 详情页内的 图片

 @param hidden 是否隐藏
 @param index 图片的 index
 */
-(void)makeHidden:(BOOL)hidden Index:(int)index;


/**
 滑动到第几个 图片

 @param index 图片的 index
 */
-(void)scrollWebViewImageForIndex:(int)index;


/**
 获取图片 Index

 @param index 图片的index
 @param completionHandler rect
 */
-(void)getImageCurrentFrameWithImageIndex:(int)index complete:(void (^ _Nullable)(CGRect rect))completionHandler;

/**
 根据 index 和 url 处理图片的加载操作

 @param url 图片的地址
 @param index 图片index
 */
-(void)HandleUrlAndIndex:(NSString *_Nullable)url :(int)index;


/**
 点击 新闻详情页内的 图片

 @param index 图片的 index
 @param rect 图片的 rect
 */
-(void)clickDetailWebViewImage:(int)index frame:(CGRect)rect;


/**
 开放 滑动方法

 @param scrollView 滑动视图
 */
-(void)scrollViewDidScroll:(UIScrollView *_Nullable)scrollView;
@end
