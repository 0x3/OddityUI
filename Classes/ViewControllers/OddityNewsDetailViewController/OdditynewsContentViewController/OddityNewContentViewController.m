//
//  OddityNewContentViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/17.
//
//

#import "OddityModels.h"

#import "OddityUtils.h"
#import "OddityCategorys.h"
#import "OddityPlayerView.h"

#import "OddityNew+Advertisement.h"

#import "OddityNewsDetailViewController.h"
#import "OddityNewContentViewController.h"

#import "OddityNewAdsDetailViewController.h"

#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

#import "OddityNewArrayListViewController.h"

#import "OddityNewDetailImageViewController.h"


#import "OddityNewContentViewController+UIWeb.h"
#import "OddityNewContentViewController+WKWeb.h"
#import "OddityNewContentViewController+tableViewAdpter.h"

#define WebViewSystemVersion 8.0

#define OddityWkWebViewConfiguration @"JSBridge"


@interface OddityNewContentViewController ()<UIAlertViewDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong) NSMutableDictionary *imageArray;

@property(nonatomic,strong) UIButton *playButton;

@property(nonatomic,strong) WKWebViewConfiguration *configuration;

@end

@implementation OddityNewContentViewController

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    
    if (!self.presentedViewController) {
        
        self.wkWebView.scrollView.delegate = nil;
        
        [self.configuration.userContentController removeScriptMessageHandlerForName: OddityWkWebViewConfiguration];
    }
}


/// 滑动到第 Index 个图片
-(void)scrollWebViewImageForIndex:(int)index{
    
    typeof(self) __weak weakSelf = self;
    [self getImageCurrentFrameWithImageIndex:index complete:^(CGRect rect) {
        
        [weakSelf.tableView setContentOffset:CGPointMake(0, CGRectGetMinY(rect)-20) animated:true];
    }];
}

-(void)makeHidden:(BOOL)hidden Index:(int)index{
    
    NSString *javaScriptString = [NSString stringWithFormat:@"makeHidden(%d,%d)",hidden,index];
    
    if (self.wkWebView) {
        
        [self.wkWebView evaluateJavaScript:javaScriptString completionHandler: nil];
    }
    
    if (self.webView) {
        
        [self.webView stringByEvaluatingJavaScriptFromString:javaScriptString];
    }
}

-(void)getImageCurrentFrameWithImageIndex:(int)index complete:(void (^)(CGRect rect))completionHandler{
    
    NSString *javaScriptString = [NSString stringWithFormat:@"getAbsoluteRectByIndex(%d)",index];
    
    if (self.wkWebView) {
     
        [self.wkWebView evaluateJavaScript:javaScriptString  completionHandler:^(id _Nullable data, NSError * _Nullable error) {
            
            CGRect rect = CGRectFromString([NSString stringWithFormat:@"%@",data]);
            
            completionHandler(rect);
        }];
    }
    
    if (self.webView) {
    
        CGRect rect = CGRectFromString([self.webView stringByEvaluatingJavaScriptFromString:javaScriptString]);
        
        completionHandler(rect);
    }
}

-(void)clickDetailWebViewImage:(int)index frame:(CGRect)rect{
    
    CGRect nrect = rect;
    
    nrect.origin.y = rect.origin.y-self.wkWebView.scrollView.contentOffset.y;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickNewContentImage:index:clickFrame:)]) {
        
        [self.delegate didClickNewContentImage:self.nContent.contentBodyImageArray index:index clickFrame:nrect];
    }
}

-(void)HandleUrlAndIndex:(NSString *)url :(int)index{
    
    typeof(self) __weak weakSelf = self;
    
    [url oddity_downloadImageByUrl:nil completed:^(NSString *results) {
        
        NSString *javaScriptString = [NSString stringWithFormat:@"$(\"img\").eq(%d).attr(\"src\",\"%@\")",index,results];
        
        if (weakSelf.wkWebView) {
            
            [weakSelf.wkWebView evaluateJavaScript:javaScriptString completionHandler: nil];
        }
        
        if (weakSelf.webView) {
            
            [weakSelf.webView stringByEvaluatingJavaScriptFromString:javaScriptString];
        }
    }];
}

-(void)didReceiveMemoryWarning{
    
    [super didReceiveMemoryWarning];
    
    if (!self.superViewController.isVideo) {
        typeof(self) __weak weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf reloadWebview];
        });
    }
}


-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (!self.superViewController.isVideo) {
        typeof(self) __weak weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf reloadWebview];
        });
    }
}

-(void)reloadWebview{
    
    if (self.wkWebView) {
        
        if ( self.wkWebView.title.length <= 0  && self.nContent) {
            
            [self.wkWebView loadHTMLString:[self.nContent WK_WebViewLocalHtmlString:true] baseURL:NSBundle.oddity_shareBundle.bundleURL];
        }
    }
    
    if (self.webView) {
        
        NSString *theTitle=[self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
        
        if ( theTitle.length <= 0 && self.nContent) {
            
            [self.webView loadHTMLString:[self.nContent WK_WebViewLocalHtmlString:false] baseURL:NSBundle.oddity_shareBundle.bundleURL];
        }
    }
}


-(instancetype)initWithSuperViewController:(OddityNewsDetailViewController *)viewController{
    
    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        
        self.superViewController = viewController;
        
        self.tableView.tag = 100;
        
        _headerView = [[UIView alloc]init];
        _headerView.frame = self.view.frame;
        _headerView.clipsToBounds = true;
        _headerView.backgroundColor = [UIColor color_t_6];
        
        if( !viewController.isVideo ){
            
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= WebViewSystemVersion) {
                
                _configuration = [[WKWebViewConfiguration alloc] init];
                [_configuration.userContentController addScriptMessageHandler:self name: OddityWkWebViewConfiguration];
                
                _wkWebView = [[OddityCustomWkWebView alloc]initWithFrame:(CGRectZero) configuration:_configuration];
                _wkWebView.backgroundColor = [UIColor clearColor];
                _wkWebView.scrollView.backgroundColor = [UIColor clearColor];
                _wkWebView.navigationDelegate = self;
                _wkWebView.userInteractionEnabled = true;
                _wkWebView.scrollView.scrollEnabled = false;
                _wkWebView.scrollView.scrollsToTop = false;
                _wkWebView.scrollView.bounces = false;
            }else{
                
                _webView = [[OddityCustomWebView alloc]init];
                _webView.delegate = self;
                _webView.backgroundColor = [UIColor color_t_6];
                _webView.scrollView.scrollEnabled = false;
                _webView.scrollView.scrollsToTop = false;
                _webView.scrollView.bounces = false;
            }
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    if (self.superViewController.isVideo) {
        
        [self makePlayView];
        [self makePlayButton];
        [self makeTableView];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFontModeMethod) name:@"oddityFontModeChange" object:nil];
}

-(void)makePlayView{

    _playView = [[OddityThemeImageView alloc] init];
    [_playView setPin_updateWithProgress:YES];
    _playView.userInteractionEnabled = true;
    _playView.clipsToBounds = true;
    _playView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_playView];
    [_playView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.mas_topLayoutGuide);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.height.equalTo(_playView.mas_width).multipliedBy((CGFloat)211/375);
    }];
}

-(void)makePlayButton{

    _playButton = [[UIButton alloc]init];
    [_playButton setImage:[UIImage oddity_video_play_image] forState:(UIControlStateNormal)];
    [_playButton addTarget:self action:@selector(playVideo) forControlEvents:(UIControlEventTouchUpInside)];
    [self.playView addSubview:self.playButton];
    [self.playButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.center.equalTo(self.playView);
    }];
}

-(void)makeTableView{

    [self.tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.playView.mas_bottom);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [self.view layoutIfNeeded];
}

-(void)refreshFontModeMethod{
    
    if (!self.superViewController.isVideo) {
        
        NSString *javascript = [NSString stringWithFormat:@"refreshFontModeMethod(%f,%f)",UIFont.font_t_text_detail_title.pointSize,UIFont.font_t_detail_context.pointSize];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= WebViewSystemVersion) {
            
            [self.wkWebView evaluateJavaScript:javascript completionHandler:nil];
            
            [self adaptionWebViewHeightMethod:nil];
        }else{
            
            [self.webView stringByEvaluatingJavaScriptFromString:javascript];
            
            [self fixHeightFoWebView];
        }
    }
    
    [self.tableView reloadData];
}

-(void)refreshThemeMethod{
    
    self.tableView.backgroundColor = [UIColor color_t_6];
    [self.tableView reloadData];
    
    if (!self.superViewController.isVideo) {
        
        CGFloat alphtFloat = [OddityUISetting shareOddityUISetting].oddityThemeMode == OddityThemeModeNormal ? 1 : 0.45;;
        
        NSString *javascript = [NSString stringWithFormat:@"refreshThemeMethod('%@','%@','%@','%@',%f)",UIColor.color_t_6.oddity_color2String,UIColor.color_t_2.oddity_color2String,UIColor.color_t_3.oddity_color2String,UIColor.color_t_5.oddity_color2String,alphtFloat];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= WebViewSystemVersion) {
            
            [self.wkWebView evaluateJavaScript:javascript completionHandler:nil];
        }else{
            
            [self.webView stringByEvaluatingJavaScriptFromString:javascript];
        }
    }
}

-(void)refreshWithCommentSuccess{
    
    NSString *javascript = [NSString stringWithFormat:@"refreshInfoDescString('%@')",[self.nContent jouDescString:true]];
    
    __weak typeof(self) weakSelf = self;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= WebViewSystemVersion) {
        
        [self.wkWebView evaluateJavaScript:javascript completionHandler:^(id _Nullable ij, NSError * _Nullable error) {
            
            [weakSelf adaptionWebViewHeightMethod:nil];
        }];
    }else{
        
        [self.webView stringByEvaluatingJavaScriptFromString:javascript];
        
        [self fixHeightFoWebView];
    }
}

-(void)playVideo{
    
    OddityPlayerObject *playObject = [[OddityPlayerObject alloc] initWith:self.superViewController.nid VideoUrl:self.superViewController.videoUrl Title:self.superViewController.nContent.title Objtect:self.superViewController.videoObjt];
    [[OddityPlayerView sharePlayerView] playWithPlayerObject:playObject];
    
    [[OddityPlayerView sharePlayerView] toState:(OddityPlayViewStateNormal) inView:self.playView];
}



-(void)setContentAfterRequestFinished:(OddityNewContent *)newContent{
    
    [super setContentAfterRequestFinished:newContent];
    
    if ( !self.superViewController.isVideo ) {
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= WebViewSystemVersion) {
            
            [_wkWebView loadHTMLString:[newContent WK_WebViewLocalHtmlString:false] baseURL:[NSBundle oddity_shareBundle].bundleURL];
            
        }else{
            
            CGRect temp = _headerView.frame;
            
            temp.size.height = self.tableView.frame.size.height;
            
            _headerView.frame  = temp;
            
            self.tableView.tableHeaderView = _headerView;
            [_headerView addSubview:_webView ];
            [_webView mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.top.equalTo(self.view.mas_top);
                make.left.equalTo(_headerView.mas_left);
                make.right.equalTo(_headerView.mas_right);
                make.height.equalTo(self.tableView.mas_height);
            }];
            
            
            [_webView loadHTMLString:[newContent WK_WebViewLocalHtmlString:true] baseURL:[NSBundle oddity_shareBundle].bundleURL];
        }
    }else{
        
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:(UITableViewRowAnimationNone)];
        
        [self.superViewController oddity_hiddenWaitView];
    }
    
    [self humbnail:newContent];
    
    __weak typeof(self) weakSelf = self;
    [OddityNew requestAdsMethod:^(OddityNew *new) {
        
        if (new) {
            
            weakSelf.journalism = new;
            
            [weakSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:(UITableViewRowAnimationNone)];
            
            /// 打点 -> 获取广告打点
            [OddityLogObject createAdGet:new snum:1 rnum:1];
            
        }else{
            
            /// 打点 -> 获取广告打点
            [OddityLogObject createAdGet:nil snum:1 rnum:0];
        }
    }];
}

/// 设置缩略图
-(void)humbnail:(OddityNewContent *)newContent{
    
    if (self.superViewController.isVideo) {
        
        [_playView pin_setImageFromURL:[[NSURL alloc]initWithString:newContent.thumbnail]];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    typeof(self) __weak weakSelf = self;
    if ( !weakSelf.superViewController.isVideo ) {
        
        CGFloat height = oddityUIScreenHeight+scrollView.contentOffset.y;
        
        NSString *javaScriptString = [NSString stringWithFormat:@"scrollMethod(%f,%d)",height,[[OdditySetting shareOdditySetting]isCacheDetailImage] ? 1 : 0];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= WebViewSystemVersion) {
            
            // 发送当前华东位置
            [weakSelf adaptionWebViewHeightMethod:nil];
            
            _wkWebView.currentOffset = scrollView.contentOffset;
            _wkWebView.scrollView.contentInset = UIEdgeInsetsMake(10000, 0, 10000, 0);
            
            [_wkWebView evaluateJavaScript:javaScriptString completionHandler:nil];
            
        }else{
            
            _webView.scrollView.contentOffset = scrollView.contentOffset;
            
            [_webView stringByEvaluatingJavaScriptFromString:javaScriptString];
            
            [weakSelf fixHeightFoWebView];
        }
    }
    
    CGFloat thismaxy = scrollView.contentOffset.y+CGRectGetHeight(scrollView.frame);
    CGFloat progress = thismaxy/CGRectGetHeight(self.headerView.frame);
    
    [self.nContent savaPercentValue:(float)progress];
}

@end
