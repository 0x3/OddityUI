//
//  OdditySearchResultViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/27.
//
//

#import <UIKit/UIKit.h>
#import "OdditySearch.h"
#import "OddityCustomView.h"

@interface OdditySearchResultViewController : UIViewController

@property(nonatomic,strong) OdditySearch *searchObject;

@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) OddityNavigationBarView *navVarView;

@property(nonatomic,strong) OdditySearchHeaderView *searchHeaderView;

@end
