//
//  OdditySearchResultViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/27.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"

#import "OddityUISetting+Theme.h"

#import "OddityHtmlTitleCache.h"
#import "OddityCustomTableViewCell.h"
#import "OddityPromptInfoTableViewCell.h"
#import "OddityNewsDetailViewController.h"
#import "OddityCustomSearchTransitioning.h"
#import "OdditySearchResultViewController.h"
#import "UITableView+FDTemplateLayoutCell.h"

@interface OdditySearchResultViewController ()<UIViewControllerTransitioningDelegate,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property(nonatomic,strong) UIView *topView;

@property(nonatomic,strong) NSString *noFindAnyErrorString;
@property(nonatomic,assign) BOOL noFindAnyThingForThisWord;

@property(nonatomic,strong) RLMNotificationToken *notificationToken;

@property(nonatomic,strong) UIPanGestureRecognizer *panGestureRecognizer;

@property(nonatomic,strong) OddityCustomViewControllerDrivenInteractiveTransition *interactiveTransitioning;
@property(nonatomic,strong) OddityCustomSearchResultDismissTransitioning *customSearchDismissTransitioning;
@property(nonatomic,strong) OddityCustomSearchResultPresentedTransitioning *customSearchPresentedTransitioning;

@end

@implementation OdditySearchResultViewController


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [OddityUISetting searchStatusBarStyle];
}

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        _interactiveTransitioning = [[OddityCustomViewControllerDrivenInteractiveTransition alloc] init];
        _customSearchDismissTransitioning = [[OddityCustomSearchResultDismissTransitioning alloc ]init];
        _customSearchPresentedTransitioning = [[OddityCustomSearchResultPresentedTransitioning alloc ]init];
        
        self.transitioningDelegate = self;
        self.modalPresentationCapturesStatusBarAppearance = true;
    }
    return self;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{
    
    return _customSearchPresentedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{
    
    return _customSearchDismissTransitioning;
}



-(id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator{
    
    if (_customSearchDismissTransitioning.isInteraction) {
        
        return _interactiveTransitioning;
    }
    return nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor color_t_6];
    
    __weak typeof(self) weakSelf = self;
    _navVarView = [[OddityNavigationBarView alloc]init:@"" titleFont:[UIFont font_t_7] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    
    _navVarView.cancelButton.tintColor = [UIColor color_t_2];
    _navVarView.borderView.backgroundColor = [UIColor color_t_5];
    
    [self.view addSubview:_navVarView];
    [_navVarView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.height.mas_equalTo(44);
    }];
    
    _searchHeaderView = [[OdditySearchHeaderView alloc] init];
    [_navVarView addSubview:_searchHeaderView.inputTextFiled];
    _searchHeaderView.inputTextFiled.delegate = self;
    [_searchHeaderView.inputTextFiled mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(_navVarView.cancelButton.mas_right);
        make.right.mas_equalTo(-12);
        make.centerY.equalTo(_navVarView.cancelButton);
        make.height.mas_equalTo(28);
    }];
    
    [_searchHeaderView makeFeed];
    _navVarView.backgroundColor = _searchHeaderView.backgroundColor;
    
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = self.view.backgroundColor;
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(_navVarView.mas_bottom);
        make.left.with.right.with.bottom.mas_equalTo(0);
    }];
    
    [self.view layoutIfNeeded];

    [_tableView registerClass:[OddityPromptInfoTableViewCell class] forCellReuseIdentifier:@"prompt"];
    [_tableView registerClass:[OddityNewVideoTableViewCell class] forCellReuseIdentifier:@"video"];
    [_tableView registerClass:[OddityNewSpecialTableViewCell class] forCellReuseIdentifier:@"special"];
    [_tableView registerClass:[OddityRefreshTableViewCell class] forCellReuseIdentifier:@"refresh"];
    [_tableView registerClass:[OddityNewReserveTableViewCell class] forCellReuseIdentifier:@"nocontent"];
    [_tableView registerClass:[OddityNewTwoTableViewCell class] forCellReuseIdentifier:@"twocell"];
    [_tableView registerClass:[OddityNewThreeTableViewCell class] forCellReuseIdentifier:@"threecell"];
    [_tableView registerClass:[OddityNewBaseTableViewCell class] forCellReuseIdentifier:@"basecell"];
    [_tableView registerClass:[OddityNewOneTableViewCell class] forCellReuseIdentifier:@"onecell"];
    [_tableView registerClass:[OddityCoverImageTableViewCell class] forCellReuseIdentifier:@"bigcell"];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    self.tableView.mj_footer = self.CustomRefreshFooterView;
    
    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGestureRecognizerMethod:)];
    _panGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:_panGestureRecognizer];
    
    [self.tableView.panGestureRecognizer requireGestureRecognizerToFail:_panGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFontModeMethod) name:@"oddityFontModeChange" object:nil];
    
    _topView = [[UIView alloc] init];
    _topView.backgroundColor = UIColor.color_t_6;
    [self.view addSubview:_topView];
    [_topView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.bottom.mas_equalTo(_navVarView.mas_top);
        make.left.right.mas_equalTo(_navVarView);
        make.height.mas_equalTo(20);
    }];
}

-(void)refreshFontModeMethod{
    
    [self.tableView reloadData];
}

-(void)refreshThemeMethod{
    
    [_searchHeaderView makeFeed];
    _navVarView.backgroundColor = _searchHeaderView.backgroundColor;
    
    [_tableView reloadData];
    _tableView.backgroundColor = [UIColor color_t_6];
    self.view.backgroundColor = [UIColor color_t_6];
    [self setNeedsStatusBarAppearanceUpdate];
    
    _topView.backgroundColor = UIColor.color_t_6;
    
    _navVarView.cancelButton.tintColor = [UIColor color_t_2];
    _navVarView.borderView.backgroundColor = [UIColor color_t_5];
}

/**
 创建一个 尾部 请求接口

 @return 创建一个 自定义的 上拉加载
 */
-(OddityCustomRefreshFooterView *)CustomRefreshFooterView{
    
    __weak typeof(self) weakSelf = self;
    
    return [OddityCustomRefreshFooterView footerWithRefreshingBlock:^{
        
        int count = (int)weakSelf.searchObject.pressListArray.count;
        
        int pages = (int)count/(int)OddityRequestSearchLimit + 1 + ((int)count%(int)OddityRequestSearchLimit == 0 ? 0 : 1);
        
        [weakSelf requestMethod:pages];
    }];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    NSString *textValue = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (textValue.length <= 0) {
        
        NSLog(@"滚");
    }
    
    self.searchObject =  [OdditySearch createLogSearch:textValue];
    
    return true;
}

-(void)setSearchObject:(OdditySearch *)searchObject{

    if (searchObject.title != _searchObject.title || _searchObject.state != searchObject.state) {

        _searchObject = searchObject;
        
        [searchObject removeAllNewsArray];
        
        [self createNotifitionByNewSearchSelfObject];
    }
    
    self.noFindAnyThingForThisWord = false;
    
    [self.searchHeaderView.inputTextFiled resignFirstResponder];
    [self oddity_showWaitView:64];
    [self.tableView setContentOffset:(CGPointZero)];
    
    [self requestMethod:1];
}


-(void)requestMethod:(int)page{

    __weak typeof(self) weakSelf = self;
    [weakSelf.searchObject RequestPage:page finishBlock:^(OddityResponesState state) {
        
        [weakSelf oddity_hiddenWaitView];
        
        switch (state) {
            case OddityResponesStateDataFail:case OddityResponesStateWifiFail:{
            
                [weakSelf.tableView.mj_footer endRefreshing];
                
                weakSelf.noFindAnyThingForThisWord = true;
                weakSelf.noFindAnyErrorString = @"请求失败，待网络好些后，重新搜索";
                
                weakSelf.tableView.mj_footer = nil;
                
                [weakSelf.tableView reloadData];
            }
                break;
            case OddityResponesStateNoMore:{
            
                if (weakSelf.searchObject.pressListArray.count <= 0 ) {
                    
                    [weakSelf.tableView.mj_footer endRefreshing];
                    weakSelf.noFindAnyThingForThisWord = true;
                    weakSelf.noFindAnyErrorString = [NSString stringWithFormat:@"没有找到任何关于[%@]",self.searchObject.title];
                    
                    weakSelf.tableView.mj_footer = nil;
                    
                    [weakSelf.tableView reloadData];
                    
                }else{
                
                    [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            }
                break;
            case OddityResponesStateSuccess:{
                
                if (!weakSelf.tableView.mj_footer) {
                    
                    weakSelf.tableView.mj_footer = weakSelf.CustomRefreshFooterView;
                }
            }
                break;
        }
    }];
}


-(void)setNotificationToken:(RLMNotificationToken *)notificationToken{

    if (_notificationToken) {
        
        [_notificationToken stop];
    }
    
    _notificationToken = notificationToken;
}


/**
 根本 本地的 搜索对象 创建一个监控 对象
 */
-(void)createNotifitionByNewSearchSelfObject{
   
    __weak typeof(self) weakSelf = self;
    self.notificationToken = [self.searchObject.pressListArray addNotificationBlock:^(RLMArray<OddityNew *> * _Nullable array, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        UITableView *tableView = weakSelf.tableView;
        
        if (!changes) {
            [tableView reloadData];
            return;
        }
        
        if ([changes insertions].count > 0) {
            
            [weakSelf.tableView.mj_footer endRefreshing];
        }
        
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:[changes insertionsInSection:0] withRowAnimation:UITableViewRowAnimationNone];
        [tableView deleteRowsAtIndexPaths:[changes deletionsInSection:0] withRowAnimation:(UITableViewRowAnimationMiddle)];
        [tableView reloadRowsAtIndexPaths:[changes modificationsInSection:0] withRowAnimation:(UITableViewRowAnimationNone)];
        [tableView endUpdates];
    }];
}

-(void)handlePanGestureRecognizerMethod:(UIPanGestureRecognizer *)pan{
    
    CGPoint point = [pan translationInView:self.view];
    
    switch (pan.state) {
            
        case UIGestureRecognizerStateBegan:
            
            self.customSearchDismissTransitioning.isInteraction = true;
            
            [self dismissViewControllerAnimated:true completion:nil];
            break;
            
        case UIGestureRecognizerStateChanged:
            
            [_interactiveTransitioning updateInteractiveTransition: (CGFloat)point.x/CGRectGetWidth(self.view.frame)];
            break;
        default:
            
            self.customSearchDismissTransitioning.isInteraction = false;
            
            CGFloat locationX = ABS(point.x);
            CGFloat velocityX = [pan velocityInView:self.view].x;
            
            if (velocityX >= 800 || locationX >= CGRectGetWidth(self.view.frame)/2) {
                
                [_interactiveTransitioning finishInteractiveTransition];
            }else{
                
                [_interactiveTransitioning cancelInteractiveTransition];
            }
            break;
    }
}


-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    CGPoint point = [_panGestureRecognizer velocityInView:self.view];
    
    return (fabs(point.x) > fabs(point.y)) && point.x > 0 && self.tableView.indexPathsForSelectedRows.count <= 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.noFindAnyThingForThisWord && indexPath.row == 0) {
        
        return;
    }
    
    OddityNew *new;
    
    if (self.searchObject.pressListArray.count < indexPath.row ) {
        
        return;
    }
    
    new = [self.searchObject.pressListArray objectAtIndex:indexPath.row];
    
    OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc]initWithNid:new.nid isVideo:(new.cellStyle == PrerryCellStyleVideo)] ;
    
    [newDACViewController makeLogType:new.logtype AndLogChid:new.logchid cid:new.channel];
    
    [[UIViewController oddityCurrentViewController] presentViewController:newDACViewController animated:true completion:^{
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
    }];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.noFindAnyThingForThisWord ? 1 : self.searchObject.pressListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.noFindAnyThingForThisWord && indexPath.row == 0) {
        
        OddityPromptInfoTableViewCell *cell = (OddityPromptInfoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"prompt"];
        
        [cell configAttriString:[OddityPromptInfoTableViewCell createAttributed: self.noFindAnyErrorString ] image:[UIImage oddityImage:@"wusousuojieguo"]];
        
        return cell;
    }
    
    OddityNew *new;
    
    if (self.searchObject.pressListArray.count > indexPath.row ) {
        
        new = [self.searchObject.pressListArray objectAtIndex:indexPath.row];
    }
    
    UITableViewCell *cell = [self tableViewCell:tableView froNew: new];
    
    if ([cell isKindOfClass:[OddityNewBaseTableViewCell class]]) {
        
        [((OddityNewBaseTableViewCell *)cell) hiddenIconView];
        [((OddityNewBaseTableViewCell *)cell) hiddenCloseButton];
        [((OddityNewBaseTableViewCell *)cell) bottomMakeHeight];
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] > 9.2) {
    
        [self configCellForHtml:cell :new];
    }
    
    return cell;
}


/**
 配置表格的 UITablViewCell 的 html 格式Label

 @param cell UITablViewCell
 @param new 新闻对象
 */
-(void)configCellForHtml:(UITableViewCell *)cell :(OddityNew *)new{

    if ([cell isKindOfClass:[OddityNewBaseTableViewCell class]]) {
        
        OddityNewBaseTableViewCell *ncell = ((OddityNewBaseTableViewCell*)cell);
        
        if (new.isread == 0) {
            
            NSString *cacheKey = new.searchFormatTitle;
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                NSAttributedString *attrString = [[OddityHtmlTitleCache sharedCache] htmlTitleByString: cacheKey];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ncell.titleLabel.attributedText = attrString;
                });
            });
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.noFindAnyThingForThisWord && indexPath.row == 0) {
    
        return CGRectGetHeight(tableView.frame);
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        return UITableViewAutomaticDimension;
    }else{
        
        return [self tableViewHeightCalendar:tableView forIndexPath:indexPath];
    }
}

-(CGFloat)tableViewHeightCalendar:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath{
    
    OddityNew *new;
    
    if (self.searchObject.pressListArray.count > indexPath.row ) {
        
        new = [self.searchObject.pressListArray objectAtIndex:indexPath.row];
    }
        
    OddityPrerryCellStyle style = [new cellStyle];
    
    if (style == PrerryCellStyleOne || style == PrerryCellStyleOneAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"onecell" configuration:^(id cell) {
            
            [(OddityNewOneTableViewCell *)cell setConfig:new];
        }];
    }
    
    if(style == PrerryCellStyleTwo){
        
        return [tableView fd_heightForCellWithIdentifier:@"twocell" configuration:^(id cell) {
            
            [(OddityNewTwoTableViewCell *)cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleThree || style == PrerryCellStyleThreeAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"threecell" configuration:^(id cell) {
            
            [(OddityNewThreeTableViewCell *)cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleBig || style == PrerryCellStyleBigAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"bigcell" configuration:^(id cell) {
            
            [(OddityCoverImageTableViewCell *)cell setConfig:new];
        }];
    }
    
    return [tableView fd_heightForCellWithIdentifier:@"basecell" configuration:^(id cell) {
        
        [(OddityNewBaseTableViewCell *)cell setConfig:new];
    }];
}


-(UITableViewCell *)tableViewCell:(UITableView *)tableView froNew:(OddityNew *)new {
    
    if ( !new ) {
        
        OddityNewReserveTableViewCell *cell = (OddityNewReserveTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"nocontent"];
        
        return cell;
    }
    
    OddityPrerryCellStyle style = [new cellStyle];
    
    if (style == PrerryCellStyleOne || style == PrerryCellStyleOneAds) {
        
        OddityNewOneTableViewCell *cell = (OddityNewOneTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"onecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if(style == PrerryCellStyleTwo){
        
        OddityNewTwoTableViewCell *cell = (OddityNewTwoTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"twocell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleThree || style == PrerryCellStyleThreeAds) {
        
        OddityNewThreeTableViewCell *cell = (OddityNewThreeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"threecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleBig || style == PrerryCellStyleBigAds) {
        
        OddityCoverImageTableViewCell *cell = (OddityCoverImageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"bigcell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    OddityNewBaseTableViewCell *cell = (OddityNewBaseTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"basecell"];
    
    [cell setConfig:new];
    
    return cell;
}

@end
