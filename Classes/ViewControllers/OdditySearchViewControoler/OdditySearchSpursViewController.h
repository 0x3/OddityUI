//
//  OdditySearchSpursViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import <UIKit/UIKit.h>
#import "OddityCustomView.h"

#define OdditySearchTypeHot @"hot"
#define OdditySearchTypeQuery @"query"
#define OdditySearchTypeHistory @"history"

@interface OdditySearchSpursViewController : UIViewController


@property(nonatomic,strong) UITableView *tableView;

@property(nonatomic,strong) OddityNavigationBarView *navVarView;

@property(nonatomic,strong) OdditySearchHeaderView *searchHeaderView;

@property(nonatomic,strong) OddityNewArrayListViewController *nArrayListViewController;

-(instancetype)initWithFromViewController:(OddityNewArrayListViewController *)newArrayListViewController;

@end
