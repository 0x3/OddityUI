//
//  OdditySearchSpursViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/4/26.
//
//

#import "OddityModels.h"
#import "OddityCategorys.h"

#import "OddityUISetting+Theme.h"

#import "OddityDescInfoHeaderView.h"

#import "OdditySearchResultViewController.h"

#import "OddityCustomSearchTransitioning.h"

#import "OddityLogSearchTableViewCell.h"
#import "OdditySearchHotCollectionViewCell.h"
#import "OddityHotSearchContainerTableViewCell.h"

#import "OddityUICollectionViewLeftAlignedLayout.h"

#import "OddityCustomTransitioning.h"
#import "OdditySearchSpursViewController.h"

@interface OdditySearchSpursViewController ()<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIViewControllerTransitioningDelegate,UIGestureRecognizerDelegate>

@property(nonatomic,strong) UIPanGestureRecognizer *panGestureRecognizer;

@property(nonatomic,strong) OddityBigButton *clearButton;

@property(nonatomic,strong) UICollectionView *collectionView;

@property(nonatomic,strong) UICollectionViewFlowLayout *collectionViewFlowLayout;

@property(nonatomic,strong) OddityCustomSearchSpursDismissTransitioning *customSearchDismissTransitioning;
@property(nonatomic,strong) OddityCustomSearchSpursPresentedTransitioning *customSearchPresentedTransitioning;

@property(nonatomic,strong) OddityCustomViewControllerDrivenInteractiveTransition *interactiveTransitioning;

@property(nonatomic,strong) RLMResults<OdditySearch *> *logSearch;
@property(nonatomic,strong) RLMResults<OdditySearch *> *hotSearch;

@property(nonatomic,strong) RLMNotificationToken *logNotificationToken;
@property(nonatomic,strong) RLMNotificationToken *hotNotificationToken;
@end

@implementation OdditySearchSpursViewController


-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [OddityUISetting searchStatusBarStyle];
}

-(instancetype)initWithFromViewController:(OddityNewArrayListViewController *)newArrayListViewController{

    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        _nArrayListViewController = newArrayListViewController;
        
        _interactiveTransitioning = [[OddityCustomViewControllerDrivenInteractiveTransition alloc] init];
        
        _customSearchDismissTransitioning = [[OddityCustomSearchSpursDismissTransitioning alloc ]init];
        _customSearchPresentedTransitioning = [[OddityCustomSearchSpursPresentedTransitioning alloc ]init];
        
        self.transitioningDelegate = self;
        self.modalPresentationCapturesStatusBarAppearance = true;
    }
    return self;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source{

    return _customSearchPresentedTransitioning;
}

-(id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed{

    return _customSearchDismissTransitioning;
}


-(id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator{
    
    if (_customSearchDismissTransitioning.isInteraction) {
        
        return _interactiveTransitioning;
    }
    return nil;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self createLayout];
    
    [self createModel];
    
    
//    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGestureRecognizerMethod:)];
//    _panGestureRecognizer.delegate = self;
//    [self.view addGestureRecognizer:_panGestureRecognizer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
}


-(BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    CGPoint point = [_panGestureRecognizer velocityInView:self.view];
    
    return (fabs(point.x) > fabs(point.y)) && point.x > 0;
}

-(void)handlePanGestureRecognizerMethod:(UIPanGestureRecognizer *)pan{
    
    CGPoint point = [pan translationInView:self.view];
    
    switch (pan.state) {
            
        case UIGestureRecognizerStateBegan:
            
            self.customSearchDismissTransitioning.isInteraction = true;
            
            [self dismissViewControllerAnimated:true completion:nil];
            break;
            
        case UIGestureRecognizerStateChanged:
            
            [_interactiveTransitioning updateInteractiveTransition: (CGFloat)point.x/CGRectGetWidth(self.view.frame)];
            break;
        default:
            
            self.customSearchDismissTransitioning.isInteraction = false;
            
            CGFloat locationX = ABS(point.x);
            CGFloat velocityX = [pan velocityInView:self.view].x;
            
            if (velocityX >= 800 || locationX >= CGRectGetWidth(self.view.frame)/2) {
                
                [_interactiveTransitioning finishInteractiveTransition];
            }else{
                
                [_interactiveTransitioning cancelInteractiveTransition];
            }
            break;
    }
}

-(void)refreshThemeMethod{
    
    [_searchHeaderView makeFeed];
    _navVarView.backgroundColor = _searchHeaderView.backgroundColor;
    
    _collectionView.backgroundColor = [UIColor color_t_6];
    
    [_tableView reloadData];
    [self.collectionView reloadData];
    _tableView.backgroundColor = [UIColor color_t_6];
    self.view.backgroundColor = [UIColor color_t_6];
    [self setNeedsStatusBarAppearanceUpdate];
    
    _navVarView.cancelButton.tintColor = [UIColor color_t_2];
    _navVarView.borderView.backgroundColor = [UIColor color_t_5];
}

-(void)createModel{
    
    _logSearch = [OdditySearch logSearchResults];
    _hotSearch = [OdditySearch hotSearchResults];
    
    [OdditySearch RequestHotWord];
    
    __weak typeof(self) weakSelf = self;
    _logNotificationToken = [_logSearch addNotificationBlock:^(RLMResults<OdditySearch *> * _Nullable results, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        weakSelf.collectionViewFlowLayout = [[OddityUICollectionViewLeftAlignedLayout alloc] init];
        weakSelf.collectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        [weakSelf.collectionView performBatchUpdates:^{
            [weakSelf.collectionView.collectionViewLayout invalidateLayout];
            [weakSelf.collectionView setCollectionViewLayout:weakSelf.collectionViewFlowLayout animated:YES];
        } completion:nil];
        
        [weakSelf.tableView reloadData];
    }];
    
    _hotNotificationToken = [_hotSearch addNotificationBlock:^(RLMResults<OdditySearch *> * _Nullable results, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        [weakSelf.collectionView reloadData];
    }];
}

-(void)createLayout{
    
    self.view.backgroundColor = [UIColor color_t_6];
    
    __weak typeof(self) weakSelf = self;
    _navVarView = [[OddityNavigationBarView alloc]init:@"" titleFont:[UIFont font_t_7] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    
    _navVarView.cancelButton.tintColor = [UIColor color_t_2];
    _navVarView.borderView.backgroundColor = [UIColor color_t_5];
    
    [self.view addSubview:_navVarView];
    
    _searchHeaderView = [[OdditySearchHeaderView alloc] init];
    _searchHeaderView.inputTextFiled.delegate = self;
    [_navVarView addSubview:_searchHeaderView.inputTextFiled];

    [_searchHeaderView makeFeed];
    _navVarView.backgroundColor = _searchHeaderView.backgroundColor;
    
    _clearButton = [[OddityBigButton alloc] init];
    [_clearButton setImage:[UIImage oddityImage:@"清理缓存icon"] forState:(UIControlStateNormal)];
    
    _collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_collectionViewFlowLayout];
    _collectionView.backgroundColor = [UIColor color_t_6];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    _collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [_collectionView registerClass:[OdditySearchHotCollectionViewCell class] forCellWithReuseIdentifier:@"hotSearch"];
    
    
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = self.view.backgroundColor;
    [_tableView registerClass:[OddityDescInfoHeaderView class] forHeaderFooterViewReuseIdentifier:@"header"];
    [_tableView registerClass:[OddityLogSearchTableViewCell class] forCellReuseIdentifier:@"logSearchCell"];
    [_tableView registerClass:[OddityHotSearchContainerTableViewCell class] forCellReuseIdentifier:@"hotSearchCell"];
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
       
        make.top.equalTo(_navVarView.mas_bottom);
        make.left.with.right.with.bottom.mas_equalTo(0);
    }];
    
    [_clearButton addTarget:self action:@selector(clearAllLogObjectsAction) forControlEvents:(UIControlEventTouchUpInside)];
    _tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self.view layoutIfNeeded];
}


-(void)clearAllLogObjectsAction{
    
    [self clearAllLogObjectsMethod];
    
//    __weak typeof(self) weakSelf = self;
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
//        
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否清除搜索历史" message:@"清除后不可复原" preferredStyle:(UIAlertControllerStyleAlert)];
//        
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
//        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好的" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
//            
//            [weakSelf clearAllLogObjectsMethod];
//        }];
//        
//        [alertController addAction:cancelAction];
//        [alertController addAction:okAction];
//        
//        [[UIViewController oddityCurrentViewController] presentViewController:alertController animated:true completion:nil];
//    }else{
//        
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否清除搜索历史" message:@"清除后不可复原" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"好的", nil];
//        
//        [alert oddity_showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
//            
//            if (buttonIndex == 1) {
//                
//                [weakSelf clearAllLogObjectsMethod];
//            }
//        }];
//    }
}

-(void)clearAllLogObjectsMethod{

    [OdditySearch removeAllLogSearchResults];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{

    NSString *textValue = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (textValue.length <= 0) {
    
        NSLog(@"滚");
    }
    
    OdditySearchResultViewController *searchResultViewController = [[OdditySearchResultViewController alloc] init];
    
    [OddityLogObject createAppAction:OddityLogAtypeSearch fromPageName:OddityLogPageSearchPage toPageName:OddityLogPageSearchPage effective:true params:@{@"query":textValue,@"type":OdditySearchTypeQuery}];
    
    [self presentViewController:searchResultViewController animated:true completion:^{
        
        searchResultViewController.searchObject = [OdditySearch createLogSearch:textValue];
        
    }];
    
    return true;
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    if (_logSearch.count <= 0) {
        
        return 1;
    }
    
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section == 0) {
        
        return 1;
    }
    
    return self.logSearch.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        
        OddityHotSearchContainerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"hotSearchCell"];
        
        [cell.contentView addSubview:self.collectionView];
        [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
            
            make.edges.mas_equalTo(cell.contentView).insets(UIEdgeInsetsZero);
        }];
        
        return cell;
    }
    
    OddityLogSearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"logSearchCell"];
    
    [cell setdescribe:[[self.logSearch objectAtIndex:indexPath.row] searchKeyAttributedString]];
    
    return cell;
}



/**
 设置表格的高度
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        
        if (self.logSearch.count <= 0) {
            
            return oddityUIScreenHeight - 64 - 38;
        }
        
        return (oddityUIScreenHeight-180)/2;
    }
    
    return 55;
}


/**
 头部视图
 */
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    OddityDescInfoHeaderView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"header"];
    
    if (section == 0) {
        
        [header setDescStr: @"热门搜索" ];
        
        [self.clearButton removeFromSuperview];
        
    } else {
        
        if (self.logSearch.count > 0) {
            
            [header addSubview:self.clearButton];
            [self.clearButton mas_remakeConstraints:^(MASConstraintMaker *make) {
                
                make.centerY.equalTo(header.descLabel);
                make.right.mas_equalTo(-20);
            }];
        }
        
        [header setDescStr: @"历史搜索"];
    }
    
    return header;
}


/**
 尾部视图
 */
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

    UIView *view = [[UIView alloc] init];
    
    view.backgroundColor = [UIColor color_t_5];
    
    return view;
}

/**
 设置 头部视图的 高度
 */
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 38;
}

/**
 设置 尾部视图的 高度
 */
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if (section == 0) {
        
        return 10;
    }
    return 0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    OdditySearch *search = [self.logSearch objectAtIndex:indexPath.row];
    
    OdditySearchResultViewController *searchResultViewController = [[OdditySearchResultViewController alloc] init];
    
    NSString *textValue = search.title;
    
    self.searchHeaderView.inputTextFiled.text = textValue;
    
    [OddityLogObject createAppAction:OddityLogAtypeSearch fromPageName:OddityLogPageSearchPage toPageName:OddityLogPageSearchPage effective:true params:@{@"query":textValue,@"type":OdditySearchTypeHistory}];
    
    [self presentViewController:searchResultViewController animated:true completion:^{
        
        searchResultViewController.searchObject = [OdditySearch createLogSearch: textValue];
    }];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    OdditySearch *search = [self.hotSearch objectAtIndex:indexPath.row];
    
    OdditySearchResultViewController *searchResultViewController = [[OdditySearchResultViewController alloc] init];
    
    NSString *textValue = search.title;
    
    self.searchHeaderView.inputTextFiled.text = textValue;
    
    [OddityLogObject createAppAction:OddityLogAtypeSearch fromPageName:OddityLogPageSearchPage toPageName:OddityLogPageSearchPage effective:true params:@{@"query":textValue,@"type":OdditySearchTypeHot}];
    
    [self presentViewController:searchResultViewController animated:true completion:^{
        
        searchResultViewController.searchObject = [OdditySearch createLogSearch: textValue];
        
    }];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return self.hotSearch.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    OdditySearchHotCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"hotSearch" forIndexPath:indexPath];
    
    [cell setdescribe:[[self.hotSearch objectAtIndex:indexPath.row] searchKeyAttributedString]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSStringDrawingOptions opts = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    CGSize size = [[self.hotSearch objectAtIndex:indexPath.row].searchKeyAttributedString boundingRectWithSize:CGSizeMake(8000, 35) options:opts context:nil].size;
    
    return CGSizeMake(size.width+24, 35);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{

    return 18;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{

    return 12;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    return UIEdgeInsetsMake(23, 12, 23, 12);
}

@end
