//
//  OdditySubNewArrayViewController.h
//  Pods
//
//  Created by 荆文征 on 2017/6/17.
//
//

#import "OddityMainModel.h"
#import <JXLPagerTabStrip/XLButtonBarPagerTabStripViewController.h>

@interface OdditySubNewArrayViewController : XLButtonBarPagerTabStripViewController<XLPagerTabStripChildItem>

-(instancetype)initWith:(OddityChannelNews *)channel managerViewController:(OddityMainManagerViewController *)mainVc;

@end
