//
//  OdditySubNewArrayViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/17.
//
//

#import "OddityNewArrayListViewController.h"

#import "OddityCategorys.h"
#import "OddityListViewControllerCacher.h"
#import "OdditySubNewArrayViewController.h"

@interface OdditySubNewArrayViewController ()<UICollectionViewDelegate>

@property(nonatomic,strong)UIView *buttonbackView;

@property(nonatomic,strong)OddityChannelNews *channel;
@property(nonatomic,strong)RLMResults<OddityChannel *> *subChannels;

@property(nonatomic,weak) OddityMainManagerViewController *managerViewController;

@property (nonatomic, retain) RLMNotificationToken *notificationToken;
@end

@implementation OdditySubNewArrayViewController

-(instancetype)initWith:(OddityChannelNews *)channel managerViewController:(OddityMainManagerViewController *)mainVc{

    self = [super initWithNibName:nil bundle:nil];
    
    if (self) {
        
        _managerViewController = mainVc;
        
        _channel = channel;
        _subChannels = channel.channel.subChannelResults;
    }
    
    return self;
}

-(void)viewDidLoad{

    [super viewDidLoad];
    
    self.buttonBarView.selectedBarHeight = 0.5;

    _buttonbackView = [[UIView alloc] init];
    _buttonbackView.backgroundColor = UIColor.color_t_9;
    self.buttonBarView.backgroundColor = UIColor.color_t_9;
    [self.view addSubview:_buttonbackView];
    [_buttonbackView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.with.right.left.mas_equalTo(0);
        make.height.mas_equalTo(44);
    }];
    
    [self.buttonbackView addSubview:self.buttonBarView];
    [self.view addSubview:self.containerView];
    
    [self.buttonBarView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.right.left.centerY.mas_equalTo(0);
        make.height.mas_equalTo(22);
    }];
    
    [self.containerView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_buttonbackView.mas_bottom);
        make.left.with.right.with.bottom.mas_equalTo(0);
    }];
    
    self.buttonBarView.backgroundColor = UIColor.color_t_9;
    self.buttonBarView.selectedBar.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
    self.skipIntermediateViewControllers = true;
    
    ((UICollectionViewFlowLayout *)self.buttonBarView.collectionViewLayout).sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
    
//    self.containerView.scrollEnabled = false;
    
    self.changeCurrentIndexBlock = ^(XLButtonBarViewCell *oldCell, XLButtonBarViewCell *newCell, BOOL animated) {
      
        oldCell.label.font = UIFont.font_t_5;
        newCell.label.font = UIFont.font_t_5;
        
        oldCell.backgroundColor = UIColor.color_t_6;
        newCell.backgroundColor = UIColor.color_t_4;
        
        oldCell.layer.cornerRadius = 2;
        newCell.layer.cornerRadius = 2;
        
        oldCell.label.textColor = UIColor.color_t_3;
        newCell.label.textColor = UIColor.color_t_9;
        
        oldCell.layer.borderColor = UIColor.color_t_5.CGColor;
        newCell.layer.borderColor = UIColor.color_t_5.CGColor;
        
        oldCell.layer.borderWidth = 0.5;
        newCell.layer.borderWidth = 0.5;
    };
    
//    self.buttonBarView.alwaysBounceHorizontal = true;
    self.buttonBarView.selectedBarAlignment = XLSelectedBarAlignmentCenter;
//    self.buttonBarView.shouldCellsFillAvailableWidth = false;
    
    
    [self.managerViewController.containerView.panGestureRecognizer requireGestureRecognizerToFail:self.buttonBarView.panGestureRecognizer];
    
    [self.buttonBarView reloadData];

    __weak typeof(self) weakSelf = self;
    self.notificationToken = [_subChannels addNotificationBlock:^(RLMResults<OddityChannel *> * _Nullable results, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if ( !error ) {
            
            if (error || !changes) { return; }
            
            if ( changes.deletions.count || changes.insertions.count || changes.modifications.count ) {
                
                [weakSelf reloadPagerTabStripView];
            }
        }
    }];
    
    [self.channel.channel requestSubChannel];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil]; // 主题发生变化
    
    self.containerView.delegate = self;
    self.buttonBarView.delegate = self;
    
    self.containerView.scrollEnabled = OdditySetting.shareOdditySetting.isCanScorllChangeSubChannel;
}

-(void)reloadCellForFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{
    
    UICollectionViewCell *toCell = [self.buttonBarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:toIndex inSection:0]];
    UICollectionViewCell *fromCell = [self.buttonBarView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:fromIndex inSection:0]];
    
    if ([toCell isKindOfClass:[XLButtonBarViewCell class]]) {
        
        XLButtonBarViewCell *cell = ((XLButtonBarViewCell *)toCell);
        
        cell.label.textColor = UIColor.color_t_9;
        cell.backgroundColor = UIColor.color_t_4;
    }
    
    if ([fromCell isKindOfClass:[XLButtonBarViewCell class]]) {
        
        XLButtonBarViewCell *cell = ((XLButtonBarViewCell *)fromCell);
        
        cell.label.textColor = UIColor.color_t_3;
        cell.backgroundColor = UIColor.color_t_6;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self moveToViewControllerAtIndex:indexPath.row];
    
    [self reloadCellForFromIndex:self.currentIndex toIndex:indexPath.row];
    
    OddityNewArrayListViewController *viewController = [self childViewControllersForPagerTabStripViewController:self][indexPath.row];
    
    self.managerViewController.currentIndexString = [NSString stringWithFormat:@"%d",viewController.channel.channel.id];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if (scrollView == self.containerView) {
        
        OddityNewArrayListViewController *viewController = [self childViewControllersForPagerTabStripViewController:self][self.currentIndex];
        
        self.managerViewController.currentIndexString = [NSString stringWithFormat:@"%d",viewController.channel.channel.id];
    }
}

-(void)refreshThemeMethod{
    
    _buttonbackView.backgroundColor = UIColor.color_t_9;
    self.buttonBarView.backgroundColor = UIColor.color_t_9;
    
    [self.buttonBarView reloadData];
}

-(void)pagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController updateIndicatorFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex{

    [super pagerTabStripViewController:pagerTabStripViewController updateIndicatorFromIndex:fromIndex toIndex:toIndex];
    
    if (![OdditySetting shareOdditySetting].isWatchVideoAllTheTime) {
        
        [[OddityPlayerView sharePlayerView] closePlayerView];
        
        return;
    }
    
    [self.managerViewController refreshPlayViewState];
}


-(NSArray *)childViewControllersForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    
    NSMutableArray *array = [NSMutableArray array];
    
    if (_channel) {
        
        id controller = [OddityListViewControllerCacher.sharedCache getSubViewControllerBy:self.channel.channel subChannel:nil viewController:self.managerViewController];
        
        [array addObject:controller];
    }
    
    for (OddityChannel *channel in _subChannels) {
        
        id controller = [OddityListViewControllerCacher.sharedCache getSubViewControllerBy:self.channel.channel subChannel:channel viewController:self.managerViewController];
        
        [array addObject:controller];
    }
    
    return array;
}

- (NSString *)titleForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController {
    
    return self.channel.channel.cname ?: @"奇点";
}

- (UIColor *)colorForPagerTabStripViewController:(XLPagerTabStripViewController *)pagerTabStripViewController{
    
    return [OddityUISetting shareOddityUISetting].titleColor;
}

@end
