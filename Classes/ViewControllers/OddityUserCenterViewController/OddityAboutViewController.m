//
//  OddityAboutViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/13.
//
//

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>
#import "OddityAboutViewController.h"

@interface OddityAboutViewController ()

@property(nonatomic,strong)UIView *contentView;
@property(nonatomic,strong)UIScrollView *contentScrollerView;


@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;

@property(nonatomic,strong)UILabel *appNameLabel;
@property(nonatomic,strong)UILabel *versionLabel;
@property(nonatomic,strong)UIImageView *iconImageView;

@property(nonatomic,strong)UILabel *copRightLabel;
@property(nonatomic,strong)UIImageView *copRightImageView;

@end

@implementation OddityAboutViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout]; // 布局方法
    
    self.scrollView = _contentScrollerView;
}

-(void)layout{
    
    __weak __typeof__(self) weakSelf = self;
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"关于" titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _contentScrollerView = [[UIScrollView alloc] init];
    _contentScrollerView.alwaysBounceVertical = true;
    _contentScrollerView.backgroundColor = UIColor.color_t_6;
    [self.view addSubview:_contentScrollerView];
    [_contentScrollerView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_naviGationBar.mas_bottom);
        make.left.with.right.with.bottom.mas_equalTo(0);
    }];
    
    _contentView = [[UIView alloc] init];
    _contentView.backgroundColor = UIColor.color_t_6;
    [self.contentScrollerView addSubview:_contentView];
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.with.left.with.right.with.bottom.mas_equalTo(0);
        make.size.mas_equalTo(self.contentScrollerView);
    }];
    
    
    _iconImageView = [[UIImageView alloc] initWithImage:UIImage.oddity_about_icon_image];
    [self.contentView addSubview:_iconImageView];
    [_iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(80);
        make.centerX.mas_equalTo(self.contentView);
    }];
    
    _appNameLabel = [[UILabel alloc] init];
    _appNameLabel.font = UIFont.font_t_2;
    _appNameLabel.text = @"奇点资讯";
    _appNameLabel.textColor = UIColor.color_t_2;
    [self.contentView addSubview:_appNameLabel];
    [_appNameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(_iconImageView.mas_bottom).offset(12);
    }];
    
    _versionLabel = [[UILabel alloc] init];
    _versionLabel.font = UIFont.font_t_3;
    _versionLabel.text = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    _versionLabel.textColor = UIColor.color_t_3;
    [self.contentView addSubview:_versionLabel];
    [_versionLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.contentView);
        make.top.mas_equalTo(_appNameLabel.mas_bottom).offset(12);
    }];
    
    
    _copRightLabel = [[UILabel alloc] init];
    _copRightLabel.font = UIFont.font_t_7;
    _copRightLabel.text = @"Copyright©2017lieying.All Rights Reserved";
    _copRightLabel.textColor = UIColor.color_t_3;
    [self.contentView addSubview:_copRightLabel];
    [_copRightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(self.contentView);
        make.bottom.mas_equalTo(-30);
    }];
    
    _copRightImageView = [[UIImageView alloc] initWithImage:UIImage.oddity_about_lieying_image];
    [self.contentView addSubview:_copRightImageView];
    [_copRightImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(_copRightLabel.mas_top).offset(-12);
        make.centerX.mas_equalTo(self.contentView);
    }];
}

@end
