//
//  OddityMyCollectViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/22.
//
//
#import "OddityShareUser.h"

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import "OddityShareViewController.h"

#import <Masonry/Masonry.h>
#import "OddityMyCollectViewController.h"

#import "OddityNewsDetailViewController.h"
#import "UITableView+FDTemplateLayoutCell.h"

#import "OddityAlertViewViewController.h"

@interface OddityMyCollectViewController ()<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)RLMNotificationToken *token;
@property(nonatomic,strong)RLMResults<OddityNew *> *results;

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)OddityBigButton *editButton;
@property(nonatomic,strong)OddityBigButton *deleteButton;
@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;

@end

@implementation OddityMyCollectViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout];
    
    [self Data];
    
    [self makeTableViewHeaderButton];
}

-(void)layout{
    
    __weak __typeof__(self) weakSelf = self;
    
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"我的收藏" titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _editButton = [[OddityBigButton alloc] init];
    [_editButton setTitle:@"编辑" forState:(UIControlStateNormal)];
    [_editButton addTarget:self action:@selector(goSetting) forControlEvents:(UIControlEventTouchUpInside)];
    _editButton.titleLabel.font = UIFont.font_t_5;
    [_naviGationBar addSubview:_editButton];
    [_editButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_naviGationBar);
        make.right.mas_equalTo(-16);
    }];
    
    
    _tableView = [[UITableView alloc] initWithFrame:(CGRectZero) style:(UITableViewStylePlain)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _tableView.allowsMultipleSelectionDuringEditing = true;
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_tableView registerClass:[OddityNewVideoTableViewCell class] forCellReuseIdentifier:@"video"];
    [_tableView registerClass:[OddityNewReserveTableViewCell class] forCellReuseIdentifier:@"nocontent"];
    [_tableView registerClass:[OddityNewTwoTableViewCell class] forCellReuseIdentifier:@"twocell"];
    [_tableView registerClass:[OddityNewThreeTableViewCell class] forCellReuseIdentifier:@"threecell"];
    [_tableView registerClass:[OddityNewBaseTableViewCell class] forCellReuseIdentifier:@"basecell"];
    
    [_tableView registerClass:[OddityNewOneTableViewCell class] forCellReuseIdentifier:@"onecell"];
    
    [_tableView registerClass:[OddityCoverImageTableViewCell class] forCellReuseIdentifier:@"bigcell"];

    _tableView.tintColor = OddityUISetting.shareOddityUISetting.mainTone;
    
    self.scrollView = _tableView;
    [self.view addSubview:_tableView];
    [_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.naviGationBar.mas_bottom);
        make.left.with.bottom.with.right.mas_equalTo(0);
    }];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    _deleteButton = [OddityBigButton buttonWithType:(UIButtonTypeSystem)];
    [_deleteButton setTitle:@"删除" forState:(UIControlStateNormal)];
    [self.view addSubview:_deleteButton];
    [_deleteButton addTarget:self action:@selector(deleteMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [_deleteButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.bottom.mas_equalTo(90);
        make.height.mas_equalTo(40);
        make.left.right.mas_equalTo(0);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    
    [self refreshThemeMethod];
}

-(void)refreshThemeMethod{
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    self.tableView.backgroundColor = UIColor.color_t_6;
    
    [_deleteButton setImage:UIImage.oddity_usercenter_comment_delete_image forState:(UIControlStateNormal)];
    
    _deleteButton.backgroundColor = [OddityUISetting shareOddityUISetting].mainTone;
    [_deleteButton setTitleColor:UIColor.whiteColor forState:(UIControlStateNormal)];
    
    [_editButton setTitleColor:[UIColor color_t_2] forState:(UIControlStateNormal)];
    
    _deleteButton.tintColor = UIColor.whiteColor;
    _deleteButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 8);
    
    [self.tableView reloadData];
}

-(void)goSetting{
    
    [self.tableView setEditing:!self.tableView.editing animated:true];
    [self makeTableViewHeaderButton];
    
    if (self.tableView.editing) {
        
        [_editButton setTitle:@"取消" forState:(UIControlStateNormal)];
    }else{
        [_editButton setTitle:@"编辑" forState:(UIControlStateNormal)];
    }
}


-(void)Data{
    
    self.results = [OddityShareUser.defaultUser.collects sortedResultsUsingKeyPath:@"ptimes" ascending:false];
    
    __weak typeof(self) weakSelf = self;
    
    self.token = [self.results addNotificationBlock:^(RLMResults<OddityNewContentComment *> * _Nullable results, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        UITableView *tableView = weakSelf.tableView;
        
        if (!changes) {
            [tableView reloadData];
            return;
        }
        
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:[changes insertionsInSection:0] withRowAnimation:UITableViewRowAnimationNone];
        [tableView deleteRowsAtIndexPaths:[changes deletionsInSection:0] withRowAnimation:(UITableViewRowAnimationMiddle)];
        [tableView reloadRowsAtIndexPaths:[changes modificationsInSection:0] withRowAnimation:(UITableViewRowAnimationNone)];
        [tableView endUpdates];
    }];
    
    [OddityShareUser.defaultUser gc];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSInteger rowCount = [self.tableView makeEmtpyView:self.results.count viewMode:OddityTableViewEmtpyBackViewModeCollect];
    
    self.editButton.hidden = rowCount <= 0;
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OddityNew *new;
    
    if (self.results.count > indexPath.row) {
        
        new = [self.results objectAtIndex:indexPath.row];
    }
    
    UITableViewCell *cell = [self tableViewCell:tableView froNew:new];
    
    if ([cell isKindOfClass:[OddityNewBaseTableViewCell class]]) {
        
        [((OddityNewBaseTableViewCell *) cell) hiddenCloseButton];
    }
    
    if ([cell isKindOfClass:[OddityNewVideoTableViewCell class]]) {
        
        OddityNewVideoTableViewCell *ncell = ((OddityNewVideoTableViewCell *) cell);
        
        [ncell.playButton oddity_handleControlEvent:(UIControlEventTouchUpInside) withBlock:^{
            
            [(OddityNewVideoTableViewCell *) cell playVideoGogo: nil];
        }];
    }
    
    return cell;
}





-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (self.tableView.editing) {
        
        [self makeTableViewHeaderButton];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.tableView.editing) {
        
        [self makeTableViewHeaderButton];
        
        return;
    }
    
    if (self.results.count > indexPath.row) {
        
        OddityNew *new = [self.results objectAtIndex:indexPath.row];
        
        OddityPrerryCellStyle style = new.cellStyle;
        
        
        if (style == PrerryCellStyleVideo) {
            
            OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc] initWithNid:new.nid isVideo:true];
            
            [newDACViewController configVideoUrl:new.videourl Object:new];
            [newDACViewController makeLogType:new.logtype AndLogChid:new.logchid cid:new.channel];
            
            /// 如果 当前的播放器没有显示 活着 当前播放器的 播放对象 不是现在的 点击的 新闻对象，那么都需要重新播放
            if (![[OddityPlayerView sharePlayerView].videoObject.object isEqual:new] || ![OddityPlayerView sharePlayerView].isPlayerViewShow) {

                OddityNewVideoTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                
                /// 打点 -> 点击新闻打点
                [OddityLogObject createNewsClick:new.nid chid: new.channel logtype:new.logtype logchid:new.logchid source:OdditySourceFeedName];
                [cell playVideoGogo:nil];
            }
            
            [self presentViewController:newDACViewController animated:true completion:^{
                
                [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
            }];
            
            return;
        }
        
        OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc] initWithNid:new.nid isVideo:(new.cellStyle == PrerryCellStyleVideo)];
        
        [newDACViewController makeLogType:new.logtype AndLogChid:new.logchid cid: new.channel];
        
        [[UIViewController oddityCurrentViewController] presentViewController:newDACViewController animated:true completion:^{
            
            [tableView deselectRowAtIndexPath:indexPath animated:YES];// 取消选中
        }];
    }
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{

    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

-(void)makeTableViewHeaderButton{

    BOOL show = self.tableView.indexPathsForSelectedRows && self.tableView.indexPathsForSelectedRows.count > 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        
        if (show) {
            
            [_deleteButton mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.bottom.mas_equalTo(0);
            }];
            
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
        }else{
            
            [_deleteButton mas_updateConstraints:^(MASConstraintMaker *make) {
                
                make.bottom.mas_equalTo(90);
            }];
            
            self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        
        
        [self.view layoutIfNeeded];
    }];
    
    [self.deleteButton setTitle:[NSString stringWithFormat:@"删除%lu个新闻",self.tableView.indexPathsForSelectedRows.count] forState:(UIControlStateNormal)];
}

-(void)deleteMethod{

    __weak typeof(self) weakSelf = self;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否删除" message:@"删除新闻不可恢复" preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf delhandleMethod];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [[UIViewController oddityCurrentViewController] presentViewController:alertController animated:true completion:nil];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否删除" message:@"删除新闻不可恢复" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"删除", nil];
        
        [alert oddity_showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex == 1) {
                
                [weakSelf delhandleMethod];
            }
        }];
    }
}

-(void)delhandleMethod{
    
    OddityAlertViewViewController *alertController = [[OddityAlertViewViewController alloc] initWitch:@"取消收藏中"];
    
    __weak __typeof__(self) weakSelf = self;
    
    [self presentViewController:alertController animated:true completion:^{
       
        NSMutableArray *array = [NSMutableArray array];
        
        for (NSIndexPath *indexPath in weakSelf.tableView.indexPathsForSelectedRows) {
            
            [array addObject:@(weakSelf.results[indexPath.row].nid)];
        }
        
        [OddityNewContent down:array complete:^(BOOL success, NSString * _Nullable mess) {
            
            [alertController makeChange:success ? @"取消收藏完成" : @"取消收藏失败" after:1 block:^{
               
                [weakSelf dismissViewControllerAnimated:true completion:nil];
                
                [weakSelf.tableView setEditing: false animated:true];
                
                [weakSelf.editButton setTitle:@"编辑" forState:(UIControlStateNormal)];
                
                [weakSelf makeTableViewHeaderButton];
            }];
        }];
    }];
}

- (UITableViewCell *)tableViewCell:(UITableView *)tableView froNew:(OddityNew *)new {
    
    OddityPrerryCellStyle style = [new cellStyle];
    
    if (style == PrerryCellStyleOne) {
        
        OddityNewOneTableViewCell *cell = (OddityNewOneTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"onecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleTwo) {
        
        OddityNewTwoTableViewCell *cell = (OddityNewTwoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"twocell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleThree) {
        
        OddityNewThreeTableViewCell *cell = (OddityNewThreeTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"threecell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleBig) {
        
        OddityCoverImageTableViewCell *cell = (OddityCoverImageTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"bigcell"];
        
        [cell setConfig:new];
        
        return cell;
    }
    
    if (style == PrerryCellStyleVideo) {
        
        OddityNewVideoTableViewCell *cell = (OddityNewVideoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"video"];
        
        [cell setConfig:new];
        
        [cell makeFullMode];
        
        cell.moreButton.hidden = true;
        
        return cell;
    }
    
    OddityNewBaseTableViewCell *cell = (OddityNewBaseTableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"basecell"];
    
    [cell setConfig:new];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        return UITableViewAutomaticDimension;
    } else {
        
        return [self tableViewHeightCalendar:tableView forIndexPath:indexPath];
    }
}

- (CGFloat)tableViewHeightCalendar:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath {
    
    OddityNew *new;
    
    if (self.results.count > indexPath.row) {
        
        new = [self.results objectAtIndex:indexPath.row];
    }
    
    OddityPrerryCellStyle style = [new cellStyle];
    
    if (style == PrerryCellStyleOne) {
        
        return [tableView fd_heightForCellWithIdentifier:@"onecell" configuration:^(id cell) {
            
            [(OddityNewOneTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleTwo) {
        
        return [tableView fd_heightForCellWithIdentifier:@"twocell" configuration:^(id cell) {
            
            [(OddityNewTwoTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleThree || style == PrerryCellStyleThreeAds) {
        
        return [tableView fd_heightForCellWithIdentifier:@"threecell" configuration:^(id cell) {
            
            [(OddityNewThreeTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleBig) {
        
        return [tableView fd_heightForCellWithIdentifier:@"bigcell" configuration:^(id cell) {
            
            [(OddityCoverImageTableViewCell *) cell setConfig:new];
        }];
    }
    
    if (style == PrerryCellStyleVideo) {
        
        return [tableView fd_heightForCellWithIdentifier:@"video" configuration:^(id cell) {
            
            [(OddityNewVideoTableViewCell *) cell setConfig:new];
        }];
    }
    
    
    return [tableView fd_heightForCellWithIdentifier:@"basecell" configuration:^(id cell) {
        
        [(OddityNewBaseTableViewCell *) cell setConfig:new];
    }];
}

@end
