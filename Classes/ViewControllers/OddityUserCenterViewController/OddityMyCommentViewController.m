//
//  OddityMyCommentViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/20.
//
//

#import "OddityNewsDetailViewController.h"

#import "OddityShareUser.h"

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterCommentTableViewCell.h"
#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>
#import "OddityMyCommentViewController.h"

#import "OddityAlertViewViewController.h"

#import "UITableView+FDTemplateLayoutCell.h"

@interface OddityMyCommentViewController ()<UITableViewDelegate,UITableViewDataSource,OddityUserCenterCommentTableViewCellDelegate>

@property(nonatomic,strong)UITableView *tableView;
@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;

@property(nonatomic,strong)RLMNotificationToken *token;

@property(nonatomic,strong)RLMResults<OddityNewContentComment *> *results;
@end

@implementation OddityMyCommentViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout];
    
    [self Data];
}

-(void)layout{
    
    __weak __typeof__(self) weakSelf = self;
    
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"我的评论" titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _tableView = [[UITableView alloc] initWithFrame:(CGRectZero) style:(UITableViewStylePlain)];
    [_tableView registerClass:[OddityUserCenterCommentTableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollView = _tableView;
    [self.view addSubview:_tableView];
    [_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.naviGationBar.mas_bottom);
        make.left.with.bottom.with.right.mas_equalTo(0);
    }];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        _tableView.estimatedRowHeight = 100;
        _tableView.rowHeight = UITableViewAutomaticDimension;
    }
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    self.tableView.backgroundColor = UIColor.color_t_6;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
}

-(void)refreshThemeMethod{
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    self.tableView.backgroundColor = UIColor.color_t_6;
    
    [self.tableView reloadData];
}

-(void)Data{

    self.results = [OddityShareUser.defaultUser.comments sortedResultsUsingKeyPath:@"ctimes" ascending:false];
    
    __weak typeof(self) weakSelf = self;
    
    self.token = [self.results addNotificationBlock:^(RLMResults<OddityNewContentComment *> * _Nullable results, RLMCollectionChange * _Nullable changes, NSError * _Nullable error) {
        
        if (error) {
            NSLog(@"Failed to open Realm on background worker: %@", error);
            return;
        }
        
        UITableView *tableView = weakSelf.tableView;
        
        if (!changes) {
            [tableView reloadData];
            return;
        }
        
        [tableView beginUpdates];
        [tableView insertRowsAtIndexPaths:[changes insertionsInSection:0] withRowAnimation:UITableViewRowAnimationNone];
        [tableView deleteRowsAtIndexPaths:[changes deletionsInSection:0] withRowAnimation:(UITableViewRowAnimationMiddle)];
        [tableView reloadRowsAtIndexPaths:[changes modificationsInSection:0] withRowAnimation:(UITableViewRowAnimationNone)];
        [tableView endUpdates];
    }];
    
    [OddityShareUser.defaultUser gm];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return [self.tableView makeEmtpyView:self.results.count viewMode:OddityTableViewEmtpyBackViewModeComment];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    OddityUserCenterCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    [cell fill:self.results[indexPath.row]];
    
    cell.delegate = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0) {
        
        return UITableViewAutomaticDimension;
    } else {
        
        return [self tableViewHeightCalendar:tableView forIndexPath:indexPath];
    }
}

- (CGFloat)tableViewHeightCalendar:(UITableView *)tableView forIndexPath:(NSIndexPath *)indexPath {
    
    return [tableView fd_heightForCellWithIdentifier:@"cell" configuration:^(id cell) {
        // configurations
        
        [cell fill:self.results[indexPath.row]];
    }];
}


-(void)clickNtitle:(OddityNewContentComment *)comment{

    OddityNewsDetailViewController *newDACViewController = [[OddityNewsDetailViewController alloc] initWithNid:comment.nid isVideo:comment.rtype == 6];
    [newDACViewController makeLogType:comment.logtype AndLogChid:comment.logchid cid: 0];
    newDACViewController.justGoBack = true;
    newDACViewController.needRePlay = true;
    [[UIViewController oddityCurrentViewController] presentViewController:newDACViewController animated:true completion:nil];
}

-(void)clickDelete:(OddityNewContentComment *)comment{

    __weak typeof(self) weakSelf = self;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否删除" message:@"删除评论不可恢复" preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"删除" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf delComment:comment];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [[UIViewController oddityCurrentViewController] presentViewController:alertController animated:true completion:nil];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否删除" message:@"删除评论不可恢复" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"删除", nil];
        
        [alert oddity_showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex == 1) {
                
                [weakSelf delComment:comment];
            }
        }];
    }
    
    
    
    
}


-(void)delComment:(OddityNewContentComment *)comment{

    OddityAlertViewViewController *alertController = [[OddityAlertViewViewController alloc] initWitch:@"删除中…"];
    
    __weak typeof(self) weakSelf = self;
    [self presentViewController:alertController animated:true completion:^{
        
        [comment del:@[] complete:^(BOOL success, NSString *mess) {
            
            [alertController makeChange:mess after:1 block:^{
                
                [weakSelf dismissViewControllerAnimated:true completion:nil];
            }];
        }];
    }];
}

-(void)clickCommentButton:(OddityNewContentComment *)comment animate:(OddityAnimationNumberView *)view button:(OddityBigButton *)button {

    view.alpha = 1;
    view.hidden = false;
    
    view.currentNumber = comment.willShowNumberString;
    
    [UIView animateWithDuration:0.3 animations:^{
        
        button.transform = CGAffineTransformScale(button.transform, 0.2, 0.2);
    }];
    
    [comment vote:@[self.token] complete:^(OddityNewContentComment * _Nullable comm) {
        
        view.currentNumber = comm.commentAttributedString.string;
        
        [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.35 initialSpringVelocity:0 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
            
            view.alpha = comm.commend <= 0 ? 0 : 1;
            view.hidden = comm.commend <= 0;
            
            [button setImage:comm.commentButtonImage forState:(UIControlStateNormal)];
            button.tintColor = comm.commentButtonTintColor;
            
            button.transform = CGAffineTransformIdentity;
            [button layoutIfNeeded];
            
        } completion:^(BOOL finished) {
            
        }];
    }];
}

@end
