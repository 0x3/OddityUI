//
//  OddityPrivacyViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/13.
//
//

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>
#import "OddityPrivacyViewController.h"

@interface OddityPrivacyViewController ()

@property(nonatomic,strong)UIWebView *webView;

@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;
@end

@implementation OddityPrivacyViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout]; // 布局方法
}

-(void)layout{
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    __weak __typeof__(self) weakSelf = self;
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"隐私政策" titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    
    _webView = [[UIWebView alloc] init];
    _webView.backgroundColor = UIColor.color_t_6;
    _webView.scrollView.backgroundColor = UIColor.color_t_6;
    [self.view addSubview:_webView];
    [_webView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_naviGationBar.mas_bottom);
        make.left.with.right.with.bottom.mas_equalTo(0);
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
        NSString *privacyHtmlString;
        
        NSString *privacyFilePath =  [[NSBundle oddity_shareBundle] pathForResource:@"privacy" ofType:@"html"];
        if (privacyFilePath) {
            privacyHtmlString = [NSString stringWithContentsOfFile:privacyFilePath encoding:(NSUTF8StringEncoding) error:nil];
            
            NSString *themeString = [NSString stringWithFormat:@"$(document).ready(function(){ $(\".title,.titles,.word,.words\").css({\"color\":\"%@\"}); $(document.body).css(\"background-color\",\"%@\") });",UIColor.color_t_2.oddity_color2String,UIColor.color_t_6.oddity_color2String];
//
            privacyHtmlString = [NSString stringWithFormat:privacyHtmlString,themeString];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (privacyHtmlString) {
                
                [weakSelf.webView loadHTMLString:privacyHtmlString baseURL:NSBundle.oddity_shareBundle.bundleURL];
            }
        });
    });
}

@end
