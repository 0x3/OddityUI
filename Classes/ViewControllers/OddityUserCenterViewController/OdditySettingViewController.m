//
//  OdditySettingViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//

#import "PINDiskCache.h"

#import "OddityShareUser.h"

#import "OddityAlertViewViewController.h"

#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>

#import "OddityUserCenterTableViewCell.h"

#import "OdditySettingViewController.h"


#import "OddityChooseFontView.h"

#import "OddityPrivacyViewController.h"

#import "OddityAboutViewController.h"

#import "OddityClearCacheTool.h"

@interface OdditySettingViewController ()<UITableViewDelegate,UITableViewDataSource>

/**
 上方 Bar 的相关
 */
@property(nonatomic,strong)OddityNavigationBarView *naviGationBar;

@property(nonatomic,strong)UITableView *tableView;

@property(nonatomic,strong)NSArray *dataSources;

@property(nonatomic,strong)UIButton *logOutButton;


@property(nonatomic,strong)OddityUserCenterObject *fontSet;
@property(nonatomic,strong)OddityUserCenterObject *pushSet;
@property(nonatomic,strong)OddityUserCenterObject *moonSet;

@property(nonatomic,strong)OddityUserCenterObject *deleteSet;

@property(nonatomic,strong)OddityUserCenterObject *aboutSet;
@property(nonatomic,strong)OddityUserCenterObject *privacySet;
@property(nonatomic,strong)OddityUserCenterObject *gradeSet;




@end

@implementation OdditySettingViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    [self.tableView deselectRowAtIndexPath:self.tableView.indexPathForSelectedRow animated:true];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self layout];
    
    [self makeDataSources];
}

-(void)layout{
    
    __weak __typeof__(self) weakSelf = self;
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    _naviGationBar = [[OddityNavigationBarView alloc] init:@"设置" titleFont:[UIFont font_t_3] block:^{
        
        [weakSelf dismissViewControllerAnimated:true completion:nil];
    }];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _tableView = [[UITableView alloc] initWithFrame:(CGRectZero) style:(UITableViewStyleGrouped)];
    [_tableView registerClass:[OddityUserCenterTableViewCell class] forCellReuseIdentifier:@"cell"];
    _tableView.sectionHeaderHeight = 0;
    _tableView.sectionFooterHeight = 0;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    [_tableView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.naviGationBar.mas_bottom);
        make.left.with.bottom.with.right.mas_equalTo(0);
    }];
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
    
    
    UIView *footerView = [[UIView alloc] initWithFrame:(CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 100))];
    self.tableView.tableFooterView = footerView;
    self.tableView.backgroundColor = [UIColor color_t_6];
    
    _logOutButton = [UIButton buttonWithType:(UIButtonTypeSystem)];
    [_logOutButton setTitle:@"退出登录" forState:(UIControlStateNormal)];
    _logOutButton.titleLabel.font = [UIFont font_t_4];
    _logOutButton.layer.cornerRadius = 2;
    _logOutButton.clipsToBounds = true;
    _logOutButton.backgroundColor = [OddityUISetting.shareOddityUISetting.mainTone colorWithAlphaComponent:0.7];
    [_logOutButton setTitleColor: [UIColor color_t_9] forState:(UIControlStateNormal)];
    [footerView addSubview:_logOutButton];
    [_logOutButton addTarget:self action:@selector(logOutMethod) forControlEvents:(UIControlEventTouchUpInside)];
    [_logOutButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(50);
    }];
    
    self.scrollView = _tableView;
}

-(void)logOutMethod{

    __weak typeof(self) weakSelf = self;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"是否注销" message:@"是否注销原有用户" preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"注销" style:(UIAlertActionStyleDestructive) handler:^(UIAlertAction * _Nonnull action) {
            
            [weakSelf handleLogOutMethod];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:okAction];
        
        [[UIViewController oddityCurrentViewController] presentViewController:alertController animated:true completion:nil];
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"是否注销" message:@"是否注销原有用户" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"注销", nil];
        
        [alert oddity_showAlertViewWithCompleteBlock:^(NSInteger buttonIndex) {
            
            if (buttonIndex == 1) {
                
                [weakSelf handleLogOutMethod];
            }
        }];
    }
}

-(void)handleLogOutMethod{

    OddityAlertViewViewController *alertController = [[OddityAlertViewViewController alloc] initWitch:@"注销中"];
    
    [self presentViewController:alertController animated:true completion:^{
        
        [OddityShareUser.defaultUser localLogOutMethod:^(BOOL success, NSString *mess) {
            
            [alertController makeChange:@"注销完成" after:1 block:^{
                
                [self dismissViewControllerAnimated:true completion:^{
                    
                    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:true completion:nil];
                }];
            }];
        }];
    }];
}

/**
 设置 数据源
 */
-(void)makeDataSources{

    _fontSet = [[OddityUserCenterObject alloc] initWith: NSString.oddity_setting_font_string iconImage: UIImage.oddity_setting_font_image type:  UserCenterSettingTypeChangeFont];
//    _pushSet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_setting_push_string iconImage: UIImage.oddity_setting_push_image type:  UserCenterSettingTypeSwitch];
    _moonSet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_setting_moon_string iconImage: UIImage.oddity_setting_moon_image type:  UserCenterSettingTypeSwitch];
    
    _deleteSet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_setting_delete_string iconImage: UIImage.oddity_setting_delete_image type:  UserCenterSettingTypeDel];
    
    _aboutSet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_setting_about_string iconImage: UIImage.oddity_setting_about_image type:  UserCenterSettingTypeGo];
    _privacySet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_setting_privacy_string iconImage: UIImage.oddity_setting_privacy_image type:  UserCenterSettingTypeGo];
    _gradeSet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_setting_grade_string iconImage: UIImage.oddity_setting_grade_image type:  UserCenterSettingTypeGo];
    
//    _dataSources = @[@[_fontSet,_pushSet,_moonSet],@[_deleteSet],@[_aboutSet,_privacySet,_gradeSet]];
    _dataSources = @[@[_fontSet,_moonSet],@[_deleteSet],@[_aboutSet,_privacySet,_gradeSet]];
}

-(void)dismissMethod{
    
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)refreshThemeMethod{
    
    _logOutButton.backgroundColor = [OddityUISetting.shareOddityUISetting.mainTone colorWithAlphaComponent:0.7];
    [_logOutButton setTitleColor: [UIColor color_t_9] forState:(UIControlStateNormal)];
    
    self.tableView.backgroundColor = [UIColor color_t_6];
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    [self makeDataSources];
    
    [self.tableView reloadData];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return self.dataSources.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    NSArray *array = (NSArray *)[self.dataSources objectAtIndex:section];
    
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    OddityUserCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    OddityUserCenterObject *object = self.dataSources[indexPath.section][indexPath.row];
    
    [self handleSetting:indexPath cell:[cell fill: object]];
    
    if ([object isEqual:self.moonSet]) {
        
        UISwitch *moonSwitch = ((UISwitch *)cell.rightView);
        
        moonSwitch.on = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNight;
        
        [moonSwitch addTarget:self action:@selector(changeNightAndDayMethod) forControlEvents:(UIControlEventTouchUpInside)];
    }
    
    if ([object isEqual:self.pushSet]) {
        
        [cell makeSubTitleString:@"关闭时，你可能错过重要资讯通知"];
        
        UISwitch *pushSwitch = ((UISwitch *)cell.rightView);
        pushSwitch.enabled = false;
    }
    
    return cell;
}

-(void)handleSetting:(NSIndexPath *)indexPath cell:(OddityUserCenterTableViewCell *)cell{

    NSInteger count = [self.tableView numberOfRowsInSection:indexPath.section];
    
    if (count > 1 ) {
        
        if (indexPath.row > 0) { [cell hidden:(UserCenterTableViewCellBorderPostionTop)]; }
        
        if (indexPath.row+1 < count) { [cell update:(UserCenterTableViewCellBorderPostionBottom) leftMargin:44]; }
    }
}



-(void)changeNightAndDayMethod{

    OddityUISetting.shareOddityUISetting.oddityThemeMode = OddityUISetting.shareOddityUISetting.oddityThemeMode == OddityThemeModeNight ? OddityThemeModeNormal : OddityThemeModeNight ;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (self.dataSources[indexPath.section][indexPath.row] == self.pushSet) {
        
        return 68;
    }
    
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if (section == 2) {
        
        return 20;
    }
    
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (self.dataSources[indexPath.section][indexPath.row] == self.privacySet) {
        
        [self presentViewController:[[OddityPrivacyViewController alloc]init] animated:true completion:nil]; // 隐私
        return;
    }
    
    if (self.dataSources[indexPath.section][indexPath.row] == self.aboutSet) {
        
        [self presentViewController:[[OddityAboutViewController alloc]init] animated:true completion:nil]; // 隐私
        return;
    }
    
    if (self.dataSources[indexPath.section][indexPath.row] == self.gradeSet) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://itunes.apple.com/app/id987333155"]];
        return;
    }
    
    if (self.dataSources[indexPath.section][indexPath.row] == self.deleteSet) {
        
        [PINDiskCache emptyTrash];
        
        [OddityClearCacheTool clearCacheWithFilePath:NSString.oddity_cachePathString];
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:(UITableViewRowAnimationNone)];
    }
}

@end
