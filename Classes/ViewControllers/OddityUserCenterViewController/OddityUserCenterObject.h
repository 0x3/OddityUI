//
//  OddityUserCenterObject.h
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, UserCenterSettingType) {
    UserCenterSettingTypeNone,
    UserCenterSettingTypeDel,
    UserCenterSettingTypeGo,
    UserCenterSettingTypeSwitch,
    UserCenterSettingTypeChangeFont
};

@interface OddityUserCenterObject : NSObject

@property(nonatomic,strong)UIImage *iconImage;

@property(nonatomic,strong)NSString *settingName;

@property(nonatomic,assign)UserCenterSettingType type;

-(instancetype)initWith:(NSString *)title iconImage:(UIImage *)icon type:(UserCenterSettingType)type;

@end
