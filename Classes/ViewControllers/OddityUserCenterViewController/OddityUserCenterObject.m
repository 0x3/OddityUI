//
//  OddityUserCenterObject.m
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//

#import "OddityUserCenterObject.h"

@implementation OddityUserCenterObject

-(instancetype)initWith:(NSString *)title iconImage:(UIImage *)icon type:(UserCenterSettingType)type{

    self = [super init];
    
    if (self) {
        
        self.settingName = title;
        self.iconImage = icon;
        self.type = type;
    }
    
    return self;
}

@end
