//
//  OddityUserCenterViewController.m
//  Pods
//
//  Created by 荆文征 on 2017/6/9.
//
//

#import "OddityShareUser.h"
#import "OddityCategorys.h"
#import "OddityNavigationBar.h"

#import "OddityUserCenterObject.h"

#import <Masonry/Masonry.h>
#import "OddityUserCenterViewController.h"

#import "OddityUserCenterTableViewCell.h"

#import "OdditySettingViewController.h"

#import "OddityNikeNameViewController.h"
#import <PINRemoteImage/PINImageView+PINRemoteImage.h>

#import "OddityMyCommentViewController.h"
#import "OddityMyCollectViewController.h"
#import "OddityMyMessageViewController.h"

#import "OddityLogObject.h"

@interface OddityUserCenterViewController ()<UITableViewDataSource,UITableViewDelegate>


/**
 上方 Bar 的相关
 */
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)OddityBigButton *cancelButton;
@property(nonatomic,strong)OddityBigButton *settingButton;
@property(nonatomic,strong)OddityNavigationBar *naviGationBar;

@property(nonatomic,strong)UIScrollView *contentView;


@property(nonatomic,strong)UITableView *settingTableView;
/**
  个人中心 的资料相关
 */
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UIView *imageBackView;
@property(nonatomic,strong)OddityThemeImageView *imageView;

@property(nonatomic,strong)NSArray<OddityUserCenterObject *> *dataSources;

@end

@implementation OddityUserCenterViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    if ([OddityPlayerView sharePlayerView].isFullScreen) {
        
        return UIStatusBarStyleLightContent;
    }
    
    return [[OddityUISetting shareOddityUISetting] statusStyle];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self layout];
}


-(void)layout{
    
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    
    _naviGationBar = [[OddityNavigationBar alloc] init];
    [self.view addSubview:_naviGationBar];
    [_naviGationBar mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.top.mas_equalTo(self.mas_topLayoutGuide);
        make.left.with.right.mas_equalTo(self.view);
        make.height.mas_equalTo(44);
    }];
    
    _cancelButton = [[OddityBigButton alloc] init];
    [_cancelButton setTitle:NSString.oddity_usercenter_close_string forState:(UIControlStateNormal)];
    [_cancelButton setTitleColor:[UIColor color_t_2] forState:(UIControlStateNormal)];
    _cancelButton.titleLabel.font = UIFont.font_t_5;
    [_naviGationBar addSubview:_cancelButton];
    [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.centerY.mas_equalTo(_naviGationBar);
        make.left.mas_equalTo(16);
    }];
    
    [_cancelButton addTarget:self action:@selector(dismissMethod) forControlEvents:(UIControlEventTouchUpInside)];
    
    _settingButton = [[OddityBigButton alloc] init];
    [_settingButton setTitle:NSString.oddity_usercenter_setting_string forState:(UIControlStateNormal)];
    [_settingButton setTitleColor:[UIColor color_t_2] forState:(UIControlStateNormal)];
    [_settingButton addTarget:self action:@selector(goSetting) forControlEvents:(UIControlEventTouchUpInside)];
    _settingButton.titleLabel.font = UIFont.font_t_5;
    [_naviGationBar addSubview:_settingButton];
    [_settingButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.mas_equalTo(_naviGationBar);
        make.right.mas_equalTo(-16);
    }];
    
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.text = NSString.oddity_usercenter_me_string;
    _titleLabel.textColor = [UIColor color_t_2];
    _titleLabel.font = [UIFont font_t_3];
    [_naviGationBar addSubview:_titleLabel];
    [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.center.mas_equalTo(_naviGationBar);
    }];
    
    _contentView = [[UIScrollView alloc] init];
    _contentView.backgroundColor = [UIColor color_t_6];
    _contentView.alwaysBounceVertical = true;
    [self.view addSubview:_contentView];
    [_contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_naviGationBar.mas_bottom);
        make.left.width.right.with.bottom.mas_equalTo(self.view);
    }];
    
    _imageBackView = [[UIView alloc] init];
    _imageBackView.backgroundColor = [UIColor color_t_9];
    _imageBackView.layer.cornerRadius = 40;
    _imageBackView.clipsToBounds = true;
    [_contentView addSubview:_imageBackView];
    [_imageBackView mas_remakeConstraints:^(MASConstraintMaker *make) {
        
        make.centerX.mas_equalTo(_contentView);
        make.top.mas_equalTo(79);
        make.size.mas_equalTo(CGSizeMake(80, 80));
    }];
    
    _imageView = [[OddityThemeImageView alloc] init];
    _imageView.layer.cornerRadius = 34.5;
    _imageView.clipsToBounds = true;
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    [_imageBackView addSubview:_imageView];
    [_imageView pin_setImageFromURL:[[NSURL alloc]initWithString:OddityShareUser.defaultUser.avator ] placeholderImage:UIImage.oddity_usercenter_avator_placeholder_image];
    [_imageView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.center.mas_equalTo(_imageBackView);
        make.size.mas_equalTo(CGSizeMake(71, 71));
    }];
    
    _nameLabel = [[UILabel alloc] init];
    _nameLabel.text = OddityShareUser.defaultUser.uname;
    _nameLabel.textColor = [UIColor color_t_2];
    _nameLabel.font = [UIFont font_t_2];
    _nameLabel.userInteractionEnabled = true;
    [self.contentView addSubview:_nameLabel];
    [_nameLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickUserNameAction)]];
    [_nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_imageBackView.mas_bottom).offset(14);
        make.centerX.mas_equalTo(_imageBackView);
    }];
    
    _settingTableView = [[UITableView alloc] initWithFrame:(CGRectZero)];
    [_settingTableView registerClass:[OddityUserCenterTableViewCell class] forCellReuseIdentifier:@"cell"];
    _settingTableView.delegate = self;
    _settingTableView.dataSource = self;
    _settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_contentView addSubview:_settingTableView];
    [_settingTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
       
        make.top.mas_equalTo(_nameLabel.mas_bottom).offset(85);
        make.left.with.right.mas_equalTo(0);
        make.width.mas_equalTo(_contentView);
        make.height.mas_equalTo(57*3);
        make.bottom.mas_lessThanOrEqualTo(-100);
    }];
    
//    [[UIImageView alloc] initWithImage:[UIImage oddityImage:@""]]
    
    [self makeDataSourcesMethod];
    
    
    UITapGestureRecognizer *boomTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeFontSizeToSmall)];
    boomTap.numberOfTouchesRequired = 2;
    boomTap.numberOfTapsRequired = 2;
    [self.contentView addGestureRecognizer:boomTap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshThemeMethod) name:@"oddityThemeModeChange" object:nil];
}

-(void)changeFontSizeToSmall{

    OddityUISetting.shareOddityUISetting.oddityFontMode = -1;
    
    [UIView animateWithDuration:0.3 animations:^{
       
        self.imageBackView.transform = CGAffineTransformTranslate(self.imageBackView.transform, -300, -200);
        self.imageBackView.transform = CGAffineTransformRotate(self.imageBackView.transform, M_PI);
    }];
    
    [UIView animateWithDuration:0.3 delay:0.3 usingSpringWithDamping:0.35 initialSpringVelocity:0 options:(UIViewAnimationOptionCurveEaseInOut) animations:^{
        
        self.imageBackView.transform = CGAffineTransformIdentity;
        
    } completion:nil];
}

-(void)clickUserNameAction{

//    [self presentViewController:[[OddityNikeNameViewController alloc] initWithOldUserName:self.nameLabel.text] animated:true completion:nil];
}

-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    
    [self.settingTableView deselectRowAtIndexPath:self.settingTableView.indexPathForSelectedRow animated:true];
}

-(void)makeDataSourcesMethod{

    
    
    OddityUserCenterObject *commentSet = [[OddityUserCenterObject alloc] initWith: NSString.oddity_usercenter_comment_string iconImage: [UIImage oddity_usercenter_comment_image] type:  UserCenterSettingTypeGo];
    OddityUserCenterObject *favoriteSet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_usercenter_favorite_string iconImage: [UIImage oddity_usercenter_favorite_image] type:  UserCenterSettingTypeGo];
    OddityUserCenterObject *newsSet = [[OddityUserCenterObject alloc] initWith:NSString.oddity_usercenter_news_string iconImage: [UIImage oddity_usercenter_news_image] type:  UserCenterSettingTypeGo];
    
    _dataSources = @[commentSet,favoriteSet,newsSet];
}

-(void)refreshThemeMethod{
    
    [_settingButton setTitleColor:[UIColor color_t_2] forState:(UIControlStateNormal)];
    [_cancelButton setTitleColor:[UIColor color_t_2] forState:(UIControlStateNormal)];
    
    _nameLabel.textColor = [UIColor color_t_2];
    _imageBackView.backgroundColor = [UIColor color_t_9];
    _titleLabel.textColor = [UIColor color_t_2];
    _titleLabel.font = [UIFont font_t_3];
    self.view.backgroundColor =  [OddityUISetting shareOddityUISetting].backColor;
    [self.naviGationBar refreshThemeMethod];
    _contentView.backgroundColor = [UIColor color_t_6];
    
    [self makeDataSourcesMethod];
    
    [self.imageView layoutSubviews];
    
    [self.settingTableView reloadData];
}

-(void)dismissMethod{

    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)goSetting{

    [OddityLogObject createAppAction:OddityLogAtypeProfileSettingClick fromPageName:OddityLogPageUserCenterPage toPageName:OddityLogPageSettingPage effective:false params:nil];
    
    [self presentViewController:[[OdditySettingViewController alloc] init] animated:true completion:nil];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return _dataSources.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    OddityUserCenterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    [cell fill:self.dataSources[indexPath.row]];
    
    if (indexPath.row == 1 || indexPath.row == 2) {
        
        [cell hidden:(UserCenterTableViewCellBorderPostionTop)];
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 57;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.row == 0) {
        
        [OddityLogObject createAppAction:OddityLogAtypeMyComments fromPageName:OddityLogPageUserCenterPage toPageName:OddityLogPageMyCommentPage effective:true params:nil];
        
        [self presentViewController:[[OddityMyCommentViewController alloc] init] animated:true completion:nil];
    } else if (indexPath.row == 1) {
        
        [OddityLogObject createAppAction:OddityLogAtypeMyCollections fromPageName:OddityLogPageUserCenterPage toPageName:OddityLogPageMyCollectionPage effective:true params:nil];
        
        [self presentViewController:[[OddityMyCollectViewController alloc] init] animated:true completion:nil];
    }else if (indexPath.row == 2) {
        
        [OddityLogObject createAppAction:OddityLogAtypeMyMessages fromPageName:OddityLogPageUserCenterPage toPageName:OddityLogPageMyMessagePage effective:true params:nil];
        
        [self presentViewController:[[OddityMyMessageViewController alloc] init] animated:true completion:nil];
    }
}

@end
