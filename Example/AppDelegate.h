//
//  AppDelegate.h
//  Example
//
//  Created by 荆文征 on 2017/5/25.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 是否为 trvis-ci budile 模式
 */
#define TRAVIS_CI_BUDLID_MODE true


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

