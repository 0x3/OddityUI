//
//  ExampleViewController.m
//  Example
//
//  Created by 荆文征 on 2017/6/29.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import "ExampleViewController.h"
#import <OddityOcUI/OddityOcUI.h>

@interface ExampleViewController ()

@end

@implementation ExampleViewController


- (IBAction)clickButton:(id)sender {
    
    [OdditySetting.shareOdditySetting toNewDetailWithNid:@(21625236) channelId:9 channelTitle: @"国际" presentingVC:self firstReuqestErrorBlock:^{
        
        NSLog(@"网络配置失败");
    }];
}

@end
