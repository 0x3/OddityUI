//
//  ViewController.m
//  StoryBoard
//
//  Created by 荆文征 on 2017/5/18.
//  Copyright © 2017年 timer. All rights reserved.
//

#import "AppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "ViewController.h"
#import <Masonry/Masonry.h>

#if !TRAVIS_CI_BUDLID_MODE

#import <UMSocialCore/UMSocialCore.h>

#endif


@implementation TabBarViewController

-(BOOL)shouldAutorotate{
    
    return false;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle{

    return [self.selectedViewController preferredStatusBarStyle];
}

-(UIViewController *)childViewControllerForStatusBarStyle{

    return self.selectedViewController;
}

-(void)viewDidLoad{

    [super viewDidLoad];
    
//    [self setSelectedIndex:1];
}

@end

#import <OddityOcUI/GDTSplashAd.h>

#define OddityGDTSDKAppkeyString @"1105877617"

@interface ViewController()<OdditySettingDelegate,GDTSplashAdDelegate>

@property(nonatomic,strong) UIView *bottomView;

@property(nonatomic,strong) GDTSplashAd *splashAd;

@end

@implementation ViewController

- (instancetype)initWithCoder:(NSCoder *)coder{
    
    self = [super initWithCoder:coder];
    if (self) {
        
        [self configOdditySetting];
        [self configOddityUISetting];
    }
    return self;
}

-(void)viewDidLoad{

    [super viewDidLoad];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(20 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSLog(@" 返回了了了了了");
        
        [OdditySetting.shareOdditySetting backRootViewControllerAnimated:true completion:^{
           
            NSLog(@"消失完成 --- ");
        }];
    });
    
//    self.splashAd = [[GDTSplashAd alloc] initWithAppkey: OddityGDTSDKAppkeyString placementId:@"4030124377302711"];
//    self.splashAd.delegate = self;
//    
//    self.splashAd.backgroundColor = UIColor.redColor;
//    self.splashAd.fetchDelay = 5;
//    
//    [self.splashAd loadAdAndShowInWindow:UIApplication.sharedApplication.keyWindow];
}

//
//
//-(void)splashAdSuccessPresentScreen:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}
//
//-(void)splashAdFailToPresent:(GDTSplashAd *)splashAd withError:(NSError *)error
//{
//    NSLog(@"----%s----%@",__FUNCTION__,error);
//}
//
//-(void)splashAdClicked:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}
//
//-(void)splashAdApplicationWillEnterBackground:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}
//
//-(void)splashAdWillClosed:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}
//
//-(void)splashAdClosed:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//    self.splashAd = nil;
//}
//
//- (void)splashAdWillPresentFullScreenModal:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}
//
//- (void)splashAdDidPresentFullScreenModal:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}
//
//- (void)splashAdWillDismissFullScreenModal:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}
//-(void)splashAdDidDismissFullScreenModal:(GDTSplashAd *)splashAd
//{
//    NSLog(@"----%s----",__FUNCTION__);
//}



-(void)configOddityUISetting{

    OddityUISetting * uiSetting = OddityUISetting.shareOddityUISetting;
    
    [uiSetting setMainToneColor:[UIColor colorWithRed:(CGFloat)251/255 green:(CGFloat)122/255 blue:(CGFloat)18/255 alpha:1] forThemeMode:(OddityThemeModeNormal)];
    [uiSetting setMainToneColor:[UIColor colorWithRed:(CGFloat)156/255 green:(CGFloat)84/255 blue:(CGFloat)27/255 alpha:1] forThemeMode:(OddityThemeModeNight)];
    
    [uiSetting setNavTitleColor:[UIColor colorWithRed:(CGFloat)51/255 green:(CGFloat)51/255 blue:(CGFloat)51/255 alpha:1] forThemeMode:(OddityThemeModeNormal)];
    [uiSetting setNavTitleColor:[UIColor colorWithRed:(CGFloat)141/255 green:(CGFloat)141/255 blue:(CGFloat)141/255 alpha:1] forThemeMode:(OddityThemeModeNight)];
}

-(void)configOdditySetting{
    
    OdditySetting *shareOdditySetting = [OdditySetting shareOdditySetting];

    shareOdditySetting.isHiddenFirstSubscribe = true;
    
    shareOdditySetting.shareObjectArray = self.setShareArray;
    
    [shareOdditySetting BugRequestCollectionMethod];
    
    shareOdditySetting.isCacheDetailImage = true;
    
    shareOdditySetting.delegate = self;
    
    [shareOdditySetting BugRequestCollectionMethod];
    
    [[NSUserDefaults standardUserDefaults] setValue:@(NO) forKey:@"_UIConstraintBasedLayoutLogUnsatisfiable"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSArray *)setShareArray{

    OddityShareModeObject *wmoments = [OddityShareModeObject.alloc initWithMode:(OddityShareModeWeChatmoMents)];
    OddityShareModeObject *wfriends = [OddityShareModeObject.alloc initWithMode:(OddityShareModeWeChatFriends)];
    OddityShareModeObject *sina = [OddityShareModeObject.alloc initWithMode:(OddityShareModeSina)];
    
    NSArray *firstArray = @[wmoments,wfriends,sina];
    
    OddityShareModeObject *sms = [OddityShareModeObject.alloc initWithMode:(OddityShareModeSMS)];
    OddityShareModeObject *email = [OddityShareModeObject.alloc initWithMode:(OddityShareModeEmail)];
    OddityShareModeObject *copylink = [OddityShareModeObject.alloc initWithMode:(OddityShareModeCopyLink)];
    OddityShareModeObject *night = [OddityShareModeObject.alloc initWithMode:(OddityShareModeTheme)];
    OddityShareModeObject *fontsize = [OddityShareModeObject.alloc initWithMode:(OddityShareModeFontSize)];
    
    NSArray *sendArray = @[sms,email,copylink,night,fontsize];
    
    return @[firstArray,sendArray];
}


#if !TRAVIS_CI_BUDLID_MODE
-(void)didClickShareActionWithMode:(enum OddityShareMode)mode shareObject:(OddityShareObject *)obj shareViewController:(UIViewController *)viewController{
    
    
    
    UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
    
    UMShareWebpageObject *shareObject = [UMShareWebpageObject shareObjectWithTitle:obj.shareTitle descr:obj.shareTitle thumImage:obj.shareImageURLString];
    shareObject.webpageUrl = obj.shareURLString;
    messageObject.shareObject = shareObject;
    
    [UMSocialGlobal shareInstance].isUsingHttpsWhenShareContent = NO;
    
    [[UMSocialManager defaultManager] shareToPlatform:(UMSocialPlatformType)ODDITY_UMENG_SHARE_PLATFORM(mode) messageObject:messageObject currentViewController:viewController completion:^(id data, NSError *error) {
        if (error) { [self showError:error]; }
    }];
    
}

-(void)showError:(NSError *)error{

    NSString *result = @"";
    
    switch (error.code) {
        case UMSocialPlatformErrorType_Unknow:
            result = @"未知错误";
            break;
        case UMSocialPlatformErrorType_NotSupport:
            result = @"不支持（url scheme 没配置，或者没有配置-ObjC， 或则SDK版本不支持或则客户端版本不支持";
            break;
        case UMSocialPlatformErrorType_AuthorizeFailed:
            result = @"授权失败";
            break;
        case UMSocialPlatformErrorType_ShareFailed:
            result = @"分享失败";
            break;
        case UMSocialPlatformErrorType_RequestForUserProfileFailed:
            result = @"请求用户信息失败";
            break;
        case UMSocialPlatformErrorType_ShareDataNil:
            result = @"分享内容为空";
            break;
        case UMSocialPlatformErrorType_ShareDataTypeIllegal:
            result = @"分享内容不支持";
            break;
        case UMSocialPlatformErrorType_CheckUrlSchemaFail:
            result = @"schemaurl fail";
            break;
        case UMSocialPlatformErrorType_NotInstall:
            result = @"应用未安装";
            break;
        case UMSocialPlatformErrorType_Cancel:
            result = @"您已取消分享";
            break;
        case UMSocialPlatformErrorType_NotNetWork:
            result = @"网络异常";
            break;
        case UMSocialPlatformErrorType_SourceError:
            result = @"第三方错误";
            break;
        case UMSocialPlatformErrorType_ProtocolNotOverride:
            result = @"对应的  UMSocialPlatformProvider的方法没有实现";
            break;
        default:
        break;
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:UIApplication.sharedApplication.keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.text = result;
    [hud hideAnimated:YES afterDelay:1.f];
}


-(void)didClickThirdSignActionWithMode:(OddityShareMode)mode{

    [[UMSocialManager defaultManager] getUserInfoWithPlatform:(UMSocialPlatformType)mode currentViewController:nil completion:^(id result, NSError *error) {
        if (error) {
            
        } else {
            
            UMSocialUserInfoResponse *resp = result;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            [OdditySetting.shareOdditySetting thirdPartySign:mode suid:resp.uid stoken:resp.accessToken sexpires:[dateFormatter stringFromDate:resp.expiration] username:resp.name gender:resp.unionGender avatar:resp.iconurl complete:^(BOOL success, NSString *mess) {
                
                
            }];
        }
    }];
}

#endif

@end
