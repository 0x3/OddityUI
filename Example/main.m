//
//  main.m
//  Example
//
//  Created by 荆文征 on 2017/5/25.
//  Copyright © 2017年 aimobier. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
