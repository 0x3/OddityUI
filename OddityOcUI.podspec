Pod::Spec.new do |s|
  s.name = 'OddityOcUI'
  s.summary          = "奇点资讯 UI 展示信息 [- OddityUI -]"
  s.homepage         = "https://github.com/AimobierCocoaPods/OddityUI/"
  s.author           = { "WenZheng Jing" => "200739491@qq.com" }
  s.ios.deployment_target = '7.0'
  s.version = '0.4.0'
  s.source = { :git => 'https://github.com/aimobier/Corn.git', :tag => s.version }
  s.license = 'MIT'



  s.subspec 'Core' do |core|

    core.source_files = 'Classes/**/*.{h,m}'
    core.exclude_files = 'Classes/GDTlibs/*.h'
    core.resource_bundles = { "OdditBundle" => "Classes/**/**/*.{js,css,png,strings,html}" }
    core.prefix_header_file = 'Classes/Utils/OddityPublic.pch'

    core.public_header_files = 'Classes/OddityOcUI.h','Classes/Utils/OdditySetting.h','Classes/Utils/OddityUISetting.h','Classes/ViewControllers/OddityMainManagerViewController.h'

    core.dependency 'KSCrash'
    core.dependency 'Realm','~> 2.8.0'
    core.dependency 'Masonry'
    core.dependency 'MJRefresh'
    core.dependency 'AFNetworking'
    core.dependency 'PINRemoteImage'
    core.dependency 'JXLPagerTabStrip'
    core.dependency 'UITableView+FDTemplateLayoutCell'

    core.ios.frameworks = 'WebKit'

    core.dependency 'OddityOcUI/GDTSDK'
  end

  s.subspec 'GDTSDK' do |gdt|

    gdt.source_files = 'Classes/GDTlibs/*.{h,m}'
    gdt.public_header_files = 'Classes/GDTlibs/*.{h,m}'
    gdt.vendored_libraries = 'Classes/GDTlibs/libGDTMobSDK.a'

    gdt.libraries = 'z'
    gdt.frameworks = 'AdSupport','CoreLocation','QuartzCore','Security','CoreTelephony','StoreKit','SystemConfiguration'
  end

end
