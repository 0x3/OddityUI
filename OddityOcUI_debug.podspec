Pod::Spec.new do |s|
  s.name = 'OddityOcUI'
  s.summary          = "奇点资讯 UI 展示信息 [- OddityUI -]"
  s.homepage         = "https://github.com/OddityUI/OddityUI"
  s.author           = { "WenZheng Jing" => "200739491@qq.com" }
  s.ios.deployment_target = '7.0'
  s.version = '0.3.6'
  s.source = { :git => 'https://github.com/OddityUI/OddityUI.git', :tag => s.version }
  s.license = 'MIT'

  s.source_files = 'Classes/**/*.h'
  s.resource = 'Classes/OdditBundle.bundle'

  s.ios.vendored_libraries = 'Classes/libOddityOcUI.a','Classes/libGDTMobSDK.a'

  s.dependency 'KSCrash'
  s.dependency 'Realm','~> 2.8.0'
  s.dependency 'Masonry'
  s.dependency 'MJRefresh'
  s.dependency 'AFNetworking'
  s.dependency 'PINRemoteImage'
  s.dependency 'JXLPagerTabStrip'
  s.dependency 'UITableView+FDTemplateLayoutCell'

  s.libraries = 'z'
  s.frameworks = 'AdSupport','CoreLocation','QuartzCore','Security','CoreTelephony','StoreKit','SystemConfiguration','WebKit'

end
